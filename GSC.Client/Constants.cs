﻿using JetBrains.Annotations;

namespace GSC.Client
{
    internal static class Constants
    {
        public const string Required = "Требуется указать значение";
        public const string Range0To5 = "Значение поля должно быть в диапазоне от 0 до 5.";
        public const string Code = "Код";
        public const string Name = "Наименование";
        public const string OnlyPositives = "Отрицательные значения недопустимы.";
        public const string EmailRegExPatt = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
    }
}