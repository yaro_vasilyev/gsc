﻿namespace GSC.Client
{
    partial class GscHostForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GscHostForm));
            this.navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgDatabase = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiExperts = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiObjects = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiViolations = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiContractInfos = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiManagers = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiAcademicDegree = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiAcademicTitle = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiSubject = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgSchedule = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiSchedule = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgContracts = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiContracts = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgMonitor = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiMonitor = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiMonitorReports = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgCallCenter = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiCallCenter = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgReports = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiReportDatabase = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiReportSchedule = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiReportContracts = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiReportMonitor = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiReportCallCenter = new DevExpress.XtraNavBar.NavBarItem();
            this.nbgAdmin = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiAdminProject = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiAdminUsers = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiPrivateCabinetAccess = new DevExpress.XtraNavBar.NavBarItem();
            this.navbarImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.navbarImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.navigationFrame = new DevExpress.XtraBars.Navigation.NavigationFrame();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.appMenu = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.mvvmContext = new DevExpress.Utils.MVVM.MVVMContext(this.components);
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navbarImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navbarImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).BeginInit();
            this.SuspendLayout();
            // 
            // navBarControl
            // 
            this.navBarControl.ActiveGroup = this.nbgDatabase;
            this.navBarControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgDatabase,
            this.nbgSchedule,
            this.nbgContracts,
            this.nbgMonitor,
            this.nbgCallCenter,
            this.nbgReports,
            this.nbgAdmin});
            this.navBarControl.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nbiExperts,
            this.nbiObjects,
            this.nbiManagers,
            this.nbiViolations,
            this.nbiContractInfos,
            this.nbiCallCenter,
            this.nbiReportDatabase,
            this.nbiReportSchedule,
            this.nbiReportContracts,
            this.nbiReportMonitor,
            this.nbiReportCallCenter,
            this.nbiMonitor,
            this.nbiContracts,
            this.nbiSchedule,
            this.nbiAcademicDegree,
            this.nbiAcademicTitle,
            this.nbiSubject,
            this.nbiAdminProject,
            this.nbiAdminUsers,
            this.nbiMonitorReports,
            this.nbiPrivateCabinetAccess});
            this.navBarControl.LargeImages = this.navbarImageCollectionLarge;
            this.navBarControl.Location = new System.Drawing.Point(0, 54);
            this.navBarControl.Name = "navBarControl";
            this.navBarControl.OptionsNavPane.ExpandedWidth = 203;
            this.navBarControl.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.navBarControl.Size = new System.Drawing.Size(203, 584);
            this.navBarControl.SmallImages = this.navbarImageCollection;
            this.navBarControl.StoreDefaultPaintStyleName = true;
            this.navBarControl.TabIndex = 0;
            this.navBarControl.Text = "navBarControl1";
            // 
            // nbgDatabase
            // 
            this.nbgDatabase.Caption = "База данных";
            this.nbgDatabase.Expanded = true;
            this.nbgDatabase.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiExperts),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiObjects),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiViolations),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiContractInfos),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiManagers),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiAcademicDegree),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiAcademicTitle),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiSubject)});
            this.nbgDatabase.Name = "nbgDatabase";
            // 
            // nbiExperts
            // 
            this.nbiExperts.Caption = "Эксперты";
            this.nbiExperts.Name = "nbiExperts";
            this.nbiExperts.Visible = false;
            // 
            // nbiObjects
            // 
            this.nbiObjects.Caption = "Объекты мониторинга";
            this.nbiObjects.Name = "nbiObjects";
            this.nbiObjects.Visible = false;
            // 
            // nbiViolations
            // 
            this.nbiViolations.Caption = "Типология нарушений";
            this.nbiViolations.Name = "nbiViolations";
            // 
            // nbiContractInfos
            // 
            this.nbiContractInfos.Caption = "Договоры";
            this.nbiContractInfos.Name = "nbiContractInfos";
            this.nbiContractInfos.Visible = false;
            // 
            // nbiManagers
            // 
            this.nbiManagers.Caption = "Менеджеры/координаторы";
            this.nbiManagers.Name = "nbiManagers";
            // 
            // nbiAcademicDegree
            // 
            this.nbiAcademicDegree.Caption = "Ученые степени";
            this.nbiAcademicDegree.Name = "nbiAcademicDegree";
            this.nbiAcademicDegree.Visible = false;
            // 
            // nbiAcademicTitle
            // 
            this.nbiAcademicTitle.Caption = "Ученые звания";
            this.nbiAcademicTitle.Name = "nbiAcademicTitle";
            this.nbiAcademicTitle.Visible = false;
            // 
            // nbiSubject
            // 
            this.nbiSubject.Caption = "Предметы";
            this.nbiSubject.Name = "nbiSubject";
            this.nbiSubject.Visible = false;
            // 
            // nbgSchedule
            // 
            this.nbgSchedule.Caption = "График";
            this.nbgSchedule.Expanded = true;
            this.nbgSchedule.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiSchedule)});
            this.nbgSchedule.Name = "nbgSchedule";
            // 
            // nbiSchedule
            // 
            this.nbiSchedule.Caption = "График выходов экспертов";
            this.nbiSchedule.Name = "nbiSchedule";
            this.nbiSchedule.Visible = false;
            // 
            // nbgContracts
            // 
            this.nbgContracts.Caption = "Договоры";
            this.nbgContracts.Expanded = true;
            this.nbgContracts.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiContracts)});
            this.nbgContracts.Name = "nbgContracts";
            // 
            // nbiContracts
            // 
            this.nbiContracts.Caption = "Договоры с экспертами";
            this.nbiContracts.Name = "nbiContracts";
            this.nbiContracts.Visible = false;
            // 
            // nbgMonitor
            // 
            this.nbgMonitor.Caption = "Мониторинг";
            this.nbgMonitor.Expanded = true;
            this.nbgMonitor.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiMonitor),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiMonitorReports)});
            this.nbgMonitor.Name = "nbgMonitor";
            // 
            // nbiMonitor
            // 
            this.nbiMonitor.Caption = "Результаты мониторинга";
            this.nbiMonitor.Name = "nbiMonitor";
            this.nbiMonitor.Visible = false;
            // 
            // nbiMonitorReports
            // 
            this.nbiMonitorReports.Caption = "Отчеты по мониторингу";
            this.nbiMonitorReports.Name = "nbiMonitorReports";
            this.nbiMonitorReports.Visible = false;
            // 
            // nbgCallCenter
            // 
            this.nbgCallCenter.Caption = "Горячая линия";
            this.nbgCallCenter.Expanded = true;
            this.nbgCallCenter.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiCallCenter)});
            this.nbgCallCenter.Name = "nbgCallCenter";
            // 
            // nbiCallCenter
            // 
            this.nbiCallCenter.Caption = "Работа горячей линии";
            this.nbiCallCenter.Name = "nbiCallCenter";
            this.nbiCallCenter.Visible = false;
            // 
            // nbgReports
            // 
            this.nbgReports.Caption = "Отчеты";
            this.nbgReports.Expanded = true;
            this.nbgReports.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiReportDatabase),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiReportSchedule),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiReportContracts),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiReportMonitor),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiReportCallCenter)});
            this.nbgReports.Name = "nbgReports";
            // 
            // nbiReportDatabase
            // 
            this.nbiReportDatabase.Caption = "База данных";
            this.nbiReportDatabase.Name = "nbiReportDatabase";
            this.nbiReportDatabase.Visible = false;
            // 
            // nbiReportSchedule
            // 
            this.nbiReportSchedule.Caption = "График выходов";
            this.nbiReportSchedule.Name = "nbiReportSchedule";
            this.nbiReportSchedule.Visible = false;
            // 
            // nbiReportContracts
            // 
            this.nbiReportContracts.Caption = "Договоры с экспертами";
            this.nbiReportContracts.Name = "nbiReportContracts";
            this.nbiReportContracts.Visible = false;
            // 
            // nbiReportMonitor
            // 
            this.nbiReportMonitor.Caption = "Результаты мониторинга";
            this.nbiReportMonitor.Name = "nbiReportMonitor";
            this.nbiReportMonitor.Visible = false;
            // 
            // nbiReportCallCenter
            // 
            this.nbiReportCallCenter.Caption = "Горячая линия";
            this.nbiReportCallCenter.Name = "nbiReportCallCenter";
            this.nbiReportCallCenter.Visible = false;
            // 
            // nbgAdmin
            // 
            this.nbgAdmin.Caption = "Администрирование";
            this.nbgAdmin.Expanded = true;
            this.nbgAdmin.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiAdminProject),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiAdminUsers),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiPrivateCabinetAccess)});
            this.nbgAdmin.Name = "nbgAdmin";
            // 
            // nbiAdminProject
            // 
            this.nbiAdminProject.Caption = "Проекты";
            this.nbiAdminProject.Name = "nbiAdminProject";
            this.nbiAdminProject.Visible = false;
            // 
            // nbiAdminUsers
            // 
            this.nbiAdminUsers.Caption = "Пользователи";
            this.nbiAdminUsers.Name = "nbiAdminUsers";
            this.nbiAdminUsers.Visible = false;
            // 
            // nbiPrivateCabinetAccess
            // 
            this.nbiPrivateCabinetAccess.Caption = "Доступ в личный кабинет";
            this.nbiPrivateCabinetAccess.Name = "nbiPrivateCabinetAccess";
            this.nbiPrivateCabinetAccess.Visible = false;
            // 
            // navbarImageCollectionLarge
            // 
            this.navbarImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("navbarImageCollectionLarge.ImageStream")));
            this.navbarImageCollectionLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.navbarImageCollectionLarge.Images.SetKeyName(0, "Mail_16x16.png");
            this.navbarImageCollectionLarge.Images.SetKeyName(1, "Organizer_16x16.png");
            // 
            // navbarImageCollection
            // 
            this.navbarImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("navbarImageCollection.ImageStream")));
            this.navbarImageCollection.TransparentColor = System.Drawing.Color.Transparent;
            this.navbarImageCollection.Images.SetKeyName(0, "Inbox_16x16.png");
            this.navbarImageCollection.Images.SetKeyName(1, "Outbox_16x16.png");
            this.navbarImageCollection.Images.SetKeyName(2, "Drafts_16x16.png");
            this.navbarImageCollection.Images.SetKeyName(3, "Trash_16x16.png");
            this.navbarImageCollection.Images.SetKeyName(4, "Calendar_16x16.png");
            this.navbarImageCollection.Images.SetKeyName(5, "Tasks_16x16.png");
            // 
            // navigationFrame
            // 
            this.navigationFrame.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.navigationFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationFrame.Location = new System.Drawing.Point(215, 54);
            this.navigationFrame.Name = "navigationFrame";
            this.navigationFrame.SelectedPage = null;
            this.navigationFrame.Size = new System.Drawing.Size(881, 584);
            this.navigationFrame.TabIndex = 0;
            this.navigationFrame.Text = "navigationFrame1";
            this.navigationFrame.SelectedPageChanged += new DevExpress.XtraBars.Navigation.SelectedPageChangedEventHandler(this.OnNavigationSelectedPageChanged);
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.appMenu;
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Images = this.ribbonImageCollection;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem});
            this.ribbonControl.LargeImages = this.ribbonImageCollectionLarge;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 68;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl.Size = new System.Drawing.Size(1096, 54);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.Merge += new DevExpress.XtraBars.Ribbon.RibbonMergeEventHandler(this.OnMergeRibbonControl);
            // 
            // appMenu
            // 
            this.appMenu.Name = "appMenu";
            this.appMenu.Ribbon = this.ribbonControl;
            this.appMenu.ShowRightPane = true;
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 638);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1096, 23);
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "Office 2013";
            // 
            // mvvmContext
            // 
            this.mvvmContext.ContainerControl = this;
            this.mvvmContext.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterDocumentManagerService(null, false, this.navigationFrame)});
            this.mvvmContext.ViewModelType = typeof(GSC.Client.ViewModels.GscContextViewModel);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(203, 54);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(12, 584);
            this.splitterControl1.TabIndex = 3;
            this.splitterControl1.TabStop = false;
            // 
            // GscHostForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 661);
            this.Controls.Add(this.navigationFrame);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.navBarControl);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.Name = "GscHostForm";
            this.Ribbon = this.ribbonControl;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Мониторинг проведения экзаменов";
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navbarImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navbarImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu appMenu;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.Utils.ImageCollection ribbonImageCollection;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private DevExpress.XtraNavBar.NavBarControl navBarControl;
        private DevExpress.Utils.ImageCollection navbarImageCollection;
        private DevExpress.Utils.ImageCollection navbarImageCollectionLarge;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private DevExpress.XtraBars.Navigation.NavigationFrame navigationFrame;
        private DevExpress.Utils.MVVM.MVVMContext mvvmContext;
        private DevExpress.XtraNavBar.NavBarGroup nbgReports;
        private DevExpress.XtraNavBar.NavBarItem nbiReportDatabase;
        private DevExpress.XtraNavBar.NavBarItem nbiReportSchedule;
        private DevExpress.XtraNavBar.NavBarItem nbiReportContracts;
        private DevExpress.XtraNavBar.NavBarItem nbiReportMonitor;
        private DevExpress.XtraNavBar.NavBarItem nbiReportCallCenter;
        private DevExpress.XtraNavBar.NavBarGroup nbgDatabase;
        private DevExpress.XtraNavBar.NavBarItem nbiExperts;
        private DevExpress.XtraNavBar.NavBarItem nbiObjects;
        private DevExpress.XtraNavBar.NavBarItem nbiManagers;
        private DevExpress.XtraNavBar.NavBarItem nbiViolations;
        private DevExpress.XtraNavBar.NavBarItem nbiContractInfos;
        private DevExpress.XtraNavBar.NavBarGroup nbgSchedule;
        private DevExpress.XtraNavBar.NavBarItem nbiSchedule;
        private DevExpress.XtraNavBar.NavBarGroup nbgContracts;
        private DevExpress.XtraNavBar.NavBarItem nbiContracts;
        private DevExpress.XtraNavBar.NavBarGroup nbgMonitor;
        private DevExpress.XtraNavBar.NavBarItem nbiMonitor;
        private DevExpress.XtraNavBar.NavBarGroup nbgCallCenter;
        private DevExpress.XtraNavBar.NavBarItem nbiCallCenter;
        private DevExpress.XtraNavBar.NavBarItem nbiAcademicTitle;
        private DevExpress.XtraNavBar.NavBarItem nbiAcademicDegree;
        private DevExpress.XtraNavBar.NavBarItem nbiSubject;
        private DevExpress.XtraNavBar.NavBarGroup nbgAdmin;
        private DevExpress.XtraNavBar.NavBarItem nbiAdminProject;
        private DevExpress.XtraNavBar.NavBarItem nbiAdminUsers;
        private DevExpress.XtraNavBar.NavBarItem nbiMonitorReports;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraNavBar.NavBarItem nbiPrivateCabinetAccess;
    }
}
