﻿namespace GSC.Client.Login
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
