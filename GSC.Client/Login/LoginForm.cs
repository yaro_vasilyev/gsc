﻿using System;
using System.Windows.Forms;

namespace GSC.Client.Login
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public LoginForm()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        public Credentials Credentials
        {
            get
            {
                var vm = mvvmContext1.OfType<LoginViewModel>().ViewModel;
                return new Credentials
                {
                    Username = vm.Username,
                    Password = vm.Password
                };
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<LoginViewModel>();
            fluent.SetBinding(loginText, e => e.EditValue, vm => vm.Username);
            fluent.SetBinding(passwordText, e => e.EditValue, vm => vm.Password);

            fluent.BindCommand(loginButton, vm => vm.Login());
            fluent.WithEvent<EventArgs>(this, "Shown").EventToCommand(vm => vm.ClearPassword());
        }

        private void OnTextEdit_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}