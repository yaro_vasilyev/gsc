﻿using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace GSC.Client.Login
{
    [POCOViewModel]
    public class LoginViewModel
    {
        public virtual string Username { get; set; }
        public virtual string Password { get; set; } = "";

        protected virtual void OnUsernameChanged() { this.RaiseCanExecuteChanged(x => x.Login()); }
        protected virtual void OnPasswordChanged() { this.RaiseCanExecuteChanged(x => x.Login()); }

        public LoginViewModel()
        {
            LoginCommand = new DelegateCommand(Login, CanLogin);
        }

        public DelegateCommand LoginCommand { get; private set; }
        
        public void Login()
        {
            // do nothing just close and check availability
        }

        public bool CanLogin()
        {
            return !string.IsNullOrEmpty(Username);
        }

        public void ClearPassword() { Password = ""; }
    }
}