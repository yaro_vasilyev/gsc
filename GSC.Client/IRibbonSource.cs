﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;

namespace GSC.Client
{
    public interface IRibbonSource
    {
        RibbonControl Ribbon { get; }
    }
}
