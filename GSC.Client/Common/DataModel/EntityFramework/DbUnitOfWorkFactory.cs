﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using DevExpress.Data.Async.Helpers;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using GSC.Client.Common.Utils;

namespace GSC.Client.Common.DataModel.EntityFramework
{
    internal class DbUnitOfWorkFactory<TUnitOfWork> : IUnitOfWorkFactory<TUnitOfWork>
        where TUnitOfWork : IUnitOfWork
    {
        private readonly Func<TUnitOfWork> createUnitOfWork;

        public DbUnitOfWorkFactory(Func<TUnitOfWork> createUnitOfWork) { this.createUnitOfWork = createUnitOfWork; }

        TUnitOfWork IUnitOfWorkFactory<TUnitOfWork>.CreateUnitOfWork() { return createUnitOfWork(); }

        IInstantFeedbackSource<TProjection> IUnitOfWorkFactory<TUnitOfWork>.CreateInstantFeedbackSource
            <TEntity, TProjection, TPrimaryKey>(
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection)
        {
            var threadSafeProperties = new TypeInfoProxied(TypeDescriptor.GetProperties(typeof(TProjection)), null).UIDescriptors;
            if (projection == null)
            {
                projection = x => x as IQueryable<TProjection>;
            }
            var source = new EntityInstantFeedbackSource(e => e.QueryableSource = projection(getRepositoryFunc(createUnitOfWork())))
            {
                KeyExpression = getRepositoryFunc(createUnitOfWork()).GetPrimaryKeyPropertyName(),
            };
            return new InstantFeedbackSource<TProjection>(source, threadSafeProperties);
        }
    }

    internal class InstantFeedbackSource<TEntity> 
        : IInstantFeedbackSource<TEntity>
        where TEntity : class
    {
        public readonly EntityInstantFeedbackSource source;
        public readonly PropertyDescriptorCollection threadSafeProperties;

        public InstantFeedbackSource(EntityInstantFeedbackSource source,
                                     PropertyDescriptorCollection threadSafeProperties)
        {
            this.source = source;
            this.threadSafeProperties = threadSafeProperties;
        }

        bool IListSource.ContainsListCollection
        {
            get { return ((IListSource) source).ContainsListCollection; }
        }

        IList IListSource.GetList() { return ((IListSource) source).GetList(); }

        TProperty IInstantFeedbackSource<TEntity>.GetPropertyValue<TProperty>(object threadSafeProxy,
                                                                              Expression<Func<TEntity, TProperty>>
                                                                                  propertyExpression)
        {
            var propertyName = ExpressionHelper.GetPropertyName(propertyExpression);
            var threadSafeProperty = threadSafeProperties[propertyName];
            return (TProperty) threadSafeProperty.GetValue(threadSafeProxy);
        }

        bool IInstantFeedbackSource<TEntity>.IsLoadedProxy(object threadSafeProxy)
        {
            return threadSafeProxy is ReadonlyThreadSafeProxyForObjectFromAnotherThread;
        }

        void IInstantFeedbackSource<TEntity>.Refresh() { source.Refresh(); }
    }
}