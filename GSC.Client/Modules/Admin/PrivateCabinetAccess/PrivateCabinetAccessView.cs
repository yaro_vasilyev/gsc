﻿using System.Linq;
using DevExpress.Data;
using GSC.Client.Util;
using GSC.Client.ViewModels.PrivateCabinetAccess;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Admin.PrivateCabinetAccess
{
    [UsedImplicitly]
    public partial class PrivateCabinetAccessView : CollectionBlockView
    {
        public PrivateCabinetAccessView()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<PrivateCabinetAccessViewModel>();
            fluent.InitGridViewReadonly(gridView, ViewModel);
            fluent.SetBinding(gridControl, control => control.DataSource, viewModel => viewModel.Entities);

            fluent.WithEvent<SelectionChangedEventArgs>(gridView, "SelectionChanged")
                            .SetBinding(viewModel => viewModel.SelectedExperts,
                                eventArgs => gridView.GetSelectedRows().Select(gridView.GetRow).Cast<Model.Expert>().ToArray());
        }

        private PrivateCabinetAccessViewModel ViewModel => mvvmContext1.GetViewModel<PrivateCabinetAccessViewModel>();

        private void gridView_DataSourceChanged(object sender, System.EventArgs e)
        {
            gridView.SelectAll();
        }
    }
}
