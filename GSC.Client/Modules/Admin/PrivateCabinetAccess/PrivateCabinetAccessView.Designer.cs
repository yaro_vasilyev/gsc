﻿namespace GSC.Client.Modules.Admin.PrivateCabinetAccess
{
    partial class PrivateCabinetAccessView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.expertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateLogins = new DevExpress.XtraBars.BarButtonItem();
            this.btsiHasLogin = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.rpExperts = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.PrivateCabinetAccess.PrivateCabinetAccessViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.PrivateCabinetAccess.PrivateCabinetAccessViewModel), "CreateLogins", this.bbiCreateLogins),
            DevExpress.Utils.MVVM.BindingExpression.CreatePropertyBinding(typeof(GSC.Client.ViewModels.PrivateCabinetAccess.PrivateCabinetAccessViewModel), "WithLogins", this.btsiHasLogin, "BindableChecked")});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.PrivateCabinetAccess.PrivateCabinetAccessViewModel);
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.expertBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 141);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(833, 374);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // expertBindingSource
            // 
            this.expertBindingSource.DataSource = typeof(GSC.Model.Expert);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colHasLogin});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsSelection.CheckBoxSelectorColumnWidth = 40;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFullName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.DataSourceChanged += new System.EventHandler(this.gridView_DataSourceChanged);
            // 
            // colFullName
            // 
            this.colFullName.Caption = "ФИО";
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 1;
            // 
            // colHasLogin
            // 
            this.colHasLogin.Caption = "Доступ представлен (есть логин)?";
            this.colHasLogin.FieldName = "HasLogin";
            this.colHasLogin.Name = "colHasLogin";
            this.colHasLogin.Visible = true;
            this.colHasLogin.VisibleIndex = 2;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiCreateLogins
            // 
            this.bbiCreateLogins.Caption = "Сформировать логины";
            this.bbiCreateLogins.Id = 12;
            this.bbiCreateLogins.ImageUri.Uri = "EditDataSource";
            this.bbiCreateLogins.Name = "bbiCreateLogins";
            // 
            // btsiHasLogin
            // 
            this.btsiHasLogin.Caption = "Отображать экспертов, имеющих доступ";
            this.btsiHasLogin.Id = 7;
            this.btsiHasLogin.ImageUri.Uri = "Apply";
            this.btsiHasLogin.Name = "btsiHasLogin";
            // 
            // ribbonControl
            // 
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiRefresh,
            this.btsiHasLogin,
            this.bbiCreateLogins});
            this.ribbonControl1.MaxItemId = 13;
            this.ribbonControl1.Name = "ribbonControl";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpExperts});
            // 
            // rpExperts
            // 
            this.rpExperts.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.ribbonPageGroup1});
            this.rpExperts.Name = "rpExperts";
            this.rpExperts.Text = "Доступ экпертов в личный кабинет";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.ItemLinks.Add(this.bbiCreateLogins);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btsiHasLogin);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Фильтрация";
            // 
            // PrivateCabinetAccessView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "PrivateCabinetAccessView";
            this.Size = new System.Drawing.Size(833, 515);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private System.Windows.Forms.BindingSource expertBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colHasLogin;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpExperts;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.BarToggleSwitchItem btsiHasLogin;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiCreateLogins;
    }
}
