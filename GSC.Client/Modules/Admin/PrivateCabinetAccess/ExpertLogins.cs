﻿using DevExpress.XtraReports.UI;

namespace GSC.Client.Modules.Admin.PrivateCabinetAccess
{
    public partial class ExpertLogins : XtraReport
    {
        public ExpertLogins()
        {
            InitializeComponent();
        }

        public class DataRow
        {
            public string FullName { get; set; }
            public string BaseRegion { get; set; }
            public string Email { get; set; }
            public string Login { get; set; }
            public string Password { get; set; }
        }


        /*
         * “ФИО эксперта”, “Базовый субъект”, “Электронная почта”, “Логин”, “Пароль”
         * */
    }
}
