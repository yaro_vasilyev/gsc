﻿using DevExpress.XtraBars.Ribbon;
using GSC.Client.Util;
using GSC.Client.ViewModels.Logins;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Admin.Logins
{
    [UsedImplicitly]
    public partial class LoginCollectionView : CollectionBlockView, IRibbonSource
    {
        public LoginCollectionView()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<LoginCollectionViewModel>();
            fluent.InitGridView(gridView, ViewModel);
            fluent.SetBinding(gridControl, control => control.DataSource, viewModel => viewModel.Entities);
        }

        private LoginCollectionViewModel ViewModel => mvvmContext1.GetViewModel<LoginCollectionViewModel>();

        RibbonControl IRibbonSource.Ribbon => ribbonControl1;
    }
}
