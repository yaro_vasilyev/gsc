﻿using GSC.Client.ViewModels.Logins;

namespace GSC.Client.Modules.Admin.Logins
{
    partial class LoginView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginBindingSource = new System.Windows.Forms.BindingSource();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteSelectedUserRoles = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddSelectedRoles = new DevExpress.XtraBars.BarButtonItem();
            this.rpAcademicDegrees = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgClose = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.userRolesGridControl = new DevExpress.XtraGrid.GridControl();
            this.userRoleBindingSource = new System.Windows.Forms.BindingSource();
            this.userRolesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSystemRole1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRole1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.allRolesGridControl = new DevExpress.XtraGrid.GridControl();
            this.rolesBindingSource = new System.Windows.Forms.BindingSource();
            this.allRolesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSystemName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.personLookUp = new DevExpress.XtraEditors.LookUpEdit();
            this.personBindingBindingSource = new System.Windows.Forms.BindingSource();
            this.personTypeRadioGroup = new DevExpress.XtraEditors.RadioGroup();
            this.UserLoginTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserLogin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.colSystemRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.userRolesBarManager = new DevExpress.XtraBars.BarManager();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.allRolesBarManager = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userRolesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRolesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personLookUp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personBindingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personTypeRadioGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserLoginTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRolesBarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesBarManager)).BeginInit();
            this.SuspendLayout();
            // 
            // loginBindingSource
            // 
            this.loginBindingSource.DataSource = typeof(GSC.Model.Login);
            this.loginBindingSource.DataSourceChanged += new System.EventHandler(this.loginBindingSource_DataSourceChanged);
            // 
            // mvvmContext
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "Close", this.bbiClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "DeleteSelectedUserRoles", this.bbiDeleteSelectedUserRoles),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Logins.LoginViewModel), "AddSelectedRoles", this.bbiAddSelectedRoles)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Logins.LoginViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // bbiDeleteSelectedUserRoles
            // 
            this.bbiDeleteSelectedUserRoles.Caption = "Удалить выбранное";
            this.bbiDeleteSelectedUserRoles.Id = 0;
            this.bbiDeleteSelectedUserRoles.ImageUri.Uri = "Delete";
            this.bbiDeleteSelectedUserRoles.Name = "bbiDeleteSelectedUserRoles";
            this.bbiDeleteSelectedUserRoles.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiAddSelectedRoles
            // 
            this.bbiAddSelectedRoles.Caption = "Добавить выбранное";
            this.bbiAddSelectedRoles.Id = 0;
            this.bbiAddSelectedRoles.ImageUri.Uri = "Add";
            this.bbiAddSelectedRoles.Name = "bbiAddSelectedRoles";
            this.bbiAddSelectedRoles.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpAcademicDegrees});
            this.ribbonControl1.Size = new System.Drawing.Size(941, 141);
            // 
            // rpAcademicDegrees
            // 
            this.rpAcademicDegrees.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgClose});
            this.rpAcademicDegrees.Name = "rpAcademicDegrees";
            this.rpAcademicDegrees.Text = "Ученые степени";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgClose
            // 
            this.rpgClose.ItemLinks.Add(this.bbiReset);
            this.rpgClose.ItemLinks.Add(this.bbiClose);
            this.rpgClose.Name = "rpgClose";
            this.rpgClose.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.splitContainer1);
            this.dataLayoutControl1.Controls.Add(this.personLookUp);
            this.dataLayoutControl1.Controls.Add(this.personTypeRadioGroup);
            this.dataLayoutControl1.Controls.Add(this.UserLoginTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PasswordTextEdit);
            this.dataLayoutControl1.DataSource = this.loginBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(965, 211, 696, 640);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(941, 568);
            this.dataLayoutControl1.TabIndex = 9;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(24, 206);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.userRolesGridControl);
            this.splitContainer1.Panel1.Controls.Add(this.barDockControl3);
            this.splitContainer1.Panel1.Controls.Add(this.barDockControl4);
            this.splitContainer1.Panel1.Controls.Add(this.barDockControl2);
            this.splitContainer1.Panel1.Controls.Add(this.barDockControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.allRolesGridControl);
            this.splitContainer1.Panel2.Controls.Add(this.barDockControlLeft);
            this.splitContainer1.Panel2.Controls.Add(this.barDockControlRight);
            this.splitContainer1.Panel2.Controls.Add(this.barDockControlBottom);
            this.splitContainer1.Panel2.Controls.Add(this.barDockControlTop);
            this.splitContainer1.Size = new System.Drawing.Size(893, 338);
            this.splitContainer1.SplitterDistance = 451;
            this.splitContainer1.TabIndex = 13;
            // 
            // userRolesGridControl
            // 
            this.userRolesGridControl.DataSource = this.userRoleBindingSource;
            this.userRolesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userRolesGridControl.Location = new System.Drawing.Point(0, 24);
            this.userRolesGridControl.MainView = this.userRolesGridView;
            this.userRolesGridControl.MenuManager = this.ribbonControl1;
            this.userRolesGridControl.Name = "userRolesGridControl";
            this.userRolesGridControl.Size = new System.Drawing.Size(451, 314);
            this.userRolesGridControl.TabIndex = 11;
            this.userRolesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.userRolesGridView});
            // 
            // userRoleBindingSource
            // 
            this.userRoleBindingSource.DataSource = typeof(GSC.Model.UserRole);
            // 
            // userRolesGridView
            // 
            this.userRolesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSystemRole1,
            this.colRole1});
            this.userRolesGridView.GridControl = this.userRolesGridControl;
            this.userRolesGridView.Name = "userRolesGridView";
            this.userRolesGridView.OptionsBehavior.Editable = false;
            this.userRolesGridView.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.userRolesGridView.OptionsSelection.MultiSelect = true;
            this.userRolesGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.userRolesGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colSystemRole1
            // 
            this.colSystemRole1.Caption = "Системное имя";
            this.colSystemRole1.FieldName = "SystemRole";
            this.colSystemRole1.Name = "colSystemRole1";
            this.colSystemRole1.Visible = true;
            this.colSystemRole1.VisibleIndex = 1;
            // 
            // colRole1
            // 
            this.colRole1.Caption = "Описание";
            this.colRole1.FieldName = "Role.Description";
            this.colRole1.Name = "colRole1";
            this.colRole1.Visible = true;
            this.colRole1.VisibleIndex = 2;
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 24);
            this.barDockControl3.Size = new System.Drawing.Size(0, 314);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(451, 24);
            this.barDockControl4.Size = new System.Drawing.Size(0, 314);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 338);
            this.barDockControl2.Size = new System.Drawing.Size(451, 0);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(451, 24);
            // 
            // allRolesGridControl
            // 
            this.allRolesGridControl.DataSource = this.rolesBindingSource;
            this.allRolesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allRolesGridControl.Location = new System.Drawing.Point(0, 24);
            this.allRolesGridControl.MainView = this.allRolesGridView;
            this.allRolesGridControl.MenuManager = this.ribbonControl1;
            this.allRolesGridControl.Name = "allRolesGridControl";
            this.allRolesGridControl.Size = new System.Drawing.Size(438, 314);
            this.allRolesGridControl.TabIndex = 12;
            this.allRolesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.allRolesGridView});
            // 
            // rolesBindingSource
            // 
            this.rolesBindingSource.DataSource = typeof(GSC.Model.Role);
            // 
            // allRolesGridView
            // 
            this.allRolesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSystemName,
            this.colDescription});
            this.allRolesGridView.GridControl = this.allRolesGridControl;
            this.allRolesGridView.Name = "allRolesGridView";
            this.allRolesGridView.OptionsBehavior.Editable = false;
            this.allRolesGridView.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.allRolesGridView.OptionsSelection.MultiSelect = true;
            this.allRolesGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.allRolesGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colSystemName
            // 
            this.colSystemName.Caption = "Системное имя";
            this.colSystemName.FieldName = "SystemName";
            this.colSystemName.Name = "colSystemName";
            this.colSystemName.Visible = true;
            this.colSystemName.VisibleIndex = 1;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Описание";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 314);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(438, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 314);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 338);
            this.barDockControlBottom.Size = new System.Drawing.Size(438, 0);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(438, 24);
            // 
            // personLookUp
            // 
            this.personLookUp.Location = new System.Drawing.Point(24, 140);
            this.personLookUp.MenuManager = this.ribbonControl1;
            this.personLookUp.Name = "personLookUp";
            this.personLookUp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.personLookUp.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.personLookUp.Properties.DataSource = this.personBindingBindingSource;
            this.personLookUp.Properties.DisplayMember = "Name";
            this.personLookUp.Properties.ValueMember = "Id";
            this.personLookUp.Size = new System.Drawing.Size(893, 20);
            this.personLookUp.StyleController = this.dataLayoutControl1;
            this.personLookUp.TabIndex = 10;
            // 
            // personBindingBindingSource
            // 
            this.personBindingBindingSource.DataSource = typeof(GSC.Client.ViewModels.Logins.LoginViewModel.PersonBinding);
            // 
            // personTypeRadioGroup
            // 
            this.personTypeRadioGroup.Location = new System.Drawing.Point(24, 90);
            this.personTypeRadioGroup.MenuManager = this.ribbonControl1;
            this.personTypeRadioGroup.Name = "personTypeRadioGroup";
            this.personTypeRadioGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Менеджер"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Координатор РСМ")});
            this.personTypeRadioGroup.Size = new System.Drawing.Size(893, 30);
            this.personTypeRadioGroup.StyleController = this.dataLayoutControl1;
            this.personTypeRadioGroup.TabIndex = 9;
            // 
            // UserLoginTextEdit
            // 
            this.UserLoginTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.loginBindingSource, "UserLogin", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UserLoginTextEdit.Location = new System.Drawing.Point(57, 12);
            this.UserLoginTextEdit.Name = "UserLoginTextEdit";
            this.UserLoginTextEdit.Size = new System.Drawing.Size(872, 20);
            this.UserLoginTextEdit.StyleController = this.dataLayoutControl1;
            this.UserLoginTextEdit.TabIndex = 4;
            // 
            // PasswordTextEdit
            // 
            this.PasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.loginBindingSource, "Password", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PasswordTextEdit.Location = new System.Drawing.Point(57, 36);
            this.PasswordTextEdit.Name = "PasswordTextEdit";
            this.PasswordTextEdit.Properties.PasswordChar = '*';
            this.PasswordTextEdit.Size = new System.Drawing.Size(872, 20);
            this.PasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.PasswordTextEdit.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(941, 568);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPassword,
            this.ItemForUserLogin,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(921, 548);
            // 
            // ItemForPassword
            // 
            this.ItemForPassword.Control = this.PasswordTextEdit;
            this.ItemForPassword.Location = new System.Drawing.Point(0, 24);
            this.ItemForPassword.Name = "ItemForPassword";
            this.ItemForPassword.Size = new System.Drawing.Size(921, 24);
            this.ItemForPassword.Text = "Пароль";
            this.ItemForPassword.TextSize = new System.Drawing.Size(42, 13);
            // 
            // ItemForUserLogin
            // 
            this.ItemForUserLogin.Control = this.UserLoginTextEdit;
            this.ItemForUserLogin.Location = new System.Drawing.Point(0, 0);
            this.ItemForUserLogin.Name = "ItemForUserLogin";
            this.ItemForUserLogin.Size = new System.Drawing.Size(921, 24);
            this.ItemForUserLogin.Text = "Логин";
            this.ItemForUserLogin.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(921, 116);
            this.layoutControlGroup3.Text = "Привязка к персоне";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.personTypeRadioGroup;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(897, 34);
            this.layoutControlItem1.Text = "Тип персоны";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.personLookUp;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(897, 40);
            this.layoutControlItem2.Text = "Персона";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 164);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(921, 384);
            this.layoutControlGroup4.Text = "Роли";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.splitContainer1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(897, 342);
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // colSystemRole
            // 
            this.colSystemRole.Caption = "Системное имя";
            this.colSystemRole.FieldName = "SystemRole";
            this.colSystemRole.Name = "colSystemRole";
            this.colSystemRole.Visible = true;
            this.colSystemRole.VisibleIndex = 0;
            // 
            // colRole
            // 
            this.colRole.Caption = "Роль";
            this.colRole.FieldName = "Role.Description";
            this.colRole.Name = "colRole";
            this.colRole.Visible = true;
            this.colRole.VisibleIndex = 1;
            // 
            // userRolesBarManager
            // 
            this.userRolesBarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar5});
            this.userRolesBarManager.DockControls.Add(this.barDockControl1);
            this.userRolesBarManager.DockControls.Add(this.barDockControl2);
            this.userRolesBarManager.DockControls.Add(this.barDockControl3);
            this.userRolesBarManager.DockControls.Add(this.barDockControl4);
            this.userRolesBarManager.Form = this.splitContainer1.Panel1;
            this.userRolesBarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiDeleteSelectedUserRoles});
            this.userRolesBarManager.MainMenu = this.bar5;
            this.userRolesBarManager.MaxItemId = 1;
            // 
            // bar5
            // 
            this.bar5.BarName = "Main menu";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteSelectedUserRoles)});
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.MultiLine = true;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Main menu";
            // 
            // allRolesBarManager
            // 
            this.allRolesBarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.allRolesBarManager.DockControls.Add(this.barDockControlTop);
            this.allRolesBarManager.DockControls.Add(this.barDockControlBottom);
            this.allRolesBarManager.DockControls.Add(this.barDockControlLeft);
            this.allRolesBarManager.DockControls.Add(this.barDockControlRight);
            this.allRolesBarManager.Form = this.splitContainer1.Panel2;
            this.allRolesBarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddSelectedRoles});
            this.allRolesBarManager.MainMenu = this.bar2;
            this.allRolesBarManager.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddSelectedRoles)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // LoginView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "LoginView";
            this.Size = new System.Drawing.Size(941, 709);
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userRolesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRolesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personLookUp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personBindingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personTypeRadioGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserLoginTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userRolesBarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allRolesBarManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource loginBindingSource;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit UserLoginTextEdit;
        private DevExpress.XtraEditors.TextEdit PasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserLogin;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPassword;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpAcademicDegrees;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgClose;
        private System.Windows.Forms.BindingSource userRoleBindingSource;
        private DevExpress.XtraEditors.LookUpEdit personLookUp;
        private DevExpress.XtraEditors.RadioGroup personTypeRadioGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource personBindingBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.BindingSource rolesBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.GridControl userRolesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView userRolesGridView;
        private DevExpress.XtraGrid.GridControl allRolesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView allRolesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemName;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemRole;
        private DevExpress.XtraGrid.Columns.GridColumn colRole;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager userRolesBarManager;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteSelectedUserRoles;
        private DevExpress.XtraGrid.Columns.GridColumn colSystemRole1;
        private DevExpress.XtraGrid.Columns.GridColumn colRole1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager allRolesBarManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddSelectedRoles;
    }
}
