﻿using System.Linq;
using DevExpress.Data;
using GSC.Client.Util;
using GSC.Client.ViewModels.Logins;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Admin.Logins
{
    [UsedImplicitly]
    public partial class LoginView : BaseEditView
    {
        public LoginView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();

                mvvmContext1.RegisterService(LoginViewModel.UserRolesGridServiceKey,
                    new UpdateGridService(userRolesGridControl));
                mvvmContext1.RegisterService(LoginViewModel.AllRolesGridServiceKey,
                    new UpdateGridService(allRolesGridControl));
            }}

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<LoginViewModel>();
            fluent.SetObjectDataSourceBinding(loginBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
 
            fluent.SetBinding(personTypeRadioGroup, rg => rg.SelectedIndex, vm => vm.SelectedPersonType, pt => (int)pt - 1, i => (PersonType)(i + 1));
            fluent.SetBinding(personBindingBindingSource, bs => bs.DataSource, vm => vm.LookupPerson.Entities);
            fluent.SetBinding(personLookUp, lu => lu.EditValue, vm => vm.PersonId);
            fluent.SetBinding(rolesBindingSource, bs => bs.DataSource, vm => vm.LookupRoles.Entities);
            fluent.SetBinding(userRoleBindingSource, bs => bs.DataSource, vm => vm.UserRoles);

            fluent.WithEvent<SelectionChangedEventArgs>(userRolesGridView, "SelectionChanged")
                .SetBinding(vm => vm.SelectedUserRoles,
                    ea => userRolesGridView.GetSelectedRows().Select(userRolesGridView.GetRow).Cast<UserRole>().ToArray());

            fluent.WithEvent<SelectionChangedEventArgs>(allRolesGridView, "SelectionChanged")
                .SetBinding(vm => vm.SelectedRoles,
                    ea => allRolesGridView.GetSelectedRows().Select(allRolesGridView.GetRow).Cast<Role>().ToArray());
        }

        private void loginBindingSource_DataSourceChanged(object sender, System.EventArgs e)
        {
            PostInitBindings();
        }

        private void PostInitBindings()
        {
            var fluent = mvvmContext1.OfType<LoginViewModel>();
            fluent.SetBinding(UserLoginTextEdit, te => te.ReadOnly, vm => vm.IsEditMode);
        }
    }
}
