﻿using DevExpress.XtraEditors;
using GSC.Client.Util;
using GSC.Client.ViewModels.Project;

namespace GSC.Client.Modules.Admin.Project
{
    public partial class QueryProjectView : XtraUserControl
    {
        public QueryProjectView()
        {
            InitializeComponent();
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<QueryProjectViewModel>();
            var vmObj = mvvmContext1.GetViewModel<QueryProjectViewModel>();
            fluent.InitGridViewReadonly(gridView1, vmObj);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
        }

        private void mvvmContext1_ViewModelSet(object sender, DevExpress.Utils.MVVM.ViewModelSetEventArgs e)
        {
            InitBindings();
        }
    }
}
