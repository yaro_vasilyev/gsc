﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraEditors;
using GSC.Client.Util;
using GSC.Client.ViewModels.Project;

namespace GSC.Client.Modules.Admin.Project
{
    public partial class ProjectView : BaseEditView
    {
        public ProjectView()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                var fluent = mvvmContext1.OfType<ProjectViewModel>();
                fluent.SetObjectDataSourceBinding(projectBindingSource, vm => vm.Entity, vm => vm.Update());
                fluent.SetBinding(allProjectsBindingSource, bs => bs.DataSource, vm => vm.LookupProjects.Entities);
                fluent.SetBinding(checkEdit1, ch => ch.Checked, vm => vm.CopyData);
                fluent.SetBinding(lookupProjects, lo => lo.ReadOnly, vm => vm.CopyData, ch => !ch);
            }
        }

        private void projectBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                Debug.Assert(projectBindingSource.DataSource != null);
                InitBinding();
            }
        }

        private void InitBinding()
        {
            var fluent = mvvmContext1.OfType<ProjectViewModel>();
            fluent.SetBinding(lookupProjects, e => e.EditValue, vm => vm.ProjectId, null, null);
        }
    }
}
