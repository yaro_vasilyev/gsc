﻿using System;
using GSC.Client.Util;
using GSC.Client.ViewModels.Project;

namespace GSC.Client.Modules.Admin.Project
{
    public partial class ProjectCollectionView : CollectionBlockView
    {
        public ProjectCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var vm = mvvmContext1.GetViewModel<ProjectCollectionViewModel>();

            var fluent = mvvmContext1.OfType<ProjectCollectionViewModel>();
            fluent.InitGridView(gridView1, vm);

            fluent.SetBinding(gridControl1, c => c.DataSource, x => x.Entities);
        }
    }
}
