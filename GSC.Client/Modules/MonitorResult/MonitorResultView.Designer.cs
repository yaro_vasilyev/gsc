﻿namespace GSC.Client.Modules.MonitorResult
{
    partial class MonitorResultView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOfficial = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSocial = new DevExpress.XtraBars.BarCheckItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgFilters = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiAddSelectedExecs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemoveSelectedDetails = new DevExpress.XtraBars.BarButtonItem();
            this.monitorResultBindingSource = new System.Windows.Forms.BindingSource();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.radioObjectType = new DevExpress.XtraEditors.RadioGroup();
            this.lookupScheduleDetails = new DevExpress.XtraEditors.LookUpEdit();
            this.scheduleDetailsBindingSource = new System.Windows.Forms.BindingSource();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridExecs = new DevExpress.XtraGrid.GridControl();
            this.execsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewExecs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExamSubjectName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomVideo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.gridDetails = new DevExpress.XtraGrid.GridControl();
            this.detailsBindingSource = new System.Windows.Forms.BindingSource();
            this.gridViewDetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExecSubjectName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountPart2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoom2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomVideo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.lookupViolators = new DevExpress.XtraEditors.LookUpEdit();
            this.violatorsBindingSource = new System.Windows.Forms.BindingSource();
            this.lookupSchedules = new DevExpress.XtraEditors.LookUpEdit();
            this.schedulesBindingSource = new System.Windows.Forms.BindingSource();
            this.lookupViolations = new DevExpress.XtraEditors.LookUpEdit();
            this.violationsBindingSource = new System.Windows.Forms.BindingSource();
            this.lookupRegions = new DevExpress.XtraEditors.LookUpEdit();
            this.regionsBindingSource = new System.Windows.Forms.BindingSource();
            this.comboExamType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.comboPeriodType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.available_barManager = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.detail_barManager = new DevExpress.XtraBars.BarManager();
            this.bar5 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorResultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioObjectType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupScheduleDetails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridExecs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.execsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExecs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupViolators.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.violatorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSchedules.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupViolations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.violationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupRegions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboExamType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.available_barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose,
            this.bbiOfficial,
            this.bbiSocial});
            this.ribbonControl1.MaxItemId = 12;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(693, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "Close", this.bbiClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "AddSelectedExecs", this.bbiAddSelectedExecs),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "RemoveSelectedDetails", this.bbiRemoveSelectedDetails),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel), "SaveAndNewOverride", this.bbiSaveAndNew)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить из закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Созранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // bbiOfficial
            // 
            this.bbiOfficial.Caption = "Эксперт-член ГЭК";
            this.bbiOfficial.Id = 10;
            this.bbiOfficial.Name = "bbiOfficial";
            // 
            // bbiSocial
            // 
            this.bbiSocial.Caption = "Общественный наблюдатель";
            this.bbiSocial.Id = 11;
            this.bbiSocial.Name = "bbiSocial";
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel,
            this.rpgFilters});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // rpgFilters
            // 
            this.rpgFilters.ItemLinks.Add(this.bbiOfficial, false, "", "", true);
            this.rpgFilters.ItemLinks.Add(this.bbiSocial, false, "", "", true);
            this.rpgFilters.Name = "rpgFilters";
            this.rpgFilters.Text = "Фильтр";
            // 
            // bbiAddSelectedExecs
            // 
            this.bbiAddSelectedExecs.Caption = "Добавить выбранное";
            this.bbiAddSelectedExecs.Id = 0;
            this.bbiAddSelectedExecs.ImageUri.Uri = "Add";
            this.bbiAddSelectedExecs.Name = "bbiAddSelectedExecs";
            this.bbiAddSelectedExecs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiRemoveSelectedDetails
            // 
            this.bbiRemoveSelectedDetails.Caption = "Удалить выбранное";
            this.bbiRemoveSelectedDetails.Id = 0;
            this.bbiRemoveSelectedDetails.ImageUri.Uri = "Delete";
            this.bbiRemoveSelectedDetails.Name = "bbiRemoveSelectedDetails";
            this.bbiRemoveSelectedDetails.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // monitorResultBindingSource
            // 
            this.monitorResultBindingSource.DataSource = typeof(GSC.Model.MonitorResult);
            this.monitorResultBindingSource.DataSourceChanged += new System.EventHandler(this.monitorResultBindingSource_DataSourceChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.radioObjectType);
            this.layoutControl1.Controls.Add(this.lookupScheduleDetails);
            this.layoutControl1.Controls.Add(this.memoNote);
            this.layoutControl1.Controls.Add(this.splitContainerControl1);
            this.layoutControl1.Controls.Add(this.dateEdit);
            this.layoutControl1.Controls.Add(this.lookupViolators);
            this.layoutControl1.Controls.Add(this.lookupSchedules);
            this.layoutControl1.Controls.Add(this.lookupViolations);
            this.layoutControl1.Controls.Add(this.lookupRegions);
            this.layoutControl1.Controls.Add(this.comboExamType);
            this.layoutControl1.Controls.Add(this.comboPeriodType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 141);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(967, 142, 347, 758);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(693, 466);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // radioObjectType
            // 
            this.radioObjectType.Location = new System.Drawing.Point(12, 108);
            this.radioObjectType.MenuManager = this.ribbonControl1;
            this.radioObjectType.Name = "radioObjectType";
            this.radioObjectType.Properties.Columns = 4;
            this.radioObjectType.Size = new System.Drawing.Size(327, 22);
            this.radioObjectType.StyleController = this.layoutControl1;
            this.radioObjectType.TabIndex = 12;
            // 
            // lookupScheduleDetails
            // 
            this.lookupScheduleDetails.Location = new System.Drawing.Point(423, 110);
            this.lookupScheduleDetails.MenuManager = this.ribbonControl1;
            this.lookupScheduleDetails.Name = "lookupScheduleDetails";
            this.lookupScheduleDetails.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupScheduleDetails.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExamDate", "Дата", 62, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StationDisplayText", "ППЭ", 119, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookupScheduleDetails.Properties.DataSource = this.scheduleDetailsBindingSource;
            this.lookupScheduleDetails.Properties.DisplayMember = "StationDisplayText";
            this.lookupScheduleDetails.Properties.ValueMember = "Id";
            this.lookupScheduleDetails.Size = new System.Drawing.Size(258, 20);
            this.lookupScheduleDetails.StyleController = this.layoutControl1;
            this.lookupScheduleDetails.TabIndex = 11;
            // 
            // scheduleDetailsBindingSource
            // 
            this.scheduleDetailsBindingSource.DataSource = typeof(GSC.Model.ScheduleDetail);
            // 
            // memoNote
            // 
            this.memoNote.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.monitorResultBindingSource, "Note", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.memoNote.Location = new System.Drawing.Point(12, 394);
            this.memoNote.MenuManager = this.ribbonControl1;
            this.memoNote.Name = "memoNote";
            this.memoNote.Size = new System.Drawing.Size(669, 60);
            this.memoNote.StyleController = this.layoutControl1;
            this.memoNote.TabIndex = 10;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Location = new System.Drawing.Point(12, 134);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridExecs);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlLeft);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlRight);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlBottom);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlTop);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridDetails);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControl3);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControl4);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(669, 240);
            this.splitContainerControl1.SplitterPosition = 332;
            this.splitContainerControl1.TabIndex = 9;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.Resize += new System.EventHandler(this.OnSplitterResize);
            // 
            // gridExecs
            // 
            this.gridExecs.DataSource = this.execsBindingSource;
            this.gridExecs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridExecs.Location = new System.Drawing.Point(0, 24);
            this.gridExecs.MainView = this.gridViewExecs;
            this.gridExecs.MenuManager = this.ribbonControl1;
            this.gridExecs.Name = "gridExecs";
            this.gridExecs.Size = new System.Drawing.Size(332, 216);
            this.gridExecs.TabIndex = 7;
            this.gridExecs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExecs});
            // 
            // execsBindingSource
            // 
            this.execsBindingSource.DataSource = typeof(GSC.Model.Exec);
            // 
            // gridViewExecs
            // 
            this.gridViewExecs.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExamSubjectName,
            this.colCountPart,
            this.colCountRoom,
            this.colCountRoomVideo});
            this.gridViewExecs.GridControl = this.gridExecs;
            this.gridViewExecs.Name = "gridViewExecs";
            this.gridViewExecs.OptionsBehavior.ReadOnly = true;
            this.gridViewExecs.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridViewExecs.OptionsSelection.MultiSelect = true;
            this.gridViewExecs.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewExecs.OptionsView.ShowGroupPanel = false;
            // 
            // colExamSubjectName
            // 
            this.colExamSubjectName.Caption = "Предмет";
            this.colExamSubjectName.FieldName = "Exam.SubjectName";
            this.colExamSubjectName.Name = "colExamSubjectName";
            this.colExamSubjectName.OptionsColumn.AllowEdit = false;
            this.colExamSubjectName.Visible = true;
            this.colExamSubjectName.VisibleIndex = 1;
            this.colExamSubjectName.Width = 72;
            // 
            // colCountPart
            // 
            this.colCountPart.Caption = "Участников";
            this.colCountPart.FieldName = "CountPart";
            this.colCountPart.Name = "colCountPart";
            this.colCountPart.OptionsColumn.AllowEdit = false;
            this.colCountPart.Visible = true;
            this.colCountPart.VisibleIndex = 2;
            this.colCountPart.Width = 50;
            // 
            // colCountRoom
            // 
            this.colCountRoom.Caption = "Аудиторий";
            this.colCountRoom.FieldName = "CountRoom";
            this.colCountRoom.Name = "colCountRoom";
            this.colCountRoom.OptionsColumn.AllowEdit = false;
            this.colCountRoom.Visible = true;
            this.colCountRoom.VisibleIndex = 3;
            this.colCountRoom.Width = 50;
            // 
            // colCountRoomVideo
            // 
            this.colCountRoomVideo.Caption = "Аудиторий с видеонаблюдением";
            this.colCountRoomVideo.FieldName = "CountRoomVideo";
            this.colCountRoomVideo.Name = "colCountRoomVideo";
            this.colCountRoomVideo.OptionsColumn.AllowEdit = false;
            this.colCountRoomVideo.Visible = true;
            this.colCountRoomVideo.VisibleIndex = 4;
            this.colCountRoomVideo.Width = 50;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 216);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(332, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 216);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 240);
            this.barDockControlBottom.Size = new System.Drawing.Size(332, 0);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(332, 24);
            // 
            // gridDetails
            // 
            this.gridDetails.DataSource = this.detailsBindingSource;
            this.gridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDetails.Location = new System.Drawing.Point(0, 24);
            this.gridDetails.MainView = this.gridViewDetails;
            this.gridDetails.MenuManager = this.ribbonControl1;
            this.gridDetails.Name = "gridDetails";
            this.gridDetails.Size = new System.Drawing.Size(332, 216);
            this.gridDetails.TabIndex = 11;
            this.gridDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetails});
            // 
            // detailsBindingSource
            // 
            this.detailsBindingSource.DataSource = typeof(GSC.Model.MrDetail);
            // 
            // gridViewDetails
            // 
            this.gridViewDetails.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExecSubjectName,
            this.colCountPart2,
            this.colCountRoom2,
            this.colCountRoomVideo2});
            this.gridViewDetails.GridControl = this.gridDetails;
            this.gridViewDetails.Name = "gridViewDetails";
            this.gridViewDetails.OptionsBehavior.ReadOnly = true;
            this.gridViewDetails.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridViewDetails.OptionsSelection.MultiSelect = true;
            this.gridViewDetails.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewDetails.OptionsView.ShowGroupPanel = false;
            // 
            // colExecSubjectName
            // 
            this.colExecSubjectName.Caption = "Предмет";
            this.colExecSubjectName.FieldName = "Exec.Exam.SubjectName";
            this.colExecSubjectName.Name = "colExecSubjectName";
            this.colExecSubjectName.OptionsColumn.AllowEdit = false;
            this.colExecSubjectName.Visible = true;
            this.colExecSubjectName.VisibleIndex = 1;
            this.colExecSubjectName.Width = 72;
            // 
            // colCountPart2
            // 
            this.colCountPart2.Caption = "Участников";
            this.colCountPart2.FieldName = "Exec.CountPart";
            this.colCountPart2.Name = "colCountPart2";
            this.colCountPart2.OptionsColumn.AllowEdit = false;
            this.colCountPart2.Visible = true;
            this.colCountPart2.VisibleIndex = 2;
            this.colCountPart2.Width = 50;
            // 
            // colCountRoom2
            // 
            this.colCountRoom2.Caption = "Аудиторий";
            this.colCountRoom2.FieldName = "Exec.CountRoom";
            this.colCountRoom2.Name = "colCountRoom2";
            this.colCountRoom2.OptionsColumn.AllowEdit = false;
            this.colCountRoom2.Visible = true;
            this.colCountRoom2.VisibleIndex = 3;
            this.colCountRoom2.Width = 48;
            // 
            // colCountRoomVideo2
            // 
            this.colCountRoomVideo2.Caption = "Аудиторий с видеонаблюдением";
            this.colCountRoomVideo2.FieldName = "Exec.CountRoomVideo";
            this.colCountRoomVideo2.Name = "colCountRoomVideo2";
            this.colCountRoomVideo2.OptionsColumn.AllowEdit = false;
            this.colCountRoomVideo2.Visible = true;
            this.colCountRoomVideo2.VisibleIndex = 4;
            this.colCountRoomVideo2.Width = 50;
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 24);
            this.barDockControl3.Size = new System.Drawing.Size(0, 216);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(332, 24);
            this.barDockControl4.Size = new System.Drawing.Size(0, 216);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 240);
            this.barDockControl2.Size = new System.Drawing.Size(332, 0);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(332, 24);
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(82, 84);
            this.dateEdit.MenuManager = this.ribbonControl1;
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.NullValuePrompt = "<Не выбрано>";
            this.dateEdit.Size = new System.Drawing.Size(257, 20);
            this.dateEdit.StyleController = this.layoutControl1;
            this.dateEdit.TabIndex = 7;
            this.dateEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.dateEdit1_QueryPopUp);
            // 
            // lookupViolators
            // 
            this.lookupViolators.Location = new System.Drawing.Point(423, 60);
            this.lookupViolators.MenuManager = this.ribbonControl1;
            this.lookupViolators.Name = "lookupViolators";
            this.lookupViolators.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupViolators.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupViolators.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Нарушитель", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookupViolators.Properties.DataSource = this.violatorsBindingSource;
            this.lookupViolators.Properties.DisplayMember = "Name";
            this.lookupViolators.Properties.NullText = "";
            this.lookupViolators.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupViolators.Properties.ValueMember = "Id";
            this.lookupViolators.Size = new System.Drawing.Size(258, 20);
            this.lookupViolators.StyleController = this.layoutControl1;
            this.lookupViolators.TabIndex = 6;
            // 
            // violatorsBindingSource
            // 
            this.violatorsBindingSource.DataSource = typeof(GSC.Model.Violator);
            // 
            // lookupSchedules
            // 
            this.lookupSchedules.Location = new System.Drawing.Point(82, 60);
            this.lookupSchedules.MenuManager = this.ribbonControl1;
            this.lookupSchedules.Name = "lookupSchedules";
            this.lookupSchedules.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupSchedules.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupSchedules.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExpertFullName", "ФИО эксперта", 91, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MonitorRcoi", "Мониторинг РЦОИ", 69, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lookupSchedules.Properties.DataSource = this.schedulesBindingSource;
            this.lookupSchedules.Properties.DisplayMember = "ExpertFullName";
            this.lookupSchedules.Properties.NullText = "";
            this.lookupSchedules.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupSchedules.Properties.ValueMember = "Id";
            this.lookupSchedules.Size = new System.Drawing.Size(257, 20);
            this.lookupSchedules.StyleController = this.layoutControl1;
            this.lookupSchedules.TabIndex = 5;
            // 
            // schedulesBindingSource
            // 
            this.schedulesBindingSource.DataSource = typeof(GSC.Model.Schedule);
            // 
            // lookupViolations
            // 
            this.lookupViolations.Location = new System.Drawing.Point(423, 36);
            this.lookupViolations.MenuManager = this.ribbonControl1;
            this.lookupViolations.Name = "lookupViolations";
            this.lookupViolations.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupViolations.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupViolations.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Нарушение", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookupViolations.Properties.DataSource = this.violationsBindingSource;
            this.lookupViolations.Properties.DisplayMember = "Name";
            this.lookupViolations.Properties.NullText = "";
            this.lookupViolations.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupViolations.Properties.ValueMember = "Id";
            this.lookupViolations.Size = new System.Drawing.Size(258, 20);
            this.lookupViolations.StyleController = this.layoutControl1;
            this.lookupViolations.TabIndex = 4;
            // 
            // violationsBindingSource
            // 
            this.violationsBindingSource.DataSource = typeof(GSC.Model.Violation);
            // 
            // lookupRegions
            // 
            this.lookupRegions.Location = new System.Drawing.Point(82, 36);
            this.lookupRegions.MenuManager = this.ribbonControl1;
            this.lookupRegions.Name = "lookupRegions";
            this.lookupRegions.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookupRegions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupRegions.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookupRegions.Properties.DataSource = this.regionsBindingSource;
            this.lookupRegions.Properties.DisplayMember = "Name";
            this.lookupRegions.Properties.NullText = "";
            this.lookupRegions.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupRegions.Properties.ValueMember = "Id";
            this.lookupRegions.Size = new System.Drawing.Size(257, 20);
            this.lookupRegions.StyleController = this.layoutControl1;
            this.lookupRegions.TabIndex = 3;
            // 
            // regionsBindingSource
            // 
            this.regionsBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // comboExamType
            // 
            this.comboExamType.Location = new System.Drawing.Point(423, 12);
            this.comboExamType.MenuManager = this.ribbonControl1;
            this.comboExamType.Name = "comboExamType";
            this.comboExamType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.comboExamType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboExamType.Properties.NullValuePrompt = "<Не выбрано>";
            this.comboExamType.Size = new System.Drawing.Size(258, 20);
            this.comboExamType.StyleController = this.layoutControl1;
            this.comboExamType.TabIndex = 2;
            // 
            // comboPeriodType
            // 
            this.comboPeriodType.Location = new System.Drawing.Point(82, 12);
            this.comboPeriodType.MenuManager = this.ribbonControl1;
            this.comboPeriodType.Name = "comboPeriodType";
            this.comboPeriodType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.comboPeriodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPeriodType.Properties.NullValuePrompt = "<Не выбрано>";
            this.comboPeriodType.Size = new System.Drawing.Size(257, 20);
            this.comboPeriodType.StyleController = this.layoutControl1;
            this.comboPeriodType.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem15,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem10});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Absolute;
            columnDefinition2.Width = 10D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 50D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3});
            rowDefinition1.Height = 24D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 24D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 24D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition4.Height = 24D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition5.Height = 26D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition6.Height = 100D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 80D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Absolute;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7});
            this.layoutControlGroup1.Size = new System.Drawing.Size(693, 466);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.comboPeriodType;
            this.layoutControlItem1.CustomizationFormText = "Этап";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(331, 24);
            this.layoutControlItem1.Text = "Этап:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.comboExamType;
            this.layoutControlItem2.CustomizationFormText = "Форма ГИА";
            this.layoutControlItem2.Location = new System.Drawing.Point(341, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(332, 24);
            this.layoutControlItem2.Text = "Форма ГИА:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lookupRegions;
            this.layoutControlItem3.CustomizationFormText = "Субъект РФ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(331, 24);
            this.layoutControlItem3.Text = "Субъект РФ:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lookupViolations;
            this.layoutControlItem4.CustomizationFormText = "Нарушение";
            this.layoutControlItem4.Location = new System.Drawing.Point(341, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(332, 24);
            this.layoutControlItem4.Text = "Нарушение:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lookupSchedules;
            this.layoutControlItem5.CustomizationFormText = "Эксперт";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem5.Size = new System.Drawing.Size(331, 24);
            this.layoutControlItem5.Text = "Эксперт:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lookupViolators;
            this.layoutControlItem6.CustomizationFormText = "Нарушитель";
            this.layoutControlItem6.Location = new System.Drawing.Point(341, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem6.Size = new System.Drawing.Size(332, 24);
            this.layoutControlItem6.Text = "Нарушитель:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.dateEdit;
            this.layoutControlItem7.CustomizationFormText = "Дата";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem7.Size = new System.Drawing.Size(331, 24);
            this.layoutControlItem7.Text = "Дата:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.splitContainerControl1;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem15.Size = new System.Drawing.Size(673, 244);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.memoNote;
            this.layoutControlItem9.CustomizationFormText = "Примечание";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 366);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 6;
            this.layoutControlItem9.Size = new System.Drawing.Size(673, 80);
            this.layoutControlItem9.Text = "Примечание:";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lookupScheduleDetails;
            this.layoutControlItem8.CustomizationFormText = "ППЭ";
            this.layoutControlItem8.Location = new System.Drawing.Point(341, 96);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(332, 26);
            this.layoutControlItem8.Text = "ППЭ:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.radioObjectType;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem10.Size = new System.Drawing.Size(331, 26);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // available_barManager
            // 
            this.available_barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.available_barManager.DockControls.Add(this.barDockControlTop);
            this.available_barManager.DockControls.Add(this.barDockControlBottom);
            this.available_barManager.DockControls.Add(this.barDockControlLeft);
            this.available_barManager.DockControls.Add(this.barDockControlRight);
            this.available_barManager.Form = this.splitContainerControl1.Panel1;
            this.available_barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddSelectedExecs});
            this.available_barManager.MainMenu = this.bar2;
            this.available_barManager.MaxItemId = 1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddSelectedExecs)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // detail_barManager
            // 
            this.detail_barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar5});
            this.detail_barManager.DockControls.Add(this.barDockControl1);
            this.detail_barManager.DockControls.Add(this.barDockControl2);
            this.detail_barManager.DockControls.Add(this.barDockControl3);
            this.detail_barManager.DockControls.Add(this.barDockControl4);
            this.detail_barManager.Form = this.splitContainerControl1.Panel2;
            this.detail_barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRemoveSelectedDetails});
            this.detail_barManager.MainMenu = this.bar5;
            this.detail_barManager.MaxItemId = 1;
            // 
            // bar5
            // 
            this.bar5.BarName = "Main menu";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemoveSelectedDetails)});
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.MultiLine = true;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Main menu";
            // 
            // MonitorResultView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "MonitorResultView";
            this.Size = new System.Drawing.Size(693, 607);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorResultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioObjectType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupScheduleDetails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridExecs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.execsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExecs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupViolators.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.violatorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSchedules.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupViolations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.violationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupRegions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboExamType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.available_barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_barManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private System.Windows.Forms.BindingSource monitorResultBindingSource;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LookUpEdit lookupViolators;
        private DevExpress.XtraEditors.LookUpEdit lookupSchedules;
        private DevExpress.XtraEditors.LookUpEdit lookupViolations;
        private DevExpress.XtraEditors.LookUpEdit lookupRegions;
        private DevExpress.XtraEditors.ImageComboBoxEdit comboExamType;
        private DevExpress.XtraEditors.ImageComboBoxEdit comboPeriodType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager available_barManager;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager detail_barManager;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiAddSelectedExecs;
        private DevExpress.XtraBars.BarButtonItem bbiRemoveSelectedDetails;
        private DevExpress.XtraGrid.GridControl gridExecs;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExecs;
        private DevExpress.XtraGrid.GridControl gridDetails;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetails;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgFilters;
        private DevExpress.XtraBars.BarCheckItem bbiOfficial;
        private DevExpress.XtraBars.BarCheckItem bbiSocial;
        private System.Windows.Forms.BindingSource schedulesBindingSource;
        private System.Windows.Forms.BindingSource regionsBindingSource;
        private System.Windows.Forms.BindingSource violationsBindingSource;
        private System.Windows.Forms.BindingSource violatorsBindingSource;
        private System.Windows.Forms.BindingSource scheduleDetailsBindingSource;
        private System.Windows.Forms.BindingSource execsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCountPart;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoom;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomVideo;
        private DevExpress.XtraGrid.Columns.GridColumn colExamSubjectName;
        private System.Windows.Forms.BindingSource detailsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colExecSubjectName;
        private DevExpress.XtraGrid.Columns.GridColumn colCountPart2;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoom2;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomVideo2;
        private DevExpress.XtraEditors.RadioGroup radioObjectType;
        private DevExpress.XtraEditors.LookUpEdit lookupScheduleDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
