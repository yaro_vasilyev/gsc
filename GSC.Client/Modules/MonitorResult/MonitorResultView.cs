﻿using System;
using System.Diagnostics;
using System.Linq;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using GSC.Client.Util;
using GSC.Client.ViewModels.MonitorResult;

namespace GSC.Client.Modules.MonitorResult
{
    public partial class MonitorResultView : BaseEditView
    {
        public MonitorResultView()
        {
            InitializeComponent();

            radioObjectType.Properties.Items.AddEnum<Model.VisitObjectType>();
            comboPeriodType.Properties.Items.AddEnum<Model.PeriodType>();
            comboExamType.Properties.Items.AddEnum<Model.ExamType>();


            if (!DesignMode)
            {
                mvvmContext1.RegisterService(MonitorResultViewModel.AvailableExecsGridServiceKey, new UpdateGridService(gridExecs));
                mvvmContext1.RegisterService(MonitorResultViewModel.MrDetailsGridServiceKey,
                    new UpdateGridService(gridDetails));


                var fluent = mvvmContext1.OfType<MonitorResultViewModel>();
                fluent.SetObjectDataSourceBinding(monitorResultBindingSource, vm => vm.Entity, vm => vm.Update());
                fluent.SetBinding(schedulesBindingSource, bs => bs.DataSource, vm => vm.SchedulesLookup.Entities);
                fluent.SetBinding(regionsBindingSource, bs => bs.DataSource, vm => vm.RegionsLookup.Entities);
                fluent.SetBinding(violationsBindingSource, bs => bs.DataSource, vm => vm.ViolationsLookup.Entities);
                fluent.SetBinding(violatorsBindingSource, bs => bs.DataSource, vm => vm.ViolatorsLookup.Entities);
                fluent.SetBinding(scheduleDetailsBindingSource, bs => bs.DataSource, vm => vm.ScheduleDetailsLookup.Entities);
                fluent.SetBinding(execsBindingSource, bs => bs.DataSource, vm => vm.Execs);
                fluent.SetBinding(detailsBindingSource, bs => bs.DataSource, vm => vm.Details);
                fluent.SetBinding(bbiOfficial, x => x.Checked, vm => vm.ExpertKindOfficial);
                fluent.SetBinding(bbiSocial, x => x.Checked, vm => vm.ExpertKindSocial);
                fluent.SetBinding(comboExamType, x => x.ReadOnly, vm => vm.ExamTypeReadOnly);
            }

        }

        private void OnSplitterResize(object sender, EventArgs e)
        {
            var split = sender as SplitContainerControl;
            Debug.Assert(split != null);

            split.SplitterPosition = split.Width / 2;
        }

        private void monitorResultBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            Debug.Assert(monitorResultBindingSource.DataSource != null);
            InitBindings();
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<MonitorResultViewModel>();
            fluent.SetBinding(lookupSchedules, e => e.EditValue, vm => vm.ScheduleId, null, XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(lookupRegions, e => e.EditValue, vm => vm.RegionId, null, XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(lookupViolations, e => e.EditValue, vm => vm.ViolationId, null, XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(lookupViolators, e => e.EditValue, vm => vm.ViolatorId, null, XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(lookupScheduleDetails, e => e.EditValue, vm => vm.ScheduleDetailId, null, XtraEditors.ConvertBack<int?>);

            fluent.SetBinding(radioObjectType,
                              g => g.EditValue,
                              vm => vm.ObjectType,
                              null,
                              XtraEditors.ConvertBack<Model.VisitObjectType?>);
            fluent.SetBinding(lookupScheduleDetails, e => e.ReadOnly, vm => vm.DetailsReadOnly);

            fluent.SetBinding(comboPeriodType, x => x.EditValue, vm => vm.PeriodType, null, XtraEditors.ConvertBack<Model.PeriodType?>);
            fluent.SetBinding(dateEdit, e => e.EditValue, vm => vm.Date,
                time => time,
                @object => @object as DateTime?);

            fluent.SetBinding(comboExamType, x => x.EditValue, vm => vm.ExamType, null, XtraEditors.ConvertBack<Model.ExamType?>);

            fluent.BindCommand(bbiAddSelectedExecs, vm => vm.AddSelectedExecs());
            fluent.BindCommand(bbiRemoveSelectedDetails, vm => vm.RemoveSelectedDetails());

            fluent.WithEvent<SelectionChangedEventArgs>(gridViewExecs, "SelectionChanged")
                .SetBinding(vm => vm.SelectedExecs,
                    ea => gridViewExecs.GetSelectedRows().Select(gridViewExecs.GetRow).Cast<Model.Exec>().ToArray());
            fluent.WithEvent<SelectionChangedEventArgs>(gridViewDetails, "SelectionChanged")
                .SetBinding(vm => vm.SelectedDetails,
                    ea =>
                        gridViewDetails.GetSelectedRows()
                            .Select(gridViewDetails.GetRow)
                            .Cast<Model.MrDetail>()
                            .ToArray());

        }

        private MonitorResultViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<MonitorResultViewModel>(); }
        }

        private void dateEdit1_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ViewModel.Dates.Count == 0)
            {
                dateEdit.Properties.MinValue = DateTime.MinValue;
                dateEdit.Properties.MaxValue = DateTime.MaxValue;
                e.Cancel = true;
                return;
            }

            dateEdit.Properties.MinValue = ViewModel.Dates[0].Date;
            dateEdit.Properties.MaxValue = ViewModel.Dates[ViewModel.Dates.Count - 1].Date.AddDays(1).AddTicks(-1);

            dateEdit.Properties.DisabledDateProvider = new CalendarDisabledDateProvider(ViewModel.Dates);
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            layoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
