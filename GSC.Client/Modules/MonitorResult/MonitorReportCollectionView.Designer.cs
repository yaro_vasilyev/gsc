﻿namespace GSC.Client.Modules.MonitorResult
{
    partial class MonitorReportCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorReportCollectionView));
            this.imageCollectionSmall = new DevExpress.Utils.ImageCollection(this.components);
            this.bbiUploadFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDownloadFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPreviewFile = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.rpMonitorReports = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlMaster = new DevExpress.XtraGrid.GridControl();
            this.masterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewMaster = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpertFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonitorRcoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsLoaded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlDetails = new DevExpress.XtraGrid.GridControl();
            this.detailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFilename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlobSize = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Images = this.imageCollectionSmall;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiUploadFile,
            this.bbiDownloadFile,
            this.bbiDeleteFile,
            this.bbiRefresh,
            this.bbiPreviewFile});
            this.ribbonControl1.LargeImages = this.imageCollectionLarge;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 7;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMonitorReports});
            this.ribbonControl1.Size = new System.Drawing.Size(768, 141);
            // 
            // imageCollectionSmall
            // 
            this.imageCollectionSmall.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionSmall.ImageStream")));
            this.imageCollectionSmall.InsertGalleryImage("clear_16x16.png", "office2013/actions/clear_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/actions/clear_16x16.png"), 0);
            this.imageCollectionSmall.Images.SetKeyName(0, "clear_16x16.png");
            this.imageCollectionSmall.InsertGalleryImage("download_16x16.png", "office2013/actions/download_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/actions/download_16x16.png"), 1);
            this.imageCollectionSmall.Images.SetKeyName(1, "download_16x16.png");
            this.imageCollectionSmall.InsertGalleryImage("delete_16x16.png", "office2013/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/edit/delete_16x16.png"), 2);
            this.imageCollectionSmall.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollectionSmall.InsertGalleryImage("publish_16x16.png", "office2013/miscellaneous/publish_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/miscellaneous/publish_16x16.png"), 3);
            this.imageCollectionSmall.Images.SetKeyName(3, "publish_16x16.png");
            // 
            // bbiUploadFile
            // 
            this.bbiUploadFile.Caption = "Загрузить";
            this.bbiUploadFile.Id = 1;
            this.bbiUploadFile.ImageIndex = 3;
            this.bbiUploadFile.ImageUri.Uri = "UploadFile";
            this.bbiUploadFile.LargeImageIndex = 3;
            this.bbiUploadFile.Name = "bbiUploadFile";
            // 
            // bbiDownloadFile
            // 
            this.bbiDownloadFile.Caption = "Выгрузить";
            this.bbiDownloadFile.Id = 2;
            this.bbiDownloadFile.ImageIndex = 1;
            this.bbiDownloadFile.ImageUri.Uri = "DownloadFile";
            this.bbiDownloadFile.LargeImageIndex = 1;
            this.bbiDownloadFile.Name = "bbiDownloadFile";
            // 
            // bbiDeleteFile
            // 
            this.bbiDeleteFile.Caption = "Удалить";
            this.bbiDeleteFile.Id = 3;
            this.bbiDeleteFile.ImageIndex = 2;
            this.bbiDeleteFile.ImageUri.Uri = "DeleteFile";
            this.bbiDeleteFile.LargeImageIndex = 2;
            this.bbiDeleteFile.Name = "bbiDeleteFile";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiPreviewFile
            // 
            this.bbiPreviewFile.Caption = "Просмотр";
            this.bbiPreviewFile.Id = 6;
            this.bbiPreviewFile.ImageUri.Uri = "Preview";
            this.bbiPreviewFile.Name = "bbiPreviewFile";
            // 
            // imageCollectionLarge
            // 
            this.imageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionLarge.ImageStream")));
            this.imageCollectionLarge.InsertGalleryImage("clear_32x32.png", "office2013/actions/clear_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/actions/clear_32x32.png"), 0);
            this.imageCollectionLarge.Images.SetKeyName(0, "clear_32x32.png");
            this.imageCollectionLarge.InsertGalleryImage("download_32x32.png", "office2013/actions/download_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/actions/download_32x32.png"), 1);
            this.imageCollectionLarge.Images.SetKeyName(1, "download_32x32.png");
            this.imageCollectionLarge.InsertGalleryImage("delete_32x32.png", "office2013/edit/delete_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/edit/delete_32x32.png"), 2);
            this.imageCollectionLarge.Images.SetKeyName(2, "delete_32x32.png");
            this.imageCollectionLarge.InsertGalleryImage("publish_32x32.png", "office2013/miscellaneous/publish_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/miscellaneous/publish_32x32.png"), 3);
            this.imageCollectionLarge.Images.SetKeyName(3, "publish_32x32.png");
            // 
            // rpMonitorReports
            // 
            this.rpMonitorReports.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions});
            this.rpMonitorReports.Name = "rpMonitorReports";
            this.rpMonitorReports.Text = "Отчеты по мониторингу";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiUploadFile);
            this.rpgActions.ItemLinks.Add(this.bbiDownloadFile);
            this.rpgActions.ItemLinks.Add(this.bbiPreviewFile);
            this.rpgActions.ItemLinks.Add(this.bbiDeleteFile);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.ShowCaptionButton = false;
            this.rpgActions.Text = "Действия";
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel), "UploadFile", this.bbiUploadFile),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel), "DownloadFile", this.bbiDownloadFile),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel), "DeleteFile", this.bbiDeleteFile),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel), "PreviewFile", this.bbiPreviewFile)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportCollectionViewModel);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 141);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlMaster);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControlDetails);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(768, 352);
            this.splitContainerControl1.SplitterPosition = 148;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControlMaster
            // 
            this.gridControlMaster.DataSource = this.masterBindingSource;
            this.gridControlMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMaster.Location = new System.Drawing.Point(0, 0);
            this.gridControlMaster.MainView = this.gridViewMaster;
            this.gridControlMaster.MenuManager = this.ribbonControl1;
            this.gridControlMaster.Name = "gridControlMaster";
            this.gridControlMaster.Size = new System.Drawing.Size(768, 199);
            this.gridControlMaster.TabIndex = 0;
            this.gridControlMaster.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMaster});
            // 
            // masterBindingSource
            // 
            this.masterBindingSource.AllowNew = false;
            this.masterBindingSource.DataSource = typeof(GSC.Client.ViewModels.MonitorResult.MonitorReportsHolderInfo);
            // 
            // gridViewMaster
            // 
            this.gridViewMaster.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRegionName,
            this.colExpertFullName,
            this.colPeriodType,
            this.colMonitorRcoi,
            this.colReportsLoaded});
            this.gridViewMaster.GridControl = this.gridControlMaster;
            this.gridViewMaster.Name = "gridViewMaster";
            this.gridViewMaster.OptionsView.ColumnAutoWidth = false;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Субъект РФ";
            this.colRegionName.FieldName = "Region.Name";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 0;
            this.colRegionName.Width = 150;
            // 
            // colExpertFullName
            // 
            this.colExpertFullName.Caption = "ФИО эксперта";
            this.colExpertFullName.FieldName = "Expert.FullName";
            this.colExpertFullName.Name = "colExpertFullName";
            this.colExpertFullName.OptionsColumn.AllowEdit = false;
            this.colExpertFullName.OptionsColumn.ReadOnly = true;
            this.colExpertFullName.Visible = true;
            this.colExpertFullName.VisibleIndex = 1;
            this.colExpertFullName.Width = 200;
            // 
            // colPeriodType
            // 
            this.colPeriodType.Caption = "Этап";
            this.colPeriodType.FieldName = "Schedule.PeriodType";
            this.colPeriodType.Name = "colPeriodType";
            this.colPeriodType.OptionsColumn.AllowEdit = false;
            this.colPeriodType.OptionsColumn.ReadOnly = true;
            this.colPeriodType.Visible = true;
            this.colPeriodType.VisibleIndex = 2;
            this.colPeriodType.Width = 150;
            // 
            // colMonitorRcoi
            // 
            this.colMonitorRcoi.Caption = "Мониторинг РЦОИ";
            this.colMonitorRcoi.FieldName = "Schedule.MonitorRcoi";
            this.colMonitorRcoi.Name = "colMonitorRcoi";
            this.colMonitorRcoi.OptionsColumn.AllowEdit = false;
            this.colMonitorRcoi.OptionsColumn.ReadOnly = true;
            this.colMonitorRcoi.Visible = true;
            this.colMonitorRcoi.VisibleIndex = 3;
            // 
            // colReportsLoaded
            // 
            this.colReportsLoaded.Caption = "Отчет загружен";
            this.colReportsLoaded.FieldName = "ReportsLoadedDomain";
            this.colReportsLoaded.Name = "colReportsLoaded";
            this.colReportsLoaded.OptionsColumn.AllowEdit = false;
            this.colReportsLoaded.OptionsColumn.ReadOnly = true;
            this.colReportsLoaded.Visible = true;
            this.colReportsLoaded.VisibleIndex = 4;
            // 
            // gridControlDetails
            // 
            this.gridControlDetails.DataSource = this.detailsBindingSource;
            this.gridControlDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDetails.Location = new System.Drawing.Point(0, 0);
            this.gridControlDetails.MainView = this.gridViewDetails;
            this.gridControlDetails.MenuManager = this.ribbonControl1;
            this.gridControlDetails.Name = "gridControlDetails";
            this.gridControlDetails.Size = new System.Drawing.Size(768, 148);
            this.gridControlDetails.TabIndex = 0;
            this.gridControlDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetails});
            // 
            // detailsBindingSource
            // 
            this.detailsBindingSource.DataSource = typeof(GSC.Client.ViewModels.MonitorResult.MrDocInfo);
            // 
            // gridViewDetails
            // 
            this.gridViewDetails.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTs,
            this.colFilename,
            this.colBlobSize});
            this.gridViewDetails.GridControl = this.gridControlDetails;
            this.gridViewDetails.Name = "gridViewDetails";
            this.gridViewDetails.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridViewDetails.OptionsSelection.MultiSelect = true;
            this.gridViewDetails.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewDetails.OptionsView.ColumnAutoWidth = false;
            this.gridViewDetails.OptionsView.ShowGroupPanel = false;
            // 
            // colTs
            // 
            this.colTs.Caption = "Дата загрузки";
            this.colTs.DisplayFormat.FormatString = "g";
            this.colTs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTs.FieldName = "Timestamp";
            this.colTs.Name = "colTs";
            this.colTs.OptionsColumn.AllowEdit = false;
            this.colTs.OptionsColumn.ReadOnly = true;
            this.colTs.Visible = true;
            this.colTs.VisibleIndex = 1;
            this.colTs.Width = 150;
            // 
            // colFilename
            // 
            this.colFilename.Caption = "Файл";
            this.colFilename.FieldName = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.OptionsColumn.AllowEdit = false;
            this.colFilename.OptionsColumn.ReadOnly = true;
            this.colFilename.Visible = true;
            this.colFilename.VisibleIndex = 2;
            this.colFilename.Width = 250;
            // 
            // colBlobSize
            // 
            this.colBlobSize.Caption = "Размер (байт)";
            this.colBlobSize.FieldName = "BlobSize";
            this.colBlobSize.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBlobSize.Name = "colBlobSize";
            this.colBlobSize.OptionsColumn.AllowEdit = false;
            this.colBlobSize.OptionsColumn.ReadOnly = true;
            this.colBlobSize.Visible = true;
            this.colBlobSize.VisibleIndex = 3;
            this.colBlobSize.Width = 100;
            // 
            // MonitorReportCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "MonitorReportCollectionView";
            this.Size = new System.Drawing.Size(768, 493);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpMonitorReports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControlMaster;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMaster;
        private DevExpress.XtraGrid.GridControl gridControlDetails;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetails;
        private System.Windows.Forms.BindingSource masterBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpertFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colMonitorRcoi;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodType;
        private DevExpress.XtraBars.BarButtonItem bbiUploadFile;
        private DevExpress.XtraBars.BarButtonItem bbiDownloadFile;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteFile;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.Utils.ImageCollection imageCollectionLarge;
        private DevExpress.Utils.ImageCollection imageCollectionSmall;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsLoaded;
        private DevExpress.XtraBars.BarButtonItem bbiPreviewFile;
        private System.Windows.Forms.BindingSource detailsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTs;
        private DevExpress.XtraGrid.Columns.GridColumn colFilename;
        private DevExpress.XtraGrid.Columns.GridColumn colBlobSize;
    }
}
