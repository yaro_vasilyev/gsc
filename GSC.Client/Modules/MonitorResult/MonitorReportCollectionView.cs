﻿using System.Linq;
using DevExpress.Data;
using GSC.Client.Util;
using GSC.Client.ViewModels.MonitorResult;

namespace GSC.Client.Modules.MonitorResult
{
    public partial class MonitorReportCollectionView : CollectionBlockView
    {
        public MonitorReportCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                mvvmContext1.RegisterService(new UpdateGridService(gridControlDetails));
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<MonitorReportCollectionViewModel>();
            fluent.InitGridViewReadonly(gridViewMaster, fluent.ViewModel);
            fluent.SetBinding(masterBindingSource, bs => bs.DataSource, vm => vm.Entities);
            fluent.SetBinding(detailsBindingSource, bs => bs.DataSource, vm => vm.MrDocs);
            fluent.WithEvent<SelectionChangedEventArgs>(gridViewDetails, "SelectionChanged")
                .SetBinding(vm => vm.SelectedMrDocs,
                    ea => gridViewDetails.GetSelectedRows().Select(gridViewDetails.GetRow).Cast<MrDocInfo>().ToArray());
        }
    }
}
