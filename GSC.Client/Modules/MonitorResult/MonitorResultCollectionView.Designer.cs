﻿namespace GSC.Client.Modules.MonitorResult
{
    partial class MonitorResultCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpMonitor = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgMonitorActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.monitorResultBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStationCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViolationCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViolationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViolatorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViolatorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExamType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleDetail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchedule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleDetailExamDate = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorResultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 7;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMonitor});
            this.ribbonControl1.Size = new System.Drawing.Size(584, 140);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.MonitorResult.MonitorResultCollectionViewModel);
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 1;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 2;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 3;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 4;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 5;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpMonitor
            // 
            this.rpMonitor.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgMonitorActions,
            this.rpgExchange});
            this.rpMonitor.Name = "rpMonitor";
            this.rpMonitor.Text = "Результаты мониторинга";
            // 
            // rpgMonitorActions
            // 
            this.rpgMonitorActions.ItemLinks.Add(this.bbiNew);
            this.rpgMonitorActions.ItemLinks.Add(this.bbiEdit);
            this.rpgMonitorActions.ItemLinks.Add(this.bbiView);
            this.rpgMonitorActions.ItemLinks.Add(this.bbiDelete);
            this.rpgMonitorActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgMonitorActions.Name = "rpgMonitorActions";
            this.rpgMonitorActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 6;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.monitorResultBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 140);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(584, 262);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // monitorResultBindingSource
            // 
            this.monitorResultBindingSource.DataSource = typeof(GSC.Model.MonitorResult);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colScheduleRegionName,
            this.colDate,
            this.colStationCode,
            this.colViolationCode,
            this.colViolationName,
            this.colViolatorCode,
            this.colViolatorName,
            this.colNote,
            this.colObjectType,
            this.colExamType,
            this.colScheduleDetail,
            this.colSchedule,
            this.colScheduleDetailExamDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // colId
            // 
            this.colId.Caption = "ID";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.Width = 100;
            // 
            // colScheduleRegionName
            // 
            this.colScheduleRegionName.Caption = "Субъект РФ";
            this.colScheduleRegionName.FieldName = "Schedule.Region.Name";
            this.colScheduleRegionName.Name = "colScheduleRegionName";
            this.colScheduleRegionName.OptionsColumn.AllowEdit = false;
            this.colScheduleRegionName.Visible = true;
            this.colScheduleRegionName.VisibleIndex = 0;
            this.colScheduleRegionName.Width = 100;
            // 
            // colDate
            // 
            this.colDate.Caption = "Дата";
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            this.colDate.Width = 100;
            // 
            // colStationCode
            // 
            this.colStationCode.Caption = "Код ППЭ";
            this.colStationCode.FieldName = "ScheduleDetail.Station.Code";
            this.colStationCode.Name = "colStationCode";
            this.colStationCode.OptionsColumn.AllowEdit = false;
            this.colStationCode.Visible = true;
            this.colStationCode.VisibleIndex = 2;
            this.colStationCode.Width = 100;
            // 
            // colViolationCode
            // 
            this.colViolationCode.Caption = "Код нарушения";
            this.colViolationCode.FieldName = "Violation.Code";
            this.colViolationCode.Name = "colViolationCode";
            this.colViolationCode.OptionsColumn.AllowEdit = false;
            this.colViolationCode.Visible = true;
            this.colViolationCode.VisibleIndex = 3;
            this.colViolationCode.Width = 100;
            // 
            // colViolationName
            // 
            this.colViolationName.Caption = "Вид нарушения";
            this.colViolationName.FieldName = "Violation.Name";
            this.colViolationName.Name = "colViolationName";
            this.colViolationName.OptionsColumn.AllowEdit = false;
            this.colViolationName.Visible = true;
            this.colViolationName.VisibleIndex = 4;
            this.colViolationName.Width = 100;
            // 
            // colViolatorCode
            // 
            this.colViolatorCode.Caption = "Код нарушителя";
            this.colViolatorCode.FieldName = "Violator.Code";
            this.colViolatorCode.Name = "colViolatorCode";
            this.colViolatorCode.OptionsColumn.AllowEdit = false;
            this.colViolatorCode.Visible = true;
            this.colViolatorCode.VisibleIndex = 5;
            this.colViolatorCode.Width = 100;
            // 
            // colViolatorName
            // 
            this.colViolatorName.Caption = "Категория нарушителей";
            this.colViolatorName.FieldName = "Violator.Name";
            this.colViolatorName.Name = "colViolatorName";
            this.colViolatorName.OptionsColumn.AllowEdit = false;
            this.colViolatorName.Visible = true;
            this.colViolatorName.VisibleIndex = 6;
            this.colViolatorName.Width = 100;
            // 
            // colNote
            // 
            this.colNote.Caption = "Примечание";
            this.colNote.FieldName = "Note";
            this.colNote.Name = "colNote";
            this.colNote.OptionsColumn.AllowEdit = false;
            this.colNote.Width = 100;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Тип объекта";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.Width = 100;
            // 
            // colExamType
            // 
            this.colExamType.Caption = "Форма ГИА";
            this.colExamType.FieldName = "ExamType";
            this.colExamType.Name = "colExamType";
            this.colExamType.OptionsColumn.AllowEdit = false;
            this.colExamType.Visible = true;
            this.colExamType.VisibleIndex = 7;
            this.colExamType.Width = 100;
            // 
            // colScheduleDetail
            // 
            this.colScheduleDetail.Caption = "Эксперт";
            this.colScheduleDetail.FieldName = "Schedule.Expert.FullName";
            this.colScheduleDetail.Name = "colScheduleDetail";
            this.colScheduleDetail.OptionsColumn.AllowEdit = false;
            this.colScheduleDetail.Width = 100;
            // 
            // colSchedule
            // 
            this.colSchedule.Caption = "Этап";
            this.colSchedule.FieldName = "Schedule.PeriodType";
            this.colSchedule.Name = "colSchedule";
            this.colSchedule.OptionsColumn.AllowEdit = false;
            this.colSchedule.Visible = true;
            this.colSchedule.VisibleIndex = 8;
            this.colSchedule.Width = 100;
            // 
            // colScheduleDetailExamDate
            // 
            this.colScheduleDetailExamDate.Caption = "Дата экзамена";
            this.colScheduleDetailExamDate.FieldName = "ScheduleDetail.ExamDate";
            this.colScheduleDetailExamDate.Name = "colScheduleDetailExamDate";
            this.colScheduleDetailExamDate.OptionsColumn.AllowEdit = false;
            this.colScheduleDetailExamDate.Width = 100;
            // 
            // MonitorResultCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "MonitorResultCollectionView";
            this.Size = new System.Drawing.Size(584, 402);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorResultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpMonitor;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource monitorResultBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colViolationCode;
        private DevExpress.XtraGrid.Columns.GridColumn colViolatorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DevExpress.XtraGrid.Columns.GridColumn colExamType;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colSchedule;
        private DevExpress.XtraGrid.Columns.GridColumn colViolationName;
        private DevExpress.XtraGrid.Columns.GridColumn colViolatorName;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleDetailExamDate;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colStationCode;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMonitorActions;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
