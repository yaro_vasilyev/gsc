﻿using GSC.Client.Util;
using GSC.Client.ViewModels.MonitorResult;

namespace GSC.Client.Modules.MonitorResult
{
    public partial class MonitorResultCollectionView : CollectionBlockView
    {
        public MonitorResultCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<MonitorResultCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private MonitorResultCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<MonitorResultCollectionViewModel>(); }
        }
    }
}
