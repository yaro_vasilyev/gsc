﻿using System;
using DevExpress.XtraBars.Navigation;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules
{
    public partial class BaseReportsView : BaseBlockView
    {
        public BaseReportsView()
        {
            InitializeComponent();
        }

        private void accordionControl1_ElementClick(object sender, ElementClickEventArgs e)
        {
            // In case of click on category
            var reportInfo = e.Element.Tag as ReportInfo;
            if (reportInfo == null)
            {
                return;
            }

            var reportsViewModel = mvvmContext1.GetViewModel<ReportsViewModel>();
            reportsViewModel.ExecuteReport(reportInfo);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            accordionControl1.ExpandAll();
        }
    }
}
