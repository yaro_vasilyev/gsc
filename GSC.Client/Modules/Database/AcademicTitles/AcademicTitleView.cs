﻿using GSC.Client.Util;
using GSC.Client.ViewModels;
using GSC.Model;

namespace GSC.Client.Modules.Database.AcademicTitles
{
    public partial class AcademicTitleView : BaseEditView
    {
        public AcademicTitleView()
        {
            InitializeComponent();
            comboBoxEdit1.InitItemsFromDataAnnotations<AcademicTitle>(e => e.DefaultValue);

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<AcademicTitleViewModel>();
            fluent.SetObjectDataSourceBinding(academicTitleBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

    }
}
