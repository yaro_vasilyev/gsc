﻿using GSC.Client.Util;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.AcademicTitles
{
    public partial class AcademicTitleCollectionView : CollectionBlockView
    {
        public AcademicTitleCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<AcademicTitleCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                                         ViewModel,
                                         () => new GridAdapter(gridControl1));
        }

        private AcademicTitleCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<AcademicTitleCollectionViewModel>(); }
        }
    }
}
