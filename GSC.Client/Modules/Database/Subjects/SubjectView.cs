﻿using DevExpress.Utils;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Subjects
{
    public partial class SubjectView : BaseEditView
    {
        public SubjectView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<SubjectViewModel>();
            fluent.SetObjectDataSourceBinding(subjectBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
