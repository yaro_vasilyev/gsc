﻿using GSC.Client.Util;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Subjects
{
    public partial class SubjectCollectionView : CollectionBlockView
    {
        public SubjectCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<SubjectCollectionViewModel>();
            fluent.InitGridView(gridView, ViewModel);
            fluent.SetBinding(gridControl, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl));
        }

        private SubjectCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<SubjectCollectionViewModel>(); }
        }
    }
}
