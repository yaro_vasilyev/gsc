﻿using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.AcademicDegrees
{
    public partial class AcademicDegreeView : BaseEditView
    {
        public AcademicDegreeView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<AcademicDegreeViewModel>();
            fluent.SetObjectDataSourceBinding(academicDegreeBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }
    }
}
