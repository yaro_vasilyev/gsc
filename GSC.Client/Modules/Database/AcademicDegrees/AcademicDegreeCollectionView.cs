﻿using GSC.Client.ViewModels;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Database.AcademicDegrees
{
    [UsedImplicitly]
    public partial class AcademicDegreeCollectionView : CollectionBlockView
    {
        public AcademicDegreeCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<AcademicDegreeCollectionViewModel>();
            fluent.InitGridView(gridView, ViewModel);

            fluent.SetBinding(gridControl, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                                         ViewModel,
                                         () => new GridAdapter(gridControl));
        }

        private AcademicDegreeCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<AcademicDegreeCollectionViewModel>(); }
        }
    }
}
