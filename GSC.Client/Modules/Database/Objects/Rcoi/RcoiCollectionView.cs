﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Rcoi;

namespace GSC.Client.Modules.Database.Objects.Rcoi
{
    public partial class RcoiCollectionView : CollectionBlockView
    {
        public RcoiCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<RcoiCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                                         ViewModel,
                                         () => new GridAdapter(gridControl1));

        }

        private RcoiCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<RcoiCollectionViewModel>(); }
        }
    }
}
