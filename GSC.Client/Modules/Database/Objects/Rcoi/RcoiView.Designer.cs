﻿namespace GSC.Client.Modules.Database.Objects.Rcoi
{
    partial class RcoiView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.rcoiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ChiefPostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefFioTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefPhone1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefPhone2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.RegionIdTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefFio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefPhone1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefPhone2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcoiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefFioTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPhone1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPhone2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefFio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPhone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPhone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Rcoi.RcoiViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(607, 140);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefPostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefFioTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefPhone1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefPhone2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionIdTextEdit);
            this.dataLayoutControl1.DataSource = this.rcoiBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 140);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1007, 240, 522, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(607, 265);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(159, 12);
            this.NameTextEdit.MenuManager = this.ribbonControl1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Size = new System.Drawing.Size(436, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 5;
            // 
            // rcoiBindingSource
            // 
            this.rcoiBindingSource.DataSource = typeof(GSC.Model.Rcoi);
            // 
            // ChiefPostTextEdit
            // 
            this.ChiefPostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "ChiefPost", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefPostTextEdit.Location = new System.Drawing.Point(159, 137);
            this.ChiefPostTextEdit.MenuManager = this.ribbonControl1;
            this.ChiefPostTextEdit.Name = "ChiefPostTextEdit";
            this.ChiefPostTextEdit.Size = new System.Drawing.Size(436, 20);
            this.ChiefPostTextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefPostTextEdit.TabIndex = 7;
            // 
            // ChiefFioTextEdit
            // 
            this.ChiefFioTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "ChiefFio", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefFioTextEdit.Location = new System.Drawing.Point(159, 161);
            this.ChiefFioTextEdit.MenuManager = this.ribbonControl1;
            this.ChiefFioTextEdit.Name = "ChiefFioTextEdit";
            this.ChiefFioTextEdit.Size = new System.Drawing.Size(436, 20);
            this.ChiefFioTextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefFioTextEdit.TabIndex = 8;
            // 
            // ChiefPhone1TextEdit
            // 
            this.ChiefPhone1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "ChiefPhone1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefPhone1TextEdit.Location = new System.Drawing.Point(159, 185);
            this.ChiefPhone1TextEdit.MenuManager = this.ribbonControl1;
            this.ChiefPhone1TextEdit.Name = "ChiefPhone1TextEdit";
            this.ChiefPhone1TextEdit.Size = new System.Drawing.Size(436, 20);
            this.ChiefPhone1TextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefPhone1TextEdit.TabIndex = 9;
            // 
            // ChiefPhone2TextEdit
            // 
            this.ChiefPhone2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "ChiefPhone2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefPhone2TextEdit.Location = new System.Drawing.Point(159, 209);
            this.ChiefPhone2TextEdit.MenuManager = this.ribbonControl1;
            this.ChiefPhone2TextEdit.Name = "ChiefPhone2TextEdit";
            this.ChiefPhone2TextEdit.Size = new System.Drawing.Size(436, 20);
            this.ChiefPhone2TextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefPhone2TextEdit.TabIndex = 10;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.Location = new System.Drawing.Point(159, 233);
            this.EmailTextEdit.MenuManager = this.ribbonControl1;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Size = new System.Drawing.Size(436, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 11;
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "Address", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AddressTextEdit.Location = new System.Drawing.Point(159, 36);
            this.AddressTextEdit.MenuManager = this.ribbonControl1;
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Size = new System.Drawing.Size(436, 73);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 6;
            // 
            // RegionIdTextEdit
            // 
            this.RegionIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.rcoiBindingSource, "RegionId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RegionIdTextEdit.Location = new System.Drawing.Point(159, 113);
            this.RegionIdTextEdit.MenuManager = this.ribbonControl1;
            this.RegionIdTextEdit.Name = "RegionIdTextEdit";
            this.RegionIdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RegionIdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RegionIdTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegionIdTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.RegionIdTextEdit.Properties.DataSource = this.regionBindingSource;
            this.RegionIdTextEdit.Properties.DisplayMember = "Name";
            this.RegionIdTextEdit.Properties.NullText = "";
            this.RegionIdTextEdit.Properties.ValueMember = "Id";
            this.RegionIdTextEdit.Size = new System.Drawing.Size(436, 20);
            this.RegionIdTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionIdTextEdit.TabIndex = 4;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(607, 265);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForName,
            this.ItemForAddress,
            this.ItemForChiefPost,
            this.ItemForChiefFio,
            this.ItemForChiefPhone1,
            this.ItemForChiefPhone2,
            this.ItemForEmail,
            this.ItemForRegionId});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(587, 245);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(587, 24);
            this.ItemForName.Text = "Наименование организации:";
            this.ItemForName.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.Location = new System.Drawing.Point(0, 24);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(587, 77);
            this.ItemForAddress.Text = "Адрес РЦОИ:";
            this.ItemForAddress.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForChiefPost
            // 
            this.ItemForChiefPost.Control = this.ChiefPostTextEdit;
            this.ItemForChiefPost.Location = new System.Drawing.Point(0, 125);
            this.ItemForChiefPost.Name = "ItemForChiefPost";
            this.ItemForChiefPost.Size = new System.Drawing.Size(587, 24);
            this.ItemForChiefPost.Text = "Должность руководителя:";
            this.ItemForChiefPost.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForChiefFio
            // 
            this.ItemForChiefFio.Control = this.ChiefFioTextEdit;
            this.ItemForChiefFio.Location = new System.Drawing.Point(0, 149);
            this.ItemForChiefFio.Name = "ItemForChiefFio";
            this.ItemForChiefFio.Size = new System.Drawing.Size(587, 24);
            this.ItemForChiefFio.Text = "ФИО руководителя:";
            this.ItemForChiefFio.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForChiefPhone1
            // 
            this.ItemForChiefPhone1.Control = this.ChiefPhone1TextEdit;
            this.ItemForChiefPhone1.Location = new System.Drawing.Point(0, 173);
            this.ItemForChiefPhone1.Name = "ItemForChiefPhone1";
            this.ItemForChiefPhone1.Size = new System.Drawing.Size(587, 24);
            this.ItemForChiefPhone1.Text = "Рабочий телефон:";
            this.ItemForChiefPhone1.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForChiefPhone2
            // 
            this.ItemForChiefPhone2.Control = this.ChiefPhone2TextEdit;
            this.ItemForChiefPhone2.Location = new System.Drawing.Point(0, 197);
            this.ItemForChiefPhone2.Name = "ItemForChiefPhone2";
            this.ItemForChiefPhone2.Size = new System.Drawing.Size(587, 24);
            this.ItemForChiefPhone2.Text = "Мобильный телефон:";
            this.ItemForChiefPhone2.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.Location = new System.Drawing.Point(0, 221);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(587, 24);
            this.ItemForEmail.Text = "Электронная почта:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForRegionId
            // 
            this.ItemForRegionId.Control = this.RegionIdTextEdit;
            this.ItemForRegionId.Location = new System.Drawing.Point(0, 101);
            this.ItemForRegionId.Name = "ItemForRegionId";
            this.ItemForRegionId.Size = new System.Drawing.Size(587, 24);
            this.ItemForRegionId.Text = "Регион:";
            this.ItemForRegionId.TextSize = new System.Drawing.Size(144, 13);
            // 
            // RcoiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RcoiView";
            this.Size = new System.Drawing.Size(607, 405);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcoiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefFioTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPhone1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPhone2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefFio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPhone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPhone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private System.Windows.Forms.BindingSource rcoiBindingSource;
        private DevExpress.XtraEditors.TextEdit ChiefPostTextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefFioTextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefPhone1TextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefPhone2TextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.MemoEdit AddressTextEdit;
        private DevExpress.XtraEditors.LookUpEdit RegionIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefFio;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefPhone1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefPhone2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionId;
        private System.Windows.Forms.BindingSource regionBindingSource;
    }
}
