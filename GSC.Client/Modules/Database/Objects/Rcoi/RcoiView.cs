﻿using DevExpress.Utils;
using GSC.Client.ViewModels.Rcoi;

namespace GSC.Client.Modules.Database.Objects.Rcoi
{
    public partial class RcoiView : BaseEditView
    {
        public RcoiView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindigns();
            }
        }

        private void InitBindigns()
        {
            var fluent = mvvmContext1.OfType<RcoiViewModel>();
            fluent.SetBinding(regionBindingSource,
                              bs => bs.DataSource,
                              vm => vm.LookupRegions.Entities);
            fluent.SetObjectDataSourceBinding(rcoiBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
