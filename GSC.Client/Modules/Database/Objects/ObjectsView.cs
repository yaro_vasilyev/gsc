﻿using System;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using GSC.Client.Util;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Objects
{
    public partial class ObjectsView : XtraUserControl, IRibbonSource
    {
        public ObjectsView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext.OfType<ObjectsViewModel>();

            fluent.SetItemsSourceBinding(officeNavigationBar1,
                                         onb => onb.Items,
                                         vm => vm.Modules,
                                         (i, m) => Equals(i.Tag, m),
                                         m =>
                                         {
                                             var item = new NavigationBarItem
                                             {
                                                 Text = m.ModuleTitle,
                                                 Tag = m
                                             };
                                             fluent.BindCommand(item, vm => vm.Show(null), vm => item.Tag);
                                             return item;
                                         });
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var viewModel = mvvmContext.GetViewModel<ObjectsViewModel>();
            if (!viewModel.Modules.Any())
            {
                return;
            }
            viewModel.Show(viewModel.DefaultModule);
            officeNavigationBar1.SelectedItem = officeNavigationBar1.Items.First(i => Equals(i.Tag, viewModel.DefaultModule));
        }

        private IRibbonSource GetChildRibbonSource()
        {
            var activePageControl = navigationFrame.GetSelectedPageRootControl();
            return activePageControl as IRibbonSource;
        }

        public RibbonControl Ribbon => GetChildRibbonSource()?.Ribbon;

        private void OnNavigationFrameSelectedPageChanged(object sender, SelectedPageChangedEventArgs e)
        {
            Messenger.Default.Send(new MergeRibbonMessage
            {
                Ribbon = Ribbon
            });
        }
    }
}
