﻿using DevExpress.Utils;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Objects.Regions
{
    public partial class RegionView : BaseEditView
    {
        public RegionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<RegionViewModel>();
            fluent.SetObjectDataSourceBinding(regionBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
