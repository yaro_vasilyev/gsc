﻿using GSC.Client.Util;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Objects.Regions
{
    public partial class RegionCollectionView : CollectionBlockView
    {
        public RegionCollectionView()
        {
            InitializeComponent();
            repositoryItemImageComboBox1.Items.AddEnum<Model.ControlZone>(ControlZoneConverter);
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private string ControlZoneConverter(Model.ControlZone controlZone)
        {
            switch (controlZone)
            {
                case Model.ControlZone.Green:
                    return "Зеленая";
                case Model.ControlZone.Yellow:
                    return "Желтая";
                case Model.ControlZone.Red:
                    return "Красная";
                default:
                    return "Зона контроля: <неизвестно>";
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<RegionCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);

            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private RegionCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<RegionCollectionViewModel>(); }
        }
    }
}
