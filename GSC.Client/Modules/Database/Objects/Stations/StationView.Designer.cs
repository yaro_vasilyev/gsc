﻿namespace GSC.Client.Modules.Database.Objects.Stations
{
    partial class StationView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.RegionIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.stationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.tomCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTom = new DevExpress.XtraLayout.LayoutControlItem();
            this.ExamTypeImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ItemForExamType = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tomCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamTypeImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamType)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 17;
            this.ribbonControl1.Name = "ribbonControl";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(604, 141);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // mvvmContext
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Station.StationViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Station.StationViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Station.StationViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Station.StationViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Station.StationViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Station.StationViewModel);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.RegionIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.tomCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ExamTypeImageComboBoxEdit);
            this.dataLayoutControl1.DataSource = this.stationBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1321, 187, 622, 598);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(604, 299);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // RegionIdLookUpEdit
            // 
            this.RegionIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.stationBindingSource, "RegionId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RegionIdLookUpEdit.Location = new System.Drawing.Point(100, 12);
            this.RegionIdLookUpEdit.MenuManager = this.ribbonControl1;
            this.RegionIdLookUpEdit.Name = "RegionIdLookUpEdit";
            this.RegionIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RegionIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RegionIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegionIdLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.RegionIdLookUpEdit.Properties.DataSource = this.regionBindingSource;
            this.RegionIdLookUpEdit.Properties.DisplayMember = "Name";
            this.RegionIdLookUpEdit.Properties.NullText = "";
            this.RegionIdLookUpEdit.Properties.ValueMember = "Id";
            this.RegionIdLookUpEdit.Size = new System.Drawing.Size(492, 20);
            this.RegionIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RegionIdLookUpEdit.TabIndex = 4;
            // 
            // stationBindingSource
            // 
            this.stationBindingSource.DataSource = typeof(GSC.Model.Station);
            this.stationBindingSource.DataSourceChanged += new System.EventHandler(this.stationBindingSource_DataSourceChanged);
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // CodeTextEdit
            // 
            this.CodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.stationBindingSource, "Code", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CodeTextEdit.Location = new System.Drawing.Point(100, 36);
            this.CodeTextEdit.MenuManager = this.ribbonControl1;
            this.CodeTextEdit.Name = "CodeTextEdit";
            this.CodeTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CodeTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CodeTextEdit.Properties.Mask.EditMask = "N0";
            this.CodeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CodeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CodeTextEdit.Size = new System.Drawing.Size(492, 20);
            this.CodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CodeTextEdit.TabIndex = 5;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.stationBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(100, 60);
            this.NameTextEdit.MenuManager = this.ribbonControl1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Size = new System.Drawing.Size(492, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 6;
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.stationBindingSource, "Address", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AddressTextEdit.Location = new System.Drawing.Point(100, 84);
            this.AddressTextEdit.MenuManager = this.ribbonControl1;
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Size = new System.Drawing.Size(492, 20);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 7;
            // 
            // tomCheckEdit
            // 
            this.tomCheckEdit.EditValue = null;
            this.tomCheckEdit.Location = new System.Drawing.Point(100, 108);
            this.tomCheckEdit.MenuManager = this.ribbonControl1;
            this.tomCheckEdit.Name = "tomCheckEdit";
            this.tomCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.tomCheckEdit.Properties.Caption = "";
            this.tomCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.tomCheckEdit.Size = new System.Drawing.Size(492, 19);
            this.tomCheckEdit.StyleController = this.dataLayoutControl1;
            this.tomCheckEdit.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(604, 299);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRegionId,
            this.ItemForCode,
            this.ItemForName,
            this.ItemForAddress,
            this.ItemForTom,
            this.ItemForExamType});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(584, 279);
            // 
            // ItemForRegionId
            // 
            this.ItemForRegionId.Control = this.RegionIdLookUpEdit;
            this.ItemForRegionId.Location = new System.Drawing.Point(0, 0);
            this.ItemForRegionId.Name = "ItemForRegionId";
            this.ItemForRegionId.Size = new System.Drawing.Size(584, 24);
            this.ItemForRegionId.Text = "Субъект РФ:";
            this.ItemForRegionId.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForCode
            // 
            this.ItemForCode.Control = this.CodeTextEdit;
            this.ItemForCode.Location = new System.Drawing.Point(0, 24);
            this.ItemForCode.Name = "ItemForCode";
            this.ItemForCode.Size = new System.Drawing.Size(584, 24);
            this.ItemForCode.Text = "Код:";
            this.ItemForCode.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 48);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(584, 24);
            this.ItemForName.Text = "Название:";
            this.ItemForName.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.CustomizationFormText = "Адрес";
            this.ItemForAddress.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(584, 24);
            this.ItemForAddress.Text = "Адрес:";
            this.ItemForAddress.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForTom
            // 
            this.ItemForTom.Control = this.tomCheckEdit;
            this.ItemForTom.CustomizationFormText = "ТОМ";
            this.ItemForTom.Location = new System.Drawing.Point(0, 96);
            this.ItemForTom.Name = "ItemForTom";
            this.ItemForTom.Size = new System.Drawing.Size(584, 23);
            this.ItemForTom.Text = "ТОМ:";
            this.ItemForTom.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ExamTypeImageComboBoxEdit
            // 
            this.ExamTypeImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.stationBindingSource, "ExamType", true));
            this.ExamTypeImageComboBoxEdit.Location = new System.Drawing.Point(100, 131);
            this.ExamTypeImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.ExamTypeImageComboBoxEdit.Name = "ExamTypeImageComboBoxEdit";
            this.ExamTypeImageComboBoxEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExamTypeImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExamTypeImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExamTypeImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExamTypeImageComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("ЕГЭ", GSC.Model.ExamType.Ege, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("ГВЭ", GSC.Model.ExamType.Gve, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("ОГЭ", GSC.Model.ExamType.Oge, 2)});
            this.ExamTypeImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.ExamTypeImageComboBoxEdit.Size = new System.Drawing.Size(492, 20);
            this.ExamTypeImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ExamTypeImageComboBoxEdit.TabIndex = 9;
            // 
            // ItemForExamType
            // 
            this.ItemForExamType.Control = this.ExamTypeImageComboBoxEdit;
            this.ItemForExamType.Location = new System.Drawing.Point(0, 119);
            this.ItemForExamType.Name = "ItemForExamType";
            this.ItemForExamType.Size = new System.Drawing.Size(584, 160);
            this.ItemForExamType.Text = "Форма экзамена:";
            this.ItemForExamType.TextSize = new System.Drawing.Size(85, 13);
            // 
            // StationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "StationView";
            this.Size = new System.Drawing.Size(604, 440);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tomCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamTypeImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit RegionIdLookUpEdit;
        private System.Windows.Forms.BindingSource stationBindingSource;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private DevExpress.XtraEditors.TextEdit CodeTextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressTextEdit;
        private DevExpress.XtraEditors.CheckEdit tomCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTom;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraEditors.ImageComboBoxEdit ExamTypeImageComboBoxEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExamType;
    }
}
