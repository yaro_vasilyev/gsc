﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Station;

namespace GSC.Client.Modules.Database.Objects.Stations
{
    public partial class StationCollectionView : CollectionBlockView
    {
        public StationCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<StationCollectionViewModel>();
            fluent.SetBinding(gridControl, c => c.DataSource, vm => vm.Entities);

            fluent.InitInstantFeedbackGridView(gridView, ViewModel);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl, true));

        }

        private StationCollectionViewModel ViewModel => mvvmContext1.GetViewModel<StationCollectionViewModel>();
    }
}
