﻿using System;
using System.Diagnostics;
using DevExpress.Utils;
using GSC.Client.ViewModels.Station;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Database.Objects.Stations
{
    [UsedImplicitly]
    public partial class StationView : BaseEditView
    {
        public StationView()
        {
            InitializeComponent();

            ExamTypeImageComboBoxEdit.Properties.Items.AddEnum<ExamType>();

            if (!DesignMode)
            {
                var fluent = mvvmContext1.OfType<StationViewModel>();
                fluent.SetObjectDataSourceBinding(stationBindingSource,
                                                  vm => vm.Entity,
                                                  vm => vm.Update());
            }
        }

        private void stationBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            Debug.Assert(stationBindingSource.DataSource != null);
            var fluent = mvvmContext1.OfType<StationViewModel>();
            fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.LookupRegions.Entities);
            fluent.SetBinding(tomCheckEdit,
                              check => check.EditValue,
                              vm => vm.IsTom);
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
