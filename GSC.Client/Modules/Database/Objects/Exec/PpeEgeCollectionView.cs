﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Exec;
using GSC.Model;

namespace GSC.Client.Modules.Database.Objects.Exec
{
    public partial class PpeEgeCollectionView : CollectionBlockView
    {
        public PpeEgeCollectionView()
        {
            InitializeComponent();
            booleanTypeInplaceCombo.Items.AddEnum<BooleanType>();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<PpeEgeCollectionViewModel>();
            fluent.SetBinding(gridControl1,
                              c => c.DataSource,
                              vm => vm.Entities);

            fluent.InitInstantFeedbackGridView(gridView1, ViewModel);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private PpeEgeCollectionViewModel ViewModel => mvvmContext1.GetViewModel<PpeEgeCollectionViewModel>();
    }
}
