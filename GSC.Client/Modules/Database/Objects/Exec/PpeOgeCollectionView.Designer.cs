﻿namespace GSC.Client.Modules.Database.Objects.Exec
{
    partial class PpeOgeCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImport = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpPpeOge = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExamDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParticipants = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRooms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoomsVideo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.booleanTypeInplaceCombo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colTom = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.booleanTypeInplaceCombo)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.bbiImport,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpPpeOge});
            this.ribbonControl1.Size = new System.Drawing.Size(656, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "Import", this.bbiImport),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Exec.PpeOgeCollectionViewModel);
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 2;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 3;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 4;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiImport
            // 
            this.bbiImport.Caption = "Импорт из файла";
            this.bbiImport.Id = 6;
            this.bbiImport.ImageUri.Uri = "Apply";
            this.bbiImport.Name = "bbiImport";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 7;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpPpeOge
            // 
            this.rpPpeOge.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgExchange});
            this.rpPpeOge.Name = "rpPpeOge";
            this.rpPpeOge.Text = "Проведение ЕГЭ";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 8;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.ItemLinks.Add(this.bbiImport);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.booleanTypeInplaceCombo});
            this.gridControl1.Size = new System.Drawing.Size(656, 332);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRegion,
            this.colExamDate,
            this.colPeriodType,
            this.colSubject,
            this.colCode,
            this.colName,
            this.colAddress,
            this.colParticipants,
            this.colRooms,
            this.colRoomsVideo,
            this.colKim,
            this.colTom});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Station.Region.Name";
            this.colRegion.Name = "colRegion";
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 3;
            this.colRegion.Width = 100;
            // 
            // colExamDate
            // 
            this.colExamDate.Caption = "Дата экзамена";
            this.colExamDate.DisplayFormat.FormatString = "d";
            this.colExamDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExamDate.FieldName = "Exam.ExamDate";
            this.colExamDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            this.colExamDate.Name = "colExamDate";
            this.colExamDate.Visible = true;
            this.colExamDate.VisibleIndex = 0;
            this.colExamDate.Width = 100;
            // 
            // colPeriodType
            // 
            this.colPeriodType.Caption = "Этап";
            this.colPeriodType.FieldName = "Exam.PeriodType";
            this.colPeriodType.Name = "colPeriodType";
            this.colPeriodType.Visible = true;
            this.colPeriodType.VisibleIndex = 2;
            this.colPeriodType.Width = 100;
            // 
            // colSubject
            // 
            this.colSubject.Caption = "Предмет";
            this.colSubject.FieldName = "Exam.Subject.Name";
            this.colSubject.Name = "colSubject";
            this.colSubject.Visible = true;
            this.colSubject.VisibleIndex = 1;
            this.colSubject.Width = 100;
            // 
            // colCode
            // 
            this.colCode.Caption = "Код ППЭ";
            this.colCode.FieldName = "Station.Code";
            this.colCode.Name = "colCode";
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 4;
            this.colCode.Width = 100;
            // 
            // colName
            // 
            this.colName.Caption = "Название ППЭ";
            this.colName.FieldName = "Station.Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 5;
            this.colName.Width = 100;
            // 
            // colAddress
            // 
            this.colAddress.Caption = "Адрес ППЭ";
            this.colAddress.FieldName = "Station.Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 6;
            this.colAddress.Width = 100;
            // 
            // colParticipants
            // 
            this.colParticipants.Caption = "Участников";
            this.colParticipants.FieldName = "CountPart";
            this.colParticipants.Name = "colParticipants";
            this.colParticipants.Visible = true;
            this.colParticipants.VisibleIndex = 7;
            this.colParticipants.Width = 100;
            // 
            // colRooms
            // 
            this.colRooms.Caption = "Аудиторий";
            this.colRooms.FieldName = "CountRoom";
            this.colRooms.Name = "colRooms";
            this.colRooms.Visible = true;
            this.colRooms.VisibleIndex = 8;
            this.colRooms.Width = 100;
            // 
            // colRoomsVideo
            // 
            this.colRoomsVideo.Caption = "Аудиторий с видеонаблюдением";
            this.colRoomsVideo.FieldName = "CountRoomVideo";
            this.colRoomsVideo.Name = "colRoomsVideo";
            this.colRoomsVideo.Visible = true;
            this.colRoomsVideo.VisibleIndex = 9;
            this.colRoomsVideo.Width = 100;
            // 
            // colKim
            // 
            this.colKim.Caption = "Печать КИМ";
            this.colKim.ColumnEdit = this.booleanTypeInplaceCombo;
            this.colKim.FieldName = "Kim";
            this.colKim.Name = "colKim";
            this.colKim.Visible = true;
            this.colKim.VisibleIndex = 10;
            // 
            // booleanTypeInplaceCombo
            // 
            this.booleanTypeInplaceCombo.AutoHeight = false;
            this.booleanTypeInplaceCombo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.booleanTypeInplaceCombo.Name = "booleanTypeInplaceCombo";
            // 
            // colTom
            // 
            this.colTom.Caption = "ТОМ";
            this.colTom.ColumnEdit = this.booleanTypeInplaceCombo;
            this.colTom.FieldName = "Station.Tom";
            this.colTom.Name = "colTom";
            this.colTom.Visible = true;
            this.colTom.VisibleIndex = 11;
            // 
            // PpeOgeCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "PpeOgeCollectionView";
            this.Size = new System.Drawing.Size(656, 473);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.booleanTypeInplaceCombo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonPage rpPpeOge;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.BarButtonItem bbiImport;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraGrid.Columns.GridColumn colExamDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSubject;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colParticipants;
        private DevExpress.XtraGrid.Columns.GridColumn colRooms;
        private DevExpress.XtraGrid.Columns.GridColumn colRoomsVideo;
        private DevExpress.XtraGrid.Columns.GridColumn colKim;
        private DevExpress.XtraGrid.Columns.GridColumn colTom;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox booleanTypeInplaceCombo;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodType;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
