﻿namespace GSC.Client.Modules.Database.Objects.Exec
{
    partial class ExecView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.KimImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ppeEgeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SubjectLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.regionLookup = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ParticipantsTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RoomsTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RoomsVideoTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.examLookup = new DevExpress.XtraEditors.LookUpEdit();
            this.examBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stationLookup = new DevExpress.XtraEditors.LookUpEdit();
            this.stationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ItemForSubject = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForParticipants = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRooms = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoomsVideo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKim = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExamId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStationId = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KimImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppeEgeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionLookup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticipantsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsVideoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examLookup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationLookup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForParticipants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRooms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoomsVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStationId)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(528, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.ExecViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.ExecViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.ExecViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.ExecViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exec.ExecViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Exec.ExecViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.KimImageComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.SubjectLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.regionLookup);
            this.dataLayoutControl1.Controls.Add(this.ParticipantsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RoomsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RoomsVideoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.examLookup);
            this.dataLayoutControl1.Controls.Add(this.stationLookup);
            this.dataLayoutControl1.DataSource = this.ppeEgeBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubject});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(821, 153, 373, 607);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(528, 322);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // KimImageComboBoxEdit
            // 
            this.KimImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "Kim", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.KimImageComboBoxEdit.Location = new System.Drawing.Point(249, 156);
            this.KimImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.KimImageComboBoxEdit.Name = "KimImageComboBoxEdit";
            this.KimImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.KimImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.KimImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.KimImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.KimImageComboBoxEdit.Size = new System.Drawing.Size(267, 20);
            this.KimImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.KimImageComboBoxEdit.TabIndex = 11;
            // 
            // ppeEgeBindingSource
            // 
            this.ppeEgeBindingSource.DataSource = typeof(GSC.Model.Exec);
            this.ppeEgeBindingSource.DataSourceChanged += new System.EventHandler(this.ppeEgeBindingSource_DataSourceChanged);
            // 
            // SubjectLookUpEdit
            // 
            this.SubjectLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "Subject", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SubjectLookUpEdit.Location = new System.Drawing.Point(249, 181);
            this.SubjectLookUpEdit.MenuManager = this.ribbonControl1;
            this.SubjectLookUpEdit.Name = "SubjectLookUpEdit";
            this.SubjectLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubjectLookUpEdit.Properties.NullText = "";
            this.SubjectLookUpEdit.Size = new System.Drawing.Size(368, 20);
            this.SubjectLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubjectLookUpEdit.TabIndex = 14;
            // 
            // regionLookup
            // 
            this.regionLookup.Location = new System.Drawing.Point(249, 12);
            this.regionLookup.MenuManager = this.ribbonControl1;
            this.regionLookup.Name = "regionLookup";
            this.regionLookup.Properties.Appearance.Options.UseTextOptions = true;
            this.regionLookup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.regionLookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.regionLookup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.regionLookup.Properties.DataSource = this.regionBindingSource;
            this.regionLookup.Properties.DisplayMember = "Name";
            this.regionLookup.Properties.NullText = "";
            this.regionLookup.Properties.ValueMember = "Id";
            this.regionLookup.Size = new System.Drawing.Size(267, 20);
            this.regionLookup.StyleController = this.dataLayoutControl1;
            this.regionLookup.TabIndex = 15;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // ParticipantsTextEdit
            // 
            this.ParticipantsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "CountPart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ParticipantsTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ParticipantsTextEdit.Location = new System.Drawing.Point(249, 84);
            this.ParticipantsTextEdit.MaximumSize = new System.Drawing.Size(0, 20);
            this.ParticipantsTextEdit.MenuManager = this.ribbonControl1;
            this.ParticipantsTextEdit.Name = "ParticipantsTextEdit";
            this.ParticipantsTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ParticipantsTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ParticipantsTextEdit.Properties.AutoHeight = false;
            this.ParticipantsTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ParticipantsTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.ParticipantsTextEdit.Properties.IsFloatValue = false;
            this.ParticipantsTextEdit.Properties.Mask.EditMask = "N00";
            this.ParticipantsTextEdit.Properties.MaxValue = new decimal(new int[] {
            100500,
            0,
            0,
            0});
            this.ParticipantsTextEdit.Size = new System.Drawing.Size(267, 20);
            this.ParticipantsTextEdit.StyleController = this.dataLayoutControl1;
            this.ParticipantsTextEdit.TabIndex = 8;
            // 
            // RoomsTextEdit
            // 
            this.RoomsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "CountRoom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RoomsTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RoomsTextEdit.Location = new System.Drawing.Point(249, 108);
            this.RoomsTextEdit.MenuManager = this.ribbonControl1;
            this.RoomsTextEdit.Name = "RoomsTextEdit";
            this.RoomsTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.RoomsTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RoomsTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RoomsTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoomsTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.RoomsTextEdit.Properties.IsFloatValue = false;
            this.RoomsTextEdit.Properties.Mask.EditMask = "N0";
            this.RoomsTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RoomsTextEdit.Properties.MaxValue = new decimal(new int[] {
            100500,
            0,
            0,
            0});
            this.RoomsTextEdit.Size = new System.Drawing.Size(267, 20);
            this.RoomsTextEdit.StyleController = this.dataLayoutControl1;
            this.RoomsTextEdit.TabIndex = 9;
            // 
            // RoomsVideoTextEdit
            // 
            this.RoomsVideoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "CountRoomVideo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RoomsVideoTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RoomsVideoTextEdit.Location = new System.Drawing.Point(249, 132);
            this.RoomsVideoTextEdit.MenuManager = this.ribbonControl1;
            this.RoomsVideoTextEdit.Name = "RoomsVideoTextEdit";
            this.RoomsVideoTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.RoomsVideoTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RoomsVideoTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RoomsVideoTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoomsVideoTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.RoomsVideoTextEdit.Properties.IsFloatValue = false;
            this.RoomsVideoTextEdit.Properties.Mask.EditMask = "N0";
            this.RoomsVideoTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RoomsVideoTextEdit.Properties.MaxValue = new decimal(new int[] {
            100500,
            0,
            0,
            0});
            this.RoomsVideoTextEdit.Size = new System.Drawing.Size(267, 20);
            this.RoomsVideoTextEdit.StyleController = this.dataLayoutControl1;
            this.RoomsVideoTextEdit.TabIndex = 10;
            // 
            // examLookup
            // 
            this.examLookup.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ppeEgeBindingSource, "ExamId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.examLookup.Location = new System.Drawing.Point(249, 36);
            this.examLookup.MenuManager = this.ribbonControl1;
            this.examLookup.Name = "examLookup";
            this.examLookup.Properties.Appearance.Options.UseTextOptions = true;
            this.examLookup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.examLookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.examLookup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExamDate", "Дата", 62, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Subject.Name", "Название", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PeriodType", "Период", 67, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExamType", "Тип", 63, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.examLookup.Properties.DataSource = this.examBindingSource;
            this.examLookup.Properties.DisplayMember = "DisplayText";
            this.examLookup.Properties.NullText = "";
            this.examLookup.Properties.ValueMember = "Id";
            this.examLookup.Size = new System.Drawing.Size(267, 20);
            this.examLookup.StyleController = this.dataLayoutControl1;
            this.examLookup.TabIndex = 18;
            // 
            // examBindingSource
            // 
            this.examBindingSource.DataSource = typeof(GSC.Model.Exam);
            // 
            // stationLookup
            // 
            this.stationLookup.Location = new System.Drawing.Point(249, 60);
            this.stationLookup.MenuManager = this.ribbonControl1;
            this.stationLookup.Name = "stationLookup";
            this.stationLookup.Properties.Appearance.Options.UseTextOptions = true;
            this.stationLookup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.stationLookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stationLookup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RegionName", "Субъект РФ", 73, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Адрес", 49, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tom", "ТОМ", 30, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.stationLookup.Properties.DataSource = this.stationBindingSource;
            this.stationLookup.Properties.DisplayMember = "Address";
            this.stationLookup.Properties.NullText = "";
            this.stationLookup.Properties.ValueMember = "Id";
            this.stationLookup.Size = new System.Drawing.Size(267, 20);
            this.stationLookup.StyleController = this.dataLayoutControl1;
            this.stationLookup.TabIndex = 17;
            // 
            // stationBindingSource
            // 
            this.stationBindingSource.DataSource = typeof(GSC.Model.Station);
            // 
            // ItemForSubject
            // 
            this.ItemForSubject.Control = this.SubjectLookUpEdit;
            this.ItemForSubject.Location = new System.Drawing.Point(0, 169);
            this.ItemForSubject.Name = "ItemForSubject";
            this.ItemForSubject.Size = new System.Drawing.Size(609, 24);
            this.ItemForSubject.Text = "Предмет:";
            this.ItemForSubject.TextSize = new System.Drawing.Size(234, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(528, 322);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForParticipants,
            this.ItemForRooms,
            this.ItemForRoomsVideo,
            this.ItemForKim,
            this.ItemForRegionId,
            this.ItemForExamId,
            this.ItemForStationId});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(508, 302);
            // 
            // ItemForParticipants
            // 
            this.ItemForParticipants.Control = this.ParticipantsTextEdit;
            this.ItemForParticipants.CustomizationFormText = "Количество участников";
            this.ItemForParticipants.Location = new System.Drawing.Point(0, 72);
            this.ItemForParticipants.Name = "ItemForParticipants";
            this.ItemForParticipants.Size = new System.Drawing.Size(508, 24);
            this.ItemForParticipants.Text = "Количество участников:";
            this.ItemForParticipants.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForRooms
            // 
            this.ItemForRooms.Control = this.RoomsTextEdit;
            this.ItemForRooms.CustomizationFormText = "Количество аудиторий";
            this.ItemForRooms.Location = new System.Drawing.Point(0, 96);
            this.ItemForRooms.Name = "ItemForRooms";
            this.ItemForRooms.Size = new System.Drawing.Size(508, 24);
            this.ItemForRooms.Text = "Количество аудиторий:";
            this.ItemForRooms.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForRoomsVideo
            // 
            this.ItemForRoomsVideo.Control = this.RoomsVideoTextEdit;
            this.ItemForRoomsVideo.CustomizationFormText = "Количество аудиторий с видеонаблюдением";
            this.ItemForRoomsVideo.Location = new System.Drawing.Point(0, 120);
            this.ItemForRoomsVideo.Name = "ItemForRoomsVideo";
            this.ItemForRoomsVideo.Size = new System.Drawing.Size(508, 24);
            this.ItemForRoomsVideo.Text = "Количество аудиторий с видеонаблюдением:";
            this.ItemForRoomsVideo.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForKim
            // 
            this.ItemForKim.Control = this.KimImageComboBoxEdit;
            this.ItemForKim.CustomizationFormText = "Печать КИМ";
            this.ItemForKim.Location = new System.Drawing.Point(0, 144);
            this.ItemForKim.Name = "ItemForKim";
            this.ItemForKim.Size = new System.Drawing.Size(508, 158);
            this.ItemForKim.Text = "Печать КИМ:";
            this.ItemForKim.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForRegionId
            // 
            this.ItemForRegionId.Control = this.regionLookup;
            this.ItemForRegionId.CustomizationFormText = "Регион";
            this.ItemForRegionId.Location = new System.Drawing.Point(0, 0);
            this.ItemForRegionId.Name = "ItemForRegionId";
            this.ItemForRegionId.Size = new System.Drawing.Size(508, 24);
            this.ItemForRegionId.Text = "Регион:";
            this.ItemForRegionId.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForExamId
            // 
            this.ItemForExamId.Control = this.examLookup;
            this.ItemForExamId.CustomizationFormText = "Расписание";
            this.ItemForExamId.Location = new System.Drawing.Point(0, 24);
            this.ItemForExamId.Name = "ItemForExamId";
            this.ItemForExamId.Size = new System.Drawing.Size(508, 24);
            this.ItemForExamId.Text = "Расписание:";
            this.ItemForExamId.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ItemForStationId
            // 
            this.ItemForStationId.Control = this.stationLookup;
            this.ItemForStationId.CustomizationFormText = "ППЭ";
            this.ItemForStationId.Location = new System.Drawing.Point(0, 48);
            this.ItemForStationId.Name = "ItemForStationId";
            this.ItemForStationId.Size = new System.Drawing.Size(508, 24);
            this.ItemForStationId.Text = "ППЭ:";
            this.ItemForStationId.TextSize = new System.Drawing.Size(234, 13);
            // 
            // ExecView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "ExecView";
            this.Size = new System.Drawing.Size(528, 463);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KimImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppeEgeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionLookup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticipantsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsVideoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examLookup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationLookup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForParticipants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRooms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoomsVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStationId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource ppeEgeBindingSource;
        private DevExpress.XtraEditors.ImageComboBoxEdit KimImageComboBoxEdit;
        private DevExpress.XtraEditors.LookUpEdit SubjectLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForParticipants;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRooms;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoomsVideo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKim;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubject;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private DevExpress.XtraEditors.LookUpEdit regionLookup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionId;
        private DevExpress.XtraEditors.SpinEdit ParticipantsTextEdit;
        private DevExpress.XtraEditors.SpinEdit RoomsTextEdit;
        private DevExpress.XtraEditors.SpinEdit RoomsVideoTextEdit;
        private DevExpress.XtraEditors.LookUpEdit examLookup;
        private DevExpress.XtraEditors.LookUpEdit stationLookup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExamId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStationId;
        private System.Windows.Forms.BindingSource examBindingSource;
        private System.Windows.Forms.BindingSource stationBindingSource;
    }
}
