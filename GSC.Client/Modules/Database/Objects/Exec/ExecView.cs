﻿using System.Diagnostics;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels.Exec;

namespace GSC.Client.Modules.Database.Objects.Exec
{
    public partial class ExecView : BaseEditView
    {
        public ExecView()
        {
            InitializeComponent();
            KimImageComboBoxEdit.Properties.Items.AddEnum<Model.BooleanType>();

            if (!DesignMode)
            {
                var fluent = mvvmContext1.OfType<ExecViewModel>();
                fluent.SetObjectDataSourceBinding(ppeEgeBindingSource,
                                                  vm => vm.Entity,
                                                  vm => vm.Update());
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExecViewModel>();
            fluent.SetBinding(regionLookup, e => e.EditValue, vm => vm.RegionId);
            fluent.SetBinding(stationLookup, e => e.EditValue, vm => vm.StationId);
            fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.LookupRegions.Entities);
            fluent.SetBinding(examBindingSource, bs => bs.DataSource, vm => vm.LookupExams.Entities);
            fluent.SetBinding(stationBindingSource, bs => bs.DataSource, vm => vm.LookupStations.Entities);
            //fluent.SetBinding(regionLookup, e => e.EditValue, vm => vm.RegionId);
            //fluent.SetBinding(stationLookup, e => e.EditValue, vm => vm.StationId);
        }

        private void ppeEgeBindingSource_DataSourceChanged(object sender, System.EventArgs e)
        {
            Debug.Assert(ppeEgeBindingSource.DataSource != null);

            InitBindings();
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
