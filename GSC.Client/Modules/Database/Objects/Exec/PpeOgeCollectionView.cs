﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Exec;

namespace GSC.Client.Modules.Database.Objects.Exec
{
    public partial class PpeOgeCollectionView : CollectionBlockView
    {
        public PpeOgeCollectionView()
        {
            InitializeComponent();
            booleanTypeInplaceCombo.Items.AddEnum<Model.BooleanType>();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<PpeOgeCollectionViewModel>();
            fluent.SetBinding(gridControl1,
                              c => c.DataSource,
                              vm => vm.Entities);

            fluent.InitInstantFeedbackGridView(gridView1, ViewModel);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private PpeOgeCollectionViewModel ViewModel => mvvmContext1.GetViewModel<PpeOgeCollectionViewModel>();
    }
}
