﻿namespace GSC.Client.Modules.Database.Objects.OivGek
{
    partial class OivGekView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.oivGekBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ribbonControl9 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem37 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem38 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem39 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem40 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage8 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup15 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup16 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl10 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem41 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem42 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem43 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem44 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem45 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage9 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup17 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup18 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl11 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem46 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem47 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem48 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem49 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem50 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage10 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup19 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup20 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl12 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem51 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem52 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem53 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem54 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem55 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage11 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup21 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup22 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl5 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl6 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl7 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem28 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem29 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem30 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl8 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem31 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage7 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup14 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl2 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl3 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonControl4 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.nadzorContactControl = new GSC.Client.Modules.Database.Objects.OivGek.OivGekContactControl();
            this.gekContactControl = new GSC.Client.Modules.Database.Objects.OivGek.OivGekContactControl();
            this.giaContactControl = new GSC.Client.Modules.Database.Objects.OivGek.OivGekContactControl();
            this.oivContactControl = new GSC.Client.Modules.Database.Objects.OivGek.OivGekContactControl();
            this.RegionIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oivGekBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.OivGek.OivGekViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(933, 141);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // oivGekBindingSource
            // 
            this.oivGekBindingSource.DataSource = typeof(GSC.Model.OivGek);
            // 
            // ribbonControl9
            // 
            this.ribbonControl9.ExpandCollapseItem.Id = 0;
            this.ribbonControl9.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl9.ExpandCollapseItem,
            this.barButtonItem36,
            this.barButtonItem37,
            this.barButtonItem38,
            this.barButtonItem39,
            this.barButtonItem40});
            this.ribbonControl9.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl9.MaxItemId = 9;
            this.ribbonControl9.Name = "ribbonControl9";
            this.ribbonControl9.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage8});
            this.ribbonControl9.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem36
            // 
            this.barButtonItem36.Caption = "Сохранить";
            this.barButtonItem36.Id = 1;
            this.barButtonItem36.ImageUri.Uri = "Save";
            this.barButtonItem36.Name = "barButtonItem36";
            // 
            // barButtonItem37
            // 
            this.barButtonItem37.Caption = "Сохранить и закрыть";
            this.barButtonItem37.Id = 2;
            this.barButtonItem37.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem37.Name = "barButtonItem37";
            // 
            // barButtonItem38
            // 
            this.barButtonItem38.Caption = "Сохранить и создать";
            this.barButtonItem38.Id = 3;
            this.barButtonItem38.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem38.Name = "barButtonItem38";
            // 
            // barButtonItem39
            // 
            this.barButtonItem39.Caption = "Отменить изменения";
            this.barButtonItem39.Id = 4;
            this.barButtonItem39.ImageUri.Uri = "Reset";
            this.barButtonItem39.Name = "barButtonItem39";
            // 
            // barButtonItem40
            // 
            this.barButtonItem40.Caption = "Закрыть";
            this.barButtonItem40.Id = 8;
            this.barButtonItem40.ImageUri.Uri = "Close";
            this.barButtonItem40.Name = "barButtonItem40";
            // 
            // ribbonPage8
            // 
            this.ribbonPage8.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup15,
            this.ribbonPageGroup16});
            this.ribbonPage8.Name = "ribbonPage8";
            this.ribbonPage8.Text = "rpActions";
            // 
            // ribbonPageGroup15
            // 
            this.ribbonPageGroup15.ItemLinks.Add(this.barButtonItem36);
            this.ribbonPageGroup15.ItemLinks.Add(this.barButtonItem37);
            this.ribbonPageGroup15.ItemLinks.Add(this.barButtonItem38);
            this.ribbonPageGroup15.Name = "ribbonPageGroup15";
            this.ribbonPageGroup15.Text = "Сохранение";
            // 
            // ribbonPageGroup16
            // 
            this.ribbonPageGroup16.ItemLinks.Add(this.barButtonItem39);
            this.ribbonPageGroup16.ItemLinks.Add(this.barButtonItem40);
            this.ribbonPageGroup16.Name = "ribbonPageGroup16";
            this.ribbonPageGroup16.Text = "Отмена";
            // 
            // ribbonControl10
            // 
            this.ribbonControl10.ExpandCollapseItem.Id = 0;
            this.ribbonControl10.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl10.ExpandCollapseItem,
            this.barButtonItem41,
            this.barButtonItem42,
            this.barButtonItem43,
            this.barButtonItem44,
            this.barButtonItem45});
            this.ribbonControl10.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl10.MaxItemId = 9;
            this.ribbonControl10.Name = "ribbonControl10";
            this.ribbonControl10.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage9});
            this.ribbonControl10.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem41
            // 
            this.barButtonItem41.Caption = "Сохранить";
            this.barButtonItem41.Id = 1;
            this.barButtonItem41.ImageUri.Uri = "Save";
            this.barButtonItem41.Name = "barButtonItem41";
            // 
            // barButtonItem42
            // 
            this.barButtonItem42.Caption = "Сохранить и закрыть";
            this.barButtonItem42.Id = 2;
            this.barButtonItem42.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem42.Name = "barButtonItem42";
            // 
            // barButtonItem43
            // 
            this.barButtonItem43.Caption = "Сохранить и создать";
            this.barButtonItem43.Id = 3;
            this.barButtonItem43.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem43.Name = "barButtonItem43";
            // 
            // barButtonItem44
            // 
            this.barButtonItem44.Caption = "Отменить изменения";
            this.barButtonItem44.Id = 4;
            this.barButtonItem44.ImageUri.Uri = "Reset";
            this.barButtonItem44.Name = "barButtonItem44";
            // 
            // barButtonItem45
            // 
            this.barButtonItem45.Caption = "Закрыть";
            this.barButtonItem45.Id = 8;
            this.barButtonItem45.ImageUri.Uri = "Close";
            this.barButtonItem45.Name = "barButtonItem45";
            // 
            // ribbonPage9
            // 
            this.ribbonPage9.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup17,
            this.ribbonPageGroup18});
            this.ribbonPage9.Name = "ribbonPage9";
            this.ribbonPage9.Text = "rpActions";
            // 
            // ribbonPageGroup17
            // 
            this.ribbonPageGroup17.ItemLinks.Add(this.barButtonItem41);
            this.ribbonPageGroup17.ItemLinks.Add(this.barButtonItem42);
            this.ribbonPageGroup17.ItemLinks.Add(this.barButtonItem43);
            this.ribbonPageGroup17.Name = "ribbonPageGroup17";
            this.ribbonPageGroup17.Text = "Сохранение";
            // 
            // ribbonPageGroup18
            // 
            this.ribbonPageGroup18.ItemLinks.Add(this.barButtonItem44);
            this.ribbonPageGroup18.ItemLinks.Add(this.barButtonItem45);
            this.ribbonPageGroup18.Name = "ribbonPageGroup18";
            this.ribbonPageGroup18.Text = "Отмена";
            // 
            // ribbonControl11
            // 
            this.ribbonControl11.ExpandCollapseItem.Id = 0;
            this.ribbonControl11.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl11.ExpandCollapseItem,
            this.barButtonItem46,
            this.barButtonItem47,
            this.barButtonItem48,
            this.barButtonItem49,
            this.barButtonItem50});
            this.ribbonControl11.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl11.MaxItemId = 9;
            this.ribbonControl11.Name = "ribbonControl11";
            this.ribbonControl11.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage10});
            this.ribbonControl11.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem46
            // 
            this.barButtonItem46.Caption = "Сохранить";
            this.barButtonItem46.Id = 1;
            this.barButtonItem46.ImageUri.Uri = "Save";
            this.barButtonItem46.Name = "barButtonItem46";
            // 
            // barButtonItem47
            // 
            this.barButtonItem47.Caption = "Сохранить и закрыть";
            this.barButtonItem47.Id = 2;
            this.barButtonItem47.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem47.Name = "barButtonItem47";
            // 
            // barButtonItem48
            // 
            this.barButtonItem48.Caption = "Сохранить и создать";
            this.barButtonItem48.Id = 3;
            this.barButtonItem48.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem48.Name = "barButtonItem48";
            // 
            // barButtonItem49
            // 
            this.barButtonItem49.Caption = "Отменить изменения";
            this.barButtonItem49.Id = 4;
            this.barButtonItem49.ImageUri.Uri = "Reset";
            this.barButtonItem49.Name = "barButtonItem49";
            // 
            // barButtonItem50
            // 
            this.barButtonItem50.Caption = "Закрыть";
            this.barButtonItem50.Id = 8;
            this.barButtonItem50.ImageUri.Uri = "Close";
            this.barButtonItem50.Name = "barButtonItem50";
            // 
            // ribbonPage10
            // 
            this.ribbonPage10.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup19,
            this.ribbonPageGroup20});
            this.ribbonPage10.Name = "ribbonPage10";
            this.ribbonPage10.Text = "rpActions";
            // 
            // ribbonPageGroup19
            // 
            this.ribbonPageGroup19.ItemLinks.Add(this.barButtonItem46);
            this.ribbonPageGroup19.ItemLinks.Add(this.barButtonItem47);
            this.ribbonPageGroup19.ItemLinks.Add(this.barButtonItem48);
            this.ribbonPageGroup19.Name = "ribbonPageGroup19";
            this.ribbonPageGroup19.Text = "Сохранение";
            // 
            // ribbonPageGroup20
            // 
            this.ribbonPageGroup20.ItemLinks.Add(this.barButtonItem49);
            this.ribbonPageGroup20.ItemLinks.Add(this.barButtonItem50);
            this.ribbonPageGroup20.Name = "ribbonPageGroup20";
            this.ribbonPageGroup20.Text = "Отмена";
            // 
            // ribbonControl12
            // 
            this.ribbonControl12.ExpandCollapseItem.Id = 0;
            this.ribbonControl12.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl12.ExpandCollapseItem,
            this.barButtonItem51,
            this.barButtonItem52,
            this.barButtonItem53,
            this.barButtonItem54,
            this.barButtonItem55});
            this.ribbonControl12.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl12.MaxItemId = 9;
            this.ribbonControl12.Name = "ribbonControl12";
            this.ribbonControl12.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage11});
            this.ribbonControl12.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem51
            // 
            this.barButtonItem51.Caption = "Сохранить";
            this.barButtonItem51.Id = 1;
            this.barButtonItem51.ImageUri.Uri = "Save";
            this.barButtonItem51.Name = "barButtonItem51";
            // 
            // barButtonItem52
            // 
            this.barButtonItem52.Caption = "Сохранить и закрыть";
            this.barButtonItem52.Id = 2;
            this.barButtonItem52.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem52.Name = "barButtonItem52";
            // 
            // barButtonItem53
            // 
            this.barButtonItem53.Caption = "Сохранить и создать";
            this.barButtonItem53.Id = 3;
            this.barButtonItem53.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem53.Name = "barButtonItem53";
            // 
            // barButtonItem54
            // 
            this.barButtonItem54.Caption = "Отменить изменения";
            this.barButtonItem54.Id = 4;
            this.barButtonItem54.ImageUri.Uri = "Reset";
            this.barButtonItem54.Name = "barButtonItem54";
            // 
            // barButtonItem55
            // 
            this.barButtonItem55.Caption = "Закрыть";
            this.barButtonItem55.Id = 8;
            this.barButtonItem55.ImageUri.Uri = "Close";
            this.barButtonItem55.Name = "barButtonItem55";
            // 
            // ribbonPage11
            // 
            this.ribbonPage11.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup21,
            this.ribbonPageGroup22});
            this.ribbonPage11.Name = "ribbonPage11";
            this.ribbonPage11.Text = "rpActions";
            // 
            // ribbonPageGroup21
            // 
            this.ribbonPageGroup21.ItemLinks.Add(this.barButtonItem51);
            this.ribbonPageGroup21.ItemLinks.Add(this.barButtonItem52);
            this.ribbonPageGroup21.ItemLinks.Add(this.barButtonItem53);
            this.ribbonPageGroup21.Name = "ribbonPageGroup21";
            this.ribbonPageGroup21.Text = "Сохранение";
            // 
            // ribbonPageGroup22
            // 
            this.ribbonPageGroup22.ItemLinks.Add(this.barButtonItem54);
            this.ribbonPageGroup22.ItemLinks.Add(this.barButtonItem55);
            this.ribbonPageGroup22.Name = "ribbonPageGroup22";
            this.ribbonPageGroup22.Text = "Отмена";
            // 
            // ribbonControl5
            // 
            this.ribbonControl5.ExpandCollapseItem.Id = 0;
            this.ribbonControl5.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl5.ExpandCollapseItem,
            this.barButtonItem16,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barButtonItem20});
            this.ribbonControl5.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl5.MaxItemId = 9;
            this.ribbonControl5.Name = "ribbonControl5";
            this.ribbonControl5.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage4});
            this.ribbonControl5.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "Сохранить";
            this.barButtonItem16.Id = 1;
            this.barButtonItem16.ImageUri.Uri = "Save";
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "Сохранить и закрыть";
            this.barButtonItem17.Id = 2;
            this.barButtonItem17.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "Сохранить и создать";
            this.barButtonItem18.Id = 3;
            this.barButtonItem18.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "Отменить изменения";
            this.barButtonItem19.Id = 4;
            this.barButtonItem19.ImageUri.Uri = "Reset";
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "Закрыть";
            this.barButtonItem20.Id = 8;
            this.barButtonItem20.ImageUri.Uri = "Close";
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7,
            this.ribbonPageGroup8});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "rpActions";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem16);
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem17);
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem18);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Сохранение";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem19);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem20);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Отмена";
            // 
            // ribbonControl6
            // 
            this.ribbonControl6.ExpandCollapseItem.Id = 0;
            this.ribbonControl6.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl6.ExpandCollapseItem,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25});
            this.ribbonControl6.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl6.MaxItemId = 9;
            this.ribbonControl6.Name = "ribbonControl6";
            this.ribbonControl6.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage5});
            this.ribbonControl6.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "Сохранить";
            this.barButtonItem21.Id = 1;
            this.barButtonItem21.ImageUri.Uri = "Save";
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Сохранить и закрыть";
            this.barButtonItem22.Id = 2;
            this.barButtonItem22.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Сохранить и создать";
            this.barButtonItem23.Id = 3;
            this.barButtonItem23.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "Отменить изменения";
            this.barButtonItem24.Id = 4;
            this.barButtonItem24.ImageUri.Uri = "Reset";
            this.barButtonItem24.Name = "barButtonItem24";
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "Закрыть";
            this.barButtonItem25.Id = 8;
            this.barButtonItem25.ImageUri.Uri = "Close";
            this.barButtonItem25.Name = "barButtonItem25";
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup9,
            this.ribbonPageGroup10});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "rpActions";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem21);
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem22);
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem23);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Сохранение";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.barButtonItem24);
            this.ribbonPageGroup10.ItemLinks.Add(this.barButtonItem25);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Отмена";
            // 
            // ribbonControl7
            // 
            this.ribbonControl7.ExpandCollapseItem.Id = 0;
            this.ribbonControl7.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl7.ExpandCollapseItem,
            this.barButtonItem26,
            this.barButtonItem27,
            this.barButtonItem28,
            this.barButtonItem29,
            this.barButtonItem30});
            this.ribbonControl7.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl7.MaxItemId = 9;
            this.ribbonControl7.Name = "ribbonControl7";
            this.ribbonControl7.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage6});
            this.ribbonControl7.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "Сохранить";
            this.barButtonItem26.Id = 1;
            this.barButtonItem26.ImageUri.Uri = "Save";
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // barButtonItem27
            // 
            this.barButtonItem27.Caption = "Сохранить и закрыть";
            this.barButtonItem27.Id = 2;
            this.barButtonItem27.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem27.Name = "barButtonItem27";
            // 
            // barButtonItem28
            // 
            this.barButtonItem28.Caption = "Сохранить и создать";
            this.barButtonItem28.Id = 3;
            this.barButtonItem28.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem28.Name = "barButtonItem28";
            // 
            // barButtonItem29
            // 
            this.barButtonItem29.Caption = "Отменить изменения";
            this.barButtonItem29.Id = 4;
            this.barButtonItem29.ImageUri.Uri = "Reset";
            this.barButtonItem29.Name = "barButtonItem29";
            // 
            // barButtonItem30
            // 
            this.barButtonItem30.Caption = "Закрыть";
            this.barButtonItem30.Id = 8;
            this.barButtonItem30.ImageUri.Uri = "Close";
            this.barButtonItem30.Name = "barButtonItem30";
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup11,
            this.ribbonPageGroup12});
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "rpActions";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonItem26);
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonItem27);
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonItem28);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Сохранение";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.barButtonItem29);
            this.ribbonPageGroup12.ItemLinks.Add(this.barButtonItem30);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.Text = "Отмена";
            // 
            // ribbonControl8
            // 
            this.ribbonControl8.ExpandCollapseItem.Id = 0;
            this.ribbonControl8.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl8.ExpandCollapseItem,
            this.barButtonItem31,
            this.barButtonItem32,
            this.barButtonItem33,
            this.barButtonItem34,
            this.barButtonItem35});
            this.ribbonControl8.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl8.MaxItemId = 9;
            this.ribbonControl8.Name = "ribbonControl8";
            this.ribbonControl8.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage7});
            this.ribbonControl8.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem31
            // 
            this.barButtonItem31.Caption = "Сохранить";
            this.barButtonItem31.Id = 1;
            this.barButtonItem31.ImageUri.Uri = "Save";
            this.barButtonItem31.Name = "barButtonItem31";
            // 
            // barButtonItem32
            // 
            this.barButtonItem32.Caption = "Сохранить и закрыть";
            this.barButtonItem32.Id = 2;
            this.barButtonItem32.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem32.Name = "barButtonItem32";
            // 
            // barButtonItem33
            // 
            this.barButtonItem33.Caption = "Сохранить и создать";
            this.barButtonItem33.Id = 3;
            this.barButtonItem33.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem33.Name = "barButtonItem33";
            // 
            // barButtonItem34
            // 
            this.barButtonItem34.Caption = "Отменить изменения";
            this.barButtonItem34.Id = 4;
            this.barButtonItem34.ImageUri.Uri = "Reset";
            this.barButtonItem34.Name = "barButtonItem34";
            // 
            // barButtonItem35
            // 
            this.barButtonItem35.Caption = "Закрыть";
            this.barButtonItem35.Id = 8;
            this.barButtonItem35.ImageUri.Uri = "Close";
            this.barButtonItem35.Name = "barButtonItem35";
            // 
            // ribbonPage7
            // 
            this.ribbonPage7.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup13,
            this.ribbonPageGroup14});
            this.ribbonPage7.Name = "ribbonPage7";
            this.ribbonPage7.Text = "rpActions";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.barButtonItem31);
            this.ribbonPageGroup13.ItemLinks.Add(this.barButtonItem32);
            this.ribbonPageGroup13.ItemLinks.Add(this.barButtonItem33);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.Text = "Сохранение";
            // 
            // ribbonPageGroup14
            // 
            this.ribbonPageGroup14.ItemLinks.Add(this.barButtonItem34);
            this.ribbonPageGroup14.ItemLinks.Add(this.barButtonItem35);
            this.ribbonPageGroup14.Name = "ribbonPageGroup14";
            this.ribbonPageGroup14.Text = "Отмена";
            // 
            // ribbonControl2
            // 
            this.ribbonControl2.ExpandCollapseItem.Id = 0;
            this.ribbonControl2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl2.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5});
            this.ribbonControl2.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl2.MaxItemId = 9;
            this.ribbonControl2.Name = "ribbonControl2";
            this.ribbonControl2.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl2.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Сохранить";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ImageUri.Uri = "Save";
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Сохранить и закрыть";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Сохранить и создать";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Отменить изменения";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.ImageUri.Uri = "Reset";
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Закрыть";
            this.barButtonItem5.Id = 8;
            this.barButtonItem5.ImageUri.Uri = "Close";
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "rpActions";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Сохранение";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Отмена";
            // 
            // ribbonControl3
            // 
            this.ribbonControl3.ExpandCollapseItem.Id = 0;
            this.ribbonControl3.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl3.ExpandCollapseItem,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10});
            this.ribbonControl3.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl3.MaxItemId = 9;
            this.ribbonControl3.Name = "ribbonControl3";
            this.ribbonControl3.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage2});
            this.ribbonControl3.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Сохранить";
            this.barButtonItem6.Id = 1;
            this.barButtonItem6.ImageUri.Uri = "Save";
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Сохранить и закрыть";
            this.barButtonItem7.Id = 2;
            this.barButtonItem7.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Сохранить и создать";
            this.barButtonItem8.Id = 3;
            this.barButtonItem8.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Отменить изменения";
            this.barButtonItem9.Id = 4;
            this.barButtonItem9.ImageUri.Uri = "Reset";
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Закрыть";
            this.barButtonItem10.Id = 8;
            this.barButtonItem10.ImageUri.Uri = "Close";
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "rpActions";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Сохранение";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Отмена";
            // 
            // ribbonControl4
            // 
            this.ribbonControl4.ExpandCollapseItem.Id = 0;
            this.ribbonControl4.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl4.ExpandCollapseItem,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15});
            this.ribbonControl4.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl4.MaxItemId = 9;
            this.ribbonControl4.Name = "ribbonControl4";
            this.ribbonControl4.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage3});
            this.ribbonControl4.Size = new System.Drawing.Size(663, 116);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Сохранить";
            this.barButtonItem11.Id = 1;
            this.barButtonItem11.ImageUri.Uri = "Save";
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Сохранить и закрыть";
            this.barButtonItem12.Id = 2;
            this.barButtonItem12.ImageUri.Uri = "SaveAndClose";
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "Сохранить и создать";
            this.barButtonItem13.Id = 3;
            this.barButtonItem13.ImageUri.Uri = "SaveAndNew";
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Отменить изменения";
            this.barButtonItem14.Id = 4;
            this.barButtonItem14.ImageUri.Uri = "Reset";
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Закрыть";
            this.barButtonItem15.Id = 8;
            this.barButtonItem15.ImageUri.Uri = "Close";
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "rpActions";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem11);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem12);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem13);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Сохранение";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem14);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem15);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.nadzorContactControl);
            this.dataLayoutControl1.Controls.Add(this.gekContactControl);
            this.dataLayoutControl1.Controls.Add(this.giaContactControl);
            this.dataLayoutControl1.Controls.Add(this.oivContactControl);
            this.dataLayoutControl1.Controls.Add(this.RegionIdLookUpEdit);
            this.dataLayoutControl1.DataSource = this.oivGekBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(933, 479);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // nadzorContactControl
            // 
            this.nadzorContactControl.Contact = null;
            this.nadzorContactControl.HeaderTitle = "Сотрудник обрнадзора";
            this.nadzorContactControl.Location = new System.Drawing.Point(468, 254);
            this.nadzorContactControl.Name = "nadzorContactControl";
            this.nadzorContactControl.Size = new System.Drawing.Size(453, 213);
            this.nadzorContactControl.TabIndex = 8;
            this.nadzorContactControl.ContactChanged2 += new System.EventHandler(this.ContactControl_ContactPropertyChanged);
            // 
            // gekContactControl
            // 
            this.gekContactControl.Contact = null;
            this.gekContactControl.HeaderTitle = "Председатель ГЭК";
            this.gekContactControl.Location = new System.Drawing.Point(12, 254);
            this.gekContactControl.Name = "gekContactControl";
            this.gekContactControl.Size = new System.Drawing.Size(452, 213);
            this.gekContactControl.TabIndex = 7;
            this.gekContactControl.ContactChanged2 += new System.EventHandler(this.ContactControl_ContactPropertyChanged);
            // 
            // giaContactControl
            // 
            this.giaContactControl.Contact = null;
            this.giaContactControl.HeaderTitle = "Ответственный за проведение ГИА";
            this.giaContactControl.Location = new System.Drawing.Point(468, 36);
            this.giaContactControl.Name = "giaContactControl";
            this.giaContactControl.Size = new System.Drawing.Size(453, 214);
            this.giaContactControl.TabIndex = 6;
            this.giaContactControl.ContactChanged2 += new System.EventHandler(this.ContactControl_ContactPropertyChanged);
            // 
            // oivContactControl
            // 
            this.oivContactControl.Contact = null;
            this.oivContactControl.HeaderTitle = "Руководитель ОИВ";
            this.oivContactControl.Location = new System.Drawing.Point(12, 36);
            this.oivContactControl.Name = "oivContactControl";
            this.oivContactControl.Size = new System.Drawing.Size(452, 214);
            this.oivContactControl.TabIndex = 5;
            this.oivContactControl.ContactChanged2 += new System.EventHandler(this.ContactControl_ContactPropertyChanged);
            // 
            // RegionIdLookUpEdit
            // 
            this.RegionIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.oivGekBindingSource, "RegionId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RegionIdLookUpEdit.Location = new System.Drawing.Point(54, 12);
            this.RegionIdLookUpEdit.MenuManager = this.ribbonControl1;
            this.RegionIdLookUpEdit.Name = "RegionIdLookUpEdit";
            this.RegionIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RegionIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RegionIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegionIdLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.RegionIdLookUpEdit.Properties.DataSource = this.regionBindingSource;
            this.RegionIdLookUpEdit.Properties.DisplayMember = "Name";
            this.RegionIdLookUpEdit.Properties.NullText = "";
            this.RegionIdLookUpEdit.Properties.ValueMember = "Id";
            this.RegionIdLookUpEdit.Size = new System.Drawing.Size(867, 20);
            this.RegionIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RegionIdLookUpEdit.TabIndex = 4;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 50D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 24D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 218D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 217D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(933, 479);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRegionId});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlGroup2.Size = new System.Drawing.Size(913, 24);
            // 
            // ItemForRegionId
            // 
            this.ItemForRegionId.Control = this.RegionIdLookUpEdit;
            this.ItemForRegionId.Location = new System.Drawing.Point(0, 0);
            this.ItemForRegionId.Name = "ItemForRegionId";
            this.ItemForRegionId.Size = new System.Drawing.Size(913, 24);
            this.ItemForRegionId.Text = "Регион:";
            this.ItemForRegionId.TextSize = new System.Drawing.Size(39, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.oivContactControl;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem1.Size = new System.Drawing.Size(456, 218);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.giaContactControl;
            this.layoutControlItem2.Location = new System.Drawing.Point(456, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(457, 218);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gekContactControl;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(456, 217);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.nadzorContactControl;
            this.layoutControlItem4.Location = new System.Drawing.Point(456, 242);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(457, 217);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AllowDrawBackground = false;
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "autoGeneratedGroup0";
            this.layoutControlGroup7.Size = new System.Drawing.Size(417, 55);
            this.layoutControlGroup7.Text = "Ответственный за проведение ГИА";
            // 
            // OivGekView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "OivGekView";
            this.Size = new System.Drawing.Size(933, 620);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oivGekBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RegionIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private System.Windows.Forms.BindingSource oivGekBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarButtonItem barButtonItem27;
        private DevExpress.XtraBars.BarButtonItem barButtonItem28;
        private DevExpress.XtraBars.BarButtonItem barButtonItem29;
        private DevExpress.XtraBars.BarButtonItem barButtonItem30;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem31;
        private DevExpress.XtraBars.BarButtonItem barButtonItem32;
        private DevExpress.XtraBars.BarButtonItem barButtonItem33;
        private DevExpress.XtraBars.BarButtonItem barButtonItem34;
        private DevExpress.XtraBars.BarButtonItem barButtonItem35;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup14;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem36;
        private DevExpress.XtraBars.BarButtonItem barButtonItem37;
        private DevExpress.XtraBars.BarButtonItem barButtonItem38;
        private DevExpress.XtraBars.BarButtonItem barButtonItem39;
        private DevExpress.XtraBars.BarButtonItem barButtonItem40;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage8;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup15;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup16;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem41;
        private DevExpress.XtraBars.BarButtonItem barButtonItem42;
        private DevExpress.XtraBars.BarButtonItem barButtonItem43;
        private DevExpress.XtraBars.BarButtonItem barButtonItem44;
        private DevExpress.XtraBars.BarButtonItem barButtonItem45;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup17;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup18;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem46;
        private DevExpress.XtraBars.BarButtonItem barButtonItem47;
        private DevExpress.XtraBars.BarButtonItem barButtonItem48;
        private DevExpress.XtraBars.BarButtonItem barButtonItem49;
        private DevExpress.XtraBars.BarButtonItem barButtonItem50;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage10;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup19;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup20;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem51;
        private DevExpress.XtraBars.BarButtonItem barButtonItem52;
        private DevExpress.XtraBars.BarButtonItem barButtonItem53;
        private DevExpress.XtraBars.BarButtonItem barButtonItem54;
        private DevExpress.XtraBars.BarButtonItem barButtonItem55;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup21;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup22;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.LookUpEdit RegionIdLookUpEdit;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionId;
        private System.Windows.Forms.BindingSource bindingSource1;
        private OivGekContactControl nadzorContactControl;
        private OivGekContactControl gekContactControl;
        private OivGekContactControl giaContactControl;
        private OivGekContactControl oivContactControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
