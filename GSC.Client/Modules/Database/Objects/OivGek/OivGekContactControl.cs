﻿using System;
using System.ComponentModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Utils;
using GSC.Model;

namespace GSC.Client.Modules.Database.Objects.OivGek
{
    public partial class OivGekContactControl : DevExpress.XtraEditors.XtraUserControl
    {
        public const string ContactCategory = "Contact";

        public OivGekContactControl()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExplicitOivGekContactControlViewModel>();
            fluent.SetBinding(FioTextEdit, c => c.EditValue, vm => vm.Fio);
            fluent.SetBinding(WorkplaceTextEdit, c => c.EditValue, vm => vm.Workplace);
            fluent.SetBinding(PostTextEdit, c => c.EditValue, vm => vm.Post);
            fluent.SetBinding(Phone1TextEdit, c => c.EditValue, vm => vm.Phone1);
            fluent.SetBinding(Phone2TextEdit, c => c.EditValue, vm => vm.Phone2);
            fluent.SetBinding(EmailTextEdit, c => c.EditValue, vm => vm.Email);
            fluent.SetBinding(layoutControlGroup2, c => c.Text, vm => vm.HeaderTitle);
        }

        
        [Category(ContactCategory)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string HeaderTitle
        {
            get { return ViewModel.HeaderTitle; }
            set { ViewModel.HeaderTitle = value; }
        }

        [Category(ContactCategory)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler HeaderTextChanged
        {
            add { layoutControlGroup2.TextChanged += value; }
            remove { layoutControlGroup2.TextChanged -= value; }
        }

        public OivGekContact Contact
        {
            get { return ViewModel.Contact; }
            set { ViewModel.Contact = value; }
        }

        public ExplicitOivGekContactControlViewModel ViewModel
        {
            get
            {
                return mvvmContext1.GetViewModel<ExplicitOivGekContactControlViewModel>();
            }
        }

        /// <remarks>
        /// This event exists because of OivGekContact is is POCO and does not implement INotifyPropertyChanged. 
        /// So BindingSource can't determine if object's properties were changed.
        /// </remarks>
        [Category(ContactCategory)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler ContactChanged2
        {
            add { ViewModel.ContactChanged2 += value; }
            remove { ViewModel.ContactChanged2 -= value; }
        }

        #region IsReadOnly

        public DefaultBoolean IsReadOnly
        {
            get { return dataLayoutControl1.OptionsView.IsReadOnly; }
            set { dataLayoutControl1.OptionsView.IsReadOnly = value; }
        }

        #endregion
    }

    [POCOViewModel]
    public class OivGekContactControlViewModel : ViewModelBase
    {
        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Fio
        {
            get { return Contact?.Fio; }
            set
            {
                if (Contact != null)
                {
                    Contact.Fio = value;
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Workplace
        {
            get { return Contact?.Workplace; }
            set
            {
                if (Contact != null)
                {
                    Contact.Workplace = value;
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Post
        {
            get { return Contact?.Post; }
            set
            {
                if (Contact != null)
                {
                    Contact.Post = value;
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Phone1
        {
            get { return Contact?.Phone1; }
            set
            {
                if (Contact != null)
                {
                    Contact.Phone1 = value;
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Phone2
        {
            get { return Contact?.Phone2; }
            set
            {
                if (Contact != null)
                {
                    Contact.Phone2 = value;
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Email
        {
            get { return Contact?.Email; }
            set
            {
                if (Contact != null)
                {
                    Contact.Email = value;
                }
            }
        }

        public virtual string HeaderTitle { get; set; }

        public event EventHandler ContactChanged2;

        protected virtual void OnContactChanged2()
        {
            ContactChanged2?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void NotifyPropertyChanged()
        {
            OnContactChanged2();
        }

        private OivGekContact contact;

        public virtual OivGekContact Contact
        {
            get { return contact; }
            set
            {
                contact = value;
                this.RaisePropertyChanged(x => x.Fio);
                this.RaisePropertyChanged(x => x.Workplace);
                this.RaisePropertyChanged(x => x.Post);
                this.RaisePropertyChanged(x => x.Phone1);
                this.RaisePropertyChanged(x => x.Phone2);
                this.RaisePropertyChanged(x => x.Email);
            }
        }
    }

    [POCOViewModel]
    public class ExplicitOivGekContactControlViewModel : ViewModelBase
    {
        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Fio { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Workplace
        { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Post
        { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Phone1
        { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Phone2
        { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = "NotifyPropertyChanged")]
        public virtual string Email
        { get; set; }

        public virtual string HeaderTitle { get; set; }

        public event EventHandler ContactChanged2;

        protected virtual void OnContactChanged2()
        {
            ContactChanged2?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void NotifyPropertyChanged()
        {
            OnContactChanged2();
        }

        public virtual OivGekContact Contact { get; set; }
    }
}
