﻿using DevExpress.XtraGrid.Views.Grid;
using GSC.Client.Util;
using GSC.Client.ViewModels.OivGek;

namespace GSC.Client.Modules.Database.Objects.OivGek
{
    public partial class OivGekCollectionView : CollectionBlockView
    {
        public OivGekCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<OivGekCollectionViewModel>();
            var gridView = gridControl1.MainView as GridView;
            if (gridView != null)
            {
                fluent.InitGridView(gridView, ViewModel);
            }

            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private OivGekCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<OivGekCollectionViewModel>(); }
        }
    }
}
