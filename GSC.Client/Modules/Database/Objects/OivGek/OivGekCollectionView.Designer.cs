﻿namespace GSC.Client.Modules.Database.Objects.OivGek
{
    partial class OivGekCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpOivgekActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gbCommon = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcRegion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbOiv = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcOivFio = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOivPhone1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOivPhone2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOivPost = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOivWorkplace = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOivEmail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcGekFio = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGekPhone1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGekPhone2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGekPost = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGekWorkplace = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGekEmail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcGiaFio = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGiaPhone1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGiaPhone2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGiaPost = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGiaWorkplace = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcGiaEmail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcNadzorFio = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcNadzorPhone1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcNadzorPhone2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcNadzorPost = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcNadzorWorkplace = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcNadzorEmail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivFio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivWorkplace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOivEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekFio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekWorkplace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGekEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaFio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaWorkplace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGiaEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorFio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorWorkplace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNadzorEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 7;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpOivgekActions});
            this.ribbonControl1.Size = new System.Drawing.Size(644, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.OivGek.OivGekCollectionViewModel);
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 1;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 2;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 3;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 4;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 5;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpOivgekActions
            // 
            this.rpOivgekActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgExchange});
            this.rpOivgekActions.Name = "rpOivgekActions";
            this.rpOivgekActions.Text = "ОИВ/ГЭК";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 6;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(644, 339);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1,
            this.gridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbCommon,
            this.gbOiv,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bgcRegion,
            this.bgcOivFio,
            this.bgcOivPhone1,
            this.bgcOivPhone2,
            this.bgcOivPost,
            this.bgcOivWorkplace,
            this.bgcOivEmail,
            this.bgcGekFio,
            this.bgcGekPhone1,
            this.bgcGekPhone2,
            this.bgcGekPost,
            this.bgcGekWorkplace,
            this.bgcGekEmail,
            this.bgcGiaFio,
            this.bgcGiaPhone1,
            this.bgcGiaPhone2,
            this.bgcGiaPost,
            this.bgcGiaWorkplace,
            this.bgcGiaEmail,
            this.bgcNadzorFio,
            this.bgcNadzorPhone1,
            this.bgcNadzorPhone2,
            this.bgcNadzorPost,
            this.bgcNadzorWorkplace,
            this.bgcNadzorEmail});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // gbCommon
            // 
            this.gbCommon.Columns.Add(this.bgcRegion);
            this.gbCommon.Name = "gbCommon";
            this.gbCommon.OptionsBand.ShowCaption = false;
            this.gbCommon.VisibleIndex = 0;
            this.gbCommon.Width = 150;
            // 
            // bgcRegion
            // 
            this.bgcRegion.Caption = "Регион";
            this.bgcRegion.CustomizationCaption = "Регион";
            this.bgcRegion.FieldName = "Region.Name";
            this.bgcRegion.Name = "bgcRegion";
            this.bgcRegion.Visible = true;
            this.bgcRegion.Width = 150;
            // 
            // gbOiv
            // 
            this.gbOiv.Caption = "Руководитель ОИВ";
            this.gbOiv.Columns.Add(this.bgcOivFio);
            this.gbOiv.Columns.Add(this.bgcOivPhone1);
            this.gbOiv.Columns.Add(this.bgcOivPhone2);
            this.gbOiv.Columns.Add(this.bgcOivPost);
            this.gbOiv.Columns.Add(this.bgcOivWorkplace);
            this.gbOiv.Columns.Add(this.bgcOivEmail);
            this.gbOiv.Name = "gbOiv";
            this.gbOiv.VisibleIndex = 1;
            this.gbOiv.Width = 400;
            // 
            // bgcOivFio
            // 
            this.bgcOivFio.Caption = "ФИО";
            this.bgcOivFio.CustomizationCaption = "ФИО руководителя ОИВ";
            this.bgcOivFio.FieldName = "Oiv.Fio";
            this.bgcOivFio.Name = "bgcOivFio";
            this.bgcOivFio.Visible = true;
            this.bgcOivFio.Width = 200;
            // 
            // bgcOivPhone1
            // 
            this.bgcOivPhone1.Caption = "Рабочий телефон";
            this.bgcOivPhone1.CustomizationCaption = "Рабочий телефон руководителя ОИВ";
            this.bgcOivPhone1.FieldName = "Oiv.Phone1";
            this.bgcOivPhone1.Name = "bgcOivPhone1";
            this.bgcOivPhone1.Visible = true;
            this.bgcOivPhone1.Width = 100;
            // 
            // bgcOivPhone2
            // 
            this.bgcOivPhone2.Caption = "Мобильный телефон";
            this.bgcOivPhone2.CustomizationCaption = "Мобильный телефон руководителя ОИВ";
            this.bgcOivPhone2.FieldName = "Oiv.Phone2";
            this.bgcOivPhone2.Name = "bgcOivPhone2";
            this.bgcOivPhone2.Width = 100;
            // 
            // bgcOivPost
            // 
            this.bgcOivPost.Caption = "Должность";
            this.bgcOivPost.CustomizationCaption = "Должность руководителя ОИВ";
            this.bgcOivPost.FieldName = "Oiv.Post";
            this.bgcOivPost.Name = "bgcOivPost";
            this.bgcOivPost.Width = 100;
            // 
            // bgcOivWorkplace
            // 
            this.bgcOivWorkplace.Caption = "Место работы";
            this.bgcOivWorkplace.CustomizationCaption = "Место работы руководителя ОИВ";
            this.bgcOivWorkplace.FieldName = "Oiv.Workplace";
            this.bgcOivWorkplace.Name = "bgcOivWorkplace";
            this.bgcOivWorkplace.Width = 100;
            // 
            // bgcOivEmail
            // 
            this.bgcOivEmail.Caption = "Email";
            this.bgcOivEmail.CustomizationCaption = "Email руководителя ОИВ";
            this.bgcOivEmail.FieldName = "Oiv.Email";
            this.bgcOivEmail.Name = "bgcOivEmail";
            this.bgcOivEmail.Visible = true;
            this.bgcOivEmail.Width = 100;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Председатель ГЭК";
            this.gridBand3.Columns.Add(this.bgcGekFio);
            this.gridBand3.Columns.Add(this.bgcGekPhone1);
            this.gridBand3.Columns.Add(this.bgcGekPhone2);
            this.gridBand3.Columns.Add(this.bgcGekPost);
            this.gridBand3.Columns.Add(this.bgcGekWorkplace);
            this.gridBand3.Columns.Add(this.bgcGekEmail);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 400;
            // 
            // bgcGekFio
            // 
            this.bgcGekFio.Caption = "ФИО";
            this.bgcGekFio.CustomizationCaption = "ФИО председателя ГЭК";
            this.bgcGekFio.FieldName = "Gek.Fio";
            this.bgcGekFio.Name = "bgcGekFio";
            this.bgcGekFio.Visible = true;
            this.bgcGekFio.Width = 200;
            // 
            // bgcGekPhone1
            // 
            this.bgcGekPhone1.Caption = "Рабочий телефон";
            this.bgcGekPhone1.CustomizationCaption = "Рабочий телефон председателя ГЭК";
            this.bgcGekPhone1.FieldName = "Gek.Phone1";
            this.bgcGekPhone1.Name = "bgcGekPhone1";
            this.bgcGekPhone1.Visible = true;
            this.bgcGekPhone1.Width = 100;
            // 
            // bgcGekPhone2
            // 
            this.bgcGekPhone2.Caption = "Мобильный телефон";
            this.bgcGekPhone2.CustomizationCaption = "Мобильный телефон председателя ГЭК";
            this.bgcGekPhone2.FieldName = "Gek.Phone2";
            this.bgcGekPhone2.Name = "bgcGekPhone2";
            this.bgcGekPhone2.Width = 100;
            // 
            // bgcGekPost
            // 
            this.bgcGekPost.Caption = "Должность";
            this.bgcGekPost.CustomizationCaption = "Должность председателя ГЭК";
            this.bgcGekPost.FieldName = "Gek.Post";
            this.bgcGekPost.Name = "bgcGekPost";
            this.bgcGekPost.Width = 100;
            // 
            // bgcGekWorkplace
            // 
            this.bgcGekWorkplace.Caption = "Место работы";
            this.bgcGekWorkplace.CustomizationCaption = "Место работы председателя ГЭК";
            this.bgcGekWorkplace.FieldName = "Gek.Workplace";
            this.bgcGekWorkplace.Name = "bgcGekWorkplace";
            this.bgcGekWorkplace.Width = 100;
            // 
            // bgcGekEmail
            // 
            this.bgcGekEmail.Caption = "Email";
            this.bgcGekEmail.CustomizationCaption = "Email председателя ГЭК";
            this.bgcGekEmail.FieldName = "Gek.Email";
            this.bgcGekEmail.Name = "bgcGekEmail";
            this.bgcGekEmail.Visible = true;
            this.bgcGekEmail.Width = 100;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Ответственный за проведение ГИА";
            this.gridBand4.Columns.Add(this.bgcGiaFio);
            this.gridBand4.Columns.Add(this.bgcGiaPhone1);
            this.gridBand4.Columns.Add(this.bgcGiaPhone2);
            this.gridBand4.Columns.Add(this.bgcGiaPost);
            this.gridBand4.Columns.Add(this.bgcGiaWorkplace);
            this.gridBand4.Columns.Add(this.bgcGiaEmail);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 400;
            // 
            // bgcGiaFio
            // 
            this.bgcGiaFio.Caption = "ФИО";
            this.bgcGiaFio.CustomizationCaption = "ФИО ответственного за проведение ГИА";
            this.bgcGiaFio.FieldName = "Gia.Fio";
            this.bgcGiaFio.Name = "bgcGiaFio";
            this.bgcGiaFio.Visible = true;
            this.bgcGiaFio.Width = 200;
            // 
            // bgcGiaPhone1
            // 
            this.bgcGiaPhone1.Caption = "Рабочий телефон";
            this.bgcGiaPhone1.CustomizationCaption = "Рабочий телефон ответственного за проведение ГИА";
            this.bgcGiaPhone1.FieldName = "Gia.Phone1";
            this.bgcGiaPhone1.Name = "bgcGiaPhone1";
            this.bgcGiaPhone1.Visible = true;
            this.bgcGiaPhone1.Width = 100;
            // 
            // bgcGiaPhone2
            // 
            this.bgcGiaPhone2.Caption = "Мобильный телефон";
            this.bgcGiaPhone2.CustomizationCaption = "Мобильный телефон ответственного за проведение ГИА";
            this.bgcGiaPhone2.FieldName = "Gia.Phone2";
            this.bgcGiaPhone2.Name = "bgcGiaPhone2";
            this.bgcGiaPhone2.Width = 100;
            // 
            // bgcGiaPost
            // 
            this.bgcGiaPost.Caption = "Должность";
            this.bgcGiaPost.CustomizationCaption = "Должность ответственного за проведение ГИА";
            this.bgcGiaPost.FieldName = "Gia.Post";
            this.bgcGiaPost.Name = "bgcGiaPost";
            this.bgcGiaPost.Width = 100;
            // 
            // bgcGiaWorkplace
            // 
            this.bgcGiaWorkplace.Caption = "Место работы";
            this.bgcGiaWorkplace.CustomizationCaption = "Место работы ответственного за проведение ГИА";
            this.bgcGiaWorkplace.FieldName = "Gia.Workplace";
            this.bgcGiaWorkplace.Name = "bgcGiaWorkplace";
            this.bgcGiaWorkplace.Width = 100;
            // 
            // bgcGiaEmail
            // 
            this.bgcGiaEmail.Caption = "Email";
            this.bgcGiaEmail.CustomizationCaption = "Email ответственного за проведение ГИА";
            this.bgcGiaEmail.FieldName = "Gia.Email";
            this.bgcGiaEmail.Name = "bgcGiaEmail";
            this.bgcGiaEmail.Visible = true;
            this.bgcGiaEmail.Width = 100;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "Сотрудник обрнадзора";
            this.gridBand5.Columns.Add(this.bgcNadzorFio);
            this.gridBand5.Columns.Add(this.bgcNadzorPhone1);
            this.gridBand5.Columns.Add(this.bgcNadzorPhone2);
            this.gridBand5.Columns.Add(this.bgcNadzorPost);
            this.gridBand5.Columns.Add(this.bgcNadzorWorkplace);
            this.gridBand5.Columns.Add(this.bgcNadzorEmail);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 400;
            // 
            // bgcNadzorFio
            // 
            this.bgcNadzorFio.Caption = "ФИО";
            this.bgcNadzorFio.CustomizationCaption = "ФИО сотрудника обрнадзора";
            this.bgcNadzorFio.FieldName = "Nadzor.Fio";
            this.bgcNadzorFio.Name = "bgcNadzorFio";
            this.bgcNadzorFio.Visible = true;
            this.bgcNadzorFio.Width = 200;
            // 
            // bgcNadzorPhone1
            // 
            this.bgcNadzorPhone1.Caption = "Рабочий телефон";
            this.bgcNadzorPhone1.CustomizationCaption = "Рабочий телефон сотрудника обрнадзора";
            this.bgcNadzorPhone1.FieldName = "Nadzor.Phone1";
            this.bgcNadzorPhone1.Name = "bgcNadzorPhone1";
            this.bgcNadzorPhone1.Visible = true;
            this.bgcNadzorPhone1.Width = 100;
            // 
            // bgcNadzorPhone2
            // 
            this.bgcNadzorPhone2.Caption = "Мобильный телефон";
            this.bgcNadzorPhone2.CustomizationCaption = "Мобильный телефон сотрудника обрнадзора";
            this.bgcNadzorPhone2.FieldName = "Nadzor.Phone2";
            this.bgcNadzorPhone2.Name = "bgcNadzorPhone2";
            this.bgcNadzorPhone2.Width = 100;
            // 
            // bgcNadzorPost
            // 
            this.bgcNadzorPost.Caption = "Должность";
            this.bgcNadzorPost.CustomizationCaption = "Должность сотрудника обрнадзора";
            this.bgcNadzorPost.FieldName = "Nadzor.Post";
            this.bgcNadzorPost.Name = "bgcNadzorPost";
            this.bgcNadzorPost.Width = 100;
            // 
            // bgcNadzorWorkplace
            // 
            this.bgcNadzorWorkplace.Caption = "Место работы";
            this.bgcNadzorWorkplace.CustomizationCaption = "Место работы сотрудника обрнадзора";
            this.bgcNadzorWorkplace.FieldName = "Nadzor.Workplace";
            this.bgcNadzorWorkplace.Name = "bgcNadzorWorkplace";
            this.bgcNadzorWorkplace.Width = 100;
            // 
            // bgcNadzorEmail
            // 
            this.bgcNadzorEmail.Caption = "Email";
            this.bgcNadzorEmail.CustomizationCaption = "Email сотрудника обрнадзора";
            this.bgcNadzorEmail.FieldName = "Nadzor.Email";
            this.bgcNadzorEmail.Name = "bgcNadzorEmail";
            this.bgcNadzorEmail.Visible = true;
            this.bgcNadzorEmail.Width = 100;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRegion,
            this.colOivFio,
            this.colOivPhone1,
            this.colOivPhone2,
            this.colOivPost,
            this.colOivWorkplace,
            this.colOivEmail,
            this.colGekFio,
            this.colGekPhone1,
            this.colGekPhone2,
            this.colGekPost,
            this.colGekWorkplace,
            this.colGekEmail,
            this.colGiaFio,
            this.colGiaPhone1,
            this.colGiaPhone2,
            this.colGiaPost,
            this.colGiaWorkplace,
            this.colGiaEmail,
            this.colNadzorFio,
            this.colNadzorPhone1,
            this.colNadzorPhone2,
            this.colNadzorPost,
            this.colNadzorWorkplace,
            this.colNadzorEmail});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Region";
            this.colRegion.Name = "colRegion";
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 6;
            // 
            // colOivFio
            // 
            this.colOivFio.Caption = "Руководитель ОИВ";
            this.colOivFio.FieldName = "Oiv.Fio";
            this.colOivFio.Name = "colOivFio";
            this.colOivFio.Visible = true;
            this.colOivFio.VisibleIndex = 0;
            // 
            // colOivPhone1
            // 
            this.colOivPhone1.Caption = "Рабочий телефон руководителя ОИВ";
            this.colOivPhone1.FieldName = "Oiv.Phone1";
            this.colOivPhone1.Name = "colOivPhone1";
            this.colOivPhone1.Visible = true;
            this.colOivPhone1.VisibleIndex = 1;
            // 
            // colOivPhone2
            // 
            this.colOivPhone2.Caption = "Мобильный телефон руководителя ОИВ";
            this.colOivPhone2.FieldName = "Oiv.Phone2";
            this.colOivPhone2.Name = "colOivPhone2";
            // 
            // colOivPost
            // 
            this.colOivPost.Caption = "Должность руководителя ОИВ";
            this.colOivPost.FieldName = "Oiv.Post";
            this.colOivPost.Name = "colOivPost";
            // 
            // colOivWorkplace
            // 
            this.colOivWorkplace.Caption = "Место работы руководитея ОИВ";
            this.colOivWorkplace.FieldName = "Oiv.Workplace";
            this.colOivWorkplace.Name = "colOivWorkplace";
            // 
            // colOivEmail
            // 
            this.colOivEmail.Caption = "Электронная почта руководителя ОИВ";
            this.colOivEmail.FieldName = "Oiv.Email";
            this.colOivEmail.Name = "colOivEmail";
            this.colOivEmail.Visible = true;
            this.colOivEmail.VisibleIndex = 2;
            // 
            // colGekFio
            // 
            this.colGekFio.Caption = "Председатель ГЭК";
            this.colGekFio.FieldName = "Gek.Fio";
            this.colGekFio.Name = "colGekFio";
            this.colGekFio.Visible = true;
            this.colGekFio.VisibleIndex = 3;
            // 
            // colGekPhone1
            // 
            this.colGekPhone1.Caption = "Рабочий телефон председателя ГЭК";
            this.colGekPhone1.FieldName = "Gek.Phone1";
            this.colGekPhone1.Name = "colGekPhone1";
            this.colGekPhone1.Visible = true;
            this.colGekPhone1.VisibleIndex = 4;
            // 
            // colGekPhone2
            // 
            this.colGekPhone2.Caption = "Мобильный телефон председателя ГЭК";
            this.colGekPhone2.FieldName = "Gek.Phone2";
            this.colGekPhone2.Name = "colGekPhone2";
            // 
            // colGekPost
            // 
            this.colGekPost.Caption = "Должность председателя ГЭК";
            this.colGekPost.FieldName = "Gek.Post";
            this.colGekPost.Name = "colGekPost";
            // 
            // colGekWorkplace
            // 
            this.colGekWorkplace.Caption = "Место работы председателя ГЭК";
            this.colGekWorkplace.FieldName = "Gek.Workplace";
            this.colGekWorkplace.Name = "colGekWorkplace";
            // 
            // colGekEmail
            // 
            this.colGekEmail.Caption = "Электронная почта председателя ГЭК";
            this.colGekEmail.FieldName = "Gek.Email";
            this.colGekEmail.Name = "colGekEmail";
            this.colGekEmail.Visible = true;
            this.colGekEmail.VisibleIndex = 5;
            // 
            // colGiaFio
            // 
            this.colGiaFio.Caption = "Ответственный за проведение ГИА";
            this.colGiaFio.FieldName = "Gia.Fio";
            this.colGiaFio.Name = "colGiaFio";
            this.colGiaFio.Visible = true;
            this.colGiaFio.VisibleIndex = 7;
            // 
            // colGiaPhone1
            // 
            this.colGiaPhone1.Caption = "Рабочий телефон ответственного за проведение ГИА";
            this.colGiaPhone1.FieldName = "Gia.Phone1";
            this.colGiaPhone1.Name = "colGiaPhone1";
            this.colGiaPhone1.Visible = true;
            this.colGiaPhone1.VisibleIndex = 8;
            // 
            // colGiaPhone2
            // 
            this.colGiaPhone2.Caption = "Мобильный телефон ответственного за проведение ГИА";
            this.colGiaPhone2.FieldName = "Gia.Phone2";
            this.colGiaPhone2.Name = "colGiaPhone2";
            // 
            // colGiaPost
            // 
            this.colGiaPost.Caption = "Должность ответственного за проведение ГИА";
            this.colGiaPost.FieldName = "Gia.Post";
            this.colGiaPost.Name = "colGiaPost";
            // 
            // colGiaWorkplace
            // 
            this.colGiaWorkplace.Caption = "Место работы ответственного за проведение ГИА";
            this.colGiaWorkplace.FieldName = "Gia.Workplace";
            this.colGiaWorkplace.Name = "colGiaWorkplace";
            // 
            // colGiaEmail
            // 
            this.colGiaEmail.Caption = "Электронная почта ответственного за продедение ГИА";
            this.colGiaEmail.FieldName = "Gia.Email";
            this.colGiaEmail.Name = "colGiaEmail";
            this.colGiaEmail.Visible = true;
            this.colGiaEmail.VisibleIndex = 9;
            // 
            // colNadzorFio
            // 
            this.colNadzorFio.Caption = "Сотрудник обрнадзора";
            this.colNadzorFio.FieldName = "Nadzor.Fio";
            this.colNadzorFio.Name = "colNadzorFio";
            this.colNadzorFio.Visible = true;
            this.colNadzorFio.VisibleIndex = 10;
            // 
            // colNadzorPhone1
            // 
            this.colNadzorPhone1.Caption = "Рабочий телефон сотрудника обрнадзора";
            this.colNadzorPhone1.FieldName = "Nadzor.Phone1";
            this.colNadzorPhone1.Name = "colNadzorPhone1";
            this.colNadzorPhone1.Visible = true;
            this.colNadzorPhone1.VisibleIndex = 11;
            // 
            // colNadzorPhone2
            // 
            this.colNadzorPhone2.Caption = "Мобильный телефон сотрудника обрнадзора";
            this.colNadzorPhone2.FieldName = "Nadzor.Phone2";
            this.colNadzorPhone2.Name = "colNadzorPhone2";
            // 
            // colNadzorPost
            // 
            this.colNadzorPost.Caption = "Должность сотрудника обрнадзора";
            this.colNadzorPost.FieldName = "Nadzor.Post";
            this.colNadzorPost.Name = "colNadzorPost";
            // 
            // colNadzorWorkplace
            // 
            this.colNadzorWorkplace.Caption = "Место работы сотрудника обрнадзора";
            this.colNadzorWorkplace.FieldName = "Nadzor.Workplace";
            this.colNadzorWorkplace.Name = "colNadzorWorkplace";
            // 
            // colNadzorEmail
            // 
            this.colNadzorEmail.Caption = "Электронная почта сотрудника обрнадзора";
            this.colNadzorEmail.FieldName = "Nadzor.Email";
            this.colNadzorEmail.Name = "colNadzorEmail";
            this.colNadzorEmail.Visible = true;
            this.colNadzorEmail.VisibleIndex = 12;
            // 
            // OivGekCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "OivGekCollectionView";
            this.Size = new System.Drawing.Size(644, 480);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpOivgekActions;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraGrid.Columns.GridColumn colOivFio;
        private DevExpress.XtraGrid.Columns.GridColumn colGekPhone2;
        private DevExpress.XtraGrid.Columns.GridColumn colGekWorkplace;
        private DevExpress.XtraGrid.Columns.GridColumn colGekEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colOivPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colOivPhone2;
        private DevExpress.XtraGrid.Columns.GridColumn colOivWorkplace;
        private DevExpress.XtraGrid.Columns.GridColumn colOivEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colGekFio;
        private DevExpress.XtraGrid.Columns.GridColumn colGekPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaFio;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaPhone2;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaWorkplace;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorFio;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorPhone2;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorWorkplace;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colOivPost;
        private DevExpress.XtraGrid.Columns.GridColumn colGekPost;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaPost;
        private DevExpress.XtraGrid.Columns.GridColumn colNadzorPost;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbCommon;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcRegion;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbOiv;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivFio;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivPhone1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivPhone2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivPost;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivWorkplace;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOivEmail;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekFio;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekPhone1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekPhone2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekPost;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekWorkplace;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGekEmail;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaFio;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaPhone1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaPhone2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaPost;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaWorkplace;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcGiaEmail;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorFio;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorPhone1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorPhone2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorPost;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorWorkplace;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcNadzorEmail;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
