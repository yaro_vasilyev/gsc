﻿namespace GSC.Client.Modules.Database.Objects.OivGek
{
    partial class OivGekContactControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.FioTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WorkplaceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.Phone1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.Phone2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkplace = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.mvvmContext1 = new DevExpress.Utils.MVVM.MVVMContext(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FioTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.FioTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkplaceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.Phone1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.Phone2TextEdit);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(787, 234, 673, 609);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(404, 209);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // FioTextEdit
            // 
            this.FioTextEdit.Location = new System.Drawing.Point(136, 42);
            this.FioTextEdit.Name = "FioTextEdit";
            this.FioTextEdit.Size = new System.Drawing.Size(244, 20);
            this.FioTextEdit.StyleController = this.dataLayoutControl1;
            this.FioTextEdit.TabIndex = 4;
            // 
            // WorkplaceTextEdit
            // 
            this.WorkplaceTextEdit.Location = new System.Drawing.Point(136, 66);
            this.WorkplaceTextEdit.Name = "WorkplaceTextEdit";
            this.WorkplaceTextEdit.Size = new System.Drawing.Size(244, 20);
            this.WorkplaceTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkplaceTextEdit.TabIndex = 5;
            // 
            // PostTextEdit
            // 
            this.PostTextEdit.Location = new System.Drawing.Point(136, 90);
            this.PostTextEdit.Name = "PostTextEdit";
            this.PostTextEdit.Size = new System.Drawing.Size(244, 20);
            this.PostTextEdit.StyleController = this.dataLayoutControl1;
            this.PostTextEdit.TabIndex = 6;
            // 
            // Phone1TextEdit
            // 
            this.Phone1TextEdit.Location = new System.Drawing.Point(136, 114);
            this.Phone1TextEdit.Name = "Phone1TextEdit";
            this.Phone1TextEdit.Size = new System.Drawing.Size(244, 20);
            this.Phone1TextEdit.StyleController = this.dataLayoutControl1;
            this.Phone1TextEdit.TabIndex = 7;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.Location = new System.Drawing.Point(136, 162);
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Size = new System.Drawing.Size(244, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 8;
            // 
            // Phone2TextEdit
            // 
            this.Phone2TextEdit.Location = new System.Drawing.Point(136, 138);
            this.Phone2TextEdit.Name = "Phone2TextEdit";
            this.Phone2TextEdit.Size = new System.Drawing.Size(244, 20);
            this.Phone2TextEdit.StyleController = this.dataLayoutControl1;
            this.Phone2TextEdit.TabIndex = 9;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(404, 209);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowCustomizeChildren = false;
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFio,
            this.ItemForWorkplace,
            this.ItemForPost,
            this.ItemForPhone1,
            this.ItemForEmail,
            this.ItemForPhone2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(384, 189);
            // 
            // ItemForFio
            // 
            this.ItemForFio.Control = this.FioTextEdit;
            this.ItemForFio.CustomizationFormText = "ФИО";
            this.ItemForFio.Location = new System.Drawing.Point(0, 0);
            this.ItemForFio.Name = "ItemForFio";
            this.ItemForFio.Size = new System.Drawing.Size(360, 24);
            this.ItemForFio.Text = "ФИО:";
            this.ItemForFio.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForWorkplace
            // 
            this.ItemForWorkplace.Control = this.WorkplaceTextEdit;
            this.ItemForWorkplace.CustomizationFormText = "Место работы";
            this.ItemForWorkplace.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkplace.Name = "ItemForWorkplace";
            this.ItemForWorkplace.Size = new System.Drawing.Size(360, 24);
            this.ItemForWorkplace.Text = "Место работы:";
            this.ItemForWorkplace.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForPost
            // 
            this.ItemForPost.Control = this.PostTextEdit;
            this.ItemForPost.CustomizationFormText = "Должность";
            this.ItemForPost.Location = new System.Drawing.Point(0, 48);
            this.ItemForPost.Name = "ItemForPost";
            this.ItemForPost.Size = new System.Drawing.Size(360, 24);
            this.ItemForPost.Text = "Должность:";
            this.ItemForPost.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForPhone1
            // 
            this.ItemForPhone1.Control = this.Phone1TextEdit;
            this.ItemForPhone1.CustomizationFormText = "Рабочий телефон";
            this.ItemForPhone1.Location = new System.Drawing.Point(0, 72);
            this.ItemForPhone1.Name = "ItemForPhone1";
            this.ItemForPhone1.Size = new System.Drawing.Size(360, 24);
            this.ItemForPhone1.Text = "Рабочий телефон:";
            this.ItemForPhone1.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.CustomizationFormText = "Email";
            this.ItemForEmail.Location = new System.Drawing.Point(0, 120);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(360, 27);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForPhone2
            // 
            this.ItemForPhone2.Control = this.Phone2TextEdit;
            this.ItemForPhone2.CustomizationFormText = "Мобильный телефон";
            this.ItemForPhone2.Location = new System.Drawing.Point(0, 96);
            this.ItemForPhone2.Name = "ItemForPhone2";
            this.ItemForPhone2.Size = new System.Drawing.Size(360, 24);
            this.ItemForPhone2.Text = "Мобильный телефон:";
            this.ItemForPhone2.TextSize = new System.Drawing.Size(109, 13);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.Modules.Database.Objects.OivGek.ExplicitOivGekContactControlViewModel);
            // 
            // OivGekContactControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "OivGekContactControl";
            this.Size = new System.Drawing.Size(404, 209);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FioTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit FioTextEdit;
        private DevExpress.XtraEditors.TextEdit WorkplaceTextEdit;
        private DevExpress.XtraEditors.TextEdit PostTextEdit;
        private DevExpress.XtraEditors.TextEdit Phone1TextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.TextEdit Phone2TextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFio;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkplace;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone2;
        private DevExpress.Utils.MVVM.MVVMContext mvvmContext1;
    }
}
