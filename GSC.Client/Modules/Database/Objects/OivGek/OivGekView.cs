﻿using System;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels.OivGek;

namespace GSC.Client.Modules.Database.Objects.OivGek
{
    public partial class OivGekView : BaseEditView
    {
        public OivGekView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<OivGekViewModel>();
            fluent.SetBinding(regionBindingSource,
                              bs => bs.DataSource,
                              vm => vm.LookupRegions.Entities);
            fluent.SetObjectDataSourceBinding(oivGekBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());

            fluent.SetBinding(oivContactControl.ViewModel, c => c.Email, vm => vm.Entity.OivEmail);
            fluent.SetBinding(oivContactControl.ViewModel, c => c.Fio, vm => vm.Entity.OivFio);
            fluent.SetBinding(oivContactControl.ViewModel, c => c.Phone1, vm => vm.Entity.OivPhone1);
            fluent.SetBinding(oivContactControl.ViewModel, c => c.Phone2, vm => vm.Entity.OivPhone2);
            fluent.SetBinding(oivContactControl.ViewModel, c => c.Post, vm => vm.Entity.OivPost);
            fluent.SetBinding(oivContactControl.ViewModel, c => c.Workplace, vm => vm.Entity.OivWp);

            fluent.SetBinding(gekContactControl.ViewModel, c => c.Email, vm => vm.Entity.GekEmail);
            fluent.SetBinding(gekContactControl.ViewModel, c => c.Fio, vm => vm.Entity.GekFio);
            fluent.SetBinding(gekContactControl.ViewModel, c => c.Phone1, vm => vm.Entity.GekPhone1);
            fluent.SetBinding(gekContactControl.ViewModel, c => c.Phone2, vm => vm.Entity.GekPhone2);
            fluent.SetBinding(gekContactControl.ViewModel, c => c.Post, vm => vm.Entity.GekPost);
            fluent.SetBinding(gekContactControl.ViewModel, c => c.Workplace, vm => vm.Entity.GekWp);

            fluent.SetBinding(giaContactControl.ViewModel, c => c.Email, vm => vm.Entity.GiaEmail);
            fluent.SetBinding(giaContactControl.ViewModel, c => c.Fio, vm => vm.Entity.GiaFio);
            fluent.SetBinding(giaContactControl.ViewModel, c => c.Phone1, vm => vm.Entity.GiaPhone1);
            fluent.SetBinding(giaContactControl.ViewModel, c => c.Phone2, vm => vm.Entity.GiaPhone2);
            fluent.SetBinding(giaContactControl.ViewModel, c => c.Post, vm => vm.Entity.GiaPost);
            fluent.SetBinding(giaContactControl.ViewModel, c => c.Workplace, vm => vm.Entity.GiaWp);


            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Email, vm => vm.Entity.NadzorEmail);
            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Fio, vm => vm.Entity.NadzorFio);
            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Phone1, vm => vm.Entity.NadzorPhone1);
            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Phone2, vm => vm.Entity.NadzorPhone2);
            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Post, vm => vm.Entity.NadzorPost);
            fluent.SetBinding(nadzorContactControl.ViewModel, c => c.Workplace, vm => vm.Entity.NadzorWp);
        }

        private OivGekViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<OivGekViewModel>(); }
        }

        private void ContactControl_ContactPropertyChanged(object sender, EventArgs e)
        {
            ViewModel.UpdateContacts();
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            var ro = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
            dataLayoutControl1.OptionsView.IsReadOnly = ro;
            oivContactControl.IsReadOnly = ro;
            gekContactControl.IsReadOnly = ro;
            giaContactControl.IsReadOnly = ro;
            nadzorContactControl.IsReadOnly = ro;
        }
    }
}
