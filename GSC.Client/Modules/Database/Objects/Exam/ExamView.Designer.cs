﻿namespace GSC.Client.Modules.Database.Objects.Exam
{
    partial class ExamView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.PeriodTypeImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.examBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ExamDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SubjectIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.subjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ExamTypeImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExamDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubjectId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExamType = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodTypeImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamTypeImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubjectId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamType)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Exam.ExamViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(582, 141);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.PeriodTypeImageComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.ExamDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SubjectIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExamTypeImageComboBoxEdit);
            this.dataLayoutControl1.DataSource = this.examBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(582, 290);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // PeriodTypeImageComboBoxEdit
            // 
            this.PeriodTypeImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.examBindingSource, "PeriodType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PeriodTypeImageComboBoxEdit.Location = new System.Drawing.Point(94, 12);
            this.PeriodTypeImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.PeriodTypeImageComboBoxEdit.Name = "PeriodTypeImageComboBoxEdit";
            this.PeriodTypeImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PeriodTypeImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PeriodTypeImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PeriodTypeImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.PeriodTypeImageComboBoxEdit.Size = new System.Drawing.Size(476, 20);
            this.PeriodTypeImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PeriodTypeImageComboBoxEdit.TabIndex = 4;
            // 
            // examBindingSource
            // 
            this.examBindingSource.DataSource = typeof(GSC.Model.Exam);
            // 
            // ExamDateDateEdit
            // 
            this.ExamDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.examBindingSource, "ExamDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExamDateDateEdit.EditValue = null;
            this.ExamDateDateEdit.Location = new System.Drawing.Point(94, 36);
            this.ExamDateDateEdit.MenuManager = this.ribbonControl1;
            this.ExamDateDateEdit.Name = "ExamDateDateEdit";
            this.ExamDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExamDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExamDateDateEdit.Size = new System.Drawing.Size(476, 20);
            this.ExamDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExamDateDateEdit.TabIndex = 5;
            // 
            // SubjectIdLookUpEdit
            // 
            this.SubjectIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.examBindingSource, "SubjectId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SubjectIdLookUpEdit.Location = new System.Drawing.Point(94, 60);
            this.SubjectIdLookUpEdit.MenuManager = this.ribbonControl1;
            this.SubjectIdLookUpEdit.Name = "SubjectIdLookUpEdit";
            this.SubjectIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SubjectIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SubjectIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubjectIdLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.SubjectIdLookUpEdit.Properties.DataSource = this.subjectBindingSource;
            this.SubjectIdLookUpEdit.Properties.DisplayMember = "Name";
            this.SubjectIdLookUpEdit.Properties.NullText = "";
            this.SubjectIdLookUpEdit.Properties.ValueMember = "Id";
            this.SubjectIdLookUpEdit.Size = new System.Drawing.Size(476, 20);
            this.SubjectIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubjectIdLookUpEdit.TabIndex = 6;
            // 
            // subjectBindingSource
            // 
            this.subjectBindingSource.DataSource = typeof(GSC.Model.Subject);
            // 
            // ExamTypeImageComboBoxEdit
            // 
            this.ExamTypeImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.examBindingSource, "ExamType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExamTypeImageComboBoxEdit.Location = new System.Drawing.Point(94, 84);
            this.ExamTypeImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.ExamTypeImageComboBoxEdit.Name = "ExamTypeImageComboBoxEdit";
            this.ExamTypeImageComboBoxEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ExamTypeImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExamTypeImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExamTypeImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExamTypeImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.ExamTypeImageComboBoxEdit.Size = new System.Drawing.Size(476, 20);
            this.ExamTypeImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ExamTypeImageComboBoxEdit.TabIndex = 7;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(582, 290);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPeriodType,
            this.ItemForExamDate,
            this.ItemForSubjectId,
            this.ItemForExamType});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(562, 270);
            // 
            // ItemForPeriodType
            // 
            this.ItemForPeriodType.Control = this.PeriodTypeImageComboBoxEdit;
            this.ItemForPeriodType.Location = new System.Drawing.Point(0, 0);
            this.ItemForPeriodType.Name = "ItemForPeriodType";
            this.ItemForPeriodType.Size = new System.Drawing.Size(562, 24);
            this.ItemForPeriodType.Text = "Период:";
            this.ItemForPeriodType.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForExamDate
            // 
            this.ItemForExamDate.Control = this.ExamDateDateEdit;
            this.ItemForExamDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForExamDate.Name = "ItemForExamDate";
            this.ItemForExamDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForExamDate.Text = "Дата экзамена:";
            this.ItemForExamDate.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForSubjectId
            // 
            this.ItemForSubjectId.Control = this.SubjectIdLookUpEdit;
            this.ItemForSubjectId.Location = new System.Drawing.Point(0, 48);
            this.ItemForSubjectId.Name = "ItemForSubjectId";
            this.ItemForSubjectId.Size = new System.Drawing.Size(562, 24);
            this.ItemForSubjectId.Text = "Предмет:";
            this.ItemForSubjectId.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForExamType
            // 
            this.ItemForExamType.Control = this.ExamTypeImageComboBoxEdit;
            this.ItemForExamType.Location = new System.Drawing.Point(0, 72);
            this.ItemForExamType.Name = "ItemForExamType";
            this.ItemForExamType.Size = new System.Drawing.Size(562, 198);
            this.ItemForExamType.Text = "Тип экзамена:";
            this.ItemForExamType.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ExamView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "ExamView";
            this.Size = new System.Drawing.Size(582, 431);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PeriodTypeImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamTypeImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubjectId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExamType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit PeriodTypeImageComboBoxEdit;
        private System.Windows.Forms.BindingSource examBindingSource;
        private DevExpress.XtraEditors.DateEdit ExamDateDateEdit;
        private DevExpress.XtraEditors.LookUpEdit SubjectIdLookUpEdit;
        private System.Windows.Forms.BindingSource subjectBindingSource;
        private DevExpress.XtraEditors.ImageComboBoxEdit ExamTypeImageComboBoxEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPeriodType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExamDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubjectId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExamType;
    }
}
