﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Exam;
using GSC.Model;

namespace GSC.Client.Modules.Database.Objects.Exam
{
    public partial class ExamCollectionView : CollectionBlockView
    {
        public ExamCollectionView()
        {
            InitializeComponent();
            periodTypeInplaceCombo.Items.AddEnum<PeriodType>();
            examTypeInplaceCombo.Items.AddEnum<ExamType>();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExamCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);

            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private ExamCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ExamCollectionViewModel>(); }
        }
    }
}
