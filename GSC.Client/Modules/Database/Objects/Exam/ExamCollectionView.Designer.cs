﻿namespace GSC.Client.Modules.Database.Objects.Exam
{
    partial class ExamCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiImportFromCsv = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpExams = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPeriodType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.periodTypeInplaceCombo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colExamDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExamType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.examTypeInplaceCombo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodTypeInplaceCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examTypeInplaceCombo)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiImportFromCsv,
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 8;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpExams});
            this.ribbonControl1.Size = new System.Drawing.Size(576, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "ImportFromCsv", this.bbiImportFromCsv),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Exam.ExamCollectionViewModel);
            // 
            // bbiImportFromCsv
            // 
            this.bbiImportFromCsv.Caption = "Импорт из файла";
            this.bbiImportFromCsv.Id = 1;
            this.bbiImportFromCsv.ImageUri.Uri = "Apply";
            this.bbiImportFromCsv.Name = "bbiImportFromCsv";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 2;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 3;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 4;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 6;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpExams
            // 
            this.rpExams.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgExchange});
            this.rpExams.Name = "rpExams";
            this.rpExams.Text = "Расписание";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 7;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.ItemLinks.Add(this.bbiImportFromCsv);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.periodTypeInplaceCombo,
            this.examTypeInplaceCombo});
            this.gridControl1.Size = new System.Drawing.Size(576, 241);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPeriodType,
            this.colExamDate,
            this.colSubject,
            this.colExamType});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // colPeriodType
            // 
            this.colPeriodType.Caption = "Период";
            this.colPeriodType.ColumnEdit = this.periodTypeInplaceCombo;
            this.colPeriodType.FieldName = "PeriodType";
            this.colPeriodType.Name = "colPeriodType";
            this.colPeriodType.Visible = true;
            this.colPeriodType.VisibleIndex = 0;
            this.colPeriodType.Width = 167;
            // 
            // periodTypeInplaceCombo
            // 
            this.periodTypeInplaceCombo.AutoHeight = false;
            this.periodTypeInplaceCombo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.periodTypeInplaceCombo.Name = "periodTypeInplaceCombo";
            // 
            // colExamDate
            // 
            this.colExamDate.Caption = "Дата";
            this.colExamDate.DisplayFormat.FormatString = "d";
            this.colExamDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExamDate.FieldName = "ExamDate";
            this.colExamDate.Name = "colExamDate";
            this.colExamDate.Visible = true;
            this.colExamDate.VisibleIndex = 1;
            this.colExamDate.Width = 118;
            // 
            // colSubject
            // 
            this.colSubject.Caption = "Предмет";
            this.colSubject.FieldName = "Subject.Name";
            this.colSubject.Name = "colSubject";
            this.colSubject.Visible = true;
            this.colSubject.VisibleIndex = 3;
            this.colSubject.Width = 202;
            // 
            // colExamType
            // 
            this.colExamType.Caption = "Тип экзамена";
            this.colExamType.ColumnEdit = this.examTypeInplaceCombo;
            this.colExamType.FieldName = "ExamType";
            this.colExamType.Name = "colExamType";
            this.colExamType.Visible = true;
            this.colExamType.VisibleIndex = 2;
            this.colExamType.Width = 116;
            // 
            // examTypeInplaceCombo
            // 
            this.examTypeInplaceCombo.AutoHeight = false;
            this.examTypeInplaceCombo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.examTypeInplaceCombo.Name = "examTypeInplaceCombo";
            // 
            // ExamCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "ExamCollectionView";
            this.Size = new System.Drawing.Size(576, 382);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodTypeInplaceCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examTypeInplaceCombo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpExams;
        private DevExpress.XtraBars.BarButtonItem bbiImportFromCsv;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodType;
        private DevExpress.XtraGrid.Columns.GridColumn colExamDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSubject;
        private DevExpress.XtraGrid.Columns.GridColumn colExamType;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox periodTypeInplaceCombo;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox examTypeInplaceCombo;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
