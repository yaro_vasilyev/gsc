﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels.Exam;
using GSC.Model;

namespace GSC.Client.Modules.Database.Objects.Exam
{
    public partial class ExamView : BaseEditView
    {
        public ExamView()
        {
            InitializeComponent();
            PeriodTypeImageComboBoxEdit.Properties.Items.AddEnum<PeriodType>();
            ExamTypeImageComboBoxEdit.Properties.Items.AddEnum<ExamType>();
            
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExamViewModel>();

            fluent.SetBinding(subjectBindingSource,
                              bs => bs.DataSource,
                              vm => vm.LookupSubjects.Entities);

            fluent.SetObjectDataSourceBinding(examBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
