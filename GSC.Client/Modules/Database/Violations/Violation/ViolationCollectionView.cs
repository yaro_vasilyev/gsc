﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Violation;

namespace GSC.Client.Modules.Database.Violations.Violation
{
    public partial class ViolationCollectionView : CollectionBlockView
    {
        public ViolationCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ViolationCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private ViolationCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ViolationCollectionViewModel>(); }
        }
    }
}
