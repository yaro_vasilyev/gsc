﻿using DevExpress.Utils;
using GSC.Client.ViewModels.Violation;

namespace GSC.Client.Modules.Database.Violations.Violation
{
    public partial class ViolationView : BaseEditView
    {
        public ViolationView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ViolationViewModel>();
            fluent.SetObjectDataSourceBinding(violationBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
