﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels.Violator;

namespace GSC.Client.Modules.Database.Violations.Violator
{
    public partial class ViolatorView : BaseEditView
    {
        public ViolatorView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ViolatorViewModel>();
            fluent.SetObjectDataSourceBinding(violatorBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
