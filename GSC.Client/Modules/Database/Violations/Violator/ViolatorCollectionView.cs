﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Violator;

namespace GSC.Client.Modules.Database.Violations.Violator
{
    public partial class ViolatorCollectionView : CollectionBlockView
    {
        public ViolatorCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ViolatorCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private ViolatorCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ViolatorCollectionViewModel>(); }
        }
    }
}
