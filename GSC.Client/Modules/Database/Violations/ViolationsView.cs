﻿using System;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using GSC.Client.Util;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules.Database.Violations
{
    public partial class ViolationsView : XtraUserControl, IRibbonSource
    {
        // TODO: Refactor this. This control is a copy-paste from ObjectsView

        public ViolationsView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ViolationsViewModel>();

            fluent.SetItemsSourceBinding(officeNavigationBar1,
                                         onb => onb.Items,
                                         vm => vm.Modules,
                                         (i, m) => object.Equals(i.Tag, m),
                                         m =>
                                         {
                                             var item = new NavigationBarItem
                                             {
                                                 Text = m.ModuleTitle,
                                                 Tag = m
                                             };
                                             fluent.BindCommand(item, vm => vm.Show(null), vm => item.Tag);
                                             return item;
                                         });
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var viewModel = mvvmContext1.GetViewModel<ViolationsViewModel>();
            if (viewModel.Modules.Any())
            {
                viewModel.Show(viewModel.DefaultModule);
                officeNavigationBar1.SelectedItem = officeNavigationBar1.Items.First(i => Equals(i.Tag, viewModel.DefaultModule));
            }
        }

        private IRibbonSource GetChildRibbonSource()
        {
            var activePageControl = navigationFrame1.GetSelectedPageRootControl();
            return activePageControl as IRibbonSource;
        }

        public RibbonControl Ribbon => GetChildRibbonSource()?.Ribbon;

        private void OnNavigationFrameSelectedPageChanged(object sender, SelectedPageChangedEventArgs e)
        {
            Messenger.Default.Send(new MergeRibbonMessage
            {
                Ribbon = this.Ribbon
            });
        }
    }
}
