﻿using DevExpress.XtraGrid.Views.Base;
using GSC.Client.ViewModels.ContractInfo;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Database.ContractInfos
{
    [UsedImplicitly]
    public partial class ContractInfoCollectionView : CollectionBlockView
    {
        public ContractInfoCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ContractInfoCollectionViewModel>();
            fluent.SetBinding(gridView1, v => v.LoadingPanelVisible, vm => vm.IsLoading);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);
            fluent.WithEvent<ColumnView, FocusedRowObjectChangedEventArgs>(gridView1, "FocusedRowObjectChanged")
                .SetBinding(vm => vm.SelectedEntity,
                            ea => ea.Row as ContractInfo,
                            (v, e) => v.FocusedRowHandle = v.FindRow(e));
        }
    }
}
