﻿using DevExpress.Utils;
using GSC.Client.ViewModels.ContractInfo;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Database.ContractInfos
{
    [UsedImplicitly]
    public partial class ContractInfoView : BaseEditView
    {
        public ContractInfoView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ContractInfoViewModel>();
            fluent.SetObjectDataSourceBinding(contractInfoBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
