﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels.Expert;

namespace GSC.Client.Modules.Database.Experts
{
    public partial class ExpertAgreementControl : XtraUserControl
    {
        private const string Category = "Expert Agreement";

        private static readonly object EventAddVisibleChanged = new object();
        private static readonly object EventRemoveVisibleChanged = new object();
        private static readonly object EventPreviewVisibleChanged = new object();
        private static readonly object EventMessageChangedChanged = new object();

        private readonly EventHandlerList handlers = new EventHandlerList();
        private Color backColor = Color.FromArgb(255, 255, 192);
        private Color foreColor = Color.Black;
        private bool addVisible = true;
        private bool removeVisible = true;
        private bool previewVisible = true;
        private string message = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod";

        public ExpertAgreementControl()
        {
            InitializeComponent();
        }

        [Category(Category)]
        [DefaultValue(typeof(Color), "255, 255, 192")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override Color BackColor
        {
            get { return backColor; }
            set
            {
                if (backColor != value)
                {
                    backColor = value;
                    OnBackColorChanged(EventArgs.Empty);
                }
            }
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            tableLayoutPanel1.BackColor = backColor;
        }

        [Category(Category)]
        [DefaultValue(typeof(Color), "0, 0, 0")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override Color ForeColor
        {
            get { return foreColor; }
            set
            {
                if (foreColor != value)
                {
                    foreColor = value;
                    OnForeColorChanged(EventArgs.Empty);
                }
            }
        }

        [Category(Category)]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool RemoveVisible
        {
            get { return removeVisible; }
            set
            {
                if (removeVisible != value)
                {
                    removeVisible = value;
                    OnRemoveVisibleChanged(EventArgs.Empty);
                }
            }
        }

        protected virtual void OnRemoveVisibleChanged(EventArgs e)
        {
            linkRemove.Visible = removeVisible;
            var handler = (EventHandler)handlers[EventRemoveVisibleChanged];
            handler?.Invoke(this, e);
        }

        [Category(Category)]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool PreviewVisible
        {
            get { return previewVisible; }
            set
            {
                if (previewVisible != value)
                {
                    previewVisible = value;
                    OnPreviewVisibleChanged(EventArgs.Empty);
                }
            }
        }

        protected virtual void OnPreviewVisibleChanged(EventArgs e)
        {
            linkPreview.Visible = previewVisible;
            var handler = (EventHandler)handlers[EventPreviewVisibleChanged];
            handler?.Invoke(this, e);
        }

        [Category(Category)]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool AddVisible
        {
            get { return addVisible; }
            set
            {
                if (addVisible != value)
                {
                    addVisible = value;
                    OnAddVisibleChanged(EventArgs.Empty);
                }
            }
        }

        protected virtual void OnAddVisibleChanged(EventArgs e)
        {
            linkAdd.Visible = addVisible;
            var handler = (EventHandler)handlers[EventAddVisibleChanged];
            handler?.Invoke(this, e);
        }

        [Category(Category)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler PreviewClick
        {
            add { linkPreview.Click += value; }
            remove { linkPreview.Click -= value; }
        }

        [Category(Category)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler AddClick
        {
            add { linkAdd.Click += value; }
            remove { linkAdd.Click -= value; }
        }

        [Category(Category)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler RemoveClick
        {
            add { linkRemove.Click += value; }
            remove { linkRemove.Click -= value; }
        }

        [Category(Category)]
        [DefaultValue("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string Message
        {
            get { return message; }
            set
            {
                if (message != value)
                {
                    message = value;
                    OnMessageChanged(EventArgs.Empty);
                }
            }
        }

        protected virtual void OnMessageChanged(EventArgs e)
        {
            labelMessage.Text = message;

            var handler = (EventHandler)handlers[EventMessageChangedChanged];
            handler?.Invoke(this, e);
        }
    }
}
