﻿namespace GSC.Client.Modules.Database.Experts
{
    partial class ExpertView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ImportCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.expertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportsCountValueEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PatronymicTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WorkplaceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BirthDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.BirthPlaceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LiveAddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ExpirienceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SpecialityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AwardsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefPostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChiefEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ExpertKindCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.CanMissionCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.InterviewStatusCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.EngageCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.EfficiencyCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.NextPeriodProposalCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.BaseRegionIdTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ObserverIdTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.observerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AcademicDegreeCodeTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.academicDegreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AcademicTitleCodeTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.academicTitleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Stage1PointsEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Stage2PointsEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TestResultsEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WorkYearsChiefTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WorkYearsTotalTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WorkYearsEducationTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PostValueCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AcademicDegreeValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AcademicTitleValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SpecialityValueCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.WorkYearsEducationValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ExpYearsEducationValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AwardsValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CanMissionValueTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TestResultsValueCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.InterviewStatusValueCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SubjectLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.AccuracyLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.PunctualLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.IndependencyLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.MoralLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.DocsImprovementParticipationLevelValueEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ExpYearsEducationTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ExpertDocStatTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForImportCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPatronymic = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkplace = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBirthDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBirthPlace = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBaseRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLiveAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpirience = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkYearsTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkYearsChief = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChiefEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcademicDegreeCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcademicDegreeValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcademicTitleCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcademicTitleValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpeciality = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpecialityValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkYearsEducation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkYearsEducationValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpYearsEducation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpYearsEducationValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAwards = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAwardsValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCanMission = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCanMissionValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpertDocStat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForObserverId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTestResults = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInterviewStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStage1points = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKind = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubjectLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccuracyLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPunctualLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIndependencyLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMoralLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDocsImprovementParticipationLevelValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStage2points = new DevExpress.XtraLayout.LayoutControlItem();
            this.EfficiencyComboGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTestResultsValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInterviewStatusValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEngage = new DevExpress.XtraLayout.LayoutControlItem();
            this.NextPeriodProposalComboGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.expertAgreementControl1 = new GSC.Client.Modules.Database.Experts.ExpertAgreementControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImportCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsCountValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatronymicTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthPlaceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveAddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpirienceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwardsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpertKindCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CanMissionCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterviewStatusCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngageCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfficiencyCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextPeriodProposalCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseRegionIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObserverIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.observerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicDegreeCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.academicDegreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicTitleCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.academicTitleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Stage1PointsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Stage2PointsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestResultsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsChiefTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsTotalTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsEducationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostValueCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicDegreeValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicTitleValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialityValueCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsEducationValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpYearsEducationValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwardsValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CanMissionValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestResultsValueCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterviewStatusValueCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccuracyLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PunctualLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IndependencyLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoralLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocsImprovementParticipationLevelValueEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpYearsEducationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpertDocStatTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImportCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPatronymic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthPlace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBaseRegionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLiveAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpirience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsChief)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicDegreeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicDegreeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicTitleCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicTitleValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialityValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsEducation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsEducationValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpYearsEducation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpYearsEducationValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAwards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAwardsValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCanMission)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCanMissionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertDocStat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObserverId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTestResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInterviewStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStage1points)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubjectLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccuracyLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPunctualLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIndependencyLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMoralLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocsImprovementParticipationLevelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStage2points)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfficiencyComboGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTestResultsValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInterviewStatusValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextPeriodProposalComboGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(801, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Expert.ExpertViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ImportCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.reportsCountValueEdit);
            this.dataLayoutControl1.Controls.Add(this.SurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PatronymicTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkplaceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BirthDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.BirthPlaceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LiveAddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpirienceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SpecialityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AwardsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefPostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChiefEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpertKindCombo);
            this.dataLayoutControl1.Controls.Add(this.CanMissionCombo);
            this.dataLayoutControl1.Controls.Add(this.InterviewStatusCombo);
            this.dataLayoutControl1.Controls.Add(this.EngageCombo);
            this.dataLayoutControl1.Controls.Add(this.EfficiencyCombo);
            this.dataLayoutControl1.Controls.Add(this.NextPeriodProposalCombo);
            this.dataLayoutControl1.Controls.Add(this.BaseRegionIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ObserverIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AcademicDegreeCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AcademicTitleCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.Stage1PointsEdit);
            this.dataLayoutControl1.Controls.Add(this.Stage2PointsEdit);
            this.dataLayoutControl1.Controls.Add(this.TestResultsEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkYearsChiefTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkYearsTotalTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkYearsEducationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostValueCombo);
            this.dataLayoutControl1.Controls.Add(this.AcademicDegreeValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AcademicTitleValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SpecialityValueCombo);
            this.dataLayoutControl1.Controls.Add(this.WorkYearsEducationValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpYearsEducationValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AwardsValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CanMissionValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TestResultsValueCombo);
            this.dataLayoutControl1.Controls.Add(this.InterviewStatusValueCombo);
            this.dataLayoutControl1.Controls.Add(this.SubjectLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.AccuracyLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.PunctualLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.IndependencyLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.MoralLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.DocsImprovementParticipationLevelValueEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpYearsEducationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpertDocStatTextEdit);
            this.dataLayoutControl1.DataSource = this.expertBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForImportCode});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 165);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(50, 308, 623, 602);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(801, 470);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ImportCodeTextEdit
            // 
            this.ImportCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "ImportCode", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ImportCodeTextEdit.Location = new System.Drawing.Point(233, 414);
            this.ImportCodeTextEdit.MenuManager = this.ribbonControl1;
            this.ImportCodeTextEdit.Name = "ImportCodeTextEdit";
            this.ImportCodeTextEdit.Size = new System.Drawing.Size(544, 20);
            this.ImportCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.ImportCodeTextEdit.TabIndex = 4;
            // 
            // expertBindingSource
            // 
            this.expertBindingSource.DataSource = typeof(GSC.Model.Expert);
            this.expertBindingSource.DataSourceChanged += new System.EventHandler(this.expertBindingSource_DataSourceChanged);
            // 
            // reportsCountValueEdit
            // 
            this.reportsCountValueEdit.Location = new System.Drawing.Point(706, 262);
            this.reportsCountValueEdit.MenuManager = this.ribbonControl1;
            this.reportsCountValueEdit.Name = "reportsCountValueEdit";
            this.reportsCountValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.reportsCountValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.reportsCountValueEdit.Properties.NullText = "0";
            this.reportsCountValueEdit.Properties.ReadOnly = true;
            this.reportsCountValueEdit.Size = new System.Drawing.Size(71, 20);
            this.reportsCountValueEdit.StyleController = this.dataLayoutControl1;
            this.reportsCountValueEdit.TabIndex = 55;
            // 
            // SurnameTextEdit
            // 
            this.SurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Surname", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SurnameTextEdit.Location = new System.Drawing.Point(233, 46);
            this.SurnameTextEdit.MenuManager = this.ribbonControl1;
            this.SurnameTextEdit.Name = "SurnameTextEdit";
            this.SurnameTextEdit.Size = new System.Drawing.Size(172, 20);
            this.SurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.SurnameTextEdit.TabIndex = 5;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(233, 70);
            this.NameTextEdit.MenuManager = this.ribbonControl1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Size = new System.Drawing.Size(172, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 6;
            // 
            // PatronymicTextEdit
            // 
            this.PatronymicTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Patronymic", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PatronymicTextEdit.Location = new System.Drawing.Point(233, 94);
            this.PatronymicTextEdit.MenuManager = this.ribbonControl1;
            this.PatronymicTextEdit.Name = "PatronymicTextEdit";
            this.PatronymicTextEdit.Size = new System.Drawing.Size(172, 20);
            this.PatronymicTextEdit.StyleController = this.dataLayoutControl1;
            this.PatronymicTextEdit.TabIndex = 7;
            // 
            // WorkplaceTextEdit
            // 
            this.WorkplaceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Workplace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WorkplaceTextEdit.Location = new System.Drawing.Point(233, 118);
            this.WorkplaceTextEdit.MenuManager = this.ribbonControl1;
            this.WorkplaceTextEdit.Name = "WorkplaceTextEdit";
            this.WorkplaceTextEdit.Size = new System.Drawing.Size(172, 20);
            this.WorkplaceTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkplaceTextEdit.TabIndex = 8;
            // 
            // PostTextEdit
            // 
            this.PostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Post", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PostTextEdit.Location = new System.Drawing.Point(628, 70);
            this.PostTextEdit.MenuManager = this.ribbonControl1;
            this.PostTextEdit.Name = "PostTextEdit";
            this.PostTextEdit.Size = new System.Drawing.Size(74, 20);
            this.PostTextEdit.StyleController = this.dataLayoutControl1;
            this.PostTextEdit.TabIndex = 9;
            // 
            // BirthDateDateEdit
            // 
            this.BirthDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "BirthDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BirthDateDateEdit.EditValue = null;
            this.BirthDateDateEdit.Location = new System.Drawing.Point(233, 142);
            this.BirthDateDateEdit.MenuManager = this.ribbonControl1;
            this.BirthDateDateEdit.Name = "BirthDateDateEdit";
            this.BirthDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BirthDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BirthDateDateEdit.Size = new System.Drawing.Size(172, 20);
            this.BirthDateDateEdit.StyleController = this.dataLayoutControl1;
            this.BirthDateDateEdit.TabIndex = 11;
            // 
            // BirthPlaceTextEdit
            // 
            this.BirthPlaceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "BirthPlace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BirthPlaceTextEdit.Location = new System.Drawing.Point(233, 166);
            this.BirthPlaceTextEdit.MenuManager = this.ribbonControl1;
            this.BirthPlaceTextEdit.Name = "BirthPlaceTextEdit";
            this.BirthPlaceTextEdit.Size = new System.Drawing.Size(172, 20);
            this.BirthPlaceTextEdit.StyleController = this.dataLayoutControl1;
            this.BirthPlaceTextEdit.TabIndex = 12;
            // 
            // LiveAddressTextEdit
            // 
            this.LiveAddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "LiveAddress", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.LiveAddressTextEdit.Location = new System.Drawing.Point(233, 214);
            this.LiveAddressTextEdit.MenuManager = this.ribbonControl1;
            this.LiveAddressTextEdit.Name = "LiveAddressTextEdit";
            this.LiveAddressTextEdit.Size = new System.Drawing.Size(172, 20);
            this.LiveAddressTextEdit.StyleController = this.dataLayoutControl1;
            this.LiveAddressTextEdit.TabIndex = 13;
            // 
            // PhoneTextEdit
            // 
            this.PhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Phone", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PhoneTextEdit.Location = new System.Drawing.Point(233, 238);
            this.PhoneTextEdit.MenuManager = this.ribbonControl1;
            this.PhoneTextEdit.Name = "PhoneTextEdit";
            this.PhoneTextEdit.Size = new System.Drawing.Size(172, 20);
            this.PhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PhoneTextEdit.TabIndex = 14;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.Location = new System.Drawing.Point(233, 262);
            this.EmailTextEdit.MenuManager = this.ribbonControl1;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Properties.Mask.EditMask = "\\S+@\\S+\\.\\S+";
            this.EmailTextEdit.Properties.Mask.IgnoreMaskBlank = false;
            this.EmailTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.EmailTextEdit.Properties.Mask.ShowPlaceHolders = false;
            this.EmailTextEdit.Size = new System.Drawing.Size(172, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 15;
            // 
            // ExpirienceTextEdit
            // 
            this.ExpirienceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Expirience", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExpirienceTextEdit.Location = new System.Drawing.Point(233, 286);
            this.ExpirienceTextEdit.MenuManager = this.ribbonControl1;
            this.ExpirienceTextEdit.Name = "ExpirienceTextEdit";
            this.ExpirienceTextEdit.Size = new System.Drawing.Size(172, 20);
            this.ExpirienceTextEdit.StyleController = this.dataLayoutControl1;
            this.ExpirienceTextEdit.TabIndex = 20;
            // 
            // SpecialityTextEdit
            // 
            this.SpecialityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "Speciality", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SpecialityTextEdit.Location = new System.Drawing.Point(628, 142);
            this.SpecialityTextEdit.MenuManager = this.ribbonControl1;
            this.SpecialityTextEdit.Name = "SpecialityTextEdit";
            this.SpecialityTextEdit.Size = new System.Drawing.Size(74, 20);
            this.SpecialityTextEdit.StyleController = this.dataLayoutControl1;
            this.SpecialityTextEdit.TabIndex = 21;
            // 
            // AwardsTextEdit
            // 
            this.AwardsTextEdit.Location = new System.Drawing.Point(628, 214);
            this.AwardsTextEdit.MenuManager = this.ribbonControl1;
            this.AwardsTextEdit.Name = "AwardsTextEdit";
            this.AwardsTextEdit.Size = new System.Drawing.Size(74, 20);
            this.AwardsTextEdit.StyleController = this.dataLayoutControl1;
            this.AwardsTextEdit.TabIndex = 29;
            // 
            // ChiefNameTextEdit
            // 
            this.ChiefNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "ChiefName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefNameTextEdit.Location = new System.Drawing.Point(233, 358);
            this.ChiefNameTextEdit.MenuManager = this.ribbonControl1;
            this.ChiefNameTextEdit.Name = "ChiefNameTextEdit";
            this.ChiefNameTextEdit.Size = new System.Drawing.Size(172, 20);
            this.ChiefNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefNameTextEdit.TabIndex = 32;
            // 
            // ChiefPostTextEdit
            // 
            this.ChiefPostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "ChiefPost", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefPostTextEdit.Location = new System.Drawing.Point(233, 382);
            this.ChiefPostTextEdit.MenuManager = this.ribbonControl1;
            this.ChiefPostTextEdit.Name = "ChiefPostTextEdit";
            this.ChiefPostTextEdit.Size = new System.Drawing.Size(172, 20);
            this.ChiefPostTextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefPostTextEdit.TabIndex = 33;
            // 
            // ChiefEmailTextEdit
            // 
            this.ChiefEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "ChiefEmail", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ChiefEmailTextEdit.Location = new System.Drawing.Point(233, 406);
            this.ChiefEmailTextEdit.MenuManager = this.ribbonControl1;
            this.ChiefEmailTextEdit.Name = "ChiefEmailTextEdit";
            this.ChiefEmailTextEdit.Size = new System.Drawing.Size(172, 20);
            this.ChiefEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.ChiefEmailTextEdit.TabIndex = 34;
            // 
            // ExpertKindCombo
            // 
            this.ExpertKindCombo.Location = new System.Drawing.Point(233, 118);
            this.ExpertKindCombo.MenuManager = this.ribbonControl1;
            this.ExpertKindCombo.Name = "ExpertKindCombo";
            this.ExpertKindCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpertKindCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExpertKindCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpertKindCombo.Properties.UseCtrlScroll = true;
            this.ExpertKindCombo.Size = new System.Drawing.Size(544, 20);
            this.ExpertKindCombo.StyleController = this.dataLayoutControl1;
            this.ExpertKindCombo.TabIndex = 35;
            // 
            // CanMissionCombo
            // 
            this.CanMissionCombo.Location = new System.Drawing.Point(628, 238);
            this.CanMissionCombo.MenuManager = this.ribbonControl1;
            this.CanMissionCombo.Name = "CanMissionCombo";
            this.CanMissionCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.CanMissionCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CanMissionCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CanMissionCombo.Properties.UseCtrlScroll = true;
            this.CanMissionCombo.Size = new System.Drawing.Size(74, 20);
            this.CanMissionCombo.StyleController = this.dataLayoutControl1;
            this.CanMissionCombo.TabIndex = 36;
            // 
            // InterviewStatusCombo
            // 
            this.InterviewStatusCombo.Location = new System.Drawing.Point(233, 70);
            this.InterviewStatusCombo.MenuManager = this.ribbonControl1;
            this.InterviewStatusCombo.Name = "InterviewStatusCombo";
            this.InterviewStatusCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InterviewStatusCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.InterviewStatusCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InterviewStatusCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InterviewStatusCombo.Properties.UseCtrlScroll = true;
            this.InterviewStatusCombo.Size = new System.Drawing.Size(165, 20);
            this.InterviewStatusCombo.StyleController = this.dataLayoutControl1;
            this.InterviewStatusCombo.TabIndex = 41;
            // 
            // EngageCombo
            // 
            this.EngageCombo.Location = new System.Drawing.Point(621, 94);
            this.EngageCombo.MenuManager = this.ribbonControl1;
            this.EngageCombo.Name = "EngageCombo";
            this.EngageCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EngageCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.EngageCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.EngageCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EngageCombo.Properties.UseCtrlScroll = true;
            this.EngageCombo.Size = new System.Drawing.Size(156, 20);
            this.EngageCombo.StyleController = this.dataLayoutControl1;
            this.EngageCombo.TabIndex = 44;
            // 
            // EfficiencyCombo
            // 
            this.EfficiencyCombo.Location = new System.Drawing.Point(233, 310);
            this.EfficiencyCombo.MenuManager = this.ribbonControl1;
            this.EfficiencyCombo.Name = "EfficiencyCombo";
            this.EfficiencyCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EfficiencyCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.EfficiencyCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.EfficiencyCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EfficiencyCombo.Properties.UseCtrlScroll = true;
            this.EfficiencyCombo.Size = new System.Drawing.Size(165, 20);
            this.EfficiencyCombo.StyleController = this.dataLayoutControl1;
            this.EfficiencyCombo.TabIndex = 52;
            // 
            // NextPeriodProposalCombo
            // 
            this.NextPeriodProposalCombo.Location = new System.Drawing.Point(621, 310);
            this.NextPeriodProposalCombo.MenuManager = this.ribbonControl1;
            this.NextPeriodProposalCombo.Name = "NextPeriodProposalCombo";
            this.NextPeriodProposalCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.NextPeriodProposalCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.NextPeriodProposalCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NextPeriodProposalCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NextPeriodProposalCombo.Properties.UseCtrlScroll = true;
            this.NextPeriodProposalCombo.Size = new System.Drawing.Size(156, 20);
            this.NextPeriodProposalCombo.StyleController = this.dataLayoutControl1;
            this.NextPeriodProposalCombo.TabIndex = 53;
            // 
            // BaseRegionIdTextEdit
            // 
            this.BaseRegionIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "BaseRegionId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BaseRegionIdTextEdit.Location = new System.Drawing.Point(233, 190);
            this.BaseRegionIdTextEdit.MenuManager = this.ribbonControl1;
            this.BaseRegionIdTextEdit.Name = "BaseRegionIdTextEdit";
            this.BaseRegionIdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BaseRegionIdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BaseRegionIdTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BaseRegionIdTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.BaseRegionIdTextEdit.Properties.DataSource = this.regionBindingSource;
            this.BaseRegionIdTextEdit.Properties.DisplayMember = "Name";
            this.BaseRegionIdTextEdit.Properties.NullText = "";
            this.BaseRegionIdTextEdit.Properties.ValueMember = "Id";
            this.BaseRegionIdTextEdit.Size = new System.Drawing.Size(172, 20);
            this.BaseRegionIdTextEdit.StyleController = this.dataLayoutControl1;
            this.BaseRegionIdTextEdit.TabIndex = 37;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // ObserverIdTextEdit
            // 
            this.ObserverIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "ObserverId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ObserverIdTextEdit.Location = new System.Drawing.Point(628, 46);
            this.ObserverIdTextEdit.MenuManager = this.ribbonControl1;
            this.ObserverIdTextEdit.Name = "ObserverIdTextEdit";
            this.ObserverIdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ObserverIdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ObserverIdTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ObserverIdTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fio", "ФИО", 24, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.ObserverIdTextEdit.Properties.DataSource = this.observerBindingSource;
            this.ObserverIdTextEdit.Properties.DisplayMember = "Fio";
            this.ObserverIdTextEdit.Properties.NullText = "";
            this.ObserverIdTextEdit.Properties.ValueMember = "Id";
            this.ObserverIdTextEdit.Size = new System.Drawing.Size(149, 20);
            this.ObserverIdTextEdit.StyleController = this.dataLayoutControl1;
            this.ObserverIdTextEdit.TabIndex = 38;
            // 
            // observerBindingSource
            // 
            this.observerBindingSource.DataSource = typeof(GSC.Model.Observer);
            // 
            // AcademicDegreeCodeTextEdit
            // 
            this.AcademicDegreeCodeTextEdit.Location = new System.Drawing.Point(628, 94);
            this.AcademicDegreeCodeTextEdit.MenuManager = this.ribbonControl1;
            this.AcademicDegreeCodeTextEdit.Name = "AcademicDegreeCodeTextEdit";
            this.AcademicDegreeCodeTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.AcademicDegreeCodeTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcademicDegreeCodeTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 48, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.AcademicDegreeCodeTextEdit.Properties.DataSource = this.academicDegreeBindingSource;
            this.AcademicDegreeCodeTextEdit.Properties.DisplayMember = "Name";
            this.AcademicDegreeCodeTextEdit.Properties.NullText = "<Не выбрано>";
            this.AcademicDegreeCodeTextEdit.Properties.ValueMember = "Id";
            this.AcademicDegreeCodeTextEdit.Size = new System.Drawing.Size(74, 20);
            this.AcademicDegreeCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.AcademicDegreeCodeTextEdit.TabIndex = 16;
            // 
            // academicDegreeBindingSource
            // 
            this.academicDegreeBindingSource.DataSource = typeof(GSC.Model.AcademicDegree);
            // 
            // AcademicTitleCodeTextEdit
            // 
            this.AcademicTitleCodeTextEdit.Location = new System.Drawing.Point(628, 118);
            this.AcademicTitleCodeTextEdit.MenuManager = this.ribbonControl1;
            this.AcademicTitleCodeTextEdit.Name = "AcademicTitleCodeTextEdit";
            this.AcademicTitleCodeTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.AcademicTitleCodeTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcademicTitleCodeTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 48, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Наименование", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.AcademicTitleCodeTextEdit.Properties.DataSource = this.academicTitleBindingSource;
            this.AcademicTitleCodeTextEdit.Properties.DisplayMember = "Name";
            this.AcademicTitleCodeTextEdit.Properties.NullText = "<Не выбрано>";
            this.AcademicTitleCodeTextEdit.Properties.ValueMember = "Id";
            this.AcademicTitleCodeTextEdit.Size = new System.Drawing.Size(74, 20);
            this.AcademicTitleCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.AcademicTitleCodeTextEdit.TabIndex = 18;
            // 
            // academicTitleBindingSource
            // 
            this.academicTitleBindingSource.DataSource = typeof(GSC.Model.AcademicTitle);
            // 
            // Stage1PointsEdit
            // 
            this.Stage1PointsEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Stage1PointsEdit.Location = new System.Drawing.Point(233, 94);
            this.Stage1PointsEdit.MenuManager = this.ribbonControl1;
            this.Stage1PointsEdit.Name = "Stage1PointsEdit";
            this.Stage1PointsEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.Stage1PointsEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.Stage1PointsEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Stage1PointsEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Stage1PointsEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.Stage1PointsEdit.Properties.Mask.EditMask = "N0";
            this.Stage1PointsEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Stage1PointsEdit.Properties.MaxValue = new decimal(new int[] {
            100500,
            0,
            0,
            0});
            this.Stage1PointsEdit.Size = new System.Drawing.Size(165, 20);
            this.Stage1PointsEdit.StyleController = this.dataLayoutControl1;
            this.Stage1PointsEdit.TabIndex = 43;
            // 
            // Stage2PointsEdit
            // 
            this.Stage2PointsEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Stage2PointsEdit.Location = new System.Drawing.Point(233, 286);
            this.Stage2PointsEdit.MenuManager = this.ribbonControl1;
            this.Stage2PointsEdit.Name = "Stage2PointsEdit";
            this.Stage2PointsEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.Stage2PointsEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.Stage2PointsEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Stage2PointsEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Stage2PointsEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.Stage2PointsEdit.Properties.Mask.EditMask = "N0";
            this.Stage2PointsEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Stage2PointsEdit.Properties.MaxValue = new decimal(new int[] {
            100500,
            0,
            0,
            0});
            this.Stage2PointsEdit.Size = new System.Drawing.Size(544, 20);
            this.Stage2PointsEdit.StyleController = this.dataLayoutControl1;
            this.Stage2PointsEdit.TabIndex = 51;
            // 
            // TestResultsEdit
            // 
            this.TestResultsEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TestResultsEdit.Location = new System.Drawing.Point(233, 46);
            this.TestResultsEdit.MenuManager = this.ribbonControl1;
            this.TestResultsEdit.Name = "TestResultsEdit";
            this.TestResultsEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TestResultsEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TestResultsEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TestResultsEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TestResultsEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TestResultsEdit.Properties.IsFloatValue = false;
            this.TestResultsEdit.Properties.Mask.EditMask = "N0";
            this.TestResultsEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TestResultsEdit.Properties.MaxValue = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.TestResultsEdit.Size = new System.Drawing.Size(165, 20);
            this.TestResultsEdit.StyleController = this.dataLayoutControl1;
            this.TestResultsEdit.TabIndex = 39;
            // 
            // WorkYearsChiefTextEdit
            // 
            this.WorkYearsChiefTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "WorkYearsChief", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WorkYearsChiefTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WorkYearsChiefTextEdit.Location = new System.Drawing.Point(233, 334);
            this.WorkYearsChiefTextEdit.MenuManager = this.ribbonControl1;
            this.WorkYearsChiefTextEdit.Name = "WorkYearsChiefTextEdit";
            this.WorkYearsChiefTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WorkYearsChiefTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WorkYearsChiefTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WorkYearsChiefTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkYearsChiefTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.WorkYearsChiefTextEdit.Properties.Mask.EditMask = "N0";
            this.WorkYearsChiefTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WorkYearsChiefTextEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.WorkYearsChiefTextEdit.Size = new System.Drawing.Size(172, 20);
            this.WorkYearsChiefTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkYearsChiefTextEdit.TabIndex = 26;
            // 
            // WorkYearsTotalTextEdit
            // 
            this.WorkYearsTotalTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.expertBindingSource, "WorkYearsTotal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WorkYearsTotalTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WorkYearsTotalTextEdit.Location = new System.Drawing.Point(233, 310);
            this.WorkYearsTotalTextEdit.MenuManager = this.ribbonControl1;
            this.WorkYearsTotalTextEdit.Name = "WorkYearsTotalTextEdit";
            this.WorkYearsTotalTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WorkYearsTotalTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WorkYearsTotalTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkYearsTotalTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.WorkYearsTotalTextEdit.Properties.Mask.EditMask = "N0";
            this.WorkYearsTotalTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WorkYearsTotalTextEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.WorkYearsTotalTextEdit.Size = new System.Drawing.Size(172, 20);
            this.WorkYearsTotalTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkYearsTotalTextEdit.TabIndex = 23;
            // 
            // WorkYearsEducationTextEdit
            // 
            this.WorkYearsEducationTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WorkYearsEducationTextEdit.Location = new System.Drawing.Point(628, 166);
            this.WorkYearsEducationTextEdit.MenuManager = this.ribbonControl1;
            this.WorkYearsEducationTextEdit.Name = "WorkYearsEducationTextEdit";
            this.WorkYearsEducationTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WorkYearsEducationTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WorkYearsEducationTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkYearsEducationTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.WorkYearsEducationTextEdit.Properties.Mask.EditMask = "N0";
            this.WorkYearsEducationTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WorkYearsEducationTextEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.WorkYearsEducationTextEdit.Size = new System.Drawing.Size(74, 20);
            this.WorkYearsEducationTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkYearsEducationTextEdit.TabIndex = 24;
            // 
            // PostValueCombo
            // 
            this.PostValueCombo.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PostValueCombo.Location = new System.Drawing.Point(706, 70);
            this.PostValueCombo.MenuManager = this.ribbonControl1;
            this.PostValueCombo.Name = "PostValueCombo";
            this.PostValueCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.PostValueCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PostValueCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PostValueCombo.Size = new System.Drawing.Size(71, 20);
            this.PostValueCombo.StyleController = this.dataLayoutControl1;
            this.PostValueCombo.TabIndex = 10;
            // 
            // AcademicDegreeValueTextEdit
            // 
            this.AcademicDegreeValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcademicDegreeValueTextEdit.Location = new System.Drawing.Point(706, 94);
            this.AcademicDegreeValueTextEdit.MenuManager = this.ribbonControl1;
            this.AcademicDegreeValueTextEdit.Name = "AcademicDegreeValueTextEdit";
            this.AcademicDegreeValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AcademicDegreeValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AcademicDegreeValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcademicDegreeValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.AcademicDegreeValueTextEdit.StyleController = this.dataLayoutControl1;
            this.AcademicDegreeValueTextEdit.TabIndex = 17;
            // 
            // AcademicTitleValueTextEdit
            // 
            this.AcademicTitleValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcademicTitleValueTextEdit.Location = new System.Drawing.Point(706, 118);
            this.AcademicTitleValueTextEdit.MenuManager = this.ribbonControl1;
            this.AcademicTitleValueTextEdit.Name = "AcademicTitleValueTextEdit";
            this.AcademicTitleValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AcademicTitleValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AcademicTitleValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcademicTitleValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.AcademicTitleValueTextEdit.StyleController = this.dataLayoutControl1;
            this.AcademicTitleValueTextEdit.TabIndex = 19;
            // 
            // SpecialityValueCombo
            // 
            this.SpecialityValueCombo.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpecialityValueCombo.Location = new System.Drawing.Point(706, 142);
            this.SpecialityValueCombo.MenuManager = this.ribbonControl1;
            this.SpecialityValueCombo.Name = "SpecialityValueCombo";
            this.SpecialityValueCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.SpecialityValueCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SpecialityValueCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpecialityValueCombo.Size = new System.Drawing.Size(71, 20);
            this.SpecialityValueCombo.StyleController = this.dataLayoutControl1;
            this.SpecialityValueCombo.TabIndex = 22;
            // 
            // WorkYearsEducationValueTextEdit
            // 
            this.WorkYearsEducationValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WorkYearsEducationValueTextEdit.Location = new System.Drawing.Point(706, 166);
            this.WorkYearsEducationValueTextEdit.MenuManager = this.ribbonControl1;
            this.WorkYearsEducationValueTextEdit.Name = "WorkYearsEducationValueTextEdit";
            this.WorkYearsEducationValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WorkYearsEducationValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WorkYearsEducationValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkYearsEducationValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.WorkYearsEducationValueTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkYearsEducationValueTextEdit.TabIndex = 25;
            // 
            // ExpYearsEducationValueTextEdit
            // 
            this.ExpYearsEducationValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpYearsEducationValueTextEdit.Location = new System.Drawing.Point(706, 190);
            this.ExpYearsEducationValueTextEdit.MenuManager = this.ribbonControl1;
            this.ExpYearsEducationValueTextEdit.Name = "ExpYearsEducationValueTextEdit";
            this.ExpYearsEducationValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpYearsEducationValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExpYearsEducationValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpYearsEducationValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.ExpYearsEducationValueTextEdit.StyleController = this.dataLayoutControl1;
            this.ExpYearsEducationValueTextEdit.TabIndex = 28;
            // 
            // AwardsValueTextEdit
            // 
            this.AwardsValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AwardsValueTextEdit.Location = new System.Drawing.Point(706, 214);
            this.AwardsValueTextEdit.MenuManager = this.ribbonControl1;
            this.AwardsValueTextEdit.Name = "AwardsValueTextEdit";
            this.AwardsValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AwardsValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AwardsValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AwardsValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.AwardsValueTextEdit.StyleController = this.dataLayoutControl1;
            this.AwardsValueTextEdit.TabIndex = 30;
            // 
            // CanMissionValueTextEdit
            // 
            this.CanMissionValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CanMissionValueTextEdit.Location = new System.Drawing.Point(706, 238);
            this.CanMissionValueTextEdit.MenuManager = this.ribbonControl1;
            this.CanMissionValueTextEdit.Name = "CanMissionValueTextEdit";
            this.CanMissionValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CanMissionValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CanMissionValueTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CanMissionValueTextEdit.Size = new System.Drawing.Size(71, 20);
            this.CanMissionValueTextEdit.StyleController = this.dataLayoutControl1;
            this.CanMissionValueTextEdit.TabIndex = 31;
            // 
            // TestResultsValueCombo
            // 
            this.TestResultsValueCombo.Location = new System.Drawing.Point(621, 46);
            this.TestResultsValueCombo.MenuManager = this.ribbonControl1;
            this.TestResultsValueCombo.Name = "TestResultsValueCombo";
            this.TestResultsValueCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TestResultsValueCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.TestResultsValueCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TestResultsValueCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TestResultsValueCombo.Size = new System.Drawing.Size(156, 20);
            this.TestResultsValueCombo.StyleController = this.dataLayoutControl1;
            this.TestResultsValueCombo.TabIndex = 40;
            // 
            // InterviewStatusValueCombo
            // 
            this.InterviewStatusValueCombo.Location = new System.Drawing.Point(621, 70);
            this.InterviewStatusValueCombo.MenuManager = this.ribbonControl1;
            this.InterviewStatusValueCombo.Name = "InterviewStatusValueCombo";
            this.InterviewStatusValueCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InterviewStatusValueCombo.Properties.Appearance.Options.UseTextOptions = true;
            this.InterviewStatusValueCombo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.InterviewStatusValueCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InterviewStatusValueCombo.Size = new System.Drawing.Size(156, 20);
            this.InterviewStatusValueCombo.StyleController = this.dataLayoutControl1;
            this.InterviewStatusValueCombo.TabIndex = 42;
            // 
            // SubjectLevelValueEdit
            // 
            this.SubjectLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SubjectLevelValueEdit.Location = new System.Drawing.Point(233, 142);
            this.SubjectLevelValueEdit.MenuManager = this.ribbonControl1;
            this.SubjectLevelValueEdit.Name = "SubjectLevelValueEdit";
            this.SubjectLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SubjectLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SubjectLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubjectLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.SubjectLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.SubjectLevelValueEdit.TabIndex = 45;
            // 
            // AccuracyLevelValueEdit
            // 
            this.AccuracyLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AccuracyLevelValueEdit.Location = new System.Drawing.Point(233, 166);
            this.AccuracyLevelValueEdit.MenuManager = this.ribbonControl1;
            this.AccuracyLevelValueEdit.Name = "AccuracyLevelValueEdit";
            this.AccuracyLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AccuracyLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AccuracyLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccuracyLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.AccuracyLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.AccuracyLevelValueEdit.TabIndex = 46;
            // 
            // PunctualLevelValueEdit
            // 
            this.PunctualLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PunctualLevelValueEdit.Location = new System.Drawing.Point(233, 190);
            this.PunctualLevelValueEdit.MenuManager = this.ribbonControl1;
            this.PunctualLevelValueEdit.Name = "PunctualLevelValueEdit";
            this.PunctualLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PunctualLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PunctualLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PunctualLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.PunctualLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.PunctualLevelValueEdit.TabIndex = 47;
            // 
            // IndependencyLevelValueEdit
            // 
            this.IndependencyLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.IndependencyLevelValueEdit.Location = new System.Drawing.Point(233, 214);
            this.IndependencyLevelValueEdit.MenuManager = this.ribbonControl1;
            this.IndependencyLevelValueEdit.Name = "IndependencyLevelValueEdit";
            this.IndependencyLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.IndependencyLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.IndependencyLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IndependencyLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.IndependencyLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.IndependencyLevelValueEdit.TabIndex = 48;
            // 
            // MoralLevelValueEdit
            // 
            this.MoralLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MoralLevelValueEdit.Location = new System.Drawing.Point(233, 238);
            this.MoralLevelValueEdit.MenuManager = this.ribbonControl1;
            this.MoralLevelValueEdit.Name = "MoralLevelValueEdit";
            this.MoralLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.MoralLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MoralLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MoralLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.MoralLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.MoralLevelValueEdit.TabIndex = 49;
            // 
            // DocsImprovementParticipationLevelValueEdit
            // 
            this.DocsImprovementParticipationLevelValueEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DocsImprovementParticipationLevelValueEdit.Location = new System.Drawing.Point(233, 262);
            this.DocsImprovementParticipationLevelValueEdit.MenuManager = this.ribbonControl1;
            this.DocsImprovementParticipationLevelValueEdit.Name = "DocsImprovementParticipationLevelValueEdit";
            this.DocsImprovementParticipationLevelValueEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DocsImprovementParticipationLevelValueEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DocsImprovementParticipationLevelValueEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DocsImprovementParticipationLevelValueEdit.Size = new System.Drawing.Size(544, 20);
            this.DocsImprovementParticipationLevelValueEdit.StyleController = this.dataLayoutControl1;
            this.DocsImprovementParticipationLevelValueEdit.TabIndex = 50;
            // 
            // ExpYearsEducationTextEdit
            // 
            this.ExpYearsEducationTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpYearsEducationTextEdit.Location = new System.Drawing.Point(628, 190);
            this.ExpYearsEducationTextEdit.MenuManager = this.ribbonControl1;
            this.ExpYearsEducationTextEdit.Name = "ExpYearsEducationTextEdit";
            this.ExpYearsEducationTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpYearsEducationTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExpYearsEducationTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpYearsEducationTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.ExpYearsEducationTextEdit.Properties.Mask.EditMask = "N0";
            this.ExpYearsEducationTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpYearsEducationTextEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ExpYearsEducationTextEdit.Size = new System.Drawing.Size(74, 20);
            this.ExpYearsEducationTextEdit.StyleController = this.dataLayoutControl1;
            this.ExpYearsEducationTextEdit.TabIndex = 27;
            // 
            // ExpertDocStatTextEdit
            // 
            this.ExpertDocStatTextEdit.Location = new System.Drawing.Point(628, 262);
            this.ExpertDocStatTextEdit.MenuManager = this.ribbonControl1;
            this.ExpertDocStatTextEdit.Name = "ExpertDocStatTextEdit";
            this.ExpertDocStatTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpertDocStatTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExpertDocStatTextEdit.Properties.NullText = "0";
            this.ExpertDocStatTextEdit.Properties.ReadOnly = true;
            this.ExpertDocStatTextEdit.Size = new System.Drawing.Size(74, 20);
            this.ExpertDocStatTextEdit.StyleController = this.dataLayoutControl1;
            this.ExpertDocStatTextEdit.TabIndex = 54;
            // 
            // ItemForImportCode
            // 
            this.ItemForImportCode.Control = this.ImportCodeTextEdit;
            this.ItemForImportCode.CustomizationFormText = "Код";
            this.ItemForImportCode.Location = new System.Drawing.Point(0, 368);
            this.ItemForImportCode.Name = "ItemForImportCode";
            this.ItemForImportCode.Size = new System.Drawing.Size(757, 24);
            this.ItemForImportCode.Text = "Код:";
            this.ItemForImportCode.TextSize = new System.Drawing.Size(212, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(801, 470);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(781, 450);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8,
            this.layoutControlGroup7,
            this.layoutControlGroup9});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(757, 404);
            this.layoutControlGroup3.Text = "Персональные данные";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSurname,
            this.ItemForName,
            this.ItemForPatronymic,
            this.ItemForWorkplace,
            this.ItemForBirthDate,
            this.ItemForBirthPlace,
            this.ItemForBaseRegionId,
            this.ItemForLiveAddress,
            this.ItemForPhone,
            this.ItemForEmail,
            this.ItemForExpirience,
            this.ItemForWorkYearsTotal,
            this.ItemForWorkYearsChief,
            this.ItemForChiefName,
            this.ItemForChiefPost,
            this.ItemForChiefEmail,
            this.emptySpaceItem2});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(395, 404);
            // 
            // ItemForSurname
            // 
            this.ItemForSurname.Control = this.SurnameTextEdit;
            this.ItemForSurname.Location = new System.Drawing.Point(0, 0);
            this.ItemForSurname.Name = "ItemForSurname";
            this.ItemForSurname.Size = new System.Drawing.Size(385, 24);
            this.ItemForSurname.Text = "Фамилия:";
            this.ItemForSurname.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 24);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(385, 24);
            this.ItemForName.Text = "Имя:";
            this.ItemForName.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForPatronymic
            // 
            this.ItemForPatronymic.Control = this.PatronymicTextEdit;
            this.ItemForPatronymic.Location = new System.Drawing.Point(0, 48);
            this.ItemForPatronymic.Name = "ItemForPatronymic";
            this.ItemForPatronymic.Size = new System.Drawing.Size(385, 24);
            this.ItemForPatronymic.Text = "Отчество:";
            this.ItemForPatronymic.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForWorkplace
            // 
            this.ItemForWorkplace.Control = this.WorkplaceTextEdit;
            this.ItemForWorkplace.Location = new System.Drawing.Point(0, 72);
            this.ItemForWorkplace.Name = "ItemForWorkplace";
            this.ItemForWorkplace.Size = new System.Drawing.Size(385, 24);
            this.ItemForWorkplace.Text = "Место работы:";
            this.ItemForWorkplace.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForBirthDate
            // 
            this.ItemForBirthDate.Control = this.BirthDateDateEdit;
            this.ItemForBirthDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForBirthDate.Name = "ItemForBirthDate";
            this.ItemForBirthDate.Size = new System.Drawing.Size(385, 24);
            this.ItemForBirthDate.Text = "Дата рождения:";
            this.ItemForBirthDate.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForBirthPlace
            // 
            this.ItemForBirthPlace.Control = this.BirthPlaceTextEdit;
            this.ItemForBirthPlace.Location = new System.Drawing.Point(0, 120);
            this.ItemForBirthPlace.Name = "ItemForBirthPlace";
            this.ItemForBirthPlace.Size = new System.Drawing.Size(385, 24);
            this.ItemForBirthPlace.Text = "Место рождения:";
            this.ItemForBirthPlace.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForBaseRegionId
            // 
            this.ItemForBaseRegionId.Control = this.BaseRegionIdTextEdit;
            this.ItemForBaseRegionId.Location = new System.Drawing.Point(0, 144);
            this.ItemForBaseRegionId.Name = "ItemForBaseRegionId";
            this.ItemForBaseRegionId.Size = new System.Drawing.Size(385, 24);
            this.ItemForBaseRegionId.Text = "Базовый субъект:";
            this.ItemForBaseRegionId.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForLiveAddress
            // 
            this.ItemForLiveAddress.Control = this.LiveAddressTextEdit;
            this.ItemForLiveAddress.Location = new System.Drawing.Point(0, 168);
            this.ItemForLiveAddress.Name = "ItemForLiveAddress";
            this.ItemForLiveAddress.Size = new System.Drawing.Size(385, 24);
            this.ItemForLiveAddress.Text = "Адрес фактического проживания:";
            this.ItemForLiveAddress.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.Control = this.PhoneTextEdit;
            this.ItemForPhone.Location = new System.Drawing.Point(0, 192);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(385, 24);
            this.ItemForPhone.Text = "Телефон:";
            this.ItemForPhone.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.Location = new System.Drawing.Point(0, 216);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(385, 24);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForExpirience
            // 
            this.ItemForExpirience.Control = this.ExpirienceTextEdit;
            this.ItemForExpirience.Location = new System.Drawing.Point(0, 240);
            this.ItemForExpirience.Name = "ItemForExpirience";
            this.ItemForExpirience.Size = new System.Drawing.Size(385, 24);
            this.ItemForExpirience.Text = "Опыт аналогичной работы:";
            this.ItemForExpirience.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForWorkYearsTotal
            // 
            this.ItemForWorkYearsTotal.Control = this.WorkYearsTotalTextEdit;
            this.ItemForWorkYearsTotal.Location = new System.Drawing.Point(0, 264);
            this.ItemForWorkYearsTotal.Name = "ItemForWorkYearsTotal";
            this.ItemForWorkYearsTotal.Size = new System.Drawing.Size(385, 24);
            this.ItemForWorkYearsTotal.Text = "Общий стаж работы:";
            this.ItemForWorkYearsTotal.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForWorkYearsChief
            // 
            this.ItemForWorkYearsChief.Control = this.WorkYearsChiefTextEdit;
            this.ItemForWorkYearsChief.Location = new System.Drawing.Point(0, 288);
            this.ItemForWorkYearsChief.Name = "ItemForWorkYearsChief";
            this.ItemForWorkYearsChief.Size = new System.Drawing.Size(385, 24);
            this.ItemForWorkYearsChief.Text = "Стаж руководящей работы:";
            this.ItemForWorkYearsChief.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForChiefName
            // 
            this.ItemForChiefName.Control = this.ChiefNameTextEdit;
            this.ItemForChiefName.Location = new System.Drawing.Point(0, 312);
            this.ItemForChiefName.Name = "ItemForChiefName";
            this.ItemForChiefName.Size = new System.Drawing.Size(385, 24);
            this.ItemForChiefName.Text = "ФИО руководителя:";
            this.ItemForChiefName.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForChiefPost
            // 
            this.ItemForChiefPost.Control = this.ChiefPostTextEdit;
            this.ItemForChiefPost.Location = new System.Drawing.Point(0, 336);
            this.ItemForChiefPost.Name = "ItemForChiefPost";
            this.ItemForChiefPost.Size = new System.Drawing.Size(385, 24);
            this.ItemForChiefPost.Text = "Должность руководителя:";
            this.ItemForChiefPost.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForChiefEmail
            // 
            this.ItemForChiefEmail.Control = this.ChiefEmailTextEdit;
            this.ItemForChiefEmail.Location = new System.Drawing.Point(0, 360);
            this.ItemForChiefEmail.Name = "ItemForChiefEmail";
            this.ItemForChiefEmail.Size = new System.Drawing.Size(385, 44);
            this.ItemForChiefEmail.Text = "Email работодателя:";
            this.ItemForChiefEmail.TextSize = new System.Drawing.Size(206, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(385, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 404);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPost,
            this.ItemForPostValue,
            this.ItemForAcademicDegreeCode,
            this.ItemForAcademicDegreeValue,
            this.ItemForAcademicTitleCode,
            this.ItemForAcademicTitleValue,
            this.ItemForSpeciality,
            this.ItemForSpecialityValue,
            this.ItemForWorkYearsEducation,
            this.ItemForWorkYearsEducationValue,
            this.ItemForExpYearsEducation,
            this.ItemForExpYearsEducationValue,
            this.ItemForAwards,
            this.ItemForAwardsValue,
            this.ItemForCanMission,
            this.ItemForCanMissionValue,
            this.ItemForExpertDocStat,
            this.layoutControlItem1});
            this.layoutControlGroup7.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup7.Location = new System.Drawing.Point(395, 24);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 100D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Absolute;
            columnDefinition2.Width = 75D;
            this.layoutControlGroup7.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 24D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 24D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 24D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition4.Height = 24D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition5.Height = 24D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition6.Height = 24D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition7.Height = 24D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition8.Height = 24D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition9.Height = 188D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.AutoSize;
            this.layoutControlGroup7.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7,
            rowDefinition8,
            rowDefinition9});
            this.layoutControlGroup7.Size = new System.Drawing.Size(362, 380);
            // 
            // ItemForPost
            // 
            this.ItemForPost.Control = this.PostTextEdit;
            this.ItemForPost.Location = new System.Drawing.Point(0, 0);
            this.ItemForPost.Name = "ItemForPost";
            this.ItemForPost.Size = new System.Drawing.Size(287, 24);
            this.ItemForPost.Text = "Должность:";
            this.ItemForPost.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForPostValue
            // 
            this.ItemForPostValue.Control = this.PostValueCombo;
            this.ItemForPostValue.Location = new System.Drawing.Point(287, 0);
            this.ItemForPostValue.Name = "ItemForPostValue";
            this.ItemForPostValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForPostValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForPostValue.Text = "*Должность:";
            this.ItemForPostValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForPostValue.TextVisible = false;
            // 
            // ItemForAcademicDegreeCode
            // 
            this.ItemForAcademicDegreeCode.Control = this.AcademicDegreeCodeTextEdit;
            this.ItemForAcademicDegreeCode.Location = new System.Drawing.Point(0, 24);
            this.ItemForAcademicDegreeCode.Name = "ItemForAcademicDegreeCode";
            this.ItemForAcademicDegreeCode.OptionsTableLayoutItem.RowIndex = 1;
            this.ItemForAcademicDegreeCode.Size = new System.Drawing.Size(287, 24);
            this.ItemForAcademicDegreeCode.Text = "Ученая степень:";
            this.ItemForAcademicDegreeCode.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForAcademicDegreeValue
            // 
            this.ItemForAcademicDegreeValue.Control = this.AcademicDegreeValueTextEdit;
            this.ItemForAcademicDegreeValue.Location = new System.Drawing.Point(287, 24);
            this.ItemForAcademicDegreeValue.Name = "ItemForAcademicDegreeValue";
            this.ItemForAcademicDegreeValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForAcademicDegreeValue.OptionsTableLayoutItem.RowIndex = 1;
            this.ItemForAcademicDegreeValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForAcademicDegreeValue.Text = "*Ученая степень:";
            this.ItemForAcademicDegreeValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAcademicDegreeValue.TextVisible = false;
            // 
            // ItemForAcademicTitleCode
            // 
            this.ItemForAcademicTitleCode.Control = this.AcademicTitleCodeTextEdit;
            this.ItemForAcademicTitleCode.Location = new System.Drawing.Point(0, 48);
            this.ItemForAcademicTitleCode.Name = "ItemForAcademicTitleCode";
            this.ItemForAcademicTitleCode.OptionsTableLayoutItem.RowIndex = 2;
            this.ItemForAcademicTitleCode.Size = new System.Drawing.Size(287, 24);
            this.ItemForAcademicTitleCode.Text = "Ученое звание:";
            this.ItemForAcademicTitleCode.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForAcademicTitleValue
            // 
            this.ItemForAcademicTitleValue.Control = this.AcademicTitleValueTextEdit;
            this.ItemForAcademicTitleValue.Location = new System.Drawing.Point(287, 48);
            this.ItemForAcademicTitleValue.Name = "ItemForAcademicTitleValue";
            this.ItemForAcademicTitleValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForAcademicTitleValue.OptionsTableLayoutItem.RowIndex = 2;
            this.ItemForAcademicTitleValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForAcademicTitleValue.Text = "*Ученое звание:";
            this.ItemForAcademicTitleValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAcademicTitleValue.TextVisible = false;
            // 
            // ItemForSpeciality
            // 
            this.ItemForSpeciality.Control = this.SpecialityTextEdit;
            this.ItemForSpeciality.Location = new System.Drawing.Point(0, 72);
            this.ItemForSpeciality.Name = "ItemForSpeciality";
            this.ItemForSpeciality.OptionsTableLayoutItem.RowIndex = 3;
            this.ItemForSpeciality.Size = new System.Drawing.Size(287, 24);
            this.ItemForSpeciality.Text = "Образование (специализация):";
            this.ItemForSpeciality.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForSpecialityValue
            // 
            this.ItemForSpecialityValue.Control = this.SpecialityValueCombo;
            this.ItemForSpecialityValue.Location = new System.Drawing.Point(287, 72);
            this.ItemForSpecialityValue.Name = "ItemForSpecialityValue";
            this.ItemForSpecialityValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForSpecialityValue.OptionsTableLayoutItem.RowIndex = 3;
            this.ItemForSpecialityValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForSpecialityValue.Text = "*Образование (специализация):";
            this.ItemForSpecialityValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSpecialityValue.TextVisible = false;
            // 
            // ItemForWorkYearsEducation
            // 
            this.ItemForWorkYearsEducation.Control = this.WorkYearsEducationTextEdit;
            this.ItemForWorkYearsEducation.Location = new System.Drawing.Point(0, 96);
            this.ItemForWorkYearsEducation.Name = "ItemForWorkYearsEducation";
            this.ItemForWorkYearsEducation.OptionsTableLayoutItem.RowIndex = 4;
            this.ItemForWorkYearsEducation.Size = new System.Drawing.Size(287, 24);
            this.ItemForWorkYearsEducation.Text = "Стаж работы в системе образования:";
            this.ItemForWorkYearsEducation.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForWorkYearsEducationValue
            // 
            this.ItemForWorkYearsEducationValue.Control = this.WorkYearsEducationValueTextEdit;
            this.ItemForWorkYearsEducationValue.Location = new System.Drawing.Point(287, 96);
            this.ItemForWorkYearsEducationValue.Name = "ItemForWorkYearsEducationValue";
            this.ItemForWorkYearsEducationValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForWorkYearsEducationValue.OptionsTableLayoutItem.RowIndex = 4;
            this.ItemForWorkYearsEducationValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForWorkYearsEducationValue.Text = "*Стаж работы в системе образования:";
            this.ItemForWorkYearsEducationValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWorkYearsEducationValue.TextVisible = false;
            // 
            // ItemForExpYearsEducation
            // 
            this.ItemForExpYearsEducation.Control = this.ExpYearsEducationTextEdit;
            this.ItemForExpYearsEducation.Location = new System.Drawing.Point(0, 120);
            this.ItemForExpYearsEducation.Name = "ItemForExpYearsEducation";
            this.ItemForExpYearsEducation.OptionsTableLayoutItem.RowIndex = 5;
            this.ItemForExpYearsEducation.Size = new System.Drawing.Size(287, 24);
            this.ItemForExpYearsEducation.Text = "Опыт работы в системе образования:";
            this.ItemForExpYearsEducation.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForExpYearsEducationValue
            // 
            this.ItemForExpYearsEducationValue.Control = this.ExpYearsEducationValueTextEdit;
            this.ItemForExpYearsEducationValue.Location = new System.Drawing.Point(287, 120);
            this.ItemForExpYearsEducationValue.Name = "ItemForExpYearsEducationValue";
            this.ItemForExpYearsEducationValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForExpYearsEducationValue.OptionsTableLayoutItem.RowIndex = 5;
            this.ItemForExpYearsEducationValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForExpYearsEducationValue.Text = "*Опыт работы в системе образования:";
            this.ItemForExpYearsEducationValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForExpYearsEducationValue.TextVisible = false;
            // 
            // ItemForAwards
            // 
            this.ItemForAwards.Control = this.AwardsTextEdit;
            this.ItemForAwards.Location = new System.Drawing.Point(0, 144);
            this.ItemForAwards.Name = "ItemForAwards";
            this.ItemForAwards.OptionsTableLayoutItem.RowIndex = 6;
            this.ItemForAwards.Size = new System.Drawing.Size(287, 24);
            this.ItemForAwards.Text = "Награды и звания:";
            this.ItemForAwards.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForAwardsValue
            // 
            this.ItemForAwardsValue.Control = this.AwardsValueTextEdit;
            this.ItemForAwardsValue.Location = new System.Drawing.Point(287, 144);
            this.ItemForAwardsValue.Name = "ItemForAwardsValue";
            this.ItemForAwardsValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForAwardsValue.OptionsTableLayoutItem.RowIndex = 6;
            this.ItemForAwardsValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForAwardsValue.Text = "*Награды и звания:";
            this.ItemForAwardsValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAwardsValue.TextVisible = false;
            // 
            // ItemForCanMission
            // 
            this.ItemForCanMission.Control = this.CanMissionCombo;
            this.ItemForCanMission.Location = new System.Drawing.Point(0, 168);
            this.ItemForCanMission.Name = "ItemForCanMission";
            this.ItemForCanMission.OptionsTableLayoutItem.RowIndex = 7;
            this.ItemForCanMission.Size = new System.Drawing.Size(287, 24);
            this.ItemForCanMission.Text = "Готовность к выезду в другой субъект:";
            this.ItemForCanMission.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForCanMissionValue
            // 
            this.ItemForCanMissionValue.Control = this.CanMissionValueTextEdit;
            this.ItemForCanMissionValue.Location = new System.Drawing.Point(287, 168);
            this.ItemForCanMissionValue.Name = "ItemForCanMissionValue";
            this.ItemForCanMissionValue.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForCanMissionValue.OptionsTableLayoutItem.RowIndex = 7;
            this.ItemForCanMissionValue.Size = new System.Drawing.Size(75, 24);
            this.ItemForCanMissionValue.Text = "*Готовность к выезду в другой субъект:";
            this.ItemForCanMissionValue.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCanMissionValue.TextVisible = false;
            // 
            // ItemForExpertDocStat
            // 
            this.ItemForExpertDocStat.Control = this.ExpertDocStatTextEdit;
            this.ItemForExpertDocStat.CustomizationFormText = "Количество отчетов";
            this.ItemForExpertDocStat.Location = new System.Drawing.Point(0, 192);
            this.ItemForExpertDocStat.Name = "ItemForExpertDocStat";
            this.ItemForExpertDocStat.OptionsTableLayoutItem.RowIndex = 8;
            this.ItemForExpertDocStat.Size = new System.Drawing.Size(287, 188);
            this.ItemForExpertDocStat.Text = "Количество отчетов:";
            this.ItemForExpertDocStat.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.reportsCountValueEdit;
            this.layoutControlItem1.Location = new System.Drawing.Point(287, 192);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem1.OptionsTableLayoutItem.RowIndex = 8;
            this.layoutControlItem1.Size = new System.Drawing.Size(75, 188);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.GroupBordersVisible = false;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForObserverId});
            this.layoutControlGroup9.Location = new System.Drawing.Point(395, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(362, 24);
            // 
            // ItemForObserverId
            // 
            this.ItemForObserverId.Control = this.ObserverIdTextEdit;
            this.ItemForObserverId.Location = new System.Drawing.Point(0, 0);
            this.ItemForObserverId.Name = "ItemForObserverId";
            this.ItemForObserverId.Size = new System.Drawing.Size(362, 24);
            this.ItemForObserverId.Text = "Куратор:";
            this.ItemForObserverId.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTestResults,
            this.ItemForInterviewStatus,
            this.ItemForStage1points,
            this.ItemForKind,
            this.ItemForSubjectLevelValue,
            this.ItemForAccuracyLevelValue,
            this.ItemForPunctualLevelValue,
            this.ItemForIndependencyLevelValue,
            this.ItemForMoralLevelValue,
            this.ItemForDocsImprovementParticipationLevelValue,
            this.ItemForStage2points,
            this.EfficiencyComboGroup,
            this.emptySpaceItem1,
            this.ItemForTestResultsValue,
            this.ItemForInterviewStatusValue,
            this.ItemForEngage,
            this.NextPeriodProposalComboGroup,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(757, 404);
            this.layoutControlGroup4.Text = "Оценка эффективности работы";
            // 
            // ItemForTestResults
            // 
            this.ItemForTestResults.Control = this.TestResultsEdit;
            this.ItemForTestResults.Location = new System.Drawing.Point(0, 0);
            this.ItemForTestResults.Name = "ItemForTestResults";
            this.ItemForTestResults.Size = new System.Drawing.Size(378, 24);
            this.ItemForTestResults.Text = "Результаты тестирования:";
            this.ItemForTestResults.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForInterviewStatus
            // 
            this.ItemForInterviewStatus.Control = this.InterviewStatusCombo;
            this.ItemForInterviewStatus.Location = new System.Drawing.Point(0, 24);
            this.ItemForInterviewStatus.Name = "ItemForInterviewStatus";
            this.ItemForInterviewStatus.Size = new System.Drawing.Size(378, 24);
            this.ItemForInterviewStatus.Text = "Собеседование:";
            this.ItemForInterviewStatus.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForStage1points
            // 
            this.ItemForStage1points.Control = this.Stage1PointsEdit;
            this.ItemForStage1points.Location = new System.Drawing.Point(0, 48);
            this.ItemForStage1points.Name = "ItemForStage1points";
            this.ItemForStage1points.Size = new System.Drawing.Size(378, 24);
            this.ItemForStage1points.Text = "Итоги, 1-й этап:";
            this.ItemForStage1points.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForKind
            // 
            this.ItemForKind.Control = this.ExpertKindCombo;
            this.ItemForKind.Location = new System.Drawing.Point(0, 72);
            this.ItemForKind.Name = "ItemForKind";
            this.ItemForKind.Size = new System.Drawing.Size(757, 24);
            this.ItemForKind.Text = "Категория:";
            this.ItemForKind.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForSubjectLevelValue
            // 
            this.ItemForSubjectLevelValue.Control = this.SubjectLevelValueEdit;
            this.ItemForSubjectLevelValue.Location = new System.Drawing.Point(0, 96);
            this.ItemForSubjectLevelValue.Name = "ItemForSubjectLevelValue";
            this.ItemForSubjectLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForSubjectLevelValue.Text = "*Компетентностный уровень:";
            this.ItemForSubjectLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForAccuracyLevelValue
            // 
            this.ItemForAccuracyLevelValue.Control = this.AccuracyLevelValueEdit;
            this.ItemForAccuracyLevelValue.Location = new System.Drawing.Point(0, 120);
            this.ItemForAccuracyLevelValue.Name = "ItemForAccuracyLevelValue";
            this.ItemForAccuracyLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForAccuracyLevelValue.Text = "*Уровень аккуратности:";
            this.ItemForAccuracyLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForPunctualLevelValue
            // 
            this.ItemForPunctualLevelValue.Control = this.PunctualLevelValueEdit;
            this.ItemForPunctualLevelValue.Location = new System.Drawing.Point(0, 144);
            this.ItemForPunctualLevelValue.Name = "ItemForPunctualLevelValue";
            this.ItemForPunctualLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForPunctualLevelValue.Text = "*Уровень пунктуальности:";
            this.ItemForPunctualLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForIndependencyLevelValue
            // 
            this.ItemForIndependencyLevelValue.Control = this.IndependencyLevelValueEdit;
            this.ItemForIndependencyLevelValue.Location = new System.Drawing.Point(0, 168);
            this.ItemForIndependencyLevelValue.Name = "ItemForIndependencyLevelValue";
            this.ItemForIndependencyLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForIndependencyLevelValue.Text = "*Уровень независимости:";
            this.ItemForIndependencyLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForMoralLevelValue
            // 
            this.ItemForMoralLevelValue.Control = this.MoralLevelValueEdit;
            this.ItemForMoralLevelValue.Location = new System.Drawing.Point(0, 192);
            this.ItemForMoralLevelValue.Name = "ItemForMoralLevelValue";
            this.ItemForMoralLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForMoralLevelValue.Text = "*Высокие моральные качества:";
            this.ItemForMoralLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForDocsImprovementParticipationLevelValue
            // 
            this.ItemForDocsImprovementParticipationLevelValue.Control = this.DocsImprovementParticipationLevelValueEdit;
            this.ItemForDocsImprovementParticipationLevelValue.Location = new System.Drawing.Point(0, 216);
            this.ItemForDocsImprovementParticipationLevelValue.Name = "ItemForDocsImprovementParticipationLevelValue";
            this.ItemForDocsImprovementParticipationLevelValue.Size = new System.Drawing.Size(757, 24);
            this.ItemForDocsImprovementParticipationLevelValue.Text = "*Участие в совершенствовании НПА:";
            this.ItemForDocsImprovementParticipationLevelValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForStage2points
            // 
            this.ItemForStage2points.Control = this.Stage2PointsEdit;
            this.ItemForStage2points.Location = new System.Drawing.Point(0, 240);
            this.ItemForStage2points.Name = "ItemForStage2points";
            this.ItemForStage2points.Size = new System.Drawing.Size(757, 24);
            this.ItemForStage2points.Text = "Итоги, 2-й этап:";
            this.ItemForStage2points.TextSize = new System.Drawing.Size(206, 13);
            // 
            // EfficiencyComboGroup
            // 
            this.EfficiencyComboGroup.Control = this.EfficiencyCombo;
            this.EfficiencyComboGroup.Location = new System.Drawing.Point(0, 264);
            this.EfficiencyComboGroup.Name = "EfficiencyComboGroup";
            this.EfficiencyComboGroup.Size = new System.Drawing.Size(378, 24);
            this.EfficiencyComboGroup.Text = "Эффективность работы:";
            this.EfficiencyComboGroup.TextSize = new System.Drawing.Size(206, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 288);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(757, 116);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTestResultsValue
            // 
            this.ItemForTestResultsValue.Control = this.TestResultsValueCombo;
            this.ItemForTestResultsValue.Location = new System.Drawing.Point(388, 0);
            this.ItemForTestResultsValue.Name = "ItemForTestResultsValue";
            this.ItemForTestResultsValue.Size = new System.Drawing.Size(369, 24);
            this.ItemForTestResultsValue.Text = "*Результаты тестирования:";
            this.ItemForTestResultsValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForInterviewStatusValue
            // 
            this.ItemForInterviewStatusValue.Control = this.InterviewStatusValueCombo;
            this.ItemForInterviewStatusValue.Location = new System.Drawing.Point(388, 24);
            this.ItemForInterviewStatusValue.Name = "ItemForInterviewStatusValue";
            this.ItemForInterviewStatusValue.Size = new System.Drawing.Size(369, 24);
            this.ItemForInterviewStatusValue.Text = "*Собеседование:";
            this.ItemForInterviewStatusValue.TextSize = new System.Drawing.Size(206, 13);
            // 
            // ItemForEngage
            // 
            this.ItemForEngage.Control = this.EngageCombo;
            this.ItemForEngage.Location = new System.Drawing.Point(388, 48);
            this.ItemForEngage.Name = "ItemForEngage";
            this.ItemForEngage.Size = new System.Drawing.Size(369, 24);
            this.ItemForEngage.Text = "Включение в базу экспертов:";
            this.ItemForEngage.TextSize = new System.Drawing.Size(206, 13);
            // 
            // NextPeriodProposalComboGroup
            // 
            this.NextPeriodProposalComboGroup.Control = this.NextPeriodProposalCombo;
            this.NextPeriodProposalComboGroup.Location = new System.Drawing.Point(388, 264);
            this.NextPeriodProposalComboGroup.Name = "NextPeriodProposalComboGroup";
            this.NextPeriodProposalComboGroup.Size = new System.Drawing.Size(369, 24);
            this.NextPeriodProposalComboGroup.Text = "Включение в БД следующего года:";
            this.NextPeriodProposalComboGroup.TextSize = new System.Drawing.Size(206, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(378, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 72);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(378, 264);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // expertAgreementControl1
            // 
            this.expertAgreementControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.expertAgreementControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expertAgreementControl1.Location = new System.Drawing.Point(0, 141);
            this.expertAgreementControl1.Name = "expertAgreementControl1";
            this.expertAgreementControl1.Size = new System.Drawing.Size(801, 24);
            this.expertAgreementControl1.TabIndex = 2;
            // 
            // ExpertView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.expertAgreementControl1);
            this.Name = "ExpertView";
            this.Size = new System.Drawing.Size(801, 635);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.expertAgreementControl1, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImportCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsCountValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatronymicTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthPlaceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveAddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpirienceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwardsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefPostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChiefEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpertKindCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CanMissionCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterviewStatusCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngageCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfficiencyCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextPeriodProposalCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseRegionIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObserverIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.observerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicDegreeCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.academicDegreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicTitleCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.academicTitleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Stage1PointsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Stage2PointsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestResultsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsChiefTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsTotalTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsEducationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostValueCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicDegreeValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcademicTitleValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialityValueCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkYearsEducationValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpYearsEducationValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwardsValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CanMissionValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestResultsValueCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterviewStatusValueCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccuracyLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PunctualLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IndependencyLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoralLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocsImprovementParticipationLevelValueEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpYearsEducationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpertDocStatTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImportCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPatronymic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthPlace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBaseRegionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLiveAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpirience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsChief)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChiefEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicDegreeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicDegreeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicTitleCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcademicTitleValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialityValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsEducation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkYearsEducationValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpYearsEducation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpYearsEducationValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAwards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAwardsValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCanMission)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCanMissionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertDocStat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObserverId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTestResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInterviewStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStage1points)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubjectLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccuracyLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPunctualLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIndependencyLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMoralLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocsImprovementParticipationLevelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStage2points)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfficiencyComboGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTestResultsValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInterviewStatusValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextPeriodProposalComboGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit ImportCodeTextEdit;
        private System.Windows.Forms.BindingSource expertBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraEditors.TextEdit SurnameTextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit PatronymicTextEdit;
        private DevExpress.XtraEditors.TextEdit WorkplaceTextEdit;
        private DevExpress.XtraEditors.TextEdit PostTextEdit;
        private DevExpress.XtraEditors.DateEdit BirthDateDateEdit;
        private DevExpress.XtraEditors.TextEdit BirthPlaceTextEdit;
        private DevExpress.XtraEditors.TextEdit LiveAddressTextEdit;
        private DevExpress.XtraEditors.TextEdit PhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.TextEdit ExpirienceTextEdit;
        private DevExpress.XtraEditors.TextEdit SpecialityTextEdit;
        private DevExpress.XtraEditors.TextEdit AwardsTextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefPostTextEdit;
        private DevExpress.XtraEditors.TextEdit ChiefEmailTextEdit;
        private DevExpress.XtraEditors.ImageComboBoxEdit ExpertKindCombo;
        private DevExpress.XtraEditors.ImageComboBoxEdit CanMissionCombo;
        private DevExpress.XtraEditors.ImageComboBoxEdit InterviewStatusCombo;
        private DevExpress.XtraEditors.ImageComboBoxEdit EngageCombo;
        private DevExpress.XtraEditors.ImageComboBoxEdit EfficiencyCombo;
        private DevExpress.XtraEditors.ImageComboBoxEdit NextPeriodProposalCombo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForImportCode;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private DevExpress.XtraEditors.LookUpEdit BaseRegionIdTextEdit;
        private System.Windows.Forms.BindingSource observerBindingSource;
        private DevExpress.XtraEditors.LookUpEdit ObserverIdTextEdit;
        private System.Windows.Forms.BindingSource academicDegreeBindingSource;
        private DevExpress.XtraEditors.LookUpEdit AcademicDegreeCodeTextEdit;
        private System.Windows.Forms.BindingSource academicTitleBindingSource;
        private DevExpress.XtraEditors.LookUpEdit AcademicTitleCodeTextEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraEditors.SpinEdit Stage1PointsEdit;
        private DevExpress.XtraEditors.SpinEdit Stage2PointsEdit;
        private DevExpress.XtraEditors.SpinEdit TestResultsEdit;
        private DevExpress.XtraLayout.LayoutControlItem NextPeriodProposalComboGroup;
        private DevExpress.XtraLayout.LayoutControlItem EfficiencyComboGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStage2points;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocsImprovementParticipationLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMoralLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIndependencyLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPunctualLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccuracyLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubjectLevelValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKind;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEngage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStage1points;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInterviewStatusValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInterviewStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTestResultsValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTestResults;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCanMissionValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCanMission;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAwardsValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAwards;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpYearsEducationValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpYearsEducation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkYearsEducationValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkYearsEducation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpecialityValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpeciality;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcademicTitleValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcademicTitleCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcademicDegreeValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcademicDegreeCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForObserverId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChiefName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkYearsChief;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkYearsTotal;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpirience;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLiveAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBaseRegionId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBirthPlace;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBirthDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkplace;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPatronymic;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurname;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SpinEdit WorkYearsChiefTextEdit;
        private DevExpress.XtraEditors.SpinEdit WorkYearsTotalTextEdit;
        private DevExpress.XtraEditors.SpinEdit WorkYearsEducationTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit PostValueCombo;
        private DevExpress.XtraEditors.ComboBoxEdit AcademicDegreeValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit AcademicTitleValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit SpecialityValueCombo;
        private DevExpress.XtraEditors.ComboBoxEdit WorkYearsEducationValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit ExpYearsEducationValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit AwardsValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit CanMissionValueTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit TestResultsValueCombo;
        private DevExpress.XtraEditors.ComboBoxEdit InterviewStatusValueCombo;
        private DevExpress.XtraEditors.ComboBoxEdit SubjectLevelValueEdit;
        private DevExpress.XtraEditors.ComboBoxEdit AccuracyLevelValueEdit;
        private DevExpress.XtraEditors.ComboBoxEdit PunctualLevelValueEdit;
        private DevExpress.XtraEditors.ComboBoxEdit IndependencyLevelValueEdit;
        private DevExpress.XtraEditors.ComboBoxEdit MoralLevelValueEdit;
        private DevExpress.XtraEditors.ComboBoxEdit DocsImprovementParticipationLevelValueEdit;
        private DevExpress.XtraEditors.SpinEdit ExpYearsEducationTextEdit;
        private ExpertAgreementControl expertAgreementControl1;
        private DevExpress.XtraEditors.TextEdit ExpertDocStatTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpertDocStat;
        private DevExpress.XtraEditors.TextEdit reportsCountValueEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
