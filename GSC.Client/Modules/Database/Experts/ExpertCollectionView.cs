﻿using DevExpress.XtraBars;
using GSC.Client.ViewModels.Expert;
using GSC.Client.Util;

namespace GSC.Client.Modules.Database.Experts
{
    public partial class ExpertCollectionView : CollectionBlockView
    {
        public ExpertCollectionView()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExpertCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        public override BarButtonItem EditButton => bbiEdit;

        public override BarButtonItem ViewButton => bbiView;

        private ExpertCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ExpertCollectionViewModel>(); }
        }
    }
}
