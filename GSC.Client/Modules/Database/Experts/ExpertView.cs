﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq.Expressions;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using GSC.Client.Util;
using GSC.Client.ViewModels.Expert;

namespace GSC.Client.Modules.Database.Experts
{
    public partial class ExpertView : BaseEditView
    {
        private static readonly Color agreementGoodBackColor = Color.FromArgb(192, 255, 192);
        private static readonly Color agreementBadBackColor = Color.FromArgb(255, 192, 192);
        private static readonly Color agreementGoodForeColor = Color.Green;
        private static readonly Color agreementBadForeColor = Color.FromArgb(192, 0, 0);

        public ExpertView()
        {
            InitializeComponent();

            ExpertKindCombo.Properties.Items.AddEnum<Model.ExpertKind>();
            CanMissionCombo.Properties.Items.AddEnum<Model.BooleanType>();
            EngageCombo.Properties.Items.AddEnum<Model.BooleanType>();
            EfficiencyCombo.Properties.Items.AddEnum<Model.ExpertEfficiency>();
            NextPeriodProposalCombo.Properties.Items.AddEnum<Model.ProposalType>();
            InterviewStatusCombo.Properties.Items.AddEnum<Model.InterviewStatus>();

            var helper = new InitCombo<Model.Expert>();

            helper.Disabled(PostValueCombo, e => e.PostValue);
            helper.Disabled(AcademicDegreeValueTextEdit, e => e.AcademicDegreeValue);
            helper.Disabled(AcademicTitleValueTextEdit, e => e.AcademicTitleValue);
            helper.Disabled(SpecialityValueCombo, e => e.SpecialityValue);
            helper.Disabled(WorkYearsEducationValueTextEdit, e => e.WorkYearsEducationValue);
            helper.Disabled(ExpYearsEducationValueTextEdit, e => e.ExpYearsEducationValue);
            helper.Disabled(AwardsValueTextEdit, e => e.AwardsValue);
            helper.Disabled(CanMissionValueTextEdit, e => e.CanMissionValue);
            helper.Disabled(TestResultsValueCombo, e => e.TestResultsValue);
            helper.Disabled(InterviewStatusValueCombo, e => e.InterviewStatusValue);
            helper.Disabled(SubjectLevelValueEdit, e => e.SubjectLevelValue);
            helper.Disabled(AccuracyLevelValueEdit, e => e.AccuracyLevelValue);
            helper.Disabled(PunctualLevelValueEdit, e => e.PunctualLevelValue);
            helper.Disabled(IndependencyLevelValueEdit, e => e.IndependencyLevelValue);
            helper.Disabled(MoralLevelValueEdit, e => e.MoralLevelValue);
            helper.Disabled(DocsImprovementParticipationLevelValueEdit, e => e.DocsImprovementParticipationLevelValue);

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        #region InitCombo helper

        private class InitCombo<TEntity>
        {
            private static void Init(ComboBoxEdit combo, Expression<Func<TEntity, object>> propertyAccessor)
            {
                combo.InitItemsFromDataAnnotations(propertyAccessor);
            }

            public void Disabled(ComboBoxEdit combo, Expression<Func<TEntity, object>> propertyAccessor)
            {
                combo.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                Init(combo, propertyAccessor);
            }
        }

        #endregion

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ExpertViewModel>();

            fluent.SetObjectDataSourceBinding(expertBindingSource,
                vm => vm.Entity,
                vm => Update());

            fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.LookupRegions.Entities);
            fluent.SetBinding(observerBindingSource, bs => bs.DataSource, vm => vm.LookupObservers.Entities);
            fluent.SetBinding(academicTitleBindingSource, bs => bs.DataSource, vm => vm.LookupAcademicTitles.Entities);
            fluent.SetBinding(academicDegreeBindingSource, bs => bs.DataSource, vm => vm.LookupAcademicDegrees.Entities);

            fluent.EventToCommand<EventArgs>(expertAgreementControl1, "AddClick", vm => vm.AddExpertAgreement());
            fluent.EventToCommand<EventArgs>(expertAgreementControl1, "RemoveClick", vm => vm.RemoveExpertAgreement());
            fluent.EventToCommand<EventArgs>(expertAgreementControl1, "PreviewClick", vm => vm.PreviewExpertAgreement());
            fluent.SetBinding(expertAgreementControl1, c => c.AddVisible, vm => vm.AddVisible);
            fluent.SetBinding(expertAgreementControl1, c => c.RemoveVisible, vm => vm.RemoveVisible);
            fluent.SetBinding(expertAgreementControl1, c => c.PreviewVisible, vm => vm.PreviewVisible);
            fluent.SetBinding(expertAgreementControl1, c => c.Message, vm => vm.AgreementMessage);

            fluent.SetTrigger(vm => vm.Agreement, AgreementChanged);
        }

        protected virtual void AgreementChanged(Model.ExpertAgreement agreement)
        {
            var agreementExists = agreement != null;

            expertAgreementControl1.ForeColor = agreementExists ? agreementGoodForeColor : agreementBadForeColor;
            expertAgreementControl1.BackColor = agreementExists ? agreementGoodBackColor : agreementBadBackColor;
        }


        private void expertBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            Debug.Assert((sender as BindingSource)?.DataSource != null);

            var fluent = mvvmContext1.OfType<ExpertViewModel>();

            fluent.SetBinding(ExpYearsEducationTextEdit, c => c.EditValue, vm => vm.ExpYearsEducation);
            fluent.SetBinding(ExpYearsEducationValueTextEdit, c => c.EditValue, vm => vm.ExpYearsEducationValue);
            fluent.SetBinding(WorkYearsEducationTextEdit, c => c.EditValue, vm => vm.WorkYearsEducation);
            fluent.SetBinding(WorkYearsEducationValueTextEdit, c => c.EditValue, vm => vm.WorkYearsEducationValue);
            fluent.SetBinding(CanMissionCombo, c => c.EditValue, vm => vm.CanMission);
            fluent.SetBinding(CanMissionValueTextEdit, c => c.EditValue, vm => vm.CanMissionValue);
            fluent.SetBinding(ExpertKindCombo, c => c.EditValue, vm => vm.Kind);

            fluent.SetBinding(AcademicDegreeCodeTextEdit,
                c => c.EditValue,
                vm => vm.AcademicDegreeId,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (int?) o);
            fluent.SetBinding(AcademicDegreeValueTextEdit, c => c.EditValue, vm => vm.AcademicDegreeValue);

            fluent.SetBinding(AcademicTitleCodeTextEdit,
                c => c.EditValue,
                vm => vm.AcademicTitleId,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (int?) o);
            fluent.SetBinding(AcademicTitleValueTextEdit, c => c.EditValue, vm => vm.AcademicTitleValue);

            fluent.SetBinding(AwardsTextEdit, c => c.EditValue, vm => vm.Awards);
            fluent.SetBinding(AwardsValueTextEdit, c => c.EditValue, vm => vm.AwardsValue);


            fluent.SetBinding(TestResultsEdit,
                c => c.EditValue,
                vm => vm.TestResults,
                i => i ?? 0m,
                o => ReferenceEquals(o, null) ? null : (int?) Convert.ToInt32(o));
            fluent.SetBinding(TestResultsValueCombo,
                c => c.EditValue,
                vm => vm.TestResultsValue,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (int?) o);

            fluent.SetBinding(InterviewStatusCombo,
                c => c.EditValue,
                vm => vm.InterviewStatus,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (Model.InterviewStatus?) o);
            fluent.SetBinding(InterviewStatusValueCombo,
                c => c.EditValue,
                vm => vm.InterviewStatusValue,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (int?) o);

            fluent.SetBinding(SpecialityValueCombo, c => c.EditValue, vm => vm.SpecialityValue);
            fluent.SetBinding(PostValueCombo, c => c.EditValue, vm => vm.PostValue);

            fluent.SetBinding(Stage1PointsEdit,
                c => c.EditValue,
                vm => vm.Stage1points,
                i => i ?? 0m,
                o => ReferenceEquals(o, null) ? null : (int?) Convert.ToInt32(o));
            fluent.SetBinding(EngageCombo,
                c => c.EditValue,
                vm => vm.Engage,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (Model.BooleanType?) o);
            fluent.SetBinding(SubjectLevelValueEdit, c => c.EditValue, vm => vm.SubjectLevelValue);
            fluent.SetBinding(AccuracyLevelValueEdit, c => c.EditValue, vm => vm.AccuracyLevelValue);
            fluent.SetBinding(PunctualLevelValueEdit, c => c.EditValue, vm => vm.PunctualLevelValue);
            fluent.SetBinding(IndependencyLevelValueEdit, c => c.EditValue, vm => vm.IndependencyLevelValue);
            fluent.SetBinding(MoralLevelValueEdit, c => c.EditValue, vm => vm.MoralLevelValue);
            fluent.SetBinding(DocsImprovementParticipationLevelValueEdit,
                c => c.EditValue,
                vm => vm.DocsImprovementParticipationLevelValue);
            fluent.SetBinding(Stage2PointsEdit,
                c => c.EditValue,
                vm => vm.Stage2points,
                i => i ?? 0m,
                o => ReferenceEquals(o, null) ? null : (int?) Convert.ToInt32(o));
            fluent.SetBinding(EfficiencyCombo,
                c => c.EditValue,
                vm => vm.Efficiency,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (Model.ExpertEfficiency?) o);
            fluent.SetBinding(NextPeriodProposalCombo,
                c => c.EditValue,
                vm => vm.NextPeriodProposal,
                i => i ?? (object) DBNull.Value,
                o => Equals(o, DBNull.Value) ? null : (Model.ProposalType?) o);

            fluent.SetBinding(ExpertDocStatTextEdit, c => c.EditValue, vm => vm.ReportsCount);
            fluent.SetBinding(reportsCountValueEdit, c => c.EditValue, vm => vm.ReportsCountValue);
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}