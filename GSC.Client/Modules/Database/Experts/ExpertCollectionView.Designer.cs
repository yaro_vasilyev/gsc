﻿namespace GSC.Client.Modules.Database.Experts
{
    partial class ExpertCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiImportFromCsv = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpExperts = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPatronymic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkplace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObserver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthPlace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLiveAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcademicDegreeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcademicTitleValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpirience = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialityValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkYearsTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkYearsEducation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkYearsEducationValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpYearsEducation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpYearsEducationValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkYearsChief = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAwards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAwardsValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCanMission = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCanMissionValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChiefName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChiefPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChiefEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTestResults = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTestResultsValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInterviewStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInterviewStatusValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStage1points = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubjectLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccuracyLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPunctualLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIndependencyLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMoralLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocsImprovementParticipationLevelValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStage2points = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEfficiency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextPeriodProposal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcademicDegree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcademicTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTs = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiImportFromCsv,
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 8;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpExperts});
            this.ribbonControl1.Size = new System.Drawing.Size(621, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "ImportFromCsv", this.bbiImportFromCsv),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Expert.ExpertCollectionViewModel);
            // 
            // bbiImportFromCsv
            // 
            this.bbiImportFromCsv.Caption = "Импорт из файла";
            this.bbiImportFromCsv.Id = 1;
            this.bbiImportFromCsv.ImageUri.Uri = "Apply";
            this.bbiImportFromCsv.Name = "bbiImportFromCsv";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 2;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 3;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 4;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 5;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 6;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpExperts
            // 
            this.rpExperts.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgExchange});
            this.rpExperts.Name = "rpExperts";
            this.rpExperts.Text = "Эксперты";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 7;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.ItemLinks.Add(this.bbiImportFromCsv);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(621, 284);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKind,
            this.colSurname,
            this.colName,
            this.colPatronymic,
            this.colBaseRegion,
            this.colWorkplace,
            this.colPost,
            this.colPhone,
            this.colEmail,
            this.colObserver,
            this.colDocumentsCount,
            this.colImportCode,
            this.colPostValue,
            this.colBirthDate,
            this.colBirthPlace,
            this.colLiveAddress,
            this.colAcademicDegreeValue,
            this.colAcademicTitleValue,
            this.colExpirience,
            this.colSpeciality,
            this.colSpecialityValue,
            this.colWorkYearsTotal,
            this.colWorkYearsEducation,
            this.colWorkYearsEducationValue,
            this.colExpYearsEducation,
            this.colExpYearsEducationValue,
            this.colWorkYearsChief,
            this.colAwards,
            this.colAwardsValue,
            this.colCanMission,
            this.colCanMissionValue,
            this.colChiefName,
            this.colChiefPost,
            this.colChiefEmail,
            this.colTestResults,
            this.colTestResultsValue,
            this.colInterviewStatus,
            this.colInterviewStatusValue,
            this.colStage1points,
            this.colEngage,
            this.colSubjectLevelValue,
            this.colAccuracyLevelValue,
            this.colPunctualLevelValue,
            this.colIndependencyLevelValue,
            this.colMoralLevelValue,
            this.colDocsImprovementParticipationLevelValue,
            this.colStage2points,
            this.colEfficiency,
            this.colNextPeriodProposal,
            this.colAcademicDegree,
            this.colAcademicTitle,
            this.colPeriod,
            this.colTs});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // colKind
            // 
            this.colKind.Caption = "Категория эксперта";
            this.colKind.FieldName = "Kind";
            this.colKind.Name = "colKind";
            this.colKind.Visible = true;
            this.colKind.VisibleIndex = 7;
            this.colKind.Width = 125;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Фамилия";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 111;
            // 
            // colName
            // 
            this.colName.Caption = "Имя";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 136;
            // 
            // colPatronymic
            // 
            this.colPatronymic.Caption = "Отчество";
            this.colPatronymic.FieldName = "Patronymic";
            this.colPatronymic.Name = "colPatronymic";
            this.colPatronymic.Visible = true;
            this.colPatronymic.VisibleIndex = 2;
            this.colPatronymic.Width = 110;
            // 
            // colBaseRegion
            // 
            this.colBaseRegion.Caption = "Базовый субъект";
            this.colBaseRegion.FieldName = "BaseRegion.Name";
            this.colBaseRegion.Name = "colBaseRegion";
            this.colBaseRegion.Visible = true;
            this.colBaseRegion.VisibleIndex = 8;
            this.colBaseRegion.Width = 140;
            // 
            // colWorkplace
            // 
            this.colWorkplace.Caption = "Место работы";
            this.colWorkplace.FieldName = "Workplace";
            this.colWorkplace.Name = "colWorkplace";
            this.colWorkplace.Visible = true;
            this.colWorkplace.VisibleIndex = 3;
            this.colWorkplace.Width = 172;
            // 
            // colPost
            // 
            this.colPost.Caption = "Должность";
            this.colPost.FieldName = "Post";
            this.colPost.Name = "colPost";
            this.colPost.Visible = true;
            this.colPost.VisibleIndex = 4;
            this.colPost.Width = 131;
            // 
            // colPhone
            // 
            this.colPhone.Caption = "Телефон";
            this.colPhone.FieldName = "Phone";
            this.colPhone.Name = "colPhone";
            this.colPhone.Visible = true;
            this.colPhone.VisibleIndex = 5;
            this.colPhone.Width = 149;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 6;
            this.colEmail.Width = 143;
            // 
            // colObserver
            // 
            this.colObserver.Caption = "Куратор";
            this.colObserver.FieldName = "Observer.Fio";
            this.colObserver.Name = "colObserver";
            this.colObserver.Visible = true;
            this.colObserver.VisibleIndex = 9;
            this.colObserver.Width = 192;
            // 
            // colDocumentsCount
            // 
            this.colDocumentsCount.Caption = "Количество отчетов";
            this.colDocumentsCount.FieldName = "ExpertDocStat.DocumentsCount";
            this.colDocumentsCount.Name = "colDocumentsCount";
            this.colDocumentsCount.Visible = true;
            this.colDocumentsCount.VisibleIndex = 10;
            // 
            // colImportCode
            // 
            this.colImportCode.Caption = "Код эксперта";
            this.colImportCode.FieldName = "ImportCode";
            this.colImportCode.Name = "colImportCode";
            // 
            // colPostValue
            // 
            this.colPostValue.Caption = "*Должность";
            this.colPostValue.FieldName = "PostValue";
            this.colPostValue.Name = "colPostValue";
            // 
            // colBirthDate
            // 
            this.colBirthDate.Caption = "Дата рождения";
            this.colBirthDate.FieldName = "BirthDate";
            this.colBirthDate.Name = "colBirthDate";
            // 
            // colBirthPlace
            // 
            this.colBirthPlace.Caption = "Место рождения";
            this.colBirthPlace.FieldName = "BirthPlace";
            this.colBirthPlace.Name = "colBirthPlace";
            // 
            // colLiveAddress
            // 
            this.colLiveAddress.Caption = "Адрес";
            this.colLiveAddress.FieldName = "LiveAddress";
            this.colLiveAddress.Name = "colLiveAddress";
            // 
            // colAcademicDegreeValue
            // 
            this.colAcademicDegreeValue.Caption = "*Ученая степень";
            this.colAcademicDegreeValue.FieldName = "AcademicDegreeValue";
            this.colAcademicDegreeValue.Name = "colAcademicDegreeValue";
            // 
            // colAcademicTitleValue
            // 
            this.colAcademicTitleValue.Caption = "*Ученое звание";
            this.colAcademicTitleValue.FieldName = "AcademicTitleValue";
            this.colAcademicTitleValue.Name = "colAcademicTitleValue";
            // 
            // colExpirience
            // 
            this.colExpirience.Caption = "Опыт аналогичной работы";
            this.colExpirience.FieldName = "Expirience";
            this.colExpirience.Name = "colExpirience";
            // 
            // colSpeciality
            // 
            this.colSpeciality.Caption = "Специализация";
            this.colSpeciality.FieldName = "Speciality";
            this.colSpeciality.Name = "colSpeciality";
            // 
            // colSpecialityValue
            // 
            this.colSpecialityValue.Caption = "*Специализация";
            this.colSpecialityValue.FieldName = "SpecialityValue";
            this.colSpecialityValue.Name = "colSpecialityValue";
            // 
            // colWorkYearsTotal
            // 
            this.colWorkYearsTotal.Caption = "Общий стаж";
            this.colWorkYearsTotal.FieldName = "WorkYearsTotal";
            this.colWorkYearsTotal.Name = "colWorkYearsTotal";
            // 
            // colWorkYearsEducation
            // 
            this.colWorkYearsEducation.Caption = "Стаж работы в системе образования";
            this.colWorkYearsEducation.FieldName = "WorkYearsEducation";
            this.colWorkYearsEducation.Name = "colWorkYearsEducation";
            // 
            // colWorkYearsEducationValue
            // 
            this.colWorkYearsEducationValue.Caption = "*Стаж работы в системе образования";
            this.colWorkYearsEducationValue.FieldName = "WorkYearsEducationValue";
            this.colWorkYearsEducationValue.Name = "colWorkYearsEducationValue";
            // 
            // colExpYearsEducation
            // 
            this.colExpYearsEducation.Caption = "Опыт работы в системе образования";
            this.colExpYearsEducation.FieldName = "ExpYearsEducation";
            this.colExpYearsEducation.Name = "colExpYearsEducation";
            // 
            // colExpYearsEducationValue
            // 
            this.colExpYearsEducationValue.Caption = "*Опыт работы в системе образования";
            this.colExpYearsEducationValue.FieldName = "ExpYearsEducationValue";
            this.colExpYearsEducationValue.Name = "colExpYearsEducationValue";
            // 
            // colWorkYearsChief
            // 
            this.colWorkYearsChief.Caption = "Стаж руководящей работы";
            this.colWorkYearsChief.FieldName = "WorkYearsChief";
            this.colWorkYearsChief.Name = "colWorkYearsChief";
            // 
            // colAwards
            // 
            this.colAwards.Caption = "Ведомственные награды и звания";
            this.colAwards.FieldName = "Awards";
            this.colAwards.Name = "colAwards";
            // 
            // colAwardsValue
            // 
            this.colAwardsValue.Caption = "*Ведомственные награды и звания";
            this.colAwardsValue.FieldName = "AwardsValue";
            this.colAwardsValue.Name = "colAwardsValue";
            // 
            // colCanMission
            // 
            this.colCanMission.Caption = "Готовность к выезду в другой субъект";
            this.colCanMission.FieldName = "CanMission";
            this.colCanMission.Name = "colCanMission";
            // 
            // colCanMissionValue
            // 
            this.colCanMissionValue.Caption = "*Готовность к выезду в другой субъект";
            this.colCanMissionValue.FieldName = "CanMissionValue";
            this.colCanMissionValue.Name = "colCanMissionValue";
            // 
            // colChiefName
            // 
            this.colChiefName.Caption = "ФИО руководителя";
            this.colChiefName.FieldName = "ChiefName";
            this.colChiefName.Name = "colChiefName";
            // 
            // colChiefPost
            // 
            this.colChiefPost.Caption = "Должность руководителя";
            this.colChiefPost.FieldName = "ChiefPost";
            this.colChiefPost.Name = "colChiefPost";
            // 
            // colChiefEmail
            // 
            this.colChiefEmail.Caption = "Email руководителя";
            this.colChiefEmail.FieldName = "ChiefEmail";
            this.colChiefEmail.Name = "colChiefEmail";
            // 
            // colTestResults
            // 
            this.colTestResults.Caption = "Результаты тестирования";
            this.colTestResults.FieldName = "TestResults";
            this.colTestResults.Name = "colTestResults";
            // 
            // colTestResultsValue
            // 
            this.colTestResultsValue.Caption = "*Результаты тестирования";
            this.colTestResultsValue.FieldName = "TestResultsValue";
            this.colTestResultsValue.Name = "colTestResultsValue";
            // 
            // colInterviewStatus
            // 
            this.colInterviewStatus.Caption = "Собеседование";
            this.colInterviewStatus.FieldName = "InterviewStatus";
            this.colInterviewStatus.Name = "colInterviewStatus";
            // 
            // colInterviewStatusValue
            // 
            this.colInterviewStatusValue.Caption = "*Собеседование";
            this.colInterviewStatusValue.FieldName = "InterviewStatusValue";
            this.colInterviewStatusValue.Name = "colInterviewStatusValue";
            // 
            // colStage1points
            // 
            this.colStage1points.Caption = "Итоги 1-й этап";
            this.colStage1points.FieldName = "Stage1points";
            this.colStage1points.Name = "colStage1points";
            // 
            // colEngage
            // 
            this.colEngage.Caption = "Включение в базу экспертов";
            this.colEngage.FieldName = "Engage";
            this.colEngage.Name = "colEngage";
            // 
            // colSubjectLevelValue
            // 
            this.colSubjectLevelValue.Caption = "*Компетентностный уровень";
            this.colSubjectLevelValue.FieldName = "SubjectLevelValue";
            this.colSubjectLevelValue.Name = "colSubjectLevelValue";
            // 
            // colAccuracyLevelValue
            // 
            this.colAccuracyLevelValue.Caption = "*Уровень аккуратности";
            this.colAccuracyLevelValue.FieldName = "AccuracyLevelValue";
            this.colAccuracyLevelValue.Name = "colAccuracyLevelValue";
            // 
            // colPunctualLevelValue
            // 
            this.colPunctualLevelValue.Caption = "*Уровень пунктуальности";
            this.colPunctualLevelValue.FieldName = "PunctualLevelValue";
            this.colPunctualLevelValue.Name = "colPunctualLevelValue";
            // 
            // colIndependencyLevelValue
            // 
            this.colIndependencyLevelValue.Caption = "*Уровень независимости";
            this.colIndependencyLevelValue.FieldName = "IndependencyLevelValue";
            this.colIndependencyLevelValue.Name = "colIndependencyLevelValue";
            // 
            // colMoralLevelValue
            // 
            this.colMoralLevelValue.Caption = "*Высокие моральные эксперта";
            this.colMoralLevelValue.FieldName = "MoralLevelValue";
            this.colMoralLevelValue.Name = "colMoralLevelValue";
            // 
            // colDocsImprovementParticipationLevelValue
            // 
            this.colDocsImprovementParticipationLevelValue.Caption = "*Участие в совершенствовании нормативных документов";
            this.colDocsImprovementParticipationLevelValue.FieldName = "DocsImprovementParticipationLevelValue";
            this.colDocsImprovementParticipationLevelValue.Name = "colDocsImprovementParticipationLevelValue";
            // 
            // colStage2points
            // 
            this.colStage2points.Caption = "Итоги 2-й этап";
            this.colStage2points.FieldName = "Stage2points";
            this.colStage2points.Name = "colStage2points";
            // 
            // colEfficiency
            // 
            this.colEfficiency.Caption = "Эффективность";
            this.colEfficiency.FieldName = "Efficiency";
            this.colEfficiency.Name = "colEfficiency";
            // 
            // colNextPeriodProposal
            // 
            this.colNextPeriodProposal.Caption = "Включение в БД следующего года";
            this.colNextPeriodProposal.FieldName = "NextPeriodProposal";
            this.colNextPeriodProposal.Name = "colNextPeriodProposal";
            // 
            // colAcademicDegree
            // 
            this.colAcademicDegree.Caption = "Ученая степень";
            this.colAcademicDegree.FieldName = "AcademicDegree.Code";
            this.colAcademicDegree.Name = "colAcademicDegree";
            // 
            // colAcademicTitle
            // 
            this.colAcademicTitle.Caption = "Ученое звание";
            this.colAcademicTitle.FieldName = "AcademicTitle.Code";
            this.colAcademicTitle.Name = "colAcademicTitle";
            // 
            // colPeriod
            // 
            this.colPeriod.Caption = "Учетный период";
            this.colPeriod.FieldName = "Period.Title";
            this.colPeriod.Name = "colPeriod";
            // 
            // colTs
            // 
            this.colTs.Caption = "Timestamp";
            this.colTs.DisplayFormat.FormatString = "O";
            this.colTs.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTs.FieldName = "Ts";
            this.colTs.Name = "colTs";
            // 
            // ExpertCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "ExpertCollectionView";
            this.Size = new System.Drawing.Size(621, 425);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpExperts;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem bbiImportFromCsv;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraGrid.Columns.GridColumn colImportCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colPatronymic;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkplace;
        private DevExpress.XtraGrid.Columns.GridColumn colPost;
        private DevExpress.XtraGrid.Columns.GridColumn colPostValue;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthPlace;
        private DevExpress.XtraGrid.Columns.GridColumn colLiveAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colAcademicDegreeValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAcademicTitleValue;
        private DevExpress.XtraGrid.Columns.GridColumn colExpirience;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciality;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialityValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkYearsTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkYearsEducation;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkYearsEducationValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkYearsChief;
        private DevExpress.XtraGrid.Columns.GridColumn colExpYearsEducation;
        private DevExpress.XtraGrid.Columns.GridColumn colExpYearsEducationValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAwards;
        private DevExpress.XtraGrid.Columns.GridColumn colAwardsValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCanMissionValue;
        private DevExpress.XtraGrid.Columns.GridColumn colChiefName;
        private DevExpress.XtraGrid.Columns.GridColumn colChiefPost;
        private DevExpress.XtraGrid.Columns.GridColumn colChiefEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colKind;
        private DevExpress.XtraGrid.Columns.GridColumn colCanMission;
        private DevExpress.XtraGrid.Columns.GridColumn colTestResults;
        private DevExpress.XtraGrid.Columns.GridColumn colTestResultsValue;
        private DevExpress.XtraGrid.Columns.GridColumn colInterviewStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colInterviewStatusValue;
        private DevExpress.XtraGrid.Columns.GridColumn colStage1points;
        private DevExpress.XtraGrid.Columns.GridColumn colEngage;
        private DevExpress.XtraGrid.Columns.GridColumn colSubjectLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAccuracyLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPunctualLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colIndependencyLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMoralLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDocsImprovementParticipationLevelValue;
        private DevExpress.XtraGrid.Columns.GridColumn colStage2points;
        private DevExpress.XtraGrid.Columns.GridColumn colEfficiency;
        private DevExpress.XtraGrid.Columns.GridColumn colNextPeriodProposal;
        private DevExpress.XtraGrid.Columns.GridColumn colAcademicDegree;
        private DevExpress.XtraGrid.Columns.GridColumn colAcademicTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colObserver;
        private DevExpress.XtraGrid.Columns.GridColumn colTs;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentsCount;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
