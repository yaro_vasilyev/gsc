﻿namespace GSC.Client.Modules.Database.Experts
{
    partial class ExpertAgreementControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.linkPreview = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.linkAdd = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.labelMessage = new DevExpress.XtraEditors.LabelControl();
            this.linkRemove = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.linkPreview, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.linkAdd, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelMessage, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.linkRemove, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(730, 30);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // linkPreview
            // 
            this.linkPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkPreview.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkPreview.Location = new System.Drawing.Point(573, 8);
            this.linkPreview.Name = "linkPreview";
            this.linkPreview.Size = new System.Drawing.Size(48, 13);
            this.linkPreview.TabIndex = 0;
            this.linkPreview.Text = "Просмотр";
            // 
            // linkAdd
            // 
            this.linkAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkAdd.Location = new System.Drawing.Point(627, 8);
            this.linkAdd.Name = "linkAdd";
            this.linkAdd.Size = new System.Drawing.Size(50, 13);
            this.linkAdd.TabIndex = 1;
            this.linkAdd.Text = "Добавить";
            // 
            // labelMessage
            // 
            this.labelMessage.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelMessage.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelMessage.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMessage.Location = new System.Drawing.Point(3, 3);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(564, 24);
            this.labelMessage.TabIndex = 2;
            this.labelMessage.Text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod";
            // 
            // linkRemove
            // 
            this.linkRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkRemove.Location = new System.Drawing.Point(683, 8);
            this.linkRemove.Name = "linkRemove";
            this.linkRemove.Size = new System.Drawing.Size(44, 13);
            this.linkRemove.TabIndex = 3;
            this.linkRemove.Text = "Удалить";
            // 
            // ExpertAgreementControl
            // 
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ExpertAgreementControl";
            this.Size = new System.Drawing.Size(730, 30);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.HyperlinkLabelControl linkPreview;
        private DevExpress.XtraEditors.HyperlinkLabelControl linkAdd;
        private DevExpress.XtraEditors.LabelControl labelMessage;
        private DevExpress.XtraEditors.HyperlinkLabelControl linkRemove;
    }
}
