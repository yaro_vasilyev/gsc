﻿using DevExpress.Utils;
using GSC.Client.ViewModels.Observer;

namespace GSC.Client.Modules.Database.Observers
{
    public partial class ObserverView : BaseEditView
    {
        public ObserverView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ObserverViewModel>();
            fluent.SetObjectDataSourceBinding(managerBindingSource,
                                              vm => vm.Entity,
                                              vm => vm.Update());
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
