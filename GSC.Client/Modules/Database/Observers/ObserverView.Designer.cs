﻿namespace GSC.Client.Modules.Database.Observers
{
    partial class ObserverView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.FioTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.managerBindingSource = new System.Windows.Forms.BindingSource();
            this.WorkplaceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.Phone1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.Phone2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForPhone2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkplace = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FioTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Observer.ObserverViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Observer.ObserverViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Observer.ObserverViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Observer.ObserverViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Observer.ObserverViewModel), "Close", this.bbiClose)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Observer.ObserverViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(569, 140);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.FioTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkplaceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.Phone1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.Phone2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.DataSource = this.managerBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 140);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1187, 176, 500, 514);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(569, 230);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // FioTextEdit
            // 
            this.FioTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Fio", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FioTextEdit.Location = new System.Drawing.Point(124, 12);
            this.FioTextEdit.MenuManager = this.ribbonControl1;
            this.FioTextEdit.Name = "FioTextEdit";
            this.FioTextEdit.Size = new System.Drawing.Size(433, 20);
            this.FioTextEdit.StyleController = this.dataLayoutControl1;
            this.FioTextEdit.TabIndex = 4;
            // 
            // managerBindingSource
            // 
            this.managerBindingSource.DataSource = typeof(GSC.Model.Observer);
            // 
            // WorkplaceTextEdit
            // 
            this.WorkplaceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Workplace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.WorkplaceTextEdit.Location = new System.Drawing.Point(124, 36);
            this.WorkplaceTextEdit.MenuManager = this.ribbonControl1;
            this.WorkplaceTextEdit.Name = "WorkplaceTextEdit";
            this.WorkplaceTextEdit.Size = new System.Drawing.Size(433, 20);
            this.WorkplaceTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkplaceTextEdit.TabIndex = 5;
            // 
            // PostTextEdit
            // 
            this.PostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Post", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PostTextEdit.Location = new System.Drawing.Point(124, 60);
            this.PostTextEdit.MenuManager = this.ribbonControl1;
            this.PostTextEdit.Name = "PostTextEdit";
            this.PostTextEdit.Size = new System.Drawing.Size(433, 20);
            this.PostTextEdit.StyleController = this.dataLayoutControl1;
            this.PostTextEdit.TabIndex = 6;
            // 
            // Phone1TextEdit
            // 
            this.Phone1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Phone1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Phone1TextEdit.Location = new System.Drawing.Point(124, 84);
            this.Phone1TextEdit.MenuManager = this.ribbonControl1;
            this.Phone1TextEdit.Name = "Phone1TextEdit";
            this.Phone1TextEdit.Size = new System.Drawing.Size(433, 20);
            this.Phone1TextEdit.StyleController = this.dataLayoutControl1;
            this.Phone1TextEdit.TabIndex = 7;
            // 
            // Phone2TextEdit
            // 
            this.Phone2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Phone2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Phone2TextEdit.Location = new System.Drawing.Point(124, 108);
            this.Phone2TextEdit.MenuManager = this.ribbonControl1;
            this.Phone2TextEdit.Name = "Phone2TextEdit";
            this.Phone2TextEdit.Size = new System.Drawing.Size(433, 20);
            this.Phone2TextEdit.StyleController = this.dataLayoutControl1;
            this.Phone2TextEdit.TabIndex = 8;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.managerBindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.Location = new System.Drawing.Point(124, 132);
            this.EmailTextEdit.MenuManager = this.ribbonControl1;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Size = new System.Drawing.Size(433, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 9;
            // 
            // ItemForPhone2
            // 
            this.ItemForPhone2.Control = this.Phone2TextEdit;
            this.ItemForPhone2.Location = new System.Drawing.Point(0, 96);
            this.ItemForPhone2.Name = "ItemForPhone2";
            this.ItemForPhone2.Size = new System.Drawing.Size(549, 24);
            this.ItemForPhone2.Text = "Мобильный телефон:";
            this.ItemForPhone2.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(569, 230);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFio,
            this.ItemForWorkplace,
            this.ItemForPost,
            this.ItemForPhone1,
            this.ItemForEmail,
            this.ItemForPhone2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(549, 210);
            // 
            // ItemForFio
            // 
            this.ItemForFio.Control = this.FioTextEdit;
            this.ItemForFio.Location = new System.Drawing.Point(0, 0);
            this.ItemForFio.Name = "ItemForFio";
            this.ItemForFio.Size = new System.Drawing.Size(549, 24);
            this.ItemForFio.Text = "ФИО:";
            this.ItemForFio.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForWorkplace
            // 
            this.ItemForWorkplace.Control = this.WorkplaceTextEdit;
            this.ItemForWorkplace.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkplace.Name = "ItemForWorkplace";
            this.ItemForWorkplace.Size = new System.Drawing.Size(549, 24);
            this.ItemForWorkplace.Text = "Место работы:";
            this.ItemForWorkplace.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForPost
            // 
            this.ItemForPost.Control = this.PostTextEdit;
            this.ItemForPost.Location = new System.Drawing.Point(0, 48);
            this.ItemForPost.Name = "ItemForPost";
            this.ItemForPost.Size = new System.Drawing.Size(549, 24);
            this.ItemForPost.Text = "Должность:";
            this.ItemForPost.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForPhone1
            // 
            this.ItemForPhone1.Control = this.Phone1TextEdit;
            this.ItemForPhone1.Location = new System.Drawing.Point(0, 72);
            this.ItemForPhone1.Name = "ItemForPhone1";
            this.ItemForPhone1.Size = new System.Drawing.Size(549, 24);
            this.ItemForPhone1.Text = "Рабочий телефон:";
            this.ItemForPhone1.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.Location = new System.Drawing.Point(0, 120);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(549, 90);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "ManagerView";
            this.Size = new System.Drawing.Size(569, 370);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FioTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkplaceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit FioTextEdit;
        private System.Windows.Forms.BindingSource managerBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraEditors.TextEdit WorkplaceTextEdit;
        private DevExpress.XtraEditors.TextEdit PostTextEdit;
        private DevExpress.XtraEditors.TextEdit Phone1TextEdit;
        private DevExpress.XtraEditors.TextEdit Phone2TextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFio;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkplace;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
    }
}
