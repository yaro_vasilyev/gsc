﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Observer;

namespace GSC.Client.Modules.Database.Observers.Manager
{
    public partial class ManagerCollectionView : CollectionBlockView
    {
        public ManagerCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ManagerCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private ManagerCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ManagerCollectionViewModel>(); }
        }
    }
}
