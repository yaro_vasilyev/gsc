﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Observer;

namespace GSC.Client.Modules.Database.Observers.RsmCoordinator
{
    public partial class RsmCoordinatorCollectionView : CollectionBlockView
    {
        public RsmCoordinatorCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindigns();
            }
        }

        private void InitBindigns()
        {
            var fluent = mvvmContext1.OfType<RsmCoordinatorCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private RsmCoordinatorCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<RsmCoordinatorCollectionViewModel>(); }
        }
    }
}
