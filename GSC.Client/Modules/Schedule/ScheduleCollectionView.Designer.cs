﻿namespace GSC.Client.Modules.Schedule
{
    partial class ScheduleCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.beRegion = new DevExpress.XtraBars.BarEditItem();
            this.filterLookupRegion = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource();
            this.beExamType = new DevExpress.XtraBars.BarEditItem();
            this.filterComboExamType = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.bePeriodType = new DevExpress.XtraBars.BarEditItem();
            this.filterComboPeriodType = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpSchedules = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgFilters = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.scheduleInfoBindingSource = new System.Windows.Forms.BindingSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colExpertName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonitorRcoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFixed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionCode = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLookupRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterComboExamType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterComboPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.beRegion,
            this.beExamType,
            this.bePeriodType,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 22;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpSchedules});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.filterLookupRegion,
            this.filterComboExamType,
            this.filterComboPeriodType});
            this.ribbonControl1.Size = new System.Drawing.Size(702, 141);
            this.ribbonControl1.Visible = false;
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Schedule.ScheduleCollectionViewModel);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 2;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 3;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 4;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // beRegion
            // 
            this.beRegion.Caption = "Субъект РФ";
            this.beRegion.Edit = this.filterLookupRegion;
            this.beRegion.EditWidth = 204;
            this.beRegion.Id = 5;
            this.beRegion.Name = "beRegion";
            this.beRegion.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // filterLookupRegion
            // 
            this.filterLookupRegion.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.filterLookupRegion.AutoHeight = false;
            this.filterLookupRegion.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            this.filterLookupRegion.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.filterLookupRegion.DataSource = this.regionBindingSource;
            this.filterLookupRegion.DisplayMember = "Name";
            this.filterLookupRegion.LookAndFeel.UseDefaultLookAndFeel = false;
            this.filterLookupRegion.Name = "filterLookupRegion";
            this.filterLookupRegion.NullText = "";
            this.filterLookupRegion.NullValuePrompt = "Субъект РФ";
            this.filterLookupRegion.NullValuePromptShowForEmptyValue = true;
            this.filterLookupRegion.ValueMember = "Id";
            this.filterLookupRegion.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.filterEdit_ButtonClick);
            this.filterLookupRegion.EditValueChanged += new System.EventHandler(this.filterLookupRegion_EditValueChanged);
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // beExamType
            // 
            this.beExamType.Caption = "Форма";
            this.beExamType.Edit = this.filterComboExamType;
            this.beExamType.EditWidth = 100;
            this.beExamType.Id = 11;
            this.beExamType.Name = "beExamType";
            this.beExamType.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // filterComboExamType
            // 
            this.filterComboExamType.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.filterComboExamType.AutoHeight = false;
            this.filterComboExamType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            this.filterComboExamType.LookAndFeel.UseDefaultLookAndFeel = false;
            this.filterComboExamType.Name = "filterComboExamType";
            this.filterComboExamType.NullValuePrompt = "Форма";
            this.filterComboExamType.NullValuePromptShowForEmptyValue = true;
            this.filterComboExamType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.filterEdit_ButtonClick);
            this.filterComboExamType.EditValueChanged += new System.EventHandler(this.filterComboExamType_EditValueChanged);
            // 
            // bePeriodType
            // 
            this.bePeriodType.Caption = "Этап";
            this.bePeriodType.Edit = this.filterComboPeriodType;
            this.bePeriodType.EditWidth = 100;
            this.bePeriodType.Id = 12;
            this.bePeriodType.Name = "bePeriodType";
            this.bePeriodType.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // filterComboPeriodType
            // 
            this.filterComboPeriodType.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.filterComboPeriodType.AutoHeight = false;
            this.filterComboPeriodType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            this.filterComboPeriodType.LookAndFeel.UseDefaultLookAndFeel = false;
            this.filterComboPeriodType.Name = "filterComboPeriodType";
            this.filterComboPeriodType.NullValuePrompt = "Период";
            this.filterComboPeriodType.NullValuePromptShowForEmptyValue = true;
            this.filterComboPeriodType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.filterEdit_ButtonClick);
            this.filterComboPeriodType.EditValueChanged += new System.EventHandler(this.filterComboPeriodType_EditValueChanged);
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 20;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpSchedules
            // 
            this.rpSchedules.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgFilters,
            this.rpgExchange});
            this.rpSchedules.Name = "rpSchedules";
            this.rpSchedules.Text = "Графики";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 21;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgFilters
            // 
            this.rpgFilters.ItemLinks.Add(this.beRegion, false, "", "", true);
            this.rpgFilters.ItemLinks.Add(this.bePeriodType, false, "", "", true);
            this.rpgFilters.ItemLinks.Add(this.beExamType, false, "", "", true);
            this.rpgFilters.Name = "rpgFilters";
            this.rpgFilters.Text = "Фильтры";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.scheduleInfoBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridControl1.Size = new System.Drawing.Size(702, 334);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // scheduleInfoBindingSource
            // 
            this.scheduleInfoBindingSource.DataSource = typeof(GSC.Client.ViewModels.Schedule.ScheduleInfo);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRegionName,
            this.colExpertName,
            this.colMonitorRcoi,
            this.colFixed,
            this.colPeriodType,
            this.colRegionCode});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.gridView1_CellMerge);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Субъект РФ";
            this.colRegionName.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 0;
            this.colRegionName.Width = 117;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // colExpertName
            // 
            this.colExpertName.Caption = "Эксперт";
            this.colExpertName.ColumnEdit = this.repositoryItemMemoEdit1;
            this.colExpertName.FieldName = "ExpertName";
            this.colExpertName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpertName.MinWidth = 100;
            this.colExpertName.Name = "colExpertName";
            this.colExpertName.Visible = true;
            this.colExpertName.VisibleIndex = 1;
            this.colExpertName.Width = 133;
            // 
            // colMonitorRcoi
            // 
            this.colMonitorRcoi.AppearanceCell.Options.UseTextOptions = true;
            this.colMonitorRcoi.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colMonitorRcoi.Caption = "Мониторинг РЦОИ";
            this.colMonitorRcoi.FieldName = "MonitorRcoi";
            this.colMonitorRcoi.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colMonitorRcoi.MinWidth = 50;
            this.colMonitorRcoi.Name = "colMonitorRcoi";
            this.colMonitorRcoi.Visible = true;
            this.colMonitorRcoi.VisibleIndex = 2;
            // 
            // colFixed
            // 
            this.colFixed.AppearanceCell.Options.UseTextOptions = true;
            this.colFixed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colFixed.Caption = "Зафиксирован";
            this.colFixed.FieldName = "Fixed";
            this.colFixed.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colFixed.MinWidth = 50;
            this.colFixed.Name = "colFixed";
            this.colFixed.Visible = true;
            this.colFixed.VisibleIndex = 3;
            // 
            // colPeriodType
            // 
            this.colPeriodType.AppearanceCell.Options.UseTextOptions = true;
            this.colPeriodType.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colPeriodType.Caption = "Этап";
            this.colPeriodType.FieldName = "PeriodType";
            this.colPeriodType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPeriodType.MinWidth = 50;
            this.colPeriodType.Name = "colPeriodType";
            this.colPeriodType.Visible = true;
            this.colPeriodType.VisibleIndex = 4;
            // 
            // colRegionCode
            // 
            this.colRegionCode.AppearanceCell.Options.UseTextOptions = true;
            this.colRegionCode.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colRegionCode.Caption = "Код субъекта РФ";
            this.colRegionCode.FieldName = "RegionCode";
            this.colRegionCode.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRegionCode.MinWidth = 50;
            this.colRegionCode.Name = "colRegionCode";
            // 
            // ScheduleCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "ScheduleCollectionView";
            this.Size = new System.Drawing.Size(702, 475);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLookupRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterComboExamType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterComboPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpSchedules;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.BarEditItem beRegion;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit filterLookupRegion;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgFilters;
        private DevExpress.XtraBars.BarEditItem beExamType;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox filterComboExamType;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private DevExpress.XtraBars.BarEditItem bePeriodType;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox filterComboPeriodType;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource scheduleInfoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpertName;
        private DevExpress.XtraGrid.Columns.GridColumn colMonitorRcoi;
        private DevExpress.XtraGrid.Columns.GridColumn colFixed;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodType;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionCode;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.BarButtonItem bbiView;
    }
}
