﻿using System;
using System.Diagnostics;
using System.Linq;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using GSC.Client.Util;
using GSC.Client.ViewModels.Schedule;

namespace GSC.Client.Modules.Schedule
{
    public partial class ScheduleView : BaseEditView
    {
        public ScheduleView()
        {
            InitializeComponent();
            periodTypeCombo.Properties.Items.AddEnum<Model.PeriodType>();

            if (!DesignMode)
            {
                RegisterServices();
                var fluent = mvvmContext1.OfType<ScheduleViewModel>();
                fluent.SetObjectDataSourceBinding(scheduleBindingSource, vm => vm.Entity, vm => vm.Update());
            }
        }

        private void RegisterServices()
        {
            mvvmContext1.RegisterService(this);
            mvvmContext1.RegisterService(ScheduleViewModel.PpeGridServiceKey, new UpdateGridService(gridControl1));
            mvvmContext1.RegisterService(ScheduleViewModel.DetailsGridServiceKey, new UpdateGridService(gridControl2));
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            //dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
            MonitorRcoiCheckEdit.ReadOnly = readOnly;
        }

        /// <remarks>
        /// Called from BidningSource.DataSourceChanged (after Entity assignment in ViewModel)
        /// </remarks>
        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ScheduleViewModel>();

            fluent.SetBinding(periodTypeCombo, x => x.EditValue, vm => vm.PeriodType);

            #region Filters

            fluent.SetBinding(bciExamTypeEge, x => x.BindableChecked, vm => vm.ExamTypeEge);
            fluent.SetBinding(bciExamTypeGve, x => x.BindableChecked, vm => vm.ExamTypeGve);
            fluent.SetBinding(bciExamTypeOge, x => x.BindableChecked, vm => vm.ExamTypeOge);

            fluent.SetBinding(bciExpertKindOfficial, x => x.BindableChecked, vm => vm.ExpertKindOfficial);
            fluent.SetBinding(bciExpertKindSocial, x => x.BindableChecked, vm => vm.ExpertKindSocial);

            #endregion


            fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.LookupRegions.Entities);

            /*
            fluent.SetItemsSourceBinding(examDateCombo.Properties / * because fuck you, that's why! 
                https://www.devexpress.com/Support/Center/Question/Details/T324151 * /,
                                         props => props.Items,
                                         vm => vm.Dates,
                                         (item, date) => (DateTime) item.Value == date,
                                         date => new ImageComboBoxItem(date));
            */

            fluent.SetBinding(expertBindingSource, bs => bs.DataSource, vm => vm.LookupExperts.Entities);
            fluent.SetBinding(scheduleDetailBindingSource, bs => bs.DataSource, vm => vm.FilteredScheduleDetails);
            fluent.SetBinding(ppeBindingSource, bs => bs.DataSource, vm => vm.AvailableStations);

            fluent.SetBinding(layoutScheduleExists, c => c.ContentVisible, vm => vm.ScheduleExists);

            fluent.SetBinding(RegionLookUpEdit,
                              e => e.EditValue,
                              vm => vm.RegionId,
                              null,
                              XtraEditors.ConvertBack<int>);

            fluent.SetBinding(examDateEdit, e => e.EditValue, vm => vm.SelectedDate, 
                time => time,
                @object => @object as DateTime?);

            fluent.SetBinding(ExpertIdLookUpEdit,
                                e => e.EditValue,
                                vm => vm.ExpertId,
                                null,
                                XtraEditors.ConvertBack<int>);

            fluent.SetBinding(MonitorRcoiCheckEdit, ch => ch.EditValue, vm => vm.MonitorRcoi);

            fluent.WithEvent<SelectionChangedEventArgs>(gridView1, "SelectionChanged")
                .SetBinding(vm => vm.SelectedStations,
                            ea => gridView1.GetSelectedRows().Select(gridView1.GetRow).Cast<Model.Station>().ToArray());
            fluent.WithEvent<SelectionChangedEventArgs>(gridView2, "SelectionChanged")
                .SetBinding(vm => vm.SelectedDetails,
                            ea => gridView2.GetSelectedRows().Select(gridView2.GetRow).Cast<Model.ScheduleDetail>().ToArray());

            #region New/Edit form status
            fluent.SetBinding(periodTypeCombo, c => c.ReadOnly, vm => vm.IsEditMode);
            fluent.SetBinding(RegionLookUpEdit, c => c.ReadOnly, vm => vm.IsEditMode);
            fluent.SetBinding(ExpertIdLookUpEdit, c => c.ReadOnly, vm => vm.IsEditMode);
            #endregion
        }

        private void ScheduleBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            Debug.Assert(scheduleBindingSource.DataSource != null);
            InitBindings();
        }

        private ScheduleViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ScheduleViewModel>(); }
        }

        private void OnSplitterResize(object sender, EventArgs e)
        {
            var split = sender as SplitContainerControl;
            Debug.Assert(split != null);

            split.SplitterPosition = split.Width / 2;
        }

        private void dateEdit1_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ViewModel.Dates.Count == 0)
            {
                examDateEdit.Properties.MinValue = DateTime.MinValue;
                examDateEdit.Properties.MaxValue = DateTime.MaxValue;
                e.Cancel = true;
                return;
            }

            examDateEdit.Properties.MinValue = ViewModel.Dates[0].Date;
            examDateEdit.Properties.MaxValue = ViewModel.Dates[ViewModel.Dates.Count - 1].Date.AddDays(1).AddTicks(-1);

            examDateEdit.Properties.DisabledDateProvider = new CalendarDisabledDateProvider(ViewModel.Dates);
        }

        #region Master-Detail

        private void gridView1_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gridView1_MasterRowEmpty(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventArgs e)
        {
            e.IsEmpty = e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0;
        }

        private void gridView1_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            var view = (GridView)sender;
            if (e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0)
                return;

            var masterStation = view.GetRow(e.RowHandle) as Model.Station;
            if (masterStation == null)
                return;

            e.ChildList = ViewModel.GetExecsForStation(masterStation);
        }

        private void gridView1_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            if (e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0)
                return;
            e.RelationName = "ExecsOnStation";
        }

        private void gridView2_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gridView2_MasterRowEmpty(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventArgs e)
        {
            e.IsEmpty = e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0;
        }

        private void gridView2_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            var view = (GridView) sender;
            if (e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0)
                return;

            var detail = view.GetRow(e.RowHandle) as Model.ScheduleDetail;
            if (detail?.Station == null)
                return;

            e.ChildList = ViewModel.GetExecsForStation(detail.Station);
        }

        private void gridView2_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            if (e.RowHandle == GridControl.InvalidRowHandle || e.RelationIndex != 0)
                return;
            e.RelationName = "ExecsOnDetail";
        }

        #endregion
    }
}
