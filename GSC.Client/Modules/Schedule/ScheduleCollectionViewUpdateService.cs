﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using GSC.Client.ViewModels.Schedule;

namespace GSC.Client.Modules.Schedule
{

    public interface IScheduleCollectionViewUpdateService
    {
        void RefreshSchedules(IEnumerable<ScheduleInfo> schedules);

        void RefreshStations(IEnumerable<StationInfo> stations);

        void RefreshExamDates(IEnumerable<DateTime> examDates);
    }

    internal class ScheduleCollectionViewUpdateService : IScheduleCollectionViewUpdateService
    {
        private readonly GridView gridView;

        private const int DefaultWidth = 100;

        private readonly DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit edit
            = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();


        public ScheduleCollectionViewUpdateService(GridView gridView)
        {
            Debug.Assert(gridView != null);
            this.gridView = gridView;
            edit.WordWrap = true;
        }

        public void RefreshSchedules(IEnumerable<ScheduleInfo> schedules)
        {
            gridView.RefreshData();
        }

        public void RefreshStations(IEnumerable<StationInfo> stations) { }


        public void RefreshExamDates(IEnumerable<DateTime> examDates)
        {
            gridView.BeginUpdate();
            try
            {
                RemoveUnboundColumns();

                foreach (var examDate in examDates)
                {
                    var column = gridView.Columns.AddVisible(examDate.ToString("d"));
                    //column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    column.Tag = examDate;
                    column.Caption = examDate.ToString("d");
                    column.MinWidth = DefaultWidth;
                    column.ColumnEdit = edit;
                    column.UnboundType = UnboundColumnType.String;
                }
            }
            finally
            {
                gridView.EndUpdate();
            }
        }

        private void RemoveUnboundColumns()
        {
            var unboundColumns = gridView.Columns.Where(c => c.UnboundType != UnboundColumnType.Bound).ToArray();
            foreach (var column in unboundColumns)
            {
                gridView.Columns.Remove(column);
            }
        }
    }
}
