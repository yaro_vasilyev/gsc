﻿namespace GSC.Client.Modules.Schedule
{
    partial class ScheduleView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            this.gvExecsOnStation = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubjectNameL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountPartL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomVideoL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKimL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubjectCodeL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.ppeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.bciExamTypeEge = new DevExpress.XtraBars.BarCheckItem();
            this.bciExamTypeGve = new DevExpress.XtraBars.BarCheckItem();
            this.bciExamTypeOge = new DevExpress.XtraBars.BarCheckItem();
            this.bciExpertKindOfficial = new DevExpress.XtraBars.BarCheckItem();
            this.bciExpertKindSocial = new DevExpress.XtraBars.BarCheckItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.prgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgExamType = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgExpertKind = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gvExecsOnDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubjectNameR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountPartR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountRoomVideoR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKimR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubjectCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.scheduleDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiAddSelected = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemoveSelected = new DevExpress.XtraBars.BarButtonItem();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.examDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.periodTypeCombo = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.labelScheduleExists = new DevExpress.XtraEditors.LabelControl();
            this.ExpertIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.scheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.expertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MonitorRcoiCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RegionLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRegionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpertId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMonitorRcoi = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutScheduleExists = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.detailsControl_BarManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.availablePpes_BarManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.gvExecsOnStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExecsOnDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.examDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodTypeCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExpertIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonitorRcoiCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMonitorRcoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutScheduleExists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsControl_BarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availablePpes_BarManager)).BeginInit();
            this.SuspendLayout();
            // 
            // gvExecsOnStation
            // 
            this.gvExecsOnStation.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubjectNameL,
            this.colCountPartL,
            this.colCountRoomL,
            this.colCountRoomVideoL,
            this.colKimL,
            this.colSubjectCodeL});
            this.gvExecsOnStation.GridControl = this.gridControl1;
            this.gvExecsOnStation.Name = "gvExecsOnStation";
            this.gvExecsOnStation.OptionsBehavior.ReadOnly = true;
            this.gvExecsOnStation.OptionsView.ShowGroupPanel = false;
            this.gvExecsOnStation.ViewCaption = "Экзамены";
            // 
            // colSubjectNameL
            // 
            this.colSubjectNameL.Caption = "Предмет";
            this.colSubjectNameL.FieldName = "Exam.Subject.Name";
            this.colSubjectNameL.MinWidth = 100;
            this.colSubjectNameL.Name = "colSubjectNameL";
            this.colSubjectNameL.OptionsColumn.AllowEdit = false;
            this.colSubjectNameL.Visible = true;
            this.colSubjectNameL.VisibleIndex = 0;
            this.colSubjectNameL.Width = 100;
            // 
            // colCountPartL
            // 
            this.colCountPartL.Caption = "Участников";
            this.colCountPartL.FieldName = "CountPart";
            this.colCountPartL.MinWidth = 50;
            this.colCountPartL.Name = "colCountPartL";
            this.colCountPartL.OptionsColumn.AllowEdit = false;
            this.colCountPartL.Visible = true;
            this.colCountPartL.VisibleIndex = 1;
            this.colCountPartL.Width = 63;
            // 
            // colCountRoomL
            // 
            this.colCountRoomL.Caption = "Аудиторий";
            this.colCountRoomL.FieldName = "CountRoom";
            this.colCountRoomL.MinWidth = 50;
            this.colCountRoomL.Name = "colCountRoomL";
            this.colCountRoomL.OptionsColumn.AllowEdit = false;
            this.colCountRoomL.Visible = true;
            this.colCountRoomL.VisibleIndex = 2;
            this.colCountRoomL.Width = 63;
            // 
            // colCountRoomVideoL
            // 
            this.colCountRoomVideoL.Caption = "Аудиторий с видеонаблюдением";
            this.colCountRoomVideoL.FieldName = "CountRoomVideo";
            this.colCountRoomVideoL.MinWidth = 50;
            this.colCountRoomVideoL.Name = "colCountRoomVideoL";
            this.colCountRoomVideoL.OptionsColumn.AllowEdit = false;
            this.colCountRoomVideoL.Visible = true;
            this.colCountRoomVideoL.VisibleIndex = 3;
            this.colCountRoomVideoL.Width = 63;
            // 
            // colKimL
            // 
            this.colKimL.Caption = "Печать КИМ";
            this.colKimL.FieldName = "Kim";
            this.colKimL.MinWidth = 50;
            this.colKimL.Name = "colKimL";
            this.colKimL.OptionsColumn.AllowEdit = false;
            this.colKimL.Visible = true;
            this.colKimL.VisibleIndex = 4;
            this.colKimL.Width = 50;
            // 
            // colSubjectCodeL
            // 
            this.colSubjectCodeL.Caption = "Код предмета";
            this.colSubjectCodeL.FieldName = "Exam.Subject.Code";
            this.colSubjectCodeL.MinWidth = 50;
            this.colSubjectCodeL.Name = "colSubjectCodeL";
            this.colSubjectCodeL.OptionsColumn.AllowEdit = false;
            this.colSubjectCodeL.Visible = true;
            this.colSubjectCodeL.VisibleIndex = 5;
            this.colSubjectCodeL.Width = 83;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.ppeBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvExecsOnStation;
            gridLevelNode1.RelationName = "ExecsOnStation";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 24);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(440, 369);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gvExecsOnStation});
            // 
            // ppeBindingSource
            // 
            this.ppeBindingSource.DataSource = typeof(GSC.Model.Station);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCode1,
            this.colName1,
            this.colAddress1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 40;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gridView1_MasterRowEmpty);
            this.gridView1.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridView1_MasterRowGetChildList);
            this.gridView1.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationName);
            this.gridView1.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridView1_MasterRowGetRelationCount);
            // 
            // colCode1
            // 
            this.colCode1.Caption = "Код";
            this.colCode1.FieldName = "Code";
            this.colCode1.MinWidth = 25;
            this.colCode1.Name = "colCode1";
            this.colCode1.OptionsColumn.AllowEdit = false;
            this.colCode1.OptionsColumn.ReadOnly = true;
            this.colCode1.Visible = true;
            this.colCode1.VisibleIndex = 1;
            this.colCode1.Width = 45;
            // 
            // colName1
            // 
            this.colName1.Caption = "Название";
            this.colName1.FieldName = "Name";
            this.colName1.MinWidth = 100;
            this.colName1.Name = "colName1";
            this.colName1.OptionsColumn.AllowEdit = false;
            this.colName1.OptionsColumn.ReadOnly = true;
            this.colName1.Visible = true;
            this.colName1.VisibleIndex = 2;
            this.colName1.Width = 116;
            // 
            // colAddress1
            // 
            this.colAddress1.Caption = "Адрес";
            this.colAddress1.FieldName = "Address";
            this.colAddress1.MinWidth = 100;
            this.colAddress1.Name = "colAddress1";
            this.colAddress1.OptionsColumn.AllowEdit = false;
            this.colAddress1.OptionsColumn.ReadOnly = true;
            this.colAddress1.Visible = true;
            this.colAddress1.VisibleIndex = 3;
            this.colAddress1.Width = 116;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose,
            this.bciExamTypeEge,
            this.bciExamTypeGve,
            this.bciExamTypeOge,
            this.bciExpertKindOfficial,
            this.bciExpertKindSocial});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 19;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(908, 141);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // bciExamTypeEge
            // 
            this.bciExamTypeEge.Caption = "ЕГЭ";
            this.bciExamTypeEge.GroupIndex = 2;
            this.bciExamTypeEge.Id = 14;
            this.bciExamTypeEge.Name = "bciExamTypeEge";
            // 
            // bciExamTypeGve
            // 
            this.bciExamTypeGve.Caption = "ГВЭ-11";
            this.bciExamTypeGve.GroupIndex = 2;
            this.bciExamTypeGve.Id = 15;
            this.bciExamTypeGve.Name = "bciExamTypeGve";
            // 
            // bciExamTypeOge
            // 
            this.bciExamTypeOge.Caption = "ОГЭ";
            this.bciExamTypeOge.GroupIndex = 2;
            this.bciExamTypeOge.Id = 16;
            this.bciExamTypeOge.Name = "bciExamTypeOge";
            // 
            // bciExpertKindOfficial
            // 
            this.bciExpertKindOfficial.BindableChecked = true;
            this.bciExpertKindOfficial.Caption = "Член ГЭК";
            this.bciExpertKindOfficial.Checked = true;
            this.bciExpertKindOfficial.Id = 17;
            this.bciExpertKindOfficial.Name = "bciExpertKindOfficial";
            // 
            // bciExpertKindSocial
            // 
            this.bciExpertKindSocial.BindableChecked = true;
            this.bciExpertKindSocial.Caption = "Общественный наблюдатель";
            this.bciExpertKindSocial.Checked = true;
            this.bciExpertKindSocial.Id = 18;
            this.bciExpertKindSocial.Name = "bciExpertKindSocial";
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.prgSave,
            this.rpgExamType,
            this.rpgExpertKind,
            this.rpgCancel});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // prgSave
            // 
            this.prgSave.ItemLinks.Add(this.bbiSave);
            this.prgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.prgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.prgSave.Name = "prgSave";
            this.prgSave.Text = "Сохранение";
            // 
            // rpgExamType
            // 
            this.rpgExamType.ItemLinks.Add(this.bciExamTypeEge);
            this.rpgExamType.ItemLinks.Add(this.bciExamTypeGve);
            this.rpgExamType.ItemLinks.Add(this.bciExamTypeOge);
            this.rpgExamType.Name = "rpgExamType";
            this.rpgExamType.Text = "Форма ГИА";
            // 
            // rpgExpertKind
            // 
            this.rpgExpertKind.ItemLinks.Add(this.bciExpertKindOfficial);
            this.rpgExpertKind.ItemLinks.Add(this.bciExpertKindSocial);
            this.rpgExpertKind.Name = "rpgExpertKind";
            this.rpgExpertKind.Text = "Эксперт";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // gvExecsOnDetail
            // 
            this.gvExecsOnDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubjectNameR,
            this.colCountPartR,
            this.colCountRoomR,
            this.colCountRoomVideoR,
            this.colKimR,
            this.colSubjectCode});
            this.gvExecsOnDetail.GridControl = this.gridControl2;
            this.gvExecsOnDetail.Name = "gvExecsOnDetail";
            this.gvExecsOnDetail.OptionsView.ShowGroupPanel = false;
            this.gvExecsOnDetail.ViewCaption = "Экзамены";
            // 
            // colSubjectNameR
            // 
            this.colSubjectNameR.Caption = "Предмет";
            this.colSubjectNameR.FieldName = "Exam.Subject.Name";
            this.colSubjectNameR.MinWidth = 100;
            this.colSubjectNameR.Name = "colSubjectNameR";
            this.colSubjectNameR.OptionsColumn.AllowEdit = false;
            this.colSubjectNameR.Visible = true;
            this.colSubjectNameR.VisibleIndex = 0;
            this.colSubjectNameR.Width = 100;
            // 
            // colCountPartR
            // 
            this.colCountPartR.Caption = "Участников";
            this.colCountPartR.FieldName = "CountPart";
            this.colCountPartR.MinWidth = 50;
            this.colCountPartR.Name = "colCountPartR";
            this.colCountPartR.OptionsColumn.AllowEdit = false;
            this.colCountPartR.Visible = true;
            this.colCountPartR.VisibleIndex = 1;
            // 
            // colCountRoomR
            // 
            this.colCountRoomR.Caption = "Аудиторий";
            this.colCountRoomR.FieldName = "CountRoom";
            this.colCountRoomR.MinWidth = 50;
            this.colCountRoomR.Name = "colCountRoomR";
            this.colCountRoomR.OptionsColumn.AllowEdit = false;
            this.colCountRoomR.Visible = true;
            this.colCountRoomR.VisibleIndex = 2;
            // 
            // colCountRoomVideoR
            // 
            this.colCountRoomVideoR.Caption = "Аудиторий с видеонаблюдением";
            this.colCountRoomVideoR.FieldName = "CountRoomVideo";
            this.colCountRoomVideoR.MinWidth = 50;
            this.colCountRoomVideoR.Name = "colCountRoomVideoR";
            this.colCountRoomVideoR.OptionsColumn.AllowEdit = false;
            this.colCountRoomVideoR.Visible = true;
            this.colCountRoomVideoR.VisibleIndex = 3;
            // 
            // colKimR
            // 
            this.colKimR.Caption = "Печать КИМ";
            this.colKimR.FieldName = "Kim";
            this.colKimR.MinWidth = 50;
            this.colKimR.Name = "colKimR";
            this.colKimR.OptionsColumn.AllowEdit = false;
            this.colKimR.Visible = true;
            this.colKimR.VisibleIndex = 4;
            // 
            // colSubjectCode
            // 
            this.colSubjectCode.Caption = "Код предмета";
            this.colSubjectCode.FieldName = "Exam.Subject.Code";
            this.colSubjectCode.MinWidth = 50;
            this.colSubjectCode.Name = "colSubjectCode";
            this.colSubjectCode.OptionsColumn.AllowEdit = false;
            this.colSubjectCode.Visible = true;
            this.colSubjectCode.VisibleIndex = 5;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.scheduleDetailBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.LevelTemplate = this.gvExecsOnDetail;
            gridLevelNode2.RelationName = "ExecsOnDetail";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl2.Location = new System.Drawing.Point(0, 24);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.ribbonControl1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(439, 369);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gvExecsOnDetail});
            // 
            // scheduleDetailBindingSource
            // 
            this.scheduleDetailBindingSource.DataSource = typeof(GSC.Model.ScheduleDetail);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCode2,
            this.colName2,
            this.colAddress2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.ReadOnly = true;
            this.gridView2.OptionsDetail.ShowDetailTabs = false;
            this.gridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 40;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gridView2_MasterRowEmpty);
            this.gridView2.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridView2_MasterRowGetChildList);
            this.gridView2.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView2_MasterRowGetRelationName);
            this.gridView2.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridView2_MasterRowGetRelationCount);
            // 
            // colCode2
            // 
            this.colCode2.Caption = "Код";
            this.colCode2.FieldName = "Station.Code";
            this.colCode2.MinWidth = 50;
            this.colCode2.Name = "colCode2";
            this.colCode2.OptionsColumn.AllowEdit = false;
            this.colCode2.OptionsColumn.ReadOnly = true;
            this.colCode2.Visible = true;
            this.colCode2.VisibleIndex = 1;
            this.colCode2.Width = 50;
            // 
            // colName2
            // 
            this.colName2.Caption = "Название";
            this.colName2.FieldName = "Station.Name";
            this.colName2.MinWidth = 100;
            this.colName2.Name = "colName2";
            this.colName2.OptionsColumn.AllowEdit = false;
            this.colName2.OptionsColumn.ReadOnly = true;
            this.colName2.Visible = true;
            this.colName2.VisibleIndex = 2;
            this.colName2.Width = 117;
            // 
            // colAddress2
            // 
            this.colAddress2.Caption = "Адрес";
            this.colAddress2.FieldName = "Station.Address";
            this.colAddress2.MinWidth = 100;
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.OptionsColumn.AllowEdit = false;
            this.colAddress2.OptionsColumn.ReadOnly = true;
            this.colAddress2.Visible = true;
            this.colAddress2.VisibleIndex = 3;
            this.colAddress2.Width = 117;
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "Close", this.bbiClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "AddSelectedStations", this.bbiAddSelected),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel), "RemoveSelectedDetails", this.bbiRemoveSelected)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Schedule.ScheduleViewModel);
            // 
            // bbiAddSelected
            // 
            this.bbiAddSelected.Caption = "Добавить выбранное";
            this.bbiAddSelected.Id = 0;
            this.bbiAddSelected.ImageUri.Uri = "Add";
            this.bbiAddSelected.Name = "bbiAddSelected";
            this.bbiAddSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiRemoveSelected
            // 
            this.bbiRemoveSelected.Caption = "Удалить выбранное";
            this.bbiRemoveSelected.Id = 0;
            this.bbiRemoveSelected.ImageUri.Uri = "Delete";
            this.bbiRemoveSelected.Name = "bbiRemoveSelected";
            this.bbiRemoveSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.examDateEdit);
            this.dataLayoutControl1.Controls.Add(this.periodTypeCombo);
            this.dataLayoutControl1.Controls.Add(this.splitContainerControl1);
            this.dataLayoutControl1.Controls.Add(this.labelScheduleExists);
            this.dataLayoutControl1.Controls.Add(this.ExpertIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.MonitorRcoiCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionLookUpEdit);
            this.dataLayoutControl1.DataSource = this.scheduleBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(960, 141, 727, 773);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(908, 506);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // examDateEdit
            // 
            this.examDateEdit.EditValue = null;
            this.examDateEdit.Location = new System.Drawing.Point(80, 60);
            this.examDateEdit.MenuManager = this.ribbonControl1;
            this.examDateEdit.Name = "examDateEdit";
            this.examDateEdit.Properties.AppearanceCalendar.DayCell.Options.UseForeColor = true;
            this.examDateEdit.Properties.AppearanceCalendar.DayCellSpecial.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.examDateEdit.Properties.AppearanceCalendar.DayCellSpecial.Options.UseFont = true;
            this.examDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.examDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.examDateEdit.Size = new System.Drawing.Size(372, 20);
            this.examDateEdit.StyleController = this.dataLayoutControl1;
            this.examDateEdit.TabIndex = 5;
            this.examDateEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.dateEdit1_QueryPopUp);
            // 
            // periodTypeCombo
            // 
            this.periodTypeCombo.Location = new System.Drawing.Point(80, 12);
            this.periodTypeCombo.MenuManager = this.ribbonControl1;
            this.periodTypeCombo.Name = "periodTypeCombo";
            this.periodTypeCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.periodTypeCombo.Size = new System.Drawing.Size(372, 20);
            this.periodTypeCombo.StyleController = this.dataLayoutControl1;
            this.periodTypeCombo.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Location = new System.Drawing.Point(12, 101);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControl4);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControlLeft);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControlRight);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControlBottom);
            this.splitContainerControl1.Panel2.Controls.Add(this.barDockControlTop);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(884, 393);
            this.splitContainerControl1.SplitterPosition = 440;
            this.splitContainerControl1.TabIndex = 6;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.Resize += new System.EventHandler(this.OnSplitterResize);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 24);
            this.barDockControl3.Size = new System.Drawing.Size(0, 369);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(440, 24);
            this.barDockControl4.Size = new System.Drawing.Size(0, 369);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 393);
            this.barDockControl2.Size = new System.Drawing.Size(440, 0);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(440, 24);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 369);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(439, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 369);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 393);
            this.barDockControlBottom.Size = new System.Drawing.Size(439, 0);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(439, 24);
            // 
            // labelScheduleExists
            // 
            this.labelScheduleExists.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelScheduleExists.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelScheduleExists.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelScheduleExists.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelScheduleExists.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelScheduleExists.Location = new System.Drawing.Point(12, 84);
            this.labelScheduleExists.Name = "labelScheduleExists";
            this.labelScheduleExists.Size = new System.Drawing.Size(884, 13);
            this.labelScheduleExists.StyleController = this.dataLayoutControl1;
            this.labelScheduleExists.TabIndex = 1;
            this.labelScheduleExists.Text = "Нельзя создать график с выбранными в текущий момент экспертом, субъектом РФ и эта" +
    "пом. Такой график уже существует.";
            this.labelScheduleExists.Visible = false;
            // 
            // ExpertIdLookUpEdit
            // 
            this.ExpertIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.scheduleBindingSource, "ExpertId", true));
            this.ExpertIdLookUpEdit.Location = new System.Drawing.Point(524, 12);
            this.ExpertIdLookUpEdit.MenuManager = this.ribbonControl1;
            this.ExpertIdLookUpEdit.Name = "ExpertIdLookUpEdit";
            this.ExpertIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpertIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExpertIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpertIdLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "ФИО", 56, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Surname", "Фамилия", 52, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Имя", 37, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Patronymic", "Отчество", 63, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BirthDate", "Дата рождения", 58, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kind", "Категория", 30, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CanMission", "Готовность к выезду в регион", 66, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.ExpertIdLookUpEdit.Properties.DataSource = this.expertBindingSource;
            this.ExpertIdLookUpEdit.Properties.DisplayMember = "FullName";
            this.ExpertIdLookUpEdit.Properties.NullText = "";
            this.ExpertIdLookUpEdit.Properties.ValueMember = "Id";
            this.ExpertIdLookUpEdit.Size = new System.Drawing.Size(372, 20);
            this.ExpertIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ExpertIdLookUpEdit.TabIndex = 2;
            // 
            // scheduleBindingSource
            // 
            this.scheduleBindingSource.DataSource = typeof(GSC.Model.Schedule);
            this.scheduleBindingSource.DataSourceChanged += new System.EventHandler(this.ScheduleBindingSource_DataSourceChanged);
            // 
            // expertBindingSource
            // 
            this.expertBindingSource.DataSource = typeof(GSC.Model.Expert);
            // 
            // MonitorRcoiCheckEdit
            // 
            this.MonitorRcoiCheckEdit.Location = new System.Drawing.Point(524, 36);
            this.MonitorRcoiCheckEdit.MenuManager = this.ribbonControl1;
            this.MonitorRcoiCheckEdit.Name = "MonitorRcoiCheckEdit";
            this.MonitorRcoiCheckEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.MonitorRcoiCheckEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MonitorRcoiCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.MonitorRcoiCheckEdit.Properties.Caption = "Мониторинг РЦОИ";
            this.MonitorRcoiCheckEdit.Size = new System.Drawing.Size(372, 19);
            this.MonitorRcoiCheckEdit.StyleController = this.dataLayoutControl1;
            this.MonitorRcoiCheckEdit.TabIndex = 4;
            // 
            // RegionLookUpEdit
            // 
            this.RegionLookUpEdit.Location = new System.Drawing.Point(80, 36);
            this.RegionLookUpEdit.MenuManager = this.ribbonControl1;
            this.RegionLookUpEdit.Name = "RegionLookUpEdit";
            this.RegionLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RegionLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RegionLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegionLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.RegionLookUpEdit.Properties.DataSource = this.regionBindingSource;
            this.RegionLookUpEdit.Properties.DisplayMember = "Name";
            this.RegionLookUpEdit.Properties.NullText = "";
            this.RegionLookUpEdit.Properties.ValueMember = "Id";
            this.RegionLookUpEdit.Size = new System.Drawing.Size(372, 20);
            this.RegionLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RegionLookUpEdit.TabIndex = 3;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRegionId,
            this.ItemForExpertId,
            this.ItemForMonitorRcoi,
            this.layoutScheduleExists,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 50D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 24D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 24D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 24D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition4.Height = 17D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition5.Height = 100D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.layoutControlGroup1.Size = new System.Drawing.Size(908, 506);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRegionId
            // 
            this.ItemForRegionId.Control = this.RegionLookUpEdit;
            this.ItemForRegionId.CustomizationFormText = "Субъект РФ";
            this.ItemForRegionId.Location = new System.Drawing.Point(0, 24);
            this.ItemForRegionId.Name = "ItemForRegionId";
            this.ItemForRegionId.OptionsTableLayoutItem.RowIndex = 1;
            this.ItemForRegionId.Size = new System.Drawing.Size(444, 24);
            this.ItemForRegionId.Text = "Субъект РФ:";
            this.ItemForRegionId.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForExpertId
            // 
            this.ItemForExpertId.Control = this.ExpertIdLookUpEdit;
            this.ItemForExpertId.CustomizationFormText = "Эксперт";
            this.ItemForExpertId.Location = new System.Drawing.Point(444, 0);
            this.ItemForExpertId.Name = "ItemForExpertId";
            this.ItemForExpertId.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForExpertId.Size = new System.Drawing.Size(444, 24);
            this.ItemForExpertId.Text = "Эксперт:";
            this.ItemForExpertId.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForMonitorRcoi
            // 
            this.ItemForMonitorRcoi.Control = this.MonitorRcoiCheckEdit;
            this.ItemForMonitorRcoi.Location = new System.Drawing.Point(444, 24);
            this.ItemForMonitorRcoi.Name = "ItemForMonitorRcoi";
            this.ItemForMonitorRcoi.OptionsTableLayoutItem.ColumnIndex = 1;
            this.ItemForMonitorRcoi.OptionsTableLayoutItem.RowIndex = 1;
            this.ItemForMonitorRcoi.Size = new System.Drawing.Size(444, 24);
            this.ItemForMonitorRcoi.Spacing = new DevExpress.XtraLayout.Utils.Padding(68, 0, 0, 0);
            this.ItemForMonitorRcoi.Text = "Мониторинг РЦОИ";
            this.ItemForMonitorRcoi.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForMonitorRcoi.TextVisible = false;
            // 
            // layoutScheduleExists
            // 
            this.layoutScheduleExists.Control = this.labelScheduleExists;
            this.layoutScheduleExists.Location = new System.Drawing.Point(0, 72);
            this.layoutScheduleExists.Name = "layoutScheduleExists";
            this.layoutScheduleExists.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutScheduleExists.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutScheduleExists.Size = new System.Drawing.Size(888, 17);
            this.layoutScheduleExists.TextSize = new System.Drawing.Size(0, 0);
            this.layoutScheduleExists.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.splitContainerControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem1.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem1.Size = new System.Drawing.Size(888, 397);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.periodTypeCombo;
            this.layoutControlItem2.CustomizationFormText = "Этап";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(444, 24);
            this.layoutControlItem2.Text = "Этап:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.examDateEdit;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(444, 24);
            this.layoutControlItem3.Text = "Дата:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(65, 13);
            // 
            // detailsControl_BarManager
            // 
            this.detailsControl_BarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar5});
            this.detailsControl_BarManager.DockControls.Add(this.barDockControlTop);
            this.detailsControl_BarManager.DockControls.Add(this.barDockControlBottom);
            this.detailsControl_BarManager.DockControls.Add(this.barDockControlLeft);
            this.detailsControl_BarManager.DockControls.Add(this.barDockControlRight);
            this.detailsControl_BarManager.Form = this.splitContainerControl1.Panel2;
            this.detailsControl_BarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRemoveSelected});
            this.detailsControl_BarManager.MainMenu = this.bar5;
            this.detailsControl_BarManager.MaxItemId = 2;
            // 
            // bar5
            // 
            this.bar5.BarName = "Main menu";
            this.bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemoveSelected)});
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.MultiLine = true;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Main menu";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // availablePpes_BarManager
            // 
            this.availablePpes_BarManager.AllowQuickCustomization = false;
            this.availablePpes_BarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.availablePpes_BarManager.DockControls.Add(this.barDockControl1);
            this.availablePpes_BarManager.DockControls.Add(this.barDockControl2);
            this.availablePpes_BarManager.DockControls.Add(this.barDockControl3);
            this.availablePpes_BarManager.DockControls.Add(this.barDockControl4);
            this.availablePpes_BarManager.Form = this.splitContainerControl1.Panel1;
            this.availablePpes_BarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddSelected});
            this.availablePpes_BarManager.MainMenu = this.bar3;
            this.availablePpes_BarManager.MaxItemId = 2;
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddSelected)});
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // ScheduleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "ScheduleView";
            this.Size = new System.Drawing.Size(908, 647);
            ((System.ComponentModel.ISupportInitialize)(this.gvExecsOnStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExecsOnDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.examDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodTypeCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExpertIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonitorRcoiCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMonitorRcoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutScheduleExists)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsControl_BarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availablePpes_BarManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource scheduleBindingSource;
        private DevExpress.XtraEditors.LookUpEdit RegionLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit ExpertIdLookUpEdit;
        private DevExpress.XtraEditors.CheckEdit MonitorRcoiCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMonitorRcoi;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpertId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionId;
        private DevExpress.XtraBars.BarCheckItem bciExamTypeEge;
        private DevExpress.XtraBars.BarCheckItem bciExamTypeGve;
        private DevExpress.XtraBars.BarCheckItem bciExamTypeOge;
        private DevExpress.XtraBars.BarCheckItem bciExpertKindOfficial;
        private DevExpress.XtraBars.BarCheckItem bciExpertKindSocial;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExamType;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExpertKind;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private System.Windows.Forms.BindingSource scheduleDetailBindingSource;
        private System.Windows.Forms.BindingSource ppeBindingSource;
        private DevExpress.XtraEditors.LabelControl labelScheduleExists;
        private DevExpress.XtraLayout.LayoutControlItem layoutScheduleExists;
        private System.Windows.Forms.BindingSource expertBindingSource;
        private DevExpress.XtraBars.BarManager detailsControl_BarManager;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiRemoveSelected;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarManager availablePpes_BarManager;
        private DevExpress.XtraBars.BarButtonItem bbiAddSelected;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress2;
        private DevExpress.XtraGrid.Columns.GridColumn colCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colName2;
        private DevExpress.XtraEditors.ImageComboBoxEdit periodTypeCombo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DateEdit examDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Views.Grid.GridView gvExecsOnStation;
        private DevExpress.XtraGrid.Columns.GridColumn colSubjectNameL;
        private DevExpress.XtraGrid.Columns.GridColumn colCountPartL;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomL;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomVideoL;
        private DevExpress.XtraGrid.Columns.GridColumn colKimL;
        private DevExpress.XtraGrid.Columns.GridColumn colSubjectCodeL;
        private DevExpress.XtraGrid.Views.Grid.GridView gvExecsOnDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colSubjectNameR;
        private DevExpress.XtraGrid.Columns.GridColumn colCountPartR;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomR;
        private DevExpress.XtraGrid.Columns.GridColumn colCountRoomVideoR;
        private DevExpress.XtraGrid.Columns.GridColumn colKimR;
        private DevExpress.XtraGrid.Columns.GridColumn colSubjectCode;
    }
}
