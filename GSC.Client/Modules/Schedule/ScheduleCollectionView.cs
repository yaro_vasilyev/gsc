﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using GSC.Client.Util;
using GSC.Client.ViewModels.Schedule;

namespace GSC.Client.Modules.Schedule
{
    public partial class ScheduleCollectionView : CollectionBlockView
    {
        public ScheduleCollectionView()
        {
            InitializeComponent();


            filterComboExamType.Items.AddEnum<Model.ExamType>();
            filterComboPeriodType.Items.AddEnum<Model.PeriodType>();

            if (!DesignMode)
            {
                mvvmContext1.RegisterService(new ScheduleCollectionViewUpdateService(gridView1));
                InitBindigs();
            }
        }

        #region MVVM-init

        private void InitBindigs()
        {
            var fluent = mvvmContext1.OfType<ScheduleCollectionViewModel>();
            fluent.SetBinding(gridView1, v => v.LoadingPanelVisible, vm => vm.IsLoading);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.FilteredScheduleInfos);

            fluent.WithEvent<ColumnView, FocusedRowObjectChangedEventArgs>(gridView1, "FocusedRowObjectChanged")
                .SetBinding(vm => vm.SelectedSchedule,
                            ea => ea.Row as ScheduleInfo,
                            (v, e) => v.FocusedRowHandle = v.FindRow(e));

            fluent.WithEvent<RowCellClickEventArgs>(gridView1, "RowCellClick")
                .EventToCommand(vm => vm.Edit(null),
                    vm => vm.SelectedEntity /* EditCommand uses SelectedSchedule anyway */,
                    args =>
                        (args.Clicks == 2) &&
                        (args.Button == MouseButtons.Left) &&
                        (ViewModel.CanEdit(null /* internally used SelectedSchedule here */)));

            fluent.WithEvent<EventArgs>(this, "Load").EventToCommand(vm => vm.OnLoaded());
            fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.RegionsLookup.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));
        }

        private ScheduleCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<ScheduleCollectionViewModel>(); }
        }

        #endregion

        #region Filter handlers

        private void filterEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == ButtonPredefines.Clear)
            {
                var baseEdit = sender as BaseEdit;
                Debug.Assert(baseEdit != null);
                baseEdit.EditValue = null;
            }
        }

        private void filterLookupRegion_EditValueChanged(object sender, EventArgs e)
        {
            // INFO: MVVM-binding to EditValue of control hosted in ribbon does not supported.
            var lookup = sender as LookUpEdit;
            Debug.Assert(lookup != null);

            if (ReferenceEquals(lookup.EditValue, null))
                ViewModel.RegionId = null;
            else
                ViewModel.RegionId = (int) lookup.EditValue;
        }

        private void filterComboExamType_EditValueChanged(object sender, EventArgs e)
        {
            // INFO: MVVM-binding to EditValue of control hosted in ribbon does not supported.
            var combo = sender as ImageComboBoxEdit;
            Debug.Assert(combo != null);

            if (ReferenceEquals(combo.EditValue, null))
                ViewModel.ExamType = null;
            else
                ViewModel.ExamType = (Model.ExamType) (int) combo.EditValue;
        }

        private void filterComboPeriodType_EditValueChanged(object sender, EventArgs e)
        {
            // INFO: MVVM-binding to EditValue of control hosted in ribbon does not supported.
            var combo = sender as ImageComboBoxEdit;
            Debug.Assert(combo != null);
            if (ReferenceEquals(combo.EditValue, null))
                ViewModel.PeriodType = null;
            else
                ViewModel.PeriodType = (Model.PeriodType) (int) combo.EditValue;
        }

        #endregion

        #region Grid merge/column-custom-data

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            var scheduleInfo = e.Row as ScheduleInfo;
            if (scheduleInfo == null)
                return;
            var date = (DateTime) e.Column.Tag;

            if (e.IsGetData)
            {

                var station =
                    ViewModel.FilteredStationInfos.Where(s => Equals(s.ExamDate, date) &&
                                                              s.ScheduleId == scheduleInfo.ScheduleId)
                        .Skip(scheduleInfo.MasterRowId)
                        .Take(1)
                        .FirstOrDefault();

                e.Value = station?.StationDisplayText;
            }
        }

        private void gridView1_CellMerge(object sender, DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs e)
        {
            if (e.Column.UnboundType == UnboundColumnType.Bound)
            {
                var row1 = (ScheduleInfo)gridView1.GetRow(e.RowHandle1);
                var row2 = (ScheduleInfo)gridView1.GetRow(e.RowHandle2);
                if (row1.ScheduleId == row2.ScheduleId && Equals(e.CellValue1, e.CellValue2))
                {
                    e.Merge = true;
                }
                else
                {
                    e.Merge = false;
                }
            }
            e.Handled = true;
        }

        #endregion
    }
}
