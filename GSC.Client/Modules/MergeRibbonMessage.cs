﻿using DevExpress.XtraBars.Ribbon;

namespace GSC.Client.Modules
{
    internal class MergeRibbonMessage
    {
        public RibbonControl Ribbon { get; set; }
    }
}
