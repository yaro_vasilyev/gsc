﻿using System;
using JetBrains.Annotations;
using System.Linq;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.Contracts
{
    public partial class Rep104ContractReestr : GscReport
    {
        public Rep104ContractReestr()
        {
            InitializeComponent();
        }
        [UsedImplicitly]
        public Rep104ContractReestr(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class DataRow
        {
            public int numpp { get; set; }
            public string number { get; set; }
            public string contractDate { get; set; }
            public string fullname { get; set; }
            public string period { get; set; }
            public int contract_type { get; set; }
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            var currProject = ViewModel.CurrentProject.Id;

            //todo перенести в БД или в LINQ
            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<DataRow>(@"
            SELECT row_number() over() as numpp,
                   c.number,
                   to_char(ci.contract_date, 'DD.MM.YYYY') as contractDate,
                   concat_ws(' ', exp.surname, exp.name, exp.patronymic) as fullname,
                   concat_ws('-', to_char(c.date_from, 'DD.MM.YYYY'), to_char(c.date_to,
                     'DD.MM.YYYY')) as period,
                   c.contract_type
            FROM gsc.v_contract c
                 JOIN gsc.v_contract_info ci ON ci.number = c.number
                 JOIN gsc.v_schedule sh ON sh.id = c.schedule_id
                 JOIN gsc.v_expert exp ON exp.id = sh.expert_id
            WHERE exp.period_id = :p0
            ORDER BY ci.contract_date", currProject).ToList();
            bindingSource1.DataSource = data;
        }

    }
}
