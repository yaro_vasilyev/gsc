﻿using System;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Data;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports
{
    public class GscReport : XtraReport, IParameterSupplier
    {
        protected TopMarginBand topMarginBand1;
        protected DetailBand detailBand1;
        private XRControlStyle xrReportTitle;
        private XRControlStyle xrHeaderL;
        private XRControlStyle xrBodyL;
        private XRControlStyle xrBodyC;
        private XRControlStyle xrSummaryTitleR;
        private XRControlStyle xrHeaderC;
        protected BottomMarginBand bottomMarginBand1;

        [UsedImplicitly]
        public GscReport()
        {
            InitializeComponent();
        }

        public GscReport([NotNull] ReportsViewModel reportsViewModel)
        {
            if (reportsViewModel == null)
                throw new ArgumentNullException(nameof(reportsViewModel));

            InitializeComponent();
            ViewModel = reportsViewModel;
        }

        protected ReportsViewModel ViewModel { get; }

        ICollection<Parameter> IParameterSupplier.GetParameters()
        {
            ModifyParameters(Parameters);
            return Parameters;
        }

        protected virtual void ModifyParameters(ICollection<Parameter> parameters)
        {
        }


        protected virtual CheckEdit CreateBooleanCheckEditForParameter([NotNull] ParameterInfo parameter,
                                                                       string displayText,
                                                                       Action<CheckEdit> checkedChangedAction)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var editor = new CheckEdit();
            editor.Properties.ValueUnchecked = false;
            editor.Properties.ValueChecked = true;
            editor.Properties.AllowGrayed = false;
            editor.Text = displayText;

            editor.CheckedChanged += (sender, args) =>
            {
                var check = (CheckEdit)sender;
                checkedChangedAction?.Invoke(check);
            };

            parameter.Editor = editor;

            parameter.Parameter.Description = " "; // clear parameter name on parameters form

            return editor;
        }

        protected DateTime? GetDateParameterValue([NotNull] Parameter parameter)
        {
            if (parameter == null)
                throw new ArgumentNullException(nameof(parameter));

            if (!(parameter.Value is DateTime))
                return null;

            if (Equals(parameter.Value, DateTime.MinValue))
                return null;

            return (DateTime)parameter.Value;
        }

        protected string GetLookupParameterDisplayText([NotNull] ParametersRequestEventArgs requestParameters, string parameterName)
        {
            if (requestParameters == null)
            {
                throw new ArgumentNullException(nameof(requestParameters));
            }

            foreach (var parameterInfo in requestParameters.ParametersInformation)
            {
                if (parameterInfo.Parameter.Name == parameterName)
                {
                    return ((LookUpEdit)parameterInfo.Editor).Properties.GetDisplayText(parameterInfo.Parameter.Value);
                }
            }

            throw new ArgumentException("Report parameter not found.");
        }



        private void InitializeComponent()
        {
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand1 = new DevExpress.XtraReports.UI.DetailBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrReportTitle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrHeaderL = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrBodyL = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrBodyC = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrSummaryTitleR = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrHeaderC = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 100F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // detailBand1
            // 
            this.detailBand1.HeightF = 100F;
            this.detailBand1.Name = "detailBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 100F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrReportTitle
            // 
            this.xrReportTitle.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrReportTitle.Name = "xrReportTitle";
            this.xrReportTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrReportTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrHeaderL
            // 
            this.xrHeaderL.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrHeaderL.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeaderL.Name = "xrHeaderL";
            this.xrHeaderL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrHeaderL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrBodyL
            // 
            this.xrBodyL.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrBodyL.Name = "xrBodyL";
            this.xrBodyL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrBodyL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrBodyC
            // 
            this.xrBodyC.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrBodyC.Name = "xrBodyC";
            this.xrBodyC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrBodyC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrSummaryTitleR
            // 
            this.xrSummaryTitleR.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrSummaryTitleR.Name = "xrSummaryTitleR";
            this.xrSummaryTitleR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrSummaryTitleR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrHeaderC
            // 
            this.xrHeaderC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrHeaderC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeaderC.Name = "xrHeaderC";
            this.xrHeaderC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrHeaderC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GscReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1});
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrReportTitle,
            this.xrHeaderL,
            this.xrBodyL,
            this.xrBodyC,
            this.xrSummaryTitleR,
            this.xrHeaderC});
            this.Version = "15.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
    }
}