﻿namespace GSC.Client.Modules.Reports.Schedule
{
    partial class Rep062Schedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRowBody = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellRowNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellRcoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRowHeader = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.styleHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleCell = new DevExpress.XtraReports.UI.XRControlStyle();
            this.dateFrom = new DevExpress.XtraReports.Parameters.Parameter();
            this.dateTo = new DevExpress.XtraReports.Parameters.Parameter();
            this.checkOfficial = new DevExpress.XtraReports.Parameters.Parameter();
            this.checkSocial = new DevExpress.XtraReports.Parameters.Parameter();
            this.calcDateFrom = new DevExpress.XtraReports.UI.CalculatedField();
            this.calcDateTo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calcNotEmptyRow = new DevExpress.XtraReports.UI.CalculatedField();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 61F;
            this.topMarginBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.topMarginBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // detailBand1
            // 
            this.detailBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.detailBand1.FormattingRules.Add(this.formattingRule1);
            this.detailBand1.HeightF = 22.91667F;
            this.detailBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.detailBand1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("RegionName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("ExpertName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.detailBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 47F;
            this.bottomMarginBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.bottomMarginBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRowBody});
            this.xrTable2.SizeF = new System.Drawing.SizeF(627F, 22.91667F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRowBody
            // 
            this.xrTableRowBody.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellRowNumber,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCellRcoi});
            this.xrTableRowBody.Dpi = 100F;
            this.xrTableRowBody.Name = "xrTableRowBody";
            this.xrTableRowBody.Weight = 1D;
            // 
            // xrTableCellRowNumber
            // 
            this.xrTableCellRowNumber.Dpi = 100F;
            this.xrTableCellRowNumber.Name = "xrTableCellRowNumber";
            this.xrTableCellRowNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTableCellRowNumber.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.xrTableCellRowNumber.StylePriority.UsePadding = false;
            this.xrTableCellRowNumber.StylePriority.UseTextAlignment = false;
            this.xrTableCellRowNumber.Text = "RN";
            this.xrTableCellRowNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCellRowNumber.Weight = 0.35880399835197768D;
            this.xrTableCellRowNumber.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCellRowNumber_BeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RegionName")});
            this.xrTableCell4.Dpi = 100F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StyleName = "styleCell";
            this.xrTableCell4.Weight = 0.95681066227194078D;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.regionCell_BeforePrint);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertName")});
            this.xrTableCell5.Dpi = 100F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StyleName = "styleCell";
            this.xrTableCell5.Weight = 0.95681066227194067D;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.expertCell_BeforePrint);
            // 
            // xrTableCellRcoi
            // 
            this.xrTableCellRcoi.Dpi = 100F;
            this.xrTableCellRcoi.Name = "xrTableCellRcoi";
            this.xrTableCellRcoi.StyleName = "styleCell";
            this.xrTableCellRcoi.Text = "xrTableCellRcoi";
            this.xrTableCellRcoi.Weight = 0.71760799670395525D;
            this.xrTableCellRcoi.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCellRcoi_BeforePrint);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 96.875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(627F, 50.08334F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "График выходов (выездов) экспертов по мониторингу ГИА (в период с [calcDateFrom] " +
    "по [calcDateTo])";
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 65.62499F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRowHeader});
            this.xrTable1.SizeF = new System.Drawing.SizeF(627F, 31.25F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRowHeader
            // 
            this.xrTableRowHeader.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRowHeader.Dpi = 100F;
            this.xrTableRowHeader.Name = "xrTableRowHeader";
            this.xrTableRowHeader.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 100F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StyleName = "styleHeader";
            this.xrTableCell7.Text = "№ п/п";
            this.xrTableCell7.Weight = 0.35880399835197779D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StyleName = "styleHeader";
            this.xrTableCell1.Text = "Субъект контроля";
            this.xrTableCell1.Weight = 0.95681066227194056D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 100F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StyleName = "styleHeader";
            this.xrTableCell2.Text = "ФИО эксперта";
            this.xrTableCell2.Weight = 0.95681066227194067D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StyleName = "styleHeader";
            this.xrTableCell3.Text = "Контроль в РЦОИ";
            this.xrTableCell3.Weight = 0.71760799670395525D;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(GSC.Client.ViewModels.Schedule.ScheduleInfo);
            // 
            // styleHeader
            // 
            this.styleHeader.Name = "styleHeader";
            this.styleHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // styleCell
            // 
            this.styleCell.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleCell.Name = "styleCell";
            this.styleCell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleCell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dateFrom
            // 
            this.dateFrom.Description = "С:";
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Type = typeof(System.DateTime);
            // 
            // dateTo
            // 
            this.dateTo.Description = "По:";
            this.dateTo.Name = "dateTo";
            this.dateTo.Type = typeof(System.DateTime);
            // 
            // checkOfficial
            // 
            this.checkOfficial.Description = "Эксперты-члены ГЭК";
            this.checkOfficial.Name = "checkOfficial";
            this.checkOfficial.Type = typeof(bool);
            this.checkOfficial.ValueInfo = "True";
            // 
            // checkSocial
            // 
            this.checkSocial.Description = "Эксперты-общественные наблюдатели";
            this.checkSocial.Name = "checkSocial";
            this.checkSocial.Type = typeof(bool);
            this.checkSocial.ValueInfo = "True";
            // 
            // calcDateFrom
            // 
            this.calcDateFrom.Expression = "GetDate([Parameters.dateFrom])";
            this.calcDateFrom.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calcDateFrom.Name = "calcDateFrom";
            this.calcDateFrom.GetValue += new DevExpress.XtraReports.UI.GetValueEventHandler(this.calcDateFrom_GetValue);
            // 
            // calcDateTo
            // 
            this.calcDateTo.Expression = "GetDate([Parameters.dateTo])";
            this.calcDateTo.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calcDateTo.Name = "calcDateTo";
            this.calcDateTo.GetValue += new DevExpress.XtraReports.UI.GetValueEventHandler(this.calcDateTo_GetValue);
            // 
            // calcNotEmptyRow
            // 
            this.calcNotEmptyRow.Name = "calcNotEmptyRow";
            this.calcNotEmptyRow.GetValue += new DevExpress.XtraReports.UI.GetValueEventHandler(this.calcNotEmptyRow_GetValue);
            // 
            // formattingRule1
            // 
            this.formattingRule1.Condition = "[calcNotEmptyRow] != True";
            // 
            // 
            // 
            this.formattingRule1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.formattingRule1.Name = "formattingRule1";
            // 
            // Rep062Schedule
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1,
            this.ReportHeader});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calcDateFrom,
            this.calcDateTo,
            this.calcNotEmptyRow});
            this.DataSource = this.bindingSource1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 61, 47);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.dateFrom,
            this.dateTo,
            this.checkOfficial,
            this.checkSocial});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.styleHeader,
            this.styleCell});
            this.Version = "16.1";
            this.Controls.SetChildIndex(this.ReportHeader, 0);
            this.Controls.SetChildIndex(this.bottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.detailBand1, 0);
            this.Controls.SetChildIndex(this.topMarginBand1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRowHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellRcoi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRowBody;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellRowNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRControlStyle styleHeader;
        private DevExpress.XtraReports.UI.XRControlStyle styleCell;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.Parameters.Parameter dateFrom;
        private DevExpress.XtraReports.Parameters.Parameter dateTo;
        private DevExpress.XtraReports.Parameters.Parameter checkOfficial;
        private DevExpress.XtraReports.Parameters.Parameter checkSocial;
        private DevExpress.XtraReports.UI.CalculatedField calcDateFrom;
        private DevExpress.XtraReports.UI.CalculatedField calcDateTo;
        private DevExpress.XtraReports.UI.CalculatedField calcNotEmptyRow;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
    }
}
