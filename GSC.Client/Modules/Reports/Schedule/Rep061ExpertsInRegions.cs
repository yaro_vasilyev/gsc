﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Schedule
{
    public partial class Rep061ExpertsInRegions : GscReport
    {
        public Rep061ExpertsInRegions()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep061ExpertsInRegions(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class ReportRow
        {
            public string RegionCode { get; set; }
            public string RegionName { get; set; }
            public decimal Official { get; set; }
            public decimal Social { get; set; }
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var projectId = ViewModel.CurrentProject.Id;

            var periodTypeParam = EnumReportParameter.NullableValue<int>(Parameters["periodType"]);

            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<ReportRow>(@"
select * 
  from gsc.r_gsc61_experts_count_in_regions(:p0::integer, :p1::integer)
    as f (RegionCode, RegionName, Official, Social)
 order by RegionName",
                projectId,
                periodTypeParam);

            bindingSource1.DataSource = data.ToList();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.PeriodType>(
                "periodType", "Этап:", EnumReportParameter.Null));
        }
    }
}
