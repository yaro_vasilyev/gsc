﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Rcoi;
using GSC.Client.ViewModels.Reports;
using GSC.Client.ViewModels.Schedule;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Schedule
{
    /// <summary>
    /// Отчет «График выходов (выездов) экспертов по мониторингу ГИА»
    /// </summary>
    public partial class Rep062Schedule : GscReport
    {
        private List<ScheduleInfo> schedules;
        private List<StationInfo> stations;
        private List<RcoiInfo> rcois;
        private List<DateTime> examDates;
        private int prevScheduleId = -1;
        private int rowNumber = 0;
        private const int DynamicColWidth = 150;

        public Rep062Schedule()
        {
            InitializeComponent();
        }

        public Rep062Schedule(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void OnParametersRequestSubmit(ParametersRequestEventArgs e)
        {
            base.OnParametersRequestSubmit(e);
            ResetReport();
        }

        private void ResetReport()
        {
            schedules?.Clear();
            stations?.Clear();
            rcois?.Clear();
            examDates?.Clear();
            prevScheduleId = -1;
            rowNumber = 0;

            RemoveTailTaggedCells<DateTime>(xrTableRowHeader);
            RemoveTailTaggedCells<DateTime>(xrTableRowBody);
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var projectId = ViewModel.CurrentProject.Id;

            ExpertKind? expertKind = null;
            if (!Equals(checkSocial.Value, checkOfficial.Value))
            {
                expertKind = Equals(checkOfficial.Value, true)
                    ? ExpertKind.Official
                    : ExpertKind.Social;
            }

            var dateFromValue = GetDateParameterValue(dateFrom);
            var dateToValue = GetDateParameterValue(dateTo);

            var x1 = ScheduleInfo.GetScheduleInfos(ViewModel.UnitOfWork.Context, projectId, null, expertKind).ToListAsync();
            var x2 = StationInfo.GetStationInfos(ViewModel.UnitOfWork.Context,
                projectId,
                dateFromValue,
                dateToValue).ToListAsync();

            var x3 = RcoiInfo.GetRegionRcoiInfos(ViewModel.UnitOfWork.Context).ToListAsync();

            Task.WaitAll(x1, x2, x3);

            ResetReport();

            schedules = x1.Result;
            stations = x2.Result;
            rcois = x3.Result;

            examDates = ViewModel.UnitOfWork.Exams.Where(_ => _.Subject.ProjectId == projectId &&
                                                              (dateFromValue == null || _.ExamDate >= dateFromValue) &&
                                                              (dateToValue == null || _.ExamDate <= dateToValue))
                .Select(_ => _.ExamDate)
                .Distinct()
                .OrderBy(_ => _)
                .ToList();

            //examDates = stations.Select(x => x.ExamDate.Date).Distinct().ToList();
            //examDates.Sort();

            try
            {
                xrTable1.BeginInit();
                xrTable2.BeginInit();

                foreach (var examDate in examDates)
                {
                    var cellHeader = new XRTableCell
                    {
                        Tag = examDate,
                        Width = DynamicColWidth,
                        Text = examDate.ToString("d"),
                        StyleName = "styleHeader",
                    };

                    var cellBody = new XRTableCell
                    {
                        Tag = examDate,
                        Width = DynamicColWidth,
                        StyleName = "styleCell"
                    };

                    cellBody.BeforePrint += CellBodyOnBeforePrint;

                    xrTableRowHeader.Cells.Add(cellHeader);
                    xrTableRowBody.Cells.Add(cellBody);

                    xrTableRowHeader.Width += cellHeader.Width;
                    xrTableRowBody.Width += cellBody.Width;

                    xrTable1.Width += cellHeader.Width;
                    xrTable2.Width += cellBody.Width;
                }

            }
            finally
            {
                xrTable1.EndInit();
                xrTable2.EndInit();
            }

            bindingSource1.DataSource = schedules;
        }

        private void RemoveTailTaggedCells<T>([NotNull] XRTableRow tableRow)
        {
            if (tableRow == null)
                throw new ArgumentNullException(nameof(tableRow));

            while (tableRow.Cells.Count > 0 &&
                tableRow.Cells[tableRow.Cells.Count - 1].Tag is T)
            {
                var w = tableRow.Cells[tableRow.Cells.Count - 1].Width;
                tableRow.Cells.RemoveAt(tableRow.Cells.Count - 1);
                tableRow.Width -= w;
                tableRow.Table.Width -= w;
            }
        }

        private void CellBodyOnBeforePrint(object sender, PrintEventArgs printEventArgs)
        {
            var control = (XRControl)sender;
            var examDate = (DateTime)control.Tag;
            var scheduleInfo = (ScheduleInfo)GetCurrentRow();
            if (scheduleInfo == null)
                return;

            var station = stations.Where(s => Equals(s.ExamDate.Date, examDate) &&
                                              s.ScheduleId == scheduleInfo.ScheduleId)
                .Skip(scheduleInfo.MasterRowId)
                .FirstOrDefault();
            control.Text = station?.StationDisplayText;
        }

        private int ScheduleId
        {
            set
            {
                if (prevScheduleId != value)
                {
                    ++rowNumber;
                    prevScheduleId = value;
                }
            }
        }

        private void xrTableCellRowNumber_BeforePrint(object sender, PrintEventArgs e)
        {
            var scheduleInfo = GetCurrentRow() as ScheduleInfo;
            if (scheduleInfo == null)
                return;

            ScheduleId = scheduleInfo.ScheduleId;

            var control = sender as XRControl;
            control.Text = rowNumber.ToString();
        }

        private void xrTableCellRcoi_BeforePrint(object sender, PrintEventArgs e)
        {
            var scheduleInfo = GetCurrentRow() as ScheduleInfo;
            if (scheduleInfo == null)
                return;

            var rcoi = rcois.FirstOrDefault(r => r.RegionId == scheduleInfo.RegionId);

            var control = sender as XRControl;
            control.Text = rcoi?.Rcoi;
        }

        private void calcDateFrom_GetValue(object sender, GetValueEventArgs e)
        {
            e.Value = GetDateParameterValue(dateFrom)?.ToString("d") ?? "<Не задано>";
        }

        private void calcDateTo_GetValue(object sender, GetValueEventArgs e)
        {
            e.Value = GetDateParameterValue(dateTo)?.ToString("d") ?? "<Не задано>";
        }

        private void calcNotEmptyRow_GetValue(object sender, GetValueEventArgs e)
        {
            if (Equals(dateFrom.Value, DateTime.MinValue) && Equals(dateTo.Value, DateTime.MinValue))
            {
                // no filter set
                e.Value = true;
                return;
            }

            var scheduleInfo = GetCurrentRow() as ScheduleInfo;
            if (scheduleInfo == null)
            {
                // curious
                return;
            }

            var stationExistInRange = stations.Exists(s => s.ExamDate.Date >= (DateTime)dateFrom.Value &&
                                                           s.ExamDate.Date <= (DateTime)dateTo.Value &&
                                                           s.ScheduleId == scheduleInfo.ScheduleId);
            e.Value = stationExistInRange;
        }

        private void regionCell_BeforePrint(object sender, PrintEventArgs e)
        {
        }

        private void expertCell_BeforePrint(object sender, PrintEventArgs e)
        {
        }
    }
}
