﻿using DevExpress.XtraBars.Navigation;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.Schedule
{
    public partial class ScheduleReportsView : BaseReportsView
    {
        public ScheduleReportsView()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ScheduleReportsViewModel>();
            fluent.SetItemsSourceBinding(
                accordionControl1,
                ctrl => ctrl.Elements,
                vm => vm.Categories,
                (elemCat, cat) => Equals(elemCat.Tag, cat),
                cat => new AccordionControlElement(ElementStyle.Group),
                null,
                (elemCat, cat) => {
                    elemCat.Text = cat.DisplayName;
                    elemCat.Tag = cat;
                    fluent.SetItemsSourceBinding(
                        elemCat,
                        e => e.Elements,
                        vm => vm.Reports,
                        (e, vm) => Equals(elemCat.Tag, cat) && Equals(e.Tag, vm),
                        vm => new AccordionControlElement(ElementStyle.Item),
                        null,
                        (e, vm) => {
                            e.Tag = vm;
                            e.Text = vm.DisplayName;
                        });
                });
        }
    }
}
