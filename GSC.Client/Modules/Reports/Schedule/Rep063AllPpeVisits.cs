﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Schedule
{
    public partial class Rep063AllPpeVisits : GscReport
    {
        private const int DynamicColWidth = 120;
        private const string ParDateName = "parDate";
        private int? currentProject;

        public Rep063AllPpeVisits()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep063AllPpeVisits(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            base.ModifyParameters(parameters);

            var lookUpSettings = new StaticListLookUpSettings();
            lookUpSettings.LookUpValues.AddRange(
                ViewModel.UnitOfWork.Exams.Select(e => e.ExamDate)
                    .Distinct()
                    .OrderBy(d => d)
                    .ToList()
                    .Select(d => new LookUpValue(d, $"{d:d}")));
            var parDate = new Parameter
            {
                Name = ParDateName,
                Description = @"Дата",
                Type = typeof(DateTime),
                LookUpSettings = lookUpSettings
            };
            parameters.Add(parDate);
        }

        /*
         № п/п	Субъект РФ	Код ППЭ	Наименование ППЭ	Адрес ППЭ	Дата экзамена, предмет 1	…	Дата экзамена, предмет n	ФИО экспертов – членов ГЭК	ФИО экспертов – общественных наблюдателей
        */

        private void Rep063AllPpeVisits_DataSourceDemanded(object sender, EventArgs ea)
        {
            var unitOfWork = ViewModel.UnitOfWork;
            var scheduleDetails = unitOfWork.ScheduleDetails;
            var exams = unitOfWork.Exams;
            var date = (DateTime) Parameters[ParDateName].Value;

            var project = ViewModel.CurrentProject.Id;

            var query = scheduleDetails
                .Where(sd => sd.Schedule.Expert.ProjectId == project && sd.ExamDate == date)
                .GroupBy(sd => sd.Station)
                .Select(g => new DataRow
                {
                    Region = g.Key.Region.Name,
                    StationId = g.Key.Id,
                    Code = g.Key.Code,
                    Name = g.Key.Name,
                    Address = g.Key.Address,
                    ExamDates = g.Select(sd => sd.ExamDate).Distinct(),
                    Experts = g.Select(sd => sd.Schedule.Expert).Distinct()
                })
                .OrderBy(row => row.Region)
                .ThenBy(row => row.Code)
                .ToList();

            // дата теперь одна - она выбирается в параметре
            /*var visitingDates = query // список дат для формирования динамических колонок
                .SelectMany(row => row.ExamDates)
                .Distinct()
                .ToList();*/

            var visitingExams = exams // для динамических колонок
                .Where(e => e.Subject.ProjectId == project && /*visitingDates.Contains(ex.ExamDate)*/ e.ExamDate == date)
                .Select(e => new
                {
                    e.Id,
                    e.ExamDate,
                    e.Subject.Name
                })
                .OrderBy(r => r.ExamDate)
                .ThenBy(r => r.Name)
                .ToList();

            #region Формирование динамических колонок и ячеек

            var insHeadCellIndex = tcOffExpertsHead.Index;
            var insBodyCellIndex = tcOffExpertsBody.Index;

            // save right cell and its width
            var headRightCell = tcOffExpertsHead;
            var headRightCellWidth = headRightCell.Width;
            var bodyRightCell = tcOffExpertsBody;
            var bodyRightCellWidth = headRightCell.Width;

            tHeader.BeginInit();
            try
            {
                tBody.BeginInit();
                try
                {
                    foreach (var exam in visitingExams)
                    {
                        // create dynamic head cell
                        var headCell = new XRTableCell
                        {
                            Text = $@"{exam.ExamDate:d},
{exam.Name}",
                            Tag = exam.Id,
                            Multiline = true,
                            CanGrow = true,
                            StyleName = FieldCaption.Name
                        };

                        // insert dynamic head cell
                        trHeader.InsertCell(headCell, insHeadCellIndex++);
                        headCell.Width = DynamicColWidth;

                        // wide head row and table be added column
                        trHeader.Width += headCell.Width;
                        tHeader.Width += headCell.Width;

                        // restore right head cell width
                        headRightCell.Width = headRightCellWidth;

                        // create dynamic body cell
                        var bodyCell = new XRTableCell
                        {
                            Tag = exam.Id,
                            Multiline = true,
                            CanGrow = true,
                            StyleName = DataField.Name
                        };
                        bodyCell.BeforePrint += BodyCellOnBeforePrint;

                        // insert dynamic body cell
                        trBody.InsertCell(bodyCell, insBodyCellIndex++);
                        bodyCell.Width = DynamicColWidth;

                        // wide body row and table be added column
                        trBody.Width += bodyCell.Width;
                        tBody.Width += bodyCell.Width;

                        // restore right body cell width
                        bodyRightCell.Width = bodyRightCellWidth;
                    }
                }
                finally
                {
                    tBody.EndInit();
                }
            }
            finally
            {
                tHeader.EndInit();
            }

            #endregion

            reportBindingSource.DataSource = query;
        }

        private void BodyCellOnBeforePrint(object sender, PrintEventArgs printEventArgs)
        {
            var cell = sender as XRTableCell;
            var examId = cell?.Tag as int?;
            var currRow = GetCurrentRow() as DataRow;
            var stationId = currRow?.StationId;

            int? countPart = null;
            if (stationId != null)
            {
                var project = ViewModel.CurrentProject.Id;

                countPart = ViewModel.UnitOfWork.Execs
                    .Where(e => e.Exam.Subject.ProjectId == project && e.ExamId == examId && e.StationId == stationId)
                    .Select(e => e.CountPart)
                    .FirstOrDefault();
            }

            if (cell != null)
            {
                cell.Text = $@"{countPart}";
            }
        }

        public class DataRow
        {
            private static readonly Func<string, Expert, string> aggNameFunc =
                (resStr, expert) => $"{resStr}{(string.Empty.Equals(resStr) ? string.Empty : ", ")}{expert.FullName}";

            public string Region { get; set; }
            public int Code { get; set; }
            public string Name { [UsedImplicitly] get; set; }
            public string Address { [UsedImplicitly] get; set; }

            [UsedImplicitly]
            public string SocialExperts
                => Experts.Where(e => e.Kind == ExpertKind.Social).Aggregate(string.Empty, aggNameFunc);

            [UsedImplicitly]
            public string OfficialExperts
                => Experts.Where(e => e.Kind == ExpertKind.Official).Aggregate(string.Empty, aggNameFunc);

            internal int StationId { get; set; }
            internal IEnumerable<Expert> Experts { private get; set; }
            public IEnumerable<DateTime> ExamDates { get; set; }
        }

        private void Rep063AllPpeVisits_ParametersRequestSubmit(object sender, ParametersRequestEventArgs e)
        {
            // remove dynamic cells created in previous submit

            tHeader.BeginInit();
            try
            {
                IEnumerable<XRTableCell> headCells = trHeader.Cells;
                foreach (var cell in headCells.Where(c => c.Tag is int).ToList())
                {
                    var width = cell.Width;
                    trHeader.Cells.Remove(cell);
                    trHeader.Width -= width;
                    tHeader.Width -= width;
                }
            }
            finally
            {
                tHeader.EndInit();
            }

            tBody.BeginInit();
            try
            {
                IEnumerable<XRTableCell> bodyCells = trBody.Cells;
                foreach (var cell in bodyCells.Where(c => c.Tag is int).ToList())
                {
                    var width = cell.Width;
                    trBody.Cells.Remove(cell);
                    trBody.Width -= width;
                    tBody.Width -= width;
                }
            }
            finally
            {
                tBody.EndInit();
            }
        }
    }
}