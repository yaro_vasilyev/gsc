﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Подробные результаты мониторинга ГИА»
    /// </summary>
    public partial class Rep095MonitorResultsDetailed : GscReport
    {
        public Rep095MonitorResultsDetailed()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep095MonitorResultsDetailed(ReportsViewModel reportsViewModel)
            : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.PeriodType>(
                "periodType", "Этап:", EnumReportParameter.Null));

            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Тип экзамена:", EnumReportParameter.Null));

            parameters.Add(new Parameter
            {
                Name = "useDate",
                Type = typeof(System.Boolean),
                Value = true
            });

            parameters.Add(new Parameter
            {
                Name = "date",
                Description = " ",
                Type = typeof(System.DateTime),
                Value = DateTime.Today,
            });
        }

        protected override void OnParametersRequestBeforeShow(ParametersRequestEventArgs e)
        {
            var useDateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "useDate");
            var dateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "date");

            var dateParameterEditor = dateParameter.GetEditor(true) as DateEdit;
            Debug.Assert(dateParameterEditor != null);
            dateParameterEditor.Properties.AllowNullInput = DefaultBoolean.True;
            dateParameterEditor.Properties.NullDate = DateTime.MinValue;
            dateParameterEditor.Properties.NullText = "Не задано";

            CreateBooleanCheckEditForParameter(useDateParameter, "Только по дате",
                check =>
                {
                    if (!check.Checked)
                    {
                        dateParameter.Parameter.Value = null;
                        dateParameterEditor.EditValue = null;
                    }
//                    dateParameterEditor.Enabled = check.Checked;
                    dateParameterEditor.Visible = check.Checked;
                });
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var periodTypeParam = Parameters["periodType"];
            var examTypeParam = Parameters["examType"];
            var useDateParam = Parameters["useDate"];
            var dateParam = Parameters["date"];

            var useDate = (bool) useDateParam.Value;
            var date = (DateTime) dateParam.Value;

            var project = ViewModel.CurrentProject;

            var data = DataRow.GetData(ViewModel.UnitOfWork.Context,
                project.Id,
                EnumReportParameter.NullableValue<Model.PeriodType>(periodTypeParam),
                EnumReportParameter.NullableValue<Model.ExamType>(examTypeParam),
                useDate ? date : (DateTime?) null).ToList();

            bindingSource1.DataSource = data;
        }

        #region DataRow class

        public class DataRow
        {
            public DateTime Date { get; set; }

            public int RegionCode { get; set; }

            public string RegionName { get; set; }

            public Model.ControlZone ControlZone { get; set; }

            public string SubjectName { get; set; }

            public string ExpertFullName { get; set; }

            public Model.ExpertKind ExpertKind { get; set; }

            public string StationCode { get; set; }

            public string ViolationCode { get; set; }

            public string ViolationName { get; set; }

            public string ViolatorCode { get; set; }

            public string ViolatorName { get; set; }

            public string Note { get; set; }

            public static DbRawSqlQuery<DataRow> GetData([NotNull] DbContext context,
                                                         int projectId,
                                                         Model.PeriodType? periodType,
                                                         Model.ExamType? examType,
                                                         DateTime? date)
            {
                if (context == null)
                    throw new ArgumentNullException(nameof(context));

                //todo перенести в БД или в LINQ
                return context.Database.SqlQuery<DataRow>(@"
select * 
  from gsc.r_gsc095_monitor_results_detailed(
            p_project_id        => :p0::integer, 
            p_period_type       => :p1::integer, 
            p_exam_type         => :p2::integer, 
            p_date              => :p3::date) 
    as f (  Date, 
            RegionCode, 
            RegionName, 
            ControlZone, 
            SubjectName, 
            ExpertFullName, 
            ExpertKind, 
            StationCode, 
            ViolationCode, 
            ViolationName, 
            ViolatorCode, 
            ViolatorName, 
            Note
        )
 order by Date, RegionName, ExpertFullName",
       projectId, periodType, examType, date);
            }
        }

        #endregion

        private void calcExpertKind_GetValue(object sender, GetValueEventArgs e)
        {
            var kind = e.GetColumnValue(nameof(DataRow.ExpertKind));
            if (ReferenceEquals(kind, null) || Convert.IsDBNull(kind))
            {
                e.Value = string.Empty;
                return;
            }

            switch ((Model.ExpertKind)kind)
            {
                case Model.ExpertKind.Official:
                    e.Value = "Член ГЭК";
                    break;
                case Model.ExpertKind.Social:
                    e.Value = "Общественный наблюдатель";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void caclControlZone_GetValue(object sender, GetValueEventArgs e)
        {
            var zone = e.GetColumnValue(nameof(DataRow.ControlZone));
            if (ReferenceEquals(zone, null) || Convert.IsDBNull(zone))
            {
                e.Value = string.Empty;
                return;
            }
            switch ((Model.ControlZone)zone)
            {
                case Model.ControlZone.Green:
                    e.Value = "Зеленая";
                    break;
                case Model.ControlZone.Yellow:
                    e.Value = "Желтая";
                    break;
                case Model.ControlZone.Red:
                    e.Value = "Красная";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
