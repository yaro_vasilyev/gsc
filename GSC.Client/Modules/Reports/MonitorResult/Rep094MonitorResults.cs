﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Результаты мониторинга ГИА»
    /// </summary>
    public partial class Rep094MonitorResults : GscReport
    {
        public Rep094MonitorResults()
        {
            InitializeComponent();
        }

        public Rep094MonitorResults(ReportsViewModel reportsViewModel)
            : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.PeriodType>(
                "periodType", "Этап:", EnumReportParameter.Null));
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Тип экзамена:", EnumReportParameter.Null));

            parameters.Add(new Parameter
            {
                Name = "useDate",
                Description = " ",
                Type = typeof(bool),
                Value = true
            });

            parameters.Add(new Parameter
            {
                Name = "date",
                Description = " ",
                Type = typeof(System.DateTime),
                Value = DateTime.Today,
            });
        }

        protected override void OnParametersRequestBeforeShow(ParametersRequestEventArgs e)
        {
            var useDateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "useDate");
            var dateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "date");

            var dateParameterEditor = dateParameter.GetEditor(true) as DateEdit;
            Debug.Assert(dateParameterEditor != null);
            dateParameterEditor.Properties.AllowNullInput = DefaultBoolean.True;
            dateParameterEditor.Properties.NullDate = DateTime.MinValue;
            dateParameterEditor.Properties.NullText = "Не задано";

            CreateBooleanCheckEditForParameter(useDateParameter, "Только по дате", check =>
            {
                if (!check.Checked)
                {
                    dateParameter.Parameter.Value = null;
                    dateParameterEditor.EditValue = null;
                }
//                    dateParameterEditor.Enabled = check.Checked;
                dateParameterEditor.Visible = check.Checked;
            });
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var periodTypeParam = Parameters["periodType"];
            var examTypeParam = Parameters["examType"];
            var useDateParam = Parameters["useDate"];
            var dateParam = Parameters["date"];

            var useDate = (bool)useDateParam.Value;
            var date = (DateTime)dateParam.Value;

            var data = DataRow.GetData(ViewModel.UnitOfWork.Context,
                ViewModel.CurrentProject.Id,
                EnumReportParameter.NullableValue<Model.PeriodType>(periodTypeParam),
                EnumReportParameter.NullableValue<Model.ExamType>(examTypeParam),
                useDate ? date : ((DateTime?) null)).ToList();

            bindingSource1.DataSource = data;
        }

        #region DataRow class

        public class DataRow
        {
            public DateTime Date { get; set; }

            public int RegionCode { get; set; }

            public string RegionName { get; set; }

            public string ExpertFullName { get; set; }

            public Model.ExpertKind ExpertKind { get; set; }

            public string StationCode { get; set; }

            public string ViolationPpe { get; set; }

            public string ViolationOther { get; set; }

            public string Note { get; set; }

            public static DbRawSqlQuery<DataRow> GetData([NotNull] DbContext context,
                                                         int projectId,
                                                         Model.PeriodType? periodType,
                                                         Model.ExamType? examType,
                                                         DateTime? date)
            {
                if (context == null)
                    throw new ArgumentNullException(nameof(context));

                return context.Database.SqlQuery<DataRow>(@"
select Date, RegionCode, RegionName, ExpertFullName, ExpertKind, StationCode, ViolationPpe, ViolationOther, Note 
  from gsc.r_gsc094_monitor_results (:p0, :p1::integer, :p2::integer, :p3::date) 
    as x (Date, RegionCode, RegionName, ExpertFullName, ExpertKind, StationCode, ViolationPpe, ViolationOther, Note)",
                    projectId, periodType, examType, date);
            }
        }

        #endregion

        private void calcExpertKind_GetValue(object sender, GetValueEventArgs e)
        {
            var kind = e.GetColumnValue(nameof(DataRow.ExpertKind));
            if (ReferenceEquals(kind, null) || Convert.IsDBNull(kind))
            {
                e.Value = string.Empty;
                return;
            }

            switch ((Model.ExpertKind)kind)
            {
                case Model.ExpertKind.Official:
                    e.Value = "Член ГЭК";
                    break;
                case Model.ExpertKind.Social:
                    e.Value = "Общественный наблюдатель";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
