﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    public partial class Rep105ScheduleRegions : GscReport
    {
        public Rep105ScheduleRegions()
        {
            InitializeComponent();
        }

        public Rep105ScheduleRegions(ReportsViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.PeriodType>("periodType",
                "Этап:",
                EnumReportParameter.Null));
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var project = ViewModel.CurrentProject;

            var periodType = EnumReportParameter.NullableValue<Model.PeriodType>(Parameters["periodType"]);

            var regions = ViewModel.UnitOfWork.Schedules
                .Where(x => x.Expert.ProjectId == project.Id &&
                            (periodType == null || x.PeriodType == periodType.Value))
                .Select(x => x.Region)
                .Distinct()
                .OrderBy(x => x.Name);

            bindingSource1.DataSource = regions.ToList();
        }
    }
}
