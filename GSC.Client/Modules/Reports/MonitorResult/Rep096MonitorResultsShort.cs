﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Краткие результаты мониторинга»
    /// </summary>
    public partial class Rep096MonitorResultsShort : GscReport
    {
        #region ctor
        public Rep096MonitorResultsShort()
        {
            InitializeComponent();
        }

        public Rep096MonitorResultsShort(ReportsViewModel reportsViewModel)
            : base(reportsViewModel)
        {
            InitializeComponent();
        }
        #endregion

        public class DataRow
        {
            public string RegionName { get; set; }

            public DateTime? Date { get; set; }

            public string Subjects { get; set; }

            public string StationCode { get; set; }

            public string ViolationCode { get; set; }

            public string ViolatorCode { get; set; }

            public string Note { get; set; }
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            base.ModifyParameters(parameters);

            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                    "examType", "Форма ГИА: ", EnumReportParameter.Null));
            parameters.Add(new Parameter
            {
                Name = "useDate",
                Type = typeof (bool),
                Value = true
            });
            parameters.Add(new Parameter
            {
                Name = "date",
                Description = " ",
                Type = typeof (DateTime),
                Value = null
            });

        }

        protected override void OnParametersRequestBeforeShow(ParametersRequestEventArgs e)
        {
            var useDateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "useDate");
            var dateParameter = e.ParametersInformation.First(x => x.Parameter.Name == "date");

            var dateParameterEditor = dateParameter.GetEditor(true) as DateEdit;
            Debug.Assert(dateParameterEditor != null);
            dateParameterEditor.Properties.AllowNullInput = DefaultBoolean.True;
            dateParameterEditor.Properties.NullDate = DateTime.MinValue;
            dateParameterEditor.Properties.NullText = "Не задано";

            CreateBooleanCheckEditForParameter(useDateParameter, "Только по дате",
                check =>
                {
                    if (!check.Checked)
                    {
                        dateParameter.Parameter.Value = null;
                        dateParameterEditor.EditValue = null;
                    }
//                    dateParameterEditor.Enabled = check.Checked;
                    dateParameterEditor.Visible = check.Checked;
                });
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var projectId = ViewModel.CurrentProject.Id;

            var examType = EnumReportParameter.NullableValue<int>(Parameters["examType"]);
            var date = GetDateParameterValue(Parameters["date"]);

            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<DataRow>(@"
select * 
  from gsc.r_gsc096_monitor_results_short (:p0::integer, :p1::integer, :p2::date)
    as f (RegionName, Date, Subjects, StationCode, ViolationCode, ViolatorCode, Note)
 order by RegionName, Date", projectId, examType, date);

            bindingSource1.DataSource = data.ToList();
        }
    }
}
