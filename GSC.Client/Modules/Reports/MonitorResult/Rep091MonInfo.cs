﻿using System;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    public partial class Rep091MonInfo : GscReport
    {
        public Rep091MonInfo()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep091MonInfo(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        private class DataRow
        {
            public string Region { get; set; }
            public int ExpertsCount { get; set; }
            public int OfficialExpertsCount { get; set; }
            public int SocialExpertsCount { get; set; }
            public int PpeCount { get; set; }

            /*
Субъект контроля
Общее количество экспертов
Количество экспертов-членов ГЭК
Количество экспертов-общественных наблюдателей
Количество ППЭ, в которые направлены эксперты    
         */
        }

        private void Rep091MonInfo_DataSourceDemanded(object sender, EventArgs ea)
        {
            var dateFrom = (DateTime) pDateFrom.Value;
            var dateTo = (DateTime) pDateTo.Value;

            var projId = ViewModel.CurrentProject.Id;
            var regions = ViewModel.UnitOfWork.Regions;

            var query = regions.Select(r => new
                {
                    RegionName = r.Name,
                    Schedules =
                    r.Schedules.Where(
                        s =>
                            s.Expert.ProjectId == projId &&
                            s.ScheduleDetails.Any(sd => sd.ExamDate >= dateFrom && sd.ExamDate <= dateTo))
                }).Select(x => new
                {
                    x.RegionName,
                    ExpertsDistinct = x.Schedules.Select(s => s.Expert).Distinct(),
                    PPEsDistinctCount =
                    x.Schedules.SelectMany(s => s.ScheduleDetails.Where(sd => sd.ExamDate >= dateFrom &&
                                                                              sd.ExamDate <= dateTo))
                        .Select(sd => sd.Station)
                        .Distinct().Count()
                })
                .Select(x => new DataRow
                {
                    Region = x.RegionName,
                    ExpertsCount = x.ExpertsDistinct.Count(),
                    OfficialExpertsCount = x.ExpertsDistinct.Count(e => e.Kind == ExpertKind.Official),
                    SocialExpertsCount = x.ExpertsDistinct.Count(e => e.Kind == ExpertKind.Social),
                    PpeCount = x.PPEsDistinctCount
                })
                .OrderBy(dr => dr.Region).Dump();

            bindingSource1.DataSource = query.ToList();
        }

        private void Rep091MonInfo_ParametersRequestBeforeShow(object sender,
            ParametersRequestEventArgs ea)
        {
            pDateFrom.Value = ViewModel.UnitOfWork.Exams.Min(e => e.ExamDate).Date;
            pDateTo.Value = ViewModel.UnitOfWork.Exams.Max(e => e.ExamDate).Date;
        }
    }
}