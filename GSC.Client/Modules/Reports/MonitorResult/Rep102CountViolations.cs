﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Количество нарушений по видам»
    /// </summary>
    public partial class Rep102CountViolations : GscReport
    {
        public Rep102CountViolations()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep102CountViolations(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class Rep102CountViolationsReportRow
        {
            public int ViolationCode { get; set; }
            public string ViolationName { get; set; }
            public int Count { get; set; }
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Форма ГИА:", EnumReportParameter.Null));
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            var projectId = ViewModel.CurrentProject.Id;

            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<Rep102CountViolationsReportRow>(@"
select * 
  from gsc.r_gsc102_violations (
            p_project_id        => :p0,
            p_exam_type         => :p1,
            p_station_code      => null,
            p_exam_date         => null) 
    as f (ViolationCode, ViolationName, Count)
 order by ViolationCode", 
                projectId, 
                EnumReportParameter.NullableValue<int>(Parameters["examType"]));

            bindingSource1.DataSource = data.ToList();
        }
    }
}
