﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Количество нарушений по категориям нарушителей»
    /// </summary>
    public partial class Rep103CountViolators : GscReport
    {
        public Rep103CountViolators()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep103CountViolators(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class Rep103CountViolatorsReportRow
        {
            public int ViolatorCode { get; set; }
            public string ViolatorName { get; set; }
            public int Count { get; set; }
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Тип экзамена:", EnumReportParameter.Null));
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            var projectId = ViewModel.CurrentProject.Id;

            //todo перенести в БД или в LINQ
            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<Rep103CountViolatorsReportRow>(@"
select f.* 
  from gsc.r_gsc103_violators (:p0::integer, :p1::integer) as f(ViolatorCode, ViolatorName, Count)
 order by ViolatorCode",
                projectId,
                EnumReportParameter.NullableValue<int>(Parameters["examType"]));

            bindingSource1.DataSource = data.ToList();
        }

    }
}
