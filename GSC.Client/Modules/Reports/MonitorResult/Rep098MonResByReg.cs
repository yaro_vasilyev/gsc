﻿using System;
using System.Linq;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    public partial class Rep098MonResByReg : GscReport
    {
        public Rep098MonResByReg()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep098MonResByReg(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class DataRow
        {
            /*
Субъект Российской Федерации
Количество экспертов
Количество нарушений
             */

            public string Region { get; set; }
            public int ExpertsCount { [UsedImplicitly] get; set; }
            public int ViolationsCount { [UsedImplicitly] get; set; }
        }

        private void Rep098MonResByReg_DataSourceDemanded(object sender, EventArgs e)
        {
            var controlZone = pControlZone.Value as short?;
            var examType = pExamType.Value as short?;

            var currProject = ViewModel.CurrentProject.Id;
            var regions = ViewModel.UnitOfWork.Regions;

            var query = regions.Where(r => controlZone == 0 || controlZone == (short?) r.Zone)
                .Select(r => new DataRow
                {
                    Region = r.Name,
                    ExpertsCount =
                        r.Schedules.Where(schedule => schedule.Expert.ProjectId == currProject)
                            .Select(schedule => schedule.ExpertId)
                            .Distinct() // отбираем уникальное кол-во экспертов в расписании региона
                            .Count(),
                    ViolationsCount =
                        r.Schedules
                            .Where(s => s.Expert.ProjectId == currProject)
                            .SelectMany(
                                s => s.MonitorResults.Where(mr => examType == 0 || examType == (short?) mr.ExamType))
                            .GroupBy(mr => new
                            {
                                mr.ScheduleDetail.StationId,
                                mr.Date,
                                mr.ViolationId,
                                mr.ViolatorId
                            }) // разбиваем по однотипным нарушениям в ППЭ на дату
                            .Select(g => g
                                    .GroupBy(ge => ge.Schedule.ExpertId)
                                    // разбиваем по экспертам внутри групп однотипных нарушений
                                    .Select(ge => ge.Count()) // берём кол-во однотипных нарушений у этого экперта
                                    .Max()
                                // отбираем максимум найденных нарушений одного типа - чтобы убрать дублирующие нарушения от других экспертов
                            )
                            .Sum() // берём сумму по данному региону
                }).OrderBy(r => r.Region).Dump("Результаты");

            bindingSource1.DataSource = query.ToList();
        }
    }
}