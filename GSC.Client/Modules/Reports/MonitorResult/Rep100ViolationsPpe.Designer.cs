﻿namespace GSC.Client.Modules.Reports.MonitorResult
{
    partial class Rep100ViolationsPpe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.stationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.violationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.tableHeader = new DevExpress.XtraReports.UI.XRTable();
            this.headerRowTop = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellHeaderNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderRegion = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderPpeCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.headerRowBottom = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellHeaderNumber2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderRegion2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderPpeCode2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderTotal2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.tableSummary = new DevExpress.XtraReports.UI.XRTable();
            this.summaryRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSummaryTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.stylePpageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleReportHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            this.tableBody = new DevExpress.XtraReports.UI.XRTable();
            this.bodyRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellBodyTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.styleHeaderCenter = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleBodyCenter = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleBodyLeft = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleHeaderLeft = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleHeaderDynamic = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleBodyRight = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.stationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.violationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableBody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 26.04167F;
            // 
            // detailBand1
            // 
            this.detailBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableBody});
            this.detailBand1.HeightF = 22.27564F;
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 26.04167F;
            // 
            // Detail
            // 
            this.Detail.HeightF = 100F;
            this.Detail.Name = "Detail";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 100F;
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 100F;
            this.BottomMargin.Name = "BottomMargin";
            // 
            // stationsBindingSource
            // 
            this.stationsBindingSource.DataSource = typeof(GSC.Client.Modules.Reports.MonitorResult.Rep100ViolationsPpe.StationsInfo);
            // 
            // violationsBindingSource
            // 
            this.violationsBindingSource.DataSource = typeof(GSC.Client.Modules.Reports.MonitorResult.Rep100ViolationsPpe.ViolationsInfo);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.tableHeader,
            this.xrLabelHeader});
            this.ReportHeader.HeightF = 152.0833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 43.41668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(460.6682F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "(без нарушений РЦОИ, КК, ППЗ)";
            // 
            // tableHeader
            // 
            this.tableHeader.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tableHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 74.74358F);
            this.tableHeader.Name = "tableHeader";
            this.tableHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.headerRowTop,
            this.headerRowBottom});
            this.tableHeader.SizeF = new System.Drawing.SizeF(460.6682F, 77.33975F);
            // 
            // headerRowTop
            // 
            this.headerRowTop.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellHeaderNumber,
            this.cellHeaderRegion,
            this.cellHeaderPpeCode,
            this.cellHeaderTotal});
            this.headerRowTop.Name = "headerRowTop";
            this.headerRowTop.Weight = 1.3019519407357743D;
            // 
            // cellHeaderNumber
            // 
            this.cellHeaderNumber.CanGrow = false;
            this.cellHeaderNumber.Name = "cellHeaderNumber";
            this.cellHeaderNumber.RowSpan = 2;
            this.cellHeaderNumber.StyleName = "styleHeaderCenter";
            this.cellHeaderNumber.Text = "№ п/п";
            this.cellHeaderNumber.Weight = 0.3394638079212407D;
            // 
            // cellHeaderRegion
            // 
            this.cellHeaderRegion.CanGrow = false;
            this.cellHeaderRegion.Name = "cellHeaderRegion";
            this.cellHeaderRegion.RowSpan = 2;
            this.cellHeaderRegion.StyleName = "styleHeaderLeft";
            this.cellHeaderRegion.Text = "Субъект Российской Федерации";
            this.cellHeaderRegion.Weight = 0.7970236143212599D;
            // 
            // cellHeaderPpeCode
            // 
            this.cellHeaderPpeCode.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellHeaderPpeCode.CanGrow = false;
            this.cellHeaderPpeCode.Name = "cellHeaderPpeCode";
            this.cellHeaderPpeCode.RowSpan = 2;
            this.cellHeaderPpeCode.StyleName = "styleHeaderCenter";
            this.cellHeaderPpeCode.StylePriority.UseBorders = false;
            this.cellHeaderPpeCode.Text = "Код ППЭ";
            this.cellHeaderPpeCode.Weight = 0.28825069672561821D;
            // 
            // cellHeaderTotal
            // 
            this.cellHeaderTotal.CanGrow = false;
            this.cellHeaderTotal.Name = "cellHeaderTotal";
            this.cellHeaderTotal.RowSpan = 2;
            this.cellHeaderTotal.StyleName = "styleHeaderCenter";
            this.cellHeaderTotal.Text = "Общее количество нарушений";
            this.cellHeaderTotal.Weight = 0.27930080076695657D;
            // 
            // headerRowBottom
            // 
            this.headerRowBottom.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellHeaderNumber2,
            this.cellHeaderRegion2,
            this.cellHeaderPpeCode2,
            this.cellHeaderTotal2});
            this.headerRowBottom.Name = "headerRowBottom";
            this.headerRowBottom.Weight = 0.54496741758580436D;
            // 
            // cellHeaderNumber2
            // 
            this.cellHeaderNumber2.CanGrow = false;
            this.cellHeaderNumber2.Name = "cellHeaderNumber2";
            this.cellHeaderNumber2.Weight = 0.41426019842985778D;
            // 
            // cellHeaderRegion2
            // 
            this.cellHeaderRegion2.CanGrow = false;
            this.cellHeaderRegion2.Name = "cellHeaderRegion2";
            this.cellHeaderRegion2.Weight = 0.97263755506038774D;
            // 
            // cellHeaderPpeCode2
            // 
            this.cellHeaderPpeCode2.CanGrow = false;
            this.cellHeaderPpeCode2.Name = "cellHeaderPpeCode2";
            this.cellHeaderPpeCode2.Weight = 0.35176266010171642D;
            // 
            // cellHeaderTotal2
            // 
            this.cellHeaderTotal2.CanGrow = false;
            this.cellHeaderTotal2.Name = "cellHeaderTotal2";
            this.cellHeaderTotal2.Weight = 0.34084111244884624D;
            // 
            // xrLabelHeader
            // 
            this.xrLabelHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrLabelHeader.Name = "xrLabelHeader";
            this.xrLabelHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelHeader.SizeF = new System.Drawing.SizeF(460.6683F, 33.41667F);
            this.xrLabelHeader.StyleName = "styleReportHeader";
            this.xrLabelHeader.Text = "Нарушения по видам в разрезе ППЭ";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 63.54167F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(507.8751F, 30.54167F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(253.125F, 23F);
            this.xrPageInfo1.StyleName = "stylePpageInfo";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableSummary});
            this.ReportFooter.HeightF = 36.45833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // tableSummary
            // 
            this.tableSummary.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableSummary.Name = "tableSummary";
            this.tableSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.summaryRow});
            this.tableSummary.SizeF = new System.Drawing.SizeF(460.6682F, 18.48958F);
            // 
            // summaryRow
            // 
            this.summaryRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.cellSummaryTotal});
            this.summaryRow.Name = "summaryRow";
            this.summaryRow.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StyleName = "styleBodyRight";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "Итого:";
            this.xrTableCell1.Weight = 3.0000000877314985D;
            // 
            // cellSummaryTotal
            // 
            this.cellSummaryTotal.Name = "cellSummaryTotal";
            this.cellSummaryTotal.StyleName = "styleBodyCenter";
            this.cellSummaryTotal.Weight = 0.58810934157356787D;
            this.cellSummaryTotal.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cellSummaryTotal_BeforePrint);
            // 
            // stylePpageInfo
            // 
            this.stylePpageInfo.Name = "stylePpageInfo";
            this.stylePpageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.stylePpageInfo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // styleReportHeader
            // 
            this.styleReportHeader.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.styleReportHeader.Name = "styleReportHeader";
            this.styleReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // tableBody
            // 
            this.tableBody.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableBody.Name = "tableBody";
            this.tableBody.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.bodyRow});
            this.tableBody.SizeF = new System.Drawing.SizeF(460.6683F, 22.27564F);
            // 
            // bodyRow
            // 
            this.bodyRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell12,
            this.cellBodyTotal});
            this.bodyRow.Name = "bodyRow";
            this.bodyRow.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StyleName = "styleBodyCenter";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell4.Summary = xrSummary1;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 0.25004588287549934D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RegionName")});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StyleName = "styleBodyLeft";
            this.xrTableCell5.Weight = 0.58708018246597571D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "StationCode")});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StyleName = "styleBodyCenter";
            this.xrTableCell12.Weight = 0.21232272941190056D;
            // 
            // cellBodyTotal
            // 
            this.cellBodyTotal.Name = "cellBodyTotal";
            this.cellBodyTotal.StyleName = "styleBodyCenter";
            this.cellBodyTotal.Weight = 0.20573059898195434D;
            this.cellBodyTotal.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cellBodyTotal_BeforePrint);
            // 
            // styleHeaderCenter
            // 
            this.styleHeaderCenter.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleHeaderCenter.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.styleHeaderCenter.Name = "styleHeaderCenter";
            this.styleHeaderCenter.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleHeaderCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // styleBodyCenter
            // 
            this.styleBodyCenter.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleBodyCenter.Name = "styleBodyCenter";
            this.styleBodyCenter.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleBodyCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // styleBodyLeft
            // 
            this.styleBodyLeft.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleBodyLeft.Name = "styleBodyLeft";
            this.styleBodyLeft.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleBodyLeft.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // styleHeaderLeft
            // 
            this.styleHeaderLeft.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleHeaderLeft.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.styleHeaderLeft.Name = "styleHeaderLeft";
            this.styleHeaderLeft.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleHeaderLeft.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // styleHeaderDynamic
            // 
            this.styleHeaderDynamic.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleHeaderDynamic.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.styleHeaderDynamic.Name = "styleHeaderDynamic";
            this.styleHeaderDynamic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleHeaderDynamic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // styleBodyRight
            // 
            this.styleBodyRight.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.styleBodyRight.Name = "styleBodyRight";
            this.styleBodyRight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.styleBodyRight.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Rep100ViolationsPpe
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1,
            this.ReportHeader,
            this.PageFooter,
            this.ReportFooter});
            this.DataSource = this.stationsBindingSource;
            this.Margins = new System.Drawing.Printing.Margins(26, 30, 26, 26);
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.stylePpageInfo,
            this.styleReportHeader,
            this.styleHeaderCenter,
            this.styleBodyCenter,
            this.styleBodyLeft,
            this.styleHeaderLeft,
            this.styleHeaderDynamic,
            this.styleBodyRight});
            this.Version = "15.2";
            this.Controls.SetChildIndex(this.ReportFooter, 0);
            this.Controls.SetChildIndex(this.PageFooter, 0);
            this.Controls.SetChildIndex(this.ReportHeader, 0);
            this.Controls.SetChildIndex(this.bottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.detailBand1, 0);
            this.Controls.SetChildIndex(this.topMarginBand1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.stationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.violationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableBody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private System.Windows.Forms.BindingSource stationsBindingSource;
        private System.Windows.Forms.BindingSource violationsBindingSource;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRControlStyle stylePpageInfo;
        private DevExpress.XtraReports.UI.XRLabel xrLabelHeader;
        private DevExpress.XtraReports.UI.XRTable tableBody;
        private DevExpress.XtraReports.UI.XRTableRow bodyRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell cellBodyTotal;
        private DevExpress.XtraReports.UI.XRTable tableHeader;
        private DevExpress.XtraReports.UI.XRTableRow headerRowTop;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderNumber;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderRegion;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderTotal;
        private DevExpress.XtraReports.UI.XRControlStyle styleReportHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderPpeCode;
        private DevExpress.XtraReports.UI.XRControlStyle styleHeaderCenter;
        private DevExpress.XtraReports.UI.XRControlStyle styleBodyCenter;
        private DevExpress.XtraReports.UI.XRControlStyle styleBodyLeft;
        private DevExpress.XtraReports.UI.XRControlStyle styleHeaderLeft;
        private DevExpress.XtraReports.UI.XRTableRow headerRowBottom;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderNumber2;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderRegion2;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderPpeCode2;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderTotal2;
        private DevExpress.XtraReports.UI.XRControlStyle styleHeaderDynamic;
        private DevExpress.XtraReports.UI.XRTable tableSummary;
        private DevExpress.XtraReports.UI.XRTableRow summaryRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell cellSummaryTotal;
        private DevExpress.XtraReports.UI.XRControlStyle styleBodyRight;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
