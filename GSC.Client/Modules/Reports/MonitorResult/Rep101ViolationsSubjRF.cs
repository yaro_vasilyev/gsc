﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Нарушения, выявленные в субъекте Российской Федерации»
    /// </summary>
    public partial class Rep101ViolationsSubjRF : GscReport
    {
        private string regionName;

        public Rep101ViolationsSubjRF()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep101ViolationsSubjRF(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class DataRow
        {
            public DateTime? Date { get; set; }

            public string Subjects { get; set; }

            public string Station { get; set; }

            public string Violation { get; set; }

            public string Violator { get; set; }

            public decimal Serie { get; set; }
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            var region = ViewModel.UnitOfWork.Regions
                    .OrderBy(x => x.Name)
                    .ToList()
                    .Select(x => new LookUpValue(x.Id, x.Name));

            parameters.Add(EnumReportParameter.CreateDynamicListParam(
                "regionId", "Субъект РФ:", region, EnumReportParameter.Null));
            parameters.Add(EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Тип экзамена:", EnumReportParameter.Null));
        }

        protected override void OnParametersRequestSubmit(ParametersRequestEventArgs e)
        {
            base.OnParametersRequestSubmit(e);

            regionName = GetLookupParameterDisplayText(e, "regionId");
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var projectId = ViewModel.CurrentProject.Id;

            var examTypeParam = EnumReportParameter.NullableValue<int>(Parameters["examType"]);
            var regionParam = Parameters["regionId"];

            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<DataRow>(@"
select Date, Subjects, Station, Violation, Violator 
  from gsc.r_gsc101_violations_in_region (:p0::integer, :p1::integer, :p2::integer)
    as f (Date, Subjects, Station, ViolationCode, Violation, ViolatorCode, Violator, Serie)
 order by Date, Station, Subjects, ViolationCode, Violation, ViolatorCode, Violator, Serie", 
                projectId, regionParam.Value, examTypeParam);

            bindingSource1.DataSource = data.ToList();
        }

        private void regionField_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            e.Value = regionName;
        }
    }
}
