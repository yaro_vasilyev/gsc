﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using System.Linq;
using GSC.Client.ViewModels.Reports;
using GSC.Model;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Общее количество нарушений по ППЭ»
    /// </summary>
    public partial class Rep097CountMrPpe : GscReport
    {
        public Rep097CountMrPpe()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep097CountMrPpe(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class Data
        {
            public Model.ExamType ExamType { get; set; }
            public decimal Violations { get; set; }
            /// <summary>
            /// Выходы экспертов
            /// </summary>
            public decimal ScheduleRuns { get; set; }
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var project = ViewModel.CurrentProject.Id;

            //todo перенести в БД или в LINQ
            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<Data>(@"
select v.ExamType,
       v.Violations,
       v.ScheduleRuns
  from gsc.r_gsc097_count_mr_ppe (:p0::integer)
    as v (ExamType, Violations, ScheduleRuns)", project);

            bindingSource1.DataSource = data.ToList();
        }

        private void calculatedExamType_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            switch (((Data) e.Row).ExamType)
            {
                case ExamType.Ege:
                    e.Value = "ЕГЭ";
                    break;
                case ExamType.Oge:
                    e.Value = "ОГЭ";
                    break;
                case ExamType.Gve:
                    e.Value = "ГВЭ";
                    break;
            }
        }

        private void calculatedPercentTotal_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            var data = ((IEnumerable<Data>)bindingSource1.DataSource).ToList();
            var totalViolations = data.Aggregate(0m, (arg1, data1) => arg1 + data1.Violations);
            var totalScheduleRuns = data.Aggregate(0m, (arg1, data1) => arg1 + data1.ScheduleRuns);
            e.Value = totalViolations/totalScheduleRuns; // multiply by 100 in Format
        }
    }
}
