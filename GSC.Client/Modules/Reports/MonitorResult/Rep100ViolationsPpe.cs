﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    /// <summary>
    /// Отчет «Нарушения по видам в разрезе ППЭ»
    /// </summary>
    public partial class Rep100ViolationsPpe : GscReport
    {
        private const int DynamicColWidth = 100;

        public Rep100ViolationsPpe()
        {
            InitializeComponent();
        }

        public Rep100ViolationsPpe(ReportsViewModel reportsViewModel)
            : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class StationsInfo
        {
            public string RegionCode { get; set; }
            public string RegionName { get; set; }
            public int StationId { get; set; }
            public string StationCode { get; set; }
            public string StationName { get; set; }
        }

        public class ViolationsInfo
        {
            public int StationId { get; set; }
            public string ViolationCode { get; set; }
            public string ViolationName { get; set; }
            public long Count { get; set; }
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            var examTypeParam = EnumReportParameter.CreateStaticListParam<Model.ExamType>(
                "examType", "Форма ГИА", EnumReportParameter.Null);
            parameters.Add(examTypeParam);
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var currentProjectId = ViewModel.CurrentProject.Id;

            var examTypeParam = EnumReportParameter.NullableValue<int>(Parameters["examType"]);

            var db = ViewModel.UnitOfWork.Context.Database;
            var stationsTask = db.SqlQuery<StationsInfo>(@"
select f.RegionCode,
       f.RegionName,
       f.StationId,
       f.StationCode,
       f.StationName
  from gsc.r_gsc100_stations (:p0::integer, :p1::integer) as f(RegionCode, RegionName, StationId, StationCode, StationName)",
                currentProjectId, examTypeParam).ToListAsync();

            var violationsTask = db.SqlQuery<ViolationsInfo>(@"
select f.StationId, 
       f.ViolationCode,
       f.ViolationName,
       f.Count
  from gsc.r_gsc100_violations (:p0::integer, :p1::integer) as f(StationId, ViolationCode, ViolationName, Count)",
                currentProjectId, examTypeParam).ToListAsync();

            var allComplete = Task.WaitAll(new Task[] {stationsTask, violationsTask}, TimeSpan.FromMinutes(1.0));
            if (allComplete)
            {
                stationsBindingSource.DataSource = stationsTask.Result;
                var violations = violationsTask.Result;
                violationsBindingSource.DataSource = violations;

                var distinctViolations = violations
                    .Select(x => x.ViolationCode)
                    .Distinct()
                    .OrderBy(x => x)
                    .ToList();

                try
                {
                    tableHeader.BeginInit();
                    tableBody.BeginInit();

                    var numberSpanWidth = cellHeaderNumber.WidthF;
                    var regionSpanWidth = cellHeaderRegion.WidthF;
                    var totalSpanWidth = cellHeaderTotal.WidthF;
                    var ppeCodeSpanWidth = cellHeaderPpeCode.WidthF;

                    XRTableCell cellViolHeader = null;
                    for (int index = 0; index < distinctViolations.Count; index++)
                    {
                        var violationCode = distinctViolations[index];

                        if (index == 0)
                        {
                            cellViolHeader = new XRTableCell
                            {
                                Name = "cellViolHeader",
                                WidthF = DynamicColWidth,
                                Text = "Нарушения, выявленные при проведении ГИА, по видам",
                                StyleName = "styleHeaderCenter"
                            };
                            headerRowTop.Cells.Add(cellViolHeader);
                        }
                        else
                        {
                            Debug.Assert(cellViolHeader != null);
                        }


                        var cellHeader = new XRTableCell
                        {
                            Tag = violationCode,
                            Width = DynamicColWidth,
                            Text = violationCode,
                            StyleName = "styleHeaderDynamic"
                        };

                        var cellBody = new XRTableCell
                        {
                            Tag = violationCode,
                            Width = DynamicColWidth,
                            StyleName = "styleBodyCenter"
                        };

                        var cellSummary = new XRTableCell
                        {
                            Tag = violationCode,
                            WidthF = DynamicColWidth,
                            StyleName = "styleBodyCenter"
                        };

                        cellBody.BeforePrint += CellBodyOnBeforePrint;
                        cellSummary.BeforePrint += CellSummaryOnBeforePrint;

                        if (index > 0)
                            cellViolHeader.WidthF += DynamicColWidth;


                        tableHeader.WidthF += DynamicColWidth;
                        tableBody.WidthF += DynamicColWidth;
                        tableSummary.WidthF += DynamicColWidth;

                        headerRowBottom.Cells.Add(cellHeader);
                        bodyRow.Cells.Add(cellBody);
                        summaryRow.Cells.Add(cellSummary);
                    }
                }
                finally
                {
                    tableBody.EndInit();
                    tableHeader.EndInit();
                }
            }
        }

        private void CellBodyOnBeforePrint(object sender, PrintEventArgs printEventArgs)
        {
            var control = (XRControl)sender;
            var row = (Rep100ViolationsPpe.StationsInfo) GetCurrentRow();
            var violationCode = control.Tag as string;
            if (violationCode != null)
            {
                var violations = (IEnumerable<ViolationsInfo>) violationsBindingSource.DataSource;
                var count = violations.Where(v => v.StationId == row.StationId && v.ViolationCode == violationCode)
                    .Sum(x => x.Count);
                control.Text = count.ToString();
            }
        }

        private void CellSummaryOnBeforePrint(object sender, PrintEventArgs e)
        {
            var control = (XRControl)sender;
            var violationCode = control.Tag as string;
            if (violationCode != null)
            {
                var violations = (IEnumerable<ViolationsInfo>) violationsBindingSource.DataSource;
                var count = violations.Where(x => x.ViolationCode == violationCode).Sum(x => x.Count);
                control.Text = count.ToString();
            }
        }


        private void cellBodyTotal_BeforePrint(object sender, PrintEventArgs e)
        {
            var control = (XRControl)sender;
            var row = (Rep100ViolationsPpe.StationsInfo)GetCurrentRow();
            var violations = (IEnumerable<ViolationsInfo>)violationsBindingSource.DataSource;

            var count = violations.Where(x => x.StationId == row.StationId).Sum(x => x.Count);
            control.Text = count.ToString();
        }

        private void cellSummaryTotal_BeforePrint(object sender, PrintEventArgs e)
        {
            var control = (XRControl)sender;
            var violations = (IEnumerable<ViolationsInfo>)violationsBindingSource.DataSource;

            var count = violations.Sum(x => x.Count);
            control.Text = count.ToString();
        }
    }
}
