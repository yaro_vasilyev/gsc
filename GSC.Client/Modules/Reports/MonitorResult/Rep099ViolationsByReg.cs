﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.MonitorResult
{
    public partial class Rep099ViolationsByReg : GscReport
    {
        private const int DynamicCellWidth = 150;
        private const string TopHeadCellName = "tcDynCollsComm" /* for auto deletion */;

        public Rep099ViolationsByReg()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep099ViolationsByReg(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        /*
            № п/п	Субъект Российской Федерации	Общее количество нарушений	Нарушения, выявленные при проведении ГИА, по видам	
                                                                                Нарушение 1	Нарушение 2	…	Нарушение n
 	 	 	 	 	 	 
                    Итого:	 	 	 	 	  	 
        */

        private void Rep099ViolationsByReg_DataSourceDemanded(object sender, EventArgs e)
        {
            var project = ViewModel.CurrentProject;

            var regions = ViewModel.UnitOfWork.Regions;
            var examType = (int) pExamType.Value;

            var query = regions
                .Select(r => new DataRow
                {
                    Region = r.Name,
                    ViolationQties = r.Schedules
                        .Where(s => s.Expert.ProjectId == project.Id)
                        .SelectMany(s => s.MonitorResults)
                        .Where(mr => examType == 0 || (int) mr.ExamType == examType)
                        .GroupBy(mr => mr.Violation)
                        .Select(mrGv => new ViolQty
                        {
                            Violation = mrGv.Key,
                            Quantity = mrGv
                                .GroupBy(mr => new
                                {
                                    mr.ScheduleDetail.StationId,
                                    mr.Date,
                                    mr.ViolatorId
                                })
                                .Select(mrGvt => mrGvt
                                        .GroupBy(mr => mr.Schedule.ExpertId)
                                        // разбиваем по экспертам внутри групп однотипных нарушений
                                        .Max(mrGe => mrGe.Count())
                                    // отбираем максимум кол-ва найденных нарушений одного типа - чтобы убрать дублирующие нарушения от других экспертов
                                )
                                .Sum() // суммируем максимумы по группам
                        })
                })
                .OrderBy(dr => dr.Region)
                .ToList().Dump("Query by regions");

            var dynCols = query
                .SelectMany(dr => dr.ViolationQties, (dr, x) => x.Violation)
                .Distinct()
                .OrderBy(v => v.Code)
                .ToList().Dump("Динамические колонки");

            // create dynamic cells
            tHead.BeginInit();
            try
            {
                tBody.BeginInit();
                try
                {
                    tFooter.BeginInit();
                    try
                    {
                        var topCellWidth = 0;

                        foreach (var dynCol in dynCols)
                        {
                            // add head dynamic col
                            var headCell = new XRTableCell
                            {
                                Text = $@"{dynCol.Code}. {dynCol.Name}",
                                Tag = dynCol.Code,
                                Width = DynamicCellWidth,
                                CanGrow = true,
                                Multiline = true
                            };
                            trHead.Cells.Add(headCell);
                            trHead.Width += headCell.Width;
                            tHead.Width += headCell.Width;
                            topCellWidth += headCell.Width;

                            // add body dynamic col
                            var bodyCell = new XRTableCell
                            {
                                Width = DynamicCellWidth,
                                Tag = dynCol.Code,
                                CanGrow = true,
                                Multiline = true
                            };
                            bodyCell.BeforePrint += BodyCellOnBeforePrint;
                            trBody.Cells.Add(bodyCell);
                            trBody.Width += bodyCell.Width;
                            tBody.Width += bodyCell.Width;

                            // add footer dynamic sum
                            var footCell = new XRTableCell
                            {
                                Tag = dynCol.Code,
                                Width = DynamicCellWidth,
                                CanGrow = true
                            };
                            footCell.BeforePrint += FootCellOnBeforePrint;
                            trFooter.Cells.Add(footCell);
                            trFooter.Width += footCell.Width;
                            tFooter.Width += footCell.Width;
                        }

                        // add common head for dynamic cols
                        if (topCellWidth > 0)
                        {
                            var topHeadCell = new XRTableCell
                            {
                                Text = @"Нарушения, выявленные при проведении ГИА, по видам",
                                Name = TopHeadCellName /* for auto deletion */,
                                CanGrow = true,
                                Width = topCellWidth
                            };
                            trTopHead.Cells.Add(topHeadCell);
                        }
                    }
                    finally
                    {
                        tFooter.EndInit();
                    }
                }
                finally
                {
                    tBody.EndInit();
                }
            }
            finally
            {
                tHead.EndInit();
            }

            bindingSource1.DataSource = query;
        }

        private void FootCellOnBeforePrint(object sender, PrintEventArgs printEventArgs)
        {
            var cell = sender as XRTableCell;
            var violCode = (int?) cell?.Tag;
            var dataSource = bindingSource1.DataSource as List<DataRow>;
            var totalCount = dataSource?.SelectMany(dr => dr.ViolationQties).Where(v => v.Violation?.Code == violCode).Sum(v => v.Quantity);

            if (cell != null)
            {
                cell.Text = $@"{totalCount}";
            }
        }

        private void BodyCellOnBeforePrint(object sender, PrintEventArgs printEventArgs)
        {
            var cell = sender as XRTableCell;
            var violCode = (int?) cell?.Tag;
            var row = GetCurrentRow() as DataRow;
            var count = row?.ViolationQties.Where(vr => vr.Violation.Code == violCode).Sum(vr => vr.Quantity);

            if (cell != null)
            {
                cell.Text = $@"{count}";
            }
        }

        private void Rep099ViolationsByReg_ParametersRequestSubmit(object sender,
            ParametersRequestEventArgs e)
        {
            tHead.BeginInit();
            try
            {
                IEnumerable<XRTableCell> headCells = trHead.Cells;
                foreach (var cell in headCells.Where(cell => cell.Tag is int).ToList())
                {
                    var width = cell.Width;
                    trHead.Cells.Remove(cell);
                    trHead.Width -= width;
                    tHead.Width -= width;
                }

                var topHeadCell = trTopHead.Cells[TopHeadCellName];
                if (topHeadCell != null)
                {
                    trTopHead.Cells.Remove(topHeadCell);
                }
            }
            finally
            {
                tHead.EndInit();
            }

            tBody.BeginInit();
            try
            {
                IEnumerable<XRTableCell> bodyCells = trBody.Cells;
                foreach (var cell in bodyCells.Where(cell => cell.Tag is int).ToList())
                {
                    var width = cell.Width;
                    trBody.Cells.Remove(cell);
                    trBody.Width -= width;
                    tBody.Width -= width;
                }
            }
            finally
            {
                tBody.EndInit();
            }

            tFooter.BeginInit();
            try
            {
                IEnumerable<XRTableCell> footCells = trFooter.Cells;
                foreach (var cell in footCells.Where(c => c.Tag is int).ToList())
                {
                    var width = cell.Width;
                    trFooter.Cells.Remove(cell);
                    trFooter.Width -= width;
                    tFooter.Width -= width;
                }
            }
            finally
            {
                tFooter.EndInit();
            }
        }

        public class ViolQty
        {
            public int Quantity { get; set; }
            public Violation Violation { get; set; }
        }

        public class DataRow
        {
            public string Region { get; set; }
            public IEnumerable<ViolQty> ViolationQties { get; set; }
            [UsedImplicitly]
            public int ViolationsCount => ViolationQties.Sum(v => v.Quantity);
        }
    }
}