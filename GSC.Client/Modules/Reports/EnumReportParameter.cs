﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using DevExpress.XtraReports.Parameters;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports
{
    internal static class EnumReportParameter
    {
        public static readonly LookUpValue Null = new LookUpValue(-1, "<Не выбрано>");

        public static Parameter CreateStaticListParam<TEnum>(
            string name,
            string description,
            LookUpValue nullOption = null)
        {
            var t = typeof(TEnum);
            if (!typeof(Enum).IsAssignableFrom(t))
            {
                throw new ArgumentException("Generic Type parameter should be enum.");
            }

            var lookup = new StaticListLookUpSettings();

            var optionNames = Enum.GetNames(t);
            var optionValues = Enum.GetValues(t);

            Debug.Assert(optionNames.Length == optionValues.Length);

            if (nullOption != null)
            {
                lookup.LookUpValues.Add(nullOption);
            }

            for (var i = 0; i < optionNames.Length; i++)
            {
                var optionName = optionNames[i];
                var field = t.GetField(optionName, BindingFlags.Public | BindingFlags.Static);
                var displayName = field.GetCustomAttribute<DisplayAttribute>();

                lookup.LookUpValues.Add(new LookUpValue(optionValues.GetValue(i),
                    displayName?.Name ?? optionName));
            }

            var param = new Parameter
            {
                Name = name,
                Description = description,
                LookUpSettings = lookup,
                Type = typeof(int),
            };

            if (nullOption != null)
            {
                param.Value = nullOption.Value;
            }

            return param;
        }

        public static Parameter CreateDynamicListParam(
            string name,
            string description,
            IEnumerable<LookUpValue> dataSource,
            LookUpValue nullOption = null)
        {
            if (nullOption != null)
            {
                var list = dataSource as IList;
                if (list != null)
                {
                    list.Insert(0, nullOption);
                }
                else
                {
                    list = dataSource.ToList();
                    list.Insert(0, nullOption);
                    dataSource = (IEnumerable<LookUpValue>) list; // Is it even possible?
                }
            }

            var lookup = new DynamicListLookUpSettings
            {
                DataSource = dataSource,
                ValueMember = nameof(LookUpValue.Value),
                DisplayMember = nameof(LookUpValue.Description),
            };

            var param = new Parameter
            {
                Name = name,
                Description = description,
                LookUpSettings = lookup,
                Type = typeof(int)
            };

            if (nullOption != null)
            {
                param.Value = nullOption.Value;
            }

            return param;
        }

        public static T? NullableValue<T>([NotNull] Parameter param)
            where T : struct
        {
            if (param == null)
            {
                throw new ArgumentNullException(nameof(param));
            }

            var val = param.Value;
            if (ReferenceEquals(val, null) || Equals(val, Null.Value))
            {
                return null;
            }

            return (T)val;
        }
    }
}
