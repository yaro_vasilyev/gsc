﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;
using GSC.Model;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep067ExpertContact : GscReport
    {
        public Rep067ExpertContact()
        {
            InitializeComponent();
        }

        public Rep067ExpertContact(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            parameters.Add(EnumReportParameter.CreateStaticListParam<PeriodType>(
                "periodType", "Этап:", EnumReportParameter.Null));
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var periodType = Parameters["periodType"].Value;

            var projectId = ViewModel.CurrentProject.Id;
            var schedules = ViewModel.UnitOfWork.Schedules
                .Where(x => x.Expert.ProjectId == projectId &&
                            (periodType == null || x.PeriodType == (PeriodType) (int) periodType))
                .Include(x => x.Expert)
                .Include(x => x.Expert.BaseRegion)
                .Include(x => x.Region);

            bindingSource1.DataSource = schedules.ToList();
        }
    }
}
