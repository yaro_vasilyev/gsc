﻿using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class TestReport : GscReport
    {
        public TestReport(ReportsViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }

        protected override void OnReportInitialize()
        {
            base.OnReportInitialize();

            var label = new XRLabel();
            var reportHeader = new ReportHeaderBand();
            reportHeader.Controls.Add(label);
            Bands.Add(reportHeader);
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            // Create a parameter and specify its name.
            var param1 = new Parameter
            {
                Name = "CatID",
                Type = typeof (int),
                Value = 1,
                Description = @"Category: ",
                Visible = true
            };

            // Specify other parameter properties.

            // Add the parameter to the report.
            parameters.Add(param1);
        }

        protected override void OnParametersRequestSubmit(ParametersRequestEventArgs e)
        {
            base.OnParametersRequestSubmit(e);

            var label = Bands.GetBandByType(typeof (ReportHeaderBand))?.Controls.OfType<XRLabel>().SingleOrDefault();
            if (label == null)
            {
                return;
            }

            var binding = label.DataBindings.SingleOrDefault(x => x.PropertyName == "Text");
            if (binding != null)
            {
                label.DataBindings.Remove(binding);
            }

            label.DataBindings.Add(new XRBinding(Parameters[0], "Text", "Category: {0}"));
        }
    }
}