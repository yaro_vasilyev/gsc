﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep060ExpertList : GscReport
    {
        public Rep060ExpertList()
        {
            InitializeComponent();
        }

        public Rep060ExpertList(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void ModifyParameters(ICollection<Parameter> parameters)
        {
            base.ModifyParameters(parameters);

            var p = EnumReportParameter.CreateStaticListParam<Model.ExpertKind>("expertKind",
                                                                                "Категория:",
                                                                                EnumReportParameter.Null);
            parameters.Add(p);

            var observers =
                ViewModel.UnitOfWork.Observers
                    .OrderBy(x => x.Fio)
                    .ToList()
                    .Select(x => new LookUpValue(x.Id, x.Fio));

            p = EnumReportParameter.CreateDynamicListParam("observerId",
                                                           "Менеджер/координатор:",
                                                           observers,
                                                           EnumReportParameter.Null);
            parameters.Add(p);

            p = new Parameter
                {
                    Name = "observerFlag",
                    Description = "Учитывать невыбранное значение параметра Менеджер/координатор:",
                    Type = typeof (bool),
                    Value = true
                };
            parameters.Add(p);
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var projectId = ViewModel.CurrentProject.Id;

            bindingSource1.DataSource = DataRow.GetData(ViewModel.UnitOfWork.Context,
                                                        projectId,
                                                        EnumReportParameter.NullableValue<Model.ExpertKind>(Parameters["expertKind"]),
                                                        EnumReportParameter.NullableValue<int>(Parameters["observerId"]),
                                                        (bool)Parameters["observerFlag"].Value)
                .ToList();
        }

        public class DataRow
        {
            public int ExpertId { get; set; }
            public string ExpertFio { get; set; }
            public string BaseRegion { get; set; }
            public string ScheduleRegion { get; set; }
            public string MonitorObjects { get; set; }
            public string ExpertPostAndWp { get; set; }
            public string ExpertPhone { get; set; }
            public string ExpertEmail { get; set; }
            public string ExpertAdAt { get; set; }
            public string Expirience { get; set; }

            public static DbRawSqlQuery<DataRow> GetData([NotNull] DbContext context,
                                                         int projectId,
                                                         Model.ExpertKind? expertKind,
                                                         int? observerId,
                                                         bool useObserver)
            {
                if (context == null)
                    throw new ArgumentNullException(nameof(context));

                //todo перенести в БД или в LINQ
                return context.Database.SqlQuery<DataRow>("select * from gsc.r_gsc060_expert_list(:p0::integer, :p1::integer, :p2::integer, :p3::boolean)",
                    projectId, expertKind, observerId, useObserver);
            }
        }
    }
}
