﻿using System;
using JetBrains.Annotations;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep068InfoCardExpert : GscReport
    {
        public Rep068InfoCardExpert()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep068InfoCardExpert(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class ReportRow
        {
            public string fullname { get; set; }
            public string liveRegion { get; set; }
            public string speciality { get; set; }
            public string post { get; set; }
            public string workplace { get; set; }
            public int work_years_total { get; set; }
            public int exp_years_education { get; set; }
            public string expirience { get; set; }
            public string academicDegree { get; set; }
            public string academicTitle { get; set; }
            public string awards { get; set; }
            public int can_mission { get; set; }
            public string chief { get; set; }

        }

        protected override void OnParametersRequestBeforeShow(ParametersRequestEventArgs e)
        {
            base.OnParametersRequestBeforeShow(e);
            var unitOfWork = ViewModel.UnitOfWork;
            var currProject = ViewModel.CurrentProject.Id;
            expertBindingSource.DataSource = unitOfWork.Experts.Where(expert => expert.ProjectId == currProject);
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            //todo перенести в БД или в LINQ
            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<ReportRow>(@"
            select concat_ws(' ',ex.surname, ex.name, ex.patronymic)  as fullname,
                   r.name as liveRegion,
                   ex.speciality,
                   ex.post,
                   ex.workplace,
                   ex.work_years_total,
                   ex.exp_years_education,
                   ex.expirience,
                   ad.name as academicDegree,
                   at.name as academicTitle,
                   ex.awards,
                   ex.can_mission,
                   concat_ws('; ', ex.chief_post, ex.chief_name, ex.chief_email) as chief       
            from gsc.v_expert ex
                 left join gsc.v_region r on (ex.base_region_id = r.id)
                 left join gsc.v_academic_degree ad on (ex.academic_degree_id = ad.id)
                 left join gsc.v_academic_title at on (ex.academic_title_id = at.id)   
            where ex.id =:p0", expert_id.Value).ToList();
            bindingSource1.DataSource = data;
        }
    }
}
