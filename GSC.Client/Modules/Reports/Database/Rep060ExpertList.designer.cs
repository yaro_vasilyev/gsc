﻿namespace GSC.Client.Modules.Reports.Database
{
    partial class Rep060ExpertList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.rData = new DevExpress.XtraReports.UI.XRTableRow();
            this.cNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.cFio = new DevExpress.XtraReports.UI.XRTableCell();
            this.cBaseRegion = new DevExpress.XtraReports.UI.XRTableCell();
            this.cRegion = new DevExpress.XtraReports.UI.XRTableCell();
            this.cStations = new DevExpress.XtraReports.UI.XRTableCell();
            this.cPostWp = new DevExpress.XtraReports.UI.XRTableCell();
            this.cPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.cEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.cAdAt = new DevExpress.XtraReports.UI.XRTableCell();
            this.cExpirience = new DevExpress.XtraReports.UI.XRTableCell();
            this.pageHeaderBand1 = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.rHeader = new DevExpress.XtraReports.UI.XRTableRow();
            this.hNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.hFio = new DevExpress.XtraReports.UI.XRTableCell();
            this.hBaseRegion = new DevExpress.XtraReports.UI.XRTableCell();
            this.hRegion = new DevExpress.XtraReports.UI.XRTableCell();
            this.hStations = new DevExpress.XtraReports.UI.XRTableCell();
            this.hPostWp = new DevExpress.XtraReports.UI.XRTableCell();
            this.hPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.hEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.hAdAt = new DevExpress.XtraReports.UI.XRTableCell();
            this.hExpirience = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            this.styleCell = new DevExpress.XtraReports.UI.XRControlStyle();
            this.bindingSource1 = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 96F;
            this.topMarginBand1.HeightF = 36F;
            this.topMarginBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.topMarginBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // detailBand1
            // 
            this.detailBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.detailBand1.Dpi = 96F;
            this.detailBand1.HeightF = 24F;
            this.detailBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.detailBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 96F;
            this.bottomMarginBand1.HeightF = 36.01004F;
            this.bottomMarginBand1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.bottomMarginBand1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 96F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.rData});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1049F, 24F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // rData
            // 
            this.rData.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cNumber,
            this.cFio,
            this.cBaseRegion,
            this.cRegion,
            this.cStations,
            this.cPostWp,
            this.cPhone,
            this.cEmail,
            this.cAdAt,
            this.cExpirience});
            this.rData.Dpi = 96F;
            this.rData.Name = "rData";
            this.rData.Weight = 1D;
            // 
            // cNumber
            // 
            this.cNumber.Dpi = 96F;
            this.cNumber.Name = "cNumber";
            this.cNumber.StyleName = "styleCell";
            this.cNumber.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.cNumber.Summary = xrSummary1;
            this.cNumber.Text = "cNumber";
            this.cNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cNumber.Weight = 0.57247673034667967D;
            // 
            // cFio
            // 
            this.cFio.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertFio")});
            this.cFio.Dpi = 96F;
            this.cFio.Name = "cFio";
            this.cFio.StyleName = "styleCell";
            this.cFio.StylePriority.UseTextAlignment = false;
            this.cFio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cFio.Weight = 0.73394401550292976D;
            // 
            // cBaseRegion
            // 
            this.cBaseRegion.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseRegion")});
            this.cBaseRegion.Dpi = 96F;
            this.cBaseRegion.Name = "cBaseRegion";
            this.cBaseRegion.StyleName = "styleCell";
            this.cBaseRegion.StylePriority.UseTextAlignment = false;
            this.cBaseRegion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cBaseRegion.Weight = 0.68907853108361439D;
            // 
            // cRegion
            // 
            this.cRegion.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ScheduleRegion")});
            this.cRegion.Dpi = 96F;
            this.cRegion.Name = "cRegion";
            this.cRegion.StyleName = "styleCell";
            this.cRegion.StylePriority.UseTextAlignment = false;
            this.cRegion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cRegion.Weight = 0.85574533626013771D;
            // 
            // cStations
            // 
            this.cStations.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MonitorObjects")});
            this.cStations.Dpi = 96F;
            this.cStations.Multiline = true;
            this.cStations.Name = "cStations";
            this.cStations.StyleName = "styleCell";
            this.cStations.StylePriority.UseTextAlignment = false;
            this.cStations.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cStations.Weight = 0.95221510052796965D;
            // 
            // cPostWp
            // 
            this.cPostWp.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertPostAndWp")});
            this.cPostWp.Dpi = 96F;
            this.cPostWp.Name = "cPostWp";
            this.cPostWp.StyleName = "styleCell";
            this.cPostWp.StylePriority.UseTextAlignment = false;
            this.cPostWp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cPostWp.Weight = 0.93546508351837265D;
            // 
            // cPhone
            // 
            this.cPhone.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertPhone")});
            this.cPhone.Dpi = 96F;
            this.cPhone.Name = "cPhone";
            this.cPhone.StyleName = "styleCell";
            this.cPhone.StylePriority.UseTextAlignment = false;
            this.cPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cPhone.Weight = 0.60812695312511655D;
            // 
            // cEmail
            // 
            this.cEmail.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertEmail")});
            this.cEmail.Dpi = 96F;
            this.cEmail.Name = "cEmail";
            this.cEmail.StyleName = "styleCell";
            this.cEmail.StylePriority.UseTextAlignment = false;
            this.cEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cEmail.Weight = 0.7509826618375457D;
            // 
            // cAdAt
            // 
            this.cAdAt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ExpertAdAt")});
            this.cAdAt.Dpi = 96F;
            this.cAdAt.Name = "cAdAt";
            this.cAdAt.StyleName = "styleCell";
            this.cAdAt.StylePriority.UseTextAlignment = false;
            this.cAdAt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cAdAt.Weight = 0.946220318112915D;
            // 
            // cExpirience
            // 
            this.cExpirience.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cExpirience.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Expirience")});
            this.cExpirience.Dpi = 96F;
            this.cExpirience.Name = "cExpirience";
            this.cExpirience.StyleName = "styleCell";
            this.cExpirience.StylePriority.UseBorders = false;
            this.cExpirience.StylePriority.UseTextAlignment = false;
            this.cExpirience.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.cExpirience.Weight = 0.95574633779995422D;
            // 
            // pageHeaderBand1
            // 
            this.pageHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.pageHeaderBand1.Dpi = 96F;
            this.pageHeaderBand1.HeightF = 62.00001F;
            this.pageHeaderBand1.Name = "pageHeaderBand1";
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 96F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.rHeader});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1049F, 62.00001F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // rHeader
            // 
            this.rHeader.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.hNumber,
            this.hFio,
            this.hBaseRegion,
            this.hRegion,
            this.hStations,
            this.hPostWp,
            this.hPhone,
            this.hEmail,
            this.hAdAt,
            this.hExpirience});
            this.rHeader.Dpi = 96F;
            this.rHeader.Name = "rHeader";
            this.rHeader.Weight = 1D;
            // 
            // hNumber
            // 
            this.hNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.hNumber.CanGrow = false;
            this.hNumber.Dpi = 96F;
            this.hNumber.Name = "hNumber";
            this.hNumber.StyleName = "styleHeader";
            this.hNumber.StylePriority.UseBorders = false;
            this.hNumber.StylePriority.UseTextAlignment = false;
            this.hNumber.Text = "№ п/п";
            this.hNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.hNumber.Weight = 0.15234373092651377D;
            // 
            // hFio
            // 
            this.hFio.CanGrow = false;
            this.hFio.Dpi = 96F;
            this.hFio.Name = "hFio";
            this.hFio.StyleName = "styleHeader";
            this.hFio.Text = "ФИО";
            this.hFio.Weight = 0.19531229019165042D;
            // 
            // hBaseRegion
            // 
            this.hBaseRegion.CanGrow = false;
            this.hBaseRegion.Dpi = 96F;
            this.hBaseRegion.Name = "hBaseRegion";
            this.hBaseRegion.StyleName = "styleHeader";
            this.hBaseRegion.Text = "Базовый субъект";
            this.hBaseRegion.Weight = 0.18337301061835304D;
            // 
            // hRegion
            // 
            this.hRegion.CanGrow = false;
            this.hRegion.Dpi = 96F;
            this.hRegion.Name = "hRegion";
            this.hRegion.StyleName = "styleHeader";
            this.hRegion.Text = "Субъект мониторинга";
            this.hRegion.Weight = 0.22772527576062651D;
            // 
            // hStations
            // 
            this.hStations.CanGrow = false;
            this.hStations.Dpi = 96F;
            this.hStations.Name = "hStations";
            this.hStations.StyleName = "styleHeader";
            this.hStations.Text = "Объекты мониторинга";
            this.hStations.Weight = 0.25339720095375273D;
            // 
            // hPostWp
            // 
            this.hPostWp.CanGrow = false;
            this.hPostWp.Dpi = 96F;
            this.hPostWp.Name = "hPostWp";
            this.hPostWp.StyleName = "styleHeader";
            this.hPostWp.Text = "Должность и место работы";
            this.hPostWp.Weight = 0.24893984346691428D;
            // 
            // hPhone
            // 
            this.hPhone.CanGrow = false;
            this.hPhone.Dpi = 96F;
            this.hPhone.Name = "hPhone";
            this.hPhone.StyleName = "styleHeader";
            this.hPhone.Text = "Номер телефона";
            this.hPhone.Weight = 0.1618305896748182D;
            // 
            // hEmail
            // 
            this.hEmail.CanGrow = false;
            this.hEmail.Dpi = 96F;
            this.hEmail.Name = "hEmail";
            this.hEmail.StyleName = "styleHeader";
            this.hEmail.Text = "Email";
            this.hEmail.Weight = 0.19984655838915164D;
            // 
            // hAdAt
            // 
            this.hAdAt.CanGrow = false;
            this.hAdAt.Dpi = 96F;
            this.hAdAt.Name = "hAdAt";
            this.hAdAt.StyleName = "styleHeader";
            this.hAdAt.Text = "Ученая степень/звание";
            this.hAdAt.Weight = 0.25180204821195518D;
            // 
            // hExpirience
            // 
            this.hExpirience.CanGrow = false;
            this.hExpirience.Dpi = 96F;
            this.hExpirience.Name = "hExpirience";
            this.hExpirience.StyleName = "styleHeader";
            this.hExpirience.Text = "Опыт аналогичной работы";
            this.hExpirience.Weight = 0.25433669362755107D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "xrTableCell2";
            this.xrTableCell2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.Weight = 1D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 1D;
            // 
            // reportHeaderBand1
            // 
            this.reportHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.reportHeaderBand1.Dpi = 96F;
            this.reportHeaderBand1.HeightF = 51F;
            this.reportHeaderBand1.Name = "reportHeaderBand1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 96F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.4F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1049F, 33.6F);
            this.xrLabel1.StyleName = "Title";
            this.xrLabel1.Text = "Перечень экспертов по мониторингу ГИА";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 96F;
            this.PageFooter.HeightF = 22.08F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 96F;
            this.xrPageInfo1.Format = "Страница {0}/{1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(921.7972F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127.2028F, 22.08F);
            this.xrPageInfo1.StyleName = "styleCell";
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BorderColor = System.Drawing.Color.Black;
            this.Title.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1F;
            this.Title.Font = new System.Drawing.Font("Times New Roman", 21F);
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Name = "Title";
            // 
            // styleHeader
            // 
            this.styleHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.styleHeader.Name = "styleHeader";
            this.styleHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 96F);
            this.styleHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // styleCell
            // 
            this.styleCell.Name = "styleCell";
            this.styleCell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 96F);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(GSC.Client.Modules.Reports.Database.Rep060ExpertList.DataRow);
            // 
            // Rep060ExpertList
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1,
            this.pageHeaderBand1,
            this.reportHeaderBand1,
            this.PageFooter});
            this.DataSource = this.bindingSource1;
            this.Dpi = 96F;
            this.Margins = new System.Drawing.Printing.Margins(37, 36, 36, 36);
            this.PageHeight = 794;
            this.PageWidth = 1123;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4Rotated;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.Pixels;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.styleHeader,
            this.styleCell});
            this.Version = "15.2";
            this.Controls.SetChildIndex(this.PageFooter, 0);
            this.Controls.SetChildIndex(this.reportHeaderBand1, 0);
            this.Controls.SetChildIndex(this.pageHeaderBand1, 0);
            this.Controls.SetChildIndex(this.bottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.detailBand1, 0);
            this.Controls.SetChildIndex(this.topMarginBand1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.PageHeaderBand pageHeaderBand1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.ReportHeaderBand reportHeaderBand1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell hRegion;
        private DevExpress.XtraReports.UI.XRTableCell hPostWp;
        private DevExpress.XtraReports.UI.XRTableCell hEmail;
        private DevExpress.XtraReports.UI.XRTableCell hExpirience;
        private DevExpress.XtraReports.UI.XRTableCell hAdAt;
        private DevExpress.XtraReports.UI.XRTableCell hPhone;
        private DevExpress.XtraReports.UI.XRTableCell hStations;
        private DevExpress.XtraReports.UI.XRTableCell hBaseRegion;
        private DevExpress.XtraReports.UI.XRTableCell hFio;
        private DevExpress.XtraReports.UI.XRTableCell hNumber;
        private DevExpress.XtraReports.UI.XRTableRow rHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow rData;
        private DevExpress.XtraReports.UI.XRTableCell cNumber;
        private DevExpress.XtraReports.UI.XRTableCell cFio;
        private DevExpress.XtraReports.UI.XRTableCell cBaseRegion;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableCell cPostWp;
        private DevExpress.XtraReports.UI.XRTableCell cStations;
        private DevExpress.XtraReports.UI.XRTableCell cRegion;
        private DevExpress.XtraReports.UI.XRTableCell cPhone;
        private DevExpress.XtraReports.UI.XRTableCell cEmail;
        private DevExpress.XtraReports.UI.XRTableCell cAdAt;
        private DevExpress.XtraReports.UI.XRTableCell cExpirience;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRControlStyle Title;
        private DevExpress.XtraReports.UI.XRControlStyle styleHeader;
        private DevExpress.XtraReports.UI.XRControlStyle styleCell;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}
