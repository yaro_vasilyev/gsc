﻿using System;
using System.Linq;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep065CriterialTable : GscReport
    {
        public Rep065CriterialTable()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep065CriterialTable(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);

            var unitOfWork = ViewModel.UnitOfWork;
            var currProject = ViewModel.CurrentProject.Id;
            var expertKind = ExpertKind.Value as short?;
            var experts =
                unitOfWork.Experts.Where(
                    expert => expert.ProjectId == currProject && (expertKind == 0 || expertKind == (short?) expert.Kind))
                    .ToList();
            bindingSource1.DataSource = experts;
        }

        private void CanMissionStr_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = BooleanType.Yes.Equals(expert?.CanMission) ? "Да" : "Нет";
        }

        private void InterviewStatusStr_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = InterviewStatus.Success.Equals(expert?.InterviewStatus) ? "Прошёл" : "Не прошёл";
        }

        private void EngageStr_GetValue(object sender, DevExpress.XtraReports.UI.GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = BooleanType.Yes.Equals(expert?.Engage) ? "Да" : "Нет";
        }
    }
}