﻿using System;
using System.Linq;
using GSC.Client.ViewModels.Reports;
using GSC.Model;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Experts4EmailRep : GscReport
    {
        public Experts4EmailRep(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        private void Experts4EmailRep_DataSourceDemanded(object sender, EventArgs e)
        {
            var unitOfWork = ViewModel.UnitOfWork;

            var project = ViewModel.CurrentProject.Id;
            var results = unitOfWork.Experts.Where(
                expert => expert.ProjectId == project && expert.Engage == BooleanType.Yes).ToList();
            expertBindingSource.DataSource = results;
        }
    }
}