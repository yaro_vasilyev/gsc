﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.Parameters;
using GSC.Client.ViewModels.Reports;
using JetBrains.Annotations;
using static GSC.Client.Util.ArrayExt;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep064RegionContacts : GscReport
    {
        public Rep064RegionContacts()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep064RegionContacts(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        private class DataRow
        {
            public string SpecialistCategory { [UsedImplicitly] get; set; }
            public string Fio { [UsedImplicitly] get; set; }
            public string WpAndPost { [UsedImplicitly] get; set; }
            public string Phone { [UsedImplicitly] get; set; }
            public string Email { [UsedImplicitly] get; set; }
        }

        protected override void OnParametersRequestBeforeShow(ParametersRequestEventArgs e)
        {
            base.OnParametersRequestBeforeShow(e);
            regionBindingSource.DataSource =
                ViewModel.UnitOfWork.Regions
                    .Where(r => r.ProjectId == ViewModel.CurrentProject.Id)
                    .OrderBy(r => r.Name)
                    .ToList();
        }

        private void Rep064RegionContacts_DataSourceDemanded(object sender, EventArgs e)
        {
            var unitOfWork = ViewModel.UnitOfWork;
            regionBindingSource.DataSource = unitOfWork.Regions;

            var currProject = ViewModel.CurrentProject.Id;
            var regionId = (int) RegionId.Value;
            var resultList = new List<DataRow>();

            var oivGeks = unitOfWork.OivGeks.Where(og => og.RegionId == regionId
                /* todo add project_id*/).ToList();
            foreach (var oivGek in oivGeks)
            {
                resultList.AddRange(new[]
                {
                    new DataRow
                    {
                        SpecialistCategory = @"Руководитель ОИВ",
                        Fio = oivGek?.OivFio,
                        WpAndPost = JoinNonEmpty(", ", oivGek?.OivWp, oivGek?.OivPost),
                        Phone = oivGek?.OivPhone1,
                        Email = oivGek?.OivEmail
                    },
                    new DataRow
                    {
                        SpecialistCategory = @"Председатель ГЭК",
                        Fio = oivGek?.GekFio,
                        WpAndPost = JoinNonEmpty(", ", oivGek?.GekWp, oivGek?.GekPost),
                        Phone = oivGek?.GekPhone1,
                        Email = oivGek?.GekEmail
                    },
                    new DataRow
                    {
                        SpecialistCategory = @"Ответственный за ГИА в субъекте",
                        Fio = oivGek?.GiaFio,
                        WpAndPost = JoinNonEmpty(", ", oivGek?.GiaWp, oivGek?.GiaPost),
                        Phone = oivGek?.GiaPhone1,
                        Email = oivGek?.GiaEmail
                    },
                    new DataRow
                    {
                        SpecialistCategory = @"Сотрудник обрнадзора",
                        Fio = oivGek?.NadzorFio,
                        WpAndPost = JoinNonEmpty(", ", oivGek?.NadzorWp, oivGek?.NadzorPost),
                        Phone = oivGek?.NadzorPhone1,
                        Email = oivGek?.NadzorEmail
                    }
                });
            }

            var rcois = unitOfWork.Rcois;
            resultList.AddRange(
                rcois.Where(r => r.RegionId == regionId)
                    .ToList()
                    .Select(rcoi => new DataRow
                    {
                        SpecialistCategory = @"Руководитель РЦОИ",
                        Fio = rcoi?.ChiefFio,
                        WpAndPost = JoinNonEmpty(", ", rcoi?.Name, rcoi?.ChiefPost),
                        Phone = rcoi?.ChiefPhone1,
                        Email = rcoi?.Email
                    }));

            var schedules = unitOfWork.Schedules;
            resultList.AddRange(schedules.Where(s => s.RegionId == regionId && s.Expert.ProjectId == currProject)
                .Select(s => s.Expert)
                .Distinct()
                .ToList()
                // это нужно, чтобы заработали калькулирумые свойства типа FullName и вызовы сторонних методов типа JoinNonEmpty
                .Dump("experts")
                .Select(expert => new DataRow
                {
                    SpecialistCategory = @"Эксперт-член ГЭК",
                    Fio = expert.FullName,
                    WpAndPost = JoinNonEmpty(", ", expert.Workplace, expert.Post),
                    Phone = expert.Phone,
                    Email = expert.Email
                }));

            bindingSource.DataSource = resultList;
        }
    }
}