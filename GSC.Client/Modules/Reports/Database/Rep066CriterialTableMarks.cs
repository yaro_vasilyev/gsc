﻿using System;
using System.Linq;
using DevExpress.XtraReports.UI;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.Database
{
    public partial class Rep066CriterialTableMarks : GscReport
    {
        public Rep066CriterialTableMarks()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep066CriterialTableMarks(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        protected override void OnDataSourceDemanded(EventArgs ea)
        {
            base.OnDataSourceDemanded(ea);

            // param
            var expertKind = ExpertKind.Value as short?;
            var currProj = ViewModel.CurrentProject.Id;

            var experts =
                ViewModel.UnitOfWork.Experts.Where(
                        e =>
                            e.ProjectId == currProj && (expertKind == 0 || expertKind == (short?) e.Kind) &&
                            e.Engage == BooleanType.Yes)
                    .ToList();
            bindingSource1.DataSource = experts;
        }

        private void CanMissionStr_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = BooleanType.Yes.Equals(expert?.CanMission) ? "Да" : "Нет";
        }

        private void InterviewStatusStr_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = InterviewStatus.Success.Equals(expert?.InterviewStatus) ? "Прошёл" : "Не прошёл";
        }

        private void EngageStr_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            e.Value = BooleanType.Yes.Equals(expert?.Engage) ? "Да" : "Нет";
        }

        private void MRDocReports_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            //e.Value = expert?.Schedules.AsQueryable().Sum(s => s.MrDocs.Count).Dump("Expert MR docs"); // этот путь порождает много запросов и выборку блоба
            e.Value = ViewModel.UnitOfWork.MrDocs.Count(md => md.Schedule.ExpertId == expert.Id).Dump("From MR Doc");
        }

        private void EfficiencyStr_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            var efficiency = expert?.Efficiency;
            switch (efficiency)
            {
                case ExpertEfficiency.None:
                    e.Value = "Отсутствие эффективности";
                    break;
                case ExpertEfficiency.Low:
                    e.Value = "Низкая";
                    break;
                case ExpertEfficiency.Mid:
                    e.Value = "Средняя";
                    break;
                case ExpertEfficiency.High:
                    e.Value = "Высокая";
                    break;
                case null:
                    e.Value = "";
                    break;
                default:
                    e.Value = efficiency.ToString();
                    break;
            }
        }

        private void NextPeriodProposalStr_GetValue(object sender, GetValueEventArgs e)
        {
            var expert = e.Row as Expert;
            var proposal = expert?.NextPeriodProposal;
            switch (proposal)
            {
                case ProposalType.Main:
                    e.Value = "Основной";
                    break;
                case ProposalType.Reserve:
                    e.Value = "Резерв";
                    break;
                case ProposalType.Exclude:
                    e.Value = "Исключить";
                    break;
                case null:
                    e.Value = "";
                    break;
                default:
                    e.Value = proposal.ToString();
                    break;
            }
        }
    }
}