﻿using System;
using JetBrains.Annotations;
using System.Linq;
using GSC.Client.ViewModels.Reports;

namespace GSC.Client.Modules.Reports.CallCenter
{
    public partial class Rep108CountCallOfRegions : GscReport
    {
        public Rep108CountCallOfRegions()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep108CountCallOfRegions(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        public class Rep108CountCallOfRegionsReportRow
        {
            public int numpp { get; set; }
            public string region { get; set; }
            public int cnt { get; set; }
            public float percent { get; set; }
        }

        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            var currProject = ViewModel.CurrentProject.Id;

            //todo перенести в БД или в LINQ
            var data = ViewModel.UnitOfWork.Context.Database.SqlQuery<Rep108CountCallOfRegionsReportRow>(@"
select * 
  from gsc.r_gsc108_count_calls_regions (:p0) 
    as f (numpp, region, cnt, percent)", currProject).ToList();
            bindingSource1.DataSource = data;
        }

    }
}
