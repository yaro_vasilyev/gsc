﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Reports.CallCenter
{
    public partial class Rep167CallCentSpecDisp : GscReport
    {
        public Rep167CallCentSpecDisp()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep167CallCentSpecDisp(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        private class DataRow
        {
            public string User { get; set; }
            public int Sms { get; set; }
            public int Mobile { get; set; }
            public int Email { get; set; }
            public int Phone { get; set; }
        }

        private void Rep167CallCentSpecDisp_DataSourceDemanded(object sender, EventArgs ea)
        {
            var currProj = ViewModel.CurrentProject.Id;
            var events = ViewModel.UnitOfWork.Events;
            var observers = ViewModel.UnitOfWork.Observers;
            var logins = ViewModel.UnitOfWork.Logins;

            var observersTask = observers.Where(o => o.UserGuid != null && o.ProjectId == ViewModel.CurrentProject.Id).Select(o => new
            {
                o.UserGuid,
                o.Fio
            }).ToDictionaryAsync(x => x.UserGuid, x => x.Fio).Dump("Кэш ФИО менеджеров по гуиду");
            var loginsTask = logins.Select(l => new
            {
                l.Guid,
                l.UserLogin
            }).Distinct().ToDictionaryAsync(x => x.Guid, x => x.UserLogin).Dump("Кэш логинов пользователей");
            // TODO: get rid of DISTINCT here
            Task.WaitAll(observersTask, loginsTask);
            var observersDict = observersTask.Result;
            var loginsDict = loginsTask.Result;

            var query = events
                .Where(e => e.Schedule.Expert.ProjectId == currProj)
                .GroupBy(e => e.UserGuid ?? Guid.Empty)
                .Select(eGug => new
                {
                    UserGuid = eGug.Key,
                    Sms = eGug.Count(e => e.Channel == ChannelType.SMS),
                    Mobile = eGug.Count(e => e.Channel == ChannelType.Mobile),
                    Email = eGug.Count(e => e.Channel == ChannelType.Email),
                    Phone = eGug.Count(e => e.Channel == ChannelType.Phone)
                })
                .AsEnumerable() // надо инстанциировать запрос тут, иначе ниже не будут работать observers и logins
                .Select(x => new DataRow
                {
                    User =
                        x.UserGuid == Guid.Empty
                            ? "(Пользователь не указан)"
                            : observersDict.ContainsKey(x.UserGuid)
                                ? observersDict[x.UserGuid] ?? "(Имя менеджера не указано)"
                                : $"(Пользователь с логином {(loginsDict.ContainsKey(x.UserGuid) ? loginsDict[x.UserGuid] : x.UserGuid.ToString())})",
                    Sms = x.Sms,
                    Mobile = x.Mobile,
                    Email = x.Email,
                    Phone = x.Phone
                }).ToList().Dump("Итого");

            bindingSource1.DataSource = query;
        }
    }
}