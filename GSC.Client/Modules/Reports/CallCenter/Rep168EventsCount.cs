﻿using System;
using System.Drawing.Printing;
using System.Linq;
using GSC.Client.ViewModels.Reports;
using GSC.Model;
using JetBrains.Annotations;
using log4net;

namespace GSC.Client.Modules.Reports.CallCenter
{
    public partial class Rep168EventsCount : GscReport
    {
        private readonly ILog log = LogManager.GetLogger(typeof (Rep168EventsCount));

        public Rep168EventsCount()
        {
            InitializeComponent();
        }

        [UsedImplicitly]
        public Rep168EventsCount(ReportsViewModel reportsViewModel) : base(reportsViewModel)
        {
            InitializeComponent();
        }

        private class DataRow
        {
            public string Region { get; set; }
            public int EventsCount { get; set; }
            public decimal Percent { get; set; }
        }

        private void Rep168EventsCount_DataSourceDemanded(object sender, EventArgs e)
        {
            var unitOfWork = ViewModel.UnitOfWork;
            var regions = unitOfWork.Regions;
            var currProj = ViewModel.CurrentProject.Id;
            var channelType = (int) parChannelType.Value;

            var query = regions.Select(region => new
            {
                Region = region.Name,
                EventsCount =
                    region.Schedules.Where(schedule => schedule.Expert.ProjectId == currProj)
                        .Sum(
                            schedule =>
                                schedule.Events.Count(
                                    @event => channelType == 0 || (int) @event.Channel == channelType))
            }).OrderBy(row => row.Region).ToList();
            var allQty = query.Where(row => row.EventsCount > 0).Sum(row => row.EventsCount);
            var results = query.Select(row => new DataRow
            {
                Region = row.Region,
                EventsCount = row.EventsCount,
                Percent = allQty != 0 ? (decimal) row.EventsCount / allQty : 0
            });

            bindingSource1.DataSource = results.ToList();
        }

        private void tcQtyHead_BeforePrint(object sender, PrintEventArgs e)
        {
            var headText = GetHeadText(ChannelType);
            var cellText = $"Количество {headText}";
            tcQtyHead.Text = cellText;
        }

        private void tcPercHead_BeforePrint(object sender, PrintEventArgs e)
        {
            var headText = GetHeadText(ChannelType);
            var cellText = $"% от общего числа {headText}";
            tcPercHead.Text = cellText;
        }

        private static string GetHeadText(ChannelType channelType)
        {
            string headText;
            switch (channelType)
            {
                case ChannelType.SMS:
                    headText = "sms-сообщений";
                    break;
                case ChannelType.Mobile:
                    headText = "обращений по сотовому телефону";
                    break;
                case ChannelType.Email:
                    headText = "обращений по электронной почте";
                    break;
                case ChannelType.Phone:
                    headText = "телефонных обращений";
                    break;
                default:
                    headText = "обращений";
                    break;
            }
            return headText;
        }

        private ChannelType ChannelType
        {
            get
            {
                ChannelType channelType;
                var parValue = parChannelType.Value;
                try
                {
                    channelType = (ChannelType) (int) parValue;
                }
                catch (Exception exception)
                {
                    channelType = 0;
                    log?.Error($"Unknown channel type: {parValue}. Using 0 as all types for report. Error: {exception}");
                }
                return channelType;
            }
        }
    }
}