﻿namespace GSC.Client.Modules.CallCenter
{
    partial class EventView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddNewResult = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgMonitorResults = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.eventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lookupRegion = new DevExpress.XtraEditors.LookUpEdit();
            this.regionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TimestampDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.radioChannel = new DevExpress.XtraEditors.RadioGroup();
            this.radioType = new DevExpress.XtraEditors.RadioGroup();
            this.lookupSchedules = new DevExpress.XtraEditors.LookUpEdit();
            this.scheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radioObjectType = new DevExpress.XtraEditors.RadioGroup();
            this.UserGuidLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.observerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForObjectType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChannel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForScheduleId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimestamp = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserGuid = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookupRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimestampDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimestampDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSchedules.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioObjectType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserGuidLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.observerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForScheduleId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimestamp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserGuid)).BeginInit();
            this.SuspendLayout();
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "Close", this.bbiClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventViewModel), "AddNewMonitorResult", this.bbiAddNewResult)});
            this.mvvmContext1.ContainerControl = this;
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.CallCenter.EventViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Созранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // bbiAddNewResult
            // 
            this.bbiAddNewResult.Caption = "Добавить результат";
            this.bbiAddNewResult.Id = 9;
            this.bbiAddNewResult.ImageUri.Uri = "AddItem";
            this.bbiAddNewResult.Name = "bbiAddNewResult";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose,
            this.bbiAddNewResult});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 10;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(743, 141);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel,
            this.rpgMonitorResults});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // rpgMonitorResults
            // 
            this.rpgMonitorResults.ItemLinks.Add(this.bbiAddNewResult);
            this.rpgMonitorResults.Name = "rpgMonitorResults";
            this.rpgMonitorResults.Text = "Мониторинг";
            // 
            // eventBindingSource
            // 
            this.eventBindingSource.DataSource = typeof(GSC.Model.Event);
            this.eventBindingSource.DataSourceChanged += new System.EventHandler(this.eventBindingSource_DataSourceChanged);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.lookupRegion);
            this.dataLayoutControl1.Controls.Add(this.TimestampDateEdit);
            this.dataLayoutControl1.Controls.Add(this.radioChannel);
            this.dataLayoutControl1.Controls.Add(this.radioType);
            this.dataLayoutControl1.Controls.Add(this.lookupSchedules);
            this.dataLayoutControl1.Controls.Add(this.radioObjectType);
            this.dataLayoutControl1.Controls.Add(this.UserGuidLookUpEdit);
            this.dataLayoutControl1.DataSource = this.eventBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(540, 269, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(743, 357);
            this.dataLayoutControl1.TabIndex = 3;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // lookupRegion
            // 
            this.lookupRegion.Location = new System.Drawing.Point(139, 36);
            this.lookupRegion.MenuManager = this.ribbonControl1;
            this.lookupRegion.Name = "lookupRegion";
            this.lookupRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupRegion.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Код", 40, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Субъект РФ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookupRegion.Properties.DataSource = this.regionBindingSource;
            this.lookupRegion.Properties.DisplayMember = "Name";
            this.lookupRegion.Properties.NullText = "<Не выбрано>";
            this.lookupRegion.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupRegion.Properties.ValueMember = "Id";
            this.lookupRegion.Size = new System.Drawing.Size(592, 20);
            this.lookupRegion.StyleController = this.dataLayoutControl1;
            this.lookupRegion.TabIndex = 9;
            // 
            // regionBindingSource
            // 
            this.regionBindingSource.DataSource = typeof(GSC.Model.Region);
            // 
            // TimestampDateEdit
            // 
            this.TimestampDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.eventBindingSource, "Timestamp", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TimestampDateEdit.EditValue = null;
            this.TimestampDateEdit.Location = new System.Drawing.Point(139, 12);
            this.TimestampDateEdit.MenuManager = this.ribbonControl1;
            this.TimestampDateEdit.Name = "TimestampDateEdit";
            this.TimestampDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.TimestampDateEdit.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.TimestampDateEdit.Properties.DisplayFormat.FormatString = "g";
            this.TimestampDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimestampDateEdit.Properties.Mask.EditMask = "g";
            this.TimestampDateEdit.Size = new System.Drawing.Size(592, 20);
            this.TimestampDateEdit.StyleController = this.dataLayoutControl1;
            this.TimestampDateEdit.TabIndex = 4;
            // 
            // radioChannel
            // 
            this.radioChannel.AutoSizeInLayoutControl = true;
            this.radioChannel.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.eventBindingSource, "Channel", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.radioChannel.Location = new System.Drawing.Point(139, 118);
            this.radioChannel.MenuManager = this.ribbonControl1;
            this.radioChannel.Name = "radioChannel";
            this.radioChannel.Properties.Columns = 4;
            this.radioChannel.Size = new System.Drawing.Size(592, 13);
            this.radioChannel.StyleController = this.dataLayoutControl1;
            this.radioChannel.TabIndex = 5;
            // 
            // radioType
            // 
            this.radioType.AutoSizeInLayoutControl = true;
            this.radioType.Location = new System.Drawing.Point(139, 84);
            this.radioType.MenuManager = this.ribbonControl1;
            this.radioType.Name = "radioType";
            this.radioType.Properties.Columns = 4;
            this.radioType.Size = new System.Drawing.Size(592, 13);
            this.radioType.StyleController = this.dataLayoutControl1;
            this.radioType.TabIndex = 6;
            // 
            // lookupSchedules
            // 
            this.lookupSchedules.Location = new System.Drawing.Point(139, 60);
            this.lookupSchedules.MenuManager = this.ribbonControl1;
            this.lookupSchedules.Name = "lookupSchedules";
            this.lookupSchedules.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupSchedules.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExpertFullName", "Эксперт", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PeriodType", "Этап", 50, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MonitorRcoi", "Мониторинг РЦОИ", 50, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lookupSchedules.Properties.DataSource = this.scheduleBindingSource;
            this.lookupSchedules.Properties.DisplayMember = "ExpertFullName";
            this.lookupSchedules.Properties.NullText = "<Не выбрано>";
            this.lookupSchedules.Properties.NullValuePrompt = "<Не выбрано>";
            this.lookupSchedules.Properties.ValueMember = "Id";
            this.lookupSchedules.Size = new System.Drawing.Size(592, 20);
            this.lookupSchedules.StyleController = this.dataLayoutControl1;
            this.lookupSchedules.TabIndex = 7;
            // 
            // scheduleBindingSource
            // 
            this.scheduleBindingSource.DataSource = typeof(GSC.Model.Schedule);
            // 
            // radioObjectType
            // 
            this.radioObjectType.AutoSizeInLayoutControl = true;
            this.radioObjectType.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.eventBindingSource, "ObjectType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.radioObjectType.Location = new System.Drawing.Point(139, 101);
            this.radioObjectType.MenuManager = this.ribbonControl1;
            this.radioObjectType.Name = "radioObjectType";
            this.radioObjectType.Properties.Columns = 4;
            this.radioObjectType.Size = new System.Drawing.Size(592, 13);
            this.radioObjectType.StyleController = this.dataLayoutControl1;
            this.radioObjectType.TabIndex = 8;
            // 
            // UserGuidLookUpEdit
            // 
            this.UserGuidLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.eventBindingSource, "UserGuid", true));
            this.UserGuidLookUpEdit.Location = new System.Drawing.Point(139, 135);
            this.UserGuidLookUpEdit.MenuManager = this.ribbonControl1;
            this.UserGuidLookUpEdit.Name = "UserGuidLookUpEdit";
            this.UserGuidLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.UserGuidLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UserGuidLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fio", "Менеджер", 24, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.UserGuidLookUpEdit.Properties.DataSource = this.observerBindingSource;
            this.UserGuidLookUpEdit.Properties.DisplayMember = "Fio";
            this.UserGuidLookUpEdit.Properties.NullText = "";
            this.UserGuidLookUpEdit.Properties.ValueMember = "UserGuid";
            this.UserGuidLookUpEdit.Size = new System.Drawing.Size(592, 20);
            this.UserGuidLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserGuidLookUpEdit.TabIndex = 10;
            // 
            // observerBindingSource
            // 
            this.observerBindingSource.DataSource = typeof(GSC.Model.Observer);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(743, 357);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForType,
            this.ItemForObjectType,
            this.ItemForChannel,
            this.ItemForScheduleId,
            this.layoutControlItem1,
            this.ItemForTimestamp,
            this.ItemForUserGuid});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 100D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1});
            rowDefinition1.Height = 24D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 24D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 24D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition4.Height = 17D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition5.Height = 17D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition6.Height = 17D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition7.Height = 24D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition8.Height = 100D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7,
            rowDefinition8});
            this.layoutControlGroup2.Size = new System.Drawing.Size(723, 337);
            // 
            // ItemForType
            // 
            this.ItemForType.Control = this.radioType;
            this.ItemForType.Location = new System.Drawing.Point(0, 72);
            this.ItemForType.Name = "ItemForType";
            this.ItemForType.OptionsTableLayoutItem.RowIndex = 3;
            this.ItemForType.Size = new System.Drawing.Size(723, 17);
            this.ItemForType.StartNewLine = true;
            this.ItemForType.Text = "Тип обращения:";
            this.ItemForType.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForObjectType
            // 
            this.ItemForObjectType.Control = this.radioObjectType;
            this.ItemForObjectType.CustomizationFormText = "Объект мониторинга";
            this.ItemForObjectType.Location = new System.Drawing.Point(0, 89);
            this.ItemForObjectType.Name = "ItemForObjectType";
            this.ItemForObjectType.OptionsTableLayoutItem.RowIndex = 4;
            this.ItemForObjectType.Size = new System.Drawing.Size(723, 17);
            this.ItemForObjectType.StartNewLine = true;
            this.ItemForObjectType.Text = "Объект мониторинга:";
            this.ItemForObjectType.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForChannel
            // 
            this.ItemForChannel.Control = this.radioChannel;
            this.ItemForChannel.CustomizationFormText = "Способ обращения";
            this.ItemForChannel.Location = new System.Drawing.Point(0, 106);
            this.ItemForChannel.Name = "ItemForChannel";
            this.ItemForChannel.OptionsTableLayoutItem.RowIndex = 5;
            this.ItemForChannel.Size = new System.Drawing.Size(723, 17);
            this.ItemForChannel.StartNewLine = true;
            this.ItemForChannel.Text = "Способ обращения:";
            this.ItemForChannel.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForScheduleId
            // 
            this.ItemForScheduleId.Control = this.lookupSchedules;
            this.ItemForScheduleId.CustomizationFormText = "Эксперт";
            this.ItemForScheduleId.Location = new System.Drawing.Point(0, 48);
            this.ItemForScheduleId.Name = "ItemForScheduleId";
            this.ItemForScheduleId.OptionsTableLayoutItem.RowIndex = 2;
            this.ItemForScheduleId.Size = new System.Drawing.Size(723, 24);
            this.ItemForScheduleId.Text = "Эксперт:";
            this.ItemForScheduleId.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lookupRegion;
            this.layoutControlItem1.CustomizationFormText = "Субъект РФ";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem1.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem1.Text = "Субъект РФ:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForTimestamp
            // 
            this.ItemForTimestamp.Control = this.TimestampDateEdit;
            this.ItemForTimestamp.CustomizationFormText = "Дата/время обращения";
            this.ItemForTimestamp.Location = new System.Drawing.Point(0, 0);
            this.ItemForTimestamp.Name = "ItemForTimestamp";
            this.ItemForTimestamp.Size = new System.Drawing.Size(723, 24);
            this.ItemForTimestamp.Text = "Дата/время обращения:";
            this.ItemForTimestamp.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForUserGuid
            // 
            this.ItemForUserGuid.Control = this.UserGuidLookUpEdit;
            this.ItemForUserGuid.Location = new System.Drawing.Point(0, 123);
            this.ItemForUserGuid.Name = "ItemForUserGuid";
            this.ItemForUserGuid.OptionsTableLayoutItem.RowIndex = 6;
            this.ItemForUserGuid.Size = new System.Drawing.Size(723, 24);
            this.ItemForUserGuid.Text = "Оператор";
            this.ItemForUserGuid.TextSize = new System.Drawing.Size(124, 13);
            // 
            // EventView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "EventView";
            this.Size = new System.Drawing.Size(743, 498);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookupRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimestampDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimestampDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSchedules.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioObjectType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserGuidLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.observerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForScheduleId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimestamp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserGuid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private System.Windows.Forms.BindingSource eventBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraEditors.RadioGroup radioChannel;
        private DevExpress.XtraEditors.RadioGroup radioType;
        private DevExpress.XtraEditors.LookUpEdit lookupSchedules;
        private DevExpress.XtraEditors.RadioGroup radioObjectType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForObjectType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForScheduleId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChannel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimestamp;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit TimestampDateEdit;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lookupRegion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMonitorResults;
        private DevExpress.XtraBars.BarButtonItem bbiAddNewResult;
        private System.Windows.Forms.BindingSource regionBindingSource;
        private System.Windows.Forms.BindingSource scheduleBindingSource;
        private System.Windows.Forms.BindingSource observerBindingSource;
        private DevExpress.XtraEditors.LookUpEdit UserGuidLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserGuid;
    }
}
