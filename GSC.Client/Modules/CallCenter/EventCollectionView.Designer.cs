﻿namespace GSC.Client.Modules.CallCenter
{
    partial class EventCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.rpEvents = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgActions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiView = new DevExpress.XtraBars.BarButtonItem();
            this.rpgExchange = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.eventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTimestamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleExpertFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperator = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiExportToExcel,
            this.bbiView});
            this.ribbonControl1.MaxItemId = 7;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpEvents});
            this.ribbonControl1.Size = new System.Drawing.Size(817, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel), "Refresh", this.bbiRefresh),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel), "New", this.bbiNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel), "Edit", "SelectedEntity", this.bbiEdit),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel), "Delete", "SelectedEntity", this.bbiDelete),
            DevExpress.Utils.MVVM.BindingExpression.CreateParameterizedCommandBinding(typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel), "View", "SelectedEntity", this.bbiView)});
            this.mvvmContext1.RegistrationExpressions.AddRange(new DevExpress.Utils.MVVM.RegistrationExpression[] {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterWindowedDocumentManagerService(null, false, this, DevExpress.Utils.MVVM.Services.DefaultWindowedDocumentManagerServiceType.RibbonForm, null)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.CallCenter.EventCollectionViewModel);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Создать";
            this.bbiNew.Id = 2;
            this.bbiNew.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Изменить";
            this.bbiEdit.Id = 3;
            this.bbiEdit.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.Tag = "RS.Edit";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Удалить";
            this.bbiDelete.Id = 4;
            this.bbiDelete.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Экспорт в Excel";
            this.bbiExportToExcel.Id = 5;
            this.bbiExportToExcel.ImageUri.Uri = "ExportToXLSX";
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            // 
            // rpEvents
            // 
            this.rpEvents.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgActions,
            this.rpgExchange});
            this.rpEvents.Name = "rpEvents";
            this.rpEvents.Text = "Обращения";
            // 
            // rpgActions
            // 
            this.rpgActions.ItemLinks.Add(this.bbiNew);
            this.rpgActions.ItemLinks.Add(this.bbiEdit);
            this.rpgActions.ItemLinks.Add(this.bbiView);
            this.rpgActions.ItemLinks.Add(this.bbiDelete);
            this.rpgActions.ItemLinks.Add(this.bbiRefresh);
            this.rpgActions.Name = "rpgActions";
            this.rpgActions.Text = "Действия";
            // 
            // bbiView
            // 
            this.bbiView.Caption = "Просмотр";
            this.bbiView.Id = 6;
            this.bbiView.ImageUri.Uri = "Show";
            this.bbiView.Name = "bbiView";
            this.bbiView.Tag = "RS.View";
            // 
            // rpgExchange
            // 
            this.rpgExchange.ItemLinks.Add(this.bbiExportToExcel);
            this.rpgExchange.Name = "rpgExchange";
            this.rpgExchange.Text = "Обмен";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.eventBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 141);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(817, 349);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // eventBindingSource
            // 
            this.eventBindingSource.DataSource = typeof(GSC.Model.Event);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTimestamp,
            this.colChannel,
            this.colType,
            this.colScheduleRegionName,
            this.colObjectType,
            this.colScheduleExpertFullName,
            this.colOperator});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // 
            // colTimestamp
            // 
            this.colTimestamp.Caption = "Дата/время обращения";
            this.colTimestamp.DisplayFormat.FormatString = "g";
            this.colTimestamp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimestamp.FieldName = "Timestamp";
            this.colTimestamp.Name = "colTimestamp";
            this.colTimestamp.OptionsColumn.AllowEdit = false;
            this.colTimestamp.Visible = true;
            this.colTimestamp.VisibleIndex = 0;
            this.colTimestamp.Width = 127;
            // 
            // colChannel
            // 
            this.colChannel.Caption = "Способ обращения";
            this.colChannel.FieldName = "Channel";
            this.colChannel.Name = "colChannel";
            this.colChannel.OptionsColumn.AllowEdit = false;
            this.colChannel.Visible = true;
            this.colChannel.VisibleIndex = 1;
            this.colChannel.Width = 110;
            // 
            // colType
            // 
            this.colType.Caption = "Тип обращения";
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowEdit = false;
            this.colType.Visible = true;
            this.colType.VisibleIndex = 2;
            this.colType.Width = 95;
            // 
            // colScheduleRegionName
            // 
            this.colScheduleRegionName.Caption = "Субъект РФ";
            this.colScheduleRegionName.FieldName = "Schedule.Region.Name";
            this.colScheduleRegionName.Name = "colScheduleRegionName";
            this.colScheduleRegionName.OptionsColumn.AllowEdit = false;
            this.colScheduleRegionName.Visible = true;
            this.colScheduleRegionName.VisibleIndex = 3;
            this.colScheduleRegionName.Width = 199;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Объект мониторинга";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 4;
            this.colObjectType.Width = 117;
            // 
            // colScheduleExpertFullName
            // 
            this.colScheduleExpertFullName.Caption = "Эксперт";
            this.colScheduleExpertFullName.FieldName = "Schedule.Expert.FullName";
            this.colScheduleExpertFullName.Name = "colScheduleExpertFullName";
            this.colScheduleExpertFullName.OptionsColumn.AllowEdit = false;
            this.colScheduleExpertFullName.Visible = true;
            this.colScheduleExpertFullName.VisibleIndex = 5;
            this.colScheduleExpertFullName.Width = 190;
            // 
            // colOperator
            // 
            this.colOperator.Caption = "Оператор";
            this.colOperator.FieldName = "UserFio.Fio";
            this.colOperator.Name = "colOperator";
            this.colOperator.OptionsColumn.AllowEdit = false;
            this.colOperator.Visible = true;
            this.colOperator.VisibleIndex = 6;
            // 
            // EventCollectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "EventCollectionView";
            this.Size = new System.Drawing.Size(817, 490);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage rpEvents;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource eventBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colTimestamp;
        private DevExpress.XtraGrid.Columns.GridColumn colChannel;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleExpertFullName;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgActions;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgExchange;
        private DevExpress.XtraBars.BarButtonItem bbiView;
        private DevExpress.XtraGrid.Columns.GridColumn colOperator;
    }
}
