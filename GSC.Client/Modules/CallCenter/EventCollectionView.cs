﻿using GSC.Client.Util;
using GSC.Client.ViewModels.CallCenter;

namespace GSC.Client.Modules.CallCenter
{
    public partial class EventCollectionView : CollectionBlockView
    {
        public EventCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<EventCollectionViewModel>();
            fluent.InitGridView(gridView1, ViewModel);
            fluent.SetBinding(gridControl1, c => c.DataSource, vm => vm.Entities);

            bbiExportToExcel.BindCommand(a => ViewModel.ExportToExcel(a),
                             ViewModel,
                             () => new GridAdapter(gridControl1));

        }

        private EventCollectionViewModel ViewModel
        {
            get { return mvvmContext1.GetViewModel<EventCollectionViewModel>(); }
        }
    }
}
