﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.Utils;
using GSC.Client.Util;
using GSC.Client.ViewModels.CallCenter;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Modules.CallCenter
{
    [UsedImplicitly]
    public partial class EventView : BaseEditView
    {
        public EventView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                radioChannel.Properties.Items.AddEnum<ChannelType>();
                radioType.Properties.Items.AddEnum<EventType>();
                radioObjectType.Properties.Items.AddEnum<VisitObjectType>();

                var fluent = mvvmContext1.OfType<EventViewModel>();
                fluent.SetObjectDataSourceBinding(eventBindingSource,
                    vm => vm.Entity,
                    vm => vm.Update());

                fluent.SetBinding(regionBindingSource, bs => bs.DataSource, vm => vm.RegionsLookup.Entities);
                fluent.SetBinding(scheduleBindingSource, bs => bs.DataSource, vm => vm.SchedulesLookup.Entities);
                fluent.SetBinding(observerBindingSource, bs => bs.DataSource, vm => vm.ObserversLookup.Entities);
            }
        }

        private void eventBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            var bs = (BindingSource) sender;
            Debug.Assert(bs.DataSource != null);

            var fluent = mvvmContext1.OfType<EventViewModel>();
            fluent.SetBinding(lookupRegion, c => c.EditValue, vm => vm.RegionId, null, XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(lookupSchedules, c => c.EditValue, vm => vm.ScheduleId, null,
                XtraEditors.ConvertBack<int?>);
            fluent.SetBinding(radioType, c => c.EditValue, vm => vm.Type);
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }
    }
}
