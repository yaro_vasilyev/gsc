﻿namespace GSC.Client.Modules.Contracts
{
    partial class ContractView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGenerateDoc = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportDoc = new DevExpress.XtraBars.BarButtonItem();
            this.rpActions = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCancel = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgDoc = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiEmailContract = new DevExpress.XtraBars.BarButtonItem();
            this.contractBindingSource = new System.Windows.Forms.BindingSource();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.PeriodTypeImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ContractTypeImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.DateFromDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DateToDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SummTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RouteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NumberLookupEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.contractInfoBindingSource = new System.Windows.Forms.BindingSource();
            this.scheduleLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.scheduleBindingSource = new System.Windows.Forms.BindingSource();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpertId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSumm = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoute = new DevExpress.XtraLayout.LayoutControlItem();
            this.richEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodTypeImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSumm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoute)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiClose,
            this.bbiGenerateDoc,
            this.bbiExportDoc,
            this.bbiEmailContract});
            this.ribbonControl1.MaxItemId = 5;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpActions});
            this.ribbonControl1.Size = new System.Drawing.Size(1115, 141);
            // 
            // mvvmContext1
            // 
            this.mvvmContext1.BindingExpressions.AddRange(new DevExpress.Utils.MVVM.BindingExpression[] {
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Save", this.bbiSave),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "SaveAndClose", this.bbiSaveAndClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "SaveAndNew", this.bbiSaveAndNew),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Reset", this.bbiReset),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Expert.ExpertViewModel), "Close", this.bbiClose),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Contract.ContractViewModel), "GenerateDoc", this.bbiGenerateDoc),
            DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(typeof(GSC.Client.ViewModels.Contract.ContractViewModel), "EmailContract", this.bbiEmailContract)});
            this.mvvmContext1.ViewModelType = typeof(GSC.Client.ViewModels.Contract.ContractViewModel);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Id = 1;
            this.bbiSave.ImageUri.Uri = "Save";
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Сохранить и закрыть";
            this.bbiSaveAndClose.Id = 2;
            this.bbiSaveAndClose.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Сохранить и создать";
            this.bbiSaveAndNew.Id = 3;
            this.bbiSaveAndNew.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Отменить изменения";
            this.bbiReset.Id = 4;
            this.bbiReset.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Закрыть";
            this.bbiClose.Id = 8;
            this.bbiClose.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // bbiGenerateDoc
            // 
            this.bbiGenerateDoc.Caption = "Сформировать договор";
            this.bbiGenerateDoc.Id = 2;
            this.bbiGenerateDoc.ImageUri.Uri = "InLineWithText";
            this.bbiGenerateDoc.Name = "bbiGenerateDoc";
            // 
            // bbiExportDoc
            // 
            this.bbiExportDoc.Caption = "Экспортировать договор";
            this.bbiExportDoc.Id = 3;
            this.bbiExportDoc.ImageUri.Uri = "ExportFile";
            this.bbiExportDoc.Name = "bbiExportDoc";
            this.bbiExportDoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportDoc_ItemClick);
            // 
            // rpActions
            // 
            this.rpActions.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave,
            this.rpgCancel,
            this.rpgDoc});
            this.rpActions.Name = "rpActions";
            this.rpActions.Text = "Действия";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSave);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Сохранение";
            // 
            // rpgCancel
            // 
            this.rpgCancel.ItemLinks.Add(this.bbiReset);
            this.rpgCancel.ItemLinks.Add(this.bbiClose);
            this.rpgCancel.Name = "rpgCancel";
            this.rpgCancel.Text = "Отмена";
            // 
            // rpgDoc
            // 
            this.rpgDoc.ItemLinks.Add(this.bbiGenerateDoc);
            this.rpgDoc.ItemLinks.Add(this.bbiExportDoc);
            this.rpgDoc.ItemLinks.Add(this.bbiEmailContract);
            this.rpgDoc.Name = "rpgDoc";
            this.rpgDoc.Text = "Документ";
            // 
            // bbiEmailContract
            // 
            this.bbiEmailContract.Caption = "Email Contract";
            this.bbiEmailContract.Id = 4;
            this.bbiEmailContract.ImageUri.Uri = "SendRTF";
            this.bbiEmailContract.Name = "bbiEmailContract";
            // 
            // contractBindingSource
            // 
            this.contractBindingSource.DataSource = typeof(GSC.Model.Contract);
            this.contractBindingSource.DataSourceChanged += new System.EventHandler(this.contractBindingSource_DataSourceChanged);
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiReset);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiClose);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Отмена";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.AutoSize = true;
            this.dataLayoutControl1.Controls.Add(this.PeriodTypeImageComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractTypeImageComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.DateFromDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DateToDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SummTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RouteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NumberLookupEdit);
            this.dataLayoutControl1.Controls.Add(this.scheduleLookUpEdit);
            this.dataLayoutControl1.DataSource = this.contractBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 141);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1115, 68);
            this.dataLayoutControl1.TabIndex = 5;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // PeriodTypeImageComboBoxEdit
            // 
            this.PeriodTypeImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "PeriodType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PeriodTypeImageComboBoxEdit.Location = new System.Drawing.Point(102, 12);
            this.PeriodTypeImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.PeriodTypeImageComboBoxEdit.Name = "PeriodTypeImageComboBoxEdit";
            this.PeriodTypeImageComboBoxEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.PeriodTypeImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PeriodTypeImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PeriodTypeImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PeriodTypeImageComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Досрочный", GSC.Model.PeriodType.Early, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Основной", GSC.Model.PeriodType.Primary, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Дополнительный", GSC.Model.PeriodType.Additional, 2)});
            this.PeriodTypeImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.PeriodTypeImageComboBoxEdit.Size = new System.Drawing.Size(180, 20);
            this.PeriodTypeImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PeriodTypeImageComboBoxEdit.TabIndex = 4;
            // 
            // ContractTypeImageComboBoxEdit
            // 
            this.ContractTypeImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "ContractType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ContractTypeImageComboBoxEdit.Location = new System.Drawing.Point(650, 12);
            this.ContractTypeImageComboBoxEdit.MenuManager = this.ribbonControl1;
            this.ContractTypeImageComboBoxEdit.Name = "ContractTypeImageComboBoxEdit";
            this.ContractTypeImageComboBoxEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ContractTypeImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ContractTypeImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ContractTypeImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractTypeImageComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Договор возмездного оказания услуг", GSC.Model.ContractType.Paid, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Договор безвозмездного оказания услуг", GSC.Model.ContractType.Unpaid, 1)});
            this.ContractTypeImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.ContractTypeImageComboBoxEdit.Size = new System.Drawing.Size(180, 20);
            this.ContractTypeImageComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.ContractTypeImageComboBoxEdit.TabIndex = 6;
            // 
            // DateFromDateEdit
            // 
            this.DateFromDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "DateFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DateFromDateEdit.EditValue = null;
            this.DateFromDateEdit.Location = new System.Drawing.Point(102, 36);
            this.DateFromDateEdit.MenuManager = this.ribbonControl1;
            this.DateFromDateEdit.Name = "DateFromDateEdit";
            this.DateFromDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateFromDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateFromDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateFromDateEdit.Size = new System.Drawing.Size(180, 20);
            this.DateFromDateEdit.StyleController = this.dataLayoutControl1;
            this.DateFromDateEdit.TabIndex = 8;
            // 
            // DateToDateEdit
            // 
            this.DateToDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "DateTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DateToDateEdit.EditValue = null;
            this.DateToDateEdit.Location = new System.Drawing.Point(376, 36);
            this.DateToDateEdit.MenuManager = this.ribbonControl1;
            this.DateToDateEdit.Name = "DateToDateEdit";
            this.DateToDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateToDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateToDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateToDateEdit.Size = new System.Drawing.Size(180, 20);
            this.DateToDateEdit.StyleController = this.dataLayoutControl1;
            this.DateToDateEdit.TabIndex = 9;
            // 
            // SummTextEdit
            // 
            this.SummTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "Summ", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SummTextEdit.Location = new System.Drawing.Point(650, 36);
            this.SummTextEdit.MenuManager = this.ribbonControl1;
            this.SummTextEdit.Name = "SummTextEdit";
            this.SummTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.SummTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SummTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SummTextEdit.Properties.Mask.EditMask = "c";
            this.SummTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SummTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SummTextEdit.Size = new System.Drawing.Size(180, 20);
            this.SummTextEdit.StyleController = this.dataLayoutControl1;
            this.SummTextEdit.TabIndex = 10;
            // 
            // RouteTextEdit
            // 
            this.RouteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "Route", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RouteTextEdit.Location = new System.Drawing.Point(924, 36);
            this.RouteTextEdit.MenuManager = this.ribbonControl1;
            this.RouteTextEdit.Name = "RouteTextEdit";
            this.RouteTextEdit.Size = new System.Drawing.Size(179, 20);
            this.RouteTextEdit.StyleController = this.dataLayoutControl1;
            this.RouteTextEdit.TabIndex = 11;
            // 
            // NumberLookupEdit
            // 
            this.NumberLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "Number", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NumberLookupEdit.Location = new System.Drawing.Point(924, 12);
            this.NumberLookupEdit.MenuManager = this.ribbonControl1;
            this.NumberLookupEdit.Name = "NumberLookupEdit";
            this.NumberLookupEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.NumberLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NumberLookupEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Number", "Номер", 60, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContractDate", "Дата", 78, DevExpress.Utils.FormatType.DateTime, "dd.MM.yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.NumberLookupEdit.Properties.DataSource = this.contractInfoBindingSource;
            this.NumberLookupEdit.Properties.DisplayMember = "NumberDate";
            this.NumberLookupEdit.Properties.NullText = "";
            this.NumberLookupEdit.Properties.ValueMember = "Number";
            this.NumberLookupEdit.Size = new System.Drawing.Size(179, 20);
            this.NumberLookupEdit.StyleController = this.dataLayoutControl1;
            this.NumberLookupEdit.TabIndex = 7;
            // 
            // contractInfoBindingSource
            // 
            this.contractInfoBindingSource.DataSource = typeof(GSC.Model.ContractInfo);
            // 
            // scheduleLookUpEdit
            // 
            this.scheduleLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.contractBindingSource, "ScheduleId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.scheduleLookUpEdit.Location = new System.Drawing.Point(376, 12);
            this.scheduleLookUpEdit.MenuManager = this.ribbonControl1;
            this.scheduleLookUpEdit.Name = "scheduleLookUpEdit";
            this.scheduleLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.scheduleLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.scheduleLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.scheduleLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ExpertFullName", "ФИО эксперта", 91, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RegionName", 90, "Регион"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MonitorRcoi", "Мониторинг РЦОИ", 69, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.scheduleLookUpEdit.Properties.DataSource = this.scheduleBindingSource;
            this.scheduleLookUpEdit.Properties.DisplayMember = "ExpertFullName";
            this.scheduleLookUpEdit.Properties.NullText = "";
            this.scheduleLookUpEdit.Properties.ValueMember = "Id";
            this.scheduleLookUpEdit.Size = new System.Drawing.Size(180, 20);
            this.scheduleLookUpEdit.StyleController = this.dataLayoutControl1;
            this.scheduleLookUpEdit.TabIndex = 5;
            this.scheduleLookUpEdit.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.ScheduleIdTextEdit_Closed);
            // 
            // scheduleBindingSource
            // 
            this.scheduleBindingSource.DataSource = typeof(GSC.Model.Schedule);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1115, 68);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPeriodType,
            this.ItemForExpertId,
            this.ItemForContractType,
            this.ItemForNumber,
            this.ItemForDateFrom,
            this.ItemForDateTo,
            this.ItemForSumm,
            this.ItemForRoute});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1095, 48);
            // 
            // ItemForPeriodType
            // 
            this.ItemForPeriodType.Control = this.PeriodTypeImageComboBoxEdit;
            this.ItemForPeriodType.Location = new System.Drawing.Point(0, 0);
            this.ItemForPeriodType.Name = "ItemForPeriodType";
            this.ItemForPeriodType.Size = new System.Drawing.Size(274, 24);
            this.ItemForPeriodType.Text = "Период";
            this.ItemForPeriodType.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForExpertId
            // 
            this.ItemForExpertId.Control = this.scheduleLookUpEdit;
            this.ItemForExpertId.Location = new System.Drawing.Point(274, 0);
            this.ItemForExpertId.Name = "ItemForExpertId";
            this.ItemForExpertId.Size = new System.Drawing.Size(274, 24);
            this.ItemForExpertId.Text = "График эксперта";
            this.ItemForExpertId.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForContractType
            // 
            this.ItemForContractType.Control = this.ContractTypeImageComboBoxEdit;
            this.ItemForContractType.Location = new System.Drawing.Point(548, 0);
            this.ItemForContractType.Name = "ItemForContractType";
            this.ItemForContractType.Size = new System.Drawing.Size(274, 24);
            this.ItemForContractType.Text = "Вид договора";
            this.ItemForContractType.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForNumber
            // 
            this.ItemForNumber.Control = this.NumberLookupEdit;
            this.ItemForNumber.Location = new System.Drawing.Point(822, 0);
            this.ItemForNumber.Name = "ItemForNumber";
            this.ItemForNumber.Size = new System.Drawing.Size(273, 24);
            this.ItemForNumber.Text = "Номер и дата";
            this.ItemForNumber.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForDateFrom
            // 
            this.ItemForDateFrom.Control = this.DateFromDateEdit;
            this.ItemForDateFrom.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateFrom.Name = "ItemForDateFrom";
            this.ItemForDateFrom.Size = new System.Drawing.Size(274, 24);
            this.ItemForDateFrom.Text = "Дата с";
            this.ItemForDateFrom.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForDateTo
            // 
            this.ItemForDateTo.Control = this.DateToDateEdit;
            this.ItemForDateTo.Location = new System.Drawing.Point(274, 24);
            this.ItemForDateTo.Name = "ItemForDateTo";
            this.ItemForDateTo.Size = new System.Drawing.Size(274, 24);
            this.ItemForDateTo.Text = "Дата по";
            this.ItemForDateTo.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForSumm
            // 
            this.ItemForSumm.Control = this.SummTextEdit;
            this.ItemForSumm.Location = new System.Drawing.Point(548, 24);
            this.ItemForSumm.Name = "ItemForSumm";
            this.ItemForSumm.Size = new System.Drawing.Size(274, 24);
            this.ItemForSumm.Text = "Сумма";
            this.ItemForSumm.TextSize = new System.Drawing.Size(87, 13);
            // 
            // ItemForRoute
            // 
            this.ItemForRoute.Control = this.RouteTextEdit;
            this.ItemForRoute.Location = new System.Drawing.Point(822, 24);
            this.ItemForRoute.Name = "ItemForRoute";
            this.ItemForRoute.Size = new System.Drawing.Size(273, 24);
            this.ItemForRoute.Text = "Маршрут";
            this.ItemForRoute.TextSize = new System.Drawing.Size(87, 13);
            // 
            // richEditControl
            // 
            this.richEditControl.DataBindings.Add(new System.Windows.Forms.Binding("OpenDocumentBytes", this.contractBindingSource, "Doc", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.richEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditControl.Location = new System.Drawing.Point(0, 209);
            this.richEditControl.MenuManager = this.ribbonControl1;
            this.richEditControl.Name = "richEditControl";
            this.richEditControl.ReadOnly = true;
            this.richEditControl.Size = new System.Drawing.Size(1115, 563);
            this.richEditControl.TabIndex = 6;
            // 
            // ContractView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richEditControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "ContractView";
            this.Size = new System.Drawing.Size(1115, 772);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.richEditControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PeriodTypeImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpertId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSumm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoute)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpActions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCancel;
        private System.Windows.Forms.BindingSource contractBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiGenerateDoc;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgDoc;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit PeriodTypeImageComboBoxEdit;
        private DevExpress.XtraEditors.ImageComboBoxEdit ContractTypeImageComboBoxEdit;
        private DevExpress.XtraEditors.DateEdit DateFromDateEdit;
        private DevExpress.XtraEditors.DateEdit DateToDateEdit;
        private DevExpress.XtraEditors.TextEdit SummTextEdit;
        private DevExpress.XtraEditors.TextEdit RouteTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPeriodType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpertId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateFrom;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateTo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSumm;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoute;
        private DevExpress.XtraEditors.LookUpEdit NumberLookupEdit;
        private System.Windows.Forms.BindingSource contractInfoBindingSource;
        private DevExpress.XtraEditors.LookUpEdit scheduleLookUpEdit;
        private System.Windows.Forms.BindingSource scheduleBindingSource;
        private DevExpress.XtraBars.BarButtonItem bbiExportDoc;
        private DevExpress.XtraBars.BarButtonItem bbiEmailContract;
    }
}
