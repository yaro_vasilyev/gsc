﻿using GSC.Client.Util;
using GSC.Client.ViewModels.Contract;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Contracts
{
    [UsedImplicitly]
    public partial class ContractCollectionView : CollectionBlockView
    {
        public ContractCollectionView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                InitBindings();
            }
        }
        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ContractCollectionViewModel>();
            fluent.InitGridView(gridView, fluent.ViewModel);
            fluent.SetBinding(gridControl, control => control.DataSource, viewModel => viewModel.Entities);
        }
    }
}
