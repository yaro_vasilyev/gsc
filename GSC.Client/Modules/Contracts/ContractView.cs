﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using GSC.Client.Util;
using GSC.Client.ViewModels.Contract;
using JetBrains.Annotations;

namespace GSC.Client.Modules.Contracts
{
    [UsedImplicitly]
    public partial class ContractView : BaseEditView, IGenerateDocumentService
    {
        public ContractView()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                mvvmContext1.RegisterService(this);

                var fluent = mvvmContext1.OfType<ContractViewModel>();
                fluent.SetObjectDataSourceBinding(contractBindingSource,
                    contractViewModel => contractViewModel.Entity,
                    contractViewModel => contractViewModel.Update());
            }
        }

        private void contractBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            InitBindings();
        }

        private void InitBindings()
        {
            var fluent = mvvmContext1.OfType<ContractViewModel>();
            fluent.SetBinding(contractInfoBindingSource, source => source.DataSource,
                model => model.LookupUnusedContractInfos.Entities);
            fluent.SetBinding(scheduleBindingSource, source => source.DataSource,
                model => model.LookupSchedules.Entities);

            /*fluent.SetBinding(scheduleLookUpEdit, lookUpEdit => lookUpEdit.EditValue,
                viewModel => viewModel.Entity.ScheduleId, scheduleId => scheduleId == 0 ? (object) null : scheduleId,
                editValue => (int?) editValue ?? 0);*/

            // trigger refilter lookups
            fluent.SetBinding(PeriodTypeImageComboBoxEdit, edit => edit.EditValue, model => model.PeriodType);

            #region New/Edit form status

            fluent.SetBinding(PeriodTypeImageComboBoxEdit, edit => edit.ReadOnly, model => model.IsEditMode);
            fluent.SetBinding(scheduleLookUpEdit, edit => edit.ReadOnly, model => model.IsEditMode);
            fluent.SetBinding(NumberLookupEdit, edit => edit.ReadOnly, model => model.IsEditMode);

            #endregion
        }

        void IGenerateDocumentService.GenerateDocument(Action<Func<IRichEditDocumentServer>, Func<Document>> generateDocFunc)
        {
            generateDocFunc(() => richEditControl.CreateDocumentServer(), () => richEditControl.Document);
        }

        private void bbiExportDoc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //todo move to IGenerateDocumentService and rename it to IBusinessDocumentService
            richEditControl.SaveDocumentAs(this);
        }

        private void ScheduleIdTextEdit_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            if (e.CloseMode == PopupCloseMode.Normal &&
                XtraMessageBox.Show(this, "Заполнить период мониторинга автоматически?", "Автозаполнение дат",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // calculate monitoring dates as one day more than the first and the last schedule days
                var schedule = scheduleLookUpEdit.GetSelectedDataRow() as Model.Schedule;
                if (schedule != null)
                {
                    var dateTimes = schedule.ScheduleDetails.Select(detail => detail.ExamDate).DefaultIfEmpty(DateTime.Now).ToList();
                    var minDate = dateTimes.Min();
                    var maxDate = dateTimes.Max();
                    DateFromDateEdit.DateTime = minDate.AddDays(-1);
                    DateToDateEdit.DateTime = maxDate.AddDays(1);
                }
            }
        }

        protected override void SetViewReadOnly(bool readOnly)
        {
            dataLayoutControl1.OptionsView.IsReadOnly = readOnly ? DefaultBoolean.True : DefaultBoolean.Default;
        }

        string IGenerateDocumentService.ExportDocument(string fileNameNoPath, DocumentFormat format, bool compress)
        {
            if (string.IsNullOrEmpty(fileNameNoPath))
                throw new ArgumentNullException(nameof(fileNameNoPath));
            if (!(format == DocumentFormat.OpenXml || format == DocumentFormat.Rtf))
                throw new ArgumentOutOfRangeException(nameof(format));

            var tempFileName = Path.GetTempFileName();
            richEditControl.SaveDocument(tempFileName, format);
            var tempDirInfo = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()));
            var destFileName = Path.Combine(tempDirInfo.FullName, fileNameNoPath);
            File.Move(tempFileName, destFileName);
            var retPath = destFileName;

            if (compress)
            {
                var compressedPath = Path.ChangeExtension(destFileName, Path.GetExtension(destFileName) + ".zip");
                using (var targetStream = File.OpenWrite(compressedPath))
                {
                    using (var gzip = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        using (var srcStream = File.OpenRead(destFileName))
                        {
                            srcStream.CopyTo(gzip);
                        }
                    }
                }

                retPath = compressedPath;
            }

            return retPath;
        }
    }
}