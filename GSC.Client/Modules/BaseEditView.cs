﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules
{
    /// <summary>
    /// Базовый класс форм редактирования. Определяет (пока только) поведение контролов при валидации.
    /// </summary>
    public partial class BaseEditView : XtraUserControl
    {
        protected BaseEditView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            UpdateReadOnlyStatus(mvvmContext1.GetViewModel<object>());
        }

        private void UpdateReadOnlyStatus(object viewModel)
        {
            if (!DesignMode)
            {
                var supportReadOnlyViewModel = viewModel as ISupportReadOnlyViewModel;
                if (supportReadOnlyViewModel != null)
                    SetViewReadOnly(supportReadOnlyViewModel.ReadOnly);
            }
        }

        protected virtual void SetViewReadOnly(bool readOnly)
        {
        }

        private void mvvmContext1_ViewModelSet(object sender, DevExpress.Utils.MVVM.ViewModelSetEventArgs e)
        {
            UpdateReadOnlyStatus(e.ViewModel);
        }
    }
}
