﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using GSC.Client.ViewModels;

namespace GSC.Client.Modules
{
    public partial class CollectionBlockView : BaseBlockView, IRibbonSource
    {
        public CollectionBlockView()
        {
            InitializeComponent();
        }

        public RibbonControl Ribbon => ribbonControl1;

        public virtual BarButtonItem EditButton
        {
            get { return FindRibbonItemByTag("RS.Edit") as BarButtonItem; }
        }

        public virtual BarButtonItem ViewButton
        {
            get { return FindRibbonItemByTag("RS.View") as BarButtonItem; }
        }

        protected virtual BarItem FindRibbonItemByTag(string tag)
        {
            return ribbonControl1.Items.OfType<BarItem>().FirstOrDefault(bbi => Equals(bbi.Tag, tag));
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Application.Idle += OnApplicationIdle;

            Disposed += (sender, args) => Application.Idle -= OnApplicationIdle;
        }


        private void OnApplicationIdle(object sender, EventArgs eventArgs)
        {
            if (!DesignMode)
            {
                var supportReadOnlyViewModel = mvvmContext1.GetViewModel<object>() as ISupportReadOnlyViewModel;
                if (supportReadOnlyViewModel != null)
                {
                    if (EditButton != null)
                    {
                        EditButton.Visibility = supportReadOnlyViewModel.ReadOnly
                            ? BarItemVisibility.Never
                            : BarItemVisibility.Always;
                    }
                    if (ViewButton != null)
                    {
                        ViewButton.Visibility = supportReadOnlyViewModel.ReadOnly
                            ? BarItemVisibility.Always
                            : BarItemVisibility.Never;
                    }
                }
            }
        }
    }
}
