﻿using System;
using GSC.Client.Common.DataModel;
using GSC.Model;

namespace GSC.Client.GscContextDataModel
{
    /// <summary>
    /// IGscContextUnitOfWork extends the IUnitOfWork interface with repositories representing specific entities.
    /// </summary>
    public interface IGscContextUnitOfWork : IUnitOfWork
    {
        GscContext Context { get; }
        /// <summary>
        /// The AcademicDegree entities repository.
        /// </summary>
        IRepository<AcademicDegree, int> AcademicDegrees { get; }

        /// <summary>
        /// The AcademicTitle entities repository.
        /// </summary>
        IRepository<AcademicTitle, int> AcademicTitles { get; }

        /// <summary>
        /// The Expert entities repository.
        /// </summary>
        IRepository<Expert, int> Experts { get; }

        IRepository<ExpertAgreement, int> ExpertAgreements { get; }

        /// <summary>
        /// Менеджеры плюс РСМ-координаторы
        /// </summary>
        IRepository<Observer, int> Observers { get; }

        /// <summary>
        /// The Period entities repository.
        /// </summary>
        IRepository<Project, int> Projects { get; }

        /// <summary>
        /// The Region entities repository.
        /// </summary>
        IRepository<Region, int> Regions { get; }

        /// <summary>
        /// Subjects
        /// </summary>
        IRepository<Subject, int> Subjects { get; }

        IRepository<Exec, int> Execs { get; }

        /// <summary>
        /// РЦОИ
        /// </summary>
        IRepository<Rcoi, int> Rcois { get; }

        IRepository<OivGek, int> OivGeks { get; }

        /// <summary>
        /// Расписание
        /// </summary>
        IRepository<Exam, int> Exams { get; }

        IRepository<Station, int> Stations { get; }

        IRepository<Violation, int> Violations { get; }

        IRepository<Violator, int> Violators { get; }

        IRepository<ContractInfo, string> ContractInfos { get; }

        IRepository<Schedule, int> Schedules { get; }

        IRepository<ScheduleDetail, int> ScheduleDetails { get; }

        IRepository<MonitorResult, int> MonitorResults { get; }

        IRepository<MrDetail, int> MrDetails { get; }

        IRepository<Event, int> Events { get; }

        IRepository<Contract, string> Contracts { get; }

        IRepository<MrDoc, int> MrDocs { get; }

        IRepository<Model.Login, int> Logins { get; }

        IRepository<Role, string> Roles { get; }

        IRepository<UserRole, Tuple<int, string>> UserRoles { get; }

    }
}
