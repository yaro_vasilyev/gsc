﻿using System;
using DevExpress.Mvvm;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.GscContextDataModel
{
    /// <summary>
    /// A GscContextUnitOfWork instance that represents the run-time implementation of the IGscContextUnitOfWork interface.
    /// </summary>
    public class GscContextUnitOfWork : DbUnitOfWork<GscContext>, IGscContextUnitOfWork
    {
        public GscContextUnitOfWork(Func<GscContext> contextFactory)
            : base(contextFactory)
        {
        }

        IRepository<AcademicDegree, int> IGscContextUnitOfWork.AcademicDegrees
        {
            get { return GetRepository(x => x.Set<AcademicDegree>(), x => x.Id); }
        }

        IRepository<AcademicTitle, int> IGscContextUnitOfWork.AcademicTitles
        {
            get { return GetRepository(x => x.Set<AcademicTitle>(), x => x.Id); }
        }

        IRepository<Expert, int> IGscContextUnitOfWork.Experts
        {
            get { return GetRepository(x => x.Set<Expert>(), x => x.Id); }
        }

        IRepository<ExpertAgreement, int> IGscContextUnitOfWork.ExpertAgreements
        {
            get { return GetRepository(x => x.Set<ExpertAgreement>(), x => x.ExpertId); }
        }


        public IRepository<Observer, int> Observers
        {
            get { return GetRepository(x => x.Set<Observer>(), x => x.Id); }
        }

        IRepository<Project, int> IGscContextUnitOfWork.Projects
        {
            get { return GetRepository(x => x.Set<Project>(), x => x.Id); }
        }

        IRepository<Region, int> IGscContextUnitOfWork.Regions
        {
            get { return GetRepository(x => x.Set<Region>(), x => x.Id); }
        }

        IRepository<Subject, int> IGscContextUnitOfWork.Subjects
        {
            get { return GetRepository(x => x.Set<Subject>(), x => x.Id); }
        }

        IRepository<Exec, int> IGscContextUnitOfWork.Execs
        {
            get { return GetRepository(x => x.Set<Exec>(), x => x.Id); }
        }

        IRepository<Rcoi, int> IGscContextUnitOfWork.Rcois
        {
            get { return GetRepository(x => x.Set<Rcoi>(), x => x.Id); }
        }

        IRepository<OivGek, int> IGscContextUnitOfWork.OivGeks
        {
            get { return GetRepository(x => x.Set<OivGek>(), x => x.Id); }
        }

        IRepository<Exam, int> IGscContextUnitOfWork.Exams
        {
            get { return GetRepository(x => x.Set<Exam>(), x => x.Id); }
        }

        IRepository<Station, int> IGscContextUnitOfWork.Stations
        {
            get { return GetRepository(x => x.Set<Station>(), x => x.Id); }
        }

        IRepository<Violation, int> IGscContextUnitOfWork.Violations
        {
            get { return GetRepository(x => x.Set<Violation>(), x => x.Id); }
        }

        IRepository<Violator, int> IGscContextUnitOfWork.Violators
        {
            get { return GetRepository(x => x.Set<Violator>(), x => x.Id); }
        }

        IRepository<ContractInfo, string> IGscContextUnitOfWork.ContractInfos
        {
            get { return GetRepository(x => x.Set<ContractInfo>(), x => x.Number); }
        }

        IRepository<Schedule, int> IGscContextUnitOfWork.Schedules
        {
            get { return GetRepository(x => x.Set<Schedule>(), x => x.Id); }
        }

        public IRepository<ScheduleDetail, int> ScheduleDetails
        {
            get { return GetRepository(x => x.Set<ScheduleDetail>(), x => x.Id); }
        }

        public IRepository<MonitorResult, int> MonitorResults
        {
            get { return GetRepository(x => x.Set<MonitorResult>(), x => x.Id); }
        }

        public IRepository<MrDetail, int> MrDetails
        {
            get { return GetRepository(x => x.Set<MrDetail>(), x => x.Id); }
        }

        public IRepository<Event, int> Events
        {
            get { return GetRepository(x => x.Set<Event>(), x => x.Id); }
        }

        public IRepository<Contract, string> Contracts
        {
            get { return GetRepository(context => context.Set<Contract>(), contract => contract.Number); }
        }

        public IRepository<MrDoc, int> MrDocs
        {
            get { return GetRepository(context => context.Set<MrDoc>(), d => d.Id); }
        }

        public IRepository<Model.Login, int> Logins
        {
            get { return GetRepository(context => context.Set<Model.Login>(), d => d.Id); }
        }



        public IRepository<Role, string> Roles
        {
            get { return GetRepository(context => context.Set<Role>(), r => r.SystemName); }
        }

        public IRepository<UserRole, Tuple<int, string>> UserRoles
        {
            get
            {
                return GetRepository(context => context.Set<UserRole>(),
                    ur => new Tuple<int, string>(ur.UserId, ur.SystemRole));
            }
        }
    }
}