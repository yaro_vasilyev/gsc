﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Mvvm;
using DevExpress.Utils;
using DevExpress.Utils.MVVM;
using DevExpress.XtraEditors;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.Login;
using GSC.Client.Security;
using GSC.Client.Util;
using GSC.Model;

namespace GSC.Client.GscContextDataModel
{
    /// <summary>
    /// Provides methods to obtain the relevant IUnitOfWorkFactory.
    /// </summary>
    public static class UnitOfWorkSource
    {
        private static readonly object Lock = new object();
        private static ISecurityService _securityService;

        public static ISecurityService SecurityService
        {
            set
            {
                if (_securityService != value)
                {
                    lock (Lock)
                    {
                        if (_securityService != value)
                        {
                            _securityService = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the IUnitOfWorkFactory implementation.
        /// </summary>
        public static IUnitOfWorkFactory<IGscContextUnitOfWork> GetUnitOfWorkFactory()
        {
            return new DbUnitOfWorkFactory<IGscContextUnitOfWork>(
                () => new GscContextUnitOfWork(
                    () => {
                        Debug.Assert(_securityService != null);
                        return new GscContext(_securityService.ConnectionString);
                    }));
        }
    }
}