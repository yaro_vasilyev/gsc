﻿using System;
using DevExpress.XtraBars.Ribbon;
using GSC.Client.Modules;
using JetBrains.Annotations;

namespace GSC.Client
{
    [Obsolete("Code stinks.")]
    internal static class RefactorThis
    {
        public static void MergeRibbonWithoutFlickering([NotNull] RibbonControl ribbonControl, MergeRibbonMessage message)
        {
            RibbonPage tempPage = null;

            ribbonControl.SuspendLayout();
            try
            {
                ribbonControl.BeginInit();

                try
                {
                    if (message.Ribbon != null)
                    {
                        tempPage = new RibbonPage(message.Ribbon.Pages[0].Text + " ");
                        ribbonControl.Pages.Add(tempPage);
                    }

                    ribbonControl.UnMergeRibbon();
                    if (message.Ribbon == null)
                    {
                        return;
                    }
                    ribbonControl.MergeRibbon(message.Ribbon);
                    if (tempPage != null)
                    {
                        ribbonControl.Pages.Remove(tempPage);
                    }
                }
                finally
                {
                    ribbonControl.EndInit();
                }
            }
            finally
            {
                ribbonControl.ResumeLayout();
            }
        }
    }
}