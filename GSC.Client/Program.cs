﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Mvvm;
using DevExpress.Utils.MVVM;
using DevExpress.XtraEditors;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;
using log4net;

namespace GSC.Client
{
    internal static class Program
    {
        private static ILog _log;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            //System.Runtime.ProfileOptimization.SetProfileRoot(Application.StartupPath);

            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                var exception = eventArgs.ExceptionObject as Exception;
                var message = exception?.Message ?? eventArgs.ExceptionObject?.ToString();
                Log.Error(message, exception);
            };

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("ru");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += (sender, eventArgs) =>
            {
                var exception = eventArgs?.Exception;
                Log.Error(exception?.Message, exception);
                XtraMessageBox.Show(UserLookAndFeel.Default, exception?.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };

            //DevExpress.Skins.SkinManager.EnableFormSkins();
            //DevExpress.UserSkins.BonusSkins.Register();

            //ServiceContainer.Default.RegisterService(new DbRoleBasedSecurityService(UnitOfWorkSource.GetUnitOfWorkFactory()), true);
            //ServiceContainer.Default.RegisterService(new SecurityDialogService(), true);


            try
            {
                Application.Run(new GscHostForm());
            }
            catch (GscAuthCancelledException)
            {
                Application.Exit();
            }
            catch(Exception e)
            {
                Log.Fatal("Unhandled exception", e);
            }
        }

        private static ILog Log => _log ?? (_log = LogManager.GetLogger(nameof(Program)));
    }
}