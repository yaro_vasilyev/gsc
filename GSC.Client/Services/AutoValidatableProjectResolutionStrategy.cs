using System;
using System.Linq;
using System.Timers;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.Services
{
    internal abstract class AutoValidatableProjectResolutionStrategy 
        : IProjectResolutionStrategy
            , IDisposable
    {
        protected static readonly object TheLock = new object();
        private bool disposed;
        private bool valid;
        private readonly Timer timer = new Timer();
        private Model.Project selectedProject;


        protected AutoValidatableProjectResolutionStrategy(
            [NotNull] object viewModelServiceContainer)
        {
            if (viewModelServiceContainer == null)
                throw new ArgumentNullException(nameof(viewModelServiceContainer));
            ServiceContainer = viewModelServiceContainer;

            OptimizeCurrentProjectCaching();
        }

        protected object ServiceContainer { get; private set; }

        protected IUnitOfWorkFactory<IGscContextUnitOfWork> UnitOfWorkFactory { get; set; }

        public virtual Model.Project ResolveProject(
            [NotNull] IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory)
        {
            if (UnitOfWorkFactory == null)
                UnitOfWorkFactory = unitOfWorkFactory;

            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            return ResolveProjectInternal();
        }

        protected abstract Model.Project ResolveProjectInternal();

        public bool IsValid(Model.Project project)
        {
            if(UnitOfWorkFactory == null)
                throw new InvalidOperationException();

            if (!valid)
            {
                lock (TheLock)
                {
                    if (!valid)
                    {
                        try
                        {
                            if (project != null)
                            {
                                var uw = UnitOfWorkFactory.CreateUnitOfWork();
                                var dbProject = uw.Projects.FirstOrDefault(p => p.Id == project.Id && p.Ts == project.Ts);
                                return dbProject != null; // ����� ��� ������� ����� �����
                            }
                        }
                        finally
                        {
                            // object refreshed anyway
                            valid = true;
                        }
                    }
                }
            }

            return project != null;
        }

        private void OptimizeCurrentProjectCaching()
        {
            // ThrowIfDisposed();
            timer.AutoReset = true;
            timer.Interval = GetIntervalForCurrentProjectTimer();
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Invalidate();
        }

        private void Invalidate()
        {
            ThrowIfDisposed();
            if (valid)
            {
                lock (TheLock)
                {
                    if (valid)
                    {
                        valid = false;
                    }
                }
            }
        }

        private double GetIntervalForCurrentProjectTimer()
        {
            var settingValue = AppSettings.GetTimsSpan(
                AppSettings.CurrentProjectCacheInvalidateInterval, TimeSpan.FromMinutes(1));
            return settingValue.TotalMilliseconds;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                timer.Stop();
                timer.Dispose();
            }

            disposed = true;
        }

        ~AutoValidatableProjectResolutionStrategy() { Dispose(false); }


        private void ThrowIfDisposed()
        {
            if (disposed)
                throw new ObjectDisposedException("AutoValidatableProjectResolutionStrategy");
        }

    }
}