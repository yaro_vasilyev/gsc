﻿using System;
using System.Collections.Generic;
using System.Linq;
using CsvHelper.Configuration;
using DevExpress.Mvvm;
using GSC.Client.Util;
using GSC.Client.ViewModels;
using JetBrains.Annotations;
using log4net;

namespace GSC.Client.Services.Import
{
    public interface ICsvImportService
    {
        void Import<TImportEntity, TCsvMap, TImportResult>(Action<TImportEntity> importRowAction,
                                                                Func<IEnumerable<TImportResult>> importFunc,
                                                                Func<TImportResult, string> resultToStringFunc,
                                                                ICollectionViewModelRefresh collectionRefresh,
                                                                string csvDelimiter = null)
            where TCsvMap : CsvClassMap<TImportEntity> where TImportResult : class;
    }

    internal class CsvImportService : ImportServiceBase, ICsvImportService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof (CsvImportService));

        public CsvImportService([NotNull] IOpenFileDialogService openFileDialogService,
                                [NotNull] IMessageBoxService messageBoxService)
            : base(openFileDialogService, messageBoxService) { }

        /// <summary>
        /// Выполнить импорт CSV-файла коллекции заданных сущностей <see cref="TImportEntity"/>. С выбором файла, загрузкой в БД и выдачей сообщений, что в файле отсутсвуют записи, присутствующие в БД.
        /// </summary>
        /// <typeparam name="TImportEntity">Импортируемая сущность. На ней основан маппинг CSV. Проще всего взять сущность из Entity.</typeparam>
        /// <typeparam name="TCsvMap">CSV-маппинг. См. <see cref="CsvClassMap{T}"/>. Также см. иснтрукцию http://joshclose.github.io/CsvHelper/#mapping.</typeparam>
        /// <typeparam name="TImportResult">Класс результата импорта, возвращаемого действием импорта <paramref name="importFunc"/>.</typeparam>
        /// <param name="importRowAction">Действие по загрузке одной строки импорта. Параметр - экземпляр импортируемой сущности.
        ///     Не должен быть <c>null</c>.</param>
        /// <param name="importFunc">Функция пакетного импорта загруженных строк. Возвращает список записей, которые отсуствуют в файле, но присутсвуют в БД. Не должен быть <c>null</c>.</param>
        /// <param name="resultToStringFunc">Функция преобразования результата в строку для вывода на экран списка.</param>
        /// <param name="collectionRefresh"></param>
        /// <param name="csvDelimiter">Разделитель в CSV-файле. если передан <c>null</c>, берётся значение по умолчанию (см. <see cref="CsvImporter{TEntity,TCsvMap,TImportResult}.Import"/>).</param>
        public void Import<TImportEntity, TCsvMap, TImportResult>(Action<TImportEntity> importRowAction,
                                                                          Func<IEnumerable<TImportResult>> importFunc,
                                                                          Func<TImportResult, string> resultToStringFunc,
                                                                          ICollectionViewModelRefresh collectionRefresh,
                                                                          string csvDelimiter = null)
            where TCsvMap : CsvClassMap<TImportEntity> where TImportResult : class
        {
            var csvfullFileName = ShowFileOpenDialog("Выберите файл для импорта", "Файлы CSV|*.csv|Все файлы|*.*");
            if (csvfullFileName == null)
            {
                return;
            }

            var csvImporter = new CsvImporter<TImportEntity, TCsvMap, TImportResult>(importRowAction, importFunc);

            IEnumerable<TImportResult> importResult;
            string faultsFileName;
            try
            {
                importResult = csvImporter.Import(csvfullFileName, out faultsFileName, csvDelimiter).ToList();
            }
            catch (Exception e)
            {
                log.Error("Error during import", e);

                MessageBoxService.Show(e.Message,
                                       "Ошибка при импорте CSV-файла",
                                       MessageButton.OK,
                                       MessageIcon.Error,
                                       MessageResult.OK);
                return;
            }

            if (importResult.Any())
            {
                var missingText = string.Join("\n", importResult.Select(resultToStringFunc));
                MessageBoxService.Show($"В CSV-файле отсутствуют некоторые записи:\n{missingText}",
                                       "Отсутствующие записи",
                                       MessageButton.OK,
                                       MessageIcon.Information,
                                       MessageResult.OK);
            }

            collectionRefresh?.Refresh();

            const string loadIsEndText = "Загрузка завершена.";
            var messageText = loadIsEndText;
            if (faultsFileName != null)
            {
                messageText +=
                    $" Но при загрузке были обнаружены ошибки в файле, они выгружены в файл \"{faultsFileName}\".";
            }
            MessageBoxService.Show(messageText,
                                   "Импорт CSV-файла",
                                   MessageButton.OK,
                                   MessageIcon.Information,
                                   MessageResult.OK);
        }
    }
}
