﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using DevExpress.Mvvm;
using GSC.Client.Util;
using GSC.Client.ViewModels;
using JetBrains.Annotations;
using log4net;

namespace GSC.Client.Services.Import
{

    public interface IXmlImportService
    {
        /// <summary>
        /// Выполнить импорт XML-файла коллекции заданных сущностей <see cref="TXmlEntity"/>. С выбором файла, загрузкой в БД и выдачей сообщений, что в файле отсутсвуют записи, присутствующие в БД.
        /// </summary>
        /// <typeparam name="TXmlEntity">Импортируемая сущность. На ней основан маппинг XML.</typeparam>
        /// <typeparam name="TImportResult">Класс результата импорта, возвращаемого действием импорта <paramref name="importFunc"/>.</typeparam>
        /// <param name="rootTagName">Корневой тег XML.</param>
        /// <param name="importRowAction">Действие по загрузке одной строки импорта. Параметр - экземпляр импортируемой сущности.
        ///     Не должен быть <c>null</c>.</param>
        /// <param name="importFunc">Функция пакетного импорта загруженных строк. Возвращает список записей, которые отсуствуют в файле, но присутсвуют в БД. Не должен быть <c>null</c>.</param>
        /// <param name="resultToStringFunc">Функция преобразования результата в строку для вывода на экран списка.</param>
        /// <param name="collectionRefresh"></param>
        void Import<TXmlEntity, TImportResult>(string rootTagName,
                                                    Action<TXmlEntity> importRowAction,
                                                    Func<IEnumerable<TImportResult>> importFunc,
                                                    Func<TImportResult, string> resultToStringFunc,
                                                    ICollectionViewModelRefresh collectionRefresh)
            where TXmlEntity : class where TImportResult : class;
    }

    internal class XmlImportService : ImportServiceBase, IXmlImportService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(XmlImportService));

        public XmlImportService([NotNull] IOpenFileDialogService openFileDialogService,
                                [NotNull] IMessageBoxService messageBoxService)
            : base(openFileDialogService, messageBoxService) { }

        /// <summary>
        /// Выполнить импорт XML-файла коллекции заданных сущностей <see cref="TXmlEntity"/>. С выбором файла, загрузкой в БД и выдачей сообщений, что в файле отсутсвуют записи, присутствующие в БД.
        /// </summary>
        /// <typeparam name="TXmlEntity">Импортируемая сущность. На ней основан маппинг XML.</typeparam>
        /// <typeparam name="TImportResult">Класс результата импорта, возвращаемого действием импорта <paramref name="importFunc"/>.</typeparam>
        /// <param name="rootTagName">Корневой тег XML.</param>
        /// <param name="importRowAction">Действие по загрузке одной строки импорта. Параметр - экземпляр импортируемой сущности.
        ///     Не должен быть <c>null</c>.</param>
        /// <param name="importFunc">Функция пакетного импорта загруженных строк. Возвращает список записей, которые отсуствуют в файле, но присутсвуют в БД. Не должен быть <c>null</c>.</param>
        /// <param name="resultToStringFunc">Функция преобразования результата в строку для вывода на экран списка.</param>
        /// <param name="collectionRefresh"></param>
        public void Import<TXmlEntity, TImportResult>(string rootTagName, Action<TXmlEntity> importRowAction,
            Func<IEnumerable<TImportResult>> importFunc,
            Func<TImportResult, string> resultToStringFunc,
            ICollectionViewModelRefresh collectionRefresh)
            where TXmlEntity : class where TImportResult : class
        {
            var xmlFileName = ShowFileOpenDialog("Выберите файл для импорта", "Файлы XML|*.xml|Все файлы|*.*");
            if (xmlFileName == null)
            {
                return;
            }

            List<TImportResult> importResults = null;
            try
            {
                var serializer = new XmlSerializer(typeof(List<TXmlEntity>), new XmlRootAttribute(rootTagName));
                List<TXmlEntity> xmlList;
                using (var streamReader = new StreamReader(xmlFileName))
                {
                    xmlList = serializer.Deserialize(streamReader) as List<TXmlEntity>;
                }

                try
                {
                    if (xmlList != null)
                    {
                        foreach (var xmlEntity in xmlList)
                        {
                            importRowAction?.Invoke(xmlEntity);
                        }

                        importResults = importFunc?.Invoke()?.ToList();
                    }
                }
                catch (EntityCommandExecutionException e)
                {
                    var errorMessage = e.ExtractPgSqlErrorMessage();
                    throw new InvalidOperationException(errorMessage, e);
                }
            }
            catch (Exception e)
            {
                log.Error("Error during import", e);

                MessageBoxService.Show(e.Message, "Ошибка при импорте XML-файла", MessageButton.OK, MessageIcon.Error,
                    MessageResult.OK);
                return;
            }

            if (importResults?.Any() ?? false)
            {
                var missingText = string.Join("\n", importResults.Select(resultToStringFunc));
                MessageBoxService.Show($"В XML-файле отсутствуют некоторые записи:\n{missingText}",
                    "Отсутствующие записи", MessageButton.OK, MessageIcon.Information, MessageResult.OK);
            }

            collectionRefresh?.Refresh();

            const string loadIsEndText = "Загрузка завершена.";
            const string messageText = loadIsEndText;
            MessageBoxService.Show(messageText, "Импорт XML-файла", MessageButton.OK, MessageIcon.Information,
                MessageResult.OK);
        }
    }
}
