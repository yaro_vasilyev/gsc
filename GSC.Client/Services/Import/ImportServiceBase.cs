﻿using System;
using System.Diagnostics;
using DevExpress.Mvvm;
using JetBrains.Annotations;

namespace GSC.Client.Services.Import
{
    internal abstract class ImportServiceBase
    {
        protected ImportServiceBase([NotNull] IOpenFileDialogService openFileDialogService,
                                    [NotNull] IMessageBoxService messageBoxService)
        {
            if (openFileDialogService == null)
            {
                throw new ArgumentNullException(nameof(openFileDialogService));
            }
            if (messageBoxService == null)
            {
                throw new ArgumentNullException(nameof(messageBoxService));
            }
            this.OpenFileDialogService = openFileDialogService;
            this.MessageBoxService = messageBoxService;
        }

        protected IOpenFileDialogService OpenFileDialogService { get; }

        protected IMessageBoxService MessageBoxService { get; }

        protected string ShowFileOpenDialog(string title, string filter)
        {
            Debug.Assert(OpenFileDialogService != null, "OpenFileDialogService != null");

            OpenFileDialogService.Title = title;
            OpenFileDialogService.Filter = filter;
            return OpenFileDialogService.ShowDialog() ? OpenFileDialogService.GetFullFileName() : null;
        }
    }
}
