using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.Services
{
    public interface IProjectResolutionStrategy
    {
        Model.Project ResolveProject(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory);
        bool IsValid(Model.Project project);
    }
}