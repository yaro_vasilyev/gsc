using System;
using System.Linq;
using JetBrains.Annotations;

namespace GSC.Client.Services
{
    internal class SingleOpenProjectResolutionStrategy : AutoValidatableProjectResolutionStrategy
    {
        public SingleOpenProjectResolutionStrategy([NotNull] object viewModelServiceContainer)
            : base(viewModelServiceContainer)
        {
        }

        protected override Model.Project ResolveProjectInternal()
        {
            try
            {
                var uw = UnitOfWorkFactory.CreateUnitOfWork();
                return uw.Projects.SingleOrDefault(p => p.Status == Model.ProjectStatus.Opened);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
    }
}