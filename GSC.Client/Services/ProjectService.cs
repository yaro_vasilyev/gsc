﻿using System;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Util;
using JetBrains.Annotations;
using Optional;

namespace GSC.Client.Services
{
    internal class ProjectService 
        : IProjectService
        , IDisposable
    {
        private readonly IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory;

        private bool disposed;
        private Option<Model.Project, MissingProjectException> currentProject;
        private static readonly object TheLock = new object();

        public ProjectService([NotNull] IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public Option<Model.Project, MissingProjectException> GetCurrentProject()
        {
            ThrowIfDisposed();

            lock (TheLock)
            {

                if (resolutionStrategy == null)
                    throw new InvalidOperationException();

                if (currentProject.HasValue)
                {
                    if (resolutionStrategy.IsValid(currentProject.ValueOr((Model.Project) null)))
                    {
                        return currentProject;
                    }
                }

                currentProject = resolutionStrategy.ResolveProject(unitOfWorkFactory)
                    .SomeNotNull(MissingProjectException.Default);

                return currentProject;
            }
        }

        private IProjectResolutionStrategy resolutionStrategy;

        public void SetResolutionStrategy(IProjectResolutionStrategy resolutionStrategy)
        {
            if (this.resolutionStrategy != null)
            {
                throw new InvalidOperationException();
            }

            this.resolutionStrategy = resolutionStrategy;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                var d = resolutionStrategy as IDisposable;
                d?.Dispose();
            }

            disposed = true;
        }

        ~ProjectService() { Dispose(false); }


        private void ThrowIfDisposed()
        {
            if (disposed)
                throw new ObjectDisposedException("ProjectService");
        }
    }
}
