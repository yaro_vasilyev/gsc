﻿using GSC.Client.Util;
using JetBrains.Annotations;
using Optional;

namespace GSC.Client.Services
{
    public interface IProjectService
    {
        /// <summary>
        /// Возвращает текущий проект
        /// </summary>
        Option<Model.Project, MissingProjectException> GetCurrentProject();

        void SetResolutionStrategy([NotNull] IProjectResolutionStrategy resolutionStrategy);
    }
}
