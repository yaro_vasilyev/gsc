using System;
using System.Diagnostics;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using GSC.Client.ViewModels.Project;
using JetBrains.Annotations;

namespace GSC.Client.Services
{
    internal class QueryUserProjectResolutionStrategy : AutoValidatableProjectResolutionStrategy
    {
        private readonly IDialogService dialogService;

        public QueryUserProjectResolutionStrategy([NotNull] object viewModelServiceContainer)
            : base(viewModelServiceContainer)
        {
            dialogService = ServiceContainer.GetRequiredService<IDialogService>();
        }

        protected override Model.Project ResolveProjectInternal()
        {
            var viewModel = ViewModelSource<QueryProjectViewModel>.Create(UnitOfWorkFactory);
            
            if (MessageResult.OK == dialogService.ShowDialog(MessageButton.OKCancel,
                "����� �������",
                null,
                viewModel))
            {
                Debug.Assert(viewModel.SelectedEntity != null);
                return viewModel.SelectedEntity;
            }
            return null;
        }
    }
}