﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Client.Util
{
    public static class XtraEditors
    {
        public static T ConvertBack<T>(object value)
        {
            if (value == null || value is DBNull)
                return default(T);
            return (T)value;
        }
    }
}
