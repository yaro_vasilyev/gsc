﻿using System;
using System.Configuration;

namespace GSC.Client.Util
{
    internal static class AppSettings
    {
        public const string LoggingEfGscContextKey = "logging.ef.gsc.enable";
        public const string LoggingEfSecurityContextKey = "logging.ef.security.enable";
        public const string CurrentProjectCacheInvalidateInterval = "currentProject.cacheInvalidateInterval";

        public static bool GetBool(string key, bool fallbackValue = false)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            bool value;
            var valueSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(valueSetting) || !bool.TryParse(valueSetting, out value))
            {
                value = fallbackValue;
            }

            return value;
        }

        public static TimeSpan GetTimsSpan(string key, TimeSpan fallbackValue)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            TimeSpan value;
            var valueSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(valueSetting) || !TimeSpan.TryParse(valueSetting, out value))
            {
                value = fallbackValue;
            }

            return value;
        }
    }
}
