﻿using System;
using System.Text;

namespace GSC.Client.Util
{
    public static class ExceptionExt
    {
        public static string GetFullMessage(this Exception e, string separator = " ")
        {
            var sb = new StringBuilder();

            var current = e;
            while (current != null)
            {
                sb.Append(current.Message).Append(separator);
                current = current.InnerException;
            }
            return sb.ToString();
        }
    }
}
