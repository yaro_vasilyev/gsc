﻿using System;

namespace GSC.Client.Util
{
    internal static class DateTimeExt
    {
        public static int IntMonth(this DateTime date)
        {
            return date.Year * 12 + date.Month;
        }

        public static DateTime StartMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime StartYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        public static DateTime NextMonth(this DateTime date)
        {
            return date.AddMonths(1);
        }
    }
}
