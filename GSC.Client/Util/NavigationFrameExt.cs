﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars.Navigation;

namespace GSC.Client.Util
{
    internal static class NavigationFrameExt
    {
        public static Control GetSelectedPageRootControl(this NavigationFrame navigationFrame)
        {
            if (navigationFrame?.SelectedPage != null)
            {
                var page = navigationFrame.SelectedPage as NavigationPage;
                Debug.Assert(page != null && page.Controls.Count > 0);

                return page.Controls[0];
            }

            return null;
        }
    }
}
