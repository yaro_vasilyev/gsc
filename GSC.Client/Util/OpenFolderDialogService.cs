﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gsc.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.Util
{
    public interface IOpenFolderDialogService
    {
        /// <summary>
        /// Gets/sets folder in which dialog will be open.
        /// </summary>
        string InitialFolder { get; set; }

        /// <summary>
        /// Gets/sets directory in which dialog will be open if there is no recent directory available.
        /// </summary>
        string DefaultFolder { get; set; }

        /// <summary>
        /// Gets selected folder.
        /// </summary>
        string Folder { get; }


        bool ShowDialog();
    }

    internal class OpenFolderDialogService : IDisposable, IOpenFolderDialogService
    {
        private readonly IWin32Window owner;
        private readonly OpenFolderDialog openFolderDialog = new OpenFolderDialog();
        private bool disposed;

        public OpenFolderDialogService([NotNull] IWin32Window owner)
        {
            if (owner == null)
            {
                throw new ArgumentNullException(nameof(owner));
            }
            this.owner = owner;
        }

        private void ThrowIfDisposed()
        {
            if (disposed)
                throw new ObjectDisposedException("OpenFolderDialogService");
        }

        public string InitialFolder
        {
            get
            {
                ThrowIfDisposed();
                return openFolderDialog.InitialFolder;
            }
            set
            {
                ThrowIfDisposed();
                openFolderDialog.InitialFolder = value;
            }
        }

        public string DefaultFolder
        {
            get
            {
                ThrowIfDisposed();
                return openFolderDialog.DefaultFolder;
            }
            set
            {
                ThrowIfDisposed();
                openFolderDialog.DefaultFolder = value;
            }
        }

        public string Folder
        {
            get
            {
                ThrowIfDisposed();
                return openFolderDialog.Folder;
            }
        }


        public bool ShowDialog()
        {
            ThrowIfDisposed();
            return DialogResult.OK == openFolderDialog.ShowDialog(owner);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                openFolderDialog.Dispose();
            }

            disposed = true;
        }

        ~OpenFolderDialogService()
        {
            Dispose(false);
        }
    }
}
