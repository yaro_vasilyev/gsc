using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using Devart.Data.PostgreSql;
using log4net;

namespace GSC.Client.Util
{
    //todo �������� CSV-������� �������.
    /// <summary>
    ///     �������� CSV � ��. ������ ���������� ��������: ������� � �� ������������� ��� ������ �� ����� ��� ������ ��������
    ///     <c>importRowAction</c> (��. �����������), � ����� �������������� ������ �������� <c>importFunc</c> (��.
    ///     �����������), ������������ ������ �����������.
    /// </summary>
    /// <typeparam name="TEntity">������������� ��������. ��������� � ������ ������� ������ (��. �����������).</typeparam>
    /// <typeparam name="TCsvMap">CSV-�������. ������ ���� �������� ������ <see cref="CsvClassMap" />.</typeparam>
    /// <typeparam name="TImportResult">
    ///     ��������� �������, ������������ Devart Entity Developer. ������������ ��� ��� ������
    ///     �����������, ������������ ������� <see cref="Import" />.
    /// </typeparam>
    public class CsvImporter<TEntity, TCsvMap, TImportResult> where TCsvMap : CsvClassMap where TImportResult : class
    {
        private readonly Func<IEnumerable<TImportResult>> importFunc;
        private readonly Action<TEntity> importRowAction;
        private readonly ILog log;

        /// <summary>
        ///     ����������� ���������. �������� �����-���������� ��. � ������
        ///     <see cref="CsvImporter{TEntity,TCsvMap,TImportResult}" />.
        /// </summary>
        /// <param name="importRowAction">
        ///     �������� �� �������� ����� ������ �������. �������� - ��������� ������������� ��������.
        ///     �� ������ ���� <c>null</c>.
        /// </param>
        /// <param name="importFunc">
        ///     ������� ��������� ������� ����������� �����. ���������� ������ �������, ������� �����
        ///     ���������� ����� <see cref="Import" />. �� ������ ���� <c>null</c>.
        /// </param>
        /// <exception cref="ArgumentNullException">�������������, ���� ���� �� ���������� <c>null</c>.</exception>
        public CsvImporter(Action<TEntity> importRowAction, Func<IEnumerable<TImportResult>> importFunc)
        {
            Contract.Requires(importRowAction != null);
            Contract.Requires(importFunc != null);
            if (importRowAction == null)
            {
                throw new ArgumentNullException(nameof(importRowAction));
            }
            if (importFunc == null)
            {
                throw new ArgumentNullException(nameof(importFunc));
            }
            this.importRowAction = importRowAction;
            this.importFunc = importFunc;

            log = LogManager.GetLogger(GetType());
        }

        /// <summary>
        ///     ������ �����. ��. �������� ������� � ������ <see cref="CsvImporter{TEntity,TCsvMap,TImportResult}" />.
        /// </summary>
        /// <param name="csvfullFileName">������ ���� � �������������� CSV-�����.</param>
        /// <param name="faultsFileName"></param>
        /// <param name="delimiter">����������� � CSV-�����. ���� ������� <c>null</c>, ������ �������� �� ��������� <c>";"</c>.</param>
        /// <returns>������ �����������. ������ - ������, �������������� � ��, �� ������������� � �����.</returns>
        /// <exception cref="ImportException">
        ///     �������������, ���� �� ����� �������� ����� ���������� ������ (��.
        ///     <see cref="CsvHelperException" />).
        /// </exception>
        /// <exception cref="InvalidOperationException">
        ///     �������������, ���� ��������� ������ ��� ������ � �� (��.
        ///     <see cref="EntityCommandExecutionException" />).
        /// </exception>
        public IEnumerable<TImportResult> Import(string csvfullFileName, out string faultsFileName, string delimiter = null)
        {
            //todo do import async

            try
            {
                var csvReadingProblems = new List<Tuple<Exception, int?, string[]>>();
                var actualDelemiter = delimiter ?? ";";
                using (var fileStream = File.OpenRead(csvfullFileName))
                {
                    using (var streamReader = new StreamReader(fileStream, Encoding.Default
                        /*todo move encoding to config (Default is option too)*/))
                    {
                        using (var csv = new CsvReader(streamReader))
                        {
                            csv.Configuration.RegisterClassMap<TCsvMap>();
                            csv.Configuration.Delimiter = actualDelemiter; /*todo move to config*/
                            csv.Configuration.DetectColumnCountChanges = true;
                            csv.Configuration.HasHeaderRecord = true;
                            csv.Configuration.SkipEmptyRecords = true;
                            csv.Configuration.TrimFields = true;
                            csv.Configuration.TrimHeaders = true;
                            csv.Configuration.IgnoreReadingExceptions = true;
                            // it's for collection problems via ReadingExceptionCallback
                            csv.Configuration.ReadingExceptionCallback = (exception, reader) =>
                            {
                                csvReadingProblems.Add(new Tuple<Exception, int?, string[]>(exception, reader.Row,
                                    reader.CurrentRecord));
                                //todo ����������� ���������, ��������� CsvReader
                            };

                            try
                            {
                                var isRead = false;
                                do
                                {
                                    try
                                    {
                                        isRead = csv.Read();
                                        if (!isRead)
                                        {
                                            continue;
                                        }
                                        var record = csv.GetRecord<TEntity>();
                                        importRowAction?.Invoke(record);
                                    }
                                    catch (Exception e)
                                    {
                                        csvReadingProblems.Add(new Tuple<Exception, int?, string[]>(e, csv.Row,
                                            csv.CurrentRecord));
                                    }
                                } while (isRead);
                            }
                            catch (CsvHelperException e)
                            {
                                throw new ImportException(
                                    $"�������������� ������ ��� ������ CSV-�����:\n{e.Message}\n\n���. ����:\n{e.Data["CsvHelper"]}",
                                    e)
                                {
                                    FileName = csvfullFileName
                                };
                            }
                        }
                    }
                }

                var importResults = importFunc?.Invoke();

                if (csvReadingProblems.Count == 0)
                {
                    faultsFileName = null;
                    return importResults;
                }

                faultsFileName = Path.ChangeExtension(Path.GetTempFileName(), ".csv");
                using (var file = File.CreateText(faultsFileName))
                {
                    log.Warn($"There are errors during loading csv-file {csvfullFileName}:");
                    foreach (var tuple in csvReadingProblems.Where(tuple => tuple.Item3 != null))
                    {
                        var fields = tuple.Item3;
                        var exception = tuple.Item1;
                        var rowNum = tuple.Item2;
                        var rowString = string.Join(actualDelemiter, fields);

                        file.WriteLine(rowString);
                        log.Warn($"Row {rowNum}: \"{rowString}\"\n{exception}");}
                }

                return importResults;
            }
            catch (EntityCommandExecutionException e)
            {
                var errorMessage = e.ExtractPgSqlErrorMessage();
                throw new InvalidOperationException(errorMessage, e);
            }
        }
    }
}