﻿using System;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using JetBrains.Annotations;

namespace GSC.Client.Util
{
    public class ScheduleDetailInfo
    {
        public string ExamDate { get; set; }
        public string StationName { get; set; }
        public string StationCode { get; set; }
        public string StationAddress { get; set; }
        public string ExamTypes { get; set; }
    }

    interface IGenerateDocumentService
    {
        void GenerateDocument([NotNull] Action<Func<IRichEditDocumentServer>, Func<Document>> generateDocFunc);
        string ExportDocument([NotNull] string fileNameNoPath, DocumentFormat format, bool compress);
    }
}
