﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace GSC.Client.Util
{
    public static class EnumExt
    {
        [NotNull]
        public static string RetrieveDisplayName([CanBeNull] this Enum enumMember)
        {
            if (enumMember == null)
            {
                return string.Empty;
            }

            var memberInfo = enumMember.GetType().GetMember(enumMember.ToString()).First();
            var displayAttribute = memberInfo?.GetCustomAttribute(typeof(DisplayAttribute), false) as DisplayAttribute;
            return displayAttribute?.Name ?? string.Empty;
        }
    }
}
