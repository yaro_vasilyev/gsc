﻿using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;

namespace GSC.Client.Util
{

    public interface IUpdateGridService
    {
        void Update(bool clearSelection = false);
    }

    internal class UpdateGridService : IUpdateGridService
    {
        private readonly GridControl gridControl;

        public UpdateGridService(GridControl gridControl)
        {
            this.gridControl = gridControl;
            Debug.Assert(gridControl != null);
        }


        public void Update(bool clearSelection = false)
        {
            var bindingSource = gridControl.DataSource as BindingSource;
            if (bindingSource != null)
            {
                bindingSource.ResetBindings(false);
            }
            else
            {
                gridControl.RefreshDataSource();
            }

            if (clearSelection)
            {
                (gridControl.MainView as ColumnView)?.ClearSelection();
            }
        }
    }
}
