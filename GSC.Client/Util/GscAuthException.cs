﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace GSC.Client.Util
{
    [Serializable]
    public class GscAuthException : Exception
    {
        public GscAuthException()
        {
        }

        public GscAuthException(string message) : base(message)
        {
        }

        public GscAuthException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GscAuthException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
