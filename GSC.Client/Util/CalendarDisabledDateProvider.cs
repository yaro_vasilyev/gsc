﻿using System;
using System.Collections.Generic;
using DevExpress.XtraEditors.Controls;

namespace GSC.Client.Util
{
    internal class CalendarDisabledDateProvider : ICalendarDisabledDateProvider
    {
        private readonly List<DateTime> enabledDays = new List<DateTime>();

        public CalendarDisabledDateProvider(IEnumerable<DateTime> enabledDays)
        {
            this.enabledDays.AddRange(enabledDays);
        }

        public bool IsDisabledDate(DateTime date, DateEditCalendarViewType view)
        {
            return -1 == enabledDays.IndexOf(date.Date);
        }
    }
}
