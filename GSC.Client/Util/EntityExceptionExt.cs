using System;
using System.Data.Entity.Core;
using System.Runtime.Serialization;
using Devart.Data.PostgreSql;
using GSC.Client.Common;
using GSC.Client.Common.DataModel;
using JetBrains.Annotations;
using SerializationInfo = DevExpress.Utils.Serializing.Helpers.SerializationInfo;

namespace GSC.Client.Util
{
    public static class EntityExceptionExt
    {
        /// <summary>
        /// ������ ��������� ���������������� ��������� PostgreSQL.
        /// </summary>
        /// <param name="exception">��. <see cref="EntityCommandExecutionException"/></param>
        /// <returns>���������� ��������� ������� <see cref="PgSqlException"/></returns>
        public static string ExtractPgSqlErrorMessage([CanBeNull] this EntityCommandExecutionException exception)
        {
            if (exception == null)
            {
                return string.Empty;
            }

            var pgSqlException = exception.InnerException as PgSqlException;
            var message = pgSqlException?.Message;
            var detailMessage = pgSqlException?.DetailMessage;
            if (!string.IsNullOrWhiteSpace(message) && !string.IsNullOrWhiteSpace(detailMessage))
            {
                message += "\n" + detailMessage;
            }

            var hint = pgSqlException?.Hint;
            if (!string.IsNullOrWhiteSpace(message) && !string.IsNullOrWhiteSpace(hint))
            {
                message += "\n���������: " + hint;
            }

            var errorMessage = !string.IsNullOrWhiteSpace(message)
                ? message
                : (exception.InnerException?.Message ?? exception.Message);

            return errorMessage;
        }

        public static PgSqlException FindUnderlyingDataSourceException(System.Exception exception)
        {
            var ex = exception;
            while (ex != null)
            {
                var pg = ex as PgSqlException;
                if (pg != null)
                    return pg;

                ex = ex.InnerException;
            }
            return null;
        }

        public static DbException ConvertException(DbException ex)
        {
            var pgException = FindUnderlyingDataSourceException(ex);
            if (pgException != null)
            {
                if (pgException.ErrorCode == "23503")
                    return new ForeignKeyViolationException("������� ������� ������, ������� ������������.",
                                                            CommonResources.Exception_UpdateErrorCaption,
                                                            ex);
            }
            return ex;
        }
    }

    public class ForeignKeyViolationException : DbException
    {
        public ForeignKeyViolationException(string errorMessage, string errorCaption, Exception innerException)
            : base(errorMessage, errorCaption, innerException) { }
    }
}