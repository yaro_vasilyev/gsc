﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Data.Async.Helpers;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Utils.MVVM;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.ViewModels;

namespace GSC.Client.Util
{
    internal static class FluentExt
    {
        public static void InitGridView<TEntity, TProjection, TPrimaryKey, TUnitOfWork, TViewModel>(
            this MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView,
            CollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork> viewModel)
            where TViewModel : CollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
            where TEntity : class
            where TProjection : class
            where TUnitOfWork : IUnitOfWork
        {
            fluent.InitGridViewReadonly(gridView, viewModel);

            fluent.WithEvent<RowCellClickEventArgs>(gridView, "RowCellClick")
                .EventToCommand(vm => vm.Edit(null),
                    vw => vw.SelectedEntity,
                    args =>
                        (args.Clicks == 2) &&
                        (args.Button == MouseButtons.Left) &&
                        (viewModel.CanEdit(viewModel.SelectedEntity)));
        }

        public static void InitGridViewReadonly<TEntity, TProjection, TUnitOfWork, TViewModel>(
            this MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView,
            ReadOnlyCollectionViewModelBase<TEntity, TProjection, TUnitOfWork> viewModel)
            where TViewModel : ReadOnlyCollectionViewModelBase<TEntity, TProjection, TUnitOfWork>
            where TEntity : class
            where TProjection : class
            where TUnitOfWork : IUnitOfWork
        {
            InitGridViewMultiselect<TEntity, TProjection, TUnitOfWork, TViewModel>(fluent, gridView);

            fluent.SetBinding(gridView, v => v.LoadingPanelVisible, vm => vm.IsLoading);
            fluent.WithEvent<ColumnView, FocusedRowObjectChangedEventArgs>(gridView, "FocusedRowObjectChanged")
                .SetBinding(vm => vm.SelectedEntity,
                            ea => ea.Row as TProjection,
                            (v, e) => v.FocusedRowHandle = v.FindRow(e));
        }

        #region Multiselect support

        private static void InitGridViewMultiselect<TEntity, TProjection, TUnitOfWork, TViewModel>(
            MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView)
            where TViewModel : ReadOnlyCollectionViewModelBase<TEntity, TProjection, TUnitOfWork> where TEntity : class
            where TProjection : class where TUnitOfWork : IUnitOfWork
        {
            if (gridView.OptionsSelection.MultiSelect)
            {
                gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                fluent.WithEvent<SelectionChangedEventArgs>(gridView, "SelectionChanged")
                    .SetBinding(x => x.SelectedEntities,
                                args => gridView.GetSelectedRows().Select(r => gridView.GetRow(r) as TProjection));
            }
        }

        private static void InitGridViewMultiselect<TEntity, TProjection, TPrimaryKey, TUnitOfWork, TViewModel>(
            MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView)
            where TViewModel : InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
            where TEntity : class, new() where TProjection : class where TUnitOfWork : IUnitOfWork
        {
            if (gridView.OptionsSelection.MultiSelect)
            {
                gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                fluent.WithEvent<SelectionChangedEventArgs>(gridView, "SelectionChanged")
                    .SetBinding(x => x.SelectedEntities,
                                args => gridView.GetSelectedRows().Select(gridView.GetRow));
            }
        }

        #endregion

        #region InstantFeedback grid support

        public static void InitInstantFeedbackGridViewReadOnly
            <TEntity, TProjection, TPrimaryKey, TUnitOfWork, TViewModel>(
            this MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView,
            InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork> viewModel)
            where TViewModel : InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
            where TEntity : class, new()
            where TProjection : class
            where TUnitOfWork : IUnitOfWork
        {
            var typeInfo = new TypeInfoProxied(TypeDescriptor.GetProperties(typeof (TProjection)), null);
            var pdId = typeInfo.UIDescriptors.Find(viewModel.PrimaryKeyPropertyName, false);
            var idIndex = typeInfo.UIDescriptors.IndexOf(pdId);

            InitGridViewMultiselect<TEntity, TProjection, TPrimaryKey, TUnitOfWork, TViewModel>(fluent, gridView);

            fluent.WithEvent<ColumnView, FocusedRowObjectChangedEventArgs>(gridView, "FocusedRowObjectChanged")
                .SetBinding(vm => vm.SelectedEntity,
                            ea => {
                                var content = new object[typeInfo.UIDescriptors.Count];
                                content[idIndex] = ea.Row;
                                return new ReadonlyThreadSafeProxyForObjectFromAnotherThread(ea.Row, content);
                            },
                            (v, e) => v.FocusedRowHandle = v.FindRow(e));
        }

        public static void InitInstantFeedbackGridView<TEntity, TProjection, TPrimaryKey, TUnitOfWork, TViewModel>(
            this MVVMContextFluentAPI<TViewModel> fluent,
            GridView gridView,
            InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork> viewModel)
            where TViewModel : InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
            where TEntity : class, new()
            where TProjection : class
            where TUnitOfWork : IUnitOfWork
        {
            fluent.InitInstantFeedbackGridViewReadOnly(gridView, viewModel);

            fluent.WithEvent<RowCellClickEventArgs>(gridView, "RowCellClick")
                .EventToCommand(vm => vm.Edit(null),
                                vw => vw.SelectedEntity,
                                args =>
                                    (args.Clicks == 2) &&
                                    (args.Button == System.Windows.Forms.MouseButtons.Left) &&
                                    (viewModel.CanEdit(viewModel.SelectedEntity)));
        }

        #endregion
    }
}
