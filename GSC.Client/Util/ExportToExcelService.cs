﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

namespace GSC.Client.Util
{
    internal class ExportToExcelService : IExportToExcelService
    {
        private readonly XlsxExportOptions defaultExportOptions;

        public ExportToExcelService(XlsxExportOptions defaultExportOptions)
        {
            this.defaultExportOptions = defaultExportOptions;
        }

        public void Export(GridAdapter gridControl, XlsxExportOptions exportOptions = null)
        {
            if (gridControl == null)
            {
                throw new ArgumentNullException(nameof(gridControl));
            }

            var tempFileName = Path.GetTempFileName();
            gridControl.ExportToXlsx(tempFileName, exportOptions ?? defaultExportOptions);

            try
            {
                var excelType = Type.GetTypeFromProgID("Excel.Application", true);
                // NOTE: Do not optimize this code to something like that `excel.Workbooks.Open(...)`. Each excel's object or collection you use, should be disposed to avoid memory leaks
                dynamic excel = null;
                try
                {
                    excel = Activator.CreateInstance(excelType);
                    excel.Visible = true;
                    var workbooks = excel.Workbooks;
                    var wb = workbooks.Open(tempFileName);
                    Marshal.ReleaseComObject(wb);
                    Marshal.ReleaseComObject(workbooks);
                }
                finally
                {
                    if (excel != null)
                        Marshal.ReleaseComObject(excel);
                }
            }
            catch (COMException e)
            {
                throw new Exception(
                    $"Microsoft Office Excel не установлен или не найден. Данные были выгружены в файл {tempFileName}",
                    e);
            }
        }
    }


    public interface IExportToExcelService
    {
        void Export(GridAdapter gridControl, XlsxExportOptions exportOptions = null);
    }


    public sealed class GridAdapter
    {
        private readonly GridControl control;
        private readonly bool clearGrouping;

        // TODO: clearGrouping = true by default?
        public GridAdapter(GridControl control, bool clearGrouping = false)
        {
            if (control == null)
            {
                throw new ArgumentNullException(nameof(control));
            }
            this.control = control;
            this.clearGrouping = clearGrouping;
        }

        public void ExportToXlsx(string fileName, XlsxExportOptions exportOptions = null)
        {
            var view = control.DefaultView;
            using (var stream = new MemoryStream())
            {
                try
                {
                    if (clearGrouping)
                    {
                        view.SaveLayoutToStream(stream);
                        (view as GridView)?.ClearGrouping();
                    }
                    control.ExportToXlsx(fileName, exportOptions);
                }
                finally
                {
                    if (clearGrouping)
                    {
                        stream.Position = 0;
                        view.RestoreLayoutFromStream(stream);
                    }
                }
            }
        }
    }
}
