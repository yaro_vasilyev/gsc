﻿using System;
using System.Runtime.Serialization;

namespace GSC.Client.Util
{
    [Serializable]
    public class GscAuthCancelledException : GscAuthException
    {
        public GscAuthCancelledException()
        {
        }

        public GscAuthCancelledException(string message) : base(message)
        {
        }

        public GscAuthCancelledException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GscAuthCancelledException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
