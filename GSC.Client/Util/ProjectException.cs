﻿using System;
using System.Runtime.Serialization;
using Optional;

namespace GSC.Client.Util
{
    [Serializable]
    public abstract class ProjectException : Exception
    {
        protected ProjectException()
        {
        }

        protected ProjectException(string message) : base(message)
        {
        }

        protected ProjectException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProjectException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            ProjectId = info.GetInt32(nameof(ProjectId));
            ProjectName = info.GetString(nameof(ProjectName));
        }

        public int ProjectId { get; set; }

        /// <summary>
        /// TODO: Rename Project.Title to Project.Name
        /// </summary>
        public string ProjectName { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(nameof(ProjectId), ProjectId);
            info.AddValue(nameof(ProjectName), ProjectName);
            base.GetObjectData(info, context);
        }
    }

    [Serializable]
    public class MissingProjectException : ProjectException
    {
        public MissingProjectException()
        {
        }

        public MissingProjectException(string message) : base(message)
        {
        }

        public MissingProjectException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MissingProjectException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        private static readonly MissingProjectException defaultInstance = 
            new MissingProjectException("Открытый проект недоступен или отсутствует.");

        public static MissingProjectException Default
        {
            get { return defaultInstance; }
        }
    }
}
