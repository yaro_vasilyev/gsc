﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using static System.String;

namespace GSC.Client.Util
{
    public static class ArrayExt
    {
        /// <summary>
        /// Склеивает значения: делает <see cref="string.Join(string,object[])"/>, но только для тех элементов, 
        /// которые не <c>null</c> и не пробельные (см. <see cref="string.IsNullOrWhiteSpace"/>).
        /// </summary>
        /// <param name="values">Значения для склейки.</param>
        /// <param name="separator">Разделитель.</param>
        /// <returns></returns>
        [UsedImplicitly]
        public static string JoinNonEmpty([NotNull] this IEnumerable<object> values, string separator = ", ")
        {
            return string.Join(separator, values.Where(o => !IsNullOrWhiteSpace(o?.ToString())));
        }

        /// <summary>
        /// Склеивает значения: делает <see cref="string.Join(string,object[])"/>, но только для тех элементов, 
        /// которые не <c>null</c> и не пробельные (см. <see cref="string.IsNullOrWhiteSpace"/>).
        /// </summary>
        /// <param name="separator">Разделитель.</param>
        /// <param name="values">Значения для склейки.</param>
        /// <returns></returns>
        [UsedImplicitly]
        public static string JoinNonEmpty(string separator, [NotNull] params object[] values)
        {
            return values.JoinNonEmpty(separator);
        }
    }
}