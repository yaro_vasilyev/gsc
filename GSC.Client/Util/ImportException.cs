﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace GSC.Client.Util
{
    [Serializable]
    public class ImportException : IOException
    {
        public ImportException() {}

        public ImportException(string message)
            : base(message) {}

        public ImportException(string message, int hresult)
            : base(message, hresult) {}

        public ImportException(string message, Exception innerException)
            : base(message, innerException) {}

        protected ImportException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            FileName = info.GetString(nameof(FileName));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue(nameof(FileName), FileName);
        }

        public string FileName { get; set; }
    }
}
