﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.Utils.MVVM.Services;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Native;

namespace GSC.Client.Util
{
    public class ModalDocumentAdapter : IDocumentAdapter
    {
        private readonly IWin32Window parentWindow;
        private readonly Dictionary<Control, XtraDialogForm> dialogs =
            new Dictionary<Control, XtraDialogForm>();

        private Control control1;
        private XtraDialogForm form1;

        public ModalDocumentAdapter(IWin32Window parentWindow)
        {
            this.parentWindow = parentWindow;
        }

        public void Dispose()
        {
            foreach (var form in dialogs.Values)
            {
                form.Dispose();
            }

            form1?.Dispose();
        }

        public void Show(Control control)
        {
            //            XtraDialogForm form;
            //            if (dialogs.TryGetValue(control, out form))
            //            {
            //                Debug.Assert(form != null);
            //                form.ShowDialog(parentWindow);
            //            }
            //            else
            //            {
            //                form = new XtraDialogForm();
            //                form.Closed += Form_Closed;
            //                form.FormClosing += Form_FormClosing;
            //                form.Controls.Add(control);
            //                control.Dock = DockStyle.Fill;
            //                dialogs.Add(control, form);
            //
            //                form.ShowDialog(parentWindow);
            //            }
            form1 = new XtraDialogForm();
            control1 = control;
            form1.Closed += Form_Closed;
            form1.FormClosing += Form_FormClosing;
            form1.Controls.Add(control);
            control.Dock = DockStyle.Fill;
            dialogs.Add(control1, form1);

            form1.ShowDialog(parentWindow);

        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
            Closing?.Invoke(sender, e);
        }

        private void Form_Closed(object sender, EventArgs e)
        {
            //            var controls = ((XtraDialogForm)sender).Controls;
            //            Debug.Assert(controls.Count > 0);

            //            dialogs.Remove(controls[0]);

            form1.FormClosing -= Form_FormClosing;
            form1.Closed -= Form_Closed;

            Closed?.Invoke(sender, e);
        }

        public void Close(Control control, bool force = true)
        {
//            XtraDialogForm form;
//            if (dialogs.TryGetValue(control, out form))
//            {
//                Debug.Assert(form != null);
//                form.Close();
//                dialogs.Remove(control);
//            }
            form1.Close();
        }

        public event CancelEventHandler Closing;
        public event EventHandler Closed;
    }
}
