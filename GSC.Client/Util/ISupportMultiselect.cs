﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Client.Util
{
    public interface ISupportMultiselect
    {
        bool Multiselect { get; set; }
    }
}
