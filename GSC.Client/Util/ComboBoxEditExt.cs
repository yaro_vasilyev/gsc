﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DevExpress.XtraEditors;
using GSC.Model.DataAnnotations;

namespace GSC.Client.Util
{
    public static class ComboBoxEditExt
    {
        public static void InitItemsFromDataAnnotations<T>(this ComboBoxEdit comboBox, Expression<Func<T, object>> propertyAccessor)
        {
            Debug.Assert(propertyAccessor != null);

            if (propertyAccessor.Body.NodeType == ExpressionType.Convert)
            {
                var expr = propertyAccessor.Body as UnaryExpression;
                if (expr == null)
                    throw new NotSupportedException("Поддерживаются только простые выражения доступа к свойству или полю.");

                var me = expr.Operand as MemberExpression;
                if (me == null)
                    throw new NotSupportedException("Поддерживаются только простые выражения доступа к свойству или полю.");

                var propertyInfo = me.Member as PropertyInfo;
                if (propertyInfo == null)
                    throw new NotSupportedException("Поддерживаются только простые выражения доступа к свойству или полю.");

                if (propertyInfo.DeclaringType.IsDefined(typeof (MetadataTypeAttribute)))
                {
                    var metadata = (MetadataTypeAttribute)propertyInfo.DeclaringType.GetCustomAttributes(typeof (MetadataTypeAttribute)).First();
                    propertyInfo = metadata.MetadataClassType.GetProperty(propertyInfo.Name);
                }

                var onlyNumbers = propertyInfo.GetCustomAttributes(typeof(OnlyNumbersAttribute), true).ToArray();
                if (onlyNumbers.Length > 0)
                {
                    var only = (OnlyNumbersAttribute)onlyNumbers[0];

                    comboBox.Properties.Items.Clear();
                    comboBox.Properties.Items.AddRange(only.Items);
                    return;
                }

                var ranges = propertyInfo.GetCustomAttributes(typeof(RangeAttribute), true).ToArray();
                if (ranges.Length > 0)
                {
                    var range = (RangeAttribute)ranges[0];
                    if (range.Minimum.GetType() != typeof(int))
                    {
                        throw new NotSupportedException("Поддерживается привязка только целочисленных диапазонов.");
                    }

                    comboBox.Properties.Items.Clear();
                    var min = (int) range.Minimum;
                    var max = (int) range.Maximum;
                    if (min > max)
                        throw new ArgumentOutOfRangeException("Указано минимальное значение, большее чем максимальное.");

                    comboBox.Properties.Items.AddRange(Enumerable.Range(min, max - min + 1).ToList());
                    return;
                }

                return;
            }

            if (propertyAccessor.Body.NodeType == ExpressionType.MemberAccess)
            {
                throw new NotImplementedException();
            }

            throw new ArgumentException("Поддерживаются только выражения обращения к свойству или полю.");
        }
    }
}
