﻿using System;
using DevExpress.Export;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Utils;
using DevExpress.Utils.MVVM;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraNavBar;
using DevExpress.XtraPrinting;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules;
using GSC.Client.Security;
using GSC.Client.Services;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using GSC.Client.ViewModels;
using JetBrains.Annotations;

//using GSC.Client.GscContextDataModel;

namespace GSC.Client
{
    public partial class GscHostForm : RibbonForm
    {

        public GscHostForm()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                mvvmContext.RegisterDefaultService(new SecurityDialogService());
                mvvmContext.RegisterDefaultService(new DbRoleBasedSecurityService(UnitOfWorkSource.GetUnitOfWorkFactory()));
                var secService = mvvmContext.GetService<ISecurityService>();
                UnitOfWorkSource.SecurityService = secService;

                Messenger.Default.Register<MergeRibbonMessage>(this, OnMergeRibbonMessage);

                mvvmContext.RegisterDefaultService(new OpenFolderDialogService(this));
                mvvmContext.RegisterDefaultService(new ExportToExcelService(new XlsxExportOptionsEx(TextExportMode.Value)
                {
                    ExportType = ExportType.DataAware,
                    AllowCellMerge = DefaultBoolean.True,
                    RawDataMode = true
                }));

                var openFileDialogService = mvvmContext.GetService<IOpenFileDialogService>();
                var messageBoxService = mvvmContext.GetService<IMessageBoxService>();

                mvvmContext.RegisterDefaultService(new CsvImportService(openFileDialogService, messageBoxService));
                mvvmContext.RegisterDefaultService(new XmlImportService(openFileDialogService, messageBoxService));

                if (!secService.QueryUserCredentials(mvvmContext.GetService<ISecurityDialogService>(), true))
                {
                    throw new GscAuthCancelledException();
                }

                RegisterProjectService(secService);
            }
        }

        private void RegisterProjectService(ISecurityService securityService)
        {
            var unitOfWorkFactory = UnitOfWorkSource.GetUnitOfWorkFactory();
            var projectService = new ProjectService(unitOfWorkFactory);
            mvvmContext.RegisterDefaultService(projectService);
            IProjectResolutionStrategy projectResolutionStrategy =
                securityService.OperationAllowed(SecurityOperations.Projects.New)
                    ? new QueryUserProjectResolutionStrategy(ViewModel)
                    : (IProjectResolutionStrategy)new SingleOpenProjectResolutionStrategy(ViewModel);

            projectService.SetResolutionStrategy(projectResolutionStrategy);
        }

        private void OnMergeRibbonMessage(MergeRibbonMessage message)
        {
            try
            {
                SuspendLayout();
                RefactorThis.MergeRibbonWithoutFlickering(ribbonControl, message);
            }
            finally
            {
                ResumeLayout();
            }
        }


        private static void Bind([NotNull] MVVMContextFluentAPI<GscContextViewModel> fluent,
                                 [NotNull] ISecurityService securityService,
                                 NavBarItem item,
                                 string loadOperation,
                                 string moduleName)
        {
            if (securityService == null)
                throw new ArgumentNullException(nameof(securityService));
            if (fluent == null)
                throw new ArgumentNullException(nameof(fluent));

            if (securityService.OperationAllowed(loadOperation))
            {
                fluent.BindCommand(item,
                    (vm, m) => vm.Show(m),
                    vm => vm.FindModule(moduleName));
                item.Visible = true;
            }
        }

        private void InitBindings()
        {
            var securityService = ViewModel.GetRequiredService<ISecurityService>();

            var fluent = mvvmContext.OfType<GscContextViewModel>();

            Action<NavBarItem, string, string> bind =
                (nbi, operation, module) => Bind(fluent, securityService, nbi, operation, module);

            // TODO: Delegate security to ObjectsView
            fluent.BindCommand(nbiManagers, (vm, m) => vm.Show(m),
                vm => vm.FindModule(GscContextViewModel.ModuleObservers));

            // TODO: Delegate security to ObjectsView
            fluent.BindCommand(nbiViolations,
                               (vm, m) => vm.Show(m),
                               vm => vm.FindModule(GscContextViewModel.ModuleViolations));

            // TODO: Delegate security to ObjectsView
            fluent.BindCommand(nbiObjects, (vm, m) => vm.Show(m),
                vm => vm.FindModule(GscContextViewModel.ModuleObjects));
            nbiObjects.Visible = true;

            bind(nbiExperts, SecurityOperations.Experts.LoadModule, GscContextViewModel.ModuleExperts);
            bind(nbiAcademicDegree, SecurityOperations.AcademicDegrees.LoadModule, GscContextViewModel.ModuleAcademicDegrees);
            bind(nbiAcademicTitle, SecurityOperations.Subjects.LoadModule, GscContextViewModel.ModuleAcademicTitles);
            bind(nbiSubject, SecurityOperations.Subjects.LoadModule, GscContextViewModel.ModuleSubjects);
            bind(nbiContractInfos, SecurityOperations.ContractInfos.LoadModule, GscContextViewModel.ModuleContractInfos);
            bind(nbiSchedule, SecurityOperations.Schedules.LoadModule, GscContextViewModel.ModuleSchedules);
            bind(nbiMonitor, SecurityOperations.MonitorResults.LoadModule, GscContextViewModel.ModuleMonitorResult);
            bind(nbiMonitorReports, SecurityOperations.MonitorReports.LoadModule, GscContextViewModel.ModuleMonitorReport);
            bind(nbiContracts, SecurityOperations.Contracts.LoadModule, GscContextViewModel.ModuleContracts);
            bind(nbiCallCenter, SecurityOperations.CallCenter.LoadModule, GscContextViewModel.ModuleCallCenter);
            bind(nbiReportDatabase, SecurityOperations.ReportsDatabase.LoadModule, GscContextViewModel.ModuleDatabaseReports);
            bind(nbiReportSchedule, SecurityOperations.ReportsSchedule.LoadModule, GscContextViewModel.ModuleScheduleReports);
            bind(nbiReportContracts, SecurityOperations.ReportsContracts.LoadModule, GscContextViewModel.ModuleContractReports);
            bind(nbiReportMonitor, SecurityOperations.ReportsMonitorResult.LoadModule, GscContextViewModel.ModuleMonitorResultReports);
            bind(nbiReportCallCenter, SecurityOperations.ReportsCallCenter.LoadModule, GscContextViewModel.ModuleCallCenterReports);
            bind(nbiPrivateCabinetAccess, SecurityOperations.PrivateCabinetAccess.LoadModule, GscContextViewModel.ModulePrivateCabinetAccess);
            bind(nbiAdminProject, SecurityOperations.Projects.LoadModule, GscContextViewModel.ModuleProjects);
            bind(nbiAdminUsers, SecurityOperations.AdminUsers.LoadModule, GscContextViewModel.ModuleAdminUsers);
        }

        private GscContextViewModel ViewModel => mvvmContext.GetViewModel<GscContextViewModel>();

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                Visible = false;
                ViewModel.OnLoaded(ViewModel.DefaultModule);
                base.OnLoad(e);
                InitBindings();
                Visible = true;
            }
            catch (GscAuthException)
            {
                // just close, we've tried to authenticate N times, or just gave up.
                // TODO: Log this
                Close();
            }
        }

        private void OnNavigationSelectedPageChanged(object sender, SelectedPageChangedEventArgs e)
        {
            var ribbonSource = navigationFrame.GetSelectedPageRootControl() as IRibbonSource;
            Messenger.Default.Send(new MergeRibbonMessage
            {
                Ribbon = ribbonSource?.Ribbon
            });
        }

        private void OnMergeRibbonControl(object sender, RibbonMergeEventArgs e)
        {
            ribbonControl.SelectedPage = e.MergedChild.SelectedPage;
        }
    }
}