﻿using System;
using System.Linq;
using System.Security;
using DevExpress.Mvvm.POCO;
using GSC.Client.Services;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.Security
{
    public static class SecurityOperations
    {
        public static class Experts
        {
            public const string LoadModule = "s_expert_module";
            public const string List = "s_expert_list";
            public const string Edit = "s_expert_edit";
            public const string Delete = "s_expert_delete";
            public const string New = "s_expert_new";
            public const string ImportFromCsv = "s_expert_import";
            public const string ExportToExcel = "s_expert_export";
            public const string AddAgreement = "s_expert_agr_upload";
            public const string RemoveAgreement = "s_expert_agr_remove";
            public const string PreviewAgreement = "s_expert_agr_preview";
        }

        public static class Projects
        {
            public const string LoadModule = "s_project_module";
            public const string New = "s_project_new";
            public const string Edit = "s_project_edit";
            public const string Delete = "s_project_delete";
        }

        public static class CallCenter
        {
            public const string LoadModule = "s_cc_module";
            public const string List = "s_cc_list";
            public const string New = "s_cc_new";
            public const string Edit = "s_cc_edit";
            public const string Delete = "s_cc_delete";
            public const string ExportToExcel = "s_cc_export";
        }

        public static class Contracts
        {
            public const string LoadModule = "s_contract_module";
            public const string List = "s_contract_list";
            public const string New = "s_contract_new";
            public const string Edit = "s_contract_edit";
            public const string Delete = "s_contract_delete";
        }

        public static class ContractInfos
        {
            public const string LoadModule = "s_contr_inf_module";
            public const string List = "s_contr_inf_list";
            public const string New = "s_contr_inf_new";
            public const string Edit = "s_contr_inf_edit";
            public const string Delete = "s_contr_inf_delete";
            public const string ExportToExcel = "s_contr_inf_export";
            public const string ImportFromCsv = "s_contr_inf_import";
        }

        public static class MonitorReports
        {
            public const string LoadModule = "s_mrep_module";
            public const string List = "s_mrep_list";
            public const string UploadFile = "s_mrep_upload";
            public const string DownloadFile = "s_mrep_download";
            public const string DeleteFile = "s_mrep_delete";
            public const string PreviewFile = "s_mrep_preview";
        }

        public static class AcademicDegrees
        {
            public const string LoadModule = "s_ad_module";
            public const string List = "s_ad_list";
            public const string New = "s_ad_new";
            public const string Edit = "s_ad_edit";
            public const string Delete = "s_ad_delete";
            public const string ImportFromCsv = "s_ad_import";
            public const string ExportToExcel = "s_ad_export";
        }

        public static class AcademicTitles
        {
            public const string LoadModule = "s_at_module";
            public const string List = "s_at_list";
            public const string New = "s_at_new";
            public const string Edit = "s_at_edit";
            public const string Delete = "s_at_delete";
            public const string ImportFromCsv = "s_at_import";
            public const string ExportToExcel = "s_at_export";
        }

        public static class Subjects
        {
            public const string LoadModule = "s_subject_module";
            public const string List = "s_subject_list";
            public const string New = "s_subject_new";
            public const string Edit = "s_subject_edit";
            public const string Delete = "s_subject_delete";
            public const string ImportFromCsv = "s_subject_import";
            public const string ExportToExcel = "s_subject_export";
        }

        public static class Exams
        {
            public const string LoadModule = "s_exam_module";
            public const string List = "s_exam_list";
            public const string New = "s_exam_new";
            public const string Edit = "s_exam_edit";
            public const string Delete = "s_exam_delete";
            public const string ImportFromCsv = "s_exam_import";
            public const string ExportToExcel = "s_exam_export";
        }

        public static class OivGeks
        {
            public const string LoadModule = "s_og_module";
            public const string List = "s_og_list";
            public const string New = "s_og_new";
            public const string Edit = "s_og_edit";
            public const string Delete = "s_og_delete";
            //public const string ImportFromCsv = "s_og_import";
            public const string ExportToExcel = "s_og_export";
        }

        public static class Execs
        {
            public const string LoadModule = "s_exec_module";
            public const string List = "s_exec_list";
            public const string New = "s_exec_new";
            public const string Edit = "s_exec_edit";
            public const string Delete = "s_exec_delete";
            public const string ImportFromCsv = "s_exec_import";
            public const string ExportToExcel = "s_exec_export";
        }

        public static class Rcoi
        {
            public const string LoadModule = "s_rcoi_module";
            public const string List = "s_rcoi_list";
            public const string New = "s_rcoi_new";
            public const string Edit = "s_rcoi_edit";
            public const string Delete = "s_rcoi_delete";
            public const string ImportFromCsv = "s_rcoi_import";
            public const string ExportToExcel = "s_rcoi_export";
        }

        public static class Regions
        {
            public const string LoadModule = "s_region_module";
            public const string List = "s_region_list";
            public const string New = "s_region_new";
            public const string Edit = "s_region_edit";
            public const string Delete = "s_region_delete";
            public const string ImportFromCsv = "s_region_import";
            public const string ExportToExcel = "s_region_export";
        }

        public static class Managers
        {
            public const string LoadModule = "s_manager_module";
            public const string List = "s_manager_list";
            public const string New = "s_manager_new";
            public const string Edit = "s_manager_edit";
            public const string Delete = "s_manager_delete";
            public const string ExportToExcel = "s_manager_export";
        }

        public static class Schedules
        {
            public const string LoadModule = "s_schedule_module";
            public const string List = "s_schedule_list";
            public const string New = "s_schedule_new";
            public const string Edit = "s_schedule_edit";
            public const string Delete = "s_schedule_delete";
            public const string ExportToExcel = "s_schedule_export";
        }

        public static class RsmCoordinators
        {
            public const string LoadModule = "s_rsm_module";
            public const string List = "s_rsm_list";
            public const string New = "s_rsm_new";
            public const string Edit = "s_rsm_edit";
            public const string Delete = "s_rsm_delete";
            public const string ExportToExcel = "s_rsm_export";
        }

        public static class Violations
        {
            public const string LoadModule = "s_violation_module";
            public const string List = "s_violation_list";
            public const string New = "s_violation_new";
            public const string Edit = "s_violation_edit";
            public const string Delete = "s_violation_delete";
            public const string ImportFromCsv = "s_violation_import";
            public const string ExportToExcel = "s_violation_export";
        }

        public static class Violators
        {
            public const string LoadModule = "s_violator_module";
            public const string List = "s_violator_list";
            public const string New = "s_violator_new";
            public const string Edit = "s_violator_edit";
            public const string Delete = "s_violator_delete";
            public const string ImportFromCsv = "s_violator_import";
            public const string ExportToExcel = "s_violator_export";
        }

        public static class Stations
        {
            public const string LoadModule = "s_station_module";
            public const string List = "s_station_list";
            public const string New = "s_station_new";
            public const string Edit = "s_station_edit";
            public const string Delete = "s_station_delete";
            public const string ImportFromCsv = "s_station_import";
            public const string ExportToExcel = "s_station_export";
        }

        public static class MonitorResults
        {
            public const string LoadModule = "s_mres_module";
            public const string List = "s_mres_list";
            public const string New = "s_mres_new";
            public const string Edit = "s_mres_edit";
            public const string Delete = "s_mres_delete";
            public const string ExportToExcel = "s_mres_export";
        }

        public static class ReportsDatabase
        {
            public const string LoadModule = "s_repdb_module";
        }

        public static class ReportsSchedule
        {
            public const string LoadModule = "s_repschedule_module";
        }

        public static class ReportsCallCenter
        {
            public const string LoadModule = "s_repcall_module";
        }

        public static class ReportsMonitorResult
        {
            public const string LoadModule = "s_repmr_module";
        }

        public static class ReportsContracts
        {
            public const string LoadModule = "s_repcontracts_module";
        }

        public static class PrivateCabinetAccess
        {
            public const string LoadModule = "s_pcab_module";
        }

        public static class AdminUsers
        {
            public const string LoadModule = "s_users_module";
            public const string List = "s_users_list";
        }

        [Obsolete("Deprecated. Use EnsureOperationAllowed(ISecureViewModel, string) method instead.")]
        public static void EnsureOperationAllowed(this ISecurityService securityService, string operation)
        {
            if (securityService == null)
            {
                throw new ArgumentNullException(nameof(securityService));
            }

            if (!OperationAllowed(securityService, operation))
            {
                throw new SecurityException("Операция не разрешена для пользователя.");
            }
        }

        public static void EnsureOperationAllowed([NotNull] this ISecureViewModel viewModel, string operation)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }

            var projectService = viewModel.GetRequiredService<IProjectService>();
            var securityService = viewModel.GetRequiredService<ISecurityService>();

            var project = projectService.GetCurrentProject().Match(p => p, e => { throw e; });

            if (!(securityService.GetOperationSecurityAction(operation) == SecurityAction.Allow &&
                project.Status == ProjectStatus.Opened))
            {
                throw new SecurityException("Операция не разрешена для пользователя.");
            }
        }

        public static void EnsureAnyOpeationAllowed([NotNull] this ISecureViewModel viewModel, params string[] operations)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }
            var projectService = viewModel.GetRequiredService<IProjectService>();
            var project = projectService.GetCurrentProject().Match(p => p, e => { throw e; });
            if (project.Status != ProjectStatus.Opened)
            {
                throw new SecurityException("Операция не разрешена для пользователя.");
            }

            foreach (var operation in operations)
            {
                try
                {
                    viewModel.EnsureOperationAllowed(operation);
                    return;
                }
                catch (SecurityException)
                {
                }
            }
            throw new SecurityException("Операция не разрешена для пользователя.");
        }

        public static void EnsureReadOnlyOperationAllowed([NotNull] this ISecureViewModel viewModel, string operation)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }

            var securityService = viewModel.GetRequiredService<ISecurityService>();

            if (securityService.GetOperationSecurityAction(operation) != SecurityAction.Allow)
            {
                throw new SecurityException("Операция не разрешена для пользователя.");
            }
        }

        [Obsolete("Deprecated. Use OperationAllowed(ISecureViewModel, string) method instead.")]
        public static bool OperationAllowed(this ISecurityService securityService, string operation)
        {
            if (securityService == null)
            {
                throw new ArgumentNullException(nameof(securityService));
            }

            return SecurityAction.Allow ==
                   securityService.GetOperationSecurityAction(operation);
        }

        public static bool OperationAllowed([NotNull] this ISecureViewModel viewModel, string operation)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }

            var projectService = viewModel.GetRequiredService<IProjectService>();
            var securityService = viewModel.GetRequiredService<ISecurityService>();

            try
            {
                var project = projectService.GetCurrentProject().Match(p => p, e => { throw e; });

                return securityService.GetOperationSecurityAction(operation) == SecurityAction.Allow &&
                       project.Status == ProjectStatus.Opened;
            }
            catch (MissingProjectException)
            {
                return false;
            }
        }

        public static bool AnyOperationAllowed([NotNull] this ISecureViewModel viewModel, params string[] operations)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }
            var projectService = viewModel.GetRequiredService<IProjectService>();
            var project = projectService.GetCurrentProject().Match(p => p, e => { throw e; });
            if (project.Status != ProjectStatus.Opened)
            {
                return false;
            }

            return operations.Any(viewModel.OperationAllowed);
        }

        public static bool ReadOnlyOperationAllowed([NotNull] this ISecureViewModel viewModel, string operation)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }

            var securityService = viewModel.GetRequiredService<ISecurityService>();

            try
            {
                return securityService.GetOperationSecurityAction(operation) == SecurityAction.Allow;
            }
            catch (MissingProjectException)
            {
                return false;
            }
        }

        public static bool CanLoadModule(this ISecurityService securityService, string moduleLoadOperation)
        {
            if (securityService == null)
            {
                throw new ArgumentNullException(nameof(securityService));
            }
            if (moduleLoadOperation == null)
            {
                throw new ArgumentNullException(nameof(moduleLoadOperation));
            }

            return SecurityAction.Allow ==
                   securityService.GetOperationSecurityAction(moduleLoadOperation);
        }
    }

    public interface ISecureViewModel
    {
        
    }
}