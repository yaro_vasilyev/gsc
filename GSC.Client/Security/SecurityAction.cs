﻿namespace GSC.Client.Security
{
    public enum SecurityAction
    {
        Deny = 0,
        Allow = 1
    }
}