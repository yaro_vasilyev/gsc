﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GSC.Client.Util;
using log4net;

namespace GSC.Client.Security
{
    internal class SecurityDbContext : DbContext
    {
        public SecurityDbContext(string connectionString)
            : base(connectionString)
        {
            InitializeLogging();
            Database.SetInitializer(new NullDatabaseInitializer<SecurityDbContext>());
        }

        public virtual DbSet<Model.User> Users { get; set; }

        private void InitializeLogging()
        {
            var logger = LogManager.GetLogger(GetType());

            if (AppSettings.GetBool(AppSettings.LoggingEfSecurityContextKey))
            {
                Database.Log = msg => logger.Debug(msg);
            }
        }

        public virtual int GetCurrentUser()
        {
            var query = Database.SqlQuery<int>("select oid from pg_roles where rolname = current_user"); //todo stored proc with rights of owner
            return query.FirstOrDefault();
        }

        public List<OperationGrant> GetOperationsACL()
        {
            // TODO: Get operations ACL
            return new List<OperationGrant>();
        }
    }
}
