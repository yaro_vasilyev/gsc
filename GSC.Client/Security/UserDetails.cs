﻿namespace GSC.Client.Security
{
    public class UserDetails
    {
        public int Id { get; set; }

        public string Role { get; set; }
    }
}