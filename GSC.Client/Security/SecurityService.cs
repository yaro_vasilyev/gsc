﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.EntityClient;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Login;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;
using log4net;
using log4net.Util;

namespace GSC.Client.Security
{
    internal class DbRoleBasedSecurityService : ISecurityService
    {
        private readonly IUnitOfWorkFactory<IGscContextUnitOfWork> uwFactory;
        private readonly List<string> roles = new List<string>();
        private User currentUser;
        private static readonly object Lock = new object();
        private static readonly ILog Log = LogManager.GetLogger(typeof (DbRoleBasedSecurityService));

        public DbRoleBasedSecurityService([NotNull] IUnitOfWorkFactory<IGscContextUnitOfWork> uwFactory)
        {
            if (uwFactory == null)
            {
                throw new ArgumentNullException(nameof(uwFactory));
            }
            this.uwFactory = uwFactory;
        }

        public User CurrentUser
        {
            get
            {
                // TODO: Добавить здесь повторное обращение к БД в случае если юзер не зарегистрирован в users (gsc_current_user будет возвращать null)
                if (currentUser == null)
                {
                    lock (Lock)
                    {
                        if (currentUser == null)
                        {
                            var context = uwFactory.CreateUnitOfWork().Context;
                            currentUser = context.GetCurrentUser().FirstOrDefault();
                            if (currentUser != null)
                            {
                                LoadUserRoles(currentUser.Role, roles);
                            }
                        }
                    }
                }
                return currentUser;
            }
        }

        public SecurityAction GetOperationSecurityAction([NotNull] string operation)
        {
            Debug.Assert(false == string.IsNullOrEmpty(operation), "Query for null/empty security operation.");

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            Log.DebugFormatExt("Query for operation: {0}", operation);

            if(CurrentUser == null)
                return SecurityAction.Deny;

            if (roles.Contains(operation))
            {
                return SecurityAction.Allow;
            }

            Log.WarnFormat("Операция {0} не разрешена для пользователя {1}", operation, CurrentUser.UserLogin);
            return SecurityAction.Deny;
        }

        private string connectionString;

        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            private set
            {
                if (connectionString != value)
                {
                    connectionString = value;
                    OnConnectionStringChanged(EventArgs.Empty);
                }
            }
        }

        public event EventHandler ConnectionStringChanged;

        protected void OnConnectionStringChanged(EventArgs ea)
        {
            ConnectionStringChanged?.Invoke(this, ea);
        }


        public bool CheckUserCredentials(Credentials credentials, bool throwOnFail)
        {
            var connStr = BuildConnectionString(credentials.Username, credentials.Password);
            try
            {
                using (var connection = CreateConnection(connStr))
                {
                    connection.Open();
                    Log.InfoFormatExt("User connected.");
                    ConnectionString = connStr;
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormatExt("Login failed. Exception: {0}", e.GetFullMessage());
                if (throwOnFail)
                {
                    throw;
                }
                return false;
            }
        }

        private void LoadUserRoles(string user, [NotNull] List<string> rolesCollection)
        {
            Log.DebugFormatExt("Loading roles for {0}", user);

            if (rolesCollection == null)
            {
                throw new ArgumentNullException(nameof(rolesCollection));
            }

            var context = uwFactory.CreateUnitOfWork().Context;
            rolesCollection.AddRange(context.GetUserRoles(user));
        }

        private static string BuildConnectionString(string username, string password)
        {
            // TODO: Refactor magic string
            var connectionString = ConfigurationManager.ConnectionStrings["GscContext"];

            var sb = new EntityConnectionStringBuilder(connectionString.ConnectionString);

            var providerFactory = DbProviderFactories.GetFactory(sb.Provider);

            var providerConnectionBuilder = providerFactory.CreateConnectionStringBuilder();
            Debug.Assert(providerConnectionBuilder != null, "providerConnectionBuilder != null");
            providerConnectionBuilder.ConnectionString = sb.ProviderConnectionString;
            providerConnectionBuilder["User Id"] = username;
            providerConnectionBuilder["Password"] = password;

            sb.ProviderConnectionString = providerConnectionBuilder.ToString();

            return sb.ToString();
        }

        private static DbConnection CreateConnection(string connectionString)
        {
            var connection = EntityProviderFactory.Instance.CreateConnection();
            Debug.Assert(connection != null);

            connection.ConnectionString = connectionString;

            return connection;
        }

    }

    public interface ISecurityService
    {
        User CurrentUser { get; }

        SecurityAction GetOperationSecurityAction(string operation);

        string ConnectionString { get; }

        event EventHandler ConnectionStringChanged;

        bool CheckUserCredentials(Credentials credentials, bool throwOnFail);
    }

    internal static class SecurityServiceExt
    {
        private static readonly object Lock = new object();

        public static bool QueryUserCredentials(this ISecurityService securityService, ISecurityDialogService securityDialogService, bool throwOnFail)
        {
            if (string.IsNullOrEmpty(securityService.ConnectionString))
            {
                lock (Lock)
                {
                    if (string.IsNullOrEmpty(securityService.ConnectionString))
                    {
                        for (int i = 0; i < 5; ++i)
                        {
                            Credentials cred;
                            if (!securityDialogService.ShowDialog(out cred))
                                break;

                            if (cred != null && securityService.CheckUserCredentials(cred, false))
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }
    }
}
