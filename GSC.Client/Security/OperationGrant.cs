namespace GSC.Client.Security
{
    public class OperationGrant
    {
        public string Operation { get; set; }

        public SecurityAction Action { get; set; }
    }
}