﻿using System.Windows.Forms;
using GSC.Client.Login;

namespace GSC.Client.Security
{
    class SecurityDialogService : ISecurityDialogService
    {
        public bool ShowDialog(out Credentials credentials)
        {
            credentials = null;
            using (var form = new LoginForm())
            {
                if (DialogResult.OK == form.ShowDialog())
                {
                    credentials = form.Credentials;
                    return true;
                }
                return false;
            }
        }
    }

    public interface ISecurityDialogService
    {
        bool ShowDialog(out Credentials credentials);
    }
}
