﻿// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public partial class Schedule : IProjectEntity
    {
        public string RegionName => Region?.Name;

        public string ExpertFullName => Expert?.FullName;

        public Project Project
        {
            get { return Expert?.Project; }
            set { }
        }

        public int ProjectId
        {
            get { return Project?.Id ?? -1; }
            set { }
        }
    }
}
