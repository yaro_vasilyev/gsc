﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public partial class ScheduleDetail
    {
        public string StationDisplayText => $"{Station?.DisplayText ?? ""}";
    }
}
