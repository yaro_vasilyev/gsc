﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ExpertEfficiency
    {
        [Display(Name = @"Отсутствие эффективности")]
        None = 0,
        [Display(Name = @"Низкая")]
        Low = 1,
        [Display(Name = @"Средняя")]
        Mid = 2,
        [Display(Name = @"Высокая")]
        High = 3
    }
}
