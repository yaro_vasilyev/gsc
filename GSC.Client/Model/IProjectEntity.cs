﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSC.Model;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public interface IProjectEntity<TProject, TProjectPrimaryKey>
        where TProject : class
    {
        TProject Project { get; set; }
        TProjectPrimaryKey ProjectId { get; set; }
    }

    public interface IProjectEntity : IProjectEntity<Project, int>
    {
    }
}
