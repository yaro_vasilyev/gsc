﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ExpertKind
    {
        [Display(Name = @"Член ГЭК")]
        Official = 1,
        [Display(Name = @"Общественный наблюдатель")]
        Social = 2
    }
}
