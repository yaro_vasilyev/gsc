﻿using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace GSC.Model
{
    [MetadataType(typeof (IContractInfo))]
    [UsedImplicitly]
    public partial class ContractInfo : IContractInfo, IProjectEntity
    {
        [UsedImplicitly]
        public string NumberDate => $"{Number} от {ContractDate.ToString("dd.MM.yyyy")}";
    }

    internal interface IContractInfo
    {
        [Required]
        [UsedImplicitly]
        string Number { get; set; }

        [Required]
        [UsedImplicitly]
        DateTime ContractDate { get; set; }
    }
}
