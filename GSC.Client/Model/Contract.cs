﻿using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace GSC.Model
{
    [MetadataType(typeof (IContract))]
    [UsedImplicitly]
    public partial class Contract : IContract, IProjectEntity
    {
        [UsedImplicitly]
        public string ContractDates => $"{DateFrom.ToString("dd.MM")}-{DateTo.ToString("dd.MM")}";

        public Project Project
        {
            get { return ContractInfo?.Project; }
            set { }
        }

        public int ProjectId
        {
            get { return Project?.Id ?? -1; }
            set { }
        }
    }

    internal interface IContract
    {
        [Required]
        [UsedImplicitly]
        PeriodType PeriodType { get; set; }

        [UsedImplicitly]
        [Required(ErrorMessage = @"Требуется выбрать график.")]
        int ScheduleId { get; set; }

        [Required]
        [UsedImplicitly]
        ContractType ContractType { get; set; }

        [Required(ErrorMessage = @"Требуется выбрать номер договора.")]
        [UsedImplicitly]
        string Number { get; set; }

        [Required]
        [UsedImplicitly]
        DateTime DateFrom { get; set; }

        [Required]
        [UsedImplicitly]
        DateTime DateTo { get; set; }

        [UsedImplicitly]
        decimal? Summ { get; set; }

        [UsedImplicitly]
        string Route { get; set; }

        [UsedImplicitly]
        byte[] Doc { get; set; }

        [UsedImplicitly]
        ContractInfo ContractInfo { get; set; }

        [UsedImplicitly]
        Schedule Schedule { get; set; }
    }
}