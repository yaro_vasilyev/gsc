﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ObserverType
    {
        [Display(Name = "Менеджер")]
        Manager = 1,
        [Display(Name = "Координатор РСМ")]
        Rsm = 2
    }
}
