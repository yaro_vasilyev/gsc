﻿namespace GSC.Model
{
    public enum PersonType
    {
        Expert = 0,
        Manager = 1,
        RsmManager = 2
    }
}