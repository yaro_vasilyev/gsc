﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum PeriodType
    {
        [Display(Name = "Досрочный")]
        Early = 1,
        [Display(Name = "Основной")]
        Primary = 2,
        [Display(Name = "Дополнительный")]
        Additional = 3
    }
}
