﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ControlZone
    {
        [Display(Name="Зеленая")]
        Green = 1,
        [Display(Name = "Желтая")]
        Yellow = 2,
        [Display(Name = "Красная")]
        Red = 3
    }
}
