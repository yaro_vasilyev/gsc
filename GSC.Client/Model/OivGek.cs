﻿namespace GSC.Model
{
    public partial class OivGek : IProjectEntity
    {
        public OivGekContact Oiv
        {
            get
            {
                return new OivGekContact
                {
                    Email = OivEmail,
                    Fio = OivFio,
                    Phone1 = OivPhone1,
                    Phone2 = OivPhone2,
                    Post = OivPost,
                    Workplace = OivWp
                };
            }
            set
            {
                OivEmail = value?.Email;
                OivFio = value?.Fio;
                OivPhone1 = value?.Phone1;
                OivPhone2 = value?.Phone2;
                OivPost = value?.Post;
                OivWp = value?.Workplace;
            }
        }

        public OivGekContact Gek
        {
            get
            {
                return new OivGekContact
                {
                    Email = GekEmail,
                    Fio = GekFio,
                    Phone1 = GekPhone1,
                    Phone2 = GekPhone2,
                    Post = GekPost,
                    Workplace = GekWp
                };
            }
            set
            {
                GekEmail = value?.Email;
                GekFio = value?.Fio;
                GekPhone1 = value?.Phone1;
                GekPhone2 = value?.Phone2;
                GekPost = value?.Post;
                GekWp = value?.Workplace;
            }
        }

        public OivGekContact Gia
        {
            get
            {
                return new OivGekContact
                {
                    Email = GiaEmail,
                    Fio = GiaFio,
                    Phone1 = GiaPhone1,
                    Phone2 = GiaPhone2,
                    Post = GiaPost,
                    Workplace = GiaWp
                };
            }
            set
            {
                GiaEmail = value?.Email;
                GiaFio = value?.Fio;
                GiaPhone1 = value?.Phone1;
                GiaPhone2 = value?.Phone2;
                GiaPost = value?.Post;
                GiaWp = value?.Workplace;
            }
        }

        public OivGekContact Nadzor
        {
            get
            {
                return new OivGekContact
                {
                    Email = NadzorEmail,
                    Fio = NadzorFio,
                    Phone1 = NadzorPhone1,
                    Phone2 = NadzorPhone2,
                    Post = NadzorPost,
                    Workplace = NadzorWp
                };
            }
            set
            {
                NadzorEmail = value?.Email;
                NadzorFio = value?.Fio;
                NadzorPhone1 = value?.Phone1;
                NadzorPhone2 = value?.Phone2;
                NadzorPost = value?.Post;
                NadzorWp = value?.Workplace;
            }
        }
    }
}