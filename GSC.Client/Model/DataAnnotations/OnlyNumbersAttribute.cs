﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GSC.Model.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class OnlyNumbersAttribute : ValidationAttribute
    {
        public int[] Items { get; }

        public OnlyNumbersAttribute(params int[] items)
        {
            Items = items;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var items = Items ?? new int[] { };
            if (!items.Contains(Convert.ToInt32(value)))
            {
                return new ValidationResult(string.IsNullOrEmpty(ErrorMessage)
                    ? $"Значение ({value}) не допустимо. Допустимые значения: {string.Join(", ", items)}."
                    : ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}
