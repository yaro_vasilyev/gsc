﻿using System;

namespace GSC.Model.DataAnnotations
{
    /// <summary>
    // Указывает, что свойство или поле является ключом импорта.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ImportKeyAttribute : Attribute
    {
    }
}
