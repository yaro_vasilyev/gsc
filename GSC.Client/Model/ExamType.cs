﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

// ReSharper disable once CheckNamespace

namespace GSC.Model
{
    [Serializable]
    public enum ExamType
    {
        [Display(Name = @"ЕГЭ"), XmlEnum("1")] Ege = 1,
        [Display(Name = @"ГВЭ"), XmlEnum("2")] Gve = 2,
        [Display(Name = @"ОГЭ"), XmlEnum("3")] Oge = 3
    }
}