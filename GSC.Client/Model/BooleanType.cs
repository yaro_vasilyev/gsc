﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

// ReSharper disable once CheckNamespace

namespace GSC.Model
{
    [Serializable]
    public enum BooleanType
    {
        [Display(Name = @"Нет"), XmlEnum("0")] No = 0,
        [Display(Name = @"Да"), XmlEnum("1")] Yes = 1
    }
}