﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using log4net;
using GSC.Client.Util;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public partial class GscContext
    {
        public GscContext(DbConnection connection) : base(connection, false)
        {
            InitializeLogging();
        }

        public GscContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            InitializeLogging();
        }

        private void InitializeLogging()
        {
            var logger = LogManager.GetLogger(GetType());

            if (AppSettings.GetBool(AppSettings.LoggingEfGscContextKey))
            {
                Database.Log = msg => logger.Debug(msg);
            }
        }
    }
}
