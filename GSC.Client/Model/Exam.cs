﻿using System;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace

namespace GSC.Model
{
    public partial class Exam : IProjectEntity
    {
        [UsedImplicitly]
        public string DisplayText => $"{ExamDate:d} {Subject?.Name}";

        [UsedImplicitly]
        public string SubjectName => $"{Subject?.Name}";

        public Project Project
        {
            get { return Subject?.Project; }
            set { }
        }

        public int ProjectId
        {
            get { return Project?.Id ?? -1; }
            set { }
        }
    }
}