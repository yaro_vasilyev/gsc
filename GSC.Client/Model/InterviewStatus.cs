﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum InterviewStatus
    {
        [Display(Name = "Не прошел")]
        Fail = 0,
        [Display(Name = "Прошел")]
        Success = 1
    }
}
