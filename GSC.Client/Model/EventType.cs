﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GSC.Model
{
    public enum EventType
    {
        [Display(Name = "Консультация")]
        Consult = 1,
        [Display(Name = "Пожелание")]
        Wish = 2,
        [Display(Name = "Нештатная ситуация")]
        Emergency = 3,
        [Display(Name = "Информация")]
        Info = 4
    }
}
