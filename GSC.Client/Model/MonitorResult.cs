﻿namespace GSC.Model
{
    public partial class MonitorResult : IProjectEntity
    {
        public Project Project
        {
            get { return Schedule?.Expert?.Project; }
            set { }
        }

        public int ProjectId
        {
            get { return Project?.Id ?? -1; }
            set { }
        }
    }
}
