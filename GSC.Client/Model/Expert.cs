﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GSC.Model.DataAnnotations;
using JetBrains.Annotations;
using static GSC.Client.Constants;

// ReSharper disable once CheckNamespace

namespace GSC.Model
{
    [MetadataType(typeof (IExpert))]
    public partial class Expert : IExpert, IProjectEntity
    {
        public string FullName => $"{Surname} {Name} {Patronymic}";

        [UsedImplicitly]
        public bool HasLogin => UserGuid != null;

        public static ValidationResult ValidateBaseRegion(int baseRegionId)
            => baseRegionId > 0 ? ValidationResult.Success : new ValidationResult(@"Не указан базовый субъект");
    }

    public interface IExpert
    {
        [Required, UsedImplicitly]
        int Id { get; set; }

        [Required(ErrorMessage = @"Не указан учетный период."), UsedImplicitly]
        int ProjectId { get; set; }

        [UsedImplicitly]
        string ImportCode { get; set; }

        [Required(ErrorMessage = @"Не задана фамилия."), UsedImplicitly]
        string Surname { get; set; }

        [Required(ErrorMessage = @"Не задано имя."), UsedImplicitly]
        string Name { get; set; }

        [UsedImplicitly]
        string Patronymic { get; set; }

        //[Required(ErrorMessage = "Не указано место работы.")]
        [UsedImplicitly]
        string Workplace { get; set; }

        //[Required(ErrorMessage = "Не указана должность.")]
        [UsedImplicitly]
        string Post { get; set; }

        //[Required(ErrorMessage = "Не задана оценка по критерию Должность.")]
        [Range(1, 5, ErrorMessage = @"Допустимые значнения: 1-5"), UsedImplicitly]
        int PostValue { get; set; }

        //[Required(ErrorMessage = "Не задана дата рождения.")]
        [UsedImplicitly]
        DateTime? BirthDate { get; set; }

        //[Required(ErrorMessage = "Не указано место рождения")]
        [UsedImplicitly]
        string BirthPlace { get; set; }

        //[Required(ErrorMessage = "Не указан адрес проживания.")]
        [UsedImplicitly]
        string LiveAddress { get; set; }

        //[Required(ErrorMessage = "Не указан телефон.")]
        [UsedImplicitly]
        string Phone { get; set; }

        [Required(ErrorMessage = @"Не указана электронная почта."),
         RegularExpression(EmailRegExPatt, ErrorMessage = @"Электронная почта в неверном формате."), UsedImplicitly]
        string Email { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Ученая степень.")]
        [OnlyNumbers(0, 5), UsedImplicitly]
        int AcademicDegreeValue { get; set; }

        /// <summary>
        /// TODO: Значение поля должно браться из справочника
        /// </summary>
        [Required(ErrorMessage = @"Не задана оценка по критерию Ученое звание.")]
        [Range(0, 5, ErrorMessage = Range0To5), UsedImplicitly]
        int AcademicTitleValue { get; set; }

        //[Required(ErrorMessage = "Не указан опыт аналогичной работы.")]
        [UsedImplicitly]
        string Expirience { get; set; }

        //[Required(ErrorMessage = "Не указано образование (специализация).")]
        [UsedImplicitly]
        string Speciality { get; set; }

        [OnlyNumbers(0, 5), UsedImplicitly]
        //[Required(ErrorMessage = "Не задана оценка по критерию Образование(специализация).")]
        int SpecialityValue { get; set; }

        //[Required(ErrorMessage = "Не указан общий стаж работы.")]
        [Range(0, int.MaxValue, ErrorMessage = OnlyPositives), UsedImplicitly]
        int WorkYearsTotal { get; set; }

        //[Required(ErrorMessage = "Не указан стаж работы в системе образования.")]
        [Range(0, int.MaxValue, ErrorMessage = OnlyPositives), UsedImplicitly]
        int WorkYearsEducation { get; set; }

        //[Required(ErrorMessage = "Не задана оценка по критерию Стаж работы в системе образования.")]
        [Range(1, 5, ErrorMessage = @"Допустимые значения: 1-5."), UsedImplicitly]
        int WorkYearsEducationValue { get; set; }

        //[Required(ErrorMessage = "Стаж руководящей работы.")]
        [Range(0, int.MaxValue, ErrorMessage = OnlyPositives), UsedImplicitly]
        int WorkYearsChief { get; set; }

        //[Required(ErrorMessage = "Не указан Опыт работы в системе образования.")]
        [Range(0, int.MaxValue, ErrorMessage = OnlyPositives), UsedImplicitly]
        int ExpYearsEducation { get; set; }

        //[Required(ErrorMessage = "Не задана оценка по критерию Опыт работы в системе образования.")]
        [Range(1, 5, ErrorMessage = @"Допустимые значения: 1-5."), UsedImplicitly]
        int ExpYearsEducationValue { get; set; }

        [Required(ErrorMessage = @"Не указаны Ведомственные награды и звания.", AllowEmptyStrings = true),
         UsedImplicitly]
        string Awards { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Ведомственные награды и звания.")]
        [OnlyNumbers(0, 5), UsedImplicitly]
        int AwardsValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Готовность к выезду в другой субъект РФ.")]
        [OnlyNumbers(0, 5), UsedImplicitly]
        int CanMissionValue { get; set; }

        //[Required(ErrorMessage = "Не указано ФИО руководителя.")]
        [UsedImplicitly]
        string ChiefName { get; set; }

        //[Required(ErrorMessage = "Не указана должность руководителя.")]
        [UsedImplicitly]
        string ChiefPost { get; set; }

        //[Required(ErrorMessage = "Не укзазан адрес электронной почты руководителя.")]
        [RegularExpression(EmailRegExPatt, ErrorMessage = @"Электронная почта в неверном формате."),
         UsedImplicitly]
        string ChiefEmail { get; set; }

        [EnumDataType(typeof (ExpertKind), ErrorMessage = @"Не указана категория эксперта"),
         UsedImplicitly]
        ExpertKind Kind { get; set; }

        [Required(ErrorMessage = @"Не задана Готовность к выезду в другой субъект РФ."), UsedImplicitly]
        BooleanType CanMission { get; set; }

        [CustomValidation(typeof (Expert), nameof(Expert.ValidateBaseRegion)), UsedImplicitly]
        int BaseRegionId { get; set; }

        /*[Required(ErrorMessage = "Не указан куратор.")]*/
        /* теперь необязательный, т.к. он импортится без менеджера */

        [UsedImplicitly]
        int? ObserverId { get; set; }

        [Range(0, 23, ErrorMessage = @"Допустимые значения: 0-23."), UsedImplicitly]
        int? TestResults { get; set; }

        [Range(0, 5, ErrorMessage = @"Допустимые значения: 0-5."), UsedImplicitly]
        int? TestResultsValue { get; set; }

        [UsedImplicitly]
        InterviewStatus? InterviewStatus { get; set; }

        /// <summary>
        /// 0,5
        /// </summary>
        [OnlyNumbers(0, 5), UsedImplicitly]
        int? InterviewStatusValue { get; set; }

        [UsedImplicitly]
        // ReSharper disable once InconsistentNaming
        int? Stage1points { get; set; }

        [UsedImplicitly]
        BooleanType? Engage { get; set; }

        // int reports
        // int reports_value

        [Required(ErrorMessage = @"Не задана оценка по критерию Компетентный уровень.")]
        [Range(0, 5, ErrorMessage = Range0To5), UsedImplicitly]
        int SubjectLevelValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Уровень аккуратности.")]
        [Range(0, 5, ErrorMessage = Range0To5), UsedImplicitly]
        int AccuracyLevelValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Уровень пунктуальности.")]
        [OnlyNumbers(0, 1, 3, 5), UsedImplicitly]
        int PunctualLevelValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Уровень независимости.")]
        [Range(0, 5, ErrorMessage = Range0To5), UsedImplicitly]
        int IndependencyLevelValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Высокие моральные качества.")]
        [Range(0, 5, ErrorMessage = Range0To5), UsedImplicitly]
        int MoralLevelValue { get; set; }

        [Required(ErrorMessage = @"Не задана оценка по критерию Участие в усовершенствовании НПД.")]
        [OnlyNumbers(0, 5), UsedImplicitly]
        int DocsImprovementParticipationLevelValue { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = OnlyPositives), UsedImplicitly]
        // ReSharper disable once InconsistentNaming
        int? Stage2points { get; set; }

        [UsedImplicitly]
        ExpertEfficiency? Efficiency { get; set; }

        [UsedImplicitly]
        ProposalType? NextPeriodProposal { get; set; }

        [Required(ErrorMessage = @"Не указано учёное звание."), UsedImplicitly]
        int? AcademicTitleId { get; set; }

        [Required(ErrorMessage = @"Не указана учёная степень."), UsedImplicitly]
        int? AcademicDegreeId { get; set; }

        [DisplayName(@"Базовый субъект")]
        [UsedImplicitly]
        Region BaseRegion { get; set; }

        [UsedImplicitly]
        Observer Observer { get; set; }

        [DisplayName(@"Учёная степень")]
        [UsedImplicitly]
        AcademicDegree AcademicDegree { get; set; }

        [DisplayName(@"Учёное звание")]
        [UsedImplicitly]
        AcademicTitle AcademicTitle { get; set; }

        [UsedImplicitly]
        Project Project { get; set; }
    }
}