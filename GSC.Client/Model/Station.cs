﻿namespace GSC.Model
{
    public partial class Station : IProjectEntity
    {
        public string DisplayText => $"{Code}, {Name}, {Address}";

        public string RegionName => $"{Region?.Name}";
    }
}
