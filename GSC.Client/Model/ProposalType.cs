﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    /// <summary>
    /// Тип включения в БД следующего учетного периода
    /// </summary>
    public enum ProposalType
    {
        [Display(Name = "Основной")]
        Main = 1,
        [Display(Name = "Резерв")]
        Reserve = 2,
        [Display(Name = "Исключить")]
        Exclude = 3
    }
}
