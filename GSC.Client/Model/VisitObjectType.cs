﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Model
{
    public enum VisitObjectType
    {
        [Display(Name = "ППЭ")]
        PPE = 1,
        [Display(Name = "РЦОИ")]
        RCOI = 2,
        [Display(Name = "КК")]
        KK = 3,
        [Display(Name = "ППЗ")]
        PPZ = 4
    }
}
