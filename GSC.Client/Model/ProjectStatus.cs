﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ProjectStatus : int
    {
        [Display(Name = "Открыт")]
        Opened = 1,
        [Display(Name = "Закрыт")]
        Closed = 2
    }
}
