﻿using System.ComponentModel.DataAnnotations;

namespace GSC.Model
{
    [MetadataType(typeof(IAcademicTitle))]
    public partial class AcademicTitle : IAcademicTitle
    {
    }

    public interface IAcademicTitle
    {
        int Id { get; set; }
        [Required]
        string Code { get; set; }
        [Required]
        string Name { get; set; }

        [Range(0, 5, ErrorMessage = "Допустимые значения: 0-5.")]
        int DefaultValue { get; set; }
    }
}
