﻿namespace GSC.Model
{
    public partial class Event : IProjectEntity
    {
        public Project Project
        {
            get { return Schedule?.Expert?.Project; }
            set { }
        }

        public int ProjectId
        {
            get { return Project?.Id ?? -1; }
            set { }
        }
    }
}
