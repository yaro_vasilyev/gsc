﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Model
{
    public enum ChannelType
    {
        [Display(Name = "СМС")]
        SMS = 1,
        [Display(Name = "Сотовый телефон")]
        Mobile = 2,
        [Display(Name = "Email")]
        Email = 3,
        [Display(Name = "Телефон")]
        Phone = 4
    }
}
