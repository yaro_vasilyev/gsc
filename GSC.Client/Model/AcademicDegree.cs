﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;
using JetBrains.Annotations;

namespace GSC.Model
{
    [MetadataType(typeof(IAcademicDegree))]
    [DebuggerDisplay("Id={Id}, Code={Code}, Name={Name}")]
    partial class AcademicDegree : IAcademicDegree
    {
    }

    interface IAcademicDegree
    {
#warning По доку Code - это int(2) и ключ
        [Required(ErrorMessage = "Не заполнен Код")]
        [UsedImplicitly]
        string Code { get; set; }

        [UsedImplicitly]
        [Required(ErrorMessage = "Не заполнено Наименование")]
        string Name { get; set; }
    }
}
