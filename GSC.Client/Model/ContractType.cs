﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace GSC.Model
{
    public enum ContractType
    {
        [Display(Name = @"Договор безвозмездного оказания услуг")]
        Unpaid = 0,
        [Display(Name = @"Договор возмездного оказания услуг")]
        Paid = 1
    }
}
