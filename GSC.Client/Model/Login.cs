﻿using System;
using System.Linq;
using GSC.Client.Util;
using JetBrains.Annotations;
using static System.String;

namespace GSC.Model
{
    public partial class Login
    {
        [UsedImplicitly]
        public string AllRolesDescriptions
        {
            get
            {
                var result =
                    UserRoles.Select(ur => ur.Role.Description ?? "[" + ur.Role.SystemName + "]").JoinNonEmpty();
                return !IsNullOrWhiteSpace(result) ? result : "[Роли не назначены]";
            }
        }

        /// <summary>
        /// Привязка к пользователю - эксперту или менеджеру. Формат: ТИП ФИО. Например: Эксперт Иванов Иван Иванович.
        /// </summary>
        [UsedImplicitly]
        public string PersonName
        {
            get
            {
                string perType;
                string fio;
                if (Observer != null)
                {
                    switch (Observer.Type)
                    {
                        case ObserverType.Manager:
                            perType = "Менеджер";
                            break;
                        case ObserverType.Rsm:
                            perType = "Куратор РСМ";
                            break;
                        default:
                            perType = Observer.Type.ToString();
                            break;
                    }
                    fio = Observer.Fio;
                }
                else if (Expert != null)
                {
                    perType = "Эксперт";
                    fio = $"{Expert.FullName} (проект {Expert.Project.Title})";
                }
                else
                {
                    return "[Пользователь не привязан]";
                }

                return new[] {perType, fio}.JoinNonEmpty(" ");
            }
        }

        [UsedImplicitly]
        public int? PersonId => ExpertId ?? ObserverId;

        [UsedImplicitly]
        public PersonType? PersonType
        {
            get
            {
                if (Expert != null) return Model.PersonType.Expert;
                if (Observer == null) return null;
                switch (Observer.Type)
                {
                    case ObserverType.Manager:
                        return Model.PersonType.Manager;
                    case ObserverType.Rsm:
                        return Model.PersonType.RsmManager;
                    default:
                        return null;
                }
            }
        }

        public void SetPerson(PersonType personType, int? personId)
        {
            switch (personType)
            {
                case Model.PersonType.Expert:
                    ExpertId = personId;
                    ObserverId = null;
                    break;
                case Model.PersonType.Manager:
                case Model.PersonType.RsmManager:
                    ObserverId = personId;
                    ExpertId = null;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(personType), personType,
                        @"Недопустимое значение типа персоны привязки (PersonType)");
            }
        }
    }
}