﻿using System.Linq;
using System.Text.RegularExpressions;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Exec
{
    public class ExecViewModel : ProjectEntityViewModel<Model.Exec>
    {
        /// <summary>
        /// Creates a new instance of ExecViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExecViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExecViewModel(unitOfWorkFactory));
        }

        private static string StationDisplayText(Model.Station station)
        {
            var original = station.Name;
            var name = Regex.Replace(original,
                                     @"Муниципальное\s+бюджетное\s+общеобразовательное\s+учреждение",
                                     "МБОУ",
                                     RegexOptions.IgnoreCase);
            name = Regex.Replace(name,
                                 @"Средняя\s+общеобразовательная\s+школа",
                                 "СОШ",
                                 RegexOptions.IgnoreCase);
            return name;
        }

        private static string EntityDisplayText(Model.Exec entity)
        {
            return string.Format("{0} @ {1}",
                                 entity?.Exam?.DisplayText,
                                 StationDisplayText(entity?.Station));
        }

        /// <summary>
        /// Initializes a new instance of the ExecViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExecViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExecViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                   x => x.Execs, EntityDisplayText) {}

        public IEntitiesViewModel<Model.Region> LookupRegions
        {
            get { return GetLookUpProjectEntitiesViewModel((ExecViewModel vm) => vm.LookupRegions, x => x.Regions); }
        }

        public IEntitiesViewModel<Model.Station> LookupStations
        {
            get { return GetLookUpProjectEntitiesViewModel((ExecViewModel vm) => vm.LookupStations, x => x.Stations, StationProjection); }
        }

        private IQueryable<Model.Station> StationProjection(IRepositoryQuery<Model.Station> query)
        {
            return from s in query
                   where s.ProjectId == CurrentProject.Id && s.RegionId == RegionId
                   select s;
        }

        public IEntitiesViewModel<Model.Exam> LookupExams
        {
            get { return GetLookUpEntitiesViewModel((ExecViewModel vm) => vm.LookupExams, x => x.Exams, ExamProjection); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>I know about <see cref="DevExpress.Mvvm.POCO.POCOViewModelExtensions.GetParentViewModel{T}(object)"/></remarks>
        private ExecCollectionViewModel GetParentViewModel()
        {
            var support = this as ISupportParentViewModel;
            return support?.ParentViewModel as ExecCollectionViewModel;
        }

        protected virtual IQueryable<Model.Exam> ExamProjection(IRepositoryQuery<Model.Exam> query)
        {
            return GetParentViewModel()?.ExamProjection(query);
        }

        private int regionId = 0;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRegionIdChanged))]
        public virtual int RegionId
        {
            get
            {
                if (regionId == 0)
                {
                    regionId = Entity?.Station?.RegionId ?? 0;
                }
                return regionId;
            }
            set
            {
                if (value != regionId)
                {
                    regionId = value;
                    this.RaisePropertyChanged(x => x.RegionId);
                }
            }
        }

        protected virtual void OnRegionIdChanged()
        {
            StationId = 0;
            RefreshLookUpCollection((ExecViewModel vm) => vm.LookupStations, true);
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStationIdChanged))]
        public virtual int StationId
        {
            get { return Entity.StationId; }
            set
            {
                if (Entity.StationId != value)
                {
                    Entity.StationId = value;
                    this.RaisePropertyChanged(x => x.StationId);
                }
            }
        }

        protected virtual void OnStationIdChanged()
        {
            Update();
        }

        protected override string LocalizedEntityName => GetParentViewModel()?.EntityName ?? "Проведение экзамена";

        protected override string SNewOperation => SecurityOperations.Execs.New;

        protected override string SEditOperation => SecurityOperations.Execs.Edit;

        protected override string SDeleteOperation => SecurityOperations.Execs.Delete;
    }
}
