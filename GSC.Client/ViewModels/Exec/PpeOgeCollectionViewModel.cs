﻿using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels.Exec
{
    public class PpeOgeCollectionViewModel : ExecCollectionViewModel
    {
        /// <summary>
        /// Creates a new instance of PpeOgeCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static PpeOgeCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PpeOgeCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the PpeEgeCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the PpeEgeCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public PpeOgeCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), ExecInfo.ExecOgeCollectionProjection) { }

        public override IQueryable<Model.Exam> ExamProjection(IRepositoryQuery<Model.Exam> query)
        {
            return query.Include(x => x.Subject).Where(x => x.Subject.ProjectId == CurrentProject.Id && x.ExamType == Model.ExamType.Oge);
        }

        public override string EntityName => "ОГЭ";
    }
}
