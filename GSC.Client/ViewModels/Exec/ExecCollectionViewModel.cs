﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Exec
{
    public abstract class ExecCollectionViewModel
        : ProjectInstantFeedbackCollectionViewModel<Model.Exec>
    {
        /// <summary>
        /// Initializes a new instance of the ExecCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the RegionCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        /// <param name="projection"></param>
        protected ExecCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
                                          Func<IRepositoryQuery<Model.Exec>, IQueryable<Model.Exec>> projection)
            : base(unitOfWorkFactory, x => x.Execs, projection)
        {
            FilterExpression = e => e.Exam.Subject.ProjectId == CurrentProject.Id;
        }

        [UsedImplicitly]
        public void Import()
        {
            this.EnsureOperationAllowed(SecurityOperations.Execs.ImportFromCsv); //todo import from Xml?

            var unitOfWork = CreateUnitOfWork();
            var gscContext = unitOfWork.Context;
            var sessionGuid = Guid.NewGuid();

            Action<XmlExecution> importRowAction = xmlExecution =>
                gscContext.ImportRowExec(sessionGuid,
                                         xmlExecution.Region,
                                         xmlExecution.StationCode,
                                         xmlExecution.ExamGlobalId,
                                         BooleanType.Yes.Equals(xmlExecution.Kim) ? 1 : 0,
                                         xmlExecution.CountPart,
                                         xmlExecution.CountRoom,
                                         xmlExecution.CountRoomVideo,
                                         (int?) xmlExecution.ExamType);

            Func<IEnumerable<ImportExecResult>> importFunc =
                () => gscContext.ImportExec(sessionGuid, CurrentProject.Id);

            Func<ImportExecResult, string> resultToStringFunc =
                result => $"Код станции (ППЭ) {result.StationCode},\t\"{result.Exam}\"";

            var importService = this.GetRequiredService<IXmlImportService>();
            importService.Import("Executions", importRowAction, importFunc, resultToStringFunc, this);
        }

        /// <summary>
        /// Example: 
        /// <code><![CDATA[<Execution Region="78" StationCode="1818" TestType="1" ExamGlobalID="63"
        /// StationFlags="292" ParticipantPPECount="139" AuditoriumsPPECount="9" AuditoriumsPPEVideoCount="9" />]]></code>
        /// </summary>
        [XmlType("Execution")]
        public class XmlExecution
        {
            [XmlAttribute("Region")]
            public int Region { get; set; }

            [XmlAttribute("StationCode")]
            public int StationCode { get; set; }

            [XmlAttribute("TestType")]
            public ExamType ExamType { get; set; }

            [XmlAttribute("ExamGlobalID")]
            public int ExamGlobalId { get; set; }

            [XmlAttribute("StationFlags")]
            public int Kim { get; set; }

            [XmlAttribute("ParticipantPPECount")]
            public int CountPart { get; set; }

            [XmlAttribute("AuditoriumsPPECount")]
            public int CountRoom { get; set; }

            [XmlAttribute("AuditoriumsPPEVideoCount")]
            public int CountRoomVideo { get; set; }
        }

        /*
                [UsedImplicitly]
                private class ImpExec : Model.Exec
                {
                    public int ImpRegion { get; [UsedImplicitly] set; }
                    public int ImpStationCode { get; [UsedImplicitly] set; }
                    public int ImpExamGlobalId { get; [UsedImplicitly] set; }
                    public ExamType ImpExamType { get; [UsedImplicitly] set; }
                }
        */

        /*
                [UsedImplicitly]
                private sealed class ExecCsvMap : CsvClassMap<ImpExec>
                {
                    public ExecCsvMap()
                    {
                        Map(exec => exec.ImpRegion).Name("Region").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.ImpStationCode).Name("StationCode").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.ImpExamGlobalId).Name("ExamGlobalID").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.Kim).Name("StationFlags").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.CountPart).Name("ParticipantPPECount").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.CountRoom).Name("AuditoriumsPPECount").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.CountRoomVideo).Name("AuditoriumsPPEVideoCount").TypeConverterOption(NumberStyles.Integer);
                        Map(exec => exec.ImpExamType).Name("TestType").TypeConverterOption(NumberStyles.Integer);
                    }
                }
        */

        [UsedImplicitly]
        public bool CanImport()
        {
            return this.OperationAllowed(SecurityOperations.Execs.ImportFromCsv);
        }

        public abstract IQueryable<Model.Exam> ExamProjection(IRepositoryQuery<Model.Exam> query);

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Execs.ExportToExcel);
            var exportToExcelService = this.GetRequiredService<IExportToExcelService>();
            exportToExcelService.Export(control);
        }

        [UsedImplicitly]
        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Execs.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.Execs.New;

        protected override string SEditOperation => SecurityOperations.Execs.Edit;

        protected override string SDeleteOperation => SecurityOperations.Execs.Delete;
    }
}