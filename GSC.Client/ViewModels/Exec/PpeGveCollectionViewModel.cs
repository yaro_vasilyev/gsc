﻿using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels.Exec
{
    public class PpeGveCollectionViewModel : ExecCollectionViewModel
    {
        /// <summary>
        /// Creates a new instance of PpeGveCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static PpeGveCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PpeGveCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the PpeGveCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the PpeGveCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public PpeGveCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), ExecInfo.ExecGveCollectionProjection) { }

        public override IQueryable<Model.Exam> ExamProjection(IRepositoryQuery<Model.Exam> query)
        {
            return query.Include(x => x.Subject).Where(x => x.Subject.ProjectId == CurrentProject.Id && x.ExamType == Model.ExamType.Gve);
        }

        public override string EntityName => "ГВЭ";
    }
}
