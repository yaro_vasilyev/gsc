﻿using System.Collections.Generic;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Database.Objects.Stations;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    public class ObjectsViewModel : GscDocumentsViewModel
    {
        public const string ModuleRegions = "Регионы";
        public const string ModulePpeEge = "ЕГЭ";
        public const string ModulePpeGve = "ГВЭ";
        public const string ModulePpeOge = "ОГЭ";
        public const string ModuleRcoi = "РЦОИ";
        public const string ModuleOivGek = "ОИВ/ГЭК";
        public const string ModuleExams = "Расписание";
        public const string ModuleStations = "ППЭ";



        public ObjectsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        { }

        protected override NavigationModuleDescription[] CreateModules()
        {
            var modules = new List<NavigationModuleDescription>();
            var securityService = this.GetRequiredService<ISecurityService>();

            if (securityService.CanLoadModule(SecurityOperations.Regions.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleRegions,
                    "RegionCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Regions)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Exams.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModulePpeEge,
                    "PpeEgeCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Execs)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Exams.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModulePpeGve,
                    "PpeGveCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Execs)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Exams.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModulePpeOge,
                    "PpeOgeCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Execs)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Exams.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleRcoi,
                    "RcoiCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Rcois)));
            }
            if (securityService.CanLoadModule(SecurityOperations.OivGeks.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleOivGek,
                    "OivGekCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.OivGeks)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Exams.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleExams,
                    "ExamCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Exams)));
            }
            if (securityService.CanLoadModule(SecurityOperations.Stations.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleStations,
                                                            nameof(StationCollectionView),
                                                            GetPeekCollectionViewModelFactory(x => x.Stations)));
            }

            return modules.ToArray();
        }
    }
}
