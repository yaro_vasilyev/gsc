﻿using System.Collections.Generic;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    public class ViolationsViewModel : GscDocumentsViewModel
    {

        public ViolationsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        protected override NavigationModuleDescription[] CreateModules()
        {
            var modules = new List<NavigationModuleDescription>();
            var securityService = this.GetRequiredService<ISecurityService>();

            if (securityService.OperationAllowed(SecurityOperations.Violations.LoadModule))
            {
                modules.Add(new NavigationModuleDescription("Нарушения",
                    "ViolationCollectionView",
                    m => GetPeekCollectionViewModelFactory(x => x.Violations)));
            }
            if (securityService.OperationAllowed(SecurityOperations.Violators.LoadModule))
            {
                modules.Add(new NavigationModuleDescription("Нарушители",
                    "ViolatorCollectionView",
                    m => GetPeekCollectionViewModelFactory(x => x.Violators)));
            }
            return modules.ToArray();
        }
    }
}
