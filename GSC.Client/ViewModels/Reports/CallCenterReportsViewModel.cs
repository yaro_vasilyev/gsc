﻿using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports.CallCenter;

namespace GSC.Client.ViewModels.Reports
{
    [POCOViewModel]
    public class CallCenterReportsViewModel : ReportsViewModel
    {
        private static readonly ReportCategoryInfo callCenterReportsCategoryInfo = new ReportCategoryInfo
        {
            DisplayName = "Работа «горячей линии»"
        };

        public CallCenterReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        public override List<ReportCategoryInfo> Categories => new List<ReportCategoryInfo>
        {
            callCenterReportsCategoryInfo
        };

        public override List<ReportInfo> Reports => new List<ReportInfo>
        {
            new ReportInfo
            {
                DisplayName = "Распределение обращений между специалистами «горячей линии»",
                Category = callCenterReportsCategoryInfo,
                ReportType = typeof (Rep167CallCentSpecDisp)
            },
            new ReportInfo
            {
                DisplayName = "Количество обращений по субъектам Российской Федерации",
                Category = callCenterReportsCategoryInfo,
                ReportType = typeof (Rep108CountCallOfRegions)
            },
            new ReportInfo
            {
                DisplayName = "Общее количество обращений на «горячую линию»",
                Category = callCenterReportsCategoryInfo,
                ReportType = typeof (Rep168EventsCount),
                RequestParametersBeforeReportExecute = true
            }
        };
    }
}