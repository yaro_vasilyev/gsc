﻿using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports.Database;

namespace GSC.Client.ViewModels.Reports
{
    [POCOViewModel]
    public class DatabaseReportsViewModel : ReportsViewModel
    {
        private static readonly ReportCategoryInfo databaseReportsCategoryInfo = new ReportCategoryInfo
        {
            DisplayName = "База данных"
        };

        public DatabaseReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        public override List<ReportCategoryInfo> Categories => new List<ReportCategoryInfo>
        {
            databaseReportsCategoryInfo
        };

        public override List<ReportInfo> Reports => new List<ReportInfo>
        {
            new ReportInfo
            {
                DisplayName = "Перечень экспертов по мониторингу ГИА",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof (Rep060ExpertList)
            },
            new ReportInfo
            {
                DisplayName = "Контактная информация по субъекту",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Rep064RegionContacts),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Критериальная таблица для формирования базы экспертовпо мониторингу проведения ГИА",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Rep065CriterialTable),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Критериальная таблица для формирования оценки эффективности работы экспертов, принимавших участие в мониторинге по проведению государственной итоговой аттестации по образовательным программам основного общего и среднего общего образования",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Rep066CriterialTableMarks),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Контактные данные эксперта для оформления проездных документов и бронирования номера в гостинице проживания",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Rep067ExpertContact)
            },
            new ReportInfo
            {
                DisplayName = "Информационная карта эксперта",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Rep068InfoCardExpert),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Список экспертов для формирования писем работодателям",
                Category = databaseReportsCategoryInfo,
                ReportType = typeof(Experts4EmailRep)
            }
        };
    }
}