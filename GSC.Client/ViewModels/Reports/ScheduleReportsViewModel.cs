﻿using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports.Schedule;

namespace GSC.Client.ViewModels.Reports
{
    [POCOViewModel]
    public class ScheduleReportsViewModel : ReportsViewModel
    {
        private static readonly ReportCategoryInfo scheduleReportsCategoryInfo = new ReportCategoryInfo
        {
            DisplayName = "График выходов"
        };

        public ScheduleReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        public override List<ReportCategoryInfo> Categories => new List<ReportCategoryInfo>
        {
            scheduleReportsCategoryInfo
        };

        public override List<ReportInfo> Reports => new List<ReportInfo>
        {
            new ReportInfo
            {
                DisplayName = "Количество экспертов по субъектам контроля",
                Category = scheduleReportsCategoryInfo,
                ReportType = typeof (Rep061ExpertsInRegions)
            },
            new ReportInfo
            {
                DisplayName = "График выходов (выездов) экспертов по мониторингу ГИА",
                Category = scheduleReportsCategoryInfo,
                ReportType = typeof (Rep062Schedule)
            },
            new ReportInfo
            {
                DisplayName = "Сводный отчёт по выходам экспертов на мониторинг ППЭ по дате проведения экзаменов",
                Category = scheduleReportsCategoryInfo,
                ReportType = typeof (Rep063AllPpeVisits),
                RequestParametersBeforeReportExecute = true
            }
        };
    }

}
