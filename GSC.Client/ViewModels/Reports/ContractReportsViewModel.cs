﻿using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports.Contracts;

namespace GSC.Client.ViewModels.Reports
{

    [POCOViewModel]
    public class ContractReportsViewModel : ReportsViewModel
    {
        private static readonly ReportCategoryInfo contractReportsCategoryInfo = new ReportCategoryInfo
        {
            DisplayName = "Договоры с экспертами"
        };

        public ContractReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        public override List<ReportCategoryInfo> Categories => new List<ReportCategoryInfo>
        {
            contractReportsCategoryInfo
        };

        public override List<ReportInfo> Reports => new List<ReportInfo>
        {
            new ReportInfo
            {
                DisplayName = "Реестр договоров по присвоенным номерам",
                Category = contractReportsCategoryInfo,
                ReportType = typeof (Rep104ContractReestr)
            },
        };
    }

}
