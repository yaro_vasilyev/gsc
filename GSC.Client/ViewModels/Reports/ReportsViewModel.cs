﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraReports.UI;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports;
using GSC.Client.Services;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Reports
{
    public abstract class ReportsViewModel : GscDocumentsViewModel
    {
        protected ReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
            UnitOfWork = CreateUnitOfWork();
        }

        public Model.Project CurrentProject
        {
            get
            {
                return this.GetRequiredService<IProjectService>().GetCurrentProject().Match(x => x, 
                    e =>
                    {
                        throw e; // TODO: Wrap exception here?
                    });
            }
        }

        [UsedImplicitly]
        public abstract List<ReportCategoryInfo> Categories { get; }

        [UsedImplicitly]
        public abstract List<ReportInfo> Reports { get; }

        public IGscContextUnitOfWork UnitOfWork { get; }

        public virtual void ExecuteReport(ReportInfo reportInfo)
        {
            if (reportInfo == null)
            {
                throw new ArgumentNullException(nameof(reportInfo));
            }

            // TODO: Handle wrong state here.
            if (reportInfo.ReportType == null)
            {
                return;
            }

            GscReport report;
            try
            {
                // Create a report instance.
                report = Activator.CreateInstance(reportInfo.ReportType, this) as GscReport;
                if (report == null)
                {
                    // TODO: Add class ReportCreationException : ReportException
                    throw new Exception("Отчет должен быть экземпляром GscReport.");
                }
            }
            catch (MissingMethodException e)
            {
                // TODO: Add class ReportCreationException : ReportException
                throw new Exception("Конструктор не найден.", e);
            }

            report.RequestParameters = reportInfo.RequestParametersBeforeReportExecute;

            var pt = new ReportPrintTool(report);
            pt.PreviewRibbonForm.FormClosed += (sender, args) =>
            {
                pt.Dispose();
                report.Dispose();
            };
            pt.AutoShowParametersPanel = true;
            pt.ShowRibbonPreview();
        }
    }

    public class ReportInfo
    {
        public string DisplayName { get; set; }

        public ReportCategoryInfo Category { get; set; }

        public Type ReportType { get; set; }

        /// <summary>
        /// Запускать отчет с параметрами по-умолчанию. Или не запускать пока их не предоставил юзер, если <c>true</c>.
        /// </summary>
        public bool RequestParametersBeforeReportExecute { get; set; } = false;
    }

    public class ReportCategoryInfo
    {
        public string DisplayName { get; set; }
    }
}