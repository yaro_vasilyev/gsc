﻿using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Reports.MonitorResult;

namespace GSC.Client.ViewModels.Reports
{
    [POCOViewModel]
    public class MonitorResultReportsViewModel : ReportsViewModel
    {
        private static readonly ReportCategoryInfo monitorResultReportsCategoryInfo = new ReportCategoryInfo
        {
            DisplayName = "Результаты мониторинга ГИА"
        };

        public MonitorResultReportsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        public override List<ReportCategoryInfo> Categories => new List<ReportCategoryInfo>
        {
            monitorResultReportsCategoryInfo
        };

        public override List<ReportInfo> Reports => new List<ReportInfo>
        {
            new ReportInfo
            {
                DisplayName = "Информация по проведению мониторинга",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep091MonInfo),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Результаты мониторинга ГИА",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep094MonitorResults)
            },
            new ReportInfo
            {
                DisplayName = "Подробные результаты мониторинга ГИА",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep095MonitorResultsDetailed)
            },
            new ReportInfo
            {
                DisplayName = "Краткие результаты мониторинга",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof(Rep096MonitorResultsShort)
            },
            new ReportInfo
            {
                DisplayName = "Общее количество нарушений по ППЭ",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep097CountMrPpe)
            },
            new ReportInfo
            {
                DisplayName = "Результаты мониторинга по субъектам Российской Федерации",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep098MonResByReg),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Нарушения по видам в разрезе субъектов",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep099ViolationsByReg),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Количество нарушений по видам",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep102CountViolations),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Количество нарушений по категориям нарушителей",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep103CountViolators),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Нарушения, выявленные в субъекте Российской Федерации",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof (Rep101ViolationsSubjRF),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Перечень субъектов Российской Федерации, в котором осуществлялся мониторинг",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof(Rep105ScheduleRegions),
                RequestParametersBeforeReportExecute = true
            },
            new ReportInfo
            {
                DisplayName = "Нарушения по видам в разрезе ППЭ",
                Category = monitorResultReportsCategoryInfo,
                ReportType = typeof(Rep100ViolationsPpe),
                RequestParametersBeforeReportExecute = true
            }
        };
    }

}
