﻿using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Model;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the single Region object view model.
    /// </summary>
    public partial class RegionViewModel : ProjectEntityViewModel<Region>
    {

        /// <summary>
        /// Creates a new instance of RegionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static RegionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new RegionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicDegreeViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected RegionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Regions, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "Регион";

        protected override string SDeleteOperation => SecurityOperations.Regions.Delete;

        protected override string SNewOperation => SecurityOperations.Regions.New;

        protected override string SEditOperation => SecurityOperations.Regions.Edit;
    }
}
