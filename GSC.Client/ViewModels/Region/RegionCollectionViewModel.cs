﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using JetBrains.Annotations;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the Regions collection view model.
    /// </summary>
    public partial class RegionCollectionViewModel : ProjectEntityCollectionViewModel<Model.Region>
    {

        /// <summary>
        /// Creates a new instance of RegionCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static RegionCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new RegionCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the RegionCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the RegionCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected RegionCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Regions)
        {
        }


        [UsedImplicitly]
        public void ImportDictionary()
        {
            this.EnsureOperationAllowed(SecurityOperations.Regions.ImportFromCsv);

            var gscContext = ((DbUnitOfWork<Model.GscContext>)CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();
            importService.Import<Model.Region, RegionCsvMap, Model.ImportResult>(
                region => gscContext.ImportRowRegion(
                    CurrentProject.Id,
                    sessionGuid,
                    region.Code,
                    region.Name),
                () => gscContext.ImportRegions(sessionGuid),
                result => $"Код {result.Code},\t{result.Name}",
                this);
        }

        [UsedImplicitly]
        private sealed class RegionCsvMap : CsvClassMap<Model.Region>
        {
            public RegionCsvMap()
            {
                Map(region => region.Code).Name("REGION").TypeConverterOption(NumberStyles.Integer);
                Map(region => region.Name).Name("RegionName");}
        }

        [UsedImplicitly]
        public bool CanImportDictionary()
        {
            return this.OperationAllowed(SecurityOperations.Regions.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Regions.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Regions.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.Regions.New;

        protected override string SEditOperation => SecurityOperations.Regions.Edit;

        protected override string SDeleteOperation => SecurityOperations.Regions.Delete;

        public override string EntityName => "Регион";
    }
}