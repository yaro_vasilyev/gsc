﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Client.ViewModels
{
    public interface ISupportReadOnlyViewModel
    {
        bool ReadOnly { get; }
    }
}
