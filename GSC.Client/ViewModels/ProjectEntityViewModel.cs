﻿using System;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels
{
    public abstract class ProjectEntityViewModel<TEntity> : ProjectEntityViewModelBase<TEntity, int>
        where TEntity : class
    {
        /// <summary>
        /// Initializes a new instance of the SingleObjectViewModel class.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create the unit of work instance.</param>
        /// <param name="getRepositoryFunc">A function that returns the repository representing entities of a given type.</param>
        /// <param name="getEntityDisplayNameFunc">An optional parameter that provides a function to obtain the display text for a given entity. If ommited, the primary key value is used as a display text.</param>
        protected ProjectEntityViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory, 
            Func<IGscContextUnitOfWork, IRepository<TEntity, int>> getRepositoryFunc,
            Func<TEntity, object> getEntityDisplayNameFunc = null)
            : base(unitOfWorkFactory, getRepositoryFunc, getEntityDisplayNameFunc)
        {
        }
    }
}
