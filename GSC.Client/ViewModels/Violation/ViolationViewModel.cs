﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Violation
{
    public class ViolationViewModel : ProjectEntityViewModel<Model.Violation>
    {

        /// <summary>
        /// Creates a new instance of ViolationViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ViolationViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ViolationViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ViolationViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ViolationViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ViolationViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Violations, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "Нарушение";

        protected override string SDeleteOperation => SecurityOperations.Violations.Delete;

        protected override string SNewOperation => SecurityOperations.Violations.New;

        protected override string SEditOperation => SecurityOperations.Violations.Edit;
    }
}
