﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Violation
{
    public class ViolationCollectionViewModel : ProjectEntityCollectionViewModel<Model.Violation>
    {
        /// <summary>
        /// Creates a new instance of ViolationCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ViolationCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ViolationCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ViolationCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ViolationCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ViolationCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Violations)
        {
        }

        [UsedImplicitly]
        public void ImportFromCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.Violations.ImportFromCsv);

            var gscContext = ((DbUnitOfWork<Model.GscContext>) CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.Violation, ViolationCsvMap, Model.ImportResult>(
                violation =>
                    gscContext.ImportRowViolation(sessionGuid, violation.Code, violation.Name),
                () => gscContext.ImportViolation(sessionGuid),
                result => $"Код {result.Code},\t\"{result.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class ViolationCsvMap : CsvClassMap<Model.Violation>
        {
            public ViolationCsvMap()
            {
                Map(violation => violation.Code).Name("Код нарушения").TypeConverterOption(NumberStyles.Integer);
                Map(violation => violation.Name).Name("Наименование нарушения");
            }
        }

        [UsedImplicitly]
        public bool CanImportFromCsv()
        {
            return this.OperationAllowed(SecurityOperations.Violations.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Violations.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Violations.ExportToExcel);
        }

        public override string EntityName => "Нарушение";

        protected override string SNewOperation => SecurityOperations.Violations.New;

        protected override string SEditOperation => SecurityOperations.Violations.Edit;

        protected override string SDeleteOperation => SecurityOperations.Violations.Delete;
    }
}