﻿using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the single AcademicDegree object view model.
    /// </summary>
    public class AcademicDegreeViewModel : SingleObjectViewModel<Model.AcademicDegree, int, IGscContextUnitOfWork>
    {
        /// <summary>
        /// Creates a new instance of AcademicDegreeViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static AcademicDegreeViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AcademicDegreeViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicDegreeViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected AcademicDegreeViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AcademicDegrees, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "Ученая степень";

        protected string SDeleteOperation => SecurityOperations.AcademicDegrees.Delete;

        protected string SNewOperation => SecurityOperations.AcademicDegrees.New;

        protected string SEditOperation => SecurityOperations.AcademicDegrees.Edit;

        public override bool CanDelete()
        {
            return base.CanDelete() && SecurityService.OperationAllowed(SDeleteOperation);
        }

        public override void Delete()
        {
            SecurityService.EnsureOperationAllowed(SDeleteOperation);
            base.Delete();
        }

        public override bool CanSave()
        {
            return base.CanSave() &&
                SecurityService.OperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
        }

        public override void Save()
        {
            SecurityService.EnsureOperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
            base.Save();
        }

        protected override bool CanCreateNewEntity()
        {
            return base.CanCreateNewEntity() &&
                SecurityService.OperationAllowed(SNewOperation);
        }

        protected override Model.AcademicDegree CreateEntity()
        {
            SecurityService.EnsureOperationAllowed(SNewOperation);
            return base.CreateEntity();
        }
    }
}
