﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.Common.ViewModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the AcademicDegrees collection view model.
    /// </summary>
    public class AcademicDegreeCollectionViewModel 
        : CollectionViewModel<Model.AcademicDegree, int, IGscContextUnitOfWork>
        , ISecureViewModel
    {
        /// <summary>
        /// Creates a new instance of AcademicDegreeCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static AcademicDegreeCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AcademicDegreeCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicDegreeCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected AcademicDegreeCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AcademicDegrees)
        {
        }

        [UsedImplicitly]
        public void ImportDictionary()
        {
            this.EnsureOperationAllowed(SecurityOperations.AcademicDegrees.ImportFromCsv);

            var gscContext = ((DbUnitOfWork<Model.GscContext>)CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.AcademicDegree, AcademicDegreeCsvMap, Model.ImportAcademicDegreesResult>(
                record => { gscContext.ImportRowAcademicDegree(sessionGuid, record.Code, record.Name); },
                () => gscContext.ImportAcademicDegrees(sessionGuid),
                academicDegree => $"Код {academicDegree.Code},\t\"{academicDegree.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class AcademicDegreeCsvMap : CsvClassMap<Model.AcademicDegree>
        {
            public AcademicDegreeCsvMap()
            {
                Map(degree => degree.Code).Name("Код").TypeConverterOption(NumberStyles.Integer);
                Map(degree => degree.Name).Name("Ученая степень");
            }
        }

        [UsedImplicitly]
        public bool CanImportDictionary()
        {
            return this.OperationAllowed(SecurityOperations.AcademicDegrees.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.AcademicDegrees.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.AcademicDegrees.ExportToExcel);
        }

        #region Operations checks

        public bool CanNew()
        {
            return SecurityService.OperationAllowed(SecurityOperations.AcademicDegrees.New);
        }

        public override void New()
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicDegrees.New);
            base.New();
        }

        public override bool CanEdit(Model.AcademicDegree projectionEntity)
        {
            return base.CanEdit(projectionEntity) &&
                   SecurityService.OperationAllowed(SecurityOperations.AcademicDegrees.Edit);
        }

        public override void Edit(Model.AcademicDegree projectionEntity)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicDegrees.Edit);
            base.Edit(projectionEntity);
        }

        public override bool CanDelete(Model.AcademicDegree projectionEntity)
        {
            return base.CanDelete(projectionEntity) &&
                   SecurityService.OperationAllowed(SecurityOperations.AcademicDegrees.Delete);
        }

        public override void Delete(Model.AcademicDegree projectionEntity)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicDegrees.Delete);
            base.Delete(projectionEntity);
        }

        #endregion

        public override string EntityName
        {
            get { return "Ученая степень"; }
        }
    }
}