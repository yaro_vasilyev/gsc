﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Security;
using GSC.Client.Services;

// ReSharper disable once CheckNamespace
namespace GSC.Client.Common.ViewModel
{
    public abstract partial class SingleObjectViewModel<TEntity, TPrimaryKey, TUnitOfWork>
        where TEntity : class
        where TUnitOfWork : IUnitOfWork
    {
        public bool IsEditMode
        {
            get { return IsPrimaryKeyReadOnly; }
        }

        public bool IsNewMode
        {
            get { return IsNew(); }
        }

        #region SecurityService

        protected ISecurityService SecurityService
        {
            get { return this.GetRequiredService<ISecurityService>(); }
        }

        #endregion

        #region ProjectService

        protected virtual IProjectService ProjectService
        {
            get { throw new NotImplementedException("Should be implemented by DevExpress MVVM Framework."); }
        }

        #endregion

        protected virtual string LocalizedEntityName => typeof(TEntity).Name;

        protected override string GetTitle()
        {
            return (LocalizedEntityName + " - " + 
                Convert.ToString(getEntityDisplayNameFunc != null ? getEntityDisplayNameFunc(Entity) : PrimaryKey))
            .Split(new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
        }

        protected override string GetTitleForNewEntity()
        {
            return LocalizedEntityName + CommonResources.Entity_New;
        }

    }
}
