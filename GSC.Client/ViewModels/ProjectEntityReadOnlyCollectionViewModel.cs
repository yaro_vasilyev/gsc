﻿using System;
using System.Linq;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    public class ProjectEntityReadOnlyCollectionViewModel<TEntity, TProjection>
        : ReadOnlyCollectionViewModel<TEntity, TProjection, IGscContextUnitOfWork>
        where TEntity : class, IProjectEntity
        where TProjection : class, IProjectEntity
    {
        public ProjectEntityReadOnlyCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
                                                        Func<IGscContextUnitOfWork, IReadOnlyRepository<TEntity>>
                                                            getRepositoryFunc,
                                                        Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>>
                                                            projection = null)
            : base(unitOfWorkFactory, getRepositoryFunc, projection)
        {
            SetFilter(UnitOfWorkFactory.CreateUnitOfWork());
        }

        protected void SetFilter(IGscContextUnitOfWork unitOfWork)
        {
            var project = CurrentProject.Id;
            FilterExpression = pe => pe.ProjectId == project;
        }

        protected virtual IProjectService ProjectService
        {
            get { throw new NotImplementedException("Should be implemented by POCOViewModel."); }
        }

        protected Model.Project CurrentProject
        {
            get
            {
                return ProjectService.GetCurrentProject().Match(x => x,
                    e =>
                    {
                        throw e; // TODO: Wrap?
                    });
            }
        }
    }

    public class ProjectEntityReadOnlyCollectionViewModel<TEntity>
        : ProjectEntityReadOnlyCollectionViewModel<TEntity, TEntity>
        where TEntity : class, IProjectEntity
    {
        public ProjectEntityReadOnlyCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
                                                        Func<IGscContextUnitOfWork, IReadOnlyRepository<TEntity>>
                                                            getRepositoryFunc,
                                                        Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>>
                                                            projection = null)
            : base(unitOfWorkFactory, getRepositoryFunc, projection)
        {
        }
    }
}
