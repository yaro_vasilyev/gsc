﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Violator
{
    public class ViolatorViewModel : ProjectEntityViewModel<Model.Violator>
    {

        /// <summary>
        /// Creates a new instance of ViolatorViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ViolatorViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ViolatorViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ViolatorViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ViolatorViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ViolatorViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Violators, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "Нарушитель";

        protected override string SDeleteOperation => SecurityOperations.Violators.Delete;

        protected override string SNewOperation => SecurityOperations.Violators.New;

        protected override string SEditOperation => SecurityOperations.Violators.Edit;
    }
}
