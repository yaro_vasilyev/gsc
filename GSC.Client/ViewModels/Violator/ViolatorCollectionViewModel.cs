﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Violator
{
    public class ViolatorCollectionViewModel : ProjectEntityCollectionViewModel<Model.Violator>
    {
        /// <summary>
        /// Creates a new instance of ViolatorCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ViolatorCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ViolatorCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ViolatorCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ViolatorCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ViolatorCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Violators)
        {
        }

        [UsedImplicitly]
        public void ImportFromCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.Violators.ImportFromCsv);
            
            var gscContext = ((DbUnitOfWork<Model.GscContext>)CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.Violator, ViolatorCsvMap, Model.ImportResult>(
                violator =>
                    gscContext.ImportRowViolator(sessionGuid, violator.Code, violator.Name),
                () => gscContext.ImportViolator(sessionGuid),
                result => $"Код {result.Code},\t\"{result.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class ViolatorCsvMap : CsvClassMap<Model.Violator>{
            public ViolatorCsvMap()
            {
                Map(violation => violation.Code).Name("Код нарушителя").TypeConverterOption(NumberStyles.Integer);
                Map(violation => violation.Name).Name("Категория нарушителей");
            }
        }

        [UsedImplicitly]
        public bool CanImportFromCsv()
        {
            return this.OperationAllowed(SecurityOperations.Violators.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Violators.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Violators.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.Violators.New;

        protected override string SEditOperation => SecurityOperations.Violators.Edit;

        protected override string SDeleteOperation => SecurityOperations.Violators.Delete;

        public override string EntityName => "Нарушитель";
    }
}
