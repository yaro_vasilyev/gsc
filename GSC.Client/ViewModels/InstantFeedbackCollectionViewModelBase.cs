﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.Util;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TProjection"></typeparam>
    /// <typeparam name="TPrimaryKey"></typeparam>
    /// <typeparam name="TUnitOfWork"></typeparam>
    /// <remarks>Source: <see cref="https://www.devexpress.com/Support/Center/Question/Details/T303979"/></remarks>
    public abstract class InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork> : IDocumentContent, ISupportLogicalLayout
        where TEntity : class, new()
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {

        #region inner classes
        public class InstantFeedbackSourceViewModel : IListSource
        {
            public static InstantFeedbackSourceViewModel Create(Func<int> getCount, IInstantFeedbackSource<TProjection> source)
            {
                return ViewModelSource.Create(() => new InstantFeedbackSourceViewModel(getCount, source));
            }

            readonly Func<int> getCount;
            readonly IInstantFeedbackSource<TProjection> source;

            protected InstantFeedbackSourceViewModel(Func<int> getCount, IInstantFeedbackSource<TProjection> source)
            {
                this.getCount = getCount;
                this.source = source;
            }

            public int Count { get { return getCount(); } }

            public void Refresh()
            {
                source.Refresh();
                this.RaisePropertyChanged(x => x.Count);
            }

            bool IListSource.ContainsListCollection { get { return source.ContainsListCollection; } }

            IList IListSource.GetList()
            {
                return source.GetList();
            }
        }
        #endregion

        protected readonly IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory;
        protected readonly Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc;
        protected Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> Projection { get; private set; }
        Func<bool> canCreateNewEntity;
        readonly IRepository<TEntity, TPrimaryKey> helperRepository;
        readonly Lazy<IInstantFeedbackSource<TProjection>> source;

        protected InstantFeedbackCollectionViewModelBase(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection,
            Func<bool> canCreateNewEntity = null)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.canCreateNewEntity = canCreateNewEntity;
            this.getRepositoryFunc = getRepositoryFunc;
            this.Projection = projection;
            this.helperRepository = CreateRepository();

            RepositoryExtensions.VerifyProjection(helperRepository, projection);

            this.source =
                new Lazy<IInstantFeedbackSource<TProjection>>(
                    () =>
                        unitOfWorkFactory.CreateInstantFeedbackSource(getRepositoryFunc,
                                                                      ReadOnlyRepositoryExtensions.AppendToProjection(
                                                                          GetFilterExpression(),
                                                                          Projection)));
            entities =
                new Lazy<InstantFeedbackSourceViewModel>(
                    () => InstantFeedbackSourceViewModel.Create(() => helperRepository.Count(), source.Value));

            if (!this.IsInDesignMode())
                OnInitializeInRuntime();
        }

        #region FilterExpression

        /// <summary>
        /// The lambda expression used to filter which entities will be loaded locally from the unit of work.
        /// Since ReadOnlyCollectionViewModelBase is a POCO view model, this property will raise INotifyPropertyChanged.PropertyEvent when modified so it can be used as a binding source in views.
        /// </summary>
        public virtual Expression<Func<TEntity, bool>> FilterExpression { get; set; }

        public virtual Expression<Func<TEntity, bool>> GetFilterExpression() { return FilterExpression; }

        // TODO: FilterExpression in InstantFeedbackCollectionViewModel does not allowed to be changed at runtime (no effect).

        #endregion


        private Lazy<InstantFeedbackSourceViewModel> entities;

        public InstantFeedbackSourceViewModel Entities => entities.Value;

        public virtual object SelectedEntity { get; set; }

        #region Selection

        public virtual IEnumerable<object> SelectedEntities { get; set; }

        protected virtual void OnSelectedEntitiesChanged() { }

        #endregion

        public virtual void New()
        {
            if (canCreateNewEntity != null && !canCreateNewEntity())
                return;
            DocumentManagerService.ShowNewEntityDocument<TEntity>(this);
        }

        public virtual void Edit(object threadSafeProxy)
        {
            if (!source.Value.IsLoadedProxy(threadSafeProxy))
            {
                return;
            }

            TPrimaryKey primaryKey = GetProxyPrimaryKey(threadSafeProxy);
            TEntity entity = helperRepository.Find(primaryKey);
            if (entity == null)
            {
                DestroyDocument(DocumentManagerService.FindEntityDocument<TEntity, TPrimaryKey>(primaryKey));
                return;
            }
            DocumentManagerService.ShowExistingEntityDocument<TEntity, TPrimaryKey>(this, primaryKey);
        }

        public virtual bool CanEdit(object threadSafeProxy)
        {
            return threadSafeProxy != null;
        }

        public virtual void Delete(object threadSafeProxy)
        {
            if ((SelectedEntities?.Count() ?? 0) > 1)
                DeleteAll();
            else
                DeleteBase(threadSafeProxy);
        }

        protected virtual void DeleteBase(object threadSafeProxy)
        {
            if (!source.Value.IsLoadedProxy(threadSafeProxy))
                return;
            if (MessageBoxService.ShowMessage(string.Format(CommonResources.Confirmation_Delete, typeof(TEntity).Name), CommonResources.Confirmation_Caption, MessageButton.YesNo) != MessageResult.Yes)
                return;
            try
            {
                DeleteCore(threadSafeProxy);
            }
            catch (DbException e)
            {
                Refresh();
                MessageBoxService.ShowMessage(e.ErrorMessage, e.ErrorCaption, MessageButton.OK, MessageIcon.Error);
            }
            Refresh();
        }

        protected virtual bool CanDeleteBase(object threadSafeProxy) { return CanDeleteCore(threadSafeProxy); }

        protected void DeleteCore(object threadSafeProxy)
        {
            TPrimaryKey primaryKey = GetProxyPrimaryKey(threadSafeProxy);
            TEntity entity = helperRepository.Find(primaryKey);
            if (entity != null)
            {
                OnBeforeEntityDeleted(primaryKey, entity);
                helperRepository.Remove(entity);
                helperRepository.UnitOfWork.SaveChanges();
                OnEntityDeleted(primaryKey, entity);
            }
        }

        protected bool CanDeleteCore(object threadSafeProxy)
        {
            return threadSafeProxy != null;
        }

        public virtual bool CanDelete(object threadSafeProxy)
        {
            if ((SelectedEntities?.Count() ?? 0) > 1)
            {
                return CanDeleteAll();
            }

            return CanDeleteBase(threadSafeProxy);
        }

        protected ILayoutSerializationService LayoutSerializationService { get { return this.GetService<ILayoutSerializationService>(); } }

        string ViewName { get { return typeof(TEntity).Name + "InstantFeedbackCollectionView"; } }

        [Display(AutoGenerateField = false)]
        public virtual void OnLoaded()
        {
            PersistentLayoutHelper.TryDeserializeLayout(LayoutSerializationService, ViewName);
        }

        [Display(AutoGenerateField = false)]
        public virtual void OnUnloaded()
        {
            SaveLayout();
        }

        void SaveLayout()
        {
            PersistentLayoutHelper.TrySerializeLayout(LayoutSerializationService, ViewName);
        }

        public virtual void Refresh()
        {
            Entities.Refresh();
        }

        protected TPrimaryKey GetProxyPrimaryKey(object threadSafeProxy)
        {
            var expression = helperRepository.GetProjectionPrimaryKeyExpression<TEntity, TProjection, TPrimaryKey>();
            return GetProxyPropertyValue(threadSafeProxy, expression);
        }

        protected TProperty GetProxyPropertyValue<TProperty>(object threadSafeProxy, Expression<Func<TProjection, TProperty>> propertyExpression)
        {
            return source.Value.GetPropertyValue(threadSafeProxy, propertyExpression);
        }

        protected virtual void OnEntityDeleted(TPrimaryKey primaryKey, TEntity entity)
        {
            Messenger.Default.Send(new EntityMessage<TEntity, TPrimaryKey>(primaryKey, EntityMessageType.Deleted));
        }

        protected IMessageBoxService MessageBoxService { get { return this.GetRequiredService<IMessageBoxService>(); } }
        protected IDocumentManagerService DocumentManagerService { get { return this.GetService<IDocumentManagerService>(); } }

        protected virtual void OnBeforeEntityDeleted(TPrimaryKey primaryKey, TEntity entity) { }

        protected void DestroyDocument(IDocument document)
        {
            if (document != null)
                document.Close();
        }

        protected IRepository<TEntity, TPrimaryKey> CreateRepository()
        {
            return getRepositoryFunc(CreateUnitOfWork());
        }

        protected TUnitOfWork CreateUnitOfWork()
        {
            return unitOfWorkFactory.CreateUnitOfWork();
        }

        protected virtual void OnInitializeInRuntime()
        {
            Messenger.Default.Register<EntityMessage<TEntity, TPrimaryKey>>(this, x => OnMessage(x));
        }

        protected virtual void OnDestroy()
        {
            Messenger.Default.Unregister(this);
        }

        void OnMessage(EntityMessage<TEntity, TPrimaryKey> message)
        {
            Refresh();
        }

        protected IDocumentOwner DocumentOwner { get; private set; }

        #region IDocumentContent
        object IDocumentContent.Title { get { return null; } }

        void IDocumentContent.OnClose(CancelEventArgs e)
        {
            SaveLayout();
        }

        void IDocumentContent.OnDestroy()
        {
            OnDestroy();
        }

        IDocumentOwner IDocumentContent.DocumentOwner
        {
            get { return DocumentOwner; }
            set { DocumentOwner = value; }
        }
        #endregion

        #region ISupportLogicalLayout
        bool ISupportLogicalLayout.CanSerialize
        {
            get { return true; }
        }

        IDocumentManagerService ISupportLogicalLayout.DocumentManagerService
        {
            get { return DocumentManagerService; }
        }

        IEnumerable<object> ISupportLogicalLayout.LookupViewModels
        {
            get { return null; }
        }
        #endregion

        public string PrimaryKeyPropertyName => helperRepository.GetPrimaryKeyPropertyName();

        #region DeleteAll

        public bool CanDeleteAll() { return SelectedEntities?.Any() ?? false; }

        public virtual void DeleteAll()
        {
            if (MessageResult.OK
                != MessageBoxService.Show("Удалить выбранные объекты?",
                                          CommonResources.Confirmation_Caption,
                                          MessageButton.OKCancel,
                                          MessageIcon.Question,
                                          MessageResult.OK))
                return;

            var items = SelectedEntities.ToArray();
            int counter = 0;
            foreach (var projection in items)
            {
                try
                {
                    DeleteCore(projection);
                    ++counter;
                }
                catch (ForeignKeyViolationException)
                {
                }
                catch (DbException)
                {
                }
            }

            MessageBoxService.Show(string.Format("Удалено {0} из {1} объектов.", counter, items.Length),
                                   "Информация",
                                   MessageButton.OK,
                                   MessageIcon.Information,
                                   MessageResult.OK);
            Refresh();
        }

        #endregion

    }
}
