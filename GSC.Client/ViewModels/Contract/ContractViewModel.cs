﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Export;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;
using Document = DevExpress.XtraRichEdit.API.Native.Document;

namespace GSC.Client.ViewModels.Contract
{
    [UsedImplicitly]
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class ContractViewModel : ProjectEntityViewModelBase<Model.Contract, string>
    {
        /// <summary>
        /// Creates a new instance of ContractInfoViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static ContractViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ContractViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ContractInfoViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ContractInfoViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ContractViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                gscContextUnitOfWork => gscContextUnitOfWork.Contracts,
                contract => contract.Number)
        {
        }

        protected override string SNewOperation => SecurityOperations.Contracts.New;

        protected override string SEditOperation => SecurityOperations.Contracts.Edit;

        protected override string SDeleteOperation => SecurityOperations.Contracts.Delete;

        protected override Model.Contract CreateEntity()
        {
            var contract = base.CreateEntity();
            contract.ContractType = ContractType.Paid;
            return contract;
        }

        public bool CanGenerateDoc()
        {
            return !ReadOnly;
        }

        public void EmailContract()
        {
            var docManager = this.DocumentOwner as DevExpress.Utils.MVVM.Services.WindowedDocumentManagerService;
            if (docManager == null)
                return;

            var documentService = this.GetRequiredService<IGenerateDocumentService>();
            var filePath = documentService.ExportDocument("Договор.rtf", DocumentFormat.Rtf, false);

            var emailSender = new EmailSender((System.Windows.Forms.Control) docManager.Owner);
            emailSender.Send(new[] { filePath },
                             new EmailOptions
                                 {
                                     RecipientAddress = Entity?.Schedule?.Expert?.Email ?? "",
                                     Subject =
                                         "Договор на выполнение работ для нужд ФГБУ «Федеральный центр тестирования»",
                                     Body = "С уважением",
                                     RecipientName = Entity?.Schedule?.ExpertFullName
                                 });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>В какой книжке учат писать такой код? Я её в полнолуние сожгу.</remarks>
        [UsedImplicitly]
        public void GenerateDoc()
        {
            var contractType = Entity?.ContractType == ContractType.Paid ? "paid" : "unpaid";
            var withRcoi = Entity?.Schedule?.MonitorRcoi == BooleanType.Yes ? "rcoi" : "worcoi";
            string fileName = $@"cont_{contractType}_{withRcoi}";
            var templatePath = $@"DocTemplates\{fileName}.docx"; // todo move to config

            var generateDocumentService = this.GetRequiredService<IGenerateDocumentService>();
            var schedule = Entity?.Schedule;
            var expert = schedule?.Expert;
            var rcoi = schedule?.Region?.Rcois?.FirstOrDefault();
            //var route = Entity?.Route;
            var fields = new List<object>
            {
                new
                {
                    GSC_ContractNumber = Entity?.Number,
                    GSC_ContractDate = Entity?.ContractInfo?.ContractDate.ToString("«dd» MMMM yyyyг."),
                    GSC_ExpertFullName = schedule?.ExpertFullName,
                    GSC_MonitoringLocation = schedule?.RegionName,
                    GSC_DateFrom = Entity?.DateFrom.ToString("dd MMMM yyyyг."),
                    GSC_DateTo = Entity?.DateTo.ToString("dd MMMM yyyyг."),
                    GSC_ExpertWorkplace = expert?.Workplace,
                    GSC_ExpertPost = expert?.Post,
                    GSC_RCOIName = rcoi?.Name,
                    GSC_RCOIAddress = rcoi?.Address,
                    //GSC_Route = route,
                }
            };
            var scheduleDetails = Entity?.Schedule?.ScheduleDetails?.OrderBy(x => x.ExamDate).Select(detail => {
                var station = detail.Station;
                var execsInThatDate = station?.Execs?.Where(exec => exec?.Exam?.ExamDate == detail.ExamDate);
                // проводимые экзамены в дату графика (schedule_detail.exam_date)
                var distinctExamTypes = execsInThatDate?.Select(exec => exec.Exam?.ExamType).Distinct();
                // типы проводимых экзаменов без дублей (distinct)
                var examTypesList = distinctExamTypes?.Select(type => type.RetrieveDisplayName()).ToList();
                // конечный список типов на русском
                var scheduleDetailInfo = new ScheduleDetailInfo
                    {
                        ExamDate = detail.ExamDate.ToString("dd.MM.yyyy"),
                        StationName = station?.Name,
                        StationCode = station?.Code.ToString(),
                        StationAddress = station?.Address,
                        ExamTypes = examTypesList != null ? string.Join(", ", examTypesList) : string.Empty
                    };
                return scheduleDetailInfo;
            }).ToList();

            generateDocumentService.GenerateDocument((createDocServer, targetDocSelector) =>
            {
                if (createDocServer == null)
                {
                    throw new ArgumentNullException(nameof(createDocServer));
                }
                if (targetDocSelector == null)
                {
                    throw new ArgumentNullException(nameof(targetDocSelector));
                }

                // failure-обёртки над лямбдами, чтобы они возвращали не null
                Func<IRichEditDocumentServer> getDocServ = () =>
                {
                    var serv = createDocServer();
                    if (serv == null)
                    {
                        throw new ArgumentNullException(nameof(createDocServer), @"Лямбда вернула null");
                    }
                    return serv;
                };
                Func<Document> getTargetDoc = () =>
                {
                    var document = targetDocSelector();
                    if (document == null)
                    {
                        throw new ArgumentNullException(nameof(targetDocSelector), @"Лямбда вернула null");
                    }
                    return document;
                };

                using (var documentServer = getDocServ())
                {
                    var mergeOptions = documentServer.CreateMailMergeOptions();
                    mergeOptions.MergeMode = MergeMode.JoinTables;
                    mergeOptions.CopyTemplateStyles = true;
                    mergeOptions.DataSource = fields;
                    documentServer.Document.LoadDocument(templatePath, DocumentFormat.OpenXml);
                    var targetDoc = getTargetDoc();
                    targetDoc.CalculateDocumentVariable += (sender, args) =>
                    {
                        /* 
                                способ вставки графика через вставки вложенных шаблонов с подтаблицами (GSC_Schedule и GSC_ScheduleDetail) не работает - всё съезжает
                                см. ниже в GSC_ScheduleTable генерацию таблицы в коде
                        */
                        switch (args.VariableName)
                        {
                            case "GSC_Schedule":
                                // вложенный шаблон DocTemplates\schedule.docx таблицы графика (без маршрута, маршрут в корневом шаблоне был)
                                var scheduleServerMaster = getDocServ();
                                var mailMergeOptions = scheduleServerMaster.CreateMailMergeOptions();
                                mailMergeOptions.DataSource = scheduleDetails;
                                mailMergeOptions.MergeMode = MergeMode.JoinTables;
                                mailMergeOptions.CopyTemplateStyles = true;
                                scheduleServerMaster.Document.LoadDocument(@"DocTemplates\schedule.docx",
                                    DocumentFormat.OpenXml);
                                var resultServerMaster = getDocServ();
                                resultServerMaster.Document.CalculateDocumentVariable += (o, eventArgs) =>
                                {
                                    switch (eventArgs.VariableName)
                                    {
                                        case "GSC_ScheduleDetail":
                                            // вложенный шаблон DocTemplates\schedule_detail.docx строк графика
                                            var scheduleDetailsServerMaster = getDocServ();
                                            var detailsMailMergeOptions =
                                                scheduleDetailsServerMaster.CreateMailMergeOptions();
                                            detailsMailMergeOptions.DataSource = scheduleDetails;
                                            detailsMailMergeOptions.MergeMode = MergeMode.JoinTables;
                                            detailsMailMergeOptions.CopyTemplateStyles = true;
                                            scheduleDetailsServerMaster.Document.LoadDocument(
                                                @"DocTemplates\schedule_detail.docx",
                                                DocumentFormat.OpenXml);
                                            var resultDetailsServerMaster = getDocServ();
                                            scheduleDetailsServerMaster.MailMerge(detailsMailMergeOptions,
                                                resultDetailsServerMaster.Document);
                                            eventArgs.Value = resultDetailsServerMaster.Document;
                                            eventArgs.Handled = true;
                                            break;
                                    }
                                };
                                scheduleServerMaster.MailMerge(mailMergeOptions, resultServerMaster.Document);
                                args.Value = resultServerMaster.Document;
                                args.Handled = true;
                                break;
                            case "GSC_ScheduleTable":
                                var doc = getDocServ().Document;

                                // create table
                                const int minRowCount = 1 + 1; // заголовок + маршрут
                                var schedulesRowCount = 1 + scheduleDetails?.Count ?? 0; // заголовок + график
                                var rowCount = Math.Max(minRowCount, schedulesRowCount);
                                /* по кол-ву строк: если график пуст или мал, то макс вернёт хотя бы две строки (заголовок и маршрут), если график больше, то макс вернёт его размер + 1 на заголовок */
                                const int columnCount = 5;
                                var table = doc.Tables.Create(doc.Range.Start, rowCount, columnCount);

                                // head
                                doc.InsertText(table.Cell(0, 0).Range.Start, @"Дата экзамена");
                                doc.InsertText(table.Cell(0, 1).Range.Start, @"Наименование ППЭ");
                                doc.InsertText(table.Cell(0, 2).Range.Start, @"Код ППЭ");
                                doc.InsertText(table.Cell(0, 3).Range.Start, @"Адрес ППЭ");
                                doc.InsertText(table.Cell(0, 4).Range.Start, @"Форма проведения ГИА");
                                
                                // define Headings paragraph style
                                // apply style and vertical alignment
                                ApplyHeadingStyle(doc, table, CreateHeadingStyle(doc));

                                // schedule
                                var row = 1;
                                if (scheduleDetails != null)
                                {
                                    foreach (var detail in scheduleDetails)
                                    {
                                        doc.InsertText(table.Cell(row, 0).Range.Start, detail.ExamDate);
                                        doc.InsertText(table.Cell(row, 1).Range.Start, detail.StationName);
                                        doc.InsertText(table.Cell(row, 2).Range.Start, detail.StationCode);
                                        doc.InsertText(table.Cell(row, 3).Range.Start, detail.StationAddress);
                                        doc.InsertText(table.Cell(row, 4).Range.Start, detail.ExamTypes);
                                        row++;
                                    }
                                }

                                // return result
                                args.Value = doc;
                                args.Handled = true;
                                break;
                        }
                    };
                    documentServer.MailMerge(mergeOptions, targetDoc);
                }
            });
        }

        private static void ApplyHeadingStyle(Document document, Table table, ParagraphStyle style)
        {
            foreach (var cell in table.FirstRow.Cells)
            {
                cell.VerticalAlignment = TableCellVerticalAlignment.Center;
            }

            var paragraphProperties = document.BeginUpdateParagraphs(table.Rows.First.Range);
            try
            {
                paragraphProperties.Style = style;
            }
            finally
            {
                document.EndUpdateParagraphs(paragraphProperties);
            }
        }

        private static ParagraphStyle CreateHeadingStyle(Document document)
        {
            const string headingStyleName = "GSC Contract Headings Style";

            var style = document.ParagraphStyles.FirstOrDefault(ps => ps.Name == headingStyleName);
            if (style == null)
            {
                style = document.ParagraphStyles.CreateNew();
                style.Name = headingStyleName;
                style.Alignment = ParagraphAlignment.Center;
                style.FontName = "Times New Roman";
                style.Bold = true;
                style.FontSize = 11;
                document.ParagraphStyles.Add(style);
            }

            return style;
        }

        public IEntitiesViewModel<Model.ContractInfo> LookupUnusedContractInfos
        {
            get
            {
                var project = CurrentProject.Id;

                return GetLookUpEntitiesViewModel((ContractViewModel viewModel) => viewModel.LookupUnusedContractInfos,
                    unitOfWork => unitOfWork.ContractInfos,
                    contractInfos =>
                        contractInfos.Where(
                            ci => ci.ProjectId == project && (ci.Number == Entity.Number || ci.Contract == null)));
                /* выбираем тот инфо, у которого не указан контракт, т.е. не ссылается контракт на него, но себя тоже можно показывать */
            }
        }

        public IEntitiesViewModel<Model.Schedule> LookupSchedules
        {
            get
            {
                var project = CurrentProject.Id;

                return GetLookUpProjectionsViewModel((ContractViewModel viewModel) => viewModel.LookupSchedules,
                    uow => uow.Schedules,
                    schedules => schedules.Where(s => s.Expert.ProjectId == project && s.PeriodType == PeriodType &&
                                                      s.Expert.Kind == ExpertKind.Official));
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual PeriodType PeriodType
        {
            get { return Entity.PeriodType; }
            [UsedImplicitly]
            set
            {
                if (Entity.PeriodType == value)
                {
                    return;
                }
                Entity.PeriodType = value;
                this.RaisePropertyChanged(model => model.PeriodType);
            }
        }

        protected virtual void OnPeriodTypeChanged()
        {
            UpdateSchedulesLookup();
            Update();
        }

        private void UpdateSchedulesLookup()
        {
            RefreshLookUpCollection((ContractViewModel model) => model.LookupSchedules, true);
        }

        protected override bool NeedSave()
        {
            // NOTE: Dirty hack
            if (ReadOnly)
                return false;
            return base.NeedSave();
        }

        protected override string LocalizedEntityName => "Договор";
    }
}