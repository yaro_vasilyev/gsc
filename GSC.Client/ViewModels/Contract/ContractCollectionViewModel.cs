﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Contract
{
    public class ContractCollectionViewModel 
        : ProjectEntityCollectionViewModelBase<Model.Contract, string>
    {
        /// <summary>
        /// Creates a new instance of ContractInfoCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static ContractCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ContractCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ContractInfoCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ContractInfoCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ContractCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(
                unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                gscContextUnitOfWork => gscContextUnitOfWork.Contracts,
                query => query
                    .Include(contract => contract.Schedule)
                    .Include(contract => contract.Schedule.Expert)
                    .Include(contract => contract.ContractInfo))
        {
            FilterExpression = c => c.ContractInfo.ProjectId == CurrentProject.Id;
        }

        protected override string SNewOperation => SecurityOperations.Contracts.New;

        protected override string SEditOperation => SecurityOperations.Contracts.Edit;

        protected override string SDeleteOperation => SecurityOperations.Contracts.Delete;
    }
}