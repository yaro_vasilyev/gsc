﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Rcoi
{
    [UsedImplicitly]
    public class RcoiInfo
    {
        public int RegionId { get; set; }
        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public int RcoiId { get; set; }
        public string Rcoi { get; set; }

        public static DbRawSqlQuery<RcoiInfo> GetRegionRcoiInfos([NotNull] DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            //todo перенести в БД или в LINQ
            return context.Database.SqlQuery<RcoiInfo>(@"
select r.id as RegionId, 
       r.code as RegionCode, 
       r.name as RegionName, 
       max_rcoi.rcoi_id as RcoiId, 
       concat_ws(', ', 
                 case rc.name when '' then null else rc.name end, 
                 case rc.address when '' then null else rc.address end) as Rcoi
  from gsc.v_region r
  left join (
  		select rc.region_id, max(rc.id) as rcoi_id
      from gsc.v_rcoi rc
      group by rc.region_id
    ) as max_rcoi on max_rcoi.region_id = r.id
  join gsc.v_rcoi rc on rc.id = max_rcoi.rcoi_id");
        }
    }
}