﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Rcoi
{
    public class RcoiCollectionViewModel : ProjectEntityCollectionViewModel<Model.Rcoi>
    {

        /// <summary>
        /// Creates a new instance of RcoiCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static RcoiCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new RcoiCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the RcoiCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the RcoiCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected RcoiCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                  x => x.Rcois,
                  rq => rq.Include(x => x.Region))
        {
        }

        [UsedImplicitly]
        public void ImportCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.Rcoi.ImportFromCsv);

            var gscContext = ((DbUnitOfWork<Model.GscContext>) CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<ImpRcoi, RcoiCsvMap, Model.ImportResult>(
                rcoi =>
                    gscContext.ImportRowRcoi(CurrentProject.Id,
                                             sessionGuid,
                                             rcoi.ImpRegion,
                                             rcoi.Name,
                                             rcoi.Address,
                                             rcoi.ChiefPost,
                                             rcoi.ChiefFio,
                                             rcoi.ChiefPhone1,
                                             rcoi.Email),
                () => gscContext.ImportRcoi(sessionGuid),
                result => $"Код {result.Code},\t\"{result.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class RcoiCsvMap : CsvClassMap<ImpRcoi>
        {
            public RcoiCsvMap()
            {
                Map(rcoi => rcoi.ImpRegion).Name("REGION").TypeConverterOption(NumberStyles.Integer);
                Map(rcoi => rcoi.Name).Name("RCOIName");
                Map(rcoi => rcoi.Address).Name("RCOIAddress");
                Map(rcoi => rcoi.ChiefPost).Name("RCOIDPosition");
                Map(rcoi => rcoi.ChiefFio).Name("RCOIDFio");
                Map(rcoi => rcoi.ChiefPhone1).Name("RCOIPhones");
                Map(rcoi => rcoi.Email).Name("RCOIEMails");
            }
        }

        [UsedImplicitly]
        private class ImpRcoi : Model.Rcoi
        {
            public int ImpRegion { get; set; }
        }

        [UsedImplicitly]
        public bool CanImportCsv() { return this.OperationAllowed(SecurityOperations.Rcoi.ImportFromCsv); }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Rcoi.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Rcoi.ExportToExcel);
        }

        protected override string SDeleteOperation => SecurityOperations.Rcoi.Delete;

        protected override string SNewOperation => SecurityOperations.Rcoi.New;

        protected override string SEditOperation => SecurityOperations.Rcoi.Edit;

        public override string EntityName => "РЦОИ";
    }
}
