﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Model;

namespace GSC.Client.ViewModels.Rcoi
{
    public class RcoiViewModel : ProjectEntityViewModel<Model.Rcoi>
    {
        /// <summary>
        /// Creates a new instance of RcoiViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static RcoiViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new RcoiViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the RcoiViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the RcoiViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected RcoiViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Rcois, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "РЦОИ";

        public IEntitiesViewModel<Region> LookupRegions
        {
            get { return GetLookUpProjectEntitiesViewModel((RcoiViewModel vm) => vm.LookupRegions, x => x.Regions); }
        }

        protected override string SDeleteOperation => SecurityOperations.Rcoi.Delete;

        protected override string SNewOperation => SecurityOperations.Rcoi.New;

        protected override string SEditOperation => SecurityOperations.Rcoi.Edit;
    }
}
