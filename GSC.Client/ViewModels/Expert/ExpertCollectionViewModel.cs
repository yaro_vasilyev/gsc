﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Expert
{
    public class ExpertCollectionViewModel : ProjectEntityCollectionViewModel<Model.Expert>
    {
        /// <summary>
        /// Creates a new instance of ExpertCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExpertCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpertCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ExpertCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExpertCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExpertCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(
                unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                contextUnitOfWork => contextUnitOfWork.Experts,
                repositoryQuery => repositoryQuery
                    .Include(e => e.Project)
                    .Include(e => e.AcademicDegree)
                    .Include(e => e.AcademicTitle)
                    .Include(e => e.BaseRegion)
                    .Include(e => e.Observer)
                    .Include(e => e.ExpertDocStat))
        {
        }

        [UsedImplicitly]
        public void ImportFromCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.Experts.ImportFromCsv);

            var uw = CreateUnitOfWork();
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<PersData, PersDataCsvMap, ImportExpertsResult>(
                data =>
                    uw.Context.ImportRowExpert(sessionGuid,
                                               data.ImportCode,
                                               data.Surname,
                                               data.Name,
                                               data.Patronymic,
                                               data.Workplace,
                                               data.Post,
                                               data.PostValue,
                                               data.BirthDate,
                                               data.BirthPlace,
                                               data.ImpBaseRegion,
                                               data.LiveAddress,
                                               data.Phone,
                                               data.Email,
                                               data.ImpAcademicDegree,
                                               data.AcademicDegreeValue,
                                               data.ImpAcademicTitle,
                                               data.AcademicTitleValue,
                                               data.Expirience,
                                               data.Speciality,
                                               data.SpecialityValue,
                                               data.WorkYearsTotal,
                                               data.WorkYearsEducation,
                                               data.WorkYearsEducationValue,
                                               data.WorkYearsChief,
                                               data.ExpYearsEducation,
                                               data.ExpYearsEducationValue,
                                               data.Awards,
                                               data.AwardsValue,
                                               BooleanType.Yes.Equals(data.CanMission) ? 1 : 0,
                                               data.CanMissionValue,
                                               data.ChiefName,
                                               data.ChiefPost,
                                               data.ChiefEmail),
                () => uw.Context.ImportExperts(sessionGuid, CurrentProject.Id),
                result => $"Код {result.Code},\t\"{result.Snp}\"",
                this);
        }

        [UsedImplicitly]
        private class PersData : Model.Expert
        {
            public string ImpBaseRegion { get; set; }
            public string ImpAcademicDegree { get; set; }
            public string ImpAcademicTitle { get; set; }
        }

        [UsedImplicitly]
        public bool CanImportFromCsv()
        {
            return this.OperationAllowed(SecurityOperations.Experts.ImportFromCsv);
        }

        [UsedImplicitly]
        private sealed class PersDataCsvMap : CsvClassMap<PersData>
        {
            public PersDataCsvMap()
            {
                Map(persData => persData.ImportCode).Name("Код эксперта").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.Surname).Name("Фамилия");
                Map(persData => persData.Name).Name("Имя");
                Map(persData => persData.Patronymic).Name("Отчество");
                Map(persData => persData.Workplace).Name("Место работы");
                Map(persData => persData.Post).Name("Должность");
                Map(persData => persData.PostValue).Name("Оценка по критерию «должность»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.BirthDate).Name("Дата рождения").TypeConverterOption("dd.MM.yyyy");
                Map(persData => persData.BirthPlace).Name("Место рождения");
                Map(persData => persData.ImpBaseRegion).Name("Базовый субъект");
                Map(persData => persData.LiveAddress).Name("Адрес фактического проживания");
                Map(persData => persData.Phone).Name("Номер телефона");
                Map(persData => persData.Email).Name("Электронная почта");
                Map(persData => persData.ImpAcademicDegree).Name("Ученая степень");
                Map(persData => persData.AcademicDegreeValue).Name("Оценка по критерию «ученая степень»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.ImpAcademicTitle).Name("Ученое звание");
                Map(persData => persData.AcademicTitleValue).Name("Оценка по критерию «ученое звание»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.Expirience).Name("Опыт аналогичной работы");
                Map(persData => persData.Speciality).Name("Образование (специализация)");
                Map(persData => persData.SpecialityValue).Name("Оценка по критерию «образование»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.WorkYearsTotal).Name("Общий стаж работы").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.WorkYearsEducation).Name("Стаж работы в системе образования").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.WorkYearsEducationValue).Name("Оценка по критерию «стаж работы в системе образования»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.WorkYearsChief).Name("Стаж руководящей работы").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.ExpYearsEducation).Name("Опыт работы в системе образования").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.ExpYearsEducationValue).Name("Оценка по критерию «опыт работы в системе образования»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.Awards).Name("Ведомственные награды и звания");
                Map(persData => persData.AwardsValue).Name("Оценка по критерию «ведомственные награды и звания»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.CanMission).Name("Готовность к выезду в другой субъект Российской Федерации").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.CanMissionValue).Name("Оценка по критерию «готовность к выезду в другой субъект Российской Федерации»").TypeConverterOption(NumberStyles.Integer);
                Map(persData => persData.ChiefName).Name("ФИО руководителя");
                Map(persData => persData.ChiefPost).Name("Должность руководителя");
                Map(persData => persData.ChiefEmail).Name("Электронный адрес для направления писем работодателю");
            }
        }

//        public override void OnLoaded()
//        {
//            base.OnLoaded();
//            UpdateCommands();
//        }

        protected override string SNewOperation => SecurityOperations.Experts.New;

        protected override string SEditOperation => SecurityOperations.Experts.Edit;

        protected override string SDeleteOperation => SecurityOperations.Experts.Delete;

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Experts.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Experts.ExportToExcel);
        }

        protected override void UpdateCommands()
        {
            base.UpdateCommands();
            this.RaiseCanExecuteChanged(x => x.ExportToExcel(null));
            this.RaiseCanExecuteChanged(x=>x.ImportFromCsv());
        }

        public override string EntityName => "Эксперт";
    }
}
