﻿using System;
using System.IO;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Expert
{
    public class ExpertViewModel : ProjectEntityViewModel<Model.Expert>
    {

        /// <summary>
        /// Creates a new instance of ExpertViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExpertViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExpertViewModel(unitOfWorkFactory));
        }

        protected override string SNewOperation => SecurityOperations.Experts.New;

        protected override string SEditOperation => SecurityOperations.Experts.Edit;

        protected override string SDeleteOperation => SecurityOperations.Experts.Delete;

        protected override Model.Expert CreateEntity()
        {
            var entity = base.CreateEntity();
            entity.CanMission = Model.BooleanType.Yes;
            return entity;
        }

        protected override string LocalizedEntityName => "Эксперт";

        /// <summary>
        /// Initializes a new instance of the ExpertViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExpertViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExpertViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                   x => x.Experts, x => x.FullName) { }

        #region Lookup datasources

        public IEntitiesViewModel<Model.Region> LookupRegions
        {
            get
            {
                return GetLookUpProjectEntitiesViewModel((ExpertViewModel vm) => vm.LookupRegions,
                                                         x => x.Regions,
                                                         q => q.Where(r => r.ProjectId == CurrentProject.Id));
            }
        }

        public IEntitiesViewModel<Model.Observer> LookupObservers
        {
            get
            {
                return GetLookUpEntitiesViewModel((ExpertViewModel vm) => vm.LookupObservers,
                                                  x => x.Observers,
                                                  q => q.Where(o => o.ProjectId == CurrentProject.Id));
            }
        }

        public IEntitiesViewModel<Model.AcademicDegree> LookupAcademicDegrees
        {
            get
            {
                return GetLookUpEntitiesViewModel((ExpertViewModel vm) => vm.LookupAcademicDegrees,
                    x => x.AcademicDegrees);
            }
        }

        public IEntitiesViewModel<Model.AcademicTitle> LookupAcademicTitles
        {
            get
            {
                return GetLookUpEntitiesViewModel((ExpertViewModel vm) => vm.LookupAcademicTitles, x => x.AcademicTitles);
            }
        }

        #endregion

        #region ExpYearsEducation/ExpYearsEducationValue
        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpYearsEducationChanged))]
        public virtual int ExpYearsEducation
        {
            get { return Entity?.ExpYearsEducation ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.ExpYearsEducation = value;
                    this.RaisePropertyChanged(x => x.ExpYearsEducation);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int ExpYearsEducationValue
        {
            get { return Entity?.ExpYearsEducationValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.ExpYearsEducationValue = value;
                    this.RaisePropertyChanged(x => x.ExpYearsEducationValue);
                }
            }
        }

        protected virtual void OnExpYearsEducationChanged()
        {
            var val = Entity.ExpYearsEducation;
            var ret = 0;
            if (val >= 0 && val < 2)
                ret = 1;
            else if (val >= 2 && val < 4)
                ret = 2;
            else if (val >= 4 && val < 6)
                ret = 3;
            else if (val >= 6 && val < 8)
                ret = 4;
            else if (val >= 8)
                ret = 5;

            ExpYearsEducationValue = ret;
            Update();
        }
        #endregion

        #region WorkYearsEducation/WorkYearsEducationValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnWorkYearsEducationChanged))]
        public virtual int WorkYearsEducation
        {
            get { return Entity?.WorkYearsEducation ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.WorkYearsEducation = value;
                    this.RaisePropertyChanged(x => x.WorkYearsEducation);
                }
            }
        }

        protected virtual void OnWorkYearsEducationChanged()
        {
            var val = Entity.WorkYearsEducation;
            var ret = 0;

            if (val >= 0 && val < 5)
                ret = 1;
            else if (val >= 5 && val < 10)
                ret = 2;
            else if (val >= 10 && val < 15)
                ret = 3;
            else if (val >= 15 && val < 20)
                ret = 4;
            else if (val >= 20)
                ret = 5;

            WorkYearsEducationValue = ret;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int WorkYearsEducationValue
        {
            get { return Entity?.WorkYearsEducationValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.WorkYearsEducationValue = value;
                    this.RaisePropertyChanged(x => x.WorkYearsEducationValue);
                }
            }
        }

        #endregion

        #region CanMission/CanMissionValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnCanMissionChanged))]
        public virtual Model.BooleanType CanMission
        {
            get { return Entity?.CanMission ?? Model.BooleanType.Yes; }
            set
            {
                if (Entity != null)
                {
                    Entity.CanMission = value;
                    this.RaisePropertyChanged(x => x.CanMission);
                }
            }
        }

        protected virtual void OnCanMissionChanged()
        {
            var cm = Entity.CanMission;
            int ret;
            switch (cm)
            {
                case Model.BooleanType.Yes:
                    ret = 5;
                    break;

                default:
                    ret = 0;
                    break;
            }

            CanMissionValue = ret;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int CanMissionValue
        {
            get { return Entity?.CanMissionValue ?? 5; }
            set
            {
                if (Entity != null)
                {
                    Entity.CanMissionValue = value;
                    this.RaisePropertyChanged(x => x.CanMissionValue);
                }
            }
        }

        #endregion

        #region AcademicDegreeId/AcademicDegreeValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnAcademicDegreeIdChanged))]
        public virtual int? AcademicDegreeId
        {
            get { return Entity?.AcademicDegreeId; }
            set
            {
                if (Entity != null)
                {
                    Entity.AcademicDegreeId = value;
                    this.RaisePropertyChanged(x => x.AcademicDegreeId);
                }
            }
        }

        protected virtual void OnAcademicDegreeIdChanged()
        {
            var defVal = 0;
            if (AcademicDegreeId.HasValue)
            {
                var academicDegree = UnitOfWork.AcademicDegrees.Find(AcademicDegreeId.Value);
                defVal = academicDegree?.DefaultVal ?? 0;
            }

            AcademicDegreeValue = defVal;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int AcademicDegreeValue
        {
            get { return Entity?.AcademicDegreeValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.AcademicDegreeValue = value;
                    this.RaisePropertyChanged(x => x.AcademicDegreeValue);
                }
            }
        }

        #endregion

        #region AcademicTitleId/AcademicTitleValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnAcademicTitleIdChanged))]
        public virtual int? AcademicTitleId
        {
            get { return Entity?.AcademicTitleId; }
            set
            {
                if (Entity != null)
                {
                    Entity.AcademicTitleId = value;
                    this.RaisePropertyChanged(x => x.AcademicTitleId);
                }
            }
        }

        protected virtual void OnAcademicTitleIdChanged()
        {
            int defVal = 0;
            if (AcademicTitleId.HasValue)
            {
                var academicTitle = UnitOfWork.AcademicTitles.Find(AcademicTitleId.Value);
                defVal = academicTitle?.DefaultValue ?? 0;
            }

            AcademicTitleValue = defVal;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int AcademicTitleValue
        {
            get { return Entity.AcademicTitleValue; }
            set
            {
                if (Entity != null)
                {
                    Entity.AcademicTitleValue = value;
                    this.RaisePropertyChanged(x => x.AcademicTitleValue);
                }
            }
        }

        #endregion

        #region Awards/AwardsValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnAwardsChanged))]
        public virtual string Awards
        {
            get { return Entity?.Awards; }
            set
            {
                if (Entity != null)
                {
                    Entity.Awards = value;
                    this.RaisePropertyChanged(x => x.Awards);
                }
            }
        }

        protected virtual void OnAwardsChanged()
        {
            AwardsValue = string.IsNullOrWhiteSpace(Awards) ? 0 : 5;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int AwardsValue
        {
            get { return Entity?.AwardsValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.AwardsValue = value;
                    this.RaisePropertyChanged(x => x.AwardsValue);
                }
            }
        }

        #endregion

        #region TestResults/TestResultsValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnTestResultsChanged))]
        public virtual int? TestResults
        {
            get { return Entity.TestResults; }
            set
            {
                if (Entity != null)
                {
                    Entity.TestResults = value;
                    this.RaisePropertyChanged(x => x.TestResults);
                }
            }
        }

        protected virtual void OnTestResultsChanged()
        {
            int? val = null;
            if (TestResults != null)
            {
                int tr = TestResults.Value;
                if (tr >= 0 && tr < 5)
                {
                    val = 0;
                }
                else if (tr >= 5 && tr < 10)
                {
                    val = 1;
                }
                else if (tr >= 10 && tr < 16)
                {
                    val = 2;
                }
                else if (tr >= 16 && tr < 19)
                {
                    val = 3;
                }
                else if (tr >= 19 && tr < 22)
                {
                    val = 4;
                }
                else if (tr >= 22 && tr <= 23)
                {
                    val = 5;
                }
            }

            TestResultsValue = val;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChangedWithEngage))]
        public virtual int? TestResultsValue
        {
            get { return Entity?.TestResultsValue; }
            set
            {
                if (Entity != null)
                {
                    Entity.TestResultsValue = value;
                    this.RaisePropertyChanged(x => x.TestResultsValue);
                }
            }
        }

        #endregion

        #region InterviewStatus/InterviewStatusValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnInterviewStatusChanged))]
        public virtual Model.InterviewStatus? InterviewStatus
        {
            get { return Entity.InterviewStatus; }
            set
            {
                if (Entity != null)
                {
                    Entity.InterviewStatus = value;
                    this.RaisePropertyChanged(x => x.InterviewStatus);
                }
            }
        }

        protected virtual void OnInterviewStatusChanged()
        {
            int? val = null;
            if (InterviewStatus != null)
            {
                var status = InterviewStatus.Value;
                switch (status)
                {
                    case Model.InterviewStatus.Success:
                        val = 5;
                        break;
                    case Model.InterviewStatus.Fail:
                        val = 0;
                        break;
                    default:
                        throw new NotSupportedException($"InterviewStatus value ({status}) is not supported.");
                }
            }

            InterviewStatusValue = val;
            Update();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChangedWithEngage))]
        public virtual int? InterviewStatusValue
        {
            get { return Entity?.InterviewStatusValue; }
            set
            {
                if (Entity != null)
                {
                    Entity.InterviewStatusValue = value;
                    this.RaisePropertyChanged(x => x.InterviewStatusValue);
                }
            }
        }

        #endregion

        #region SpecialityValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int SpecialityValue
        {
            get { return Entity?.SpecialityValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.SpecialityValue = value;
                    this.RaisePropertyChanged(x => x.SpecialityValue);
                }
            }
        }

        #endregion

        #region PostValue

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage1PointsDependencyChanged))]
        public virtual int PostValue
        {
            get { return Entity?.PostValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.PostValue = value;
                    this.RaisePropertyChanged(x => x.PostValue);
                }
            }
        }

        #endregion

        #region OnStage1PointsDependencyChanged

        protected virtual void OnStage1PointsDependencyChanged()
        {
            Stage1points = CalculateStage1Points();
            Update();
        }

        protected virtual void OnStage1PointsDependencyChangedWithEngage()
        {
            Stage1points = CalculateStage1Points();
            Engage = CalculateEngage();
            Update();
        }

        private int? CalculateStage1Points()
        {
            //if (ExpYearsEducationValue == null)
            //    return null;

            // SpecialityValue

            //if (WorkYearsEducationValue == null)
            //    return null;

            //if (CanMissionValue == null)
            //    return null;

            // PostValue

            //if (AcademicDegreeValue == null)
            //    return null;

            if (TestResultsValue == null)
                return null;

            if (InterviewStatusValue == null)
                return null;

            //if (AcademicTitleValue == null)
            //    return null;

            //if (AwardsValue == null)
            //    return null;

            var ret = ExpYearsEducationValue +
                      SpecialityValue +
                      WorkYearsEducationValue +
                      CanMissionValue +
                      PostValue +
                      AcademicDegreeValue +
                      (int)TestResultsValue +
                      (int)InterviewStatusValue +
                      AcademicTitleValue +
                      AwardsValue;
            return ret;
        }

        #endregion

        // ReSharper disable once InconsistentNaming
        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChangedWithEngage))]
        public virtual int? Stage1points
        {
            get { return Entity?.Stage1points; }
            set
            {
                if (Entity != null)
                {
                    Entity.Stage1points = value;
                    this.RaisePropertyChanged(x => x.Stage1points);
                }
            }
        }

        protected virtual void OnStage2PointsDependencyChangedWithEngage()
        {
            Engage = CalculateEngage();
            Stage2points = CalculateStage2Points();
            Update();
        }

        private Model.BooleanType? CalculateEngage()
        {
            if (TestResultsValue == null)
                return null;
            if (InterviewStatusValue == null)
                return null;
            if (Stage1points == null)
                return null;

            var trv = TestResultsValue.Value;
            var isv = InterviewStatusValue.Value;
            var p = Stage1points.Value;

            if (p < 12)
                return Model.BooleanType.No;
            if (trv < 3)
                return Model.BooleanType.No;
            if (isv == 0)
                return Model.BooleanType.No;

            return Model.BooleanType.Yes;
        }

        public virtual Model.BooleanType? Engage
        {
            get { return Entity?.Engage; }
            set
            {
                if (Entity != null)
                {
                    Entity.Engage = value;
                    this.RaisePropertyChanged(x => x.Engage);
                    Update();
                }
            }
        }

        #region Stage2points

        // ReSharper disable once InconsistentNaming
        [BindableProperty(OnPropertyChangedMethodName = nameof(OnEfficiencyDependencyChanged))]
        public virtual int? Stage2points
        {
            get { return Entity?.Stage2points; }
            set
            {
                if (Entity != null)
                {
                    Entity.Stage2points = value;
                    this.RaisePropertyChanged(x => x.Stage2points);
                }
            }
        }

        #endregion

        #region ReportsCount/ReportsCountValue

        //[BindableProperty(OnPropertyChangedMethodName = nameof(OnReportsCountChanged))]
        public virtual int ReportsCount
        {
            get { return Entity?.ExpertDocStat?.DocumentsCount ?? 0; }
            protected set { }
        }

        protected virtual void OnReportsCountChanged()
        {
            Update();
        }

        private int ComputeReportsCountValue(int reportsCount)
        {
            var rv = 0;
            if (reportsCount == 0)
                rv = 0;
            if (reportsCount >= 1 && reportsCount < 5)
                rv = 1;
            if (reportsCount >= 5 && reportsCount < 10)
                rv = 2;
            if (reportsCount >= 10 && reportsCount < 15)
                rv = 3;
            if (reportsCount >= 15 && reportsCount < 20)
                rv = 4;
            if (reportsCount >= 20)
                rv = 5;

            return rv;
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int ReportsCountValue
        {
            get { return ComputeReportsCountValue(ReportsCount); }
            protected set { }
        }

        public void RaiseReportsCountValueChanged() { this.RaisePropertyChanged(x => x.ReportsCountValue); }

        #endregion

        private Model.ExpertAgreement agreement;

        //[BindableProperty]
        public virtual Model.ExpertAgreement Agreement
        {
            get { return agreement; }
            set
            {
                agreement = value;
                OnAgreementChanged();
            }
        }

        protected virtual void OnAgreementChanged()
        {
            var exists = Agreement != null;

            AddVisible = !exists;
            RemoveVisible = exists;
            PreviewVisible = exists;
            AgreementMessage = exists
                ? "Согласен на обработку персональных данных"
                : "Нет согласия на обработку персональных данных";

            this.RaisePropertyChanged(vm => vm.Agreement);
            UpdateCommands();
        }

        #region Entity

        protected override void OnEntityChanged()
        {
            agreement = null; // w/o notify

            LoadExpertAgreement();

            base.OnEntityChanged();
        }

        private void LoadExpertAgreement()
        {
            ExpertAgreementWasAddedOrRemoved = false;

            if (Entity == null)
                return;

            var a = UnitOfWork.ExpertAgreements
                .Select(x => new
                    {
                        x.ExpertId,
                        x.Filename,
                        x.Timestamp,
                    })
                .FirstOrDefault(x => x.ExpertId == Entity.Id);

            if (a != null)
            {
                Agreement = new Model.ExpertAgreement
                    {
                        ExpertId = a.ExpertId,
                        Filename = a.Filename,
                        Timestamp = a.Timestamp
                    };
            }
            else
            {
                // for initial SetTrigger call
                Agreement = null;
            }
        }

        #endregion

        protected virtual void OnStage2PointsDependencyChanged()
        {
            Stage2points = CalculateStage2Points();
            Update();
        }

        private int? CalculateStage2Points()
        {
            if (Stage1points == null)
                return null;

            //if (ReportsCountValue == null)
            //    return null;
            //if (DocsImprovementParticipationLevelValue == null)
            //    return null;
            //if (SubjectLevelValue == null)
            //    return null;
            //if (AccuracyLevelValue == null)
            //    return null;
            //if (PunctualLevelValue == null)
            //    return null;
            //if (IndependencyLevelValue == null)
            //    return null;
            //if (MoralLevelValue == null)
            //    return null;

            int? points = (int)Stage1points +
                          ReportsCountValue +
                          DocsImprovementParticipationLevelValue +
                          SubjectLevelValue +
                          AccuracyLevelValue +
                          PunctualLevelValue +
                          IndependencyLevelValue +
                          MoralLevelValue;

            return points;
        }


        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int DocsImprovementParticipationLevelValue
        {
            get { return Entity?.DocsImprovementParticipationLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.DocsImprovementParticipationLevelValue = value;
                    this.RaisePropertyChanged(x => x.DocsImprovementParticipationLevelValue);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int SubjectLevelValue
        {
            get { return Entity?.SubjectLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.SubjectLevelValue = value;
                    this.RaisePropertyChanged(x => x.SubjectLevelValue);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int AccuracyLevelValue
        {
            get { return Entity?.AccuracyLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.AccuracyLevelValue = value;
                    this.RaisePropertyChanged(x => x.AccuracyLevelValue);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int PunctualLevelValue
        {
            get { return Entity?.PunctualLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.PunctualLevelValue = value;
                    this.RaisePropertyChanged(x => x.PunctualLevelValue);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int IndependencyLevelValue
        {
            get { return Entity?.IndependencyLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.IndependencyLevelValue = value;
                    this.RaisePropertyChanged(x => x.IndependencyLevelValue);
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnStage2PointsDependencyChanged))]
        public virtual int MoralLevelValue
        {
            get { return Entity?.MoralLevelValue ?? 0; }
            set
            {
                if (Entity != null)
                {
                    Entity.MoralLevelValue = value;
                    this.RaisePropertyChanged(x => x.MoralLevelValue);
                }
            }
        }


        protected virtual void OnEfficiencyDependencyChanged()
        {
            Efficiency = CalculateEfficiency();
            Update();
        }

        private Model.ExpertEfficiency? CalculateEfficiency()
        {
            if (Stage2points == null)
                return null;

            var p = Stage2points.Value;
            if (p < 12)
                return null;
            if (p >= 12 && p < 18)
                return Model.ExpertEfficiency.None;
            if (p >= 18 && p < 40)
                return Model.ExpertEfficiency.Low;
            if (p >= 40 && p < 60)
                return Model.ExpertEfficiency.Mid;
            if (p >= 60 && p < 86)
                return Model.ExpertEfficiency.High;

            return null;
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnNextPeriodProposalDependencyChanged))]
        public virtual Model.ExpertEfficiency? Efficiency
        {
            get { return Entity?.Efficiency; }
            set
            {
                if (Entity != null)
                {
                    Entity.Efficiency = value;
                    this.RaisePropertyChanged(x => x.Efficiency);
                }
            }
        }

        protected virtual void OnNextPeriodProposalDependencyChanged()
        {
            NextPeriodProposal = CalculateNextYearProposal();
            Update();
        }

        private Model.ProposalType? CalculateNextYearProposal()
        {
            if (Efficiency == null)
                return null;

            var e = Efficiency.Value;
            switch (e)
            {
                case Model.ExpertEfficiency.None:
                    return Model.ProposalType.Exclude;

                case Model.ExpertEfficiency.Low:
                    return Model.ProposalType.Reserve;

                case Model.ExpertEfficiency.Mid:
                case Model.ExpertEfficiency.High:
                    return Model.ProposalType.Main;

                default:
                    throw new NotSupportedException($"Значение {e} не поддерживается.");
            }
        }


        public virtual Model.ProposalType? NextPeriodProposal
        {
            get { return Entity?.NextPeriodProposal; }
            set
            {
                if (Entity != null)
                {
                    Entity.NextPeriodProposal = value;
                    this.RaisePropertyChanged(x => x.NextPeriodProposal);
                }
            }
        }

        #region Kind

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnKindChanged))]
        public virtual Model.ExpertKind Kind
        {
            get { return Entity.Kind; }
            set
            {
                if (Entity.Kind != value)
                {
                    Entity.Kind = value;
                    this.RaisePropertyChanged(x => x.Kind);
                }
            }
        }

        protected void OnKindChanged()
        {
            RefreshLookUpCollection((ExpertViewModel vm) => vm.LookupObservers, true);
        }

        #endregion

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnAddVisibleChanged))]
        public virtual bool AddVisible
        {
            get { return addVisible && !ReadOnly; }
            set { addVisible = value; }
        }

        protected virtual void OnAddVisibleChanged() { }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRemoveVisibleChanged))]
        public virtual bool RemoveVisible
        {
            get { return removeVisible && !ReadOnly; }
            set { removeVisible = value; }
        }

        protected virtual void OnRemoveVisibleChanged() { }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPreviewVisibleChanged))]
        public virtual bool PreviewVisible { get; set; }

        protected virtual void OnPreviewVisibleChanged() { }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnAgreementMessageChanged))]
        public virtual string AgreementMessage { get; set; }

        protected virtual void OnAgreementMessageChanged() { }

        private bool expertAgreementWasAddedOrRemoved = false;
        private bool addVisible;
        private bool removeVisible;

        private bool ExpertAgreementWasAddedOrRemoved
        {
            get { return expertAgreementWasAddedOrRemoved; }
            set
            {
                if (expertAgreementWasAddedOrRemoved != value)
                {
                    expertAgreementWasAddedOrRemoved = value;
                    UpdateCommands();
                }
            }
        }

        public void AddExpertAgreement()
        {
            CanAddExpertAgreement();

            this.SecurityService.EnsureOperationAllowed(SecurityOperations.Experts.AddAgreement);

            var ofd = this.GetRequiredService<IOpenFileDialogService>();

            if (ofd.ShowDialog())
            {
                var fileInfo = ofd.File;
                var agt = UnitOfWork.ExpertAgreements.Create(/*add: false*/);
                agt.ExpertId = Entity.Id;
                agt.Expert = Entity;
                var path = fileInfo.GetFullName();
                if (File.Exists(path))
                {
                    agt.Blob = File.ReadAllBytes(path);
                    agt.Filename = fileInfo.Name;
                }
                agt.Timestamp = DateTime.Now; // will be overwritten by insert-trigger on db
                Agreement = agt;
                ExpertAgreementWasAddedOrRemoved = true;
            }
        }

        public bool CanAddExpertAgreement()
        {
            // CanXxx-methods are not working with hyperlinks
            return SecurityService.OperationAllowed(SecurityOperations.Experts.AddAgreement) && Agreement == null;
        }

        public void RemoveExpertAgreement()
        {
            CanRemoveExpertAgreement();

            this.SecurityService.EnsureOperationAllowed(SecurityOperations.Experts.RemoveAgreement);

            Model.ExpertAgreement ag = null;
            if (Agreement != null)
            {
                ag = Agreement.Expert == null
                    ? UnitOfWork.ExpertAgreements.FirstOrDefault(ea => ea.ExpertId == Agreement.ExpertId)
                    : Agreement;
            }

            if (ag != null)
            {
                UnitOfWork.ExpertAgreements.Remove(ag);
                Agreement = null;
                ExpertAgreementWasAddedOrRemoved = true;
            }
        }

        public bool CanRemoveExpertAgreement()
        {
            // CanXxx-methods are not working with hyperlinks
            return SecurityService.OperationAllowed(SecurityOperations.Experts.RemoveAgreement) && Agreement != null;
        }

        public void PreviewExpertAgreement()
        {
            CanPreviewExpertAgreement();

            this.SecurityService.EnsureOperationAllowed(SecurityOperations.Experts.PreviewAgreement);

            var temp = Path.GetTempPath();
            var x = UnitOfWork.ExpertAgreements.Find(Agreement.ExpertId);
            var path = DownloadFileInternal(x, temp);

            if (!string.IsNullOrEmpty(path))
            {
                RunFile(path);
            }
        }

        private void RunFile(string path)
        {
            // TODO: Extract to service
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
            {
                FileName = path,
                UseShellExecute = true,
                Verb = "open"
            });
        }


        private string DownloadFileInternal(Model.ExpertAgreement doc, string toFolder)
        {
            if (doc?.Blob != null)
            {
                var savePath = GuardFileNameOnSave(toFolder, doc.Filename);
                File.WriteAllBytes(savePath, doc.Blob);
                return savePath;
            }
            return null;
        }

        private string GuardFileNameOnSave(string folder, string baseFileName)
        {
            var savePath = Path.Combine(folder, baseFileName);

            for (int counter = 1; counter <= 20 && File.Exists(savePath); ++counter)
            {
                savePath = Path.Combine(folder,
                                        string.Format("{0} ({1}){2}",
                                                      Path.GetFileNameWithoutExtension(baseFileName),
                                                      counter,
                                                      Path.GetExtension(baseFileName)));
            }

            return savePath;
        }

        public bool CanPreviewExpertAgreement()
        {
            // CanXxx-methods are not working with hyperlinks
            return SecurityService.OperationAllowed(SecurityOperations.Experts.PreviewAgreement) && Agreement != null;
        }

        protected override bool NeedSave()
        {
            var needSave = base.NeedSave();
            return needSave ||
                   ExpertAgreementWasAddedOrRemoved;
        }
    }
}
