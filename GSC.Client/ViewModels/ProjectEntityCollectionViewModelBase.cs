using System;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TPrimaryKey"></typeparam>
    /// <remarks>� ������������ ����� ��������� <c>FilterExpression</c>, �� ����������� ������������ ������ �������� ���������, 
    /// ���� ��������� � <c>EDMX</c>. ��������� � ����������� ��������� ������� ��������������, �� �������� �� �����.</remarks>
    public abstract class ProjectEntityCollectionViewModelBase<TEntity, TPrimaryKey> 
        : CollectionViewModel<TEntity, TPrimaryKey, IGscContextUnitOfWork>
        , ISecureViewModel
        , ISupportReadOnlyViewModel
        where TEntity : class, IProjectEntity
    {
        protected ProjectEntityCollectionViewModelBase(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
                                                       Func<IGscContextUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
                                                       Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection = null,
                                                       Action<TEntity> newEntityInitializer = null,
                                                       Func<bool> canCreateNewEntity = null,
                                                       bool ignoreSelectEntityMessage = false)
            : base(unitOfWorkFactory, getRepositoryFunc, projection,
                newEntityInitializer, canCreateNewEntity, ignoreSelectEntityMessage)
        {
            FilterExpression = x => x.ProjectId == CurrentProject.Id;
            projectService = this.GetService<IProjectService>();
        }

        private readonly IProjectService projectService;

        protected Model.Project CurrentProject
        {
            get
            {
                Debug.Assert(projectService != null);
                return projectService.GetCurrentProject().Match(x => x,
                    e =>
                    {
                        throw e; // TODO: Wrap?
                    });
            }
        }

        //protected virtual string SNewOperation => "__reserved_new";
        protected abstract string SNewOperation { get; }

        //protected virtual string SEditOperation => "__reserved_edit";
        protected abstract string SEditOperation { get; }

        //protected virtual string SDeleteOperation => "__reserved_delete";
        protected abstract string SDeleteOperation { get; }

        public virtual bool CanNew()
        {
            return !ReadOnly && this.OperationAllowed(SNewOperation);
        }

        public override void New()
        {
            this.EnsureOperationAllowed(SNewOperation);
            base.New();
        }

        public override bool CanEdit(TEntity projectionEntity)
        {
            return base.CanEdit(projectionEntity) &&
                   !ReadOnly &&
                   this.OperationAllowed(SEditOperation);
        }

        public override void Edit(TEntity projectionEntity)
        {
            this.EnsureOperationAllowed(SEditOperation);
            base.Edit(projectionEntity);
        }

        public override bool CanDelete(TEntity projectionEntity)
        {
            return base.CanDelete(projectionEntity) &&
                   !ReadOnly &&
                   this.OperationAllowed(SDeleteOperation);
        }

        public override void Delete(TEntity projectionEntity)
        {
            this.EnsureOperationAllowed(SDeleteOperation);
            base.Delete(projectionEntity);
        }

        public override void Refresh()
        {
            UpdateCommands();
            base.Refresh();
        }

        protected override void OnIsLoadingChanged()
        {
            base.OnIsLoadingChanged();
            UpdateCommands();
        }

        protected virtual void UpdateCommands()
        {
            this.RaiseCanExecuteChanged(x => x.New());
            this.RaiseCanExecuteChanged(x => x.Edit(null));
            this.RaiseCanExecuteChanged(x => x.Delete(null));
            this.RaiseCanExecuteChanged(x => x.View(null));
        }

        public virtual void View(TEntity projectionEntity)
        {
            EditCore(projectionEntity);
        }

        public virtual bool CanView(TEntity projectionEntity)
        {
            return projectionEntity != null && !IsLoading;
        }

        public virtual bool ReadOnly => CurrentProject.Status != ProjectStatus.Opened;
    }
}