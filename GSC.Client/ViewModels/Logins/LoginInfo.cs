﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSC.Client.ViewModels.Logins
{
    public class LoginInfo
    {
        public Guid Guid { get; set; }
        public string Login { get; set; }
        public string Fio { get; set; }
        /// <summary>
        /// Роль из gsc.users
        /// </summary>
        public string RegistrationRole { get; set; }
        /// <summary>
        /// Список ролей r_* юзера
        /// </summary>
        public string RoleList { get; set; }
        public bool WebAccess { get; set; }
    }
}
