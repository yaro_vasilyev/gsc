﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Logins
{
    public class LoginCollectionViewModel : CollectionViewModel<Model.Login, int, IGscContextUnitOfWork>
    {
        [UsedImplicitly]
        public LoginCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
            Func<IGscContextUnitOfWork, IRepository<Model.Login, int>> getRepositoryFunc,
            Func<IRepositoryQuery<Model.Login>, IQueryable<Model.Login>> projection = null,
            Action<Model.Login> newEntityInitializer = null, Func<bool> canCreateNewEntity = null,
            bool ignoreSelectEntityMessage = false)
            : base(
                unitOfWorkFactory, getRepositoryFunc, projection)
        {
        }

        /// <summary>
        /// Creates a new instance of LoginCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static LoginCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new LoginCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the LoginCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the LoginCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected LoginCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(
                unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                contextUnitOfWork => contextUnitOfWork.Logins,
                query => query.Include(l => l.UserRoles).Include(l => l.Observer).Include(l => l.Expert))
        {
            var project = ProjectService.GetCurrentProject().Match(p => p.Id, e => { throw e; });
            FilterExpression =
                x =>
                    (x.Expert != null && x.Expert.ProjectId == project) ||
                    (x.Observer != null && x.Observer.ProjectId == project) ||
                    (x.Observer == null && x.Expert == null);
        }

        public override bool CanEdit(Model.Login projectionEntity)
        {
            return projectionEntity != null && projectionEntity.PersonType != PersonType.Expert;
        }
    }
}