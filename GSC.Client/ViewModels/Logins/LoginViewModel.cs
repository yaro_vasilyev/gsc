﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Util;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Logins
{
    public class LoginViewModel : SingleObjectViewModel<Model.Login, int, IGscContextUnitOfWork>
    {
        private bool initialized;
        private PersonType selectedPersonType = PersonType.Manager;
        private bool isRolesModified;
        public const string UserRolesGridServiceKey = "UserRoles";
        public const string AllRolesGridServiceKey = "AllRoles";

        /// <summary>
        /// Initializes a new instance of the LoginViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public LoginViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Logins, x => x.PersonName)
        {
        }

        protected override void OnEntityChanged()
        {
            base.OnEntityChanged();

            UpdateRoles();

            if (initialized) return;
            if (IsEditMode && Entity != null)
            {
                if (Entity.ExpertId != null)
                {
                    SelectedPersonType = PersonType.Expert;
                }
                else if (Entity.Observer != null)
                {
                    switch (Entity.Observer.Type)
                    {
                        case ObserverType.Manager:
                            SelectedPersonType = PersonType.Manager;
                            break;
                        case ObserverType.Rsm:
                            SelectedPersonType = PersonType.RsmManager;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(Entity));
                    }
                }
                this.RaisePropertyChanged(vm => vm.PersonId); // без этого не происходит установка выбранного эксперта/менеджера, т.к. на момент биндинга Entity = null
            }
            initialized = true;
        }

        private void UpdateRoles()
        {
            UserRoles.Clear();
            Entity?.UserRoles.ForEach(UserRoles.Add);

            this.RaisePropertyChanged(vm => vm.SelectedUserRoles);
            this.RaisePropertyChanged(vm => vm.SelectedRoles);

            this.GetService<IUpdateGridService>(UserRolesGridServiceKey)?.Update();
            var clearSelection = SelectedRoles == null || SelectedRoles.Length == 0;
            this.GetService<IUpdateGridService>(AllRolesGridServiceKey)?.Update(clearSelection);
        }

        private Model.Project CurrentProject
        {
            get { return ProjectService.GetCurrentProject().Match(p => p, e => { throw e; }); }
        }

        /// <summary>
        /// Creates a new instance of LoginViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static LoginViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new LoginViewModel(unitOfWorkFactory));
        }

        public IEntitiesViewModel<PersonBinding> LookupPerson
        {
            get
            {
                switch (SelectedPersonType)
                {
                    case PersonType.Expert:
                        // сейчас этот тип персоны убран из radio group, т.к. привязывать к экспертам нельзя - это делается в доступе к личному кабинету
                        return GetLookUpProjectionsViewModel((LoginViewModel vm) => vm.LookupPerson, x => x.Experts,
                            q =>
                                q.Select(
                                    e =>
                                        new PersonBinding
                                        {
                                            Id = e.Id,
                                            PersonType = PersonType.Expert,
                                            Name = e.Surname + " " + e.Name + " " + e.Patronymic
                                        }));
                    case PersonType.Manager:
                    case PersonType.RsmManager:
                        return GetLookUpProjectionsViewModel((LoginViewModel vm) => vm.LookupPerson, x => x.Observers,
                            q =>
                                q.Where(
                                        o =>
                                            o.Type ==
                                            (SelectedPersonType == PersonType.Manager ? ObserverType.Manager : ObserverType.Rsm) &&
                                            (o.ProjectId == CurrentProject.Id))
                                    .Select(
                                        o =>
                                            new PersonBinding
                                            {
                                                Id = o.Id,
                                                PersonType = PersonType.Manager,
                                                Name = o.Fio
                                            }));
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPersonTypeChanged))]
        public virtual PersonType SelectedPersonType
        {
            get { return selectedPersonType; }
            [UsedImplicitly]
            set
            {
                selectedPersonType = value;
                this.RaisePropertyChanged(vm => vm.SelectedPersonType);
                this.RaisePropertyChanged(vm => vm.PersonId);
            }
        }

        public int? PersonId
        {
            get
            {
                return Entity?.PersonId;
            }

            [UsedImplicitly]
            set
            {
                Entity?.SetPerson(SelectedPersonType, value);
                Update();

                this.RaisePropertyChanged(x => x.PersonId);
            }
        }

        [UsedImplicitly]
        protected void OnPersonTypeChanged()
        {
            RefreshLookUpCollection((LoginViewModel vm) => vm.LookupPerson, true);
            this.RaisePropertyChanged(vm => vm.PersonId);
            this.RaisePropertyChanged(vm => vm.LookupPerson);
        }

        public IEntitiesViewModel<Role> LookupRoles
        {
            get { return GetLookUpEntitiesViewModel((LoginViewModel vm) => vm.LookupRoles, x => x.Roles); }
        }

        public ObservableCollection<UserRole> UserRoles { get; } = new ObservableCollection<UserRole>();

        public class PersonBinding
        {
            public int Id { get; set; }
            public PersonType? PersonType { get; set; }
            public string Name { get; set; }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnSelectedUserRolesChanged))]
        public virtual UserRole[] SelectedUserRoles { get; set; }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnSelectedRolesChanged))]
        public virtual Role[] SelectedRoles { get; set; }

        [UsedImplicitly]
        protected void OnSelectedUserRolesChanged()
        {
            Update();
        }

        [UsedImplicitly]
        protected void OnSelectedRolesChanged()
        {
            Update();
        }

        [UsedImplicitly]
        [Command(CanExecuteMethodName = nameof(CanDeleteSelectedUserRoles))]
        public void DeleteSelectedUserRoles()
        {
            Debug.Assert(SelectedUserRoles != null);

            foreach (var role in SelectedUserRoles)
            {
                Entity.UserRoles.Remove(role);
            }

            SelectedUserRoles = null;

            UpdateRoles();
            isRolesModified = true;
            Update();
        }

        [UsedImplicitly]
        [Command(CanExecuteMethodName = nameof(CanAddSelectedRoles))]
        public void AddSelectedRoles()
        {
            Debug.Assert(SelectedRoles != null);

            foreach (var role in SelectedRoles)
            {
                if (
                    Entity.UserRoles.Any(
                        ur => ur.SystemRole.Equals(role.SystemName, StringComparison.InvariantCultureIgnoreCase)))
                    continue;

                var userRole = UnitOfWork.UserRoles.Create();
                userRole.UserId = Entity.Id;
                userRole.SystemRole = role.SystemName;
                Entity.UserRoles.Add(userRole);
            }

            SelectedRoles = null;

            UpdateRoles();
            isRolesModified = true;
            Update();
        }

        protected override bool NeedSave()
        {
            return isRolesModified || base.NeedSave();
        }

        protected override void OnEntitySaved(int primaryKey, Model.Login entity, bool isNewEntity)
        {
            base.OnEntitySaved(primaryKey, entity, isNewEntity);
            isRolesModified = false;
        }

        [UsedImplicitly]
        public bool CanDeleteSelectedUserRoles()
        {
            return SelectedUserRoles != null && SelectedUserRoles.Length > 0;
        }

        [UsedImplicitly]
        public bool CanAddSelectedRoles()
        {
            return SelectedRoles != null && SelectedRoles.Length > 0;
        }

        protected override void UpdateCommands()
        {
            base.UpdateCommands();
            this.RaiseCanExecuteChanged(vm => vm.DeleteSelectedUserRoles());
            this.RaiseCanExecuteChanged(vm => vm.AddSelectedRoles());
        }
    }
}