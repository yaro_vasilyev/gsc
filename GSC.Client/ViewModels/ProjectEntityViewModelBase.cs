using System;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    public abstract class ProjectEntityViewModelBase<TEntity, TPrimaryKey> 
        : SingleObjectViewModel<TEntity, TPrimaryKey, IGscContextUnitOfWork>
        , ISecureViewModel
        , ISupportReadOnlyViewModel
        where TEntity : class
    {
        /// <summary>
        /// Initializes a new instance of the SingleObjectViewModel class.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create the unit of work instance.</param>
        /// <param name="getRepositoryFunc">A function that returns the repository representing entities of a given type.</param>
        /// <param name="getEntityDisplayNameFunc">An optional parameter that provides a function to obtain the display text for a given entity. If ommited, the primary key value is used as a display text.</param>
        protected ProjectEntityViewModelBase(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory, 
                                             Func<IGscContextUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
                                             Func<TEntity, object> getEntityDisplayNameFunc = null)
            : base(unitOfWorkFactory, getRepositoryFunc, getEntityDisplayNameFunc)
        {
        }

        protected Model.Project CurrentProject
        {
            get
            {
                return ProjectService.GetCurrentProject().Match(
                    p => p,
                    e => { throw e; });
            }
        }

        protected override void OnBeforeEntitySaved(TPrimaryKey primaryKey, TEntity entity, bool isNewEntity)
        {
            base.OnBeforeEntitySaved(primaryKey, entity, isNewEntity);
            var pe = entity as IProjectEntity;
            if (isNewEntity && pe != null)
            {
                pe.ProjectId = CurrentProject.Id;
            }
        }

        protected virtual string SDeleteOperation => "__reserved_delete";

        protected virtual string SNewOperation => "__reserved_new";

        protected virtual string SEditOperation => "__reserved_edit";

        public override bool CanDelete()
        {
            return base.CanDelete() && !ReadOnly && this.OperationAllowed(SDeleteOperation);
        }

        public override void Delete()
        {
            this.EnsureOperationAllowed(SDeleteOperation);
            base.Delete();
        }

        public override bool CanSave()
        {
                return base.CanSave() && 
                !ReadOnly && 
                this.OperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
        }

        public override void Save()
        {
            this.EnsureOperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
            base.Save();
        }

        protected override bool CanCreateNewEntity()
        {
            return base.CanCreateNewEntity() && 
                !ReadOnly &&
                this.OperationAllowed(SNewOperation);
        }

        protected override TEntity CreateEntity()
        {
            this.EnsureOperationAllowed(SNewOperation);
            return base.CreateEntity();
        }

        public virtual bool ReadOnly => CurrentProject.Status != ProjectStatus.Opened;
    }
}