﻿using System;
using System.Linq;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    /// <remarks>В конструкторе можно присвоить <c>FilterExpression</c>, но разрешается использовать только свойства сущностей, 
    /// явно описанные в <c>EDMX</c>. Обращение в вычисляемым свойствам успешно скомпилируется, но работать не будет.</remarks>
    public abstract class ProjectEntityCollectionViewModel<TEntity> 
        : ProjectEntityCollectionViewModelBase<TEntity, int>
        where TEntity : class, IProjectEntity
    {
        protected ProjectEntityCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
            Func<IGscContextUnitOfWork, IRepository<TEntity, int>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection = null,
            Action<TEntity> newEntityInitializer = null,
            Func<bool> canCreateNewEntity = null,
            bool ignoreSelectEntityMessage = false)
            : base(unitOfWorkFactory, getRepositoryFunc, projection,
                  newEntityInitializer, canCreateNewEntity, ignoreSelectEntityMessage)
        {
        }
    }
}
