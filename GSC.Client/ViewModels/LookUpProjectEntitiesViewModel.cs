﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    public class LookUpProjectEntitiesViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork> 
        : LookUpEntitiesViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
        where TEntity : class, IProjectEntity
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Creates a new instance of LookUpEntitiesViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        /// <param name="getRepositoryFunc">A function that returns a repository representing entities of the given type.</param>
        /// <param name="projection">An optional parameter that provides a LINQ function used to customize a query for entities. The parameter, for example, can be used for sorting data and/or for projecting data to a custom type that does not match the repository entity type.</param>
        public new static LookUpProjectEntitiesViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork> Create(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IReadOnlyRepository<TEntity>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection = null)
        {
            return ViewModelSource.Create(() => new LookUpProjectEntitiesViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>(unitOfWorkFactory, getRepositoryFunc, projection));
        }

        /// <summary>
        /// Initializes a new instance of the LookUpEntitiesViewModel class.
        /// This constructor is declared protected to avoid an undesired instantiation of the LookUpEntitiesViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        /// <param name="getRepositoryFunc">A function that returns a repository representing entities of the given type.</param>
        /// <param name="projection">A LINQ function used to customize a query for entities. The parameter, for example, can be used for sorting data and/or for projecting data to a custom type that does not match the repository entity type.</param>
        protected LookUpProjectEntitiesViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IReadOnlyRepository<TEntity>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection
            ) : base(unitOfWorkFactory, getRepositoryFunc, projection)
        {
        }

        protected override Expression<Func<TEntity, bool>> GetFilterExpression()
        {
            var projectService = this.GetRequiredService<IProjectService>();
            var project = projectService.GetCurrentProject().Match(p => p, e => { throw e; });

            return e => e.ProjectId == project.Id;
        }
    }
}
