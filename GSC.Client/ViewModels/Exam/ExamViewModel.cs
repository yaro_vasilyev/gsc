﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Model;

namespace GSC.Client.ViewModels.Exam
{
    /// <summary>
    /// Represents the single ExamViewModel object view model.
    /// </summary>
    public class ExamViewModel : ProjectEntityViewModel<Model.Exam>
    {

        /// <summary>
        /// Creates a new instance of ExamViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExamViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExamViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ExamViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExamViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExamViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Exams)
        {
        }

        public IEntitiesViewModel<Subject> LookupSubjects
        {
            get
            {
                return GetLookUpProjectEntitiesViewModel(
                    (ExamViewModel vm) => vm.LookupSubjects,
                    x => x.Subjects);
            }
        }

        protected override string SNewOperation => SecurityOperations.Exams.New;

        protected override string SEditOperation => SecurityOperations.Exams.Edit;

        protected override string SDeleteOperation => SecurityOperations.Exams.Delete;

        protected override string LocalizedEntityName => "Расписание";

    }
}
