﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Exam
{
    /// <summary>
    /// Represents the Exam collection view model.
    /// </summary>
    public class ExamCollectionViewModel 
        : ProjectEntityCollectionViewModel<Model.Exam>
    {
        /// <summary>
        /// Creates a new instance of ExamCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExamCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExamCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ExamCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExamCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExamCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.Exams, rq => rq.Include(x => x.Subject))
        {
            var project = CurrentProject.Id;
            FilterExpression = e => e.Subject.ProjectId == project;
        }

        [UsedImplicitly]
        public void ImportFromCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.Exams.ImportFromCsv);

            var uw = CreateUnitOfWork();
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<ImpExam, ExamCsvMap, Model.ImportExamResult>(
                exam =>
                    uw.Context.ImportRowExam(sessionGuid,
                                             exam.Examglobalid,
                                             exam.ImpSubjectCode,
                                             (int?) exam.ExamType,
                                             (int?) exam.PeriodType,
                                             exam.ExamDate),
                () => uw.Context.ImportExam(sessionGuid, CurrentProject.Id),
                result => $"Код {result.Globalid},\t\"{result.Exam}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class ExamCsvMap : CsvClassMap<ImpExam>
        {
            public ExamCsvMap()
            {
                Map(exam => exam.Examglobalid).Name("ExamGlobalID").TypeConverterOption(NumberStyles.Integer);
                Map(exam => exam.ImpSubjectCode).Name("SubjectCode").TypeConverterOption(NumberStyles.Integer);
                Map(exam => exam.ExamType).Name("TestType").TypeConverterOption(NumberStyles.Integer);
                Map(exam => exam.PeriodType).Name("Wave").TypeConverterOption(NumberStyles.Integer);
                Map(exam => exam.ExamDate).Name("ExamDate").TypeConverterOption("yyyy.MM.dd");
            }
        }

        [UsedImplicitly]
        private class ImpExam : Model.Exam
        {
            public int ImpSubjectCode { get; set; }
        }

        [UsedImplicitly]
        public bool CanImportFromCsv()
        {
            return this.OperationAllowed(SecurityOperations.Exams.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Exams.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Exams.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.Exams.New;

        protected override string SEditOperation => SecurityOperations.Exams.Edit;

        protected override string SDeleteOperation => SecurityOperations.Exams.Delete;

        public override string EntityName
        {
            get { return "Расписание(я)"; }
        }
    }
}