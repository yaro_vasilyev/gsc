﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels.Exam
{
    /// <summary>
    /// Represents the Exam collection view model.
    /// </summary>
    public class ExamCollectionReadOnlyViewModel : ReadOnlyCollectionViewModel<GSC.Model.Exam, IGscContextUnitOfWork>
    {

        /// <summary>
        /// Creates a new instance of ExamCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ExamCollectionReadOnlyViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ExamCollectionReadOnlyViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ExamCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExamCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ExamCollectionReadOnlyViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                  x => x.Exams)
        {
        }
    }
}
