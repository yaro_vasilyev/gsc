﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Model;

namespace GSC.Client.ViewModels.OivGek
{
    public class OivGekViewModel : ProjectEntityViewModel<Model.OivGek>
    {
        /// <summary>
        /// Creates a new instance of OivGekViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static OivGekViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new OivGekViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the OivGekViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the OivGekViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected OivGekViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                   x => x.OivGeks,
                   x => x?.Region?.Name ?? "") {}

        public IEntitiesViewModel<Region> LookupRegions
        {
            get { return GetLookUpProjectEntitiesViewModel((OivGekViewModel vm) => vm.LookupRegions, x => x.Regions); }
        }

        public void UpdateContacts()
        {
            //((DbUnitOfWork<GscContext>)UnitOfWork).Context.Entry(Entity).State = EntityState.Modified;
            Update();
        }

        protected override string LocalizedEntityName => "ОИВ/ГЭК";

        protected override string SNewOperation => SecurityOperations.OivGeks.New;

        protected override string SEditOperation => SecurityOperations.OivGeks.Edit;

        protected override string SDeleteOperation => SecurityOperations.OivGeks.Delete;
    }
}
