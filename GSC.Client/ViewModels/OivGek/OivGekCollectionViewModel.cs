﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.OivGek
{
    public class OivGekCollectionViewModel : ProjectEntityCollectionViewModel<Model.OivGek>
    {
        /// <summary>
        /// Creates a new instance of OivGekCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static OivGekCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new OivGekCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the OivGekCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the OivGekCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected OivGekCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.OivGeks,
                rq => rq.Include(_ => _.Region))
        {
        }

        protected override string SNewOperation => SecurityOperations.OivGeks.New;

        protected override string SEditOperation => SecurityOperations.OivGeks.Edit;

        protected override string SDeleteOperation => SecurityOperations.OivGeks.Delete;

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.OivGeks.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.OivGeks.ExportToExcel);
        }

        public override string EntityName
        {
            get { return "ОИВ/ГЭК"; }
        }
    }
}
