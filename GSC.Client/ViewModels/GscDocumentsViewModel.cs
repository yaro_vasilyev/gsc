﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels
{
    public abstract class GscDocumentsViewModel : DocumentsViewModel<NavigationModuleDescription, IGscContextUnitOfWork>
    {

        protected GscDocumentsViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory)
        { }

        protected override NavigationModuleDescription[] CreateModules()
        {
            return new NavigationModuleDescription[]
                {};
        }

        public NavigationModuleDescription FindModule(string moduleTitle)
        {
            return Array.Find(Modules, d => d.ModuleTitle == moduleTitle);
        }

        protected Func<NavigationModuleDescription, object> GetPeekCollectionViewModelFactory<TEntity, TPrimaryKey>(
            Func<IGscContextUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection) 
            where TEntity : class
        {
            return module => PeekCollectionViewModel<NavigationModuleDescription, TEntity, TPrimaryKey, IGscContextUnitOfWork>.Create(
                module, 
                unitOfWorkFactory, 
                getRepositoryFunc,
                projection).SetParentViewModel(this);
        }
    }
}
