﻿namespace GSC.Client.ViewModels
{
    public interface ICollectionViewModelRefresh
    {
        void Refresh();
    }
}
