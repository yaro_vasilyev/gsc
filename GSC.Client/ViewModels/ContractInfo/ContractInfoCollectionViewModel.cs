﻿using System;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.ContractInfo
{
    public class ContractInfoCollectionViewModel 
        : ProjectEntityCollectionViewModelBase<Model.ContractInfo, string>
    {
        /// <summary>
        /// Creates a new instance of ContractInfoCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static ContractInfoCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ContractInfoCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ContractInfoCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ContractInfoCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ContractInfoCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.ContractInfos)
        {
        }

        [UsedImplicitly]
        public bool CanImportFromCsv()
        {
            return this.OperationAllowed(SecurityOperations.ContractInfos.ImportFromCsv);
        }

        [UsedImplicitly]
        public void ImportFromCsv()
        {
            this.EnsureOperationAllowed(SecurityOperations.ContractInfos.ImportFromCsv);
            var gscContext = ((DbUnitOfWork<Model.GscContext>) CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.ContractInfo, ContractInfoCsvMap, Model.ImportContractInfosResult>(
                info => gscContext.ImportRowContractInfo(sessionGuid, info.Number, info.ContractDate),
                () => gscContext.ImportContractInfos(sessionGuid),
                result => $"Номер {result.Number},\tдата {result.ContractDate?.ToString("dd.MM.yyyy")}",
                this);
        }

        [UsedImplicitly]
        private sealed class ContractInfoCsvMap : CsvClassMap<Model.ContractInfo>
        {
            public ContractInfoCsvMap()
            {
                Map(info => info.Number).Name("Номер договора");
                Map(info => info.ContractDate).Name("Дата заключения договора").TypeConverterOption("dd.MM.yyyy");
            }
        }

        protected override string SNewOperation => SecurityOperations.ContractInfos.New;

        protected override string SEditOperation => SecurityOperations.ContractInfos.Edit;

        protected override string SDeleteOperation => SecurityOperations.ContractInfos.Delete;

        public override string EntityName { get { return "Договор(ы)"; } }
    }
}
