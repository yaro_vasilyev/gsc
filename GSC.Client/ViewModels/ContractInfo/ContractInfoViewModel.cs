﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.ContractInfo
{
    public class ContractInfoViewModel : ProjectEntityViewModelBase<Model.ContractInfo, string>
    {
        /// <summary>
        /// Creates a new instance of ContractInfoViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static ContractInfoViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ContractInfoViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ContractInfoViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ContractInfoViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ContractInfoViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.ContractInfos,
                x => x.Number)
        {
        }

        protected override string LocalizedEntityName => "Договор";

        protected override string SNewOperation => SecurityOperations.ContractInfos.New;

        protected override string SEditOperation => SecurityOperations.ContractInfos.Edit;

        protected override string SDeleteOperation => SecurityOperations.ContractInfos.Delete;
    }
}
