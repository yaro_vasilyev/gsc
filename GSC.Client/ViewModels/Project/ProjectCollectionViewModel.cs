﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Model;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Project
{
    public class ProjectCollectionViewModel : CollectionViewModel<Model.Project, int, IGscContextUnitOfWork>
    {

        /// <summary>
        /// Creates a new instance of ProjectCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ProjectCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ProjectCollectionViewModel(unitOfWorkFactory));
        }


        public ProjectCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), uw => uw.Projects, null) {}

        public void CloseProject([NotNull] Model.Project project)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.Projects.Edit);

            if (project == null)
            {
                throw new ArgumentNullException(nameof(project));
            }

            if (project.Status == ProjectStatus.Closed)
                throw new InvalidOperationException("Проект закрыт");

            project.Status = ProjectStatus.Closed;
            Save(SelectedEntity);

            Refresh();
        }

        public bool CanCloseProject(Model.Project project)
        {
            return project != null &&
                   project.Status == ProjectStatus.Opened &&
                   SecurityService.OperationAllowed(SecurityOperations.Projects.Edit);
        }

        public void OpenProject([NotNull] Model.Project project)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.Projects.Edit);

            if (project == null)
            {
                throw new ArgumentNullException(nameof(project));
            }

            if (project.Status == ProjectStatus.Opened)
                throw new InvalidOperationException("Проект открыт");

            project.Status = ProjectStatus.Opened;
            Save(SelectedEntity);
            
            Refresh();
        }

        public bool CanOpenProject(Model.Project project)
        {
            return project != null &&
                   project.Status == ProjectStatus.Closed &&
                   SecurityService.OperationAllowed(SecurityOperations.Projects.Edit);
        }

        private static IGscContextUnitOfWork GetUnitOfWork(IReadOnlyRepository<Model.Project> repository)
        {
            return (IGscContextUnitOfWork) repository.UnitOfWork;
        }


        public override void New()
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.Projects.New);

            if (Entities.Any(p => p.Status == ProjectStatus.Opened))
            {
                throw new InvalidOperationException("Необходимо закрыть открытые проекты");
            }

            base.New();
        }

        public bool CanNew()
        {
            return Entities.All(p => p.Status == ProjectStatus.Closed) &&
                   SecurityService.OperationAllowed(SecurityOperations.Projects.New);
        }

        public override bool CanDelete(Model.Project projectionEntity)
        {
            return CanDeleteCore(projectionEntity) &&
                   projectionEntity.Status == ProjectStatus.Closed &&
                   SecurityService.OperationAllowed(SecurityOperations.Projects.Delete);
        }

        public override void Delete(Model.Project projectionEntity) { DeleteBase(projectionEntity); }

        public override string EntityName => "Проект";

        protected override void OnSelectedEntityChanged()
        {
            base.OnSelectedEntityChanged();
            UpdateCommands();
        }

        public virtual void UpdateCommands()
        {
            this.RaiseCanExecuteChanged(x => x.New());
            this.RaiseCanExecuteChanged(x => x.OpenProject(null));
            this.RaiseCanExecuteChanged(x => x.CloseProject(null));
            this.RaiseCanExecuteChanged(x => x.Delete(null));
        }

        public override void Refresh()
        {
            base.Refresh();
            UpdateCommands();
        }
    }
}
