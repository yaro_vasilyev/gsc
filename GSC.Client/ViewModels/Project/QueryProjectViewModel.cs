﻿using System;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels.Project
{
    public class QueryProjectViewModel 
        : CollectionViewModel<Model.Project, int, IGscContextUnitOfWork>
    {
        public QueryProjectViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> uwFactory = null)
            : base(uwFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Projects)
        {
        }
    }
}
