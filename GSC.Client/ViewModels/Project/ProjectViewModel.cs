﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Model;
using MessageButton = DevExpress.Mvvm.MessageButton;
using MessageIcon = DevExpress.Mvvm.MessageIcon;

namespace GSC.Client.ViewModels.Project
{
    public class ProjectViewModel : SingleObjectViewModel<Model.Project, int, IGscContextUnitOfWork>
    {
        public ProjectViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                uw => uw.Projects)
        {
        }

        public IEntitiesViewModel<Model.Project> LookupProjects
        {
            get { return GetLookUpEntitiesViewModel((ProjectViewModel vm) => vm.LookupProjects, uw => uw.Projects); }
        }

        #region ProjectId

        private int? projectId;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnProjectIdChanged))]
        public virtual int? ProjectId
        {
            get { return projectId; }
            set
            {
                if (projectId != value)
                {
                    projectId = value;
                    this.RaisePropertyChanged(x => x.ProjectId);
                }
            }
        }

        protected virtual void OnProjectIdChanged()
        {
        }

        #endregion

        protected override string GetTitle()
        {
            return "Проект";
        }

        protected override string GetTitleForNewEntity()
        {
            return "Новый Проект";
        }


        private bool copyData = false;

        public virtual bool CopyData
        {
            get { return copyData; }
            set
            {
                if (copyData != value)
                {
                    copyData = value;
                    this.RaisePropertyChanged(x => x.CopyData);
                }
            }
        }

        protected virtual void OnCopyDataChanged()
        {

        }

        protected override Model.Project CreateEntity()
        {
            var e = base.CreateEntity();
            e.Status = ProjectStatus.Opened;
            e.OpenDate = DateTime.Now;
            return e;
        }

        protected override void OnEntitySaved(int primaryKey, Model.Project entity, bool isNewEntity)
        {
            base.OnEntitySaved(primaryKey, entity, isNewEntity);
            if (isNewEntity && CopyData && ProjectId != null)
            {
                UnitOfWork.Context.CopyProjectData(ProjectId.Value, entity.Id);
            }
        }
    }
}
