﻿using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Model;
using GSC.Client.Common.ViewModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the single AcademicTitle object view model.
    /// </summary>
    public class AcademicTitleViewModel : SingleObjectViewModel<AcademicTitle, int, IGscContextUnitOfWork>
    {

        /// <summary>
        /// Creates a new instance of AcademicTitleViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static AcademicTitleViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AcademicTitleViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicTitleViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AcademicTitleViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected AcademicTitleViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AcademicTitles, x => x.Code)
        {
        }

        protected override string LocalizedEntityName => "Ученое звание";

        protected string SDeleteOperation => SecurityOperations.AcademicTitles.Delete;

        protected string SNewOperation => SecurityOperations.AcademicTitles.New;

        protected string SEditOperation => SecurityOperations.AcademicTitles.Edit;

        public override bool CanDelete()
        {
            return base.CanDelete() && SecurityService.OperationAllowed(SDeleteOperation);
        }

        public override void Delete()
        {
            SecurityService.EnsureOperationAllowed(SDeleteOperation);
            base.Delete();
        }

        public override bool CanSave()
        {
            return base.CanSave() &&
                SecurityService.OperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
        }

        public override void Save()
        {
            SecurityService.EnsureOperationAllowed(IsNewMode ? SNewOperation : SEditOperation);
            base.Save();
        }

        protected override bool CanCreateNewEntity()
        {
            return base.CanCreateNewEntity() &&
                SecurityService.OperationAllowed(SNewOperation);
        }

        protected override Model.AcademicTitle CreateEntity()
        {
            SecurityService.EnsureOperationAllowed(SNewOperation);
            return base.CreateEntity();
        }

    }
}
