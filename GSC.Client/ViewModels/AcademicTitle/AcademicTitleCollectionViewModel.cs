﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using GSC.Client.Common.ViewModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the AcademicTitles collection view model.
    /// </summary>
    public class AcademicTitleCollectionViewModel 
        : CollectionViewModel<Model.AcademicTitle, int, IGscContextUnitOfWork>
        , ISecureViewModel
    {

        /// <summary>
        /// Creates a new instance of AcademicTitleCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static AcademicTitleCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new AcademicTitleCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicTitleCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AcademicTitleCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected AcademicTitleCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.AcademicTitles)
        {
        }

        [UsedImplicitly]
        public void ImportDictionary()
        {
            this.EnsureOperationAllowed(SecurityOperations.AcademicTitles.ImportFromCsv);
            var gscContext = ((DbUnitOfWork<Model.GscContext>) CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.AcademicTitle, AcademicTitleCsvMap, Model.ImportAcademicTitlesResult>(
                record => { gscContext.ImportRowAcademicTitle(sessionGuid, record.Code, record.Name); },
                () => gscContext.ImportAcademicTitles(sessionGuid),
                result => $"Код {result.Code},\t\"{result.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class AcademicTitleCsvMap : CsvClassMap<Model.AcademicTitle>
        {
            public AcademicTitleCsvMap()
            {
                Map(degree => degree.Code).Name("Код").TypeConverterOption(NumberStyles.Integer);
                Map(degree => degree.Name).Name("Ученое звание");
            }
        }

        [UsedImplicitly]
        public bool CanImportDictionary()
        {
            return this.OperationAllowed(SecurityOperations.AcademicTitles.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.AcademicTitles.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.AcademicTitles.ExportToExcel);
        }

        #region Operations checks

        public bool CanNew()
        {
            return SecurityService.OperationAllowed(SecurityOperations.AcademicTitles.New);
        }

        public override void New()
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicTitles.New);
            base.New();
        }

        public override bool CanEdit(Model.AcademicTitle projectionEntity)
        {
            return base.CanEdit(projectionEntity) &&
                   SecurityService.OperationAllowed(SecurityOperations.AcademicTitles.Edit);
        }

        public override void Edit(Model.AcademicTitle projectionEntity)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicTitles.Edit);
            base.Edit(projectionEntity);
        }

        public override bool CanDelete(Model.AcademicTitle projectionEntity)
        {
            return base.CanDelete(projectionEntity) &&
                   SecurityService.OperationAllowed(SecurityOperations.AcademicTitles.Delete);
        }

        public override void Delete(Model.AcademicTitle projectionEntity)
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.AcademicTitles.Delete);
            base.Delete(projectionEntity);
        }

        #endregion

        public override string EntityName => "Ученое звание";
    }
}