﻿using System.Linq;
using GSC.Client.Common.DataModel;

namespace GSC.Client.ViewModels
{
    internal class ExecInfo
    {
        public static IQueryable<Model.Exec> ExecEgeCollectionProjection(IRepositoryQuery<Model.Exec> query)
        {
            return query.Include(x => x.Exam)
                .Include(x => x.Exam.Subject)
                .Include(x => x.Station)
                .Include(x => x.Station.Region)
                .Where(x => x.Exam.ExamType == Model.ExamType.Ege);
        }

        public static IQueryable<Model.Exec> ExecGveCollectionProjection(IRepositoryQuery<Model.Exec> query)
        {
            return query.Include(x => x.Exam)
                .Include(x => x.Exam.Subject)
                .Include(x => x.Station)
                .Include(x => x.Station.Region)
                .Where(x => x.Exam.ExamType == Model.ExamType.Gve);
        }

        public static IQueryable<Model.Exec> ExecOgeCollectionProjection(IRepositoryQuery<Model.Exec> query)
        {
            return query.Include(x => x.Exam)
                .Include(x => x.Exam.Subject)
                .Include(x => x.Station)
                .Include(x => x.Station.Region)
                .Where(x => x.Exam.ExamType == Model.ExamType.Oge);
        }
    }
}
