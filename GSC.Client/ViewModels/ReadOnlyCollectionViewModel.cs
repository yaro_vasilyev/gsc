﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Security;
using GSC.Client.Util;

// ReSharper disable once CheckNamespace
namespace GSC.Client.Common.ViewModel
{
    public abstract partial class ReadOnlyCollectionViewModelBase<TEntity, TProjection, TUnitOfWork>
        where TEntity : class
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {
        #region Selection

        public virtual IEnumerable<TProjection> SelectedEntities { get; set; }

        protected virtual void OnSelectedEntitiesChanged() { }

        #endregion
    }

    public partial class ReadOnlyCollectionViewModel<TEntity, TProjection, TUnitOfWork>
        where TEntity : class
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {
        protected virtual IOpenFileDialogService OpenFileDialogService
        {
            get
            {
                throw new NotImplementedException(); // заглушка, перекрываемая DevExpress MVVM, т.к. в главной форме регистрируется сервис
            }
        }

        protected IMessageBoxService MessageBoxService => this.GetRequiredService<IMessageBoxService>();


        #region SecurityService

        private ISecurityService securityService;

        protected ISecurityService SecurityService
        {
            get { return securityService ?? (securityService = this.GetRequiredService<ISecurityService>()); }
        }

        #endregion

        #region ExportToExcelService

        protected virtual IExportToExcelService ExportToExcelService
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        protected string ShowFileOpenDialog(string title, string filter)
        {
            Debug.Assert(OpenFileDialogService != null, "OpenFileDialogService != null");

            OpenFileDialogService.Title = title;
            OpenFileDialogService.Filter = filter;
            return OpenFileDialogService.ShowDialog() ? OpenFileDialogService.GetFullFileName() : null;
        }

        protected override void OnEntitiesLoaded(TUnitOfWork unitOfWork, IEnumerable<TProjection> entitiesEnumerable)
        {
            // NOTE: possible problems with enumeration. В детейлах это ObservableCollection, 
            // a из CollectionViewModel в SingleObjectViewModel текущий объект передается как TProjection[1]{entity}

            base.OnEntitiesLoaded(unitOfWork, entitiesEnumerable);
            entitiesLoadedCallback?.Invoke(unitOfWork, entitiesEnumerable);
        }

        protected override void OnEntitiesAssigned(Func<TProjection> getSelectedEntityCallback)
        {
            base.OnEntitiesAssigned(getSelectedEntityCallback);
            entitiesAssignedCallback?.Invoke(getSelectedEntityCallback);
        }

        private Action<TUnitOfWork, IEnumerable<TProjection>> entitiesLoadedCallback;
        private Action<Func<TProjection>> entitiesAssignedCallback;


        public void SetEntitiesLoadingCallbacks(Action<TUnitOfWork, IEnumerable<TProjection>> asyncLoadedCallback,
                                                Action<Func<TProjection>> assignedCallback)
        {
            this.entitiesLoadedCallback = asyncLoadedCallback;
            this.entitiesAssignedCallback = assignedCallback;
        }

    }
}
