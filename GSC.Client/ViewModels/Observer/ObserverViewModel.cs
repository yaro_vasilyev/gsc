﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;

namespace GSC.Client.ViewModels.Observer
{
    public class ObserverViewModel : ProjectEntityViewModel<Model.Observer>
    {
        /// <summary>
        /// Creates a new instance of ObserverViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ObserverViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ObserverViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ObserverViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ObserverViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ObserverViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Observers, x => x.Fio)
        {
        }

        protected override string LocalizedEntityName => this.GetParentViewModel<ObserverCollectionViewModel>().EntityName;

        protected override string SNewOperation => this.GetParentViewModel<ObserverCollectionViewModel>().SPublicNewOperation;

        protected override string SEditOperation => this.GetParentViewModel<ObserverCollectionViewModel>().SPublicEditOperation;

        protected override string SDeleteOperation => this.GetParentViewModel<ObserverCollectionViewModel>().SPublicDeleteOperation;
    }
}
