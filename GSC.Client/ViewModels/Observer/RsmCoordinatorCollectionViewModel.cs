﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Observer
{
    public class RsmCoordinatorCollectionViewModel : ObserverCollectionViewModel
    {

        /// <summary>
        /// Creates a new instance of RsmCoordinatorCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static RsmCoordinatorCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new RsmCoordinatorCollectionViewModel(unitOfWorkFactory));
        }

        private static void InitializeNew(Model.Observer observer) { observer.Type = Model.ObserverType.Rsm; }

        /// <summary>
        /// Initializes a new instance of the RsmCoordinatorCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the RsmCoordinatorCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected RsmCoordinatorCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), 
                  x => x.Observers, 
                  rq => rq.Where(x => x.Type == Model.ObserverType.Rsm),
                  InitializeNew)
        {
        }

        protected override string SExportToExcelOperation => SecurityOperations.RsmCoordinators.ExportToExcel;

        protected override string SNewOperation => SecurityOperations.RsmCoordinators.New;

        protected override string SEditOperation => SecurityOperations.RsmCoordinators.Edit;

        protected override string SDeleteOperation => SecurityOperations.RsmCoordinators.Delete;

        public override string EntityName => "Координатор";
    }
}
