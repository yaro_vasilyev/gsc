﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Observer
{
    public class ManagerCollectionViewModel : ObserverCollectionViewModel
    {

        /// <summary>
        /// Creates a new instance of ManagerCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ManagerCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ManagerCollectionViewModel(unitOfWorkFactory));
        }

        private static void InitializeNew(Model.Observer observer) { observer.Type = Model.ObserverType.Manager; }

        /// <summary>
        /// Initializes a new instance of the ManagerCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ManagerCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ManagerCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), 
                  x => x.Observers, 
                  rq => rq.Where(x => x.Type == Model.ObserverType.Manager),
                  InitializeNew)
        {
        }

        protected override string SExportToExcelOperation => SecurityOperations.Managers.ExportToExcel;

        protected override string SNewOperation => SecurityOperations.Managers.New;

        protected override string SEditOperation => SecurityOperations.Managers.Edit;

        protected override string SDeleteOperation => SecurityOperations.Managers.Delete;

        public override string EntityName => "Менеджер";
    }
}
