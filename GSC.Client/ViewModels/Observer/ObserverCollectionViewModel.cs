﻿using System;
using System.Linq;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.Observer
{
    public abstract class ObserverCollectionViewModel : ProjectEntityCollectionViewModel<Model.Observer>
    {
        protected ObserverCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
                                              Func<IGscContextUnitOfWork, IRepository<Model.Observer, int>> getRepositoryFunc,
                                              Func<IRepositoryQuery<Model.Observer>, IQueryable<Model.Observer>> projection = null,
                                              Action<Model.Observer> newEntityInitializer = null,
                                              Func<bool> canCreateNewEntity = null,
                                              bool ignoreSelectEntityMessage = false)
            : base(unitOfWorkFactory, getRepositoryFunc, projection,
                newEntityInitializer, canCreateNewEntity, ignoreSelectEntityMessage)
        {
            FilterExpression = o => o.ProjectId == CurrentProject.Id;
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SExportToExcelOperation);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SExportToExcelOperation);
        }

        protected abstract string SExportToExcelOperation { get; }

        /// <summary>
        /// Только для обращения из дочерней VM.
        /// </summary>
        internal string SPublicNewOperation => SNewOperation;

        /// <summary>
        /// Только для обращения из дочерней VM.
        /// </summary>
        internal string SPublicEditOperation => SEditOperation;

        /// <summary>
        /// Только для обращения из дочерней VM.
        /// </summary>
        internal string SPublicDeleteOperation => SDeleteOperation;

    }
}
