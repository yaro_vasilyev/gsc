﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Security;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TPrimaryKey"></typeparam>
    /// <typeparam name="TUnitOfWork"></typeparam>
    /// <remarks><see cref="https://www.devexpress.com/Support/Center/Question/Details/T303979"/></remarks>
    public partial class InstantFeedbackCollectionViewModel<TEntity, TPrimaryKey, TUnitOfWork>
        : InstantFeedbackCollectionViewModelBase<TEntity, TEntity, TPrimaryKey, TUnitOfWork>
        where TEntity : class, new()
        where TUnitOfWork : IUnitOfWork
    {

        public static InstantFeedbackCollectionViewModel<TEntity, TPrimaryKey, TUnitOfWork>
            CreateInstantFeedbackCollectionViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection = null,
            Func<bool> canCreateNewEntity = null)
        {
            return
                ViewModelSource.Create(
                    () =>
                        new InstantFeedbackCollectionViewModel<TEntity, TPrimaryKey, TUnitOfWork>(unitOfWorkFactory,
                                                                                                  getRepositoryFunc,
                                                                                                  projection,
                                                                                                  canCreateNewEntity));
        }

        protected InstantFeedbackCollectionViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection = null,
            Func<bool> canCreateNewEntity = null)
            : base(unitOfWorkFactory, getRepositoryFunc, projection, canCreateNewEntity) {}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TProjection"></typeparam>
    /// <typeparam name="TPrimaryKey"></typeparam>
    /// <typeparam name="TUnitOfWork"></typeparam>
    /// <remarks><see cref="https://www.devexpress.com/Support/Center/Question/Details/T303979"/></remarks>
    public partial class InstantFeedbackCollectionViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
        : InstantFeedbackCollectionViewModelBase<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
        where TEntity : class, new()
        where TProjection : class
        where TUnitOfWork : IUnitOfWork
    {

        public static InstantFeedbackCollectionViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
            CreateInstantFeedbackCollectionViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection,
            Func<bool> canCreateNewEntity = null)
        {
            return
                ViewModelSource.Create(
                    () =>
                        new InstantFeedbackCollectionViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>(
                        unitOfWorkFactory,
                        getRepositoryFunc,
                        projection,
                        canCreateNewEntity));
        }

        protected InstantFeedbackCollectionViewModel(
            IUnitOfWorkFactory<TUnitOfWork> unitOfWorkFactory,
            Func<TUnitOfWork, IRepository<TEntity, TPrimaryKey>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TProjection>> projection,
            Func<bool> canCreateNewEntity = null)
            : base(unitOfWorkFactory, getRepositoryFunc, projection, canCreateNewEntity) {}
    }
}
