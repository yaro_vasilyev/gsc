﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services;
using GSC.Model;

namespace GSC.Client.ViewModels
{
    public abstract class ProjectInstantFeedbackCollectionViewModel<TEntity>
        : InstantFeedbackCollectionViewModel<TEntity, int, IGscContextUnitOfWork>
        , ICollectionViewModelRefresh
        , ISupportReadOnlyViewModel
        , ISecureViewModel
        where TEntity : class, new()
    {
        protected ProjectInstantFeedbackCollectionViewModel(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
            Func<IGscContextUnitOfWork, IRepository<TEntity, int>> getRepositoryFunc,
            Func<IRepositoryQuery<TEntity>, IQueryable<TEntity>> projection = null,
            Func<bool> canCreateNewEntity = null)
            : base(unitOfWorkFactory, getRepositoryFunc, projection, canCreateNewEntity) { }

        protected virtual IProjectService ProjectService
        {
            get { throw new NotImplementedException("Should be implemented by POCOViewModel."); }
        }

        protected Model.Project CurrentProject
        {
            get
            {
                return ProjectService.GetCurrentProject().Match(x => x,
                    e =>
                    {
                        throw e; // TODO: Wrap?
                    });
            }
        }

        protected virtual string SNewOperation => "__reserved_new";

        protected virtual string SEditOperation => "__reserved_edit";

        protected virtual string SDeleteOperation => "__reserved_delete";

        public virtual bool CanNew()
        {
            return !ReadOnly && this.OperationAllowed(SNewOperation);
        }

        public override void New()
        {
            this.EnsureOperationAllowed(SNewOperation);
            base.New();
        }

        public override bool CanEdit(object threadSafeProxy)
        {
            return base.CanEdit(threadSafeProxy) &&
                   !ReadOnly &&
                   this.OperationAllowed(SEditOperation);
        }

        public override void Edit(object threadSafeProxy)
        {
            this.EnsureOperationAllowed(SEditOperation);
            base.Edit(threadSafeProxy);
        }

        public override bool CanDelete(object threadSafeProxy)
        {
            return base.CanDelete(threadSafeProxy) &&
                   !ReadOnly &&
                   this.OperationAllowed(SDeleteOperation);
        }

        public override void Delete(object threadSafeProxy)
        {
            this.EnsureOperationAllowed(SDeleteOperation);
            base.Delete(threadSafeProxy);
        }

        public override void Refresh()
        {
            UpdateCommands();
            base.Refresh();
        }

//        protected override void OnIsLoadingChanged()
//        {
//            base.OnIsLoadingChanged();
//            UpdateCommands();
//        }

        protected virtual void UpdateCommands()
        {
            this.RaiseCanExecuteChanged(x => x.New());
            this.RaiseCanExecuteChanged(x => x.Edit(null));
            this.RaiseCanExecuteChanged(x => x.Delete(null));
            this.RaiseCanExecuteChanged(x => x.View(null));
        }

        public virtual void View(object threadSafeProxy)
        {
            base.Edit(threadSafeProxy);
        }

        public virtual bool CanView(object threadSafeProxy)
        {
            return threadSafeProxy != null /*&& !IsLoading*/;
        }

        protected virtual void OnSelectedEntityChanged()
        {
            UpdateCommands();
        }

        public virtual bool ReadOnly => CurrentProject.Status != ProjectStatus.Opened;

        public virtual string EntityName => typeof (TEntity).Name;
    }
}
