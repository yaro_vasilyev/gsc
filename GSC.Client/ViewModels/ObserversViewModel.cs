﻿using System.Collections.Generic;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    public class ObserversViewModel : GscDocumentsViewModel
    {
        public const string ModuleManagers = "Менеджеры";
        public const string ModuleRsmCoordinators = "Координаторы";

        public ObserversViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory())
        { }

        protected override NavigationModuleDescription[] CreateModules()
        {
            var modules = new List<NavigationModuleDescription>();
            var securityService = this.GetRequiredService<ISecurityService>();

            if (securityService.OperationAllowed(SecurityOperations.Managers.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleManagers,
                    "ManagerCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Observers)));
            }

            if (securityService.OperationAllowed(SecurityOperations.RsmCoordinators.LoadModule))
            {
                modules.Add(new NavigationModuleDescription(ModuleRsmCoordinators,
                    "RsmCoordinatorCollectionView",
                    GetPeekCollectionViewModelFactory(x => x.Observers)));
            }
            return modules.ToArray();
        }
    }
}
