﻿using System;
using GSC.Client.Common.ViewModel;

namespace GSC.Client.ViewModels
{
    public class NavigationModuleDescription : ModuleDescription<NavigationModuleDescription>
    {
        public NavigationModuleDescription(string title, string documentType,
                                           Func<NavigationModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, string.Empty, peekCollectionViewModelFactory)
        {
        }
    }
}