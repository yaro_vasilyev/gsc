﻿using System;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Admin.Logins;
using GSC.Client.Modules.Admin.PrivateCabinetAccess;
using GSC.Client.Modules.Admin.Project;
using GSC.Client.Modules.Contracts;
using GSC.Client.Modules.Database.ContractInfos;
using GSC.Client.Modules.Database.Experts;
using GSC.Client.Modules.Database.Objects;
using GSC.Client.Modules.Database.Observers;
//using GSC.Client.Modules.Database.Stations;
using GSC.Client.Modules.Database.Violations;
using GSC.Client.Modules.Database.AcademicDegrees;
using GSC.Client.Modules.Database.AcademicTitles;
using GSC.Client.Modules.Database.Subjects;
using GSC.Client.Modules.MonitorResult;
using GSC.Client.Modules.Schedule;
using GSC.Client.Modules.CallCenter;
using GSC.Client.Modules.Reports.CallCenter;
using GSC.Client.Modules.Reports.Contracts;
using GSC.Client.Modules.Reports.Database;
using GSC.Client.Modules.Reports.MonitorResult;
using GSC.Client.Modules.Reports.Schedule;
using GSC.Client.Security;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the root POCO view model for the GscContext data model.
    /// </summary>
    public partial class GscContextViewModel : DocumentsViewModel<GscContextModuleDescription, IGscContextUnitOfWork>
    {
        const string TablesGroup = "Tables";

        const string ViewsGroup = "Views";

        public const string ModuleExperts = "Experts";
        public const string ModuleObservers = "Observers";
        public const string ModuleAcademicTitles = "Academic Titles";
        public const string ModuleAcademicDegrees = "Academic Degrees";
        public const string ModuleObjects = "Objects";
        public const string ModuleSubjects = "Subjects";
        public const string ModuleViolations = "Violations";
        public const string ModuleContractInfos = "ContractInfos";
        public const string ModuleSchedules = "Schedules";
        public const string ModuleMonitorResult = "MonitorResult";
        public const string ModuleMonitorReport = "MonitorReport";
        public const string ModuleCallCenter = "CallCenter";
        public const string ModuleContracts = "Contracts";
        public const string ModuleDatabaseReports = "DatabaseReports";
        public const string ModuleScheduleReports = "ScheduleReports";
        public const string ModuleContractReports = "ContractReports";
        public const string ModuleMonitorResultReports = "MonitorResultReports";
        public const string ModulePrivateCabinetAccess = "PrivateCabinetAccess";
        public const string ModuleCallCenterReports = "CallCenterReports";
        public const string ModuleProjects = "Projects";
        public const string ModuleAdminUsers = "AdminUsers";

        /// <summary>
        /// Creates a new instance of GscContextViewModel as a POCO view model.
        /// </summary>
        //        public static GscContextViewModel Create()
        //        {
        //            return ViewModelSource.Create(() => new GscContextViewModel());
        //        }

        /// <summary>
        /// Initializes a new instance of the GscContextViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the GscContextViewModel type without the POCO proxy factory.
        /// </summary>
        protected GscContextViewModel()
            : base(UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        protected override GscContextModuleDescription[] CreateModules()
        {
            // TODO: Check module availability here.

            return new[]
            {
                new GscContextModuleDescription(ModuleAcademicDegrees,
                    nameof(AcademicDegreeCollectionView),
                    TablesGroup,
                    SecurityOperations.AcademicDegrees.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.AcademicDegrees)),
                new GscContextModuleDescription(ModuleAcademicTitles,
                    nameof(AcademicTitleCollectionView),
                    TablesGroup,
                    SecurityOperations.AcademicTitles.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.AcademicTitles)),
                new GscContextModuleDescription(ModuleContractInfos,
                    nameof(ContractInfoCollectionView),
                    TablesGroup,
                    SecurityOperations.ContractInfos.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.ContractInfos)),
                new GscContextModuleDescription(ModuleExperts,
                    nameof(ExpertCollectionView),
                    TablesGroup,
                    SecurityOperations.Experts.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Experts)),
#warning Загрузка модуля проверяется по загрузке модуля Экспертов (наименьшие права)
                new GscContextModuleDescription(ModuleObservers,
                    nameof(ObserversView),
                    ViewsGroup,
                    SecurityOperations.Experts.LoadModule),
#warning Загрузка модуля проверяется по загрузке модуля Экспертов (наименьшие права)
                new GscContextModuleDescription(ModuleViolations,
                    nameof(ViolationsView),
                    ViewsGroup,
                    SecurityOperations.Experts.LoadModule),
#warning Загрузка модуля проверяется по загрузке модуля Экспертов (наименьшие права)
                new GscContextModuleDescription(ModuleObjects,
                    nameof(ObjectsView),
                    ViewsGroup,
                    SecurityOperations.Experts.LoadModule),
                new GscContextModuleDescription(ModuleSubjects,
                    nameof(SubjectCollectionView),
                    TablesGroup,
                    SecurityOperations.Subjects.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Subjects)),
                new GscContextModuleDescription(ModuleSchedules,
                    nameof(ScheduleCollectionView),
                    TablesGroup,
                    SecurityOperations.Schedules.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Schedules)),
                new GscContextModuleDescription(ModuleMonitorResult,
                    nameof(MonitorResultCollectionView),
                    TablesGroup,
                    SecurityOperations.MonitorResults.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.MonitorResults)),
                new GscContextModuleDescription(ModuleMonitorReport,
                    nameof(MonitorReportCollectionView),
                    TablesGroup,
                    SecurityOperations.MonitorReports.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Schedules)),
                new GscContextModuleDescription(ModuleCallCenter,
                    nameof(EventCollectionView),
                    TablesGroup,
                    SecurityOperations.CallCenter.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Events)),
                new GscContextModuleDescription(ModuleContracts,
                    nameof(ContractCollectionView),
                    TablesGroup,
                    SecurityOperations.Contracts.LoadModule,
                    GetPeekCollectionViewModelFactory(x => x.Contracts)),
                new GscContextModuleDescription(ModuleDatabaseReports,
                    nameof(DatabaseReportsView),
                    ViewsGroup,
                    SecurityOperations.ReportsDatabase.LoadModule),
                new GscContextModuleDescription(ModuleScheduleReports,
                    nameof(ScheduleReportsView),
                    ViewsGroup,
                    SecurityOperations.ReportsSchedule.LoadModule),
                new GscContextModuleDescription(ModuleMonitorResultReports,
                    nameof(MonitorResultReportsView),
                    ViewsGroup,
                    SecurityOperations.ReportsMonitorResult.LoadModule),
                new GscContextModuleDescription(ModuleContractReports,
                    nameof(ContractReportsView),
                    ViewsGroup,
                    SecurityOperations.ReportsContracts.LoadModule), 
                new GscContextModuleDescription(ModulePrivateCabinetAccess,
                    nameof(PrivateCabinetAccessView),
                    ViewsGroup,
                    SecurityOperations.PrivateCabinetAccess.LoadModule),
                new GscContextModuleDescription(ModuleCallCenterReports,
                    nameof(CallCenterReportsView),
                    ViewsGroup,
                    SecurityOperations.ReportsCallCenter.LoadModule),
                new GscContextModuleDescription(ModuleAdminUsers,
                    nameof(LoginCollectionView),
                    ViewsGroup,
                    SecurityOperations.AdminUsers.LoadModule),
                new GscContextModuleDescription(ModuleProjects,
                    nameof(ProjectCollectionView),
                    ViewsGroup,
                    SecurityOperations.Projects.LoadModule)
            };
        }

        public GscContextModuleDescription FindModule(string moduleTitle)
        {
            return Array.Find(Modules, d => d.ModuleTitle == moduleTitle);
        }

        private GscContextModuleDescription FindFirstAvailableModule(ISecurityService securityService)
        {
            var module = Array.Find(Modules,
                d => securityService.CanLoadModule(d.ModuleLoadSecurityOperation));
            return module;
        }

        public override void OnLoaded(GscContextModuleDescription module)
        {
            var mod = module;
            var securityService = this.GetRequiredService<ISecurityService>();
            if (!securityService.CanLoadModule(mod.ModuleLoadSecurityOperation))
            {
                mod = FindFirstAvailableModule(securityService);
            }
            base.OnLoaded(mod);
        }

        public override GscContextModuleDescription DefaultModule => FindModule(ModuleExperts);
    }

    public class GscContextModuleDescription : ModuleDescription<GscContextModuleDescription>
    {
        public GscContextModuleDescription(string title,
            string documentType,
            string group,
            Func<GscContextModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, group, peekCollectionViewModelFactory)
        {
        }

        public GscContextModuleDescription(string title,
                                           string documentType,
                                           string group,
                                           [NotNull] string loadModuleOperation,
                                           Func<GscContextModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, group, peekCollectionViewModelFactory)
        {
            if (string.IsNullOrEmpty(loadModuleOperation))
            {
                throw new ArgumentNullException(nameof(loadModuleOperation));
            }
            ModuleLoadSecurityOperation = loadModuleOperation;
        }

        public string ModuleLoadSecurityOperation { get; set; } = "";
    }
}