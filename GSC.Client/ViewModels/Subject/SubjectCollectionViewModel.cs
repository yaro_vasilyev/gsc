﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.DataModel.EntityFramework;
using JetBrains.Annotations;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the AcademicDegrees collection view model.
    /// </summary>
    public class SubjectCollectionViewModel : ProjectEntityCollectionViewModel<Model.Subject>
    {
        /// <summary>
        /// Creates a new instance of SubjectCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SubjectCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SubjectCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicDegreeCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SubjectCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Subjects)
        {
        }

        [UsedImplicitly]
        public void ImportDictionary()
        {
            this.EnsureOperationAllowed(SecurityOperations.Subjects.ImportFromCsv);

            var gscContext = ((DbUnitOfWork<Model.GscContext>) CreateUnitOfWork()).Context;
            var sessionGuid = Guid.NewGuid();

            var importService = this.GetRequiredService<ICsvImportService>();

            importService.Import<Model.Subject, SubjectCsvMap, Model.ImportResult>(
                record => gscContext.ImportRowSubject(sessionGuid, record.Code, record.Name),
                () => gscContext.ImportSubjects(sessionGuid),
                result => $"Код {result.Code},\t\"{result.Name}\"",
                this);
        }

        [UsedImplicitly]
        private sealed class SubjectCsvMap : CsvClassMap<Model.Subject>
        {
            public SubjectCsvMap()
            {
                Map(subject => subject.Code).Name("SubjectCode").TypeConverterOption(NumberStyles.Integer);
                Map(subject => subject.Name).Name("SubjectName");
            }
        }

        [UsedImplicitly]
        public bool CanImportDictionary()
        {
            return this.OperationAllowed(SecurityOperations.Subjects.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Subjects.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Subjects.ExportToExcel);
        }

        public override string EntityName => "Предмет";

        protected override string SNewOperation => SecurityOperations.Subjects.New;

        protected override string SEditOperation => SecurityOperations.Subjects.Edit;

        protected override string SDeleteOperation => SecurityOperations.Subjects.Delete;
    }
}