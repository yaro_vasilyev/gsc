﻿using DevExpress.Mvvm.POCO;
using GSC.Client.GscContextDataModel;
using GSC.Client.Common.DataModel;
using GSC.Model;
using GSC.Client.Security;

namespace GSC.Client.ViewModels
{
    /// <summary>
    /// Represents the single SubjectViewModel object view model.
    /// </summary>
    public partial class SubjectViewModel : ProjectEntityViewModel<Subject>
    {
        /// <summary>
        /// Creates a new instance of SubjectViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SubjectViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new SubjectViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the AcademicDegreeViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the AccountViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SubjectViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Subjects, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "Предмет";

        protected override string SDeleteOperation => SecurityOperations.Subjects.Delete;

        protected override string SNewOperation => SecurityOperations.Subjects.New;

        protected override string SEditOperation => SecurityOperations.Subjects.Edit;
    }
}
