﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services.Import;
using GSC.Client.Util;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Station
{
    /// <summary>
    /// Represents the Stations collection view model.
    /// </summary>
    public class StationCollectionViewModel : ProjectInstantFeedbackCollectionViewModel<Model.Station>
    {
        /// <summary>
        /// Creates a new instance of StationCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static StationCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            Debug.Assert(unitOfWorkFactory == null);
            return ViewModelSource.Create(() => new StationCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the StationCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the StationCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected StationCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                   unitOfWork => unitOfWork.Stations,
                   query => query.Include(x => x.Region).Include(x => x.Execs))
        {
            FilterExpression = s => s.ProjectId == CurrentProject.Id;
        }

        [UsedImplicitly]
        public void ImportDictionary()
        {
            this.EnsureOperationAllowed(SecurityOperations.Stations.ImportFromCsv); //todo import from Xml?

            var unitOfWork = CreateUnitOfWork();
            var gscContext = unitOfWork.Context;
            var sessionGuid = Guid.NewGuid();

            Action<XmlStation> importRowAction = xmlStation =>
                gscContext.ImportRowStation(sessionGuid, xmlStation.Region, xmlStation.Code, xmlStation.Name,
                    xmlStation.Address, (int?) xmlStation.IsTom, (int?) xmlStation.TestType);

            Func<IEnumerable<Model.ImportStationResult>> importFunc = () => gscContext.ImportStation(sessionGuid, CurrentProject.Id);

            Func<Model.ImportStationResult, string> resultToStringFunc = result => $"Код {result.StationCode},\t\"{result.Name}\"";

            var importService = this.GetRequiredService<IXmlImportService>();
            importService.Import("Stations", importRowAction, importFunc, resultToStringFunc, this);
        }


        /// <summary>
        /// Example: 
        /// <code><![CDATA[<Station StationID="F2B1F33B-1B54-11E1-9EAE-003048CCECBE" REGION="35" StationCode="67" TestType="1" 
        /// StationName="СОШ №33 г. Вологды" StationAddress="160034, Вологодская обл., г. Вологда, ш. Окружное, д. 23а" IsTOM="0" />]]></code>
        /// </summary>
        [XmlType("Station")]
        public class XmlStation
        {
            [XmlAttribute("REGION")]
            public int Region { get; set; }

            [XmlAttribute("StationCode")]
            public int Code { get; set; }

            [XmlAttribute("TestType")]
            public Model.ExamType TestType { get; set; }

            [XmlAttribute("StationName")]
            public string Name { get; set; }

            [XmlAttribute("StationAddress")]
            public string Address { get; set; }

            [XmlAttribute("IsTom")]
            public Model.BooleanType IsTom { get; set; }
        }

/*
        private class ImpCsvStation : Model.Station
        {
            public int ImpRegion { get; set; }
        }
*/

/*
        private sealed class StationCsvMap : CsvClassMap<ImpCsvStation>
        {
            public StationCsvMap()
            {
                Map(station => station.ImpRegion).Name("REGION").TypeConverterOption(NumberStyles.Integer);
                Map(station => station.Code).Name("StationCode").TypeConverterOption(NumberStyles.Integer);
                Map(station => station.Name).Name("StationName");
                Map(station => station.Address).Name("StationAddress");
                Map(station => station.Tom).Name("IsTom").TypeConverterOption(NumberStyles.Integer);
                Map(station => station.ExamType).Name("TestType").TypeConverterOption(NumberStyles.Integer);
            }
        }
*/

        [UsedImplicitly]
        public bool CanImportDictionary()
        {
            return this.OperationAllowed(SecurityOperations.Stations.ImportFromCsv);
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Stations.ExportToExcel);
            var exportToExcelService = this.GetRequiredService<IExportToExcelService>();
            exportToExcelService.Export(control);
        }

        [UsedImplicitly]
        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Stations.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.Stations.New;

        protected override string SEditOperation => SecurityOperations.Stations.Edit;

        protected override string SDeleteOperation => SecurityOperations.Stations.Delete;

        public override string EntityName => "ППЭ";
    }
}