﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;

namespace GSC.Client.ViewModels.Station
{

    /// <summary>
    /// Represents the single Station object view model.
    /// </summary>
    public class StationViewModel : ProjectEntityViewModel<Model.Station>
    {

        /// <summary>
        /// Creates a new instance of StationViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static StationViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new StationViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the StationViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the StationViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected StationViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Stations, x => x.Name)
        {
        }

        protected override string LocalizedEntityName => "ППЭ";

        public IEntitiesViewModel<Model.Region> LookupRegions
        {
            get { return GetLookUpProjectEntitiesViewModel((StationViewModel vm) => vm.LookupRegions, uw => uw.Regions); }
        }


        public virtual bool IsTom
        {
            get { return Entity.Tom == Model.BooleanType.Yes; }
            set
            {
                Entity.Tom = value ? Model.BooleanType.Yes : Model.BooleanType.No;
                this.RaisePropertyChanged(x => x.IsTom);
                Update();
            }
        }

        protected override string SNewOperation => SecurityOperations.Stations.New;

        protected override string SEditOperation => SecurityOperations.Stations.Edit;

        protected override string SDeleteOperation => SecurityOperations.Stations.Delete;
    }
}
