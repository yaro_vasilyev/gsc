﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.ViewModels;
using GSC.Model;

// ReSharper disable once CheckNamespace
namespace GSC.Client.Common.ViewModel
{
    public abstract partial class SingleObjectViewModelBase<TEntity, TPrimaryKey, TUnitOfWork>
        where TEntity : class
        where TUnitOfWork : IUnitOfWork
    {
        protected virtual void RefreshLookUpCollectionInternal(LambdaExpression propertyExpression, bool raisePropertyChanged)
        {
            var propertyName = Utils.ExpressionHelper.GetPropertyName(propertyExpression);

            lock (LookupLock)
            {
                IDocumentContent document;
                if (lookUpViewModels.TryGetValue(propertyName, out document))
                {
                    lookUpViewModels.Remove(propertyName);
                    if (document != null)
                    {
                        document.OnDestroy();
                        if (raisePropertyChanged)
                            ((IPOCOViewModel)this).RaisePropertyChanged(propertyName);
                    }
                }
            }
        }

        protected void RefreshLookUpCollection<TViewModel, TDetailEntity, TDetailPrimaryKey>(
            Expression<Func<TViewModel, CollectionViewModel<TDetailEntity, TDetailPrimaryKey, TUnitOfWork>>>
                propertyExpression,
            bool raisePropertyChanged) where TDetailEntity : class
        {
            RefreshLookUpCollectionInternal(propertyExpression, raisePropertyChanged);
        }

        protected void RefreshLookUpCollection<TViewModel, TDetailEntity, TDetailProjection, TDetailPrimaryKey>(
            Expression<Func<TViewModel, CollectionViewModel<TDetailEntity, TDetailProjection, TDetailPrimaryKey, TUnitOfWork>>> propertyExpression,
            bool raisePropertyChanged) where TDetailEntity : class where TDetailProjection : class
        {
            RefreshLookUpCollectionInternal(propertyExpression, raisePropertyChanged);
        }
        protected void RefreshLookUpCollection<TViewModel, TDetailEntity>(
            Expression<Func<TViewModel, ReadOnlyCollectionViewModel<TDetailEntity, TDetailEntity, TUnitOfWork>>> propertyExpression,
            bool raisePropertyChanged) where TDetailEntity : class
        {
            RefreshLookUpCollectionInternal(propertyExpression, raisePropertyChanged);
        }

        protected void RefreshLookUpCollection<TViewModel, TDetailEntity, TDetailProjection>(
            Expression<Func<TViewModel, ReadOnlyCollectionViewModel<TDetailEntity, TDetailProjection, TUnitOfWork>>> propertyExpression,
            bool raisePropertyChanged) where TDetailEntity : class where TDetailProjection : class
        {
            RefreshLookUpCollectionInternal(propertyExpression, raisePropertyChanged);
        }

        protected void RefreshLookUpCollection<TViewModel, TLookUpEntity>(
            Expression<Func<TViewModel, IEntitiesViewModel<TLookUpEntity>>> propertyExpression,
            bool raisePropertyChanged) where TLookUpEntity : class
        {
            RefreshLookUpCollectionInternal(propertyExpression, raisePropertyChanged);
        }

        protected IEntitiesViewModel<TLookUpEntity> GetLookUpProjectEntitiesViewModel<TViewModel, TLookUpEntity, TLookUpEntityKey>(
            Expression<Func<TViewModel, IEntitiesViewModel<TLookUpEntity>>> propertyExpression, 
            Func<TUnitOfWork, IRepository<TLookUpEntity, TLookUpEntityKey>> getRepositoryFunc, 
            Func<IRepositoryQuery<TLookUpEntity>, IQueryable<TLookUpEntity>> projection = null) 
            where TLookUpEntity : class, IProjectEntity
        {
            return GetLookUpProjectProjectionsViewModel(propertyExpression, getRepositoryFunc, projection);
        }

        protected virtual IEntitiesViewModel<TLookUpProjection> GetLookUpProjectProjectionsViewModel<TViewModel, TLookUpEntity, TLookUpProjection, TLookUpEntityKey>(
            Expression<Func<TViewModel, IEntitiesViewModel<TLookUpProjection>>> propertyExpression, 
            Func<TUnitOfWork, IRepository<TLookUpEntity, TLookUpEntityKey>> getRepositoryFunc, 
            Func<IRepositoryQuery<TLookUpEntity>, IQueryable<TLookUpProjection>> projection) 
            where TLookUpEntity : class, IProjectEntity
            where TLookUpProjection : class
        {
            return GetEntitiesViewModelCore<IEntitiesViewModel<TLookUpProjection>, TLookUpProjection>(
                propertyExpression,
                () =>
                    LookUpProjectEntitiesViewModel<TLookUpEntity, TLookUpProjection, TLookUpEntityKey, TUnitOfWork>
                        .Create(
                            UnitOfWorkFactory,
                            getRepositoryFunc,
                            projection)
                );
        }

    }
}
