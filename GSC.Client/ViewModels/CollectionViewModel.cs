﻿using System.Collections.Generic;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Properties;
using GSC.Client.Services;
using GSC.Client.Util;
using GSC.Client.ViewModels;

// ReSharper disable once CheckNamespace
namespace GSC.Client.Common.ViewModel
{
    public partial class CollectionViewModel<TEntity, TProjection, TPrimaryKey, TUnitOfWork>
        : ICollectionViewModelRefresh
    {
        protected override bool UserPermitsDeletion(TProjection projectionEntity)
        {
            var response = MessageBoxService.ShowMessage(
                string.Format(CommonResources.Confirmation_Delete, EntityName),
                CommonResources.Confirmation_Caption,
                MessageButton.YesNo,
                MessageIcon.Question);
            return response == MessageResult.Yes;
        }

        protected virtual IProjectService ProjectService
        {
            get
            {
                throw new System.NotImplementedException("Should be implemented by POCOViewModel.");
            }
        }

        public virtual string EntityName => typeof (TEntity).Name;

        #region Selection

        protected override void OnSelectedEntitiesChanged() { this.RaiseCanExecuteChanged(x => x.DeleteAll()); }

        #endregion

        #region DeleteAll

        public bool CanDeleteAll() { return SelectedEntities?.Any() ?? false; }

        public virtual void DeleteAll()
        {
            if (MessageResult.OK
                != MessageBoxService.Show("Удалить выбранные объекты?",
                                          CommonResources.Confirmation_Caption,
                                          MessageButton.OKCancel,
                                          MessageIcon.Question,
                                          MessageResult.OK))
                return;

            var items = SelectedEntities.ToArray();
            int counter = 0;
            foreach (var projection in items)
            {
                try
                {
                    DeleteCore(projection);
                    ++counter;
                }
                catch (ForeignKeyViolationException)
                {
                }
                catch (DbException)
                {
                }
            }

            MessageBoxService.Show(string.Format("Удалено {0} из {1} объектов.", counter, items.Length),
                                   "Информация",
                                   MessageButton.OK,
                                   MessageIcon.Information,
                                   MessageResult.OK);
            Refresh();
        }

        #endregion

        public override bool CanDelete(TProjection projectionEntity)
        {
            if ((SelectedEntities?.Count() ?? 0) > 1)
            {
                return CanDeleteAll();
            }

            return CanDeleteCore(projectionEntity);
        }

        public override void Delete(TProjection projectionEntity)
        {
            if ((SelectedEntities?.Count() ?? 0) > 1)
                DeleteAll();
            else
                DeleteBase(projectionEntity);
        }
    }
}