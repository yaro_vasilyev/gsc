﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Schedule;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.Schedule
{
    public class ScheduleCollectionViewModel 
        : ProjectEntityCollectionViewModel<Model.Exam>
    {
        public ScheduleCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Exams)
        {
            FilterExpression = e => e.Subject.ProjectId == CurrentProject.Id;
        }

        /// <summary>
        /// Creates a new instance of ScheduleCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ScheduleCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ScheduleCollectionViewModel(unitOfWorkFactory));
        }

        private IScheduleCollectionViewUpdateService ViewUpdateService
        {
            get { return this.GetService<IScheduleCollectionViewUpdateService>(); }
        }

        protected override string ViewName { get { return typeof(Model.Schedule).Name + "CollectionView"; } }

        #region SelectedSchedule

        public virtual ScheduleInfo SelectedSchedule { get; set; }

        //private void UpdateCommands()
        //{
        //    this.RaiseCanExecuteChanged(x => x.New());
        //    this.RaiseCanExecuteChanged(x => x.Edit(null));
        //    this.RaiseCanExecuteChanged(x => x.Delete(null));
        //}

        protected virtual void OnSelectedScheduleChanged()
        {
            UpdateCommands();
        }

        #endregion

        #region Loading...

        private List<ScheduleInfo> scheduleInfos;
        private List<StationInfo> stationInfos;

        protected override void OnEntitiesLoaded(IGscContextUnitOfWork unitOfWork,
                                                 IEnumerable<Model.Exam> entitiesEnumerable)
        {
            base.OnEntitiesLoaded(unitOfWork, entitiesEnumerable);

            scheduleInfos = LoadScheduleInfos(unitOfWork);
            stationInfos = LoadStationInfos(unitOfWork);
        }

        private List<ScheduleInfo> LoadScheduleInfos(IGscContextUnitOfWork uw)
        {
            var context = ((GscContextUnitOfWork)uw).Context;

            var list = ScheduleInfo.GetScheduleInfos(context, CurrentProject.Id).ToListAsync().Result;

            return list;
        }

        private List<StationInfo> LoadStationInfos(IGscContextUnitOfWork uw)
        {
            var context = ((GscContextUnitOfWork)uw).Context;

            var list = StationInfo.GetStationInfos(context, CurrentProject.Id).ToListAsync().Result;

            return list;
        }

        protected override void OnEntitiesAssigned(Func<Model.Exam> getSelectedEntityCallback)
        {
            base.OnEntitiesAssigned(getSelectedEntityCallback);
            UpdateExamDates();
            UpdateScheduleInfos();
            UpdateStationInfos();
            ViewUpdateService.RefreshSchedules(FilteredScheduleInfos);
        }

        #endregion

        public override void OnLoaded()
        {
            base.OnLoaded();
            Refresh();
        }

        #region ExamType

        private Model.ExamType? examType;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExamTypeChanged))]
        public virtual Model.ExamType? ExamType
        {
            get { return examType; }
            set
            {
                if (examType != value)
                {
                    examType = value;
                    this.RaisePropertyChanged(x => x.ExamType);
                }
            }
        }

        protected virtual void OnExamTypeChanged()
        {
            UpdateExamDates();
            UpdateStationInfos();
        }

        #endregion

        #region PeriodType

        private Model.PeriodType? periodType;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual Model.PeriodType? PeriodType
        {
            get { return periodType; }
            set
            {
                if (periodType != value)
                {
                    periodType = value;
                    this.RaisePropertyChanged(x => x.PeriodType);
                }
            }
        }

        protected virtual void OnPeriodTypeChanged()
        {
            UpdateExamDates();
            UpdateScheduleInfos();
            UpdateStationInfos();
        }

        #endregion

        #region RegionId

        private int? regionId;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRegionIdChanged))]
        public virtual int? RegionId
        {
            get { return regionId; }
            set
            {
                if (regionId != value)
                {
                    regionId = value;
                    this.RaisePropertyChanged(x => x.RegionId);
                }
            }
        }

        protected void OnRegionIdChanged()
        {
            UpdateScheduleInfos();
            UpdateStationInfos();
        }

        #endregion

        #region FilteredExamDates

        private readonly List<DateTime> filteredExamDates = new List<DateTime>();

        public virtual List<DateTime> FilteredExamDates
        {
            get { return filteredExamDates; }
        }

        public virtual void UpdateExamDates()
        {
            filteredExamDates.Clear();

            var dates = from e in Entities
                        where (ExamType == null || ExamType != null && e.ExamType == ExamType) &&
                              (PeriodType == null || PeriodType != null && e.PeriodType == PeriodType)
                        orderby e.ExamDate
                        select e.ExamDate;

            filteredExamDates.AddRange(dates.Distinct());

            ViewUpdateService?.RefreshExamDates(FilteredExamDates);
        }

        #endregion

        #region FilteredScheduleInfos

        private readonly List<ScheduleInfo> filteredScheduleInfos = new List<ScheduleInfo>();

        public virtual List<ScheduleInfo> FilteredScheduleInfos
        {
            get { return filteredScheduleInfos; }
        }

        protected virtual void UpdateScheduleInfos()
        {
            filteredScheduleInfos.Clear();

            var infos = from i in scheduleInfos
                        where (PeriodType == null || PeriodType != null && i.PeriodType == PeriodType) &&
                              (RegionId == null || RegionId != null && i.RegionId == RegionId)
                        select i;

            filteredScheduleInfos.AddRange(infos);

            ViewUpdateService?.RefreshSchedules(FilteredScheduleInfos);
        }

        #endregion

        #region FilteredStationInfos

        private readonly List<StationInfo> filteredStationInfos = new List<StationInfo>();

        public List<StationInfo> FilteredStationInfos
        {
            get { return filteredStationInfos; }
        }

        protected virtual void UpdateStationInfos()
        {
            filteredStationInfos.Clear();

            var stations = from s in stationInfos
                           where (ExamType == null || ExamType != null && s.ExamType == ExamType)
                           select s;

            filteredStationInfos.AddRange(stations);
            ViewUpdateService?.RefreshStations(FilteredStationInfos);
        }

        #endregion

        #region Helpers

        private void ClearCollection(IList collection)
        {
            while (collection.Count > 0)
            {
                collection.RemoveAt(0);
            }
        }

        private Model.Schedule FindByFake(ScheduleInfo info)
        {
            if (info == null)
                return null;
            var uw = (IGscContextUnitOfWork)ReadOnlyRepository.UnitOfWork;
            return uw.Schedules.Find(SelectedSchedule.ScheduleId);
        }

        #endregion

        #region Lookups

        private RegionCollectionViewModel regionsViewModel;

        public IEntitiesViewModel<Model.Region> RegionsLookup
        {
            get
            {
                if (regionsViewModel == null)
                {
                    regionsViewModel = RegionCollectionViewModel.Create(UnitOfWorkFactory);
                    regionsViewModel.SetParentViewModel(this);
                }
                return regionsViewModel;
            }

        }

        #endregion

        #region Commands

        protected override string SNewOperation => SecurityOperations.Schedules.New;

        protected override string SEditOperation => SecurityOperations.Schedules.Edit;

        protected override string SDeleteOperation => SecurityOperations.Schedules.Delete;

        public override string EntityName => "График";

        public override void New()
        {
            this.EnsureOperationAllowed(SecurityOperations.Schedules.New);
            DocumentManagerService.ShowNewEntityDocument<Model.Schedule>(this);
        }

        public override bool CanEdit(Model.Exam projectionEntity)
        {
            var canEdit = this.OperationAllowed(SecurityOperations.Schedules.Edit);
            if (!canEdit)
                return false;

            var schedule = FindByFake(SelectedSchedule);
            return schedule != null && !IsLoading;
        }

        public override void Edit(Model.Exam projectionEntity)
        {
            this.EnsureOperationAllowed(SecurityOperations.Schedules.Edit);

            var schedule = FindByFake(SelectedSchedule);
            if (schedule == null)
                return;

            var repo = ((IGscContextUnitOfWork)ReadOnlyRepository.UnitOfWork).Schedules;

            if (repo.IsDetached(schedule))
                return;

            DocumentManagerService.ShowExistingEntityDocument<Model.Schedule, int>(this, schedule.Id);
        }

        public override void View(Model.Exam projectionEntity)
        {
            var schedule = FindByFake(SelectedSchedule);
            if (schedule == null)
                return;

            var repo = ((IGscContextUnitOfWork)ReadOnlyRepository.UnitOfWork).Schedules;

            if (repo.IsDetached(schedule))
                return;

            DocumentManagerService.ShowExistingEntityDocument<Model.Schedule, int>(this, schedule.Id);
        }

        public override bool CanDelete(Model.Exam projectionEntity)
        {
            var canDelete = this.OperationAllowed(SecurityOperations.Schedules.Delete);
            if (!canDelete)
                return false;

            var schedule = FindByFake(SelectedSchedule);
            return schedule != null && 
                !IsLoading;
        }

        public override void Delete(Model.Exam projectionEntity)
        {
            this.EnsureOperationAllowed(SecurityOperations.Schedules.Delete);

            var schedule = FindByFake(SelectedSchedule);
            if (schedule == null)
                return;

            if (MessageBoxService.ShowMessage(string.Format(CommonResources.Confirmation_Delete, typeof(Model.Schedule).Name),
                CommonResources.Confirmation_Caption, MessageButton.YesNo) != MessageResult.Yes)
                return;

            try
            {
                var repo = ((IGscContextUnitOfWork)ReadOnlyRepository.UnitOfWork).Schedules;
                scheduleInfos.RemoveAll(i => i.ScheduleId == schedule.Id);
                var repoSchedule = repo.Find(schedule.Id);
                if (repoSchedule != null)
                {
                    OnBeforeEntityDeleted(repoSchedule.Id, repoSchedule);
                    repo.Remove(repoSchedule);
                    repo.UnitOfWork.SaveChanges();
                    OnEntityDeleted(repoSchedule.Id, repoSchedule);
                }
            }
            catch (DbException e)
            {
                Refresh();
                MessageBoxService.ShowMessage(e.ErrorMessage, e.ErrorCaption, MessageButton.OK, MessageIcon.Error);
            }
        }

        protected virtual void OnBeforeEntityDeleted(int pk, Model.Schedule entity)
        {

        }

        protected virtual void OnEntityDeleted(int pk, Model.Schedule entity)
        {
            UpdateScheduleInfos();
        }

        #endregion

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.Schedules.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.Schedules.ExportToExcel);
        }


        #region Messenger handlers

        protected override void OnInitializeInRuntime()
        {
            base.OnInitializeInRuntime();
            Messenger.Default.Register<EntityMessage<Model.Schedule, int>>(this, OnEntityMessage);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Messenger.Default.Unregister(this);
        }

        protected virtual void OnEntityMessage(EntityMessage<Model.Schedule, int> message)
        {
            Debug.WriteLine("On entity message: Schedule");
            Refresh();
        }

        #endregion
    }
}
