﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Schedule
{
    public class StationInfo
    {
        public int ScheduleId { get; set; }
        public int DetailId { get; set; }
        public DateTime ExamDate { get; set; }
        public int StationId { get; set; }
        public string StationDisplayText { get; set; }
        public Model.ExamType ExamType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">REQUIRED. </param>
        /// <param name="projectId"></param>
        /// <param name="dateTo"></param>
        /// <param name="examType"></param>
        /// <param name="periodType"></param>
        /// <param name="dateFrom"></param>
        /// <returns></returns>
        public static DbRawSqlQuery<StationInfo> GetStationInfos([NotNull] DbContext context,
                                                                 int projectId,
                                                                 DateTime? dateFrom = null,
                                                                 DateTime? dateTo = null,
                                                                 Model.ExamType? examType = null,
                                                                 Model.PeriodType? periodType = null)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            //todo перенести в БД или в LINQ
            return context.Database.SqlQuery<StationInfo>(@"
select distinct
       v_schedule.id as ScheduleId, 
       v_schedule_detail.id as DetailId, 
       v_schedule_detail.exam_date as ExamDate,
       v_station.id as StationId, 
       concat_ws(', ', v_station.code, v_station.name, v_station.address) as StationDisplayText,
       v_exam.exam_type as ExamType
  from gsc.v_schedule
  join gsc.v_expert on v_schedule.expert_id = v_expert.id
  join gsc.v_schedule_detail on v_schedule.id = v_schedule_detail.schedule_id
  join gsc.v_station on v_station.id = v_schedule_detail.station_id
  join gsc.v_exec on v_exec.station_id = v_schedule_detail.station_id 
  join gsc.v_exam on v_exam.id = v_exec.exam_id and v_exam.exam_date = v_schedule_detail.exam_date 
 where v_station.project_id = :p0 and
       v_expert.period_id   = :p0 and
       (:p1::integer is null or :p1::integer  is not null and v_exam.exam_type    =   :p1::integer) and
       (:p2::integer is null or :p2::integer  is not null and v_exam.period_type  =   :p2::integer) and
       (:p3::date    is null or :p3::date     is not null and v_exam.exam_date    >=  :p3::date) and
       (:p4::date    is null or :p4::date     is not null and v_exam.exam_date    <=  :p4::date)",
                projectId,
                examType,
                periodType,
                dateFrom,
                dateTo);
        }
    }
}
