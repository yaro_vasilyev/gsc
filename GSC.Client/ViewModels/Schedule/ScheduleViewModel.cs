﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.Schedule
{
    public class ScheduleViewModel : ProjectEntityViewModel<Model.Schedule>
    {
        public const string PpeGridServiceKey = "PpeGrid";
        public const string DetailsGridServiceKey = "DetailsGrid";

        #region Ctor/Factory

        public static ScheduleViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ScheduleViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ExpertViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExpertViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ScheduleViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                   x => x.Schedules,
                   x => x.Expert.FullName)
        { }

        #endregion

        protected override string SDeleteOperation => SecurityOperations.Schedules.Delete;

        protected override string SNewOperation => SecurityOperations.Schedules.New;

        protected override string SEditOperation => SecurityOperations.Schedules.Edit;

        protected override string LocalizedEntityName => "График";

        #region SelectedStations

        public virtual Model.Station[] SelectedStations { get; set; }

        protected virtual void OnSelectedStationsChanged() { Update(); }

        #endregion

        #region [Can]AddSelectedStations

        public void AddSelectedStations()
        {
            Debug.Assert(SelectedDate != null);
            Debug.Assert(SelectedStations != null);

            foreach (var station in SelectedStations)
            {
                var scheduleDetail = UnitOfWork.ScheduleDetails.Create(true);
                scheduleDetail.ScheduleId = Entity.Id;
                scheduleDetail.StationId = station.Id;
                scheduleDetail.ExamDate = (DateTime)SelectedDate;

                ScheduleDetails.Add(scheduleDetail);
                scheduleDetailsManipulation = true;
            }

            SelectedStations = null;

            RefreshFilteredScheduleDetails();
            RefreshAvailableStationsCollection();
            Update();
        }

        public bool CanAddSelectedStations() { return !ReadOnly && (SelectedStations?.Length ?? 0) > 0; }

        #endregion

        #region SelectedDetails

        public virtual Model.ScheduleDetail[] SelectedDetails { get; set; }

        protected virtual void OnSelectedDetailsChanged() { Update(); }

        #endregion

        #region [Can]RemoveSelectedDetails

        public void RemoveSelectedDetails()
        {
            Debug.Assert(SelectedDetails != null);
            foreach (var detail in SelectedDetails)
            {
                ScheduleDetails.Remove(detail);
                UnitOfWork.ScheduleDetails.Remove(detail);
                scheduleDetailsManipulation = true;
            }

            SelectedDetails = null;

            RefreshFilteredScheduleDetails();
            RefreshAvailableStationsCollection();
            Update();
        }

        public bool CanRemoveSelectedDetails() { return !ReadOnly && SelectedDetails != null && SelectedDetails.Length > 0; }

        #endregion

        #region ScheduleDetails

        private bool scheduleDetailsManipulation = false;

        public ICollection<Model.ScheduleDetail> ScheduleDetails
        {
            get { return Entity.ScheduleDetails; }
        }

        #endregion

        #region FilteredScheduleDetails

        private readonly ObservableCollection<Model.ScheduleDetail> filteredScheduleDetails =
            new ObservableCollection<Model.ScheduleDetail>();

        public virtual ObservableCollection<Model.ScheduleDetail> FilteredScheduleDetails => filteredScheduleDetails;

        protected void RefreshFilteredScheduleDetails()
        {
            filteredScheduleDetails.Clear();

            IEnumerable<Model.ScheduleDetail> source = Enumerable.Empty<Model.ScheduleDetail>();
            if (SelectedDate != null)
                source = ScheduleDetails.Where(d => d.ExamDate == SelectedDate).ToList();

            source.ForEach(filteredScheduleDetails.Add);

            this.GetService<IUpdateGridService>(DetailsGridServiceKey)?.Update();
        }

        #endregion

        private Model.Project ExpertProject
        {
            get { return Entity?.Expert?.Project; }
        }

        private int CurrentProjectId
        {
            get { return ExpertProject?.Id ?? -1; }
        }

        #region AvailableStations

        public virtual ObservableCollection<Model.Station> AvailableStations => availableStations;

        private readonly ObservableCollection<Model.Station> availableStations =
            new ObservableCollection<Model.Station>();

        private void RefreshAvailableStationsCollection()
        {
            availableStations.Clear();

            try
            {
                if (SelectedDate == null)
                    return;

                var query = UnitOfWork.Execs
                    .Include(e => e.Station)
                    .Include(e => e.Station.Region)
                    .Include(e => e.Exam)
                    .Where(e => e.Station.ProjectId == CurrentProjectId &&
                                e.Station.RegionId == RegionId &&
                                e.Exam.PeriodType == PeriodType &&
                                e.Exam.ExamType == ExamType &&
                                e.Exam.ExamDate == SelectedDate &&
                                e.Exam.Subject.ProjectId == CurrentProjectId)
                    .Select(e => e.Station)
                    .Distinct();

                var ppeList =
                    query.ToList()
                        .Except(
                            ((ScheduleDetails ?? Enumerable.Empty<Model.ScheduleDetail>()).Where(
                                d => d.ExamDate == SelectedDate)).Select(x => x.Station))
                        .ToList();

                ppeList.ForEach(availableStations.Add);

            }
            finally
            {
                this.GetService<IUpdateGridService>(PpeGridServiceKey)?.Update();
            }
        }

        #endregion

        #region Lookup datasources

        public IEntitiesViewModel<Model.Region> LookupRegions
        {
            get { return GetLookUpProjectEntitiesViewModel((ScheduleViewModel vm) => vm.LookupRegions, x => x.Regions); }
        }

        public IEntitiesViewModel<Model.Exam> LookupExams
        {
            get
            {
                return GetLookUpProjectionsViewModel((ScheduleViewModel vm) => vm.LookupExams,
                                                     uw => uw.Exams,
                                                     ExamsProjection);
            }
        }

        private IQueryable<Model.Exam> ExamsProjection(IRepositoryQuery<Model.Exam> query)
        {
            return query.Where(e => e.Subject.ProjectId == CurrentProjectId && e.ExamType == ExamType && e.PeriodType == PeriodType)
                .Include(e => e.Subject);
        }


        public IEntitiesViewModel<Model.Expert> LookupExperts
        {
            get
            {
                return GetLookUpProjectionsViewModel((ScheduleViewModel vm) => vm.LookupExperts,
                    uw => uw.Experts,
                    ExpertsProjection);
            }
        }

        private IQueryable<Model.Expert> ExpertsProjection(IRepositoryQuery<Model.Expert> query)
        {
            var project = ProjectService.GetCurrentProject().Match(p => p.Id, e => { throw e; });

            var ret = query.Where(e => e.ProjectId == project);

            if (ExpertKindOfficial & ExpertKindSocial)
            {
                // All of them.
                return ret;
            }

            if (ExpertKindOfficial)
            {
                return ret.Where(e => e.Kind == Model.ExpertKind.Official);
            }

            if (ExpertKindSocial)
            {
                return ret.Where(e => e.Kind == Model.ExpertKind.Social);
            }

            // All of them if none of the options selected.
            return ret;
        }

        #endregion

        #region Filters

        #region PeriodType

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual bool PeriodTypeEarly
        {
            get { return Entity.PeriodType == Model.PeriodType.Early; }
            set
            {
                if (PeriodType == Model.PeriodType.Early && value)
                    return;

                PeriodType = Model.PeriodType.Early;
                this.RaisePropertyChanged(x => x.PeriodType);
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual bool PeriodTypePrimary
        {
            get { return Entity.PeriodType == Model.PeriodType.Primary; }
            set
            {
                if (PeriodType == Model.PeriodType.Primary && value)
                    return;

                PeriodType = Model.PeriodType.Primary;

                this.RaisePropertyChanged(x => x.PeriodType);
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual bool PeriodTypeAdditional
        {
            get { return Entity.PeriodType == Model.PeriodType.Additional; }
            set
            {
                if (Entity.PeriodType == Model.PeriodType.Additional && value)
                    return;

                PeriodType = Model.PeriodType.Additional;
                this.RaisePropertyChanged(x => x.PeriodType);
            }
        }


        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual Model.PeriodType PeriodType
        {
            get { return Entity.PeriodType; }
            set
            {
                if (Entity.PeriodType == value)
                    return;
                Entity.PeriodType = value;
                this.RaisePropertyChanged(x => x.PeriodType);
            }
        }

        protected virtual void OnPeriodTypeChanged()
        {
            this.RaisePropertyChanged(x => x.PeriodTypeEarly);
            this.RaisePropertyChanged(x => x.PeriodTypePrimary);
            this.RaisePropertyChanged(x => x.PeriodTypeEarly);

            UpdateSelectedExamLookup();
            RefreshDatesCollection();
            Update();
        }

        #endregion

        #region ExamType

        private bool examTypeEge = true;
        private bool examTypeGve;
        private bool examTypeOge;
        private Model.ExamType examType = Model.ExamType.Ege;

        public virtual bool ExamTypeEge
        {
            get { return examTypeEge; }
            set
            {
                if (examTypeEge == value)
                    return;

                examTypeEge = value;
                if (value)
                {
                    ExamTypeGve = false;
                    ExamTypeOge = false;
                }

                ExamType = Model.ExamType.Ege;
                this.RaisePropertyChanged(x => x.ExamTypeEge);
                this.RaisePropertyChanged(x => x.ExamType);
            }
        }

        public virtual bool ExamTypeGve
        {
            get { return examTypeGve; }
            set
            {
                if (examTypeGve == value)
                    return;

                examTypeGve = value;
                if (value)
                {
                    ExamTypeEge = false;
                    ExamTypeOge = false;
                }

                ExamType = Model.ExamType.Gve;
                this.RaisePropertyChanged(x => x.ExamTypeGve);
                this.RaisePropertyChanged(x => x.ExamType);
            }
        }

        public virtual bool ExamTypeOge
        {
            get { return examTypeOge; }
            set
            {
                if (examTypeOge == value)
                    return;

                examTypeOge = value;
                if (value)
                {
                    ExamTypeEge = false;
                    ExamTypeGve = false;
                }

                ExamType = Model.ExamType.Oge;
                this.RaisePropertyChanged(x => x.ExamTypeOge);
                this.RaisePropertyChanged(x => x.ExamType);
            }
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExamTypeChanged))]
        public virtual Model.ExamType ExamType
        {
            get { return examType; }
            protected set
            {
                if (examType == value)
                    return;
                examType = value;
                this.RaisePropertyChanged(x => x.ExamType);
            }
        }

        protected virtual void OnExamTypeChanged()
        {
            UpdateSelectedExamLookup();
            RefreshDatesCollection();
        }

        #endregion

        #region ExpertKind

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpertKindChanged))]
        public virtual bool ExpertKindOfficial { get; set; } = true;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpertKindChanged))]
        public virtual bool ExpertKindSocial { get; set; } = true;

        protected void OnExpertKindChanged()
        {
            if (IsNewMode)
            {
                ExpertId = -1;
                RefreshLookUpCollection((ScheduleViewModel vm) => vm.LookupExperts, true);
            }
        }

        #endregion

        private void UpdateSelectedExamLookup()
        {
            this.RaisePropertyChanged(x => x.FilteredScheduleDetails);
            SelectedDate = null;
        }

        #endregion

        #region ExpertId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpertIdChanged))]
        public virtual int ExpertId
        {
            get { return Entity.ExpertId; }
            set
            {
                if (Entity.ExpertId != value)
                {
                    Entity.ExpertId = value;
                    this.RaisePropertyChanged(x => x.ExpertId);
                }
            }
        }

        protected virtual void OnExpertIdChanged()
        {
            CheckScheduleExists();
            Update();
        }

        #endregion

        #region RegionId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRegionIdChanged))]
        public virtual int RegionId
        {
            get { return Entity.RegionId; }
            set
            {
                if (Entity.RegionId != value)
                {
                    Entity.RegionId = value;
                    this.RaisePropertyChanged(x => x.RegionId);
                }
            }
        }

        protected virtual void OnRegionIdChanged()
        {
            RefreshAvailableStationsCollection();
            RefreshDatesCollection();
            CheckScheduleExists();
            Update();
        }

        #endregion

        #region SelectedDate

        private DateTime? selectedDate = null;

        private readonly ObservableCollection<DateTime> dates =
            new ObservableCollection<DateTime>();

        public ObservableCollection<DateTime> Dates { get { return dates; } }

        public virtual void RefreshDatesCollection()
        {
            dates.Clear();

            SelectedDate = null;

            if (RegionId == 0)
                return;

            var query = UnitOfWork.Execs
                .Include(x => x.Station)
                .Include(x => x.Station.Region)
                .Include(x => x.Exam)
                .Where(x => x.Station.ProjectId == CurrentProjectId &&
                            x.Exam.Subject.ProjectId == CurrentProjectId &&
                            x.Station.RegionId == RegionId &&
                            x.Exam.PeriodType == PeriodType &&
                            x.Exam.ExamType == ExamType)
                .Select(x => x.Exam.ExamDate)
                .Distinct();

            var list = new List<DateTime>(query);
            list.Sort();

            list.ForEach(dates.Add);

            this.RaisePropertyChanged(x => x.Dates);
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnSelectedDateChanged))]
        public virtual DateTime? SelectedDate
        {
            get { return selectedDate; }
            set
            {
                if (selectedDate != value)
                {
                    selectedDate = value?.Date;
                    this.RaisePropertyChanged(x => x.SelectedDate);
                }
            }
        }

        protected void OnSelectedDateChanged()
        {
            RefreshAvailableStationsCollection();
            RefreshFilteredScheduleDetails();
            Update();
        }

        protected void OnDatesChanged() { SelectedDate = null; }

        #endregion

        #region MonitorRcoi

        public virtual bool MonitorRcoi
        {
            get { return Entity.MonitorRcoi == Model.BooleanType.Yes; }
            set
            {
                var newValue = value ? Model.BooleanType.Yes : Model.BooleanType.No;
                if (Entity.MonitorRcoi != newValue)
                {
                    Entity.MonitorRcoi = newValue;
                    this.RaisePropertyChanged(x => x.MonitorRcoi);
                    Update();
                }
            }
        }

        #endregion

        #region ScheduleExists

        private bool ScheduleExistsInternal()
        {
            // если правка, то важные контроды должны быть заблокированы и меняем только неважные.
            if (!IsNew())
                return false;

            // если что-то не установлено из важного - ничего не проверяем.
            if (RegionId == 0 || ExpertId == 0)
                return false;

            var query = from s in this.Repository
                        where s.Expert.ProjectId == CurrentProjectId &&
                              s.ExpertId == ExpertId &&
                              s.RegionId == RegionId &&
                              s.PeriodType == PeriodType
                        select s;
            return query.Any();
        }

        private bool scheduleExists = false;

        public virtual bool ScheduleExists
        {
            get { return scheduleExists; }
            protected set
            {
                if (scheduleExists != value)
                {
                    scheduleExists = value;
                    this.RaisePropertyChanged(x => x.ScheduleExists);
                    Update();
                }
            }
        }

        private void CheckScheduleExists() { ScheduleExists = ScheduleExistsInternal(); }

        #endregion

        protected override void UpdateCommands()
        {
            base.UpdateCommands();
            this.RaiseCanExecuteChanged(x => x.AddSelectedStations());
            this.RaiseCanExecuteChanged(x => x.RemoveSelectedDetails());
        }

        protected override bool NeedSave() { return !ReadOnly && base.NeedSave() || scheduleDetailsManipulation; }

        protected override bool NeedReset()
        {
            return base.NeedReset() || scheduleDetailsManipulation;
        }

        protected override bool HasValidationErrors() { return base.HasValidationErrors() || ScheduleExists; }

        protected override void OnEntitySaved(int primaryKey, Model.Schedule entity, bool isNewEntity)
        {
            base.OnEntitySaved(primaryKey, entity, isNewEntity);
            scheduleDetailsManipulation = false;
        }

        protected override void OnEntityChanged()
        {
            base.OnEntityChanged();
            RefreshDatesCollection();
        }

        public List<Model.Exec> GetExecsForStation(Model.Station station)
        {
            var execs = UnitOfWork.Execs
                .Include(x => x.Exam)
                .Include(x => x.Exam.Subject)
                .Where(x => x.StationId == station.Id &&
                            x.Exam.Subject.ProjectId == CurrentProjectId &&
                            x.Station.ProjectId == CurrentProjectId &&
                            x.Exam.ExamDate == SelectedDate &&
                            x.Exam.PeriodType == PeriodType &&
                            x.Exam.ExamType == ExamType);

            return new List<Model.Exec>(execs);
        }
    }
}
