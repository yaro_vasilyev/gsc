﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Services;

namespace GSC.Client.ViewModels.Schedule
{
    public class ScheduleDetailReadOnlyCollectionViewModel : ReadOnlyCollectionViewModel<Model.ScheduleDetail, IGscContextUnitOfWork>
    {
        /// <summary>
        /// Creates a new instance of ScheduleDetailReadOnlyCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ScheduleDetailReadOnlyCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new ScheduleDetailReadOnlyCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the RsmCoordinatorCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ScheduleDetailReadOnlyCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ScheduleDetailReadOnlyCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(
                unitOfWorkFactory ??
                UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.ScheduleDetails,
                x => x.Include(s => s.Station)
                .Include(s => s.Schedule))
        {
            SetFilter();
        }

        protected void SetFilter()
        {
            var projectService = this.GetRequiredService<IProjectService>();
            var project = projectService.GetCurrentProject().Match(p => p.Id, e => { throw e; });
            FilterExpression = detail => detail.Schedule.Expert.ProjectId == project;
        }
    }
}
