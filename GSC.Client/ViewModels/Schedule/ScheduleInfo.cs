﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.Schedule
{
    public class ScheduleInfo
    {
        public int ScheduleId { get; set; }
        public int RegionId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string ExpertName { get; set; }
        public Model.BooleanType MonitorRcoi { get; set; }
        public Model.BooleanType Fixed { get; set; }
        public Model.PeriodType PeriodType { get; set; }
        /// <summary>
        /// SHOULD BE in [0..n) range, where n - max details count by date
        /// </summary>
        public int MasterRowId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">REQUIRED. </param>
        /// <param name="projectId"></param>
        /// <param name="periodType"></param>
        /// <param name="expertKind"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public static DbRawSqlQuery<ScheduleInfo> GetScheduleInfos([NotNull] DbContext context,
                                                                   int projectId,
                                                                   Model.PeriodType? periodType = null,
                                                                   Model.ExpertKind? expertKind = null,
                                                                   int? regionId = null)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            //todo перенести в БД или в LINQ
            return context.Database.SqlQuery<ScheduleInfo>(@"
with date_groups as (
  select schedule_id, 
         exam_date, 
         count(*) as cnt 
    from gsc.v_schedule_detail 
   group by schedule_id, 
         exam_date
), date_group_max_dates as (
  select tt.schedule_id, 
         max(tt.cnt) as distinct_dates 
    from date_groups as tt
   group by schedule_id
)
select m.id as ScheduleId, 
       r.id as RegionId, r.code as RegionCode, r.name as RegionName,
       concat_ws(' ', e.surname, e.name, e.patronymic) as ExpertName, 
       m.monitor_rcoi as MonitorRcoi, m.fixed as Fixed, m.period_type as PeriodType, 
       generate_series(0, coalesce(d.distinct_dates, 1) - 1) as MasterRowId
  from gsc.v_schedule as m
  join gsc.v_expert as e on e.id = m.expert_id
  left join date_group_max_dates as d on d.schedule_id = m.id
  left join gsc.v_region as r on r.id = m.region_id
 where e.period_id = :p0 and
       (cast(:p1 as integer) is null or cast(:p1 as integer) is not null and m.period_type = cast(:p1 as integer)) and
       (cast(:p2 as integer) is null or cast(:p2 as integer) is not null and r.id = cast(:p2 as integer)) and
       (cast(:p3 as integer) is null or cast(:p3 as integer) is not null and e.kind = cast(:p3 as integer))",
                projectId,
                periodType,
                regionId,
                expertKind);
        }
    }
}
