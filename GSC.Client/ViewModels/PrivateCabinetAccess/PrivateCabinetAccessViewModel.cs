﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraReports.UI;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Modules.Admin.PrivateCabinetAccess;
using JetBrains.Annotations;
using MessageButton = DevExpress.Mvvm.MessageButton;
using MessageIcon = DevExpress.Mvvm.MessageIcon;

namespace GSC.Client.ViewModels.PrivateCabinetAccess
{
    [UsedImplicitly]
    public class PrivateCabinetAccessViewModel : ReadOnlyCollectionViewModel<Model.Expert, IGscContextUnitOfWork>
    {
        public PrivateCabinetAccessViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory,
            Func<IGscContextUnitOfWork, IRepository<Model.Expert, int>> getRepositoryFunc,
            Func<IRepositoryQuery<Model.Expert>, IQueryable<Model.Expert>> projection = null,
            Action<Model.Expert> newEntityInitializer = null, Func<bool> canCreateNewEntity = null,
            bool ignoreSelectEntityMessage = false)
            : base(
                unitOfWorkFactory, getRepositoryFunc, projection)
        {
        }

        /// <summary>
        /// Creates a new instance of ExpertCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static PrivateCabinetAccessViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PrivateCabinetAccessViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the PrivateCabinetAccessViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ExpertCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected PrivateCabinetAccessViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(
                unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                contextUnitOfWork => contextUnitOfWork.Experts)
        {
            UpdateFilter();
        }

        public override void Refresh()
        {
            UpdateFilter();
            base.Refresh();
        }

        private void UpdateFilter()
        {
            if (WithLogins)
            {
                FilterExpression = null;
            }
            else
            {
                FilterExpression = expert => expert.UserGuid == null;
            }
        }

        public virtual Model.Expert[] SelectedExperts { get; [UsedImplicitly] set; }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnWithLoginsChanged))]
        public virtual bool WithLogins { get; set; } = false;

        protected void OnWithLoginsChanged()
        {
            Refresh();
        }

        [UsedImplicitly]
        public void CreateLogins()
        {
            if (SelectedExperts == null || SelectedExperts.Length == 0)
            {
                MessageBoxService.Show("Не выбран ни один эксперт.", "Не выбраны эксперты", MessageButton.OK,
                    MessageIcon.Warning, MessageResult.OK);
                return;
            }

            // формировние логинов
            var context = CreateUnitOfWork().Context;
            var loginList = new List<ExpertLogins.DataRow>();
            foreach (var expert in SelectedExperts)
            {
                const int expertUserType = 0; //todo take from enum?
                var password =
                    context.Database.SqlQuery<string>("select * from gsc.u_create_user(:p0, :p1)", expertUserType,
                        expert.Id).First();
                loginList.Add(new ExpertLogins.DataRow
                {
                    FullName = expert.FullName,
                    BaseRegion = expert.BaseRegion?.Name,
                    Email = expert.Email,
                    Login = expert.Email /*todo возвращать сформированный логин из ХП*/,
                    Password = password
                });
            }

            // формирование отчёта
            var rep = new ExpertLogins {DataSource = loginList};
            rep.ShowRibbonPreview();

            Refresh();
        }
    }
}