﻿using System;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using JetBrains.Annotations;

namespace GSC.Client.ViewModels.CallCenter
{
    /// <summary>
    /// Represents the single EventViewModel object view model.
    /// </summary>
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class EventViewModel : ProjectEntityViewModel<Model.Event>
    {
        private bool initialized;

        /// <summary>
        /// Creates a new instance of EventViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        [UsedImplicitly]
        public static EventViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new EventViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the EventViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the EventViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected EventViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Events)
        {
        }

        protected override string SNewOperation => SecurityOperations.CallCenter.New;

        protected override string SEditOperation => SecurityOperations.CallCenter.Edit;

        protected override string SDeleteOperation => SecurityOperations.CallCenter.Delete;

        protected override void OnEntitySaved(int primaryKey, Model.Event entity, bool isNewEntity)
        {
            base.OnEntitySaved(primaryKey, entity, isNewEntity);
            initialized = false;
        }

        protected override void OnEntityChanged()
        {
            base.OnEntityChanged();

            if (IsEditMode && Entity != null)
            {
                RegionId = Entity.Schedule.RegionId;
                ScheduleId = Entity.ScheduleId;
            }

            Update();
            initialized = true;
        }

        protected override string LocalizedEntityName => "Обращение";

        protected override Model.Event CreateEntity()
        {
            var entity = base.CreateEntity();
            entity.ObjectType = Model.VisitObjectType.PPE;
            entity.Timestamp = DateTime.Now;
            entity.Channel = Model.ChannelType.Phone;
            entity.Type = Model.EventType.Info;
            entity.UserGuid = this.GetRequiredService<ISecurityService>()?.CurrentUser?.Guid;
            return entity;
        }

        protected override void UpdateCommands()
        {
            base.UpdateCommands();
            this.RaiseCanExecuteChanged(vm => vm.AddNewMonitorResult());
        }

        [UsedImplicitly]
        public void AddNewMonitorResult()
        {
            this.EnsureOperationAllowed(SecurityOperations.MonitorResults.New);

            Save();

            Action<Model.MonitorResult> initializer = mr =>
            {
                mr.ScheduleId = ScheduleId ?? 0;
                mr.Date = Entity.Timestamp.Date;
                mr.EventId = Entity.Id;
                mr.ObjectType = Entity.ObjectType;
            };

            var dms = this.GetService<IDocumentManagerService>();
            dms.ShowNewEntityDocument(this, initializer);
        }

        [UsedImplicitly]
        public bool CanAddNewMonitorResult()
        {
            return Entity != null &&
                   (Type == Model.EventType.Emergency ||
                    Type == Model.EventType.Info) &&
                   (ScheduleId ?? 0) != 0 &&
                   this.OperationAllowed(SecurityOperations.MonitorResults.New);
        }

        #region RegionId

        private int? regionId;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRegionIdChanged))]
        public virtual int? RegionId
        {
            get { return regionId; }
            [UsedImplicitly]
            set
            {
                if (regionId != value)
                {
                    regionId = value;
                    this.RaisePropertyChanged(x => x.RegionId);
                }
            }
        }

        protected void OnRegionIdChanged()
        {
            if (!initialized)
                return;

            ScheduleId = null;

            RefreshLookUpCollection((EventViewModel vm) => vm.SchedulesLookup, true);
        }

        #endregion

        #region ScheduleId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnScheduleIdChanged))]
        public virtual int? ScheduleId
        {
            get { return Entity?.ScheduleId; }
            [UsedImplicitly]
            set
            {
                if (Entity != null)
                    Entity.ScheduleId = value ?? 0;
                this.RaisePropertyChanged(x => x.ScheduleId);
            }
        }

        protected void OnScheduleIdChanged()
        {
            if (!initialized)
                return;
            UpdateCommands();
        }

        #endregion

        #region Type

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnTypeChanged))]
        public virtual Model.EventType Type
        {
            get { return Entity.Type; }
            [UsedImplicitly]
            set
            {
                if (Entity.Type != value)
                {
                    Entity.Type = value;
                    this.RaisePropertyChanged(x => x.Type);
                }
            }
        }

        protected void OnTypeChanged()
        {
            if (!initialized)
                return;

            UpdateCommands();
        }

        #endregion

        #region Lookups

        public IEntitiesViewModel<Model.Region> RegionsLookup
        {
            get { return GetLookUpProjectEntitiesViewModel((EventViewModel vm) => vm.RegionsLookup, x => x.Regions); }
        }

        public IEntitiesViewModel<Model.Schedule> SchedulesLookup
        {
            get
            {
                return GetLookUpProjectionsViewModel((EventViewModel vm) => vm.SchedulesLookup,
                    x => x.Schedules,
                    SchedulesLookupProjection);
            }
        }

        public IEntitiesViewModel<Model.Observer> ObserversLookup
        {
            get
            {
                return GetLookUpProjectEntitiesViewModel((EventViewModel vm) => vm.ObserversLookup, x => x.Observers,
                    query => query.Where(o => o.Type == Model.ObserverType.Manager && o.UserGuid != null));
            }
        }

        private IQueryable<Model.Schedule> SchedulesLookupProjection(IRepositoryQuery<Model.Schedule> query)
        {
            var project = ProjectService.GetCurrentProject().Match(p => p.Id, e => { throw e; });
            return query.Include(s => s.Expert).Where(s => s.Expert.ProjectId == project && s.RegionId == RegionId);
        }

        #endregion

        /*Теперь UserGuid проставляется при создании сущности
         * 
         * protected override void OnBeforeEntitySaved(int primaryKey, Event entity, bool isNewEntity)
        {
            base.OnBeforeEntitySaved(primaryKey, entity, isNewEntity);
            if (isNewEntity)
            {
                var securityService = this.GetRequiredService<ISecurityService>();
                Debug.Assert(securityService.CurrentUser != null);
                entity.UserGuid = securityService.CurrentUser?.Guid;
            }
        }*/
    }
}