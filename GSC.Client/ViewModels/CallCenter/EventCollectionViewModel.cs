﻿using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;
using GSC.Model;

namespace GSC.Client.ViewModels.CallCenter
{
    /// <summary>
    /// Represents the Exam collection view model.
    /// </summary>
    public class EventCollectionViewModel : ProjectEntityCollectionViewModel<Event>
    {
        /// <summary>
        /// Creates a new instance of EventCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static EventCollectionViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new EventCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the EventCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the EventCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected EventCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.Events, q => q.Include(e => e.Schedule).Include(e => e.Schedule.Expert).Include(e => e.Schedule.Region).Include(e => e.UserFio))
        {
            FilterExpression = e => e.Schedule.Expert.ProjectId == CurrentProject.Id;
        }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.CallCenter.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.CallCenter.ExportToExcel);
        }

        protected override string SNewOperation => SecurityOperations.CallCenter.New;

        protected override string SEditOperation => SecurityOperations.CallCenter.Edit;

        protected override string SDeleteOperation => SecurityOperations.CallCenter.Delete;
    }

}
