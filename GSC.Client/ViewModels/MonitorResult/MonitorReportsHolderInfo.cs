﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSC.Client.Common.DataModel;

namespace GSC.Client.ViewModels.MonitorResult
{
    public class MonitorReportsHolderInfo : Model.IProjectEntity
    {
        public Model.Schedule Schedule { get; set; }

        public Model.Region Region { get; set; }

        public Model.Expert Expert { get; set; }

        public bool ReportsLoaded { get; set; }

        public int ProjectId { get; set; }

        public Model.Project Project { get; set; }

        public Model.BooleanType ReportsLoadedDomain { get { return ReportsLoaded ? Model.BooleanType.Yes : Model.BooleanType.No; } }

        public static IQueryable<MonitorReportsHolderInfo> GetProjection(IRepositoryQuery<Model.Schedule> query)
        {
            // TODO: Current Project filter?

            return query.Include(x => x.Region)
                .Include(x => x.Expert)
                .Select(m => new MonitorReportsHolderInfo
                    {
                        Schedule = m,
                        Expert = m.Expert,
                        Region = m.Region,
                        ReportsLoaded = m.MrDocs.Any(d => true)
                    });
        }
    }
}
