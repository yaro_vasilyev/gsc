﻿using System;
using System.Linq;
using Devart.Data.PostgreSql.Entity;
using GSC.Client.Common.DataModel;

namespace GSC.Client.ViewModels.MonitorResult
{
    public class MrDocInfo
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Filename { get; set; }
        public int BlobSize { get; set; }

        public virtual Model.Schedule Schedule { get; set; }

        public static IQueryable<MrDocInfo> GetProjection(IRepositoryQuery<Model.MrDoc> query)
        {
            return query.Select(x => new MrDocInfo
            {
                Id = x.Id,
                Schedule = x.Schedule,
                Filename = x.Filename,
                Timestamp = x.Timestamp,
                BlobSize = PgSqlFunctions.OctetLength(x.Blob) ?? 0
            });
        }
    }
}
