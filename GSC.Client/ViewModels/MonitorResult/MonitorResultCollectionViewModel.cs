﻿using System;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.MonitorResult
{
    /// <summary>
    /// Represents the MonitorResult collection view model.
    /// </summary>
    public class MonitorResultCollectionViewModel : ProjectEntityCollectionViewModel<Model.MonitorResult>
    {
        /// <summary>
        /// Creates a new instance of MonitorResultViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static MonitorResultCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MonitorResultCollectionViewModel(unitOfWorkFactory, null));
        }

        public static MonitorResultCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactor,
            Action<Model.MonitorResult> newEntityInitializer)
        {
            return
                ViewModelSource.Create(
                    () => new MonitorResultCollectionViewModel(unitOfWorkFactor, newEntityInitializer));
        }

        /// <summary>
        /// Initializes a new instance of the MonitorResultViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the MonitorResultViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        /// <param name="newEntityInitializer"></param>
        protected MonitorResultCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null,
            Action<Model.MonitorResult> newEntityInitializer = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.MonitorResults,
                q => q.Include(x => x.Schedule)
                    .Include(x => x.Schedule.Expert)
                    .Include(x => x.Schedule.Region)
                    .Include(x => x.ScheduleDetail)
                    .Include(x => x.ScheduleDetail.Station)
                    .Include(x => x.Violator)
                    .Include(x => x.Violation),
                newEntityInitializer)
        {
            FilterExpression = mr => mr.Schedule.Expert.ProjectId == CurrentProject.Id;
        }

        protected override string SNewOperation => SecurityOperations.MonitorResults.New;

        protected override string SEditOperation => SecurityOperations.MonitorResults.Edit;

        protected override string SDeleteOperation => SecurityOperations.MonitorResults.Delete;

        public override string EntityName { get { return "Результат мониторинга"; } }

        public void ExportToExcel(GridAdapter control)
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.MonitorResults.ExportToExcel);
            ExportToExcelService.Export(control);
        }

        public virtual bool CanExportToExcel(GridAdapter control)
        {
            return this.ReadOnlyOperationAllowed(SecurityOperations.MonitorResults.ExportToExcel);
        }
    }
}
