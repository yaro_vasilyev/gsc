﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.MonitorResult
{
    /// <summary>
    /// Represents the single MonitorResult object view model.
    /// </summary>
    public class MonitorResultViewModel : ProjectEntityViewModel<Model.MonitorResult>
    {
        public const string AvailableExecsGridServiceKey = "AvailableExecs";
        public const string MrDetailsGridServiceKey = "MrDetails";

        /// <summary>
        /// Creates a new instance of MonitorResultViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static MonitorResultViewModel Create(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MonitorResultViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the MonitorResultViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the MonitorResultViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public MonitorResultViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.MonitorResults)
        {
        }

        protected override Model.MonitorResult CreateEntity()
        {
            var monitorResult = base.CreateEntity();
            monitorResult.ObjectType = Model.VisitObjectType.PPE;
            return monitorResult;
        }

        protected override string LocalizedEntityName => "Результат мониторинга";

        protected override void OnBeforeEntitySaved(int primaryKey, Model.MonitorResult entity, bool isNewEntity)
        {
            base.OnBeforeEntitySaved(primaryKey, entity, isNewEntity);
            initialized = false;
            detailsManipulation = false;
        }

        protected override void UpdateCommands()
        {
            base.UpdateCommands();
            this.RaiseCanExecuteChanged(x => x.AddSelectedExecs());
            this.RaiseCanExecuteChanged(x => x.RemoveSelectedDetails());
            this.RaiseCanExecuteChanged(x => x.SaveAndNewOverride());
        }

        private bool initialized = false;

        protected override void OnEntityChanged()
        {
            base.OnEntityChanged();

            Debug.Assert(Entity != null);

            var schedule = Entity?.Schedule;
            if (schedule != null)
            {
                PeriodType = schedule.PeriodType;
                RegionId = schedule.RegionId;
                ExpertKindOfficial = schedule.Expert.Kind == Model.ExpertKind.Official;
                ExpertKindSocial = schedule.Expert.Kind == Model.ExpertKind.Social;
                UpdateDates(false);
            }

            var scheduleDetails = Entity?.ScheduleDetail;
            if (scheduleDetails != null)
            {
                UpdateDetails();
                UpdateExecs();
            }

            // TODO:

            RefreshLookUpCollection((MonitorResultViewModel vm) => vm.SchedulesLookup, true);

            initialized = true;
        }

        #region Lookup collections

        public IEntitiesViewModel<Model.Region> RegionsLookup
        {
            get
            {
                return GetLookUpProjectEntitiesViewModel((MonitorResultViewModel vm) => vm.RegionsLookup, uw => uw.Regions);
            }
        }

        #region SchedulesLookup

        public IEntitiesViewModel<Model.Schedule> SchedulesLookup
        {
            get
            {
                return GetLookUpEntitiesViewModel((MonitorResultViewModel vm) => vm.SchedulesLookup,
                                                  uw => uw.Schedules,
                                                  SchedulesLookupProjection);
            }
        }

        private IQueryable<Model.Schedule> SchedulesLookupProjection(IRepositoryQuery<Model.Schedule> query)
        {
            if (PeriodType == null || RegionId == null)
            {
                return Enumerable.Empty<Model.Schedule>().AsQueryable();
            }

            var q = query.Where(_ => _.PeriodType == PeriodType && _.RegionId == RegionId);

            if (ExpertKindOfficial ^ ExpertKindSocial)
            {
                if (ExpertKindOfficial)
                {
                    q = q.Where(_ => _.Expert.Kind == Model.ExpertKind.Official);
                }
                else
                {
                    q = q.Where(_ => _.Expert.Kind == Model.ExpertKind.Social);
                }
            }

            return q;
        }

        private void ClearAndRefreshSchedules()
        {
            // ScheduleId = null;
            RefreshLookUpCollection((MonitorResultViewModel vm) => vm.SchedulesLookup, true);
        }

        #endregion

        public IEntitiesViewModel<Model.Violation> ViolationsLookup
        {
            get
            {
                return GetLookUpEntitiesViewModel((MonitorResultViewModel vm) => vm.ViolationsLookup,
                    uw => uw.Violations);
            }
        }

        public IEntitiesViewModel<Model.Violator> ViolatorsLookup
        {
            get
            {
                return GetLookUpEntitiesViewModel((MonitorResultViewModel vm) => vm.ViolatorsLookup, uw => uw.Violators);
            }
        }

        #region ScheduleDetailsLookup

        public IEntitiesViewModel<Model.ScheduleDetail> ScheduleDetailsLookup
        {
            get
            {
                return GetLookUpEntitiesViewModel((MonitorResultViewModel vm) => vm.ScheduleDetailsLookup,
                                                  uw => uw.ScheduleDetails,
                                                  ScheduleDetailsLookupProjection);
            }
        }

        private IQueryable<Model.ScheduleDetail> ScheduleDetailsLookupProjection(IRepositoryQuery<Model.ScheduleDetail> query)
        {
            if ((ScheduleId ?? 0) == 0)
            {
                return Enumerable.Empty<Model.ScheduleDetail>().AsQueryable();
            }

            return query.Include(x => x.Station).Where(d => d.ScheduleId == ScheduleId && d.ExamDate == Date);
        }

        #endregion

        #endregion

        #region RegionId

        private int? regionId;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnRegionIdChanged))]
        public virtual int? RegionId
        {
            get { return regionId; }
            set
            {
                if (regionId != value)
                {
                    regionId = value;
                    this.RaisePropertyChanged(x => x.RegionId);
                }
            }
        }

        protected void OnRegionIdChanged()
        {
            if (!initialized)
                return;

            ClearAndRefreshSchedules();
            Update();
        }

        #endregion

        #region ScheduleId

        [Required]
        [BindableProperty(OnPropertyChangedMethodName = nameof(OnScheduleIdChanged))]
        public virtual int? ScheduleId
        {
            get
            {
                return Entity?.ScheduleId;

            }
            set
            {
                if (Entity != null && Entity.ScheduleId != value)
                {
                    Entity.ScheduleId = value ?? 0;
                    this.RaisePropertyChanged(x => x.ScheduleId);
                }
            }
        }

        protected void OnScheduleIdChanged()
        {
            if (!initialized)
                return;

            Entity.Schedule = UnitOfWork.Schedules.Find(ScheduleId ?? 0);

            ScheduleDetailId = null;
            Date = null;
            //RefreshLookUpCollection((MonitorResultViewModel vm) => vm.ScheduleDetailsLookup, true);
            UpdateDates();
            Update();
        }

        #endregion

        #region Dates

        private readonly List<DateTime> dates = new List<DateTime>();

        public virtual List<DateTime> Dates
        {
            get { return dates; }
            protected set { }
        }

        public void UpdateDates(bool clearSelectedValue = true)
        {
            if (clearSelectedValue)
            {
                Entity.Date = DateTime.MinValue;
            }
            dates.Clear();
            if (Entity.Schedule == null)
                return;
            dates.AddRange(Entity.Schedule.ScheduleDetails.Select(x => x.ExamDate).Distinct().OrderBy(x => x));
            this.RaisePropertyChanged(x => x.Dates);
        }

        #endregion

        #region Date

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnDateChanged))]
        public virtual DateTime? Date
        {
            get { return Entity?.Date != DateTime.MinValue ? Entity?.Date : null; }
            set
            {
                if (Entity != null && Entity.Date != value)
                {
                    Entity.Date = (value ?? DateTime.MinValue).Date;
                    this.RaisePropertyChanged(x => x.Date);
                }
            }
        }

        protected void OnDateChanged()
        {
            if (!initialized)
                return;

            ScheduleDetailId = null;
            RefreshLookUpCollection((MonitorResultViewModel vm) => vm.ScheduleDetailsLookup, true);
            Update();
        }

        #endregion

        #region ViolationId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnViolationIdChanged))]
        public virtual int? ViolationId
        {
            get { return Entity?.ViolationId; }
            set
            {
                if (Entity != null && Entity.ViolationId != value)
                {
                    Entity.ViolationId = value ?? 0;
                    this.RaisePropertyChanged(x => x.ViolationId);
                }
            }
        }

        protected void OnViolationIdChanged()
        {
            if (!initialized)
                return;

            ViolatorId = null;
            Update();
            // TODO: refresh violators filter
        }

        #endregion

        #region ViolatorId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnViolatorIdChanged))]
        public virtual int? ViolatorId
        {
            get { return Entity?.ViolatorId; }
            set
            {
                if (Entity != null && Entity.ViolatorId != value)
                {
                    Entity.ViolatorId = value ?? 0;
                    this.RaisePropertyChanged(x => x.ViolatorId);
                }
            }
        }

        protected void OnViolatorIdChanged()
        {
            if (!initialized)
                return;

            Update();
            // TODO:
        }

        #endregion

        #region ExpertKind

        #region ExpertKindOfficial

        private bool expertKindOfficial;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpertKindChanged))]
        public virtual bool ExpertKindOfficial
        {
            get { return expertKindOfficial; }
            set
            {
                if (expertKindOfficial != value)
                {
                    expertKindOfficial = value;
                    this.RaisePropertyChanged(x => x.ExpertKindOfficial);
                }
            }
        }

        #endregion

        #region ExpertKindSocial

        private bool expertKindSocial;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExpertKindChanged))]
        public virtual bool ExpertKindSocial
        {
            get { return expertKindSocial; }
            set
            {
                if (expertKindSocial != value)
                {
                    expertKindSocial = value;
                    this.RaisePropertyChanged(x => x.ExpertKindSocial);
                }
            }
        }

        #endregion

        protected void OnExpertKindChanged()
        {
            if (!initialized)
                return;

            ClearAndRefreshSchedules();
        }

        #endregion

        #region ObjectType

        private Model.VisitObjectType objectType;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnObjectTypeChanged))]
        public virtual Model.VisitObjectType? ObjectType
        {
            get { return Entity?.ObjectType; }
            set
            {
                if (Entity != null && Entity.ObjectType != value)
                {
                    Entity.ObjectType = value.GetValueOrDefault(Model.VisitObjectType.PPE);
                    this.RaisePropertyChanged(x => x.ObjectType);
                }
            }
        }

        protected void OnObjectTypeChanged()
        {
            if (!initialized)
                return;

            Update();
            ScheduleDetailId = null;
            this.RaisePropertyChanged(x => x.DetailsReadOnly);
            // TODO:
        }

        #endregion

        #region Execs

        private readonly ObservableCollection<Model.Exec> execs = new ObservableCollection<Model.Exec>();

        public ObservableCollection<Model.Exec> Execs => execs;

        private void UpdateExecs()
        {
            try
            {
                execs.Clear();

                if ((ScheduleDetailId ?? 0) == 0)
                    return;

                var projectId = Entity?.Schedule?.Expert?.ProjectId ?? -1;
                var query = UnitOfWork.Execs
                    .Include(x => x.Exam)
                    .Include(x => x.Exam.Subject)
                    .Where(
                        x =>
                            x.Exam.ExamDate == Date &&
                            x.Exam.PeriodType == PeriodType &&
                            x.Exam.ExamType == ExamType &&
                            x.Exam.Subject.ProjectId == projectId &&
                            x.StationId == Entity.ScheduleDetail.StationId &&
                            x.Exam.Subject.ProjectId == projectId);

                var included = Entity.MrDetails.Select(x => x.Exec).ToList();

                query.ToList().Except(included).ForEach(execs.Add);
            }
            finally
            {
                this.GetService<IUpdateGridService>(AvailableExecsGridServiceKey)?.Update();
            }
        }

        #endregion

        private readonly ObservableCollection<Model.MrDetail> details = new ObservableCollection<Model.MrDetail>();

        /// <summary>
        /// Подмножество <c>Entity.MrDetails</c>, которое показывается в UI в соответсвие с выбранными фильтрами.
        /// </summary>
        public ObservableCollection<Model.MrDetail> Details => details;

        private void UpdateDetails()
        {
            details.Clear();

            Entity.MrDetails.ForEach(details.Add);

            this.RaisePropertyChanged(x => x.ExamTypeReadOnly);

            //UpdateDetails_end:
            this.GetService<IUpdateGridService>(MrDetailsGridServiceKey)?.Update();
        }

        #region DetailsReadOnly

        public virtual bool DetailsReadOnly
        {
            get { return Model.VisitObjectType.PPE != (ObjectType ?? Model.VisitObjectType.PPE); }
            protected set { }
        }

        #endregion

        #region ScheduleDetailId

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnScheduleDetailIdChanged))]
        public virtual int? ScheduleDetailId
        {
            get { return Entity?.ScheduleDetailId; }
            set
            {
                if (Entity != null && Entity.ScheduleDetailId != value)
                {
                    Entity.ScheduleDetailId = value;
                    this.RaisePropertyChanged(x => x.ScheduleDetailId);
                }
            }
        }

        protected void OnScheduleDetailIdChanged()
        {
            if (!initialized)
                return;

            Entity.ScheduleDetail = UnitOfWork.ScheduleDetails.Find(ScheduleDetailId ?? 0);

            // TODO: Warn user somehow

            ClearMrDetails();
            UpdateExecs();
            UpdateDetails();
            Update();
        }

        private void ClearMrDetails()
        {
            var detList = Entity.MrDetails.ToList();
            if (detList.Count > 0)
            {
                while (detList.Count > 0)
                {
                    var mr = details[0];
                    detList.RemoveAt(0);
                    UnitOfWork.MrDetails.Remove(mr);
                }
                detailsManipulation = true;
            }
        }

        #endregion

        #region PeriodType

        private Model.PeriodType? periodType;

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnPeriodTypeChanged))]
        public virtual Model.PeriodType? PeriodType
        {
            get { return periodType; }
            set
            {
                if (periodType != value)
                {
                    periodType = value;
                    this.RaisePropertyChanged(x => x.PeriodType);
                }
            }
        }

        protected void OnPeriodTypeChanged()
        {
            if (!initialized)
                return;

            ClearAndRefreshSchedules();
            Update();
        }

        #endregion

        #region ExamType

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnExamTypeChanged))]
        public virtual Model.ExamType? ExamType
        {
            get { return Entity?.ExamType; }
            set
            {
                if (Entity != null && Entity.ExamType != value)
                {
                    Entity.ExamType = value;
                    this.RaisePropertyChanged(x => x.ExamType);
                }
            }
        }

        protected void OnExamTypeChanged()
        {
            // TODO: Warn user
            ClearMrDetails();
            UpdateExecs();
            UpdateDetails();
            Update();
        }

        #endregion

        private bool detailsManipulation = false;

        public virtual Model.Exec[] SelectedExecs { get; set; }

        protected void OnSelectedExecsChanged()
        {
            Update();
        }

        public virtual Model.MrDetail[] SelectedDetails { get; set; }

        protected void OnSelectedDetailsChanged()
        {
            Update();
        }

        public void AddSelectedExecs()
        {
            Debug.Assert(Date != null);
            Debug.Assert(SelectedExecs != null);

            foreach (var exec in SelectedExecs)
            {
                if (Entity.MrDetails.All(d => d.ExecId != exec.Id))
                {
                    var detail = UnitOfWork.MrDetails.Create(true);
                    detail.MonitorResultId = Entity.Id;
                    detail.ExecId = exec.Id;
                    detail.Exec = exec;

                    Entity.MrDetails.Add(detail);
                    detailsManipulation = true;
                }
            }

            SelectedExecs = null;

            UpdateExecs();
            UpdateDetails();
            Update();
        }

        public bool CanAddSelectedExecs()
        {
            return !ReadOnly && (SelectedExecs?.Length ?? 0) > 0;
        }

        public void RemoveSelectedDetails()
        {
            Debug.Assert(SelectedDetails != null);

            foreach (var detail in SelectedDetails)
            {
                Entity.MrDetails.Remove(detail);
                UnitOfWork.MrDetails.Remove(detail);
                detailsManipulation = true;
            }

            SelectedDetails = null;

            UpdateExecs();
            UpdateDetails();
            Update();
        }

        public bool CanRemoveSelectedDetails()
        {
            return !ReadOnly && (SelectedDetails?.Length ?? 0) > 0;
        }

        #region ExamTypeReadOnly

        /// <summary>
        /// Запрет на изменение фильтра «Форма ГИА» если уже что-то добавили в <c>Entity.MrDetails</c>
        /// </summary>
        public bool ExamTypeReadOnly => Entity != null && Entity.MrDetails.Any();

        #endregion

        /// <summary>
        /// Saves changes in the underlying unit of work and create new entity.
        /// Since SingleObjectViewModelBase is a POCO view model, an instance of this class will also expose the SaveAndNewCommand property that can be used as a binding source in views.
        /// </summary>
        [Command(CanExecuteMethodName = "CanSave")]
        public void SaveAndNewOverride()
        {
            var schedule = Entity.Schedule;
            var examType = Entity.ExamType;
            var date = Entity.Date;
            var note = Entity.Note;
            var violation = Entity.Violation;
            var violator = Entity.Violator;
            var objType = Entity.ObjectType;

            if (SaveCore())
                CreateAndInitializeEntity(e =>
                {
                    e.ScheduleId = schedule.Id;
                    e.Date = date;
                    e.ExamType = examType;
                    e.Note = note;
                    e.ViolationId = violation?.Id ?? 0;
                    e.ViolatorId = violator?.Id ?? 0;
                    e.ObjectType = objType;
                });
        }

        protected override string SDeleteOperation => SecurityOperations.MonitorResults.Delete;

        protected override string SNewOperation => SecurityOperations.MonitorResults.New;

        protected override string SEditOperation => SecurityOperations.MonitorResults.Edit;
    }
}
