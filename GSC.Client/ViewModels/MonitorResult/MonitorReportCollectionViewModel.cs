﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraPrinting.Native;
using Gsc.Client.Util;
using GSC.Client.Common;
using GSC.Client.Common.DataModel;
using GSC.Client.Common.Utils;
using GSC.Client.Common.ViewModel;
using GSC.Client.GscContextDataModel;
using GSC.Client.Security;
using GSC.Client.Services;
using GSC.Client.Util;

namespace GSC.Client.ViewModels.MonitorResult
{
    /// <summary>
    /// Represents the Schedules (for MonitorReport view) collection view model.
    /// </summary>
    public class MonitorReportCollectionViewModel 
        : ReadOnlyCollectionViewModel<Model.Schedule, MonitorReportsHolderInfo, IGscContextUnitOfWork>
        , ISecureViewModel
    {
        private string saveFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        /// <summary>
        /// Creates a new instance of MonitorResultViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static MonitorReportCollectionViewModel Create(
            IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new MonitorReportCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the MonitorReportViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the MonitorReportViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public MonitorReportCollectionViewModel(IUnitOfWorkFactory<IGscContextUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(),
                x => x.Schedules,
                MonitorReportsHolderInfo.GetProjection)
        {
            var projectService = this.GetRequiredService<IProjectService>();
            var project = projectService.GetCurrentProject().Match(p => p.Id, e => { throw e; });

            FilterExpression = s => s.Expert.ProjectId == project;
        }

        public virtual void UploadFile()
        {
            this.EnsureOperationAllowed(SecurityOperations.MonitorReports.UploadFile);

            var ofd = this.OpenFileDialogService;

            var support = ofd as ISupportMultiselect;
            if (support != null)
                support.Multiselect = true;

            if (ofd.ShowDialog())
            {
                var uw = CreateUnitOfWork();

                foreach (var fileInfo in ofd.Files)
                {
                    var doc = uw.MrDocs.Create();
                    doc.ScheduleId = SelectedEntity.Schedule.Id;
                    doc.Filename = fileInfo.Name;
                    doc.Timestamp = DateTime.Now; // will be overwritten by insert-trigger on db

                    var path = fileInfo.GetFullName();
                    if (File.Exists(path))
                    {
                        doc.Blob = File.ReadAllBytes(path);
                    }
                }
                uw.SaveChanges();
            }

            RefillMrDocs();
        }

        public bool CanUploadFile()
        {
            return this.OperationAllowed(SecurityOperations.MonitorReports.UploadFile);
        }

        public void DownloadFile()
        {
            SecurityService.EnsureOperationAllowed(SecurityOperations.MonitorReports.DownloadFile);

            var uw = CreateUnitOfWork();
            var ofd = this.GetRequiredService<IOpenFolderDialogService>();

            ofd.InitialFolder = saveFolderPath;
            if (ofd.ShowDialog())
            {
                saveFolderPath = ofd.Folder;
                foreach (var doc in SelectedMrDocs)
                {
                    var doc2 = uw.MrDocs.Find(doc.Id);
                    DownloadFileInternal(doc2, saveFolderPath);
                }
            }
        }

        private string DownloadFileInternal(Model.MrDoc doc, string toFolder)
        {
            // TODO: Extract to service
            if (doc?.Blob != null)
            {
                var savePath = GuardFileNameOnSave(toFolder, doc.Filename);
                File.WriteAllBytes(savePath, doc.Blob);
                return savePath;
            }
            return null;
        }

        private string GuardFileNameOnSave(string folder, string baseFileName)
        {
            // TODO: Extract to service
            var savePath = Path.Combine(folder, baseFileName);

            for (int counter = 1; counter <= 20 && File.Exists(savePath); ++counter)
            {
                savePath = Path.Combine(folder,
                    string.Format("{0} ({1}){2}",
                        Path.GetFileNameWithoutExtension(baseFileName),
                        counter,
                        Path.GetExtension(baseFileName)));
            }

            return savePath;
        }

        public bool CanDownloadFile()
        {
            return HasSelection && SecurityService.OperationAllowed(SecurityOperations.MonitorReports.DownloadFile);
        }

        public void DeleteFile()
        {
            this.EnsureOperationAllowed(SecurityOperations.MonitorReports.DeleteFile);

            try
            {
                var uw = CreateUnitOfWork();
                foreach (var doc in SelectedMrDocs)
                {
                    var x = uw.MrDocs.Find(doc.Id);
                    if (x != null)
                    {
                        uw.MrDocs.Remove(x);
                        MrDocs.Remove(doc);
                    }
                }

                uw.SaveChanges();
            }
            finally
            {
                UpdateGrid();
                SelectedMrDocs = new MrDocInfo[0];
            }
        }

        public bool CanDeleteFile()
        {
            return HasSelection && this.OperationAllowed(SecurityOperations.MonitorReports.DeleteFile);
        }


        public void PreviewFile()
        {
            this.EnsureReadOnlyOperationAllowed(SecurityOperations.MonitorReports.PreviewFile);

            // TODO: Extract to service

            var uw = CreateUnitOfWork();

            var temp = Path.GetTempPath();
            foreach (var doc in SelectedMrDocs)
            {
                var x = uw.MrDocs.Find(doc.Id);
                var path = DownloadFileInternal(x, temp);

                if (!string.IsNullOrEmpty(path))
                {
                    RunFile(path);
                }
            }
        }

        public bool CanPreviewFile()
        {
            return HasSelection && this.ReadOnlyOperationAllowed(SecurityOperations.MonitorReports.PreviewFile);
        }

        private void RunFile(string path)
        {
            // TODO: Extract to service
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
            {
                FileName = path,
                UseShellExecute = true,
                Verb = "open"
            });
        }

        public override void Refresh()
        {
            base.Refresh();
            UpdateCommands();
        }

        private void UpdateGrid()
        {
            var gridUpdate = this.GetService<IUpdateGridService>();
            gridUpdate.Update();
        }

        protected override void OnSelectedEntityChanged()
        {
            base.OnSelectedEntityChanged();
            RefillMrDocs();
            UpdateGrid();
            UpdateCommands();
        }

        [BindableProperty(OnPropertyChangedMethodName = nameof(OnSelectedMrDocsChanged))]
        public virtual MrDocInfo[] SelectedMrDocs { get; set; }

        protected void OnSelectedMrDocsChanged()
        {
            UpdateCommands();
        }

        private bool HasSelection
        {
            get { return SelectedMrDocs != null && SelectedMrDocs.Length > 0; }
        }

        private readonly BindingList<MrDocInfo> mrDocsList = new BindingList<MrDocInfo>();

        public BindingList<MrDocInfo> MrDocs
        {
            get { return mrDocsList; }
        }

        private void RefillMrDocs()
        {
            mrDocsList.Clear();

            if (SelectedEntity != null)
            {
                var uw = CreateUnitOfWork();
                var docs = MrDocInfo.GetProjection(uw.MrDocs.Where(d => d.ScheduleId == SelectedEntity.Schedule.Id))
                    .ToList();
                docs.ForEach(mrDocsList.Add);
            }
        }

        private void UpdateCommands()
        {
            this.RaiseCanExecuteChanged(x => x.UploadFile());
            this.RaiseCanExecuteChanged(x => x.DownloadFile());
            this.RaiseCanExecuteChanged(x => x.DeleteFile());
            this.RaiseCanExecuteChanged(x => x.PreviewFile());
            this.RaiseCanExecuteChanged(x => x.Refresh());
        }
    }
}
