//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GSC.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class MrDetail
    {
        public int Id { get; set; }
        public int MonitorResultId { get; set; }
        public int ExecId { get; set; }
        public System.DateTime Ts { get; set; }
    
        public virtual Exec Exec { get; set; }
        public virtual MonitorResult MonitorResult { get; set; }
    }
}
