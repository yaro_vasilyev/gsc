//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GSC.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Station
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Station()
        {
            this.Execs = new HashSet<Exec>();
        }
    
        public int Id { get; set; }
        public int RegionId { get; set; }
        public int ProjectId { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public GSC.Model.BooleanType Tom { get; set; }
        public Nullable<GSC.Model.ExamType> ExamType { get; set; }
        public System.DateTime Ts { get; set; }
    
        public virtual Project Project { get; set; }
        public virtual Region Region { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Exec> Execs { get; set; }
    }
}
