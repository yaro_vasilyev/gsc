﻿namespace ExecXmlToSQL
{
    partial class frmExecConvert
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbXmlFile = new System.Windows.Forms.TextBox();
            this.butLoadXml = new System.Windows.Forms.Button();
            this.tbSqlFile = new System.Windows.Forms.TextBox();
            this.butSelectSql = new System.Windows.Forms.Button();
            this.butGo = new System.Windows.Forms.Button();
            this.tbGuid = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbXmlFile
            // 
            this.tbXmlFile.Location = new System.Drawing.Point(12, 11);
            this.tbXmlFile.Name = "tbXmlFile";
            this.tbXmlFile.Size = new System.Drawing.Size(502, 20);
            this.tbXmlFile.TabIndex = 0;
            // 
            // butLoadXml
            // 
            this.butLoadXml.Location = new System.Drawing.Point(520, 9);
            this.butLoadXml.Name = "butLoadXml";
            this.butLoadXml.Size = new System.Drawing.Size(75, 23);
            this.butLoadXml.TabIndex = 1;
            this.butLoadXml.Text = "Load XML";
            this.butLoadXml.UseVisualStyleBackColor = true;
            this.butLoadXml.Click += new System.EventHandler(this.butLoadXml_Click);
            // 
            // tbSqlFile
            // 
            this.tbSqlFile.Location = new System.Drawing.Point(13, 44);
            this.tbSqlFile.Name = "tbSqlFile";
            this.tbSqlFile.Size = new System.Drawing.Size(501, 20);
            this.tbSqlFile.TabIndex = 2;
            // 
            // butSelectSql
            // 
            this.butSelectSql.Location = new System.Drawing.Point(520, 35);
            this.butSelectSql.Name = "butSelectSql";
            this.butSelectSql.Size = new System.Drawing.Size(75, 37);
            this.butSelectSql.TabIndex = 3;
            this.butSelectSql.Text = "Select SQL file";
            this.butSelectSql.UseVisualStyleBackColor = true;
            this.butSelectSql.Click += new System.EventHandler(this.butSelectSql_Click);
            // 
            // butGo
            // 
            this.butGo.Location = new System.Drawing.Point(520, 78);
            this.butGo.Name = "butGo";
            this.butGo.Size = new System.Drawing.Size(75, 23);
            this.butGo.TabIndex = 4;
            this.butGo.Text = "GO!";
            this.butGo.UseVisualStyleBackColor = true;
            this.butGo.Click += new System.EventHandler(this.butGo_Click);
            // 
            // tbGuid
            // 
            this.tbGuid.Location = new System.Drawing.Point(13, 78);
            this.tbGuid.Name = "tbGuid";
            this.tbGuid.Size = new System.Drawing.Size(501, 20);
            this.tbGuid.TabIndex = 5;
            // 
            // frmExecConvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 468);
            this.Controls.Add(this.tbGuid);
            this.Controls.Add(this.butGo);
            this.Controls.Add(this.butSelectSql);
            this.Controls.Add(this.tbSqlFile);
            this.Controls.Add(this.butLoadXml);
            this.Controls.Add(this.tbXmlFile);
            this.Name = "frmExecConvert";
            this.Text = "Convert Execs from XML to SQL inserts";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbXmlFile;
        private System.Windows.Forms.Button butLoadXml;
        private System.Windows.Forms.TextBox tbSqlFile;
        private System.Windows.Forms.Button butSelectSql;
        private System.Windows.Forms.Button butGo;
        private System.Windows.Forms.TextBox tbGuid;
    }
}

