﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using GSC.Client.ViewModels.Exec;
using GSC.Model;

namespace ExecXmlToSQL
{
    public partial class frmExecConvert : Form
    {
        private List<ExecCollectionViewModel.XmlExecution> xmlExecutions;
        private string sqlFileName;

        public frmExecConvert()
        {
            InitializeComponent();
        }

        private void butLoadXml_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = @"Файлы XML|*.xml|Все файлы|*.*"
            };
            if (openFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            tbXmlFile.Text = openFileDialog.FileName;

            var serializer = new XmlSerializer(typeof (List<ExecCollectionViewModel.XmlExecution>),
                new XmlRootAttribute("Executions"));
            using (var streamReader = new StreamReader(openFileDialog.FileName))
            {
                xmlExecutions = serializer.Deserialize(streamReader) as List<ExecCollectionViewModel.XmlExecution>;
            }

            Debug.Assert(xmlExecutions != null, "xmlExecutions != null");
            MessageBox.Show($"Строк: {xmlExecutions.Count}");
        }

        private void butSelectSql_Click(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = @"Файлы SQL|*.sql|Все файлы|*.*",
                CheckFileExists = false
            };
            if (saveFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            tbSqlFile.Text = sqlFileName  = saveFileDialog.FileName;
        }

        private void butGo_Click(object sender, EventArgs e)
        {
            using (var streamWriter = new StreamWriter(sqlFileName))
            {
                var guid = Guid.NewGuid();

                tbGuid.Text = guid.ToString();
                streamWriter.WriteLine("BEGIN;");

                //var i = 0;
                foreach (var row in xmlExecutions)
                {
                    var kim = BooleanType.Yes.Equals(row.Kim) ? 1 : 0;
                    var str =
                        $"\tSELECT * FROM gsc.import_row_exec(\'{guid}\', {row.Region}, {row.StationCode}, {row.ExamGlobalId}, {kim}, {row.CountPart}, {row.CountRoom}, {row.CountRoomVideo}, {(int)row.ExamType});";

                    streamWriter.WriteLine(str);

                    /*if (Math. ++i)
                    {
                        break;
                    }*/
                }
                streamWriter.WriteLine("END;");
            }

            MessageBox.Show("Готово!");
        }
    }
}