﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Optional;
using Optional.Unsafe;

namespace GSC.Client.Tests
{
    [TestFixture]
    public class OptionTests
    {
        private readonly IItemService itemService = new ItemService();

        [Test]
        public void TestSimpleHasValue()
        {
            var ret = itemService.GetSome(true);
            Assert.IsTrue(ret.HasValue);
        }

        [Test]
        public void TestSimpleHasValueInverted ()
        {
            var ret = itemService.GetSome(false);
            Assert.IsFalse(ret.HasValue);
        }

        [Test]
        public void TestValueOr()
        {
            var defaultItem = new Item();
            var item = itemService.GetSome(true).ValueOr(defaultItem);
            Assert.AreNotEqual(defaultItem, item);
        }

        [Test]
        public void TestValueOrInverted()
        {
            var defaultItem = new Item();
            var item = itemService.GetSome(false).ValueOr(defaultItem);
            Assert.AreEqual(defaultItem, item);
        }

        [Test]
        public void TestFlatMap()
        {
            var id = itemService.GetSome(true).FlatMap(item => item.Id.Some());
            Assert.AreEqual(4, id.ValueOr(3));
        }

        [Test]
        public void TestFlatMapInvereted()
        {
            var id = itemService.GetSome(false).FlatMap(item => item.Id.Some()); // should be 4 if true, but false
            Assert.AreEqual(3, id.ValueOr(3));
        }

        [Test]
        public void Test()
        {
            var item2 = new Item {Id = 2};
            var two = itemService.GetSome(false).Else(item2.Some());
            Assert.IsTrue(two.HasValue);
            Assert.AreEqual(item2, two.ValueOrFailure());
        }

    }

    public class Item
    {
        public int Id { get; set; }
    }

    public interface IItemService
    {
        Option<Item> GetSome(bool flag);
    }

    internal class ItemService : IItemService
    {
        public Option<Item> GetSome(bool doReturn)
        {
            return doReturn
                ? Option.Some(new Item {Id = 4})
                : Option.None<Item>();
        }
    }
}
