﻿using System.Collections.Generic;
using System.Linq;
using CsvHelper.Configuration;
using GSC.Client.Util;
using JetBrains.Annotations;
using NUnit.Framework;

namespace GSC.Client.Tests
{
    [TestFixture]
    public class CsvImporterTests
    {
        [UsedImplicitly]
        public class SimpleEntity
        {
            public int IntField { get; set; }

            public string StringField { get; set; }

            public override string ToString()
            {
                return $"{{int={IntField},string={StringField}}}";
            }
        }

        [Test]
        public void TestImportRows()
        {
            var importList = new List<SimpleEntity>();
            var csvImporter = new CsvImporter<SimpleEntity, SimpleCsvMap, string>(
                entity => { importList.Add(entity); }, () => importList.Select(entity => entity.ToString()).ToList());
            string faultsFileName;
            var import = csvImporter.Import(@"Tests\CsvImportData.csv", out faultsFileName);

            Assert.That(import.Count(), Is.EqualTo(4));
            Assert.That(faultsFileName, Is.Null);
        }
    }

    [UsedImplicitly]
    public sealed class SimpleCsvMap : CsvClassMap<CsvImporterTests.SimpleEntity>
    {
        public SimpleCsvMap()
        {
            Map(entity => entity.IntField).Name("int");
            Map(entity => entity.StringField).Name("string");
        }
    }
}