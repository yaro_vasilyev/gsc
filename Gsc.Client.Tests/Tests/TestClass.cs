﻿using NUnit.Framework;

namespace Gsc.Client.Tests
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void AlwaysPass() { Assert.That(1, Is.EqualTo(1)); }
    }
}
