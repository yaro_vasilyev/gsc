@echo off
set psql=c:\Program Files\PostgreSQL\9.5\bin\
echo Default path is "%psql%". Enter other or leave blank for default.
set /p psql="Path to PostgreSQL: "
"%psql%psql.exe" < gsc.sql --dbname=gsc --username=postgres
pause