--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = gsc, pg_catalog;

ALTER TABLE IF EXISTS ONLY gsc.schedule DROP CONSTRAINT IF EXISTS schedule_region_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.schedule DROP CONSTRAINT IF EXISTS schedule_period_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.schedule DROP CONSTRAINT IF EXISTS schedule_expert_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.schedule_detail DROP CONSTRAINT IF EXISTS schedule_detail_schedule_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.schedule_detail DROP CONSTRAINT IF EXISTS schedule_detail_ppe_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.rcoi DROP CONSTRAINT IF EXISTS rcoi_region_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.ppe DROP CONSTRAINT IF EXISTS ppe_subject_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.ppe DROP CONSTRAINT IF EXISTS ppe_region_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.ppe DROP CONSTRAINT IF EXISTS ppe_project_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.oiv_gek DROP CONSTRAINT IF EXISTS oiv_gek_region_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_violator_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_violation_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_subject_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_schedule_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_ppe_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_period_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_observer_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_base_region_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_academic_title_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_academic_degree_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.exam DROP CONSTRAINT IF EXISTS exam_subject_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.exam DROP CONSTRAINT IF EXISTS exam_period_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.event DROP CONSTRAINT IF EXISTS event_schedule_id_fkey;
ALTER TABLE IF EXISTS ONLY gsc.event DROP CONSTRAINT IF EXISTS event_monitor_result_id_fkey;
DROP TRIGGER IF EXISTS expert_ts ON gsc.expert;
ALTER TABLE IF EXISTS ONLY gsc.violator DROP CONSTRAINT IF EXISTS violator_pkey;
ALTER TABLE IF EXISTS ONLY gsc.violation DROP CONSTRAINT IF EXISTS violation_pkey;
ALTER TABLE IF EXISTS ONLY gsc.subject DROP CONSTRAINT IF EXISTS subject_pkey;
ALTER TABLE IF EXISTS ONLY gsc.subject DROP CONSTRAINT IF EXISTS subject_code_key;
ALTER TABLE IF EXISTS ONLY gsc.schedule DROP CONSTRAINT IF EXISTS schedule_pkey;
ALTER TABLE IF EXISTS ONLY gsc.schedule_detail DROP CONSTRAINT IF EXISTS schedule_detail_pkey;
ALTER TABLE IF EXISTS ONLY gsc.region DROP CONSTRAINT IF EXISTS region_pkey;
ALTER TABLE IF EXISTS ONLY gsc.rcoi DROP CONSTRAINT IF EXISTS rcoi_pkey;
ALTER TABLE IF EXISTS ONLY gsc.ppe DROP CONSTRAINT IF EXISTS ppe_pkey;
ALTER TABLE IF EXISTS ONLY gsc.period DROP CONSTRAINT IF EXISTS period_pkey;
ALTER TABLE IF EXISTS ONLY gsc.oiv_gek DROP CONSTRAINT IF EXISTS oiv_gek_pkey;
ALTER TABLE IF EXISTS ONLY gsc.observer DROP CONSTRAINT IF EXISTS observer_pkey;
ALTER TABLE IF EXISTS ONLY gsc.monitor_result DROP CONSTRAINT IF EXISTS monitor_result_pkey;
ALTER TABLE IF EXISTS ONLY gsc.imp_subject DROP CONSTRAINT IF EXISTS imp_subject_pkey;
ALTER TABLE IF EXISTS ONLY gsc.imp_contract_info DROP CONSTRAINT IF EXISTS imp_contract_info_key;
ALTER TABLE IF EXISTS ONLY gsc.imp_academic_title DROP CONSTRAINT IF EXISTS imp_academic_title_pkey;
ALTER TABLE IF EXISTS ONLY gsc.imp_academic_degree DROP CONSTRAINT IF EXISTS imp_academic_degree_pkey;
ALTER TABLE IF EXISTS ONLY gsc.expert DROP CONSTRAINT IF EXISTS expert_pkey;
ALTER TABLE IF EXISTS ONLY gsc.exam DROP CONSTRAINT IF EXISTS exam_pkey;
ALTER TABLE IF EXISTS ONLY gsc.event DROP CONSTRAINT IF EXISTS event_pkey;
ALTER TABLE IF EXISTS ONLY gsc.contract_info DROP CONSTRAINT IF EXISTS contract_info_pkey;
ALTER TABLE IF EXISTS ONLY gsc.contract_info DROP CONSTRAINT IF EXISTS contract_info_number_key;
ALTER TABLE IF EXISTS ONLY gsc.academic_title DROP CONSTRAINT IF EXISTS academic_title_pkey;
ALTER TABLE IF EXISTS ONLY gsc.academic_title DROP CONSTRAINT IF EXISTS academic_title_code_key;
ALTER TABLE IF EXISTS ONLY gsc.academic_degree DROP CONSTRAINT IF EXISTS academic_degree_pkey;
ALTER TABLE IF EXISTS ONLY gsc.academic_degree DROP CONSTRAINT IF EXISTS academic_degree_code_key;
ALTER TABLE IF EXISTS gsc.violator ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.violation ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.subject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.schedule_detail ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.schedule ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.rcoi ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.ppe ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.period ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.oiv_gek ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.observer ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.monitor_result ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.expert ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.exam ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.event ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.contract_info ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.academic_title ALTER COLUMN id DROP DEFAULT;
ALTER TABLE IF EXISTS gsc.academic_degree ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE IF EXISTS gsc.violator_id_seq;
DROP TABLE IF EXISTS gsc.violator;
DROP SEQUENCE IF EXISTS gsc.violation_id_seq;
DROP TABLE IF EXISTS gsc.violation;
DROP SEQUENCE IF EXISTS gsc.subject_id_seq;
DROP TABLE IF EXISTS gsc.subject;
DROP SEQUENCE IF EXISTS gsc.schedule_id_seq;
DROP SEQUENCE IF EXISTS gsc.schedule_detail_id_seq;
DROP TABLE IF EXISTS gsc.schedule_detail;
DROP TABLE IF EXISTS gsc.schedule;
DROP TABLE IF EXISTS gsc.region;
DROP SEQUENCE IF EXISTS gsc.region_id_seq;
DROP SEQUENCE IF EXISTS gsc.rcoi_id_seq;
DROP TABLE IF EXISTS gsc.rcoi;
DROP SEQUENCE IF EXISTS gsc.ppe_id_seq;
DROP TABLE IF EXISTS gsc.ppe;
DROP SEQUENCE IF EXISTS gsc.period_seq;
DROP SEQUENCE IF EXISTS gsc.oiv_gek_id_seq;
DROP TABLE IF EXISTS gsc.oiv_gek;
DROP SEQUENCE IF EXISTS gsc.observer_id_seq;
DROP TABLE IF EXISTS gsc.observer;
DROP SEQUENCE IF EXISTS gsc.monitor_result_id_seq;
DROP TABLE IF EXISTS gsc.monitor_result;
DROP TABLE IF EXISTS gsc.imp_subject;
DROP TABLE IF EXISTS gsc.imp_contract_info;
DROP TABLE IF EXISTS gsc.imp_academic_title;
DROP TABLE IF EXISTS gsc.imp_academic_degree;
DROP SEQUENCE IF EXISTS gsc.expert_id_seq;
DROP TABLE IF EXISTS gsc.expert;
DROP SEQUENCE IF EXISTS gsc.exam_id_seq;
DROP TABLE IF EXISTS gsc.exam;
DROP SEQUENCE IF EXISTS gsc.event_id_seq;
DROP TABLE IF EXISTS gsc.event;
DROP SEQUENCE IF EXISTS gsc.contract_info_id_seq;
DROP TABLE IF EXISTS gsc.contract_info;
DROP SEQUENCE IF EXISTS gsc.academic_title_id_seq;
DROP SEQUENCE IF EXISTS gsc.academic_degree_id_seq;
DROP FUNCTION IF EXISTS gsc.schedule_calendar_details(integer);
DROP FUNCTION IF EXISTS gsc.region_update(pcode integer, pname character varying, pcode_original integer, pname_original character varying);
DROP FUNCTION IF EXISTS gsc.region_insert(pcode integer, pname character varying);
DROP FUNCTION IF EXISTS gsc.region_delete(pcode integer, pname character varying);
DROP FUNCTION IF EXISTS gsc.period_update(pid integer, ptitle character varying, pstatus integer, pid_original integer, ptitle_original character varying, pstatus_orginal integer);
DROP FUNCTION IF EXISTS gsc.period_insert(ptitle character varying, pstatus integer, OUT pid integer);
DROP FUNCTION IF EXISTS gsc.period_delete(pid integer, ptitle character varying, pstatus integer);
DROP FUNCTION IF EXISTS gsc.import_subject(p_imp_session_guid uuid);
DROP FUNCTION IF EXISTS gsc.import_row_subject(imp_session_guid uuid, code integer, name character varying);
DROP FUNCTION IF EXISTS gsc.import_row_contract_info(imp_session_guid uuid, number character varying, contract_date date);
DROP FUNCTION IF EXISTS gsc.import_row_academic_title(imp_session_guid uuid, code character varying, name character varying);
DROP FUNCTION IF EXISTS gsc.import_row_academic_degree(imp_session_guid uuid, code character varying, name character varying);
DROP FUNCTION IF EXISTS gsc.import_contract_info(p_imp_session_guid uuid);
DROP FUNCTION IF EXISTS gsc.import_academic_title(p_imp_session_guid uuid);
DROP FUNCTION IF EXISTS gsc.import_academic_degree(p_imp_session_guid uuid);
DROP FUNCTION IF EXISTS gsc.f_period_test(p period);
DROP TABLE IF EXISTS gsc.period;
DROP FUNCTION IF EXISTS gsc.f_get_schedule_header(p_project_id integer);
DROP FUNCTION IF EXISTS gsc.f_expert_stage1_points();
DROP FUNCTION IF EXISTS gsc.f_expert_academic_title(at academic_title);
DROP TABLE IF EXISTS gsc.academic_title;
DROP FUNCTION IF EXISTS gsc.f_expert_academic_degree(ad academic_degree);
DROP TABLE IF EXISTS gsc.academic_degree;
DROP FUNCTION IF EXISTS gsc.expert_update_ts();
DROP FUNCTION IF EXISTS gsc.expert_update(p_id integer, new_import_code character varying, new_period_id integer, new_surname character varying, new_name character varying, new_patronymic character varying, new_workplace character varying, new_post character varying, new_post_value integer, new_birth_date date, new_birth_place character varying, new_base_region_id integer, new_live_address character varying, new_phone character varying, new_email character varying, new_expirience character varying, new_speciality character varying, new_speciality_value integer, new_work_years_total integer, new_work_years_education integer, new_work_years_chief integer, new_exp_years_education integer, new_awards character varying, new_can_mission integer, new_chief_name character varying, new_chief_post character varying, new_chief_email character varying, new_observer_id integer, new_kind integer, new_test_results integer, new_interview_status integer, new_subject_level_value integer, new_accuracy_level_value integer, new_punctual_level_value integer, new_independency_level_value integer, new_moral_level_value integer, new_docs_impr_part_level_value integer, new_academic_title_id integer, new_academic_degree_id integer, old_ts timestamp without time zone);
DROP FUNCTION IF EXISTS gsc.expert_insert(period_id integer, import_code character varying, surname character varying, name character varying, patronymic character varying, workplace character varying, post character varying, post_value integer, birth_date date, birth_place character varying, base_region_id integer, live_address character varying, phone character varying, email character varying, expirience character varying, speciality character varying, speciality_value integer, work_years_total integer, work_years_education integer, work_years_chief integer, exp_years_education integer, awards character varying, can_mission integer, chief_name character varying, chief_post character varying, chief_email character varying, observer_id integer, kind integer, test_results integer, interview_status integer, subject_level_value integer, accuracy_level_value integer, punctual_level_value integer, independency_level_value integer, moral_level_value integer, docs_impr_part_level_value integer, academic_title_id integer, academic_degree_id integer);
DROP FUNCTION IF EXISTS gsc.expert_delete(expert_id integer);
DROP FUNCTION IF EXISTS gsc.err_concurrency_control(p_entity text);
DROP FUNCTION IF EXISTS gsc.e_work_years_education(integer);
DROP FUNCTION IF EXISTS gsc.e_test_results(integer);
DROP FUNCTION IF EXISTS gsc.e_stage2_points(p_stage1_points integer, p_reports_count integer, p_subject_level integer, p_accuracy integer, p_punctual integer, p_independent integer, p_moral_level integer, p_docs_impr_part integer);
DROP FUNCTION IF EXISTS gsc.e_stage1_points(p_exp_years_education integer, p_speciality integer, p_work_years_education integer, p_can_mission integer, p_post integer, p_academic_degree integer, p_test_results integer, p_interview_status integer, p_academic_title integer, p_awards integer);
DROP FUNCTION IF EXISTS gsc.e_reports_count(integer);
DROP FUNCTION IF EXISTS gsc.e_next_period_proposal(integer);
DROP FUNCTION IF EXISTS gsc.e_interview_status(integer);
DROP FUNCTION IF EXISTS gsc.e_exp_years_education(integer);
DROP FUNCTION IF EXISTS gsc.e_engage(stage1_points integer, test_results integer, interview_status integer);
DROP FUNCTION IF EXISTS gsc.e_efficiency(integer);
DROP FUNCTION IF EXISTS gsc.e_can_mission(integer);
DROP FUNCTION IF EXISTS gsc.e_awards(character varying);
DROP FUNCTION IF EXISTS gsc.e_academic_title(integer);
DROP FUNCTION IF EXISTS gsc.e_academic_degree(integer);
DROP FUNCTION IF EXISTS gsc.cc_expert_reports_count(integer, integer);
DROP FUNCTION IF EXISTS gsc._clean_import();
DROP TYPE IF EXISTS gsc.import_status;
DROP DOMAIN IF EXISTS gsc.d_violation_location;
DROP DOMAIN IF EXISTS gsc.d_event_type;
DROP DOMAIN IF EXISTS gsc.d_channel_type;
DROP EXTENSION IF EXISTS "uuid-ossp";
DROP EXTENSION IF EXISTS tablefunc;
DROP EXTENSION IF EXISTS plpgsql;
DROP SCHEMA IF EXISTS public;
DROP SCHEMA IF EXISTS gsc;
--
-- Name: gsc; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA gsc;


ALTER SCHEMA gsc OWNER TO postgres;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = gsc, pg_catalog;

--
-- Name: d_channel_type; Type: DOMAIN; Schema: gsc; Owner: postgres
--

CREATE DOMAIN d_channel_type AS integer
	CONSTRAINT d_channel_type_check CHECK (((VALUE >= 1) AND (VALUE <= 5)));


ALTER DOMAIN d_channel_type OWNER TO postgres;

--
-- Name: d_event_type; Type: DOMAIN; Schema: gsc; Owner: postgres
--

CREATE DOMAIN d_event_type AS integer
	CONSTRAINT d_event_type_check CHECK ((VALUE = ANY (ARRAY[1, 2, 3, 4])));


ALTER DOMAIN d_event_type OWNER TO postgres;

--
-- Name: d_violation_location; Type: DOMAIN; Schema: gsc; Owner: postgres
--

CREATE DOMAIN d_violation_location AS integer
	CONSTRAINT d_violation_location_check CHECK ((VALUE = ANY (ARRAY[1, 2, 3, 4])));


ALTER DOMAIN d_violation_location OWNER TO postgres;

--
-- Name: import_status; Type: TYPE; Schema: gsc; Owner: postgres
--

CREATE TYPE import_status AS ENUM (
    'added',
    'imported',
    'error'
);


ALTER TYPE import_status OWNER TO postgres;

--
-- Name: TYPE import_status; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON TYPE import_status IS 'Import status for import (imp_*) tables.';


--
-- Name: _clean_import(); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION _clean_import() RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  truncate gsc.imp_academic_degree;
  truncate gsc.imp_academic_title;
  truncate gsc.imp_subject;
  truncate gsc.imp_contract_info;
  truncate gsc.contract_info;
END;
$$;


ALTER FUNCTION gsc._clean_import() OWNER TO postgres;

--
-- Name: cc_expert_reports_count(integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION cc_expert_reports_count(integer, integer) RETURNS integer
    LANGUAGE sql STABLE STRICT
    AS $_$

  -- $1 expert_id

  -- $2 period_id

  select 0;

$_$;


ALTER FUNCTION gsc.cc_expert_reports_count(integer, integer) OWNER TO postgres;

--
-- Name: e_academic_degree(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_academic_degree(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$

    select case when $1 is null then 0 else 5 end;

$_$;


ALTER FUNCTION gsc.e_academic_degree(integer) OWNER TO postgres;

--
-- Name: e_academic_title(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_academic_title(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$

    select default_val 

      from gsc.academic_title

     where id = $1;

$_$;


ALTER FUNCTION gsc.e_academic_title(integer) OWNER TO postgres;

--
-- Name: e_awards(character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_awards(character varying) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$

    select case when coalesce(trim($1), '') = '' then 0 else 5 end;

$_$;


ALTER FUNCTION gsc.e_awards(character varying) OWNER TO postgres;

--
-- Name: e_can_mission(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_can_mission(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$

    select case $1

      when 0 then 0

      when 1 then 5

    end;

$_$;


ALTER FUNCTION gsc.e_can_mission(integer) OWNER TO postgres;

--
-- Name: e_efficiency(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_efficiency(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

  select case when $1 between 12 and 17 then 0 -- отсутствие эффективности

              when $1 between 18 and 39 then 1 -- низкая эффективность

              when $1 between 40 and 59 then 2 -- средняя эффективность

              when $1 between 60 and 85 then 3 -- высокая эффективность

          end;

$_$;


ALTER FUNCTION gsc.e_efficiency(integer) OWNER TO postgres;

--
-- Name: e_engage(integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_engage(stage1_points integer, test_results integer, interview_status integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$

  declare

    ret integer default 0;

  begin

    if stage1_points >= 12 and test_results >= 3 and interview_status > 0 then

      ret := 1;

    else

      ret := 0;

    end if;

    return ret;

  end;

$$;


ALTER FUNCTION gsc.e_engage(stage1_points integer, test_results integer, interview_status integer) OWNER TO postgres;

--
-- Name: e_exp_years_education(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_exp_years_education(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

  select case

    when $1 between  0 and  1 then 1

    when $1 between  2 and  3 then 2

    when $1 between  4 and  5 then 3

    when $1 between  6 and  7 then 4

    when $1 >= 8 then 5

  end;

$_$;


ALTER FUNCTION gsc.e_exp_years_education(integer) OWNER TO postgres;

--
-- Name: e_interview_status(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_interview_status(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

    select case $1

      when 0 then 0

      when 1 then 5

    end;

$_$;


ALTER FUNCTION gsc.e_interview_status(integer) OWNER TO postgres;

--
-- Name: e_next_period_proposal(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_next_period_proposal(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

  select case $1

           when 2 then 1

           when 3 then 1 -- основной

           when 1 then 2 -- резерв

           when 0 then 3 -- исключить

         end;

$_$;


ALTER FUNCTION gsc.e_next_period_proposal(integer) OWNER TO postgres;

--
-- Name: e_reports_count(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_reports_count(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

  select case

      when $1 = 0               then 0

      when $1 between  1 and  4 then 1

      when $1 between  5 and  9 then 2

      when $1 between 10 and 14 then 3

      when $1 between 15 and 19 then 4

      when $1 >= 20             then 5

    end;

$_$;


ALTER FUNCTION gsc.e_reports_count(integer) OWNER TO postgres;

--
-- Name: e_stage1_points(integer, integer, integer, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_stage1_points(p_exp_years_education integer, p_speciality integer, p_work_years_education integer, p_can_mission integer, p_post integer, p_academic_degree integer, p_test_results integer, p_interview_status integer, p_academic_title integer, p_awards integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$

    declare

      ret integer default 0;

    begin

      select p_exp_years_education +

             p_speciality +

             p_work_years_education +

             p_can_mission +

             p_post +

             p_academic_degree +

             p_test_results +

             p_interview_status +

             p_academic_title +

             p_awards

        into ret;

      return ret;

    end;

$$;


ALTER FUNCTION gsc.e_stage1_points(p_exp_years_education integer, p_speciality integer, p_work_years_education integer, p_can_mission integer, p_post integer, p_academic_degree integer, p_test_results integer, p_interview_status integer, p_academic_title integer, p_awards integer) OWNER TO postgres;

--
-- Name: e_stage2_points(integer, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_stage2_points(p_stage1_points integer, p_reports_count integer, p_subject_level integer, p_accuracy integer, p_punctual integer, p_independent integer, p_moral_level integer, p_docs_impr_part integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$

  declare

    ret integer default 0;

  begin

    select p_stage1_points +

           p_reports_count +

           p_subject_level +

           p_accuracy +

           p_punctual +

           p_independent +

           p_moral_level +

           p_docs_impr_part

      into ret;

    return ret;

  end;

$$;


ALTER FUNCTION gsc.e_stage2_points(p_stage1_points integer, p_reports_count integer, p_subject_level integer, p_accuracy integer, p_punctual integer, p_independent integer, p_moral_level integer, p_docs_impr_part integer) OWNER TO postgres;

--
-- Name: e_test_results(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_test_results(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

    select case

      when $1 between  0 and  4 then 0

      when $1 between  5 and  9 then 1

      when $1 between 10 and 15 then 2

      when $1 between 16 and 18 then 3

      when $1 between 19 and 21 then 4

      when $1 between 22 and 23 then 5

    end;

$_$;


ALTER FUNCTION gsc.e_test_results(integer) OWNER TO postgres;

--
-- Name: e_work_years_education(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION e_work_years_education(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$

  select case

    when $1 between  0 and  4 then 1

    when $1 between  5 and  9 then 2

    when $1 between 10 and 14 then 3

    when $1 between 15 and 19 then 4

    when $1 >= 20 then 5

  end;

$_$;


ALTER FUNCTION gsc.e_work_years_education(integer) OWNER TO postgres;

--
-- Name: err_concurrency_control(text); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION err_concurrency_control(p_entity text) RETURNS void
    LANGUAGE plpgsql IMMUTABLE
    AS $$

begin

  raise exception '%: Запись не найдена. Возможно она была удалена или обновлена другим пользователем.', p_entity

    using errcode = 'object_not_in_prerequisite_state',

             hint = 'Обновите таблицу или перезапустите форму редактирования.';

end;

$$;


ALTER FUNCTION gsc.err_concurrency_control(p_entity text) OWNER TO postgres;

--
-- Name: expert_delete(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION expert_delete(expert_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

begin

  delete from gsc.expert

   where id = expert_id;



  if not found then

    select gsc.err_concurrency_control('Эксперты');

  end if;

end;

$$;


ALTER FUNCTION gsc.expert_delete(expert_id integer) OWNER TO postgres;

--
-- Name: expert_insert(integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, date, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying, character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION expert_insert(period_id integer, import_code character varying, surname character varying, name character varying, patronymic character varying, workplace character varying, post character varying, post_value integer, birth_date date, birth_place character varying, base_region_id integer, live_address character varying, phone character varying, email character varying, expirience character varying, speciality character varying, speciality_value integer, work_years_total integer, work_years_education integer, work_years_chief integer, exp_years_education integer, awards character varying, can_mission integer, chief_name character varying, chief_post character varying, chief_email character varying, observer_id integer, kind integer, test_results integer, interview_status integer, subject_level_value integer, accuracy_level_value integer, punctual_level_value integer, independency_level_value integer, moral_level_value integer, docs_impr_part_level_value integer, academic_title_id integer, academic_degree_id integer) RETURNS TABLE(id integer, academic_degree_value integer, academic_title_value integer, work_years_education_value integer, exp_years_education_value integer, awards_value integer, can_mission_value integer, test_results_value integer, interview_status_value integer, stage1_points integer, engage integer, stage2_points integer, efficiency integer, next_period_proposal integer, ts timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

declare
    l_id integer;
    l_academic_degree_value integer;
    l_academic_title_value integer;
    l_work_years_education_value integer;
    l_exp_years_education_value integer;
    l_awards_value integer;
    l_can_mission_value integer;
    l_test_results_value integer;
    l_interview_status_value integer;
    l_stage1_points integer;
    l_engage integer;
    l_stage2_points integer;
    l_efficiency integer;
    l_next_period_proposal integer;
    l_ts timestamp;
begin

  l_academic_degree_value := gsc.e_academic_degree(academic_degree_id);

  l_academic_title_value := gsc.e_academic_title(academic_title_id);

  l_work_years_education_value := gsc.e_work_years_education(work_years_education);

  l_exp_years_education_value := gsc.e_exp_years_education(exp_years_education);

  l_awards_value := gsc.e_awards(awards);

  l_can_mission_value := gsc.e_can_mission(can_mission);

  l_test_results_value := gsc.e_test_results(test_results);

  l_interview_status_value := gsc.e_interview_status(interview_status);

  l_stage1_points := gsc.e_stage1_points(p_exp_years_education  := l_exp_years_education_value,

                                         p_speciality           := speciality_value,

                                         p_work_years_education := l_work_years_education_value,

                                         p_can_mission          := l_can_mission_value,

                                         p_post                 := post_value,

                                         p_academic_degree      := l_academic_degree_value,

                                         p_test_results         := l_test_results_value,

                                         p_interview_status     := l_interview_status_value,

                                         p_academic_title       := l_academic_title_value,

                                         p_awards               := l_awards_value);

  l_engage := gsc.e_engage(l_stage1_points, l_test_results_value, l_interview_status_value);

  l_stage2_points := gsc.e_stage2_points(p_stage1_points  := l_stage1_points,

                                         p_reports_count  := 0, -- for new expert

                                         p_subject_level  := subject_level_value,

                                         p_accuracy       := accuracy_level_value,

                                         p_punctual       := punctual_level_value,

                                         p_independent    := independency_level_value,

                                         p_moral_level    := moral_level_value,

                                         p_docs_impr_part := docs_impr_part_level_value);

  l_efficiency := gsc.e_efficiency(l_stage2_points);

  l_next_period_proposal := gsc.e_next_period_proposal(l_efficiency);



  insert into gsc.expert (

    --id,

    period_id,

    import_code,

    surname,

    name,

    patronymic,

    workplace,

    post,

    post_value,

    birth_date,

    birth_place,

    base_region_id,

    live_address,

    phone,

    email,

    academic_degree_value,

    academic_title_value,

    expirience,

    speciality,

    speciality_value,

    work_years_total,

    work_years_education,

    work_years_education_value,

    work_years_chief,

    exp_years_education,

    exp_years_education_value,

    awards,

    awards_value,

    can_mission,

    can_mission_value,

    chief_name,

    chief_post,

    chief_email,

    observer_id,

    kind,

    test_results,

    test_results_value,

    interview_status,

    interview_status_value,

    stage1_points,

    engage,

    subject_level_value,

    accuracy_level_value,

    punctual_level_value,

    independency_level_value,

    moral_level_value,

    docs_impr_part_level_value,

    stage2_points,

    efficiency,

    next_period_proposal,

    academic_title_id,

    academic_degree_id)

  values (

    --id,

    period_id,

    import_code,

    surname,

    name,

    patronymic,

    workplace,

    post,

    post_value,

    birth_date,

    birth_place,

    base_region_id,

    live_address,

    phone,

    email,

    l_academic_degree_value,

    l_academic_title_value,

    expirience,

    speciality,

    speciality_value,

    work_years_total,

    work_years_education,

    l_work_years_education_value,

    work_years_chief,

    exp_years_education,

    l_exp_years_education_value,

    awards,

    l_awards_value,

    can_mission,

    l_can_mission_value,

    chief_name,

    chief_post,

    chief_email,

    observer_id,

    kind,

    test_results,

    l_test_results_value,

    interview_status,

    l_interview_status_value,

    l_stage1_points,

    l_engage,

    subject_level_value,

    accuracy_level_value,

    punctual_level_value,

    independency_level_value,

    moral_level_value,

    docs_impr_part_level_value,

    l_stage2_points,

    l_efficiency,

    l_next_period_proposal,

    academic_title_id,

    academic_degree_id)

  returning expert.id, expert.ts into l_id, l_ts;
  return query select l_id as id,
                l_academic_degree_value as academic_degree_value,
                l_academic_title_value as academic_title_value,
                l_work_years_education_value as work_years_education_value,
                l_exp_years_education_value as exp_years_education_value,
                l_awards_value as awards_value,
                l_can_mission_value as can_mission_value,
                l_test_results_value as test_results_value,
                l_interview_status_value as interview_status_value,
                l_stage1_points as stage1_points,
                l_engage as engage,
                l_stage2_points as stage2_points,
                l_efficiency as efficiency,
                l_next_period_proposal as next_period_proposal,
                l_ts as ts;

end;

$$;


ALTER FUNCTION gsc.expert_insert(period_id integer, import_code character varying, surname character varying, name character varying, patronymic character varying, workplace character varying, post character varying, post_value integer, birth_date date, birth_place character varying, base_region_id integer, live_address character varying, phone character varying, email character varying, expirience character varying, speciality character varying, speciality_value integer, work_years_total integer, work_years_education integer, work_years_chief integer, exp_years_education integer, awards character varying, can_mission integer, chief_name character varying, chief_post character varying, chief_email character varying, observer_id integer, kind integer, test_results integer, interview_status integer, subject_level_value integer, accuracy_level_value integer, punctual_level_value integer, independency_level_value integer, moral_level_value integer, docs_impr_part_level_value integer, academic_title_id integer, academic_degree_id integer) OWNER TO postgres;

--
-- Name: expert_update(integer, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, date, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying, character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, timestamp without time zone); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION expert_update(p_id integer, new_import_code character varying, new_period_id integer, new_surname character varying, new_name character varying, new_patronymic character varying, new_workplace character varying, new_post character varying, new_post_value integer, new_birth_date date, new_birth_place character varying, new_base_region_id integer, new_live_address character varying, new_phone character varying, new_email character varying, new_expirience character varying, new_speciality character varying, new_speciality_value integer, new_work_years_total integer, new_work_years_education integer, new_work_years_chief integer, new_exp_years_education integer, new_awards character varying, new_can_mission integer, new_chief_name character varying, new_chief_post character varying, new_chief_email character varying, new_observer_id integer, new_kind integer, new_test_results integer, new_interview_status integer, new_subject_level_value integer, new_accuracy_level_value integer, new_punctual_level_value integer, new_independency_level_value integer, new_moral_level_value integer, new_docs_impr_part_level_value integer, new_academic_title_id integer, new_academic_degree_id integer, old_ts timestamp without time zone) RETURNS TABLE(academic_degree_value integer, academic_title_value integer, work_years_education_value integer, exp_years_education_value integer, awards_value integer, can_mission_value integer, test_results_value integer, interview_status_value integer, stage1_points integer, engage integer, stage2_points integer, efficiency integer, next_period_proposal integer, ts timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
declare
  l_academic_degree_value integer;
  l_academic_title_value integer;
  l_work_years_education_value integer;
  l_exp_years_education_value integer;
  l_awards_value integer;
  l_can_mission_value integer;
  l_test_results_value integer;
  l_interview_status_value integer;
  l_stage1_points integer;
  l_engage integer;
  l_stage2_points integer;
  l_efficiency integer;
  l_next_period_proposal integer;
  l_ts timestamp;

begin

  l_academic_degree_value := gsc.e_academic_degree(new_academic_degree_id);

  l_academic_title_value := gsc.e_academic_title(new_academic_title_id);

  l_work_years_education_value := gsc.e_work_years_education(new_work_years_education);

  l_exp_years_education_value := gsc.e_exp_years_education(new_exp_years_education);

  l_awards_value := gsc.e_awards(new_awards);

  l_can_mission_value := gsc.e_can_mission(new_can_mission);

  l_test_results_value := gsc.e_test_results(new_test_results);

  l_interview_status_value := gsc.e_interview_status(new_interview_status);

  l_stage1_points := gsc.e_stage1_points(p_exp_years_education  := l_exp_years_education_value,
                                         p_speciality           := new_speciality_value,
                                         p_work_years_education := l_work_years_education_value,
                                         p_can_mission          := l_can_mission_value,
                                         p_post                 := new_post_value,
                                         p_academic_degree      := l_academic_degree_value,
                                         p_test_results         := l_test_results_value,
                                         p_interview_status     := l_interview_status_value,
                                         p_academic_title       := l_academic_title_value,
                                         p_awards               := l_awards_value);
  l_engage := gsc.e_engage(l_stage1_points, l_test_results_value, l_interview_status_value);

  l_stage2_points := gsc.e_stage2_points(p_stage1_points  := l_stage1_points,
                                         p_reports_count  := gsc.cc_expert_reports_count(p_id, new_period_id),
                                         p_subject_level  := new_subject_level_value,
                                         p_accuracy       := new_accuracy_level_value,
                                         p_punctual       := new_punctual_level_value,
                                         p_independent    := new_independency_level_value,
                                         p_moral_level    := new_moral_level_value,
                                         p_docs_impr_part := new_docs_impr_part_level_value);
  l_efficiency := gsc.e_efficiency(l_stage2_points);

  l_next_period_proposal := gsc.e_next_period_proposal(l_efficiency);



  update gsc.expert set
    period_id = new_period_id,
    import_code = new_import_code,
    surname = new_surname,
    name = new_name,
    patronymic = new_patronymic,
    workplace = new_workplace,
    post = new_post,
    post_value = new_post_value,
    birth_date = new_birth_date,
    birth_place = new_birth_place,
    base_region_id = new_base_region_id,
    live_address = new_live_address,
    phone = new_phone,
    email = new_email,
    academic_degree_value = l_academic_degree_value,
    academic_title_value = l_academic_title_value,
    expirience = new_expirience,
    speciality = new_speciality,
    speciality_value = new_speciality_value,
    work_years_total = new_work_years_total,
    work_years_education = new_work_years_education,
    work_years_education_value = l_work_years_education_value,
    work_years_chief = new_work_years_chief,
    exp_years_education = new_exp_years_education,
    exp_years_education_value = l_exp_years_education_value,
    awards = new_awards,
    awards_value = l_awards_value,
    can_mission = new_can_mission,
    can_mission_value = l_can_mission_value,
    chief_name = new_chief_name,
    chief_post = new_chief_post,
    chief_email = new_chief_email,
    observer_id = new_observer_id,
    kind = new_kind,
    test_results = new_test_results,
    test_results_value = l_test_results_value,
    interview_status = new_interview_status,
    interview_status_value = l_interview_status_value,
    stage1_points = l_stage1_points,
    engage = l_engage,
    subject_level_value = new_subject_level_value,
    accuracy_level_value = new_accuracy_level_value,
    punctual_level_value = new_punctual_level_value,
    independency_level_value = new_independency_level_value,
    moral_level_value = new_moral_level_value,
    docs_impr_part_level_value = new_docs_impr_part_level_value,
    stage2_points = l_stage2_points,
    efficiency = l_efficiency,
    next_period_proposal = l_next_period_proposal,
    academic_title_id = new_academic_title_id,
    academic_degree_id = new_academic_degree_id
  where
    id = p_id and
    expert.ts = old_ts
  returning expert.ts into l_ts;



  if not found then

    select gsc.err_concurrency_control('Эксперты');

  end if;

  return query select l_academic_degree_value as academic_degree_value,
                      l_academic_title_value as academic_title_value,
                      l_work_years_education_value as work_years_education_value,
                      l_exp_years_education_value as exp_years_education_value,
                      l_awards_value as awards_value,
                      l_can_mission_value as can_mission_value,
                      l_test_results_value as test_results_value,
                      l_interview_status_value as interview_status_value,
                      l_stage1_points as stage1_points,
                      l_engage as engage,
                      l_stage2_points as stage2_points,
                      l_efficiency as efficiency,
                      l_next_period_proposal as next_period_proposal,
                      l_ts as ts;
end;

$$;


ALTER FUNCTION gsc.expert_update(p_id integer, new_import_code character varying, new_period_id integer, new_surname character varying, new_name character varying, new_patronymic character varying, new_workplace character varying, new_post character varying, new_post_value integer, new_birth_date date, new_birth_place character varying, new_base_region_id integer, new_live_address character varying, new_phone character varying, new_email character varying, new_expirience character varying, new_speciality character varying, new_speciality_value integer, new_work_years_total integer, new_work_years_education integer, new_work_years_chief integer, new_exp_years_education integer, new_awards character varying, new_can_mission integer, new_chief_name character varying, new_chief_post character varying, new_chief_email character varying, new_observer_id integer, new_kind integer, new_test_results integer, new_interview_status integer, new_subject_level_value integer, new_accuracy_level_value integer, new_punctual_level_value integer, new_independency_level_value integer, new_moral_level_value integer, new_docs_impr_part_level_value integer, new_academic_title_id integer, new_academic_degree_id integer, old_ts timestamp without time zone) OWNER TO postgres;

--
-- Name: expert_update_ts(); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION expert_update_ts() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.ts := now();
  return new;
end;
$$;


ALTER FUNCTION gsc.expert_update_ts() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: academic_degree; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE academic_degree (
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE academic_degree OWNER TO postgres;

--
-- Name: f_expert_academic_degree(academic_degree); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION f_expert_academic_degree(ad academic_degree) RETURNS integer
    LANGUAGE plpgsql
    AS $$

declare

  ret integer;

begin

  select CASE

            WHEN ad.code IS NULL THEN 0

            ELSE ad.default_val

        END

    into ret;

  return ret;

end;

$$;


ALTER FUNCTION gsc.f_expert_academic_degree(ad academic_degree) OWNER TO postgres;

--
-- Name: academic_title; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE academic_title (
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    default_val integer DEFAULT 0 NOT NULL,
    id integer NOT NULL,
    CONSTRAINT chk_default_val CHECK ((default_val = ANY (ARRAY[0, 1, 2, 3, 4, 5])))
);


ALTER TABLE academic_title OWNER TO postgres;

--
-- Name: f_expert_academic_title(academic_title); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION f_expert_academic_title(at academic_title) RETURNS integer
    LANGUAGE plpgsql
    AS $$

declare

  ret integer;

begin

  select CASE

            WHEN at.code IS NULL THEN 0

            ELSE at.default_val

        END

    into ret;

  return ret;

end;

$$;


ALTER FUNCTION gsc.f_expert_academic_title(at academic_title) OWNER TO postgres;

--
-- Name: f_expert_stage1_points(); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION f_expert_stage1_points() RETURNS void
    LANGUAGE plpgsql
    AS $$

begin

end;

$$;


ALTER FUNCTION gsc.f_expert_stage1_points() OWNER TO postgres;

--
-- Name: f_get_schedule_header(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION f_get_schedule_header(p_project_id integer) RETURNS TABLE(master_id integer, detail_id integer, exam_date date)
    LANGUAGE plpgsql STRICT
    AS $$
declare
  schedule_row record;
begin
  create temp table if not exists tmp_schedule_rows (
    schedule_id integer not null,
    schedule_detail_id integer,
    exam_date date
  ) on commit drop;

  delete from tmp_schedule_rows;

  for schedule_row in
    select d.id as detail_id,
           d.schedule_id as master_id,
           m.expert_id,
           p.id as ppe_id,
           p.region_id,
           p.exam_date
      from gsc.schedule_detail d
      join gsc.schedule m on d.schedule_id = m.id
      left join gsc.ppe p on p.id = d.ppe_id
     where m.period_id = p_project_id
  loop

    if exists(select *
               from tmp_schedule_rows t
              where t.schedule_id = schedule_row.master_id
                and t.exam_date = schedule_row.exam_date) then
      raise notice 'Found row with %, %. No checks for detail_id. Inserted duplicate.', schedule_row.master_id, schedule_row.exam_date;
      insert into tmp_schedule_rows (schedule_id, schedule_detail_id, exam_date)
           values (schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date);
    elsif exists(select *
                 from tmp_schedule_rows t
                 where t.schedule_id = schedule_row.master_id) then
      raise notice 'Found one detail for master %. Skip it.', schedule_row.master_id;
    else
      insert into tmp_schedule_rows (schedule_id, schedule_detail_id, exam_date)
           values (schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date);
      raise notice 'Inserted new row %, %, %', schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date;
    end if;

  end loop;

  return query select t.schedule_id, t.schedule_detail_id, t.exam_date from tmp_schedule_rows t;

end;
$$;


ALTER FUNCTION gsc.f_get_schedule_header(p_project_id integer) OWNER TO postgres;

--
-- Name: period; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE period (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    CONSTRAINT period_status_check CHECK ((status = ANY (ARRAY[1, 2])))
);


ALTER TABLE period OWNER TO postgres;

--
-- Name: f_period_test(period); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION f_period_test(p period) RETURNS integer
    LANGUAGE plpgsql
    AS $$

  declare

    ret integer;

  begin

    select 1 into ret;

    return ret;

  end;

$$;


ALTER FUNCTION gsc.f_period_test(p period) OWNER TO postgres;

--
-- Name: import_academic_degree(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_academic_degree(p_imp_session_guid uuid) RETURNS TABLE(code character varying, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes VARCHAR [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right

  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT iad.code
                      FROM gsc.imp_academic_degree iad
                      WHERE iad.imp_session_guid = p_imp_session_guid
                      GROUP BY iad.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/

  -- import rows
  WITH imported AS (
    INSERT INTO academic_degree AS ad (code, name)
      SELECT
        iad.code imp_code,
        iad.name
      FROM imp_academic_degree iad
      WHERE iad.imp_session_guid = p_imp_session_guid AND iad.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT academic_degree_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING ad.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_academic_degree iad
  SET imp_status = 'imported'
  WHERE iad.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ad.code,
                 ad.name
               FROM gsc.academic_degree ad
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_academic_degree iad
                                WHERE iad.imp_session_guid = p_imp_session_guid AND
                                      ad.code = iad.code);
END;
$$;


ALTER FUNCTION gsc.import_academic_degree(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_academic_title(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_academic_title(p_imp_session_guid uuid) RETURNS TABLE(code character varying, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes VARCHAR [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right
  
  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT iat.code
                      FROM gsc.imp_academic_title iat
                      WHERE iat.imp_session_guid = p_imp_session_guid
                      GROUP BY iat.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/

  -- import rows
  WITH imported AS (
    INSERT INTO academic_title AS act (code, name)
      SELECT
        act.code imp_code,
        act.name
      FROM imp_academic_title act
      WHERE act.imp_session_guid = p_imp_session_guid AND act.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT academic_title_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING act.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_academic_title iat
  SET imp_status = 'imported'
  WHERE iat.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 act.code,
                 act.name
               FROM gsc.academic_title act
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_academic_title iat
                                WHERE iat.imp_session_guid = p_imp_session_guid AND
                                      act.code = iat.code);
END;
$$;


ALTER FUNCTION gsc.import_academic_title(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_contract_info(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_contract_info(p_imp_session_guid uuid) RETURNS TABLE(number character varying, contract_date date)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right (grant)

  -- import rows
  WITH imported AS (
    INSERT INTO contract_info AS ci (number, contract_date)
      SELECT
        ici.number,
        ici.contract_date
      FROM imp_contract_info ici
      WHERE ici.imp_session_guid = p_imp_session_guid AND ici.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT contract_info_number_key
      DO UPDATE SET contract_date = EXCLUDED.contract_date
    RETURNING ci.number)
  SELECT array_agg(i.number)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_contract_info ici
  SET imp_status = 'imported'
  WHERE ici.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ci.number,
                 ci.contract_date
               FROM gsc.contract_info ci
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_contract_info ici
                                WHERE ici.imp_session_guid = p_imp_session_guid AND
                                      ici.number = ci.number);
END;
$$;


ALTER FUNCTION gsc.import_contract_info(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_row_academic_degree(uuid, character varying, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_row_academic_degree(imp_session_guid uuid, code character varying, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO imp_academic_degree (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;


ALTER FUNCTION gsc.import_row_academic_degree(imp_session_guid uuid, code character varying, name character varying) OWNER TO postgres;

--
-- Name: import_row_academic_title(uuid, character varying, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_row_academic_title(imp_session_guid uuid, code character varying, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO imp_academic_title (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;


ALTER FUNCTION gsc.import_row_academic_title(imp_session_guid uuid, code character varying, name character varying) OWNER TO postgres;

--
-- Name: import_row_contract_info(uuid, character varying, date); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_row_contract_info(imp_session_guid uuid, number character varying, contract_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO gsc.imp_contract_info (imp_session_guid, number, contract_date)
  VALUES (imp_session_guid, number, contract_date);
END;
$$;


ALTER FUNCTION gsc.import_row_contract_info(imp_session_guid uuid, number character varying, contract_date date) OWNER TO postgres;

--
-- Name: import_row_subject(uuid, integer, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_row_subject(imp_session_guid uuid, code integer, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO gsc.imp_subject (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;


ALTER FUNCTION gsc.import_row_subject(imp_session_guid uuid, code integer, name character varying) OWNER TO postgres;

--
-- Name: import_subject(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_subject(p_imp_session_guid uuid) RETURNS TABLE(code integer, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes INTEGER [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right (grant)

  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT ims.code
                      FROM gsc.imp_subject ims
                      WHERE ims.imp_session_guid = p_imp_session_guid
                      GROUP BY ims.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/

  -- import rows
  WITH imported AS (
    INSERT INTO subject AS s (code, name)
      SELECT
        ims.code,
        ims.name
      FROM imp_subject ims
      WHERE ims.imp_session_guid = p_imp_session_guid AND ims.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT subject_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING s.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_subject ims
  SET imp_status = 'imported'
  WHERE ims.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 s.code,
                 s.name
               FROM gsc.subject s
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_subject ims
                                WHERE ims.imp_session_guid = p_imp_session_guid AND
                                      ims.code = s.code);
END;
$$;


ALTER FUNCTION gsc.import_subject(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: period_delete(integer, character varying, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION period_delete(pid integer, ptitle character varying, pstatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    delete from gsc.period

     where id = pid

       and title = ptitle

       and status = pstatus;

  end;

$$;


ALTER FUNCTION gsc.period_delete(pid integer, ptitle character varying, pstatus integer) OWNER TO postgres;

--
-- Name: period_insert(character varying, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION period_insert(ptitle character varying, pstatus integer, OUT pid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

  declare

    ret integer;

  begin

    insert into gsc.period(title, status)

    values (ptitle, pstatus);

    select lastval() into pid;

  end;

$$;


ALTER FUNCTION gsc.period_insert(ptitle character varying, pstatus integer, OUT pid integer) OWNER TO postgres;

--
-- Name: period_update(integer, character varying, integer, integer, character varying, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION period_update(pid integer, ptitle character varying, pstatus integer, pid_original integer, ptitle_original character varying, pstatus_orginal integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update gsc.period

       set id = pid,

           title = ptitle,

           status = pstatus

     where id = pid_original

       and title = ptitle_original

       and status = pstatus_original;

  end;

$$;


ALTER FUNCTION gsc.period_update(pid integer, ptitle character varying, pstatus integer, pid_original integer, ptitle_original character varying, pstatus_orginal integer) OWNER TO postgres;

--
-- Name: region_delete(integer, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION region_delete(pcode integer, pname character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    delete from gsc.region

     where code = pcode

       and name = pname;

  end;

$$;


ALTER FUNCTION gsc.region_delete(pcode integer, pname character varying) OWNER TO postgres;

--
-- Name: region_insert(integer, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION region_insert(pcode integer, pname character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into gsc.region(code, name) values (pcode, pname);

  end;

$$;


ALTER FUNCTION gsc.region_insert(pcode integer, pname character varying) OWNER TO postgres;

--
-- Name: region_update(integer, character varying, integer, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION region_update(pcode integer, pname character varying, pcode_original integer, pname_original character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update gsc.region

       set code = pcode,

           name = pname

     where code = pcode_original

       and name = pname_original;

  end;

$$;


ALTER FUNCTION gsc.region_update(pcode integer, pname character varying, pcode_original integer, pname_original character varying) OWNER TO postgres;

--
-- Name: schedule_calendar_details(integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION schedule_calendar_details(integer) RETURNS TABLE(id integer, schedule_id integer, exam_date date, address text)
    LANGUAGE sql IMMUTABLE STRICT
    AS $$
  select schedule_detail.id, schedule_id, ppe.exam_date, ppe.address
    from gsc.schedule_detail
    join gsc.schedule on schedule.id = schedule_id
    left join gsc.ppe on ppe.id = ppe_id;
$$;


ALTER FUNCTION gsc.schedule_calendar_details(integer) OWNER TO postgres;

--
-- Name: academic_degree_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE academic_degree_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE academic_degree_id_seq OWNER TO postgres;

--
-- Name: academic_degree_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE academic_degree_id_seq OWNED BY academic_degree.id;


--
-- Name: academic_title_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE academic_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE academic_title_id_seq OWNER TO postgres;

--
-- Name: academic_title_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE academic_title_id_seq OWNED BY academic_title.id;


--
-- Name: contract_info; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE contract_info (
    id integer NOT NULL,
    number character varying(30) NOT NULL,
    contract_date date NOT NULL
);


ALTER TABLE contract_info OWNER TO postgres;

--
-- Name: contract_info_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE contract_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contract_info_id_seq OWNER TO postgres;

--
-- Name: contract_info_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE contract_info_id_seq OWNED BY contract_info.id;


--
-- Name: event; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE event (
    id integer NOT NULL,
    ts timestamp without time zone NOT NULL,
    channel d_channel_type NOT NULL,
    location d_violation_location NOT NULL,
    type d_event_type,
    schedule_id integer,
    monitor_result_id integer
);


ALTER TABLE event OWNER TO postgres;

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_id_seq OWNER TO postgres;

--
-- Name: event_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE event_id_seq OWNED BY event.id;


--
-- Name: exam; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE exam (
    id integer NOT NULL,
    period_type integer DEFAULT 2 NOT NULL,
    exam_date date NOT NULL,
    subject_id integer NOT NULL,
    exam_type integer DEFAULT 1 NOT NULL,
    period_id integer NOT NULL,
    CONSTRAINT exam_exam_type_check CHECK ((exam_type = ANY (ARRAY[1, 2, 3]))),
    CONSTRAINT exam_period_type_check CHECK ((period_type = ANY (ARRAY[1, 2, 3])))
);


ALTER TABLE exam OWNER TO postgres;

--
-- Name: exam_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE exam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE exam_id_seq OWNER TO postgres;

--
-- Name: exam_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE exam_id_seq OWNED BY exam.id;


--
-- Name: expert; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE expert (
    id integer NOT NULL,
    period_id integer NOT NULL,
    import_code character varying(100) DEFAULT public.uuid_generate_v4(),
    surname character varying(100) DEFAULT ''::character varying NOT NULL,
    name character varying(100) DEFAULT ''::character varying NOT NULL,
    patronymic character varying(100) DEFAULT ''::character varying,
    workplace character varying(500) DEFAULT ''::character varying NOT NULL,
    post character varying(500) DEFAULT ''::character varying NOT NULL,
    post_value integer DEFAULT 1 NOT NULL,
    birth_date date NOT NULL,
    birth_place character varying(300) DEFAULT ''::character varying NOT NULL,
    base_region_id integer NOT NULL,
    live_address character varying(500) DEFAULT ''::character varying NOT NULL,
    phone character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL,
    academic_degree_value integer DEFAULT 0 NOT NULL,
    academic_title_value integer DEFAULT 0 NOT NULL,
    expirience character varying(1000) DEFAULT ''::character varying NOT NULL,
    speciality character varying(300) DEFAULT ''::character varying NOT NULL,
    speciality_value integer DEFAULT 0 NOT NULL,
    work_years_total integer DEFAULT 0 NOT NULL,
    work_years_education integer DEFAULT 0 NOT NULL,
    work_years_education_value integer DEFAULT 1 NOT NULL,
    work_years_chief integer DEFAULT 0 NOT NULL,
    exp_years_education integer DEFAULT 0 NOT NULL,
    exp_years_education_value integer DEFAULT 1 NOT NULL,
    awards character varying(1000) DEFAULT ''::character varying NOT NULL,
    awards_value integer DEFAULT 0 NOT NULL,
    can_mission integer DEFAULT 1 NOT NULL,
    can_mission_value integer DEFAULT 5 NOT NULL,
    chief_name character varying(300) DEFAULT ''::character varying NOT NULL,
    chief_post character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_email character varying(100) DEFAULT ''::character varying NOT NULL,
    observer_id integer NOT NULL,
    kind integer DEFAULT 1 NOT NULL,
    test_results integer,
    test_results_value integer,
    interview_status integer,
    interview_status_value integer,
    stage1_points integer,
    engage integer,
    subject_level_value integer DEFAULT 5 NOT NULL,
    accuracy_level_value integer DEFAULT 5 NOT NULL,
    punctual_level_value integer DEFAULT 5 NOT NULL,
    independency_level_value integer DEFAULT 5 NOT NULL,
    moral_level_value integer DEFAULT 5 NOT NULL,
    docs_impr_part_level_value integer DEFAULT 0 NOT NULL,
    stage2_points integer,
    efficiency integer,
    next_period_proposal integer,
    academic_title_id integer,
    academic_degree_id integer,
    ts timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT expert_academic_degree_value_check CHECK ((academic_degree_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_academic_title_value_check CHECK ((academic_title_value = ANY (ARRAY[0, 1, 2, 3, 4, 5]))),
    CONSTRAINT expert_accuracy_level_value_check CHECK (((accuracy_level_value >= 0) AND (accuracy_level_value <= 5))),
    CONSTRAINT expert_awards_value_check CHECK ((awards_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_can_mission_check CHECK ((can_mission = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_can_mission_value_check CHECK ((can_mission_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_docs_impr_part_level_value_check CHECK ((docs_impr_part_level_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_efficiency_check CHECK (((efficiency >= 0) AND (efficiency <= 3))),
    CONSTRAINT expert_engage_ckeck CHECK ((engage = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_exp_years_education_value_check CHECK ((exp_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_independency_level_value_check CHECK (((independency_level_value >= 0) AND (independency_level_value <= 5))),
    CONSTRAINT expert_interview_status_check CHECK ((interview_status = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_interview_status_value_check CHECK ((interview_status_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_kind_check CHECK ((kind = ANY (ARRAY[1, 2]))),
    CONSTRAINT expert_moral_level_value_check CHECK (((moral_level_value >= 0) AND (moral_level_value <= 5))),
    CONSTRAINT expert_next_period_proposal_check CHECK (((efficiency >= 1) AND (efficiency <= 3))),
    CONSTRAINT expert_post_value_check CHECK ((post_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_punctual_level_value_check CHECK ((punctual_level_value = ANY (ARRAY[0, 1, 3, 5]))),
    CONSTRAINT expert_speciality_value_check CHECK ((speciality_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_stage1_points_check CHECK ((stage1_points >= 0)),
    CONSTRAINT expert_stage2_points_check CHECK ((stage2_points >= 0)),
    CONSTRAINT expert_subject_level_value_check CHECK (((subject_level_value >= 0) AND (subject_level_value <= 5))),
    CONSTRAINT expert_test_results_check CHECK (((test_results >= 0) AND (test_results <= 23))),
    CONSTRAINT expert_test_results_value_check CHECK (((test_results_value >= 0) AND (test_results_value <= 5))),
    CONSTRAINT expert_work_years_chief_check CHECK ((work_years_chief >= 0)),
    CONSTRAINT expert_work_years_education_check CHECK ((work_years_education >= 0)),
    CONSTRAINT expert_work_years_education_value_check CHECK ((work_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_work_years_total_check CHECK ((work_years_total >= 0))
);


ALTER TABLE expert OWNER TO postgres;

--
-- Name: expert_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE expert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expert_id_seq OWNER TO postgres;

--
-- Name: expert_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE expert_id_seq OWNED BY expert.id;


--
-- Name: imp_academic_degree; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE imp_academic_degree (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE imp_academic_degree OWNER TO postgres;

--
-- Name: TABLE imp_academic_degree; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON TABLE imp_academic_degree IS 'Academic degree import table.';


--
-- Name: COLUMN imp_academic_degree.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';


--
-- Name: COLUMN imp_academic_degree.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.imp_user IS 'User imported row.';


--
-- Name: COLUMN imp_academic_degree.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.imp_datetime IS 'Import timestamp.';


--
-- Name: COLUMN imp_academic_degree.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.imp_status IS 'Import status';


--
-- Name: COLUMN imp_academic_degree.code; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.code IS 'Acedemic degree code.';


--
-- Name: COLUMN imp_academic_degree.name; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_degree.name IS 'Academic degree name.';


--
-- Name: imp_academic_title; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE imp_academic_title (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE imp_academic_title OWNER TO postgres;

--
-- Name: TABLE imp_academic_title; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON TABLE imp_academic_title IS 'Academic title import table.';


--
-- Name: COLUMN imp_academic_title.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';


--
-- Name: COLUMN imp_academic_title.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.imp_user IS 'User imported row.';


--
-- Name: COLUMN imp_academic_title.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.imp_datetime IS 'Import timestamp.';


--
-- Name: COLUMN imp_academic_title.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.imp_status IS 'Import status';


--
-- Name: COLUMN imp_academic_title.code; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.code IS 'Acedemic title code.';


--
-- Name: COLUMN imp_academic_title.name; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_academic_title.name IS 'Academic title name.';


--
-- Name: imp_contract_info; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE imp_contract_info (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    number character varying(30) NOT NULL,
    contract_date date NOT NULL
);


ALTER TABLE imp_contract_info OWNER TO postgres;

--
-- Name: TABLE imp_contract_info; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON TABLE imp_contract_info IS 'Contract info import table.';


--
-- Name: COLUMN imp_contract_info.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';


--
-- Name: COLUMN imp_contract_info.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.imp_user IS 'User imported row.';


--
-- Name: COLUMN imp_contract_info.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.imp_datetime IS 'Import timestamp.';


--
-- Name: COLUMN imp_contract_info.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.imp_status IS 'Import status';


--
-- Name: COLUMN imp_contract_info.number; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.number IS 'Contract info number.';


--
-- Name: COLUMN imp_contract_info.contract_date; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_contract_info.contract_date IS 'Contract info date.';


--
-- Name: imp_subject; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE imp_subject (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE imp_subject OWNER TO postgres;

--
-- Name: TABLE imp_subject; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON TABLE imp_subject IS 'Subject import table.';


--
-- Name: COLUMN imp_subject.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';


--
-- Name: COLUMN imp_subject.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.imp_user IS 'User imported row.';


--
-- Name: COLUMN imp_subject.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.imp_datetime IS 'Import timestamp.';


--
-- Name: COLUMN imp_subject.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.imp_status IS 'Import status';


--
-- Name: COLUMN imp_subject.code; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.code IS 'Subject code.';


--
-- Name: COLUMN imp_subject.name; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN imp_subject.name IS 'Subject name.';


--
-- Name: monitor_result; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE monitor_result (
    id integer NOT NULL,
    location d_violation_location DEFAULT 1 NOT NULL,
    schedule_id integer NOT NULL,
    exam_date date NOT NULL,
    ppe_id integer,
    violation_id integer NOT NULL,
    violator_id integer NOT NULL,
    note text,
    subject_id integer
);


ALTER TABLE monitor_result OWNER TO postgres;

--
-- Name: monitor_result_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE monitor_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE monitor_result_id_seq OWNER TO postgres;

--
-- Name: monitor_result_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE monitor_result_id_seq OWNED BY monitor_result.id;


--
-- Name: observer; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE observer (
    id integer NOT NULL,
    type integer DEFAULT 1 NOT NULL,
    fio character varying(300) DEFAULT ''::character varying NOT NULL,
    wp character varying(500) DEFAULT ''::character varying NOT NULL,
    post character varying(500) DEFAULT ''::character varying NOT NULL,
    phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT observer_type_check CHECK ((type = ANY (ARRAY[1, 2])))
);


ALTER TABLE observer OWNER TO postgres;

--
-- Name: observer_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE observer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE observer_id_seq OWNER TO postgres;

--
-- Name: observer_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE observer_id_seq OWNED BY observer.id;


--
-- Name: oiv_gek; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE oiv_gek (
    id integer NOT NULL,
    region_id integer NOT NULL,
    oiv_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    oiv_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    oiv_post character varying(500) DEFAULT ''::character varying NOT NULL,
    oiv_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    oiv_email character varying(100) DEFAULT ''::character varying NOT NULL,
    gek_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    gek_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    gek_post character varying(500) DEFAULT ''::character varying NOT NULL,
    gek_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    gek_email character varying(100) DEFAULT ''::character varying NOT NULL,
    gia_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    gia_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    gia_post character varying(500) DEFAULT ''::character varying NOT NULL,
    gia_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    gia_email character varying(100) DEFAULT ''::character varying NOT NULL,
    nadzor_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    nadzor_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    nadzor_post character varying(500) DEFAULT ''::character varying NOT NULL,
    nadzor_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    nadzor_email character varying(100) DEFAULT ''::character varying NOT NULL,
    oiv_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    gek_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    gia_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    nadzor_phone2 character varying(50) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE oiv_gek OWNER TO postgres;

--
-- Name: oiv_gek_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE oiv_gek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oiv_gek_id_seq OWNER TO postgres;

--
-- Name: oiv_gek_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE oiv_gek_id_seq OWNED BY oiv_gek.id;


--
-- Name: period_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE period_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE period_seq OWNER TO postgres;

--
-- Name: period_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE period_seq OWNED BY period.id;


--
-- Name: ppe; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE ppe (
    id integer NOT NULL,
    region_id integer NOT NULL,
    exam_date date NOT NULL,
    subject_id integer NOT NULL,
    ppe_type integer DEFAULT 1 NOT NULL,
    code integer DEFAULT 0 NOT NULL,
    name character varying(500) DEFAULT ''::character varying NOT NULL,
    address character varying(500) DEFAULT ''::character varying NOT NULL,
    count_part integer,
    count_room integer,
    count_room_video integer,
    kim integer DEFAULT 0 NOT NULL,
    tom integer DEFAULT 0 NOT NULL,
    project_id integer NOT NULL,
    CONSTRAINT ppe_kim_check CHECK ((kim = ANY (ARRAY[0, 1]))),
    CONSTRAINT ppe_ppe_type_check CHECK ((ppe_type = ANY (ARRAY[1, 2, 3]))),
    CONSTRAINT ppe_tom_check CHECK ((tom = ANY (ARRAY[0, 1])))
);


ALTER TABLE ppe OWNER TO postgres;

--
-- Name: ppe_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE ppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ppe_id_seq OWNER TO postgres;

--
-- Name: ppe_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE ppe_id_seq OWNED BY ppe.id;


--
-- Name: rcoi; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE rcoi (
    id integer NOT NULL,
    region_id integer NOT NULL,
    name character varying(500) DEFAULT ''::character varying NOT NULL,
    address character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_post character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    chief_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    chief_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE rcoi OWNER TO postgres;

--
-- Name: rcoi_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE rcoi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rcoi_id_seq OWNER TO postgres;

--
-- Name: rcoi_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE rcoi_id_seq OWNED BY rcoi.id;


--
-- Name: region_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE region_id_seq OWNER TO postgres;

--
-- Name: region; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE region (
    code integer NOT NULL,
    name character varying(100) NOT NULL,
    zone integer DEFAULT 1 NOT NULL,
    id integer DEFAULT nextval('region_id_seq'::regclass) NOT NULL,
    CONSTRAINT region_zone_check CHECK ((zone = ANY (ARRAY[1, 2, 3])))
);


ALTER TABLE region OWNER TO postgres;

--
-- Name: schedule; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE schedule (
    id integer NOT NULL,
    expert_id integer NOT NULL,
    period_id integer NOT NULL,
    monitor_rcoi integer DEFAULT 0 NOT NULL,
    fixed integer DEFAULT 0 NOT NULL,
    period_type integer NOT NULL,
    region_id integer NOT NULL,
    CONSTRAINT schedule_fixed_check CHECK ((fixed = ANY (ARRAY[0, 1]))),
    CONSTRAINT schedule_monitor_rcoi_check CHECK ((monitor_rcoi = ANY (ARRAY[0, 1]))),
    CONSTRAINT schedule_period_type_chk CHECK ((period_type = ANY (ARRAY[1, 2, 3])))
);


ALTER TABLE schedule OWNER TO postgres;

--
-- Name: schedule_detail; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE schedule_detail (
    id integer NOT NULL,
    schedule_id integer NOT NULL,
    ppe_id integer NOT NULL
);


ALTER TABLE schedule_detail OWNER TO postgres;

--
-- Name: schedule_detail_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE schedule_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule_detail_id_seq OWNER TO postgres;

--
-- Name: schedule_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE schedule_detail_id_seq OWNED BY schedule_detail.id;


--
-- Name: schedule_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule_id_seq OWNER TO postgres;

--
-- Name: schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE schedule_id_seq OWNED BY schedule.id;


--
-- Name: subject; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE subject (
    id integer NOT NULL,
    name character varying(500) NOT NULL,
    code integer NOT NULL
);


ALTER TABLE subject OWNER TO postgres;

--
-- Name: COLUMN subject.code; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN subject.code IS 'Subject code';


--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subject_id_seq OWNER TO postgres;

--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE subject_id_seq OWNED BY subject.id;


--
-- Name: violation; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE violation (
    id integer NOT NULL,
    code integer NOT NULL,
    name character varying(500) NOT NULL
);


ALTER TABLE violation OWNER TO postgres;

--
-- Name: violation_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE violation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE violation_id_seq OWNER TO postgres;

--
-- Name: violation_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE violation_id_seq OWNED BY violation.id;


--
-- Name: violator; Type: TABLE; Schema: gsc; Owner: postgres
--

CREATE TABLE violator (
    id integer NOT NULL,
    code integer NOT NULL,
    name character varying(500) NOT NULL
);


ALTER TABLE violator OWNER TO postgres;

--
-- Name: violator_id_seq; Type: SEQUENCE; Schema: gsc; Owner: postgres
--

CREATE SEQUENCE violator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE violator_id_seq OWNER TO postgres;

--
-- Name: violator_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: postgres
--

ALTER SEQUENCE violator_id_seq OWNED BY violator.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_degree ALTER COLUMN id SET DEFAULT nextval('academic_degree_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_title ALTER COLUMN id SET DEFAULT nextval('academic_title_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY contract_info ALTER COLUMN id SET DEFAULT nextval('contract_info_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY event ALTER COLUMN id SET DEFAULT nextval('event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY exam ALTER COLUMN id SET DEFAULT nextval('exam_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert ALTER COLUMN id SET DEFAULT nextval('expert_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result ALTER COLUMN id SET DEFAULT nextval('monitor_result_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY observer ALTER COLUMN id SET DEFAULT nextval('observer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY oiv_gek ALTER COLUMN id SET DEFAULT nextval('oiv_gek_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY period ALTER COLUMN id SET DEFAULT nextval('period_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY ppe ALTER COLUMN id SET DEFAULT nextval('ppe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY rcoi ALTER COLUMN id SET DEFAULT nextval('rcoi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule ALTER COLUMN id SET DEFAULT nextval('schedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule_detail ALTER COLUMN id SET DEFAULT nextval('schedule_detail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY subject ALTER COLUMN id SET DEFAULT nextval('subject_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY violation ALTER COLUMN id SET DEFAULT nextval('violation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY violator ALTER COLUMN id SET DEFAULT nextval('violator_id_seq'::regclass);


--
-- Data for Name: academic_degree; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY academic_degree (code, name, id) FROM stdin;
2	доктор архитектурных наук	54
3	доктор биологических наук	55
4	доктор ветеринарных наук	56
5	доктор военных наук	57
6	доктор географических наук	58
7	доктор геолого-минералогических наук	59
8	доктор искусствоведения	60
9	доктор исторических наук	61
10	доктор медицинских наук	62
11	доктор педагогических наук	63
12	доктор политологических наук	64
13	доктор психологических наук	65
14	доктор сельскохозяйственных наук	66
15	доктор социологических наук	67
16	доктор технических наук	68
17	доктор фармацевтических наук	69
18	доктор физико-математических наук	70
19	доктор филологических наук	71
20	доктор философских наук	72
21	доктор химических наук	73
22	доктор экономических наук	74
23	доктор юридических наук	75
24	кандидат архитектурных наук	76
25	кандидат биологических наук	77
26	кандидат ветеринарных наук	78
27	кандидат военных наук	79
28	кандидат географических наук	80
29	кандидат геолого-минералогических наук	81
30	кандидат искусствоведения	82
31	кандидат исторических наук	83
32	кандидат медицинских наук	84
33	кандидат педагогических наук	85
34	кандидат политологических наук	86
35	кандидат психологических наук	87
36	кандидат сельскохозяйственных наук	88
37	кандидат социологических наук	89
38	кандидат технических наук	90
39	кандидат физико-математических наук	91
40	кандидат фармацевтических наук	92
41	кандидат филологических наук	93
42	кандидат философских наук	94
43	кандидат химических наук	95
44	кандидат экономических наук	96
45	кандидат юридических наук	97
1	нет ученой степени	52
\.


--
-- Name: academic_degree_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('academic_degree_id_seq', 98, true);


--
-- Data for Name: academic_title; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY academic_title (code, name, default_val, id) FROM stdin;
1	Нет ученого звания	0	3
2	Доцент	0	4
3	Профессор	0	5
4	Член-корреспондент РАН	0	6
5	Член-корреспондент РАМН	0	7
6	Член-корреспондент РАСХН	0	8
7	Член-корреспондент РАО	0	9
8	Академик РАМН	0	10
9	Академик РАН	0	11
10	Академик РАСХН	0	12
11	Академик РАО	0	13
12	Почетный академик	0	14
\.


--
-- Name: academic_title_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('academic_title_id_seq', 26, true);


--
-- Data for Name: contract_info; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY contract_info (id, number, contract_date) FROM stdin;
\.


--
-- Name: contract_info_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('contract_info_id_seq', 326, true);


--
-- Data for Name: event; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY event (id, ts, channel, location, type, schedule_id, monitor_result_id) FROM stdin;
\.


--
-- Name: event_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('event_id_seq', 1, false);


--
-- Data for Name: exam; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY exam (id, period_type, exam_date, subject_id, exam_type, period_id) FROM stdin;
\.


--
-- Name: exam_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('exam_id_seq', 2, true);


--
-- Data for Name: expert; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY expert (id, period_id, import_code, surname, name, patronymic, workplace, post, post_value, birth_date, birth_place, base_region_id, live_address, phone, email, academic_degree_value, academic_title_value, expirience, speciality, speciality_value, work_years_total, work_years_education, work_years_education_value, work_years_chief, exp_years_education, exp_years_education_value, awards, awards_value, can_mission, can_mission_value, chief_name, chief_post, chief_email, observer_id, kind, test_results, test_results_value, interview_status, interview_status_value, stage1_points, engage, subject_level_value, accuracy_level_value, punctual_level_value, independency_level_value, moral_level_value, docs_impr_part_level_value, stage2_points, efficiency, next_period_proposal, academic_title_id, academic_degree_id, ts) FROM stdin;
\.


--
-- Name: expert_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('expert_id_seq', 25, true);


--
-- Data for Name: imp_academic_degree; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_academic_degree (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Data for Name: imp_academic_title; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_academic_title (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Data for Name: imp_contract_info; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_contract_info (imp_session_guid, imp_user, imp_datetime, imp_status, number, contract_date) FROM stdin;
\.


--
-- Data for Name: imp_subject; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_subject (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Data for Name: monitor_result; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY monitor_result (id, location, schedule_id, exam_date, ppe_id, violation_id, violator_id, note, subject_id) FROM stdin;
\.


--
-- Name: monitor_result_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('monitor_result_id_seq', 1, false);


--
-- Data for Name: observer; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY observer (id, type, fio, wp, post, phone1, phone2, email) FROM stdin;
\.


--
-- Name: observer_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('observer_id_seq', 2, true);


--
-- Data for Name: oiv_gek; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY oiv_gek (id, region_id, oiv_fio, oiv_wp, oiv_post, oiv_phone1, oiv_email, gek_fio, gek_wp, gek_post, gek_phone1, gek_email, gia_fio, gia_wp, gia_post, gia_phone1, gia_email, nadzor_fio, nadzor_wp, nadzor_post, nadzor_phone1, nadzor_email, oiv_phone2, gek_phone2, gia_phone2, nadzor_phone2) FROM stdin;
\.


--
-- Name: oiv_gek_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('oiv_gek_id_seq', 1, true);


--
-- Data for Name: period; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY period (id, title, status) FROM stdin;
\.


--
-- Name: period_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('period_seq', 21, true);


--
-- Data for Name: ppe; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY ppe (id, region_id, exam_date, subject_id, ppe_type, code, name, address, count_part, count_room, count_room_video, kim, tom, project_id) FROM stdin;
\.


--
-- Name: ppe_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('ppe_id_seq', 6, true);


--
-- Data for Name: rcoi; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY rcoi (id, region_id, name, address, chief_post, chief_fio, chief_phone1, chief_phone2, email) FROM stdin;
\.


--
-- Name: rcoi_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('rcoi_id_seq', 1, true);


--
-- Data for Name: region; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY region (code, name, zone, id) FROM stdin;
\.


--
-- Name: region_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('region_id_seq', 1, true);


--
-- Data for Name: schedule; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY schedule (id, expert_id, period_id, monitor_rcoi, fixed, period_type, region_id) FROM stdin;
\.


--
-- Data for Name: schedule_detail; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY schedule_detail (id, schedule_id, ppe_id) FROM stdin;
\.


--
-- Name: schedule_detail_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('schedule_detail_id_seq', 4, true);


--
-- Name: schedule_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('schedule_id_seq', 2, true);


--
-- Data for Name: subject; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY subject (id, name, code) FROM stdin;
\.


--
-- Name: subject_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('subject_id_seq', 6, true);


--
-- Data for Name: violation; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY violation (id, code, name) FROM stdin;
\.


--
-- Name: violation_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('violation_id_seq', 1, true);


--
-- Data for Name: violator; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY violator (id, code, name) FROM stdin;
\.


--
-- Name: violator_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: postgres
--

SELECT pg_catalog.setval('violator_id_seq', 1, true);


--
-- Name: academic_degree_code_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_degree
    ADD CONSTRAINT academic_degree_code_key UNIQUE (code);


--
-- Name: academic_degree_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_degree
    ADD CONSTRAINT academic_degree_pkey PRIMARY KEY (id);


--
-- Name: academic_title_code_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_title
    ADD CONSTRAINT academic_title_code_key UNIQUE (code);


--
-- Name: academic_title_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_title
    ADD CONSTRAINT academic_title_pkey PRIMARY KEY (id);


--
-- Name: contract_info_number_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY contract_info
    ADD CONSTRAINT contract_info_number_key UNIQUE (number);


--
-- Name: contract_info_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY contract_info
    ADD CONSTRAINT contract_info_pkey PRIMARY KEY (id);


--
-- Name: event_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: exam_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_pkey PRIMARY KEY (id);


--
-- Name: expert_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_pkey PRIMARY KEY (id);


--
-- Name: imp_academic_degree_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY imp_academic_degree
    ADD CONSTRAINT imp_academic_degree_pkey PRIMARY KEY (imp_session_guid, code);


--
-- Name: imp_academic_title_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY imp_academic_title
    ADD CONSTRAINT imp_academic_title_pkey PRIMARY KEY (imp_session_guid, code);


--
-- Name: imp_contract_info_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY imp_contract_info
    ADD CONSTRAINT imp_contract_info_key PRIMARY KEY (imp_session_guid, number);


--
-- Name: imp_subject_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY imp_subject
    ADD CONSTRAINT imp_subject_pkey PRIMARY KEY (imp_session_guid, code);


--
-- Name: monitor_result_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_pkey PRIMARY KEY (id);


--
-- Name: observer_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY observer
    ADD CONSTRAINT observer_pkey PRIMARY KEY (id);


--
-- Name: oiv_gek_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY oiv_gek
    ADD CONSTRAINT oiv_gek_pkey PRIMARY KEY (id);


--
-- Name: period_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY period
    ADD CONSTRAINT period_pkey PRIMARY KEY (id);


--
-- Name: ppe_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_pkey PRIMARY KEY (id);


--
-- Name: rcoi_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY rcoi
    ADD CONSTRAINT rcoi_pkey PRIMARY KEY (id);


--
-- Name: region_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- Name: schedule_detail_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_pkey PRIMARY KEY (id);


--
-- Name: schedule_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (id);


--
-- Name: subject_code_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_code_key UNIQUE (code);


--
-- Name: subject_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: violation_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY violation
    ADD CONSTRAINT violation_pkey PRIMARY KEY (id);


--
-- Name: violator_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY violator
    ADD CONSTRAINT violator_pkey PRIMARY KEY (id);


--
-- Name: expert_ts; Type: TRIGGER; Schema: gsc; Owner: postgres
--

CREATE TRIGGER expert_ts BEFORE UPDATE ON expert FOR EACH ROW EXECUTE PROCEDURE expert_update_ts();


--
-- Name: event_monitor_result_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_monitor_result_id_fkey FOREIGN KEY (monitor_result_id) REFERENCES monitor_result(id);


--
-- Name: event_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id);


--
-- Name: exam_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: exam_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: expert_academic_degree_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_academic_degree_id_fkey FOREIGN KEY (academic_degree_id) REFERENCES academic_degree(id);


--
-- Name: expert_academic_title_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_academic_title_id_fkey FOREIGN KEY (academic_title_id) REFERENCES academic_title(id);


--
-- Name: expert_base_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_base_region_id_fkey FOREIGN KEY (base_region_id) REFERENCES region(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expert_observer_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_observer_id_fkey FOREIGN KEY (observer_id) REFERENCES observer(id);


--
-- Name: expert_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: monitor_result_ppe_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_ppe_id_fkey FOREIGN KEY (ppe_id) REFERENCES ppe(id);


--
-- Name: monitor_result_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id);


--
-- Name: monitor_result_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: monitor_result_violation_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_violation_id_fkey FOREIGN KEY (violation_id) REFERENCES violation(id);


--
-- Name: monitor_result_violator_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_violator_id_fkey FOREIGN KEY (violator_id) REFERENCES violator(id);


--
-- Name: oiv_gek_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY oiv_gek
    ADD CONSTRAINT oiv_gek_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);


--
-- Name: ppe_project_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_project_id_fkey FOREIGN KEY (project_id) REFERENCES period(id);


--
-- Name: ppe_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);


--
-- Name: ppe_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: rcoi_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY rcoi
    ADD CONSTRAINT rcoi_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);


--
-- Name: schedule_detail_ppe_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_ppe_id_fkey FOREIGN KEY (ppe_id) REFERENCES ppe(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_detail_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: schedule_expert_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_expert_id_fkey FOREIGN KEY (expert_id) REFERENCES expert(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

