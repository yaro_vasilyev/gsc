package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


class RoleEntryContainerImpl implements RoleEntryContainer {
  def roleEntries = [] // RoleEntry[]

  Collection<RoleEntry> getRoleEntries() {
    roleEntries
  }
}
