package org.bso.gsc

import groovy.transform.Immutable
import groovy.transform.ToString

/*
 * Created by yaro on 17.04.2016.
 */


@Immutable
@ToString(includeNames=true)
class GrantSpecImpl implements GrantSpec {
  final String target
  final GrantVerb verb

  static class Builder implements GrantSpecBuilder {
    String target
    GrantVerb verb

    GrantSpecBuilder target(String target) {
      this.target = target
      this
    }

    GrantSpecBuilder verb(GrantVerb verb) {
      this.verb = verb
      this
    }

    GrantSpec build() {
      new GrantSpecImpl(target: this.target, verb: this.verb)
    }
  }
}