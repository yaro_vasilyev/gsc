package org.bso.gsc.plugin

import org.bso.gsc.RoleEntry
import org.bso.gsc.RoleSetReader
import org.gradle.api.Plugin
import org.gradle.api.Project

/*
 * Created by yaro on 17.04.2016.
 */

class GscRolesPlugin implements Plugin<Project> {
  RoleSetReader roleEngine
  Collection<RoleEntry> roleSet
  String driver
  String url
  String user
  String password
  String schema
  boolean initialized = false

  final String CREATE_GRANT = "cg"
  final String REVOKE_DROP = "rd"


  def initialize(GscRolesPluginExtension roles) {
    if (!initialized) {
      initConnectionSettings roles
      initRoleSet roles
      initialized = true
    }
  }

  def initConnectionSettings(GscRolesPluginExtension roles) {
    def c = roles.connectionClosure
    if (c == null)
      throw new Exception("""You need to configure connection in build.gradle
                            |  like this:
                            | roles {
                            |   connection {
                            |     driver = <driver class name e.g. \"org.postgresql.Driver\">
                            |     url = <jdbc connection string>
                            |     user = <username>
                            |     password = <password>
                            |   }
                            |   ...
                            | }""".stripMargin())

    def clone = c.clone()
    clone.resolveStrategy = Closure.DELEGATE_ONLY
    clone.delegate = this
    clone()
  }

  void initRoleSet(GscRolesPluginExtension roles) {
    roleEngine = new RoleSetReader()
    roleSet = roleEngine.readRoleSet roles.roleSetClosure
  }

  @Override
  void apply(Project project) {
    project.extensions.create("roles", GscRolesPluginExtension)
    project.afterEvaluate {
      initialize(project.roles)

      roleSet.each { roleEntry ->
        generateCreateDropTasksCreate(project, roleEntry)
        generateCreateDropTasksDrop(project, roleEntry)
      }
    }
  }

  void generateCreateDropTasksCreate(Project project, RoleEntry roleEntry) {
    assert project != null
    assert roleEntry != null

    def task = findTask(project, roleEntry, CREATE_GRANT)
    if (!task) {
      task = createCreateTask(project, roleEntry)

      roleEntry.includes.each { include ->
        def includeRole = roleSet.find { it.name == include }
        def includeTask = findTask(project, includeRole, CREATE_GRANT)
        if (!includeTask) {
          includeTask = createCreateTask(project, includeRole)
        }
        task.dependsOn << includeTask
      }
    }
  }

  void generateCreateDropTasksDrop(Project project, RoleEntry roleEntry) {
    assert project != null
    assert roleEntry != null

    def task = findTask(project, roleEntry, REVOKE_DROP)
    if (!task) {
      task = createDropTask(project, roleEntry)

      roleEntry.includes.each { include ->
        def rolesThatDependOnThis = roleSet.findAll { re ->
          re.name == include
        }
        rolesThatDependOnThis.each { re ->
          def depTask = findTask(project, re, REVOKE_DROP)
          if (!depTask) {
            depTask = createDropTask(project, re)
          }
          depTask.dependsOn << task
        }
      }
    }
  }

  private static String makeCanonicalName(String operationType, RoleEntry role) {
    assert role != null
    def roleName = role.name
    roleName = roleName.toLowerCase()
    "$operationType-$roleName"
  }

  private static <T extends DbActionTask> T findTask(Project project,
                                                     RoleEntry roleEntry,
                                                     String namePrefix) {
    def canonicalName = makeCanonicalName(namePrefix, roleEntry)
    (T) project.tasks.findByPath("${project.path}$canonicalName")
  }

  private <T extends DbActionTask> T createTask(Project project,
                                                Class<T> taskClass,
                                                RoleEntry roleEntry,
                                                String namePrefix) {
    def canonicalName = makeCanonicalName(namePrefix, roleEntry)
    def task = (T) project.task(canonicalName, type: taskClass)
    task.roleName = roleEntry.name
    task.plugin = this
    task
  }

  private CreateRoleTask createCreateTask(Project project,
                                          RoleEntry roleEntry) {
    def task = createTask(project, CreateRoleTask, roleEntry, CREATE_GRANT)
    task.grants = roleEntry.getGrantsContainer().getGrants()
    task.description = "Creates ${roleEntry.name} role and grants privileges specified"
    task
  }

  private DropRoleTask createDropTask(Project project,
                                      RoleEntry roleEntry) {
    def task = createTask(project, DropRoleTask, roleEntry, REVOKE_DROP)
    task.description = "Revokes all privilges and drop role ${roleEntry.name}"
    task
  }
}


