package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


class RoleEntryContainerBuilderImpl implements RoleEntryContainerBuilder {
  def roleEntryBuilders = [] // RoleEntryBuilderImpl[]

  RoleEntryContainer build() {
    def container = new RoleEntryContainerImpl()
    roleEntryBuilders.each {
      container.roleEntries << it.build(this)
    }
    container
  }

  private def lastArg(args) {
    assert args != null
    assert args.size() > 0
    args[args.size() - 1]
  }

  def methodMissing(String name, args) {
    //assert args.size() == 1
    assert lastArg(args) instanceof Closure

    def clone = lastArg(args).clone()
    clone.resolveStrategy = Closure.DELEGATE_ONLY
    clone.delegate = new RoleEntryImpl.Builder(roleName: name)
    if (args.size() > 1) {
      clone.delegate.includes = (args - lastArg(args)).roleName
    }

    clone()
    roleEntryBuilders << clone.delegate
  }

  def getProperty(String name) {
    def builder = roleEntryBuilders.find { it.roleName == name }
    if (!builder)
      throw new MissingPropertyException("No Role with name $name defined.")

    builder
  }
}