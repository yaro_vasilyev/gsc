package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


@groovy.transform.ToString(includeNames = true)
class RoleEntryImpl implements RoleEntry {
  String name
  GrantSpecContainer grants
  def includes = []

  GrantSpecContainer getGrantsContainer() {
    grants
  }

  Collection<String> getIncludes() {
    includes
  }

  static class Builder implements RoleEntryBuilder {
    def grantSpecContainerBuilder = new GrantSpecContainerImpl.Builder()
    String roleName
    List<String> includes = []

    void select(String target) {
      addGrant(GrantVerb.SELECT, target)
    }

    void insert(String target) {
      addGrant(GrantVerb.INSERT, target)
    }

    void update(String target) {
      addGrant(GrantVerb.UPDATE, target)
    }

    void delete(String target) {
      addGrant(GrantVerb.DELETE, target)
    }

    void execute(String target) {
      addGrant(GrantVerb.EXECUTE, target)
    }

    private void addGrant(GrantVerb verb, String target) {
      def grantSpecBuilder = new GrantSpecImpl.Builder(verb: verb, target: target)
      grantSpecContainerBuilder.grantSpecBuilders << grantSpecBuilder
    }

    GrantSpecFluent grant(GrantVerb verb) {
      return new GrantSpecFluent(grantSpecContainerBuilder: this.grantSpecContainerBuilder, verb: verb)
    }


    RoleEntry build(RoleEntryContainerBuilder container) {
      assert container != null
      def roleEntry = new RoleEntryImpl(name: roleName)
      roleEntry.grants = grantSpecContainerBuilder.build()
      roleEntry.includes = includes

      roleEntry
    }
  }
}
