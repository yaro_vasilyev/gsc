package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


interface RoleEntryBuilder {
  void select(String target)
  void insert(String target)
  void update(String target)
  void delete(String target)
  void execute(String tartget)
  GrantSpecFluent grant(GrantVerb verb)
  RoleEntry build(RoleEntryContainerBuilder container)
}
