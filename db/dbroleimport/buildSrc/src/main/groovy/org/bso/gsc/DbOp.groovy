package org.bso.gsc

import groovy.sql.Sql

/*
 * Created by yaro on 18.04.2016.
 */

class DbOp {
  /**
   * Checks whether role {@code roleName} exists
   *
   * @param sql REQUIRED. {@link Sql} instance
   * @param roleName Role name to check
   * @return {@link java.lang.Boolean}
   */
  static boolean roleExists(Sql sql, String roleName) {
    assert sql != null
    def row = sql.firstRow('select 1 from pg_roles where rolname=?', [roleName])
    row != null
  }

  static def revokeRoleFrom(Sql sql, String roleToRevoke, String role) {
    assert sql != null
    sql.execute("revoke $roleToRevoke from $role".toString())
  }

  /**
   * Включает роль в другую роль.
   *
   * @param sql REQUIRED. {@link Sql} instance
   * @param role
   * @param roleToIncludeTo
   */
  static def grantRoleToRole(Sql sql, String roleToGrant, String targetRole) {
    assert sql != null
    sql.execute("grant $roleToGrant to $targetRole".toString())
  }

  /**
   * Exclude {@code role} from all other roles.
   *
   * @param sql REQUIRED. {@link Sql} instance.
   * @param role Role name
   */
  static def leaveAllRoles(Sql sql, String role) {
    assert sql != null
    def roles = getUserRoles(sql, role)
    roles.each { r ->
      revokeRoleFrom(sql, r, role)
    }
  }

  /**
   * Adds GRANT {@code verb} ON {@code target} TO {@code role}.
   *
   * @param sql REQUIRED. {@link Sql} instance.
   * @param verb {@link GrantVerb} SELECT/UPDATE/DELETE/etc.
   * @param target Database table/view/sequence/etc.
   * @param role Role name
   */
  static void grant(Sql sql, GrantVerb verb, String target, String role) {
    assert sql != null
    sql.execute("grant $verb on $target to $role".toString())
  }

  static def revoke(Sql sql, GrantVerb verb, String target, String role) {
    assert sql != null
    sql.execute("revoke $verb on $target from $role".toString())
  }

  static def revokeAll(Sql sql, String role, String schema) {
    assert sql != null
    sql.execute("revoke all on all tables in schema $schema from $role".toString())
    sql.execute("revoke all on all sequences in schema $schema from $role".toString())
    sql.execute("revoke all on all functions in schema $schema from $role".toString())
    sql.execute("revoke all on schema $schema from $role".toString())
  }

  /**
   * Creates role in database.
   *
   * @param sql REQUIRED. {@link Sql} instance.
   * @param role Role name
   */
  static def createRole(Sql sql, String role) {
    assert sql != null

    sql.execute("create role $role inherit".toString())
  }

  /**
   * Drops existing role.
   *
   * @param sql REQUIRED. {@link Sql} instance.
   * @param role Role to drop.
   */
  static def dropRole(Sql sql, String role, boolean dropIfExists = false) {
    assert sql != null
    sql.execute("drop role ${dropIfExists ? "if exists" : ""} $role".toString())
  }

  static String[] getUserRoles(Sql sql, String role) {
    assert sql != null

    def roles = sql.rows("""
select r2.rolname
from pg_auth_members as mem
join pg_roles as r1 on r1.oid = mem.member
join pg_roles as r2 on r2.oid = mem.roleid
where r1.rolname = $role""").collect { it.rolname }.toArray()
    roles
  }

  static String[] getUserRolesInherited(Sql sql, String role) {
    assert sql != null

    def roles = sql.rows("""
WITH RECURSIVE cte AS (
  SELECT oid FROM pg_roles WHERE rolname = $role
  UNION ALL
  SELECT m.roleid
  FROM   cte
  JOIN   pg_auth_members m ON m.member = cte.oid
)
SELECT cte.oid, rolname
FROM   cte
JOIN pg_roles ON pg_roles.oid = cte.oid
WHERE rolname <> $role""").collect { it.rolname }.toArray()

    roles
  }
}
