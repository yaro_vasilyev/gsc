--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = gsc, pg_catalog;

--
-- Data for Name: academic_degree; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY academic_degree (code, name, id) FROM stdin;
1	Доктор наук	1
2	Кандидат наук	2
\.


--
-- Name: academic_degree_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('academic_degree_id_seq', 2, true);


--
-- Data for Name: academic_title; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY academic_title (code, name, default_val, id) FROM stdin;
01	Академик Российской Академии Наук	5	1
02	Академик международной академии наук	4	2
03	Академик отраслевой академии наук	4	3
04	Член-корреспондент Российской Академии Наук	4	4
05	Член-корреспондент международной академии наук	3	5
\.


--
-- Name: academic_title_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('academic_title_id_seq', 5, true);


--
-- Data for Name: contract_info; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY contract_info (id, number, contract_date) FROM stdin;
\.


--
-- Name: contract_info_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('contract_info_id_seq', 1, false);


--
-- Data for Name: observer; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY observer (id, type, fio, wp, post, phone1, phone2, email) FROM stdin;
1	1	Корогод Максим Анатольевич	МИАЦ	заместитель директора	1111111111111111	2222222222222222	korogod@mail.ru
\.


--
-- Data for Name: period; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY period (id, title, status) FROM stdin;
1	(default)	1
\.


--
-- Data for Name: region; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY region (code, name, zone, id) FROM stdin;
23	Краснодарский край	1	1
28	Амурская область	2	2
\.


--
-- Data for Name: expert; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY expert (id, period_id, import_code, surname, name, patronymic, workplace, post, post_value, birth_date, birth_place, base_region_id, live_address, phone, email, academic_degree_value, academic_title_value, expirience, speciality, speciality_value, work_years_total, work_years_education, work_years_education_value, work_years_chief, exp_years_education, exp_years_education_value, awards, awards_value, can_mission, can_mission_value, chief_name, chief_post, chief_email, observer_id, kind, test_results, test_results_value, interview_status, interview_status_value, stage1_points, engage, subject_level_value, accuracy_level_value, punctual_level_value, independency_level_value, moral_level_value, docs_impr_part_level_value, stage2_points, efficiency, next_period_proposal, academic_title_id, academic_degree_id, ts) FROM stdin;
1	1	\N	Иванов	Иван	Иванович	ФЦТ	инженер	4	1983-06-21	г. Краснодар	1	г. Краснодар	2222222222	ivanov@mail.ru	5	4	нет	медицина	5	10	3	1	1	1	1	не	5	1	5	Рубцова И.Т.	директор	rub@mail.ru	1	1	15	2	1	5	37	0	5	5	5	5	5	0	62	3	1	4	1	2016-02-16 23:19:38.005761
\.


--
-- Data for Name: subject; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY subject (id, name, code) FROM stdin;
1	Математика	1
2	Русский язык	2
\.


--
-- Data for Name: ppe; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY ppe (id, region_id, exam_date, subject_id, ppe_type, code, name, address, count_part, count_room, count_room_video, kim, tom, project_id) FROM stdin;
1	2	2016-03-01	1	1	1008	МОАУ СОШ №26	675000, г. Благовещенск, ул. Комсомольская, д. 21	10	1	1	0	0	1
2	2	2016-03-01	2	1	1000	МОАУ Гимназия №1	675000, г. Благовещенск, ул. Калинина, д. 13	5	2	1	0	0	1
\.


--
-- Data for Name: schedule; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY schedule (id, expert_id, period_id, monitor_rcoi, fixed, period_type, region_id) FROM stdin;
1	1	1	1	0	2	2
\.


--
-- Data for Name: violation; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY violation (id, code, name) FROM stdin;
\.


--
-- Data for Name: violator; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY violator (id, code, name) FROM stdin;
\.


--
-- Data for Name: monitor_result; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY monitor_result (id, location, schedule_id, exam_date, ppe_id, violation_id, violator_id, note, subject_id) FROM stdin;
\.


--
-- Data for Name: event; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY event (id, ts, channel, location, type, schedule_id, monitor_result_id) FROM stdin;
\.


--
-- Name: event_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('event_id_seq', 1, false);


--
-- Data for Name: exam; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY exam (id, period_type, exam_date, subject_id, exam_type, period_id) FROM stdin;
1	2	2016-03-01	1	1	1
2	2	2016-03-01	2	1	1
\.


--
-- Name: exam_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('exam_id_seq', 2, true);


--
-- Name: expert_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('expert_id_seq', 1, true);


--
-- Data for Name: imp_academic_degree; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY imp_academic_degree (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Data for Name: imp_academic_title; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_academic_title (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Data for Name: imp_contract_info; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_contract_info (imp_session_guid, imp_user, imp_datetime, imp_status, number, contract_date) FROM stdin;
\.


--
-- Data for Name: imp_subject; Type: TABLE DATA; Schema: gsc; Owner: postgres
--

COPY imp_subject (imp_session_guid, imp_user, imp_datetime, imp_status, code, name) FROM stdin;
\.


--
-- Name: monitor_result_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('monitor_result_id_seq', 1, false);


--
-- Name: observer_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('observer_id_seq', 1, true);


--
-- Data for Name: oiv_gek; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY oiv_gek (id, region_id, oiv_fio, oiv_wp, oiv_post, oiv_phone1, oiv_email, gek_fio, gek_wp, gek_post, gek_phone1, gek_email, gia_fio, gia_wp, gia_post, gia_phone1, gia_email, nadzor_fio, nadzor_wp, nadzor_post, nadzor_phone1, nadzor_email, oiv_phone2, gek_phone2, gia_phone2, nadzor_phone2) FROM stdin;
\.


--
-- Name: oiv_gek_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('oiv_gek_id_seq', 1, false);


--
-- Name: period_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('period_seq', 1, true);


--
-- Name: ppe_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('ppe_id_seq', 2, true);


--
-- Data for Name: rcoi; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY rcoi (id, region_id, name, address, chief_post, chief_fio, chief_phone1, chief_phone2, email) FROM stdin;
\.


--
-- Name: rcoi_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('rcoi_id_seq', 1, false);


--
-- Name: region_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('region_id_seq', 2, true);


--
-- Data for Name: schedule_detail; Type: TABLE DATA; Schema: gsc; Owner: gsc
--

COPY schedule_detail (id, schedule_id, ppe_id) FROM stdin;
1	1	1
\.


--
-- Name: schedule_detail_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('schedule_detail_id_seq', 1, true);


--
-- Name: schedule_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('schedule_id_seq', 1, true);


--
-- Name: subject_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('subject_id_seq', 2, true);


--
-- Name: violation_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('violation_id_seq', 1, false);


--
-- Name: violator_id_seq; Type: SEQUENCE SET; Schema: gsc; Owner: gsc
--

SELECT pg_catalog.setval('violator_id_seq', 1, false);


SET search_path = public, pg_catalog;

--
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	001	Default schema	SQL	V001__Default_schema.sql	-466448534	postgres	2016-02-16 21:02:58.61588	0	t
2	2	002	Database extensions	SQL	V002__Database_extensions.sql	-1626690616	postgres	2016-02-16 21:02:58.705751	16	t
3	3	003	Period table and CRUD functions	SQL	V003__Period_table_and_CRUD_functions.sql	1802100490	postgres	2016-02-16 21:02:58.730629	62	t
4	4	004	AcademicDegree table	SQL	V004__AcademicDegree_table.sql	903770476	postgres	2016-02-16 21:02:58.809181	15	t
5	5	005	AcademicTitle table	SQL	V005__AcademicTitle_table.sql	1822836815	postgres	2016-02-16 21:02:58.845261	31	t
6	6	006	ContractInfo table	SQL	V006__ContractInfo_table.sql	723045312	postgres	2016-02-16 21:02:58.885344	16	t
7	7	007	CRUD concurrency control	SQL	V007__CRUD_concurrency_control.sql	1475812455	postgres	2016-02-16 21:02:58.917083	0	t
8	8	008	Subject table	SQL	V008__Subject_table.sql	-292211016	postgres	2016-02-16 21:02:58.934137	31	t
9	9	009	Exam table	SQL	V009__Exam_table.sql	-1881247201	postgres	2016-02-16 21:02:58.981358	15	t
10	10	010	Region table and CRUD functions	SQL	V010__Region_table_and_CRUD_functions.sql	1481958648	postgres	2016-02-16 21:02:59.013618	16	t
11	11	011	Observer table	SQL	V011__Observer_table.sql	1635791693	postgres	2016-02-16 21:02:59.043569	16	t
12	12	012	Rcoi table	SQL	V012__Rcoi_table.sql	-328552587	postgres	2016-02-16 21:02:59.076118	16	t
13	13	013	OivGek table	SQL	V013__OivGek_table.sql	1686778035	postgres	2016-02-16 21:02:59.110471	31	t
14	14	014	Violation table	SQL	V014__Violation_table.sql	-1539567581	postgres	2016-02-16 21:02:59.150045	15	t
15	15	015	Violator table	SQL	V015__Violator_table.sql	413058800	postgres	2016-02-16 21:02:59.181777	15	t
16	16	016	Ppe table	SQL	V016__Ppe_table.sql	461331289	postgres	2016-02-16 21:02:59.211578	32	t
17	17	017	Expert table	SQL	V017__Expert_table.sql	-280084904	postgres	2016-02-16 21:02:59.251016	47	t
18	18	018	Expert update trigger	SQL	V018__Expert_update_trigger.sql	-833750772	postgres	2016-02-16 21:02:59.308969	0	t
19	19	019	Expert calc functions	SQL	V019__Expert_calc_functions.sql	286562285	postgres	2016-02-16 21:02:59.321778	31	t
20	20	020	Expert CRUD functions	SQL	V020__Expert_CRUD_functions.sql	1819591845	postgres	2016-02-16 21:02:59.350531	78	t
21	21	021	ImportStatus enum	SQL	V021__ImportStatus_enum.sql	1042795297	postgres	2016-02-16 21:02:59.42644	0	t
22	22	022	AcademicDegree import	SQL	V022__AcademicDegree_import.sql	-803726165	postgres	2016-02-16 21:02:59.440591	31	t
23	23	023	Schedule ScheduleDetail tables	SQL	V023__Schedule_ScheduleDetail_tables.sql	-837495511	postgres	2016-02-16 21:02:59.474164	47	t
24	24	024	Schedule functions	SQL	V024__Schedule_functions.sql	-1999509820	postgres	2016-02-16 21:02:59.519731	16	t
25	25	025	Domains for call center module	SQL	V025__Domains_for_call_center_module.sql	1867477152	postgres	2016-02-16 21:02:59.535066	0	t
26	26	026	MonitorResult table	SQL	V026__MonitorResult_table.sql	-1065594702	postgres	2016-02-16 21:02:59.548803	31	t
27	27	027	Event table	SQL	V027__Event_table.sql	-1361673360	postgres	2016-02-16 21:02:59.589197	16	t
28	28	028	AcademicTitle import	SQL	V028__AcademicTitle_import.sql	847447905	postgres	2016-02-16 21:02:59.615944	31	t
29	29	029	ContractInfo import	SQL	V029__ContractInfo_import.sql	-1145986675	postgres	2016-02-16 21:02:59.647734	31	t
30	30	030	Subject import	SQL	V030__Subject_import.sql	1369513444	postgres	2016-02-16 21:02:59.680404	31	t
\.


--
-- PostgreSQL database dump complete
--

