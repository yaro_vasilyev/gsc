--
-- Name: _clean_import(); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION _clean_import() RETURNS void
    LANGUAGE plpgsql
    AS $$

BEGIN

  truncate gsc.imp_academic_degree;

  truncate gsc.imp_academic_title;

  truncate gsc.imp_subject;

  truncate gsc.imp_contract_info;

  truncate gsc.contract_info;

END;

$$;


ALTER FUNCTION gsc._clean_import() OWNER TO postgres;

--
-- Name: cc_expert_reports_count(integer, integer); Type: FUNCTION; Schema: gsc; Owner: postgres
--

