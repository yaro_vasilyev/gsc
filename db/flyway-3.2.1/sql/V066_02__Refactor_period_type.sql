set search_path = gsc,public;

create domain gsc.d_period_type as integer default 2 constraint d_period_type_chk check (value in (1,2,3));
alter domain gsc.d_period_type owner to gsc;
alter table gsc.exam alter column period_type type gsc.d_period_type;
drop view gsc.wa_v_schedule;
alter table gsc.schedule alter column period_type type gsc.d_period_type;
alter table gsc.contract alter column period_type type gsc.d_period_type;

create view gsc.wa_v_schedule as
  select sch.expert_id,
         sch.monitor_rcoi,
         sch.id as schedule_id,
         sch.period_type,
         r.name as region_name,
         schd.id as schedule_detail_id,
         schd.exam_date,
         ec.id as exec_id,
         em.exam_type,
         st.id as station_id,
         st.name as station_name,
         st.code as station_code,
         sub.name as subject_name
  from gsc.schedule sch
       join gsc.schedule_detail schd on sch.id = schd.schedule_id
       join gsc.station st on st.id = schd.station_id
       join gsc.exec ec on st.id = ec.station_id
       join gsc.exam em on em.id = ec.exam_id and schd.exam_date::date = em.exam_date::date
       join gsc.subject sub on sub.id = em.subject_id
       join gsc.region r on r.id = sch.region_id;
alter view gsc.wa_v_schedule owner to gsc;
