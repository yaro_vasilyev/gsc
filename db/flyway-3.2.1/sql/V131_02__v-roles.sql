CREATE VIEW gsc.v_roles (
	role,
  name
) 
AS 
SELECT r.rolname AS role,
	pg_catalog.shobj_description(r.oid, 'pg_authid') AS name
FROM pg_catalog.pg_roles r
WHERE rolname LIKE 'r_%' AND r.rolcanlogin IS FALSE;

ALTER TABLE gsc.v_roles
  OWNER TO gsc;