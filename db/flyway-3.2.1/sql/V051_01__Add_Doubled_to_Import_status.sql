-- create new type with new status and migrate columns to it, cause [alter type add] doesn't support transactions

ALTER TYPE gsc.import_status RENAME TO stale_import_status;

CREATE TYPE gsc.import_status AS ENUM(
  'added',
  'imported',
  'error',
  'doubled'
);

ALTER TABLE gsc.imp_academic_degree ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_academic_degree ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_academic_degree ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_academic_title ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_academic_title ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_academic_title ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_contract_info ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_contract_info ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_contract_info ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_exam ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_exam ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_exam ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_exec ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_exec ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_exec ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_expert ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_expert ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_expert ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_rcoi ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_rcoi ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_rcoi ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_region ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_region ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_region ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_station ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_station ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_station ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_subject ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_subject ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_subject ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_violation ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_violation ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_violation ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

ALTER TABLE gsc.imp_violator ALTER COLUMN imp_status DROP DEFAULT;
ALTER TABLE gsc.imp_violator ALTER COLUMN imp_status TYPE gsc.import_status USING imp_status::text::gsc.import_status;
ALTER TABLE gsc.imp_violator ALTER COLUMN imp_status SET DEFAULT 'added'::gsc.import_status;

DROP TYPE gsc.stale_import_status;