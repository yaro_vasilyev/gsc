set search_path = gsc,public;

alter table event 
  drop column location, 
  add column object_type gsc.d_visit_object_type not null;

alter table event 
  alter column schedule_id set not null;

drop domain d_violation_location;