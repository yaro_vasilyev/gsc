set search_path = gsc,public;

create or replace function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  returns table (     region_name        gsc.region.name%type,
                      date               gsc.monitor_result.date%type,
                      subjects           text,
                      station_etc        text,
                      violation_code     gsc.violation.code%type,
                      violator_code      gsc.violator.code%type,
                      note               gsc.monitor_result.note%type)
  stable
  called on null input
  security definer
  language plpgsql
as $$
-- 
-- DESCRIPTION:
--    Function for report GSC-96
-- PARAMETERS
--    p_project_id        REQUIRED. Current project
--    p_exam_type         OPTIONAL. EGE/GVE/OGE
--    p_date              OPTIONAL. Date
--
begin
  return query
    with cte_mr_station as (
      select distinct
             exec.station_id,
             mr_detail.monitor_result_id
        from gsc.mr_detail
        join gsc.exec on mr_detail.exec_id = exec.id
    ),
    cte_mr_subjects as (
      select monitor_result.id as monitor_result_id,
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        left join gsc.mr_detail on monitor_result.id = mr_detail.monitor_result_id
        left join gsc.exec on mr_detail.exec_id = exec.id
        left join gsc.exam on exec.exam_id = exam.id
        left join gsc.subject on exam.subject_id = subject.id
       where monitor_result.object_type = 1
         and schedule.period_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       group by monitor_result.id
             union all
      select monitor_result.id as monitor_result_id, 
             null as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
       where monitor_result.object_type <> 1
         and schedule.period_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
    )
    select region.name as region_name,
           monitor_result.date,
           cte_mr_subjects.subjects,
           case monitor_result.object_type
              when 1 then coalesce (station.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violator.code as violator_code,
           monitor_result.note
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.region on region.id = schedule.region_id
      left join cte_mr_subjects on cte_mr_subjects.monitor_result_id = monitor_result.id
      left join cte_mr_station on cte_mr_station.monitor_result_id = monitor_result.id
      left join gsc.station on station.id = cte_mr_station.station_id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
     where schedule.period_id = p_project_id
       and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       and (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  owner to gsc;
  
grant execute on function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  to s_repmr_module;