set search_path = gsc,public;

create or replace function gsc.u_create_password (p_user_login text, p_pwd_plain text)
  returns text
  language plpgsql
  immutable
  security definer
as $$
declare
  ret     text;
begin
  ret := 'md5' || md5(p_pwd_plain || p_user_login);
  return ret;
end;
$$;

alter function gsc.u_create_password(text, text)
  owner to gsc;


create or replace function gsc.u_create_db_user (
    p_rolname       pg_roles.rolname%type,
    p_pwd_plain     text,
    variadic        p_roles   name[])
  returns void
  security invoker
  volatile
  language plpgsql
as $$
-- Создает логин/пароль на базе, входящий в роли (список ролей прилагается)
declare 
  r name;
  l_create_role text := '';
begin
  if role_admin() = any(p_roles) then
    l_create_role := 'createrole';
  end if;

  execute format('create role %I login %s password ''%s'';', p_rolname, l_create_role, p_pwd_plain);
  foreach r in array p_roles loop
    execute format('grant %I to %I', r, p_rolname);
  end loop;
end;
$$;

alter function gsc.u_create_db_user (pg_roles.rolname%type, text, variadic name[])
  owner to gsc;
  


create or replace function gsc.u_register_db_user (
    p_rolname       pg_roles.rolname%type)
  returns gsc.users.guid%type
  security invoker
  language plpgsql
as $$
-- Добавляет юзера базы в gsc.users. Юзер базы должен существовать.
declare
  ret       uuid;
begin
  if not exists(select 1 from pg_roles where rolname = p_rolname) then
    raise exception 'Пользователь базы данных % не существует', p_rolname;
  end if;

  insert into gsc.users (
              user_login, 
              pass, 
              guid, 
              rolname)
      values (p_rolname,
              '',
              uuid_generate_v4(),
              p_rolname)
    returning guid
         into ret;

  return ret;
end;
$$;

alter function gsc.u_register_db_user (pg_roles.rolname%type)
  owner to gsc;


create or replace function gsc.u_bind_observer (
    p_guid          gsc.users.guid%type,
    p_observer_id   gsc.observer.id%type)
  returns void
  security definer
  language plpgsql
as $$
-- Привязывает id менеджера/координатора к юзеру
begin
  update gsc.observer
     set user_guid = p_guid
   where id = p_observer_id;

  if not found then
    raise exception 'Менеджер/координатор % не существует.', p_observer_id;
  end if;
end;
$$;

alter function gsc.u_bind_observer (gsc.users.guid%type, gsc.observer.id%type)
  owner to gsc;


create or replace function gsc.u_drop_password(p_guid gsc.users.guid%type)
  returns void
  security invoker
  volatile
  language plpgsql
as $$
-- Сбрасывает пароль пользователя (делая невозможным веб-логин пользователя)
begin
  update gsc.users
     set pass = ''
   where guid = p_guid;

  if not found then
    raise exception 'Пользователь % не существует', p_guid;
  end if;
end;
$$;

alter function gsc.u_drop_password(gsc.users.guid%type)
  owner to gsc;



create or replace function gsc.u_set_password (
    p_guid              gsc.users.guid%type,
    p_pwd_plain         text)
  returns void
  security invoker
  volatile
  language plpgsql
as $$
-- Устанавливает пароль для веб-входа пользователя
begin
  update gsc.users
     set pass = gsc.u_create_password(user_login, p_pwd_plain)
   where guid = p_guid;

  if not found then
    raise exception 'Пользователь % не существует.', p_guid;
  end if;
end;
$$;

alter function gsc.u_set_password(gsc.users.guid%type, text)
  owner to gsc;



create or replace function gsc.u_bind_expert (
    p_guid          gsc.users.guid%type,
    p_expert_id     gsc.expert.id%type)
  returns void
  security definer
  volatile
  language plpgsql
as $$
-- Привязывает id эксперта к юзеру
begin
  update gsc.expert
     set user_guid = p_guid
   where id = p_expert_id;

  if not found then
    raise exception 'Эксперт % не существует.', p_expert_id;
  end if;
end;
$$;

alter function gsc.u_bind_expert (gsc.users.guid%type, gsc.expert.id%type)
  owner to gsc;


create or replace function gsc.u_role_translate(text)
  returns text
  immutable
  returns null on null input
  security definer
  language sql
as $$
  select case $1
            when role_expert()        then 'Эксперт'
            when role_rsm()           then 'РСМ-координатор'
            when role_manager()       then 'Менеджер'
            when role_an()            then 'Аналитик'
            when role_an_lead()       then 'Ведущий аналитик'
            when role_boss()          then 'Руководитель'
            when role_admin()         then 'Администратор'
            else $1
        end;
$$;

alter function gsc.u_role_translate(text)
  owner to gsc;
