set search_path = gsc,public;

alter table gsc.rcoi
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger rcoi_bu before update
  on gsc.rcoi for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_rcoi
as
  select * from gsc.rcoi;

alter view gsc.v_rcoi
  owner to gsc;


create or replace function gsc.rcoi_insert (
    region_id           gsc.rcoi.region_id%type,
    name                gsc.rcoi.name%type,
    address             gsc.rcoi.address%type,
    chief_post          gsc.rcoi.chief_post%type,
    chief_fio           gsc.rcoi.chief_fio%type,
    chief_phone1        gsc.rcoi.chief_phone1%type,
    chief_phone2        gsc.rcoi.chief_phone2%type,
    email               gsc.rcoi.email%type)
  returns table(id gsc.rcoi.id%type, ts gsc.rcoi.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id    gsc.rcoi.id%type;
  l_ts    gsc.rcoi.ts%type;
begin
  insert into gsc.rcoi (
              region_id,
              name,
              address,
              chief_post,
              chief_fio,
              chief_phone1,
              chief_phone2,
              email)
      values (region_id,
              name,
              address,
              chief_post,
              chief_fio,
              chief_phone1,
              chief_phone2,
              email)
    returning rcoi.id,
              rcoi.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.rcoi_insert (
    region_id           gsc.rcoi.region_id%type,
    name                gsc.rcoi.name%type,
    address             gsc.rcoi.address%type,
    chief_post          gsc.rcoi.chief_post%type,
    chief_fio           gsc.rcoi.chief_fio%type,
    chief_phone1        gsc.rcoi.chief_phone1%type,
    chief_phone2        gsc.rcoi.chief_phone2%type,
    email               gsc.rcoi.email%type)
  owner to gsc;



create or replace function gsc.rcoi_update (
    p_id                    gsc.rcoi.id%type,
    new_region_id           gsc.rcoi.region_id%type,
    new_name                gsc.rcoi.name%type,
    new_address             gsc.rcoi.address%type,
    new_chief_post          gsc.rcoi.chief_post%type,
    new_chief_fio           gsc.rcoi.chief_fio%type,
    new_chief_phone1        gsc.rcoi.chief_phone1%type,
    new_chief_phone2        gsc.rcoi.chief_phone2%type,
    new_email               gsc.rcoi.email%type,
    old_ts                  gsc.rcoi.ts%type)
  returns table (ts gsc.rcoi.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.rcoi.ts%type;
begin
  update gsc.rcoi
     set region_id = new_region_id,
         name = new_name,
         address = new_address,
         chief_post = new_chief_post,
         chief_fio = new_chief_fio,
         chief_phone1 = new_chief_phone1,
         chief_phone2 = new_chief_phone2,
         email = new_email
   where id = p_id
     and rcoi.ts = old_ts
         returning rcoi.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('РЦОИ');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.rcoi_update (
    p_id                    gsc.rcoi.id%type,
    new_region_id           gsc.rcoi.region_id%type,
    new_name                gsc.rcoi.name%type,
    new_address             gsc.rcoi.address%type,
    new_chief_post          gsc.rcoi.chief_post%type,
    new_chief_fio           gsc.rcoi.chief_fio%type,
    new_chief_phone1        gsc.rcoi.chief_phone1%type,
    new_chief_phone2        gsc.rcoi.chief_phone2%type,
    new_email               gsc.rcoi.email%type,
    old_ts                  gsc.rcoi.ts%type)
  owner to gsc;
  

create or replace function gsc.rcoi_delete (p_id gsc.rcoi.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.rcoi
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('РЦОИ');
  end if;
end;
$$;

alter function gsc.rcoi_delete (p_id gsc.rcoi.id%type)
  owner to gsc;