CREATE OR REPLACE FUNCTION gsc.import_contract_info (
  p_imp_session_guid uuid
)
RETURNS TABLE (
  number varchar,
  contract_date date
) AS
$body$
BEGIN
  --// todo check import right (grant)

  -- import rows
  WITH imported AS (
    INSERT INTO contract_info AS ci (number, contract_date)
      SELECT
        ici.number,
        ici.contract_date
      FROM imp_contract_info ici
      WHERE ici.imp_session_guid = p_imp_session_guid AND ici.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT contract_info_pkey
      DO UPDATE SET contract_date = EXCLUDED.contract_date
    RETURNING ci.number)
    /* update import status from returning result */
  UPDATE gsc.imp_contract_info ici
  SET imp_status = 'imported'::gsc.import_status
  WHERE ici.imp_session_guid = p_imp_session_guid and ici.number = ANY(select i.number from imported i);

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ci.number,
                 ci.contract_date
               FROM gsc.contract_info ci
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_contract_info ici
                                WHERE ici.imp_session_guid = p_imp_session_guid AND
                                      ici.number = ci.number);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;