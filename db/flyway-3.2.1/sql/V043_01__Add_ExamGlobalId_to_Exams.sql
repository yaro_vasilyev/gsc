-- Добавляем колонку ExamGlobalID
ALTER TABLE gsc.exam
  ADD COLUMN ExamGlobalID INTEGER;
	
-- Добавляем уникальный ключ
ALTER TABLE gsc.exam
  ADD UNIQUE (period_id, ExamGlobalID);