CREATE OR REPLACE FUNCTION gsc.u_register_db_user (
  p_rolname name
)
RETURNS uuid AS
$body$
-- Добавляет юзера базы в gsc.users. Юзер базы должен существовать.
declare
  ret       uuid;
begin
  if not exists(select 1 from pg_roles where rolname = p_rolname) then
    raise exception 'Пользователь базы данных % не существует', p_rolname;
  end if;

  insert into gsc.users (
              user_login, 
              pass, 
              guid, 
              rolname)
      values (p_rolname,
              '',
              public.uuid_generate_v4(),
              p_rolname)
    returning guid
         into ret;

  return ret;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;