CREATE OR REPLACE FUNCTION gsc.import_rcoi (
  p_imp_session_guid uuid
)
RETURNS TABLE (
  code integer,
  name varchar
) AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  --// todo check import right

  -- import rows
  WITH imported AS (
    INSERT INTO gsc.rcoi AS r(region_id, NAME, address, chief_post, chief_fio, chief_phone1, email)
      SELECT imp.region_id,
             imp.name,
             imp.address,
             imp.chief_post,
             imp.chief_fio,
             imp.chief_phone1,
             imp.chief_email
      FROM gsc.imp_rcoi imp
    WHERE imp.imp_session_guid = p_imp_session_guid AND
          imp.imp_status = 'added'::gsc.import_status
    ON conflict (id) DO UPDATE
    	SET NAME = excluded.name
      returning r.region_id)
    /* update import status */
    UPDATE gsc.imp_rcoi imp
    SET imp_status = 'imported'::gsc.import_status
    WHERE imp.imp_session_guid = p_imp_session_guid AND
          imp.region_id = ANY (select i.region_id from imported i);

    -- return records presence in table but missing from import
    RETURN query
    SELECT rg.code,
           rc.name
    FROM gsc.rcoi rc
         LEFT JOIN gsc.region rg ON rc.region_id = rg.id
    WHERE NOT EXISTS (
                       SELECT NULL
                       FROM gsc.imp_rcoi imp
                       WHERE imp.imp_session_guid = p_imp_session_guid AND
                             rc.region_id = imp.region_id
          );
EXCEPTION
	WHEN others THEN
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION 'Ошибка БД: %', l_error_detail;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;


ALTER FUNCTION "gsc"."import_rcoi"(p_imp_session_guid uuid)
  OWNER TO gsc;