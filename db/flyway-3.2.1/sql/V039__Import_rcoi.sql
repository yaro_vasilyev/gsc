-- Убираем обязательность с морбильного номера телефона, т.к. его нет вимпортируемом CSV
ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone2 DROP NOT NULL;

-- Import table
CREATE TABLE gsc.imp_rcoi (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
  imp_region INTEGER NOT NULL,
  region_id INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  address VARCHAR(255) NOT NULL,
  chief_post VARCHAR(255) NOT NULL,
  chief_fio VARCHAR(255) NOT NULL,
  chief_phone1 VARCHAR(255) NOT NULL,
  chief_email VARCHAR(255) NOT NULL,
  CONSTRAINT imp_rcoi_pkey PRIMARY KEY(imp_session_guid, imp_region),
  CONSTRAINT imp_rcoi_region_id_fkey FOREIGN KEY (region_id)
    REFERENCES gsc.region(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE
) 
WITH (oids = false);

COMMENT ON TABLE gsc.imp_rcoi
IS 'RCOI import table.';

COMMENT ON COLUMN gsc.imp_rcoi.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_rcoi.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_rcoi.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_rcoi.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_rcoi.imp_region
IS 'Region code.';

COMMENT ON COLUMN gsc.imp_rcoi.region_id
IS 'Region id - FK on regions table.';

COMMENT ON COLUMN gsc.imp_rcoi.name
IS 'RCOI name.';

COMMENT ON COLUMN gsc.imp_rcoi.address
IS 'RCOI address.';

COMMENT ON COLUMN gsc.imp_rcoi.chief_post
IS 'RCOI chief post.';

COMMENT ON COLUMN gsc.imp_rcoi.chief_fio
IS 'RCOI chief fio.';

COMMENT ON COLUMN gsc.imp_rcoi.chief_phone1
IS 'RCOI chief work phone.';

COMMENT ON COLUMN gsc.imp_rcoi.chief_email
IS 'RCOI chief email.';

-- Import row function
CREATE OR REPLACE FUNCTION gsc.import_row_rcoi (
  imp_session_guid uuid,
  imp_region integer,
  name varchar,
  address varchar,
  chief_post varchar,
  chief_fio varchar,
  chief_phone1 varchar,
  chief_email varchar
)
RETURNS void AS
$body$
DECLARE
  l_region_id integer;
	l_error_detail text;
BEGIN
  -- get region
  SELECT r.id
  INTO l_region_id
  FROM gsc.region r
  WHERE r.code = imp_region;
  if l_region_id is null then
    RAISE EXCEPTION 'Регион с кодом "%" не найден.', imp_region
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите регионы и повторите импорт.';
  end if; 

  INSERT INTO gsc.imp_rcoi (imp_session_guid, imp_region, region_id, name, address, chief_post, chief_fio, chief_phone1, chief_email)
  VALUES (imp_session_guid, imp_region, l_region_id, name, address, chief_post, chief_fio, chief_phone1, chief_email);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', imp_region,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-- Bulk import function
CREATE OR REPLACE FUNCTION gsc.import_rcoi (
  p_imp_session_guid uuid
)
RETURNS TABLE (
  code integer,
  name varchar
) AS
$body$
DECLARE
  l_imported INTEGER [ ];
BEGIN
  --// todo check import right

  -- import rows
  WITH imported AS (
  INSERT INTO rcoi AS r(region_id, NAME, address, chief_post, chief_fio, chief_phone1, email)
  SELECT imp.region_id,
         imp.name,
         imp.address,
         imp.chief_post,
         imp.chief_fio,
         imp.chief_phone1,
         imp.chief_email
  FROM gsc.imp_rcoi imp
  WHERE imp.imp_session_guid = p_imp_session_guid AND
        imp.imp_status = 'added' ON conflict ON CONSTRAINT rcoi_pkey DO
  UPDATE
  SET NAME = excluded.name returning r.region_id)
    SELECT array_agg(
        i.region_id)
    FROM imported i
    INTO l_imported;

    -- update import status

    UPDATE gsc.imp_rcoi imp
    SET imp_status = 'imported'
    WHERE imp.imp_session_guid = p_imp_session_guid AND
          imp.region_id = ANY (l_imported);

    -- return records presence in table but missing from import
    RETURN query
    SELECT rg.code,
           rc.name
    FROM gsc.rcoi rc
         LEFT JOIN gsc.region rg ON rc.region_id = rg.id
    WHERE NOT EXISTS (
                       SELECT NULL
                       FROM gsc.imp_rcoi imp
                       WHERE imp.imp_session_guid = p_imp_session_guid AND
                             rc.region_id = imp.region_id
          );
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;