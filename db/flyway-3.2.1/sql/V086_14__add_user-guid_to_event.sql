set search_path = gsc,public;

alter table gsc.event
  add column user_guid uuid,
  add constraint event_user_guid_fkey foreign key (user_guid) references gsc.users(guid) on update cascade on delete set null;

update gsc.event as e
   set user_guid = (select u.guid from gsc.users as u where u.id = e.user_id);

update gsc.event
   set user_id = null;

/*
alter table gsc.event
  alter column user_guid set not null;
*/

