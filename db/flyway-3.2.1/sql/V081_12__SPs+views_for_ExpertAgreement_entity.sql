set search_path = gsc,public;


alter table gsc.expert_agreement
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create or replace view gsc.v_expert_agreement
as
  select a.* 
    from gsc.expert_agreement as a
    join gsc.v_expert as e on e.id = a.expert_id;

alter view gsc.v_expert_agreement
  owner to gsc;


create or replace function gsc.expert_agreement_insert (
    expert_id           gsc.expert_agreement.expert_id%type,
    filename            gsc.expert_agreement.filename%type,
    blob                gsc.expert_agreement.blob%type)
  returns table (ts gsc.expert_agreement.ts%type, expert_agreement_ts gsc.expert_agreement.expert_agreement_ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts        gsc.expert_agreement.ts%type;
  l_ea_ts     gsc.expert_agreement.expert_agreement_ts%type;
begin
  insert into gsc.expert_agreement (
              expert_id,
              filename,
              blob)
      values (expert_id,
              filename,
              blob)
    returning expert_agreement.ts,
              expert_agreement.expert_agreement_ts
         into l_ts,
              l_ea_ts;
  return query select l_ts as ts, l_ea_ts as expert_agreement_ts;
end;
$$;

alter function gsc.expert_agreement_insert (
    expert_id           gsc.expert_agreement.expert_id%type,
    filename            gsc.expert_agreement.filename%type,
    blob                gsc.expert_agreement.blob%type)
  owner to gsc;




create or replace function gsc.expert_agreement_update (
    p_expert_id         gsc.expert_agreement.expert_id%type,
    new_ea_ts           gsc.expert_agreement.expert_agreement_ts%type,
    new_filename        gsc.expert_agreement.filename%type,
    new_blob            gsc.expert_agreement.blob%type,
    old_ts              gsc.expert_agreement.ts%type)
  returns table (ts gsc.expert_agreement.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts   gsc.expert_agreement.ts%type;
begin
  update gsc.expert_agreement
     set expert_agreement_ts = new_ea_ts,
         filename = new_filename,
         blob = new_blob
   where expert_id = p_expert_id
     and expert_agreement.ts = old_ts
         returning expert_agreement.ts into l_ts;
     
  if not found then
    select gsc.err_concurrency_control('Согласие на обработку персональных данных');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.expert_agreement_update (
    p_expert_id         gsc.expert_agreement.expert_id%type,
    new_ea_ts           gsc.expert_agreement.expert_agreement_ts%type,
    new_filename        gsc.expert_agreement.filename%type,
    new_blob            gsc.expert_agreement.blob%type,
    old_ts              gsc.expert_agreement.ts%type)
  owner to gsc;





create or replace function gsc.expert_agreement_delete (p_expert_id gsc.expert_agreement.expert_id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.expert_agreement
        where expert_id = p_expert_id;
  
  if not found then
    select gsc.err_concurrency_control('Согласие на обработку персональных данных.');
  end if;
end;
$$;

alter function gsc.expert_agreement_delete (p_expert_id gsc.expert_agreement.expert_id%type)
  owner to gsc;

