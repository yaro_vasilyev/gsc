
set search_path = gsc,public;

create or replace function gsc.get_observers (
    p_rolname         name)
  returns setof gsc.observer
  language plpgsql
  stable
  called on null input
  security definer
as $$
declare
  l_user_guid         gsc.users.guid%type;
  l_observer_id       gsc.observer.id%type;
begin

  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users.', p_rolname;
      return;
  end;
  
  if gsc._xxx_roles_any(p_rolname, 
        (gsc.roles ()).boss, 
        (gsc.roles ()).admin, 
        (gsc.roles ()).sysadmin, 
        (gsc.roles ()).an, 
        (gsc.roles ()).an_lead,
        (gsc.roles ()).manager) then
    return query 
      select * 
        from gsc.observer; -- everyone
  elsif _xxx_roles_all(p_rolname, (gsc.roles ()).rsm) then
    return query 
      select o.* 
        from gsc.observer as o 
       where o.observer_type_rsm; -- rsm people only
  else
    -- I'm expert here maybe, so show me my observer
    select observer_id
      into l_observer_id
      from gsc.expert
     where user_guid = l_user_guid;
    
    return query 
      select * 
        from gsc.observer as o 
       where l_observer_id  is not null 
         and o.id           = l_observer_id; -- expert's observer only
  /*else
    return; -- noone*/
  end if;

  return;    
end;
$$;

alter function gsc.get_observers (name)
  owner to gsc;
