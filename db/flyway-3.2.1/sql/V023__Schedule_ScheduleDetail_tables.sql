SET search_path = gsc, public;

--
-- Name: schedule; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE schedule (
    id integer NOT NULL,
    expert_id integer NOT NULL,
    period_id integer NOT NULL,
    monitor_rcoi integer DEFAULT 0 NOT NULL,
    fixed integer DEFAULT 0 NOT NULL,
    period_type integer NOT NULL,
    region_id integer NOT NULL,
    CONSTRAINT schedule_fixed_check CHECK ((fixed = ANY (ARRAY[0, 1]))),
    CONSTRAINT schedule_monitor_rcoi_check CHECK ((monitor_rcoi = ANY (ARRAY[0, 1]))),
    CONSTRAINT schedule_period_type_chk CHECK ((period_type = ANY (ARRAY[1, 2, 3])))
);

--
-- Name: schedule_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (id);

--
-- Name: schedule_expert_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_expert_id_fkey FOREIGN KEY (expert_id) REFERENCES expert(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id) ON UPDATE CASCADE ON DELETE RESTRICT;



ALTER TABLE schedule OWNER TO gsc;

--
-- Name: schedule_detail; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE schedule_detail (
    id integer NOT NULL,
    schedule_id integer NOT NULL,
    ppe_id integer NOT NULL
);

--
-- Name: schedule_detail_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_pkey PRIMARY KEY (id);

--
-- Name: schedule_detail_ppe_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_ppe_id_fkey FOREIGN KEY (ppe_id) REFERENCES ppe(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: schedule_detail_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule_detail
    ADD CONSTRAINT schedule_detail_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id) ON UPDATE CASCADE ON DELETE CASCADE;



ALTER TABLE schedule_detail OWNER TO gsc;

--
-- Name: schedule_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule_id_seq OWNER TO gsc;

--
-- Name: schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE schedule_id_seq OWNED BY schedule.id;



--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule ALTER COLUMN id SET DEFAULT nextval('schedule_id_seq'::regclass);



--
-- Name: schedule_detail_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE schedule_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule_detail_id_seq OWNER TO gsc;

--
-- Name: schedule_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE schedule_detail_id_seq OWNED BY schedule_detail.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY schedule_detail ALTER COLUMN id SET DEFAULT nextval('schedule_detail_id_seq'::regclass);
