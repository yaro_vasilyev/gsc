SET search_path = gsc, public;

--
-- Name: err_concurrency_control(text); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION err_concurrency_control(p_entity text) RETURNS void
    LANGUAGE plpgsql IMMUTABLE
    AS $$
begin
  raise exception '%: Запись не найдена. Возможно она была удалена или обновлена другим пользователем.', p_entity
    using errcode = 'object_not_in_prerequisite_state',
             hint = 'Обновите таблицу или перезапустите форму редактирования.';
end;
$$;


ALTER FUNCTION gsc.err_concurrency_control(p_entity text) OWNER TO gsc;
