SET search_path = gsc, public;

--
-- Name: rcoi; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE rcoi (
    id integer NOT NULL,
    region_id integer NOT NULL,
    name character varying(500) DEFAULT ''::character varying NOT NULL,
    address character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_post character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    chief_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    chief_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL
);

--
-- Name: rcoi_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY rcoi
    ADD CONSTRAINT rcoi_pkey PRIMARY KEY (id);

--
-- Name: rcoi_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY rcoi
    ADD CONSTRAINT rcoi_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);

ALTER TABLE rcoi OWNER TO gsc;

--
-- Name: rcoi_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE rcoi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rcoi_id_seq OWNER TO gsc;

--
-- Name: rcoi_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE rcoi_id_seq OWNED BY rcoi.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY rcoi ALTER COLUMN id SET DEFAULT nextval('rcoi_id_seq'::regclass);
