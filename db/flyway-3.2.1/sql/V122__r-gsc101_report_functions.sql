set search_path = gsc,public;


create or replace function gsc.r_gsc101_violations_in_region (
    p_project_id           gsc.period.id%type,
    p_region_id            gsc.region.id%type,
    p_exam_type            gsc.monitor_result.exam_type%type)
  returns table (          date           gsc.monitor_result.date%type, 
                           subjects       text,
                           station_etc    text,
                           violation_code gsc.violation.code%type,
                           violation_name gsc.violation.name%type,
                           violator_code  gsc.violator.code%type,
                           violator_name  gsc.violator.name%type)
  stable
  called on null input
  security definer
  language plpgsql
as $$
begin
  return query
    with cte_subjects as (
      select mr_detail.monitor_result_id, 
             array_to_string (array_agg (distinct subject.name order by subject.name), ', ') as subjects
        from gsc.mr_detail
        join gsc.exec on exec.id = mr_detail.exec_id
        join gsc.exam on exam.id = exec.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where exec.project_id = p_project_id
         and exam.period_id = p_project_id
       group by mr_detail.monitor_result_id
    ),
    cte_stations as (
      select mr_detail.monitor_result_id,
             array_to_string (array_agg (distinct station.code order by station.code), ', ') as stations
        from gsc.mr_detail
        join gsc.exec on exec.id = mr_detail.exec_id
        join gsc.station on station.id = exec.station_id
       where exec.project_id = p_project_id
         and station.project_id = p_project_id
       group by mr_detail.monitor_result_id
    )
    select distinct 
           monitor_result.date, 
           cte_subjects.subjects,
           case monitor_result.object_type
              when 1 then 'ППЭ ' || coalesce (cte_stations.stations, '')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ' 
           end as station_etc,
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violator_name
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      left join cte_subjects on cte_subjects.monitor_result_id = monitor_result.id
      left join cte_stations on cte_stations.monitor_result_id = monitor_result.id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
     where schedule.period_id = p_project_id
       and (p_region_id is null or (p_region_id is not null and schedule.region_id = p_region_id))
       and (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type));
end;
$$;


alter function gsc.r_gsc101_violations_in_region (
    p_project_id           gsc.period.id%type,
    p_region_id            gsc.region.id%type,
    p_exam_type            gsc.monitor_result.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc101_violations_in_region (
    p_project_id           gsc.period.id%type,
    p_region_id            gsc.region.id%type,
    p_exam_type            gsc.monitor_result.exam_type%type)
  to s_repmr_module;