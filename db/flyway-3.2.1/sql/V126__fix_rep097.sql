set search_path = gsc,public;

create or replace function gsc.r_gsc097_count_mr_ppe (
          p_project_id     gsc.period.id%type)
  returns table (
          exam_type        gsc.d_exam_type,
          violations       numeric,
          schedules        numeric)
  stable
  returns null on null input
  security definer
  language plpgsql
as $$
begin
  return query
    with cte_violations as (
      select inner_sql.exam_type,
             inner_sql.cnt
        from (
              select 1::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 1) as v
               where v.station_id is not null
              union
              select 2::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 2) as v
               where v.station_id is not null
              union
              select 3::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 3) as v
               where v.station_id is not null
             ) as inner_sql
    ),
    cte_schedules as (
      select inner_sql.exam_type,
             count (inner_sql.*)::numeric as cnt
        from (
              select distinct
                     exam.exam_type,
                     exam.exam_date,
                     station.id as station_id,
                     schedule_detail.schedule_id
                from gsc.exec
                join gsc.exam on exam.id = exec.exam_id
                join gsc.station on station.id = exec.station_id
                join gsc.schedule_detail on schedule_detail.station_id = station.id and schedule_detail.exam_date = exam.exam_date
               where exec.project_id = p_project_id
                 and exam.period_id = p_project_id
              ) as inner_sql
        group by
              inner_sql.exam_type
    )
    select et::gsc.d_exam_type as exam_type,
           coalesce (cte_violations.cnt, 0) as violations,
           coalesce (cte_schedules.cnt, 0) as schedules
      from unnest (array [1, 2, 3]) as et
      left join cte_violations on cte_violations.exam_type = et
      left join cte_schedules on cte_schedules.exam_type = et
     order by et;
end;
$$;

alter function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  owner to gsc;

grant execute on function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  to s_repmr_module;
