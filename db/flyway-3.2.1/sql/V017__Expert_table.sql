SET search_path = gsc, public;

--
-- Name: expert; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE expert (
    id integer NOT NULL,
    period_id integer NOT NULL,
    import_code character varying(100) DEFAULT public.uuid_generate_v4(),
    surname character varying(100) DEFAULT ''::character varying NOT NULL,
    name character varying(100) DEFAULT ''::character varying NOT NULL,
    patronymic character varying(100) DEFAULT ''::character varying,
    workplace character varying(500) DEFAULT ''::character varying NOT NULL,
    post character varying(500) DEFAULT ''::character varying NOT NULL,
    post_value integer DEFAULT 1 NOT NULL,
    birth_date date NOT NULL,
    birth_place character varying(300) DEFAULT ''::character varying NOT NULL,
    base_region_id integer NOT NULL,
    live_address character varying(500) DEFAULT ''::character varying NOT NULL,
    phone character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL,
    academic_degree_value integer DEFAULT 0 NOT NULL,
    academic_title_value integer DEFAULT 0 NOT NULL,
    expirience character varying(1000) DEFAULT ''::character varying NOT NULL,
    speciality character varying(300) DEFAULT ''::character varying NOT NULL,
    speciality_value integer DEFAULT 0 NOT NULL,
    work_years_total integer DEFAULT 0 NOT NULL,
    work_years_education integer DEFAULT 0 NOT NULL,
    work_years_education_value integer DEFAULT 1 NOT NULL,
    work_years_chief integer DEFAULT 0 NOT NULL,
    exp_years_education integer DEFAULT 0 NOT NULL,
    exp_years_education_value integer DEFAULT 1 NOT NULL,
    awards character varying(1000) DEFAULT ''::character varying NOT NULL,
    awards_value integer DEFAULT 0 NOT NULL,
    can_mission integer DEFAULT 1 NOT NULL,
    can_mission_value integer DEFAULT 5 NOT NULL,
    chief_name character varying(300) DEFAULT ''::character varying NOT NULL,
    chief_post character varying(500) DEFAULT ''::character varying NOT NULL,
    chief_email character varying(100) DEFAULT ''::character varying NOT NULL,
    observer_id integer NOT NULL,
    kind integer DEFAULT 1 NOT NULL,
    test_results integer,
    test_results_value integer,
    interview_status integer,
    interview_status_value integer,
    stage1_points integer,
    engage integer,
    subject_level_value integer DEFAULT 5 NOT NULL,
    accuracy_level_value integer DEFAULT 5 NOT NULL,
    punctual_level_value integer DEFAULT 5 NOT NULL,
    independency_level_value integer DEFAULT 5 NOT NULL,
    moral_level_value integer DEFAULT 5 NOT NULL,
    docs_impr_part_level_value integer DEFAULT 0 NOT NULL,
    stage2_points integer,
    efficiency integer,
    next_period_proposal integer,
    academic_title_id integer,
    academic_degree_id integer,
    ts timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT expert_academic_degree_value_check CHECK ((academic_degree_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_academic_title_value_check CHECK ((academic_title_value = ANY (ARRAY[0, 1, 2, 3, 4, 5]))),
    CONSTRAINT expert_accuracy_level_value_check CHECK (((accuracy_level_value >= 0) AND (accuracy_level_value <= 5))),
    CONSTRAINT expert_awards_value_check CHECK ((awards_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_can_mission_check CHECK ((can_mission = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_can_mission_value_check CHECK ((can_mission_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_docs_impr_part_level_value_check CHECK ((docs_impr_part_level_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_efficiency_check CHECK (((efficiency >= 0) AND (efficiency <= 3))),
    CONSTRAINT expert_engage_ckeck CHECK ((engage = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_exp_years_education_value_check CHECK ((exp_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_independency_level_value_check CHECK (((independency_level_value >= 0) AND (independency_level_value <= 5))),
    CONSTRAINT expert_interview_status_check CHECK ((interview_status = ANY (ARRAY[0, 1]))),
    CONSTRAINT expert_interview_status_value_check CHECK ((interview_status_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_kind_check CHECK ((kind = ANY (ARRAY[1, 2]))),
    CONSTRAINT expert_moral_level_value_check CHECK (((moral_level_value >= 0) AND (moral_level_value <= 5))),
    CONSTRAINT expert_next_period_proposal_check CHECK (((efficiency >= 1) AND (efficiency <= 3))),
    CONSTRAINT expert_post_value_check CHECK ((post_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_punctual_level_value_check CHECK ((punctual_level_value = ANY (ARRAY[0, 1, 3, 5]))),
    CONSTRAINT expert_speciality_value_check CHECK ((speciality_value = ANY (ARRAY[0, 5]))),
    CONSTRAINT expert_stage1_points_check CHECK ((stage1_points >= 0)),
    CONSTRAINT expert_stage2_points_check CHECK ((stage2_points >= 0)),
    CONSTRAINT expert_subject_level_value_check CHECK (((subject_level_value >= 0) AND (subject_level_value <= 5))),
    CONSTRAINT expert_test_results_check CHECK (((test_results >= 0) AND (test_results <= 23))),
    CONSTRAINT expert_test_results_value_check CHECK (((test_results_value >= 0) AND (test_results_value <= 5))),
    CONSTRAINT expert_work_years_chief_check CHECK ((work_years_chief >= 0)),
    CONSTRAINT expert_work_years_education_check CHECK ((work_years_education >= 0)),
    CONSTRAINT expert_work_years_education_value_check CHECK ((work_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5]))),
    CONSTRAINT expert_work_years_total_check CHECK ((work_years_total >= 0))
);

--
-- Name: expert_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_pkey PRIMARY KEY (id);
--
-- Name: expert_academic_degree_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_academic_degree_id_fkey FOREIGN KEY (academic_degree_id) REFERENCES academic_degree(id);


--
-- Name: expert_academic_title_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_academic_title_id_fkey FOREIGN KEY (academic_title_id) REFERENCES academic_title(id);


--
-- Name: expert_base_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_base_region_id_fkey FOREIGN KEY (base_region_id) REFERENCES region(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expert_observer_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_observer_id_fkey FOREIGN KEY (observer_id) REFERENCES observer(id);


--
-- Name: expert_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert
    ADD CONSTRAINT expert_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;



ALTER TABLE expert OWNER TO gsc;

--
-- Name: expert_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE expert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expert_id_seq OWNER TO gsc;

--
-- Name: expert_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE expert_id_seq OWNED BY expert.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY expert ALTER COLUMN id SET DEFAULT nextval('expert_id_seq'::regclass);

