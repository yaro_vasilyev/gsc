--------------------------------------------------------------------------------------------
-- Drop constraints
--------------------------------------------------------------------------------------------

alter table gsc.observer
  drop constraint observer_user_guid_unq;

alter table gsc.observer
  add constraint observer_user_guid_unq unique(user_guid, project_id);

alter table gsc.violator
  drop constraint violator_code_key;
alter table gsc.violator
  add constraint violator_code_key unique(code, project_id);

alter table gsc.violation
  drop constraint violation_code_key;

alter table gsc.violation
  add constraint violation_code_key unique(code, project_id);

alter table gsc.expert
  drop constraint expert_user_guid_unq;

alter table gsc.expert
  add constraint expert_user_guid_unq unique(user_guid, period_id);


--------------------------------------------------------------------------------------------
-- Functions
--------------------------------------------------------------------------------------------
create or replace function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  returns void
  volatile
  security invoker
  language plpgsql
as $$
declare 
  e gsc.expert;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в исходный проект';
  end if;
  
  for e in 
      select * 
        from gsc.expert 
       where expert.period_id = p_source_project_id 
  loop
    perform gsc.expert_insert (
      period_id => p_target_project_id,
      import_code => e.import_code,
      surname => e.surname,
      name => e.name,
      patronymic => e.patronymic,
      workplace => e.workplace,
      post => e.post,
      post_value => e.post_value,
      birth_date => e.birth_date,
      birth_place => e.birth_place,
      base_region_id => e.base_region_id,
      live_address => e.live_address,
      phone => e.phone,
      email => e.email,
      expirience => e.expirience,
      speciality => e.speciality,
      speciality_value => e.speciality_value,
      work_years_total => e.work_years_total,
      work_years_education => e.work_years_education,
      work_years_chief => e.work_years_chief,
      exp_years_education => e.exp_years_education,
      awards => e.awards,
      can_mission => e.can_mission,
      chief_name => e.chief_name,
      chief_post => e.chief_post,
      chief_email => e.chief_email,
      observer_id => null,
      kind => e.kind,
      test_results => e.test_results,
      interview_status => e.interview_status,
      subject_level_value => e.subject_level_value,
      accuracy_level_value => e.accuracy_level_value,
      punctual_level_value => e.punctual_level_value,
      independency_level_value => e.independency_level_value,
      moral_level_value => e.moral_level_value,
      docs_impr_part_level_value => e.docs_impr_part_level_value,
      academic_title_id => e.academic_title_id,
      academic_degree_id => e.academic_degree_id,
      user_guid => e.user_guid);
  end loop;
end;
$$;

alter function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  owner to gsc;

--------------------------------------------------------------------------------------------

create or replace function gsc.copy_regions (
    p_source_project_id     gsc.period.id%type,
    p_target_project_id     gsc.period.id%type)
  returns void
  language plpgsql
  volatile
  security invoker
as $$
declare
  r gsc.region;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование регионов в тот же проект.';
  end if;

  for r in
    select *
      from gsc.region
     where region.project_id = p_source_project_id
  loop
    perform gsc.region_insert (
      p_project_id  => p_target_project_id,
      code          => r.code,
      name          => r.name,
      zone          => r.zone);
  end loop;
end;
$$;

alter function gsc.copy_regions (
    p_source_project_id     gsc.period.id%type,
    p_target_project_id     gsc.period.id%type)
  owner to gsc;

--------------------------------------------------------------------------------------------


create or replace function gsc.copy_violators (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  returns void
  volatile
  security invoker
  language plpgsql
as $$
declare
  v gsc.violator;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в исходный проект';
  end if;
  for v in 
      select * 
        from gsc.violator
       where violator.project_id = p_source_project_id 
  loop
    perform gsc.violator_insert (
      p_project_id    => p_target_project_id,
      code            => v.code,
      name            => v.name);
  end loop;
end;
$$;

alter function gsc.copy_violators (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  owner to gsc;
--------------------------------------------------------------------------------------------


create or replace function gsc.copy_violations (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  returns void
  volatile
  security invoker
  language plpgsql
as $$
declare
  v gsc.violation;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в исходный проект';
  end if;
  for v in 
      select * 
        from gsc.violation
       where violation.project_id = p_source_project_id 
  loop
    perform gsc.violation_insert (
      p_project_id    => p_target_project_id,
      code            => v.code,
      name            => v.name);
  end loop;
end;
$$;

alter function gsc.copy_violations (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  owner to gsc;
--------------------------------------------------------------------------------------------

create or replace function gsc.copy_observers (
    p_source_project_id   gsc.period.id%type,
    p_target_project_id   gsc.period.id%type)
  returns void
  volatile
  language plpgsql
  security invoker
as $$
declare
  o gsc.observer;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в тот же проект';
  end if;

  for o in
    select *
      from gsc.observer
     where project_id = p_source_project_id
  loop
    perform gsc.observer_insert (
      p_project_id      => p_target_project_id,
      type              => o.type,
      fio               => o.fio,
      wp                => o.wp,
      post              => o.post,
      phone1            => o.phone1,
      phone2            => o.phone2,
      email             => o.email,
      user_guid         => o.user_guid);
  end loop;
end;
$$;

alter function gsc.copy_observers (
    p_source_project_id   gsc.period.id%type,
    p_target_project_id   gsc.period.id%type)
  owner to gsc;

--------------------------------------------------------------------------------------------
create or replace function gsc.copy_project_data (
    p_source_project_id      gsc.period.id%type,
    p_target_project_id      gsc.period.id%type)
  returns void
  language plpgsql
  volatile
  security invoker
as $$
begin
  perform gsc.copy_regions (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: regions copied';

  perform gsc.copy_violations (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: violations copied';

  perform gsc.copy_violators (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: violators copied';

  perform gsc.copy_observers (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: observers copied';

  perform gsc.copy_experts (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: experts copied';

  update gsc.period
     set parent_id = p_source_project_id
   where id = p_target_project_id;

  raise notice '%', 'copy_project_data: parent updated';
end;
$$;

alter function gsc.copy_project_data (
    p_source_project_id      gsc.period.id%type,
    p_target_project_id      gsc.period.id%type)
  owner to gsc;
--------------------------------------------------------------------------------------------
drop function if exists gsc.period_insert(
    in ptitle character varying,
    in pstatus integer);

drop function if exists gsc.period_update(integer, character varying, integer, integer, character varying, integer);
drop function if exists gsc.period_delete(integer, character varying, integer);

--------------------------------------------------------------------------------------------
