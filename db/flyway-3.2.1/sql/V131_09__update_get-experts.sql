CREATE OR REPLACE FUNCTION gsc.get_experts (
  p_rolname name
)
RETURNS SETOF gsc.expert AS
$body$
declare
  l_user_guid gsc.users.guid%type;
begin
  
  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users', p_rolname;
      return; -- nope, user is not in the system
  end;
  
  if gsc._xxx_roles_any(p_rolname, gsc.grant_users_list() /*gsc.role_admin(), gsc.role_boss(), gsc.role_an_lead(), gsc.role_an(), gsc.role_manager()*/) then
    return query select * from gsc.expert; -- EVERRRRRYYYYYOOOOONNNNEEEE!!!
  elsif gsc._xxx_roles_all(p_rolname, gsc.grant_rsm_list() /*gsc.role_rsm()*/) then
    return query select e.* from gsc.expert as e where e.expert_kind_social; --only rsm people
  else --if gsc._xxx_roles_all(p_rolname, gsc.role_expert()) then
  -- if select can find a record with user guid then it's expert himself!
    return query select * from gsc.expert where user_guid = l_user_guid; -- only self
  /*else
    return; -- noone here*/
  end if;
  return;
end;
$body$
LANGUAGE 'plpgsql'
STABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100 ROWS 1000;