CREATE OR REPLACE FUNCTION gsc.login_update (
  i_id integer,
  i_password varchar,
  i_observer_id integer
)
RETURNS void AS
$body$
DECLARE
	l_login VARCHAR;
	l_guid UUID;
BEGIN
  -- check password
  if (i_password <> '') IS NOT TRUE then -- checks password for null or ''
  	raise exception 'Не допускается пустой пароль';
  end if;

	-- get login and guid
  select u.user_login, u.guid
  into strict l_login, l_guid
  from gsc.users u
  where u.id = i_id;  
  
  -- update password
  EXECUTE format('ALTER ROLE %I PASSWORD ''%s''', l_login, i_password);
  
  -- bind observer
  IF i_observer_id IS NOT NULL THEN
  	EXECUTE gsc.u_bind_observer(l_guid, i_observer_id);
  END IF;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

alter function gsc.login_update (
	i_id integer,
  i_password varchar,
  i_observer_id integer
) owner to gsc;