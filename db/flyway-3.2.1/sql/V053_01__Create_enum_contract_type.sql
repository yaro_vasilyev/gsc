CREATE TYPE gsc.contract_type AS ENUM (
    'paid', 'unpaid'
);