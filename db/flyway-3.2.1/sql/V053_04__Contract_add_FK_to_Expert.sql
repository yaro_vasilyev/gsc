ALTER TABLE gsc.contract
  ADD CONSTRAINT contract_expert_fk FOREIGN KEY (expert_id)
    REFERENCES gsc.expert(id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    NOT DEFERRABLE;