CREATE VIEW gsc.v_user_roles ( 
	user_id,  
  role
) 
AS 
SELECT
	user_id,  
  role
FROM
(
	SELECT u.id AS user_id, gsc.all_roles_of_user(u.rolname) AS role
	FROM gsc.users u
) r
WHERE r.role LIKE 'r_%';

ALTER TABLE gsc.v_user_roles
  OWNER TO gsc;