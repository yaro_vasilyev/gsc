ALTER TABLE gsc.contract
  ALTER COLUMN contract_type TYPE INTEGER
    USING case contract_type
        when 'paid'::gsc.contract_type 
        THEN 1 
        when 'unpaid'::gsc.contract_type 
        then 0 
      end;