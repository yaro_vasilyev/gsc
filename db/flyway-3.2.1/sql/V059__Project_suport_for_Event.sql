
set search_path = gsc,public;

alter table event
  add column project_id integer,
  add constraint event_project_id_fkey
    foreign key (project_id) references period (id)
      on update cascade on delete restrict;

update event
   set project_id = (select max(id) from period where status = 1);

alter table event
  alter column project_id
    set not null;