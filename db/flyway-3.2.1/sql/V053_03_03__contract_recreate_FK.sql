ALTER TABLE gsc.contract
  ADD CONSTRAINT contract_fk FOREIGN KEY (number)
    REFERENCES gsc.contract_info(number)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;