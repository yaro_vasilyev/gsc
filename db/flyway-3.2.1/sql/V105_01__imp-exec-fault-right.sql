CREATE TABLE IF NOT EXISTS gsc.imp_exec_fault_right (
  imp_session_guid UUID,
  imp_user TEXT DEFAULT "current_user"(),
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now(),
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status,
  imp_region INTEGER,
  imp_station_code INTEGER,
  station_id INTEGER,
  imp_examglobalid INTEGER,
  exam_id INTEGER,
  kim INTEGER,
  count_part INTEGER,
  count_room INTEGER,
  count_room_video INTEGER,
  exam_type gsc.d_exam_type,
	errlog TEXT
) 
WITH (oids = false);

alter table gsc.imp_exec_fault_right owner to gsc;

COMMENT ON TABLE gsc.imp_exec_fault_right
IS 'Execution import table faults.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_region
IS 'Region code.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_station_code
IS 'Station code.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.station_id
IS 'Station id - FK on station table.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.imp_examglobalid
IS 'ExamGlobalID for linking to exam_id.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.exam_id
IS 'Exam id - KF on Exam table.';

COMMENT ON COLUMN gsc.imp_exec_fault_right.kim
IS 'Is KIM printed';

COMMENT ON COLUMN gsc.imp_exec_fault_right.count_part
IS 'ParticipantPPECount';

COMMENT ON COLUMN gsc.imp_exec_fault_right.count_room
IS 'AuditoriumsPPECount';

COMMENT ON COLUMN gsc.imp_exec_fault_right.count_room_video
IS 'AuditoriumsPPEVideoCount';

COMMENT ON COLUMN gsc.imp_exec_fault_right.exam_type
IS 'Форма экзамена. ЕГЭ=1, ГВЭ=2, ОГЭ=3';