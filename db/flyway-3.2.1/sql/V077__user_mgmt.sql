set search_path = gsc,public;

alter table expert
  add column user_id integer,
  add constraint expert_user_id_fk foreign key (user_id) references gsc.users(id),
  add constraint expert_user_id_unq unique (period_id, user_id);

update gsc.expert
   set user_id = users.id
  from gsc.users
 where users.parent_id = expert.id and users.type_user = 0;


alter table gsc.observer
  add column user_id integer,
  add constraint observer_user_id_fk foreign key (user_id) references gsc.users(id),
  add constraint observer_user_id_unq unique (user_id);

update gsc.observer
   set user_id = users.id
  from gsc.users
 where users.parent_id = observer.id and users.type_user = 1;



alter table gsc.users
  add column rolname name;

alter table gsc.users
  alter column parent_id
    drop not null;

alter table gsc.users 
  add column delegate_auth_to_db gsc.d_boolean_type;
  
update gsc.users 
   set delegate_auth_to_db = 0;

alter table gsc.users 
  alter column delegate_auth_to_db set default 0;

alter table gsc.users 
  alter column delegate_auth_to_db set not null;



create or replace function gsc.users_check_role_exists() 
  returns trigger 
  as $$
begin
  if new.rolname is null then
    return new;
  end if;
  
  if exists (select 1 from pg_roles where rolname = new.rolname) then
    return new;
  end if;

  raise exception 'User % does not exists in DB.', new.rolname;
end;
$$ language plpgsql;

alter function gsc.users_check_role_exists()
  owner to gsc;


create trigger users_check_role_exists 
  before insert or update on gsc.users
  for each row execute procedure gsc.users_check_role_exists();


create or replace view gsc.v_users (
    id,
    user_login,
    rolname,
    type_user)
as
select users.id,
    users.user_login,
    users.rolname,
    users.type_user
from users;


create or replace function gsc.all_roles_of_user (name)
returns table (rolname name)
language sql
stable
as $$
  with recursive cte as (
    select oid from pg_roles where rolname = $1
    union all
    select m.roleid
    from   cte
    join   pg_auth_members m on m.member = cte.oid
  )
  select rolname
  from   cte
  join pg_roles on pg_roles.oid = cte.oid
$$;

alter function gsc.all_roles_of_user (name)
  owner to gsc;

