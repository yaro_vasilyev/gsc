set search_path = gsc,public;

------------------------------------------------------------------------------------
-- Add FK for project
------------------------------------------------------------------------------------
alter table gsc.contract_info
  add column project_id integer;

alter table gsc.contract_info
  add constraint contract_info_project_id_fkey foreign key (project_id) references gsc.period (id);


update gsc.contract_info
   set project_id = (select id 
                       from gsc.period
                      where status = 1)
 where project_id is null;

alter table gsc.contract_info
  alter column project_id set not null;


------------------------------------------------------------------------------------
-- Replace view
------------------------------------------------------------------------------------
create or replace view gsc.v_contract_info 
as
  select * from gsc.contract_info;

alter view gsc.v_contract_info
  owner to gsc;

------------------------------------------------------------------------------------
-- Insert proc
------------------------------------------------------------------------------------
create or replace function gsc.contract_info_insert (
    p_number        gsc.contract_info.number%type,
    p_contract_date gsc.contract_info.contract_date%type,
    p_project_id    gsc.period.id%type)
  returns table (ts gsc.contract_info.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts        gsc.contract_info.ts%type;
begin
  insert into gsc.contract_info (
              number,
              contract_date,
              project_id)
      values (p_number,
              p_contract_date,
              p_project_id)
    returning contract_info.ts
         into l_ts;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_info_insert (
    number        gsc.contract_info.number%type,
    contract_date gsc.contract_info.contract_date%type,
    project_id    gsc.contract_info.project_id%type)
  owner to gsc;

grant execute on function gsc.contract_info_insert (
    number        gsc.contract_info.number%type,
    contract_date gsc.contract_info.contract_date%type,
    project_id    gsc.contract_info.project_id%type)
  to s_contr_inf_new;

------------------------------------------------------------------------------------
-- Update proc
------------------------------------------------------------------------------------
create or replace function gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    new_project_id    gsc.contract_info.project_id%type,
    old_ts            gsc.contract_info.ts%type)
  returns table (ts gsc.contract_info.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts   gsc.contract_info.ts%type;
begin
  update gsc.contract_info
     set number           = new_number,
         contract_date    = new_contract_date,
         project_id       = new_project_id
   where number           = old_number
     and contract_info.ts = old_ts
         returning contract_info.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Номер договора');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    new_project_id    gsc.contract_info.project_id%type,
    old_ts            gsc.contract_info.ts%type)
  owner to gsc;

grant execute on function gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    new_project_id    gsc.contract_info.project_id%type,
    old_ts            gsc.contract_info.ts%type) 
  to s_contr_inf_edit;


------------------------------------------------------------------------------------
-- Drop the old ones
------------------------------------------------------------------------------------
drop function if exists gsc.contract_info_insert (
    number        gsc.contract_info.number%type,
    contract_date gsc.contract_info.contract_date%type);


drop function if exists gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    old_ts            gsc.contract_info.ts%type);

------------------------------------------------------------------------------------
-- End
------------------------------------------------------------------------------------
