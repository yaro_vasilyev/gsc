set search_path = gsc,public;

update gsc.expert_agreement
   set ts = current_timestamp 
 where ts is null;

alter table gsc.expert_agreement
  alter column ts set not null;

alter table gsc.expert_agreement
  alter column ts set default now();

