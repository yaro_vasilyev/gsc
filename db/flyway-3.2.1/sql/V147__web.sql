set search_path = gsc,public;

DROP FUNCTION if exists gsc.wa_get_user_info(integer);  -- старая заглушка, ненужна

--обновление: не пускать observer-ов из предыдущих проектов
DROP FUNCTION if exists gsc.wa_get_user_info(uuid);

CREATE FUNCTION gsc.wa_get_user_info(guid_ uuid) RETURNS TABLE(
        type_user character varying, 
        surname character varying, 
        name character varying, 
        patronymic character varying, 
        fio character varying, 
        post character varying, 
        phone character varying, 
        email character varying
    )    
    LANGUAGE plpgsql 
    STABLE STRICT
    security definer
    AS $$
declare  
  l_rolname   gsc.users.rolname%type;
begin
  
  begin  
  select u.rolname
    into strict l_rolname
    from gsc.users as u
   where u.guid = guid_;
  exception
    when no_data_found then
      return;
  end;
  
  if l_rolname is null then 
    return;
  end if;
  if gsc._xxx_roles_any(l_rolname, gsc.role_manager(), gsc.role_rsm()) then
    return query select 'manager'::varchar as type_user,
                        ''::varchar as surname,
                        ''::varchar as name,
                        ''::varchar as patronymic,
                        obs.fio,
                        obs.post,
                        obs.phone1,
                        obs.email
                   from gsc.observer as obs
                  where obs.user_guid = guid_
                  and obs.project_id = gsc.current_project_id();
  elsif gsc._xxx_roles_all(l_rolname, gsc.role_expert()) then
    return query select 'expert'::varchar as type_user,
                        exp.surname,
                        exp.name,
                        exp.patronymic,
                        ''::varchar as fio,
                        exp.post,
                        exp.phone,
                        exp.email    
                   from gsc.expert as exp
                  where exp.user_guid = guid_
                  and exp.period_id = gsc.current_project_id();
  else
    raise exception 'User % should be in any of the roles: %, %, %.', 
                    guid_, role_expert(), role_manager(), role_rsm();
  end if;
end;
$$;

ALTER FUNCTION gsc.wa_get_user_info(uuid) owner TO gsc;

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_exam_info AS
 SELECT ec.id AS exec_id,
    em.exam_type,
    em.exam_date,
    st.id AS station_id,
    st.name AS station_name,
    st.code AS station_code,
    sub.name AS subject_name
   FROM station st
     JOIN exec ec ON st.id = ec.station_id
     JOIN exam em ON em.id = ec.exam_id
     JOIN subject sub ON (sub.id = em.subject_id and sub.project_id = gsc.current_project_id());

ALTER VIEW gsc.wa_v_exam_info owner TO gsc;

--------------------------------------------------------------------------------
     
CREATE OR REPLACE VIEW gsc.wa_v_exp_monitor_result AS
 SELECT mr.id,
    e.user_guid AS guid,
    mr.schedule_id,
    mr.date,
    mr.violation_id,
    mr.violator_id,
    mr.note,
    mr.object_type,
    mr.schedule_detail_id,
    mr.exam_type,
    mr.event_id
   FROM monitor_result mr
     JOIN schedule sch ON sch.id = mr.schedule_id
     JOIN expert e ON (e.id = sch.expert_id and e.period_id = gsc.current_project_id());

ALTER VIEW gsc.wa_v_exp_monitor_result owner TO gsc;     

CREATE OR REPLACE RULE "_INSERT" AS
    ON INSERT TO wa_v_exp_monitor_result DO INSTEAD  INSERT INTO monitor_result (schedule_id, date, violation_id, violator_id, note, object_type, schedule_detail_id, exam_type, event_id)
  VALUES (new.schedule_id, new.date, new.violation_id, new.violator_id, new.note, new.object_type, new.schedule_detail_id, new.exam_type, new.event_id)
  RETURNING monitor_result.id,
    NULL::uuid AS uuid,
    monitor_result.schedule_id,
    monitor_result.date,
    monitor_result.violation_id,
    monitor_result.violator_id,
    monitor_result.note,
    monitor_result.object_type,
    monitor_result.schedule_detail_id,
    monitor_result.exam_type,
    monitor_result.event_id;

CREATE OR REPLACE RULE "_UPDATE" AS
    ON UPDATE TO wa_v_exp_monitor_result DO INSTEAD  UPDATE monitor_result SET schedule_id = new.schedule_id, date = new.date, violation_id = new.violation_id, violator_id = new.violator_id, note = new.note, object_type = new.object_type, schedule_detail_id = new.schedule_detail_id, exam_type = new.exam_type, event_id = new.event_id
  WHERE (monitor_result.id = old.id);

CREATE OR REPLACE RULE "_DELETE" AS
    ON DELETE TO wa_v_exp_monitor_result DO INSTEAD  DELETE FROM monitor_result
  WHERE (monitor_result.id = old.id);
     
--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_exp_ppe AS
 SELECT st.id,
    st.name AS station_name,
    r.name AS region_name
   FROM station st
     JOIN region r ON (st.region_id = r.id and st.project_id = gsc.current_project_id());
     
ALTER VIEW gsc.wa_v_exp_ppe owner TO gsc;

--------------------------------------------------------------------------------
     
CREATE OR REPLACE VIEW gsc.wa_v_exp_schedule AS
 SELECT e.user_guid AS guid,
    r.name AS region_name,
    sch.id,
    e.period_id,
    sch.monitor_rcoi,
    sch.fixed,
    sch.period_type,
    sch.expert_id
   FROM schedule sch
     JOIN expert e ON (e.id = sch.expert_id and e.period_id = gsc.current_project_id())
     JOIN region r ON r.id = sch.region_id;

ALTER VIEW gsc.wa_v_exp_schedule owner TO gsc;

--------------------------------------------------------------------------------
     
CREATE OR REPLACE VIEW gsc.wa_v_exp_user_ppe AS
 SELECT DISTINCT st.id,
    st.name,
    st.code,
    e.user_guid AS guid
   FROM station st
     JOIN schedule_detail ON schedule_detail.station_id = st.id
     JOIN schedule ON schedule_detail.schedule_id = schedule.id
     JOIN expert e ON (e.id = schedule.expert_id and e.period_id = gsc.current_project_id());

ALTER VIEW gsc.wa_v_exp_user_ppe owner TO gsc;

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_expert_agreement AS
 SELECT e.user_guid AS guid,
    expa.blob,
    expa.expert_agreement_ts AS ts,
    expa.filename
   FROM expert_agreement expa
     JOIN expert e ON (e.id = expa.expert_id and e.period_id = gsc.current_project_id());

ALTER VIEW gsc.wa_v_expert_agreement owner TO gsc;

CREATE OR REPLACE RULE "_INSERT" AS
    ON INSERT TO wa_v_expert_agreement DO INSTEAD  INSERT INTO expert_agreement (expert_id, blob, expert_agreement_ts, filename)
  VALUES (( SELECT e.id
           FROM expert e
          WHERE (e.user_guid = new.guid)), new.blob, new.ts, new.filename)
  RETURNING NULL::uuid AS guid,
    expert_agreement.blob,
    expert_agreement.expert_agreement_ts AS ts,
    expert_agreement.filename;
    
CREATE OR REPLACE RULE "_DELETE" AS
    ON DELETE TO wa_v_expert_agreement DO INSTEAD  DELETE FROM expert_agreement
  WHERE (expert_agreement.expert_id = ( SELECT e.id
           FROM expert e
          WHERE (e.user_guid = old.guid)));

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_mgr_event AS
 SELECT event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid
   FROM event
     JOIN schedule ON schedule.id = event.schedule_id
     JOIN expert ON (expert.id = schedule.expert_id AND expert.period_id = current_project_id());

ALTER VIEW gsc.wa_v_mgr_event owner TO gsc;

CREATE OR REPLACE RULE "_INSERT" AS
    ON INSERT TO wa_v_mgr_event DO INSTEAD  INSERT INTO event (event_ts, channel, type, schedule_id, object_type, user_guid)
  VALUES (new.event_ts, new.channel, new.type, new.schedule_id, new.object_type, new.user_guid)
  RETURNING event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid;

CREATE OR REPLACE RULE "_DELETE" AS
    ON DELETE TO wa_v_mgr_event DO INSTEAD  DELETE FROM event
  WHERE (event.id = old.id);

CREATE OR REPLACE RULE "_UPDATE" AS
    ON UPDATE TO wa_v_mgr_event DO INSTEAD  UPDATE event SET event_ts = new.event_ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id, object_type = new.object_type, user_guid = new.user_guid
  WHERE (event.id = old.id);

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_mgr_expert AS
 SELECT ex.id,
    o.user_guid AS manager_guid,
    ex.name,
    ex.surname,
    ex.patronymic
   FROM expert ex
     JOIN observer o ON (o.id = ex.observer_id AND ex.period_id = current_project_id());

ALTER VIEW gsc.wa_v_mgr_expert owner TO gsc;

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_mr_detail AS
 SELECT mr_detail.id,
    mr_detail.monitor_result_id,
    mr_detail.exec_id
   FROM mr_detail;

ALTER VIEW gsc.wa_v_mr_detail owner TO gsc;

CREATE OR REPLACE RULE "_INSERT" AS
    ON INSERT TO wa_v_mr_detail DO INSTEAD  INSERT INTO mr_detail (monitor_result_id, exec_id)
  VALUES (new.monitor_result_id, new.exec_id)
  RETURNING mr_detail.id,
    mr_detail.monitor_result_id,
    mr_detail.exec_id;
    
CREATE OR REPLACE RULE "_UPDATE" AS
    ON UPDATE TO wa_v_mr_detail DO INSTEAD  UPDATE mr_detail SET monitor_result_id = new.monitor_result_id, exec_id = new.exec_id
  WHERE (mr_detail.id = old.id);

CREATE OR REPLACE RULE "_DELETE" AS
    ON DELETE TO wa_v_mr_detail DO INSTEAD  DELETE FROM mr_detail
  WHERE (mr_detail.id = old.id);

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_mr_doc AS
 SELECT mr_doc.schedule_id,
    mr_doc.blob,
    mr_doc.mr_doc_ts AS ts,
    mr_doc.filename,
    mr_doc.id,
    e.user_guid AS guid
   FROM mr_doc
     JOIN schedule sch ON sch.id = mr_doc.schedule_id
     JOIN expert e ON (e.id = sch.expert_id AND e.period_id = current_project_id());

ALTER VIEW gsc.wa_v_mr_doc owner TO gsc;

CREATE OR REPLACE RULE "_INSERT" AS
    ON INSERT TO wa_v_mr_doc DO INSTEAD  INSERT INTO mr_doc (schedule_id, blob, filename)
  VALUES (new.schedule_id, new.blob, new.filename)
  RETURNING mr_doc.schedule_id,
    mr_doc.blob,
    mr_doc.mr_doc_ts AS ts,
    mr_doc.filename,
    mr_doc.id,
    NULL::uuid AS guid;

CREATE OR REPLACE RULE "_DELETE" AS
    ON DELETE TO wa_v_mr_doc DO INSTEAD  DELETE FROM mr_doc
  WHERE (mr_doc.id = old.id);

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW gsc.wa_v_schedule_detail AS
 SELECT schd.id,
    schd.schedule_id,
    schd.station_id,
    schd.exam_date
   FROM schedule_detail schd;

ALTER VIEW gsc.wa_v_schedule_detail owner TO gsc;
   
--------------------------------------------------------------------------------
   
CREATE OR REPLACE VIEW gsc.wa_v_violation AS
 SELECT violation.id,
    violation.code,
    violation.name
   FROM violation
   WHERE violation.project_id = current_project_id();

ALTER VIEW gsc.wa_v_violation owner TO gsc;   

--------------------------------------------------------------------------------
   
CREATE OR REPLACE VIEW gsc.wa_v_violator AS
 SELECT violator.id,
    violator.code,
    violator.name
   FROM violator
   WHERE violator.project_id = current_project_id();
   
ALTER VIEW gsc.wa_v_violator owner TO gsc;   

