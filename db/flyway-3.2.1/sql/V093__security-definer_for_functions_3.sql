set search_path = gsc,public;

alter function gsc.event_insert(timestamp, integer, integer, integer, integer, integer, uuid) security definer;
alter function gsc.event_update(integer, timestamp, integer, integer, integer, integer, integer, uuid, timestamp) security definer;
alter function gsc.event_delete(integer) security definer;

alter function gsc.contract_info_insert(varchar, date) security definer; 
alter function gsc.contract_info_update(varchar, varchar, date, timestamp) security definer; 
alter function gsc.contract_info_delete(varchar) security definer; 
alter function gsc.import_row_contract_info(uuid, varchar, date) security definer;
alter function gsc.import_contract_info(uuid) security definer;

alter function gsc.contract_insert(integer, integer, integer, varchar, date, date, money, varchar, bytea) security definer;
alter function gsc.contract_update(varchar, integer, integer, integer, varchar, date, date, money, varchar, bytea, timestamp) security definer;
alter function gsc.contract_delete(varchar) security definer;

alter function gsc.u_create_user(integer, integer, smallint) security definer;
