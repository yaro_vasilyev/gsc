ALTER TABLE gsc.exam
  ALTER COLUMN examglobalid DROP NOT NULL;

ALTER TABLE gsc.exam
  DROP CONSTRAINT exam_period_id_examglobalid_key RESTRICT;

CREATE UNIQUE INDEX exam_period_id_examglobalid_uniq ON gsc.exam
  USING btree (period_id, examglobalid)
  WHERE examglobalid IS NOT NULL;
