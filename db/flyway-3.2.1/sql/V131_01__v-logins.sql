CREATE OR REPLACE FUNCTION gsc.expert_fio (
  expert gsc.v_expert
)
RETURNS varchar AS
$body$
BEGIN
  return concat_ws(' ', expert.surname, expert.name, expert.patronymic);
END;
$body$
LANGUAGE 'plpgsql'
STABLE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE VIEW gsc.v_logins (
    id,
    fio,
    user_login,
    password,
    role)
AS
SELECT u.id,
        CASE
            WHEN e.id IS NOT NULL THEN 'Эксперт '::text || gsc.expert_fio(e.*)::text
            WHEN o.id IS NOT NULL THEN 'Менеджер '::text || o.fio::text
            ELSE '[Пользователь не привязан]'::text
        END AS fio,
    u.user_login,
    NULL::character varying AS password,
    concat_ws(' '::text, (
    SELECT shobj_description(r.oid, 'pg_authid'::name) AS shobj_description
    FROM pg_roles r
    WHERE r.rolname = u.rolname
    ), ('['::text || u.rolname::text) || ']'::text) AS role
FROM gsc.v_users u
     LEFT JOIN gsc.v_observer o ON u.guid = o.user_guid
     LEFT JOIN gsc.v_expert e ON u.guid = e.user_guid;
		 
ALTER TABLE gsc.v_logins
  OWNER TO gsc;
