set search_path = gsc,public;

create or replace function gsc.all_roles_of_user (name)
returns table (rolname name)
language sql
security definer
stable
as $$
  with recursive cte as (
    select oid from pg_roles where rolname = $1
    union all
    select m.roleid
    from   cte
    join   pg_auth_members m on m.member = cte.oid
  )
  select distinct rolname
  from   cte
  join pg_roles on pg_roles.oid = cte.oid
$$;

alter function gsc.all_roles_of_user (name)
  owner to gsc;

