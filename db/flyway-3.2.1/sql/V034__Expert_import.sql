﻿ALTER TABLE gsc.expert
  ADD CONSTRAINT expert_per_code_key 
    UNIQUE (period_id, import_code);

CREATE TABLE gsc.imp_expert (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
  code VARCHAR(100) NOT NULL,
  surname VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  patronymic VARCHAR(100),
  workplace VARCHAR(500) NOT NULL,
  post VARCHAR(500) NOT NULL,
  post_value INTEGER NOT NULL,
  birth_date DATE NOT NULL,
  birth_place VARCHAR(300) NOT NULL,
  imp_base_region VARCHAR(255) NOT NULL,
  base_region_id INTEGER NOT NULL,
  live_address VARCHAR(500) NOT NULL,
  phone VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  imp_academic_degree VARCHAR(80) NOT NULL,
  academic_degree_id INTEGER NOT NULL,
  academic_degree_value INTEGER NOT NULL,
  imp_academic_title VARCHAR(80) NOT NULL,
  academic_title_id INTEGER NOT NULL,
  academic_title_value INTEGER NOT NULL,
  expirience VARCHAR(1000) NOT NULL,
  speciality VARCHAR(300) NOT NULL,
  speciality_value INTEGER NOT NULL,
  work_years_total INTEGER NOT NULL,
  work_years_education INTEGER NOT NULL,
  work_years_education_value INTEGER NOT NULL,
  work_years_chief INTEGER NOT NULL,
  exp_years_education INTEGER NOT NULL,
  exp_years_education_value INTEGER NOT NULL,
  awards VARCHAR(1000) NOT NULL,
  awards_value INTEGER NOT NULL,
  can_mission INTEGER NOT NULL,
  can_mission_value INTEGER NOT NULL,
  chief_name VARCHAR(300),
  chief_post VARCHAR(500),
  chief_email VARCHAR(100),
  CONSTRAINT imp_expert_pkey PRIMARY KEY(imp_session_guid, code),
  CONSTRAINT imp_expert_academic_degree_value_check CHECK (academic_degree_value = ANY (ARRAY[0, 5])),
  CONSTRAINT imp_expert_academic_title_value_check CHECK (academic_title_value = ANY (ARRAY[0, 1, 2, 3, 4, 5])),
  CONSTRAINT imp_expert_awards_value_check CHECK (awards_value = ANY (ARRAY[0, 5])),
  CONSTRAINT imp_expert_can_mission_check CHECK (can_mission = ANY (ARRAY[0, 1])),
  CONSTRAINT imp_expert_can_mission_value_check CHECK (can_mission_value = ANY (ARRAY[0, 5])),
  CONSTRAINT imp_expert_exp_years_education_value_check CHECK (exp_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5])),
  CONSTRAINT imp_expert_post_value_check CHECK (post_value = ANY (ARRAY[1, 2, 3, 4, 5])),
  CONSTRAINT imp_expert_speciality_value_check CHECK (speciality_value = ANY (ARRAY[0, 5])),
  CONSTRAINT imp_expert_work_years_chief_check CHECK (work_years_chief >= 0),
  CONSTRAINT imp_expert_work_years_education_check CHECK (work_years_education >= 0),
  CONSTRAINT imp_expert_work_years_education_value_check CHECK (work_years_education_value = ANY (ARRAY[1, 2, 3, 4, 5])),
  CONSTRAINT imp_expert_work_years_total_check CHECK (work_years_total >= 0),
  CONSTRAINT imp_expert_academic_degree_id_fkey FOREIGN KEY (academic_degree_id)
    REFERENCES gsc.academic_degree(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE,
  CONSTRAINT imp_expert_academic_title_id_fkey FOREIGN KEY (academic_title_id)
    REFERENCES gsc.academic_title(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE,
  CONSTRAINT imp_expert_base_region_id_fkey FOREIGN KEY (base_region_id)
    REFERENCES gsc.region(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE
) 
WITH (oids = false);

CREATE OR REPLACE FUNCTION gsc.import_row_expert (
  imp_session_guid uuid,
  code varchar,
  surname varchar,
  name varchar,
  patronymic varchar,
  workplace varchar,
  post varchar,
  post_value integer,
  birth_date date,
  birth_place varchar,
  imp_base_region varchar,
  live_address varchar,
  phone varchar,
  email varchar,
  imp_academic_degree varchar,
  academic_degree_value integer,
  imp_academic_title varchar,
  academic_title_value integer,
  expirience varchar,
  speciality varchar,
  speciality_value integer,
  work_years_total integer,
  work_years_education integer,
  work_years_education_value integer,
  work_years_chief integer,
  exp_years_education integer,
  exp_years_education_value integer,
  awards varchar,
  awards_value integer,
  can_mission integer,
  can_mission_value integer,
  chief_name varchar,
  chief_post varchar,
  chief_email varchar
)
RETURNS void AS
$body$
DECLARE
  l_region_id integer;
  l_academic_degree_id integer;
  l_academic_title_id integer;
	l_error_detail text;
BEGIN
  -- get region
  SELECT r.id
  INTO l_region_id
  FROM gsc.region r
  WHERE lower(trim(r.name)) = lower(trim(imp_base_region));
  if l_region_id is null then
    RAISE EXCEPTION 'Регион с названием "%" не найден.', imp_base_region
    USING ERRCODE = 'null_value_not_allowed', HINT =
      'Исправьте файл импорта или загрузите регионы и повторите импорт.';
  end if;
  
  -- get academic degree
  SELECT ad.id
  INTO l_academic_degree_id
  FROM gsc.academic_degree ad
  WHERE lower(trim(ad.name)) = lower(trim(imp_academic_degree));
  if l_academic_degree_id is null then
    RAISE EXCEPTION 'Учёная степень с названием "%" не найдена.',
      imp_academic_degree
    USING ERRCODE = 'null_value_not_allowed', HINT =
      'Исправьте файл импорта или загрузите учёные степени и повторите импорт.';
  end if;
  
  -- get academic title
  SELECT act.id
  INTO l_academic_title_id
  FROM gsc.academic_title act
  WHERE lower(trim(act.name)) = lower(trim(imp_academic_title));
  if l_academic_title_id is null then
    RAISE EXCEPTION 'Учёное звание с названием "%" не найдено.',
      imp_academic_title
    USING ERRCODE = 'null_value_not_allowed', HINT =
      'Исправьте файл импорта или загрузите учёные звания и повторите импорт.';
  end if;  
  
  INSERT INTO gsc.imp_expert (
    imp_session_guid, 
    code, 
    surname,
    name,
    patronymic,
    workplace,
    post,
    post_value,
    birth_date,
    birth_place,
    imp_base_region,
    base_region_id,
    live_address,
    phone,
    email,
    imp_academic_degree,
    academic_degree_id,
    academic_degree_value,
    imp_academic_title,
    academic_title_id,
    academic_title_value,
    expirience,
    speciality,
    speciality_value,
    work_years_total,
    work_years_education,
    work_years_education_value,
    work_years_chief,
    exp_years_education,
    exp_years_education_value,
    awards,
    awards_value,
    can_mission,
    can_mission_value,
    chief_name,
    chief_post,
    chief_email)
  VALUES (
  	imp_session_guid, 
    code, 
    surname,
    name,
    patronymic,
    workplace,
    post,
    post_value,
    birth_date,
    birth_place,
    imp_base_region,
    l_region_id,
    live_address,
    phone,
    email,
    imp_academic_degree,
    l_academic_degree_id,
    academic_degree_value,
    imp_academic_title,
    l_academic_title_id,
    academic_title_value,
    expirience,
    speciality,
    speciality_value,
    work_years_total,
    work_years_education,
    work_years_education_value,
    work_years_chief,
    exp_years_education,
    exp_years_education_value,
    awards,
    awards_value,
    can_mission,
    can_mission_value,
    chief_name,
    chief_post,
    chief_email);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE OR REPLACE FUNCTION gsc.import_expert (
  p_imp_session_guid uuid,
  p_period_id integer
)
RETURNS TABLE (
  code varchar,
  snp text
) AS
$body$
DECLARE
  l_imported VARCHAR [];
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO expert AS e (
    	period_id,
      import_code, 
      surname,
      name,
      patronymic,
      workplace,
      post,
      post_value,
      birth_date,
      birth_place,
      base_region_id,
      live_address,
      phone,
      email,
      academic_degree_id,
      academic_degree_value,
      academic_title_id,
      academic_title_value,
      expirience,
      speciality,
      speciality_value,
      work_years_total,
      work_years_education,
      work_years_education_value,
      work_years_chief,
      exp_years_education,
      exp_years_education_value,
      awards,
      awards_value,
      can_mission,
      can_mission_value,
      chief_name,
      chief_post,
      chief_email)
      SELECT
        p_period_id,
        ie.code,
        ie.surname,
        ie.name,
        ie.patronymic,
        ie.workplace,
        ie.post,
        ie.post_value,
        ie.birth_date,
        ie.birth_place,
        ie.base_region_id,
        ie.live_address,
        ie.phone,
        ie.email,
        ie.academic_degree_id,
        ie.academic_degree_value,
        ie.academic_title_id,
        ie.academic_title_value,
        ie.expirience,
        ie.speciality,
        ie.speciality_value,
        ie.work_years_total,
        ie.work_years_education,
        ie.work_years_education_value,
        ie.work_years_chief,
        ie.exp_years_education,
        ie.exp_years_education_value,
        ie.awards,
        ie.awards_value,
        ie.can_mission,
        ie.can_mission_value,
        ie.chief_name,
        ie.chief_post,
        ie.chief_email
      FROM imp_expert ie
      WHERE ie.imp_session_guid = p_imp_session_guid AND ie.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT expert_per_code_key
      DO UPDATE SET
        surname = EXCLUDED.surname,
        name = EXCLUDED.name,
        patronymic = EXCLUDED.patronymic,
        workplace = EXCLUDED.workplace,
        post = EXCLUDED.post,
        post_value = EXCLUDED.post_value,
        birth_date = EXCLUDED.birth_date,
        birth_place = EXCLUDED.birth_place,
        base_region_id = EXCLUDED.base_region_id,
        live_address = EXCLUDED.live_address,
        phone = EXCLUDED.phone,
        email = EXCLUDED.email,
        academic_degree_id = EXCLUDED.academic_degree_id,
        academic_degree_value = EXCLUDED.academic_degree_value,
        academic_title_id = EXCLUDED.academic_title_id,
        academic_title_value = EXCLUDED.academic_title_value,
        expirience = EXCLUDED.expirience,
        speciality = EXCLUDED.speciality,
        speciality_value = EXCLUDED.speciality_value,
        work_years_total = EXCLUDED.work_years_total,
        work_years_education = EXCLUDED.work_years_education,
        work_years_education_value = EXCLUDED.work_years_education_value,
        work_years_chief = EXCLUDED.work_years_chief,
        exp_years_education = EXCLUDED.exp_years_education,
        exp_years_education_value = EXCLUDED.exp_years_education_value,
        awards = EXCLUDED.awards,
        awards_value = EXCLUDED.awards_value,
        can_mission = EXCLUDED.can_mission,
        can_mission_value = EXCLUDED.can_mission_value,
        chief_name = EXCLUDED.chief_name,
        chief_post = EXCLUDED.chief_post,
        chief_email = EXCLUDED.chief_email
    RETURNING e.import_code)
  SELECT array_agg(i.import_code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_expert ie
  SET imp_status = 'imported'
  WHERE ie.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 e.import_code,
                 e.surname || ' ' || e.name || '' || e.patronymic as snp
               FROM gsc.expert e
               WHERE e.period_id = p_period_id AND
               			NOT exists(SELECT NULL
                                FROM gsc.imp_expert ie
                                WHERE ie.imp_session_guid = p_imp_session_guid AND
                                      ie.code = e.import_code);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;