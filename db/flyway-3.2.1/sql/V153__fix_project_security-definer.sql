set search_path = gsc,public;

create or replace function gsc.project_insert (
    p_title       gsc.period.title%type,
    p_status      integer /* just in case */,
    p_open_date   timestamp without time zone,
    p_close_date  timestamp without time zone,
    p_parent_id   gsc.period.id%type)
  returns table (id gsc.period.id%type, ts gsc.period.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id    gsc.period.id%type;
  l_ts    gsc.period.ts%type;
begin
  if p_status = 2 and p_close_date is null then
    p_close_date := now();
  end if;

  if p_status = 1 and p_close_date is not null then
    p_close_date := null;
  end if;
  
  insert into gsc.period (
              title,
              status,
              open_date,
              close_date,
              parent_id)
      values (p_title,
              p_status,
              p_open_date,
              p_close_date,
              p_parent_id)
    returning period.id,
              period.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.project_insert (
    p_title       gsc.period.title%type,
    p_status      integer /* just in case */,
    p_open_date   timestamp without time zone,
    p_close_date  timestamp without time zone,
    p_parent_id   gsc.period.id%type)
  owner to gsc;



create or replace function gsc.project_update (
    p_id                gsc.period.id%type,
    p_new_title         gsc.period.title%type,
    p_new_status        integer /* just in case */,
    p_old_ts            gsc.period.ts%type,
    p_new_open_date     gsc.period.open_date%type,
    p_new_close_date    gsc.period.close_date%type,
    p_new_parent_id     gsc.period.parent_id%type)
  returns table (       ts gsc.period.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts    gsc.period.ts%type;
begin
  if p_new_status = 2 and p_new_close_date is null then
    p_new_close_date := now();
  end if;

  if p_new_status = 1 and p_new_close_date is not null then
    p_new_close_date := null;
  end if;

  update gsc.period
     set title          = p_new_title,
         status         = p_new_status,
         open_date      = p_new_open_date,
         close_date     = p_new_close_date,
         parent_id      = p_new_parent_id
   where id = p_id
     and period.ts      = p_old_ts
         returning      period.ts 
              into      l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Проект');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.project_update (
    p_id                gsc.period.id%type,
    p_new_title         gsc.period.title%type,
    p_new_status        integer /* just in case */,
    p_old_ts            gsc.period.ts%type,
    p_new_open_date     gsc.period.open_date%type,
    p_new_close_date    gsc.period.close_date%type,
    p_new_parent_id     gsc.period.parent_id%type)
  owner to gsc;


create or replace function gsc.project_delete (p_id gsc.period.id%type)
  returns void
  volatile
  security definer
  language plpgsql
as $$
begin
  delete from gsc.period
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Проект');
  end if;
end;
$$;

alter function gsc.project_delete (p_id gsc.period.id%type)
  owner to gsc;