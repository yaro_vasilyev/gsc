set search_path = gsc,public;

alter table gsc.academic_degree
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create trigger academic_degree_bu before update
  on gsc.academic_degree for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_academic_degree
as
  select * from gsc.academic_degree;

alter view gsc.v_academic_degree
  owner to gsc;


create or replace function gsc.academic_degree_insert (
    code          gsc.academic_degree.code%type,
    name          gsc.academic_degree.name%type,
    default_val   gsc.academic_degree.default_val%type)
  returns table(id gsc.academic_degree.id%type, ts gsc.academic_degree.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.academic_degree.id%type;
  l_ts        gsc.academic_degree.ts%type;
begin
  insert into gsc.academic_degree (
              code,
              name,
              default_val)
      values (code,
              name,
              default_val)
    returning academic_degree.id, academic_degree.ts
         into l_id, l_ts;                 
  
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.academic_degree_insert (
    code          gsc.academic_degree.code%type,
    name          gsc.academic_degree.name%type,
    default_val   gsc.academic_degree.default_val%type)
  owner to gsc;



create or replace function gsc.academic_degree_update (
    p_id          gsc.academic_degree.id%type,
    new_code      gsc.academic_degree.code%type,
    new_name      gsc.academic_degree.name%type,
    new_default_val gsc.academic_degree.default_val%type,
    old_ts        gsc.academic_degree.ts%type)
  returns table (ts gsc.academic_degree.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.academic_degree.ts%type;
begin
  update gsc.academic_degree
     set code = new_code,
         name = new_name,
         default_val = new_default_val
   where id = p_id
     and academic_degree.ts = old_ts
         returning academic_degree.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Ученая степень');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.academic_degree_update (
    p_id          gsc.academic_degree.id%type,
    new_code      gsc.academic_degree.code%type,
    new_name      gsc.academic_degree.name%type,
    new_default_val gsc.academic_degree.default_val%type,
    old_ts        gsc.academic_degree.ts%type)
  owner to gsc;
  


create or replace function gsc.academic_degree_delete (p_id gsc.academic_degree.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.academic_degree
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Ученая степень');
  end if;
end;
$$;


alter function gsc.academic_degree_delete (p_id gsc.academic_degree.id%type)
  owner to gsc;