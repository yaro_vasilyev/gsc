-- Файл оставлен, вдруг что-то пригодится

set search_path = gsc,public;

alter table gsc.users add column fio text;


update gsc.users
   set fio = (select concat_ws(' ', e.surname, e.name, e.patronymic) 
               from gsc.expert as e 
              where e.user_guid = guid)
 where fio is null;


update gsc.users
   set fio = (select fio
                from gsc.observer as o
               where o.user_guid = guid)
 where fio is null;

update gsc.users
   set fio = ''
 where fio is null;

alter table gsc.users
  alter column fio set not null;




create or replace function gsc.u_role_display(p_rolname name)
  returns text
  returns null on null input
  stable
  security definer
  language plpgsql
as $$
-- Возвращает строку (типа стисок чз зпт) ролей юзера, которые r_
declare
  ret text;
begin
  select array_to_string(array_agg(gsc.u_role_translate(n)), ', ')
    into ret
    from gsc.all_roles_of_user(p_rolname) as r(n)
   where n like 'r\_%';

  return ret;
end;
$$;

alter function gsc.u_role_display(name)
  owner to gsc;

-------------------------------------------------------------------------------------------------


select guid, user_login, fio, rolname, gsc.u_role_display(rolname) from gsc.users;

alter table gsc.users
  drop constraint user_rolname_unq;
  


update gsc.users
   set rolname = gsc.role_rsm()
 where rolname is null
   and exists (select o.id from gsc.observer as o where o.observer_type_rsm and o.user_guid = guid);


update gsc.users
   set rolname = gsc.role_manager()
 where rolname is null
   and exists (select o.id from gsc.observer as o where o.observer_type_manager and o.user_guid = guid);

update gsc.users
   set rolname = gsc.role_expert()
 where rolname is null
   and exists (select e.id from gsc.expert as e where e.user_guid = guid);


-----------------------------------------------------------------------------

create or replace function gsc.u_create_user (
    type_         integer,
    id_           integer,
    pg_user       smallint            = 0)
  returns varchar 
  language plpgsql
  volatile
  called on null input
  security definer
as $$
declare
  l_ob_role       text;
  l_email         text;  
  l_password      text      := gsc.u_random_string(8);
  l_user_guid     uuid;
  l_fio           text;
begin
  case type_
    when 0 then -- expert
      select exp.email, 
             concat_ws(' ', exp.surname, exp.name, exp.patronymic)
        into 
      strict l_email, 
             l_fio
        from gsc.expert exp           
       where exp.id = id_;
        
      insert into gsc.users (
                  parent_id, 
                  type_user, 
                  user_login, 
                  pass, 
                  guid, 
                  agreement, 
                  rolname, 
                  fio)
          values (id_, 
                  0, 
                  l_email, 
                  gsc.u_create_password(l_email, l_password), 
                  public.uuid_generate_v4(), 
                  null, 
                  gsc.role_expert(), 
                  l_fio)
     on conflict (parent_id, type_user) do
           update set
                  user_login          = EXCLUDED.user_login, 
                  pass                = EXCLUDED.pass, 
                  agreement           = EXCLUDED.agreement, 
                  rolname             = EXCLUDED.rolname
        returning guid 
             into l_user_guid;
      
      update gsc.expert e
         set user_guid = l_user_guid
       where e.id = id_;
        
      if pg_user = 1 then
        select gsc.create_db_user(l_email, l_password, gsc.role_expert());
      end if;
      return password;
    
    when 1 then -- manager (observer)
      select ob.email, 
             case 
               when ob.observer_type_manager then gsc.role_manager()
               when ob.observer_type_rsm then gsc.role_rsm()
             end,
             fio
        into 
      strict l_email,
             l_ob_role,
             l_fio
        from gsc.observer as ob
       where ob.id = id_;
      
      
      insert into gsc.users (
                  parent_id, 
                  type_user, 
                  user_login, 
                  pass, 
                  guid, 
                  agreement, 
                  rolname,
                  fio)
          values (id_, 
                  1, 
                  email, 
                  u_create_password(l_email, l_password), 
                  public.uuid_generate_v4(), 
                  null, 
                  l_ob_role,
                  l_fio)
     on conflict (parent_id, type_user) do
           update set
                  user_login        = EXCLUDED.user_login,
                  pass              = EXCLUDED.pass, 
                  agreement         = EXCLUDED.agreement, 
                  rolname           = EXCLUDED.rolname
        returning guid 
             into l_user_guid;
        
      update gsc.observer o
         set user_guid = l_user_guid
       where o.id = id_;
        
      if pg_user = 1 then
        select gsc.create_db_user(l_email, l_password, l_ob_role);
      end if;    
      return password;
    else
       return null;
  end case;  
exception
  when no_data_found then
    return null;
end;
$$;

comment on function gsc.u_create_user(type_ integer, id_ integer, pg_user smallint)
IS 'return password
type_ - 0=expet, 1=manager
type_ - id table expert or observer
pg_user - defaut 0, if 1 then create db user

';

alter function gsc.u_create_user (
  type_ integer,
  id_ integer,
  pg_user smallint 
) owner to gsc;

---------------------------------------------------------------------------

create or replace function gsc.u_get_login_data()
  returns table(guid uuid, login text, fio text, rolname name, roles text)
  stable
  language sql
  security invoker
as $$
  select guid, user_login, fio, rolname, gsc.u_role_display(rolname) from gsc.users;
$$;

alter function gsc.u_get_login_data()
  owner to gsc;
