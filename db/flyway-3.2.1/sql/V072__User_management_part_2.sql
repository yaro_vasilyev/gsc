set search_path = gsc,public;

create view gsc.wa_v_mr_doc (
    schedule_id,
    blob,
    ts,
    filename,
    id,
    guid)
as
select mr_doc.schedule_id,
    mr_doc.blob,
    mr_doc.ts,
    mr_doc.filename,
    mr_doc.id,
    u.guid
from gsc.mr_doc
     join gsc.schedule sch on sch.id = mr_doc.schedule_id
     join gsc.users u on u.parent_id = sch.expert_id and u.type_user = 0;

alter view gsc.wa_v_mr_doc
  owner to gsc;

create rule "_DELETE" as on delete to gsc.wa_v_mr_doc 
do instead (
delete from gsc.mr_doc
  where mr_doc.id = old.id; -- coment
);

create rule "_INSERT" as on insert to gsc.wa_v_mr_doc 
do instead (
insert into gsc.mr_doc (schedule_id, blob, filename)
  values (new.schedule_id, new.blob, new.filename)
  returning mr_doc.schedule_id,
    mr_doc.blob,
    mr_doc.ts,
    mr_doc.filename,
    mr_doc.id,
    null::uuid as guid; -- comment
);


create or replace function gsc.wa_set_agreement_file (
  guid_ uuid,
  file bytea,
  file_name varchar
)
returns void as
$body$
declare
eid integer;
begin
select u.parent_id into eid from gsc.users u 
where u.guid = guid_ and u.type_user = 0;
if found then
  insert into 
    gsc.expert_agreement
  (
    expert_id,
    blob,
    filename
  )
  values (
    eid,
    file,
    file_name
  );
end if;
end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;

alter function gsc.wa_set_agreement_file(uuid, bytea, varchar)
  owner to gsc;