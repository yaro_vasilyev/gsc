set search_path = gsc,public;

--seq user_id_seq
create sequence gsc.user_id_seq
  increment 1 minvalue 1
  maxvalue 9223372036854775807 start 1
  cache 1;

alter sequence gsc.user_id_seq restart with 5;
alter table gsc.user_id_seq
  owner to gsc;



-- table users
create table gsc.users (
  id integer default nextval('gsc.user_id_seq'::regclass) not null,
  parent_id integer not null,
  type_user smallint not null,
  user_login varchar(50) not null,
  pass varchar(50) not null,
  guid uuid not null,
  agreement boolean default false,
  constraint user_pkey primary key(id)
) 
with (oids = false);

comment on column gsc.users.type_user
is '0-expert, 1-manager,...';


alter table gsc.users
  owner to gsc;


--view wa_v_exp_monitor_result
create view gsc.wa_v_exp_monitor_result (
    id,
    guid,
    schedule_id,
    date,
    violation_id,
    violator_id,
    note,
    object_type,
    schedule_detail_id,
    exam_type,
    event_id)
as
select mr.id,
    u.guid,
    mr.schedule_id,
    mr.date,
    mr.violation_id,
    mr.violator_id,
    mr.note,
    mr.object_type,
    mr.schedule_detail_id,
    mr.exam_type,
    mr.event_id
from gsc.monitor_result mr
     join gsc.schedule sch on sch.id = mr.schedule_id
     join gsc.users u on u.parent_id = sch.expert_id and u.type_user = 0;

alter view gsc.wa_v_exp_monitor_result
  owner to gsc;

create rule "_DELETE" as on delete to gsc.wa_v_exp_monitor_result 
do instead (
delete from gsc.monitor_result where monitor_result.id = old.id; -- comment
);

create rule "_INSERT" as on insert to gsc.wa_v_exp_monitor_result 
do instead (
insert into gsc.monitor_result (schedule_id, date, violation_id, violator_id, note, object_type, schedule_detail_id, exam_type, event_id)
  values (new.schedule_id, new.date, new.violation_id, new.violator_id, new.note, new.object_type, new.schedule_detail_id, new.exam_type, new.event_id)
  returning monitor_result.id,
    null::uuid as uuid,
    monitor_result.schedule_id,
    monitor_result.date,
    monitor_result.violation_id,
    monitor_result.violator_id,
    monitor_result.note,
    monitor_result.object_type,
    monitor_result.schedule_detail_id,
    monitor_result.exam_type,
    monitor_result.event_id; -- comment
);

create rule "_UPDATE" as on update to gsc.wa_v_exp_monitor_result 
do instead (
update gsc.monitor_result set schedule_id = new.schedule_id, date = new.date, violation_id = new.violation_id, violator_id = new.violator_id, note = new.note, object_type = new.object_type, schedule_detail_id = new.schedule_detail_id, exam_type = new.exam_type, event_id = new.event_id
  where monitor_result.id = old.id; -- comment
);


--view wa_v_exp_ppe
create view gsc.wa_v_exp_ppe (
    id,
    station_name,
    region_name)
as
select st.id,
    st.name as station_name,
    r.name as region_name
from gsc.station st
     join gsc.region r on st.region_id = r.id;

alter view gsc.wa_v_exp_ppe
  owner to gsc;
   
--view wa_v_exp_schedule   
create view gsc.wa_v_exp_schedule (
    guid,
    monitor_rcoi,
    schedule_id,
    period_type,
    region_name,
    schedule_detail_id,
    exam_date,
    exec_id,
    exam_type,
    station_id,
    station_name,
    station_code,
    subject_name)
as
select u.guid,
    sch.monitor_rcoi,
    sch.id as schedule_id,
    sch.period_type,
    r.name as region_name,
    schd.id as schedule_detail_id,
    schd.exam_date,
    ec.id as exec_id,
    em.exam_type,
    st.id as station_id,
    st.name as station_name,
    st.code as station_code,
    sub.name as subject_name
from gsc.schedule sch
     join gsc.schedule_detail schd on sch.id = schd.schedule_id
     join gsc.station st on st.id = schd.station_id
     join gsc.exec ec on st.id = ec.station_id
     join gsc.exam em on em.id = ec.exam_id and schd.exam_date = em.exam_date
     join gsc.subject sub on sub.id = em.subject_id
     join gsc.region r on r.id = sch.region_id
     join gsc.users u on u.parent_id = sch.expert_id and u.type_user = 0;

alter view gsc.wa_v_exp_schedule
  owner to gsc;


--view wa_v_exp_user_ppe
create view gsc.wa_v_exp_user_ppe (
    id,
    name,
    code,
    guid)
as
select distinct st.id,
    st.name,
    st.code,
    u.guid
from gsc.station st
     join gsc.schedule_detail on schedule_detail.station_id = st.id
     join gsc.schedule on schedule_detail.schedule_id = schedule.id
     join gsc.users u on u.parent_id = schedule.expert_id and u.type_user = 0;

alter view gsc.wa_v_exp_user_ppe
  owner to gsc;

--view wa_v_mr_detail 
create view gsc.wa_v_mr_detail (
    id,
    monitor_result_id,
    exec_id)
as
select mr_detail.id,
    mr_detail.monitor_result_id,
    mr_detail.exec_id
from gsc.mr_detail;

alter view gsc.wa_v_mr_detail
  owner to gsc;

create rule "_DELETE" as on delete to gsc.wa_v_mr_detail 
do instead (
delete from gsc.mr_detail where mr_detail.id = old.id; -- comment
);

create rule "_INSERT" as on insert to gsc.wa_v_mr_detail 
do instead (
insert into gsc.mr_detail (monitor_result_id, exec_id)
  values (new.monitor_result_id, new.exec_id)
  returning mr_detail.id,
    mr_detail.monitor_result_id,
    mr_detail.exec_id; -- comment
);

create rule "_UPDATE" as on update to gsc.wa_v_mr_detail 
do instead (
update gsc.mr_detail set monitor_result_id = new.monitor_result_id, exec_id = new.exec_id
  where mr_detail.id = old.id; -- comment
);


--view wa_v_schedule_detail
create view gsc.wa_v_schedule_detail (
    id,
    schedule_id,
    station_id,
    exam_date)
as
select schedule_detail.id,
    schedule_detail.schedule_id,
    schedule_detail.station_id,
    schedule_detail.exam_date
from gsc.schedule_detail;

alter view gsc.wa_v_schedule_detail
  owner to gsc;


--view wa_v_violation
create view gsc.wa_v_violation (
    id,
    code,
    name)
as
select violation.id,
    violation.code,
    violation.name
from gsc.violation;

alter view gsc.wa_v_violation
  owner to gsc;


--view wa_v_violator
create view gsc.wa_v_violator (
    id,
    code,
    name)
as
 select violator.id,
    violator.code,
    violator.name
   from gsc.violator;

alter view gsc.wa_v_violator
  owner to gsc;

--func u_create_uesr
CREATE OR REPLACE FUNCTION gsc.u_create_uesr (
  type_ integer,
  id_ integer,
  pg_user smallint = 0
)
RETURNS varchar AS
$body$
DECLARE
  email VARCHAR;  
  password VARCHAR;  
BEGIN
 CASE type_
 WHEN 0 THEN
  SELECT exp.email INTO email
    FROM gsc.expert exp           
    WHERE exp.id = id_;
    
    password:= gsc.u_random_string(8);
--  password:= '12345'; -- for test
    
    INSERT INTO gsc.users
    (parent_id, type_user, user_login, pass, guid, agreement)   
    VALUES (id_, 0, email, 'md5' || md5(password || email), 
    uuid_generate_v4(), null);
    IF pg_user = 1 THEN
      EXECUTE 'CREATE USER '|| email ||' WITH PASSWORD '|| password ||';';
    END IF;
    RETURN password;
 WHEN 1 THEN
  SELECT ob.email INTO email
    FROM gsc.observer ob           
    WHERE ob.id = id_;
    
    password:= gsc.u_random_string(8);
--  password:= '12345'; -- for test
    
    INSERT INTO gsc.users
    (parent_id, type_user, user_login, pass, guid, agreement)   
    VALUES (id_, 1, email, 'md5' || md5(password || email), 
    uuid_generate_v4(), null);
    IF pg_user = 1 THEN
      EXECUTE 'CREATE USER '|| email ||' WITH PASSWORD '|| password ||';';
    END IF;    
    RETURN password;
 ELSE
     RETURN NULL;
 END CASE;  
EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

COMMENT ON FUNCTION gsc.u_create_uesr(type_ integer, id_ integer, pg_user smallint)
IS 'return password
type_ - 0=expet, 1=manager
type_ - id table expert or observer
pg_user - defaut 0, if 1 then create db user

';

alter function gsc.u_create_uesr (integer, integer, smallint)
  owner to gsc;


--func u_random_string
CREATE OR REPLACE FUNCTION gsc.u_random_string (
  length integer
)
RETURNS varchar AS
$body$
DECLARE
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
BEGIN
  IF length < 0 THEN
    RAISE EXCEPTION 'Given length cannot be less than 0';
  END IF;
  FOR i IN 1..length LOOP
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  END LOOP;
  RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

COMMENT ON FUNCTION gsc.u_random_string(length integer)
IS 'return random str
length - length str';

alter function gsc.u_random_string(integer)
  owner to gsc;


drop function if exists gsc.wa_auth_user (varchar, varchar);

--func wa_auth_user
CREATE FUNCTION gsc.wa_auth_user (
  login varchar,
  password varchar
)
RETURNS uuid AS
$body$
DECLARE
  l_guid uuid;
BEGIN
  l_guid := (
  SELECT u.guid
  FROM gsc.users u
  WHERE u.user_login = login AND
        u.pass = 'md5' || md5(password || login));
    return l_guid;
exception 
  when no_data_found then
    return null;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER
COST 100;

alter function gsc.wa_auth_user (varchar, varchar)
  owner to gsc;


--func wa_get_agreement
CREATE OR REPLACE FUNCTION gsc.wa_get_agreement (
  guid_ uuid
)
RETURNS boolean AS
$body$
DECLARE
  agree gsc.users.agreement%TYPE;
BEGIN
  SELECT u.agreement INTO agree
  FROM gsc.users u
  WHERE u.guid = guid_;
  RETURN agree;
exception 
  when no_data_found then
    return null;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

alter function gsc.wa_get_agreement (uuid) 
  owner to gsc;


--func wa_set_agreement
CREATE OR REPLACE FUNCTION gsc.wa_set_agreement (
  guid_ uuid
)
RETURNS void AS
$body$
BEGIN
  UPDATE gsc.users 
  SET agreement = TRUE
  WHERE guid = guid_;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

alter function gsc.wa_set_agreement (uuid)
  owner to gsc;


--func wa_get_user_info
CREATE OR REPLACE FUNCTION gsc.wa_get_user_info (
  guid_ uuid
)
RETURNS TABLE (
  type_user varchar,
  surname varchar,
  name varchar,
  patronymic varchar,
  fio varchar,
  post varchar,
  phone varchar,
  email varchar
) AS
$body$
DECLARE  
  parent gsc.users.parent_id%TYPE;
  type_ gsc.users.type_user%TYPE;
BEGIN
  SELECT u.type_user, u.parent_id 
  INTO type_, parent 
  FROM gsc.users u 
  WHERE u.guid = guid_;
  
  IF type_ = 0 THEN --expert
    RETURN QUERY
    SELECT 
      'expert'::VARCHAR as type_user,
      exp.surname,
      exp.name,
      exp.patronymic,
      ''::VARCHAR as fio,
      exp.post,
      exp.phone,
      exp.email    
    FROM gsc.expert exp
    WHERE exp.id = parent;
  ELSIF type_ = 1 THEN
    RETURN QUERY
    SELECT
      'manager'::VARCHAR as type_user,
      ''::VARCHAR as surname,
      ''::VARCHAR as name,
      ''::VARCHAR as patronymic,
      obs.fio,
      obs.post,
      obs.phone1,
      obs.email  
    FROM gsc.observer obs
    WHERE obs.id = parent;   
  END IF;         
exception 
  when no_data_found then
    return next;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

alter function gsc.wa_get_user_info (uuid)
  owner to gsc;
