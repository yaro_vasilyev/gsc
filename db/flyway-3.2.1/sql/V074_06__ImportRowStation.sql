 -- object recreation
DROP FUNCTION gsc.import_row_station(imp_session_guid uuid, imp_region integer, code integer, name varchar, address varchar, tom integer);

CREATE FUNCTION gsc.import_row_station (
  imp_session_guid uuid,
  imp_region integer,
  code integer,
  name varchar,
  address varchar,
  tom integer,
  exam_type gsc.d_exam_type
)
RETURNS void AS
$body$
DECLARE
  l_region_id integer;
	l_error_detail text;
BEGIN
  -- get region
  SELECT r.id
  INTO l_region_id
  FROM gsc.region r
  WHERE r.code = imp_region;
  if l_region_id is null then
    RAISE EXCEPTION 'Регион с кодом "%" не найден.', imp_region
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите регионы и повторите импорт.';
  end if; 

  begin
    INSERT INTO gsc.imp_station (imp_session_guid, imp_region, region_id, code, name, address, tom, exam_type)
    VALUES (imp_session_guid, imp_region, l_region_id, code, name, address, tom, exam_type);
  EXCEPTION
      WHEN unique_violation then
        -- put doubled record to fault table
      insert into gsc.imp_station_fault (imp_session_guid, imp_status, imp_region, region_id, code, name, address, tom, exam_type)
      values (imp_session_guid, 'doubled'::gsc.import_status, imp_region, l_region_id, code, name, address, tom, exam_type);

      WHEN others THEN
        GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      	insert into gsc.imp_station_fault (imp_session_guid, imp_status, imp_region, region_id, code, name, address, tom, exam_type, imp_status_comment)
      	values (imp_session_guid, 'error'::gsc.import_status, imp_region, l_region_id, code, name, address, tom, exam_type, l_error_detail);
  end;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

ALTER FUNCTION "gsc"."import_row_station"(imp_session_guid uuid, imp_region integer, code integer, name varchar, address varchar, tom integer, exam_type gsc.d_exam_type)
  OWNER TO gsc;