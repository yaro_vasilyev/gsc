set search_path = gsc,public;

alter table gsc.period
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create trigger project_bu before update
  on gsc.period for each row
    execute procedure gsc.update_ts_trigger_fn();
    

create or replace view gsc.v_project
as
  select * from gsc.period;

alter view gsc.v_project
  owner to gsc;



create or replace function gsc.project_insert (
    title     gsc.period.title%type,
    status    integer /* just in case */)
  returns table (id gsc.period.id%type, ts gsc.period.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id    gsc.period.id%type;
  l_ts    gsc.period.ts%type;
begin
  insert into gsc.period (
              title,
              status)
      values (title,
              status)
    returning period.id,
              period.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.project_insert (
    title     gsc.period.title%type,
    status    integer /* just in case */)
  owner to gsc;



create or replace function gsc.project_update (
    p_id          gsc.period.id%type,
    new_title     gsc.period.title%type,
    new_status    integer /* just in case */,
    old_ts        gsc.period.ts%type)
  returns table (ts gsc.period.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.period.ts%type;
begin
  update gsc.period
     set title = new_title,
         status = new_status
   where id = p_id
     and period.ts = old_ts
         returning period.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Проект');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.project_update (
    p_id          gsc.period.id%type,
    new_title     gsc.period.title%type,
    new_status    integer /* just in case */,
    old_ts        gsc.period.ts%type)
  owner to gsc;


create or replace function gsc.project_delete (p_id gsc.period.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.period
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Проект');
  end if;
end;
$$;

alter function gsc.project_delete (p_id gsc.period.id%type)
  owner to gsc;