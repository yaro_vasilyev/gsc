--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    select violation.code as violation_code, 
           violation.name as violation_name,
           stat.cnt::numeric as count
    from (
        select violation_id, sum(cnt) as cnt
          from gsc.r_station_violations (1, p_exam_type)
          join gsc.station on station.id = r_station_violations.station_id
         where p_station_code is null or p_station_code is not null and station.code = p_station_code
           and p_exam_date is null or p_exam_date is not null and r_station_violations.date = p_exam_date
         group by violation_id
    ) as stat
    left join gsc.violation on violation.id = stat.violation_id;

end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;