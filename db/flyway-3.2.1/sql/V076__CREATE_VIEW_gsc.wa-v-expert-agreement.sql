CREATE VIEW gsc.wa_v_expert_agreement (
        guid,
        blob,
        ts,
        filename)
    AS
    SELECT u.guid,
        expa.blob,
        expa.ts,
        expa.filename
    FROM gsc.expert_agreement expa,
        gsc.users u
    WHERE expa.expert_id = u.parent_id AND u.type_user = 0;
		
CREATE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_expert_agreement 
    DO INSTEAD (
    DELETE FROM gsc.expert_agreement
      WHERE expert_agreement.expert_id = (( SELECT u.parent_id
               FROM gsc.users u
              WHERE u.guid = old.guid)); -- comment to avoid migration fail for rule
    );

CREATE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_expert_agreement 
    DO INSTEAD (
    INSERT INTO gsc.expert_agreement (expert_id, blob, ts, filename)
      VALUES (( SELECT u.parent_id
               FROM gsc.users u
              WHERE u.guid = new.guid), new.blob, new.ts, new.filename)
      RETURNING NULL::uuid AS guid,
        expert_agreement.blob,
        expert_agreement.ts,
        expert_agreement.filename; -- comment to avoid migration fail for rule
    );		