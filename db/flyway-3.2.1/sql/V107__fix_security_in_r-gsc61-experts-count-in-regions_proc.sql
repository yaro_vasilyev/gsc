set search_path = gsc,public;


create or replace function gsc.r_gsc61_experts_count_in_regions (
      p_project_id          gsc.period.id%TYPE, 
      p_period_type         gsc.d_period_type)
  returns table (
      RegionCode            gsc.region.code%TYPE, 
      RegionName            gsc.region.name%TYPE, 
      Official              bigint, 
      Social                bigint)
  stable
  language plpgsql
  security invoker
as $$
begin
  return query select r.code,
       r.name,
       sum(case when e.kind = 1 then 1 else 0 end) as official, 
       sum(case when e.kind = 2 then 1 else 0 end) as social
  from gsc.v_schedule s
  join gsc.v_expert e on e.id = s.expert_id and e.period_id = p_project_id and s.period_type = p_period_type
 right join gsc.v_region r on r.id = s.region_id
 group by r.code,
          r.name
 order by r.code;
end;
$$;

alter function gsc.r_gsc61_experts_count_in_regions(
    p_project_id gsc.period.id%TYPE, 
    p_period_type gsc.d_period_type) 
  owner to gsc;