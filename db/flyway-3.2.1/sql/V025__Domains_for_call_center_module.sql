SET search_path = gsc, public;


CREATE DOMAIN d_channel_type AS integer
  CONSTRAINT d_channel_type_check CHECK (((VALUE >= 1) AND (VALUE <= 5)));


ALTER DOMAIN d_channel_type OWNER TO gsc;


--
-- Name: d_violation_location; Type: DOMAIN; Schema: gsc; Owner: gsc
--

CREATE DOMAIN d_violation_location AS integer
  CONSTRAINT d_violation_location_check CHECK ((VALUE = ANY (ARRAY[1, 2, 3, 4])));


ALTER DOMAIN d_violation_location OWNER TO gsc;

--
-- Name: d_event_type; Type: DOMAIN; Schema: gsc; Owner: gsc
--

CREATE DOMAIN d_event_type AS integer
  CONSTRAINT d_event_type_check CHECK ((VALUE = ANY (ARRAY[1, 2, 3, 4])));


ALTER DOMAIN d_event_type OWNER TO gsc;

