SET search_path = gsc, public;

--
-- Name: expert_delete(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION expert_delete(expert_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
  delete from gsc.expert
   where id = expert_id;
  if not found then
    select gsc.err_concurrency_control('Эксперты');
  end if;
end;
$$;

ALTER FUNCTION gsc.expert_delete(expert_id integer) OWNER TO gsc;

--
-- Name: expert_insert(integer, character varying, character varying, character varying, character varying, character varying, character varying, 
--                      integer, date, character varying, integer, character varying, character varying, character varying, character varying, 
--                      character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying, 
--                      character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer); 
-- Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION expert_insert(period_id integer, import_code character varying, surname character varying, 
                  name character varying, patronymic character varying, workplace character varying, 
                  post character varying, post_value integer, birth_date date, birth_place character varying, 
                  base_region_id integer, live_address character varying, phone character varying, email character varying, 
                  expirience character varying, speciality character varying, speciality_value integer, work_years_total integer, 
                  work_years_education integer, work_years_chief integer, exp_years_education integer, awards character varying, 
                  can_mission integer, chief_name character varying, chief_post character varying, chief_email character varying, 
                  observer_id integer, kind integer, test_results integer, interview_status integer, subject_level_value integer, 
                  accuracy_level_value integer, punctual_level_value integer, independency_level_value integer, 
                  moral_level_value integer, docs_impr_part_level_value integer, academic_title_id integer, academic_degree_id integer) 
    RETURNS TABLE(id integer, academic_degree_value integer, academic_title_value integer, 
                  work_years_education_value integer, exp_years_education_value integer, awards_value integer, 
                  can_mission_value integer, test_results_value integer, interview_status_value integer, 
                  stage1_points integer, engage integer, stage2_points integer, efficiency integer, 
                  next_period_proposal integer, ts timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
declare
    l_id integer;
    l_academic_degree_value integer;
    l_academic_title_value integer;
    l_work_years_education_value integer;
    l_exp_years_education_value integer;
    l_awards_value integer;
    l_can_mission_value integer;
    l_test_results_value integer;
    l_interview_status_value integer;
    l_stage1_points integer;
    l_engage integer;
    l_stage2_points integer;
    l_efficiency integer;
    l_next_period_proposal integer;
    l_ts timestamp;
begin
  l_academic_degree_value := gsc.e_academic_degree(academic_degree_id);
  l_academic_title_value := gsc.e_academic_title(academic_title_id);
  l_work_years_education_value := gsc.e_work_years_education(work_years_education);
  l_exp_years_education_value := gsc.e_exp_years_education(exp_years_education);
  l_awards_value := gsc.e_awards(awards);
  l_can_mission_value := gsc.e_can_mission(can_mission);
  l_test_results_value := gsc.e_test_results(test_results);
  l_interview_status_value := gsc.e_interview_status(interview_status);
  
  l_stage1_points := gsc.e_stage1_points(p_exp_years_education  := l_exp_years_education_value,
                                         p_speciality           := speciality_value,
                                         p_work_years_education := l_work_years_education_value,
                                         p_can_mission          := l_can_mission_value,
                                         p_post                 := post_value,
                                         p_academic_degree      := l_academic_degree_value,
                                         p_test_results         := l_test_results_value,
                                         p_interview_status     := l_interview_status_value,
                                         p_academic_title       := l_academic_title_value,
                                         p_awards               := l_awards_value);
  
  l_engage := gsc.e_engage(l_stage1_points, l_test_results_value, l_interview_status_value);
  
  l_stage2_points := gsc.e_stage2_points(p_stage1_points  := l_stage1_points,
                                         p_reports_count  := 0, -- for new expert
                                         p_subject_level  := subject_level_value,
                                         p_accuracy       := accuracy_level_value,
                                         p_punctual       := punctual_level_value,
                                         p_independent    := independency_level_value,
                                         p_moral_level    := moral_level_value,
                                         p_docs_impr_part := docs_impr_part_level_value);
  
  l_efficiency := gsc.e_efficiency(l_stage2_points);
  l_next_period_proposal := gsc.e_next_period_proposal(l_efficiency);
  
  insert into gsc.expert (
    --id,
    period_id,
    import_code,
    surname,
    name,
    patronymic,
    workplace,
    post,
    post_value,
    birth_date,
    birth_place,
    base_region_id,
    live_address,
    phone,
    email,
    academic_degree_value,
    academic_title_value,
    expirience,
    speciality,
    speciality_value,
    work_years_total,
    work_years_education,
    work_years_education_value,
    work_years_chief,
    exp_years_education,
    exp_years_education_value,
    awards,
    awards_value,
    can_mission,
    can_mission_value,
    chief_name,
    chief_post,
    chief_email,
    observer_id,
    kind,
    test_results,
    test_results_value,
    interview_status,
    interview_status_value,
    stage1_points,
    engage,
    subject_level_value,
    accuracy_level_value,
    punctual_level_value,
    independency_level_value,
    moral_level_value,
    docs_impr_part_level_value,
    stage2_points,
    efficiency,
    next_period_proposal,
    academic_title_id,
    academic_degree_id)
  values (
    --id,
    period_id,
    import_code,
    surname,
    name,
    patronymic,
    workplace,
    post,
    post_value,
    birth_date,
    birth_place,
    base_region_id,
    live_address,
    phone,
    email,
    l_academic_degree_value,
    l_academic_title_value,
    expirience,
    speciality,
    speciality_value,
    work_years_total,
    work_years_education,
    l_work_years_education_value,
    work_years_chief,
    exp_years_education,
    l_exp_years_education_value,
    awards,
    l_awards_value,
    can_mission,
    l_can_mission_value,
    chief_name,
    chief_post,
    chief_email,
    observer_id,
    kind,
    test_results,
    l_test_results_value,
    interview_status,
    l_interview_status_value,
    l_stage1_points,
    l_engage,
    subject_level_value,
    accuracy_level_value,
    punctual_level_value,
    independency_level_value,
    moral_level_value,
    docs_impr_part_level_value,
    l_stage2_points,
    l_efficiency,
    l_next_period_proposal,
    academic_title_id,
    academic_degree_id)
  returning expert.id, expert.ts into l_id, l_ts;
  return query select l_id as id,
                l_academic_degree_value as academic_degree_value,
                l_academic_title_value as academic_title_value,
                l_work_years_education_value as work_years_education_value,
                l_exp_years_education_value as exp_years_education_value,
                l_awards_value as awards_value,
                l_can_mission_value as can_mission_value,
                l_test_results_value as test_results_value,
                l_interview_status_value as interview_status_value,
                l_stage1_points as stage1_points,
                l_engage as engage,
                l_stage2_points as stage2_points,
                l_efficiency as efficiency,
                l_next_period_proposal as next_period_proposal,
                l_ts as ts;
end;
$$;

ALTER FUNCTION gsc.expert_insert(period_id integer, import_code character varying, surname character varying, name character varying, 
                                  patronymic character varying, workplace character varying, post character varying, post_value integer, birth_date date, 
                                  birth_place character varying, base_region_id integer, live_address character varying, phone character varying, 
                                  email character varying, expirience character varying, speciality character varying, speciality_value integer, 
                                  work_years_total integer, work_years_education integer, work_years_chief integer, exp_years_education integer, 
                                  awards character varying, can_mission integer, chief_name character varying, chief_post character varying, 
                                  chief_email character varying, observer_id integer, kind integer, test_results integer, interview_status integer, 
                                  subject_level_value integer, accuracy_level_value integer, punctual_level_value integer, independency_level_value integer, 
                                  moral_level_value integer, docs_impr_part_level_value integer, academic_title_id integer, academic_degree_id integer) 
OWNER TO gsc;

--
-- Name: expert_update(integer, character varying, integer, character varying, character varying, character varying, character varying, 
--                      character varying, integer, date, character varying, integer, character varying, character varying, character varying, 
--                      character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, 
--                      character varying, character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, 
--                      integer, integer, timestamp without time zone); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION expert_update(p_id integer, new_import_code character varying, new_period_id integer, new_surname character varying, 
                              new_name character varying, new_patronymic character varying, new_workplace character varying, new_post character varying, 
                              new_post_value integer, new_birth_date date, new_birth_place character varying, new_base_region_id integer, 
                              new_live_address character varying, new_phone character varying, new_email character varying, 
                              new_expirience character varying, new_speciality character varying, new_speciality_value integer, 
                              new_work_years_total integer, new_work_years_education integer, new_work_years_chief integer, new_exp_years_education integer, 
                              new_awards character varying, new_can_mission integer, new_chief_name character varying, new_chief_post character varying, 
                              new_chief_email character varying, new_observer_id integer, new_kind integer, new_test_results integer, 
                              new_interview_status integer, new_subject_level_value integer, new_accuracy_level_value integer, 
                              new_punctual_level_value integer, new_independency_level_value integer, new_moral_level_value integer, 
                              new_docs_impr_part_level_value integer, new_academic_title_id integer, new_academic_degree_id integer, 
                              old_ts timestamp without time zone) 
    RETURNS TABLE(academic_degree_value integer, academic_title_value integer, work_years_education_value integer, exp_years_education_value integer, 
                  awards_value integer, can_mission_value integer, test_results_value integer, interview_status_value integer, stage1_points integer, 
                  engage integer, stage2_points integer, efficiency integer, next_period_proposal integer, ts timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
declare
  l_academic_degree_value integer;
  l_academic_title_value integer;
  l_work_years_education_value integer;
  l_exp_years_education_value integer;
  l_awards_value integer;
  l_can_mission_value integer;
  l_test_results_value integer;
  l_interview_status_value integer;
  l_stage1_points integer;
  l_engage integer;
  l_stage2_points integer;
  l_efficiency integer;
  l_next_period_proposal integer;
  l_ts timestamp;
begin
  l_academic_degree_value := gsc.e_academic_degree(new_academic_degree_id);
  l_academic_title_value := gsc.e_academic_title(new_academic_title_id);
  l_work_years_education_value := gsc.e_work_years_education(new_work_years_education);
  l_exp_years_education_value := gsc.e_exp_years_education(new_exp_years_education);
  l_awards_value := gsc.e_awards(new_awards);
  l_can_mission_value := gsc.e_can_mission(new_can_mission);
  l_test_results_value := gsc.e_test_results(new_test_results);
  l_interview_status_value := gsc.e_interview_status(new_interview_status);
  
  l_stage1_points := gsc.e_stage1_points(p_exp_years_education  := l_exp_years_education_value,
                                         p_speciality           := new_speciality_value,
                                         p_work_years_education := l_work_years_education_value,
                                         p_can_mission          := l_can_mission_value,
                                         p_post                 := new_post_value,
                                         p_academic_degree      := l_academic_degree_value,
                                         p_test_results         := l_test_results_value,
                                         p_interview_status     := l_interview_status_value,
                                         p_academic_title       := l_academic_title_value,
                                         p_awards               := l_awards_value);
  
  l_engage := gsc.e_engage(l_stage1_points, l_test_results_value, l_interview_status_value);
  
  l_stage2_points := gsc.e_stage2_points(p_stage1_points  := l_stage1_points,
                                         p_reports_count  := gsc.cc_expert_reports_count(p_id, new_period_id),
                                         p_subject_level  := new_subject_level_value,
                                         p_accuracy       := new_accuracy_level_value,
                                         p_punctual       := new_punctual_level_value,
                                         p_independent    := new_independency_level_value,
                                         p_moral_level    := new_moral_level_value,
                                         p_docs_impr_part := new_docs_impr_part_level_value);
  
  l_efficiency := gsc.e_efficiency(l_stage2_points);
  l_next_period_proposal := gsc.e_next_period_proposal(l_efficiency);
  
  update gsc.expert set
    period_id = new_period_id,
    import_code = new_import_code,
    surname = new_surname,
    name = new_name,
    patronymic = new_patronymic,
    workplace = new_workplace,
    post = new_post,
    post_value = new_post_value,
    birth_date = new_birth_date,
    birth_place = new_birth_place,
    base_region_id = new_base_region_id,
    live_address = new_live_address,
    phone = new_phone,
    email = new_email,
    academic_degree_value = l_academic_degree_value,
    academic_title_value = l_academic_title_value,
    expirience = new_expirience,
    speciality = new_speciality,
    speciality_value = new_speciality_value,
    work_years_total = new_work_years_total,
    work_years_education = new_work_years_education,
    work_years_education_value = l_work_years_education_value,
    work_years_chief = new_work_years_chief,
    exp_years_education = new_exp_years_education,
    exp_years_education_value = l_exp_years_education_value,
    awards = new_awards,
    awards_value = l_awards_value,
    can_mission = new_can_mission,
    can_mission_value = l_can_mission_value,
    chief_name = new_chief_name,
    chief_post = new_chief_post,
    chief_email = new_chief_email,
    observer_id = new_observer_id,
    kind = new_kind,
    test_results = new_test_results,
    test_results_value = l_test_results_value,
    interview_status = new_interview_status,
    interview_status_value = l_interview_status_value,
    stage1_points = l_stage1_points,
    engage = l_engage,
    subject_level_value = new_subject_level_value,
    accuracy_level_value = new_accuracy_level_value,
    punctual_level_value = new_punctual_level_value,
    independency_level_value = new_independency_level_value,
    moral_level_value = new_moral_level_value,
    docs_impr_part_level_value = new_docs_impr_part_level_value,
    stage2_points = l_stage2_points,
    efficiency = l_efficiency,
    next_period_proposal = l_next_period_proposal,
    academic_title_id = new_academic_title_id,
    academic_degree_id = new_academic_degree_id
  where
    id = p_id and
    expert.ts = old_ts
  returning expert.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Эксперты');
  end if;
  
  return query select l_academic_degree_value as academic_degree_value,
                      l_academic_title_value as academic_title_value,
                      l_work_years_education_value as work_years_education_value,
                      l_exp_years_education_value as exp_years_education_value,
                      l_awards_value as awards_value,
                      l_can_mission_value as can_mission_value,
                      l_test_results_value as test_results_value,
                      l_interview_status_value as interview_status_value,
                      l_stage1_points as stage1_points,
                      l_engage as engage,
                      l_stage2_points as stage2_points,
                      l_efficiency as efficiency,
                      l_next_period_proposal as next_period_proposal,
                      l_ts as ts;
end;
$$;

ALTER FUNCTION gsc.expert_update(p_id integer, new_import_code character varying, new_period_id integer, 
                                  new_surname character varying, new_name character varying, 
                                  new_patronymic character varying, new_workplace character varying, 
                                  new_post character varying, new_post_value integer, new_birth_date date, 
                                  new_birth_place character varying, new_base_region_id integer, 
                                  new_live_address character varying, new_phone character varying, 
                                  new_email character varying, new_expirience character varying, 
                                  new_speciality character varying, new_speciality_value integer, 
                                  new_work_years_total integer, new_work_years_education integer, 
                                  new_work_years_chief integer, new_exp_years_education integer, 
                                  new_awards character varying, new_can_mission integer, new_chief_name character varying, 
                                  new_chief_post character varying, new_chief_email character varying, 
                                  new_observer_id integer, new_kind integer, new_test_results integer, 
                                  new_interview_status integer, new_subject_level_value integer, 
                                  new_accuracy_level_value integer, new_punctual_level_value integer, 
                                  new_independency_level_value integer, new_moral_level_value integer, 
                                  new_docs_impr_part_level_value integer, new_academic_title_id integer, 
                                  new_academic_degree_id integer, old_ts timestamp without time zone) 
OWNER TO gsc;
