set search_path = gsc,public;


CREATE OR REPLACE FUNCTION gsc.u_create_user (
  type_ integer,
  id_ integer,
  pg_user smallint = 0
)
RETURNS varchar AS
$body$
DECLARE
  ob_role     text;
  email VARCHAR;  
  password VARCHAR; 
  l_user_guid UUID;
BEGIN
 CASE type_
   WHEN 0 THEN -- expert
    SELECT exp.email INTO STRICT email
      FROM gsc.expert exp           
      WHERE exp.id = id_;
      
      password:= gsc.u_random_string(8);
  --  password:= '12345'; -- for test
      
      INSERT INTO gsc.users
        (parent_id, type_user, user_login, pass, guid, agreement, rolname)   
      VALUES
        (id_, 0, email, 'md5' || md5(password || email), public.uuid_generate_v4(), null, gsc.role_expert())
      ON CONFLICT (parent_id, type_user) DO
        UPDATE SET 
          user_login = EXCLUDED.user_login, 
          pass = EXCLUDED.pass, 
          agreement = EXCLUDED.agreement, 
          rolname = EXCLUDED.rolname
      RETURNING guid into l_user_guid;
      
      UPDATE gsc.expert e
        SET user_guid = l_user_guid
        WHERE e.id = id_;
      
      IF pg_user = 1 THEN
        execute format('create role %I login password ''%s'' in role %I;', email, password, gsc.role_expert());
      END IF;
      RETURN password;
   WHEN 1 THEN -- manager (observer)
    select ob.email, 
             case 
               when ob.observer_type_manager then gsc.role_manager()
               when ob.observer_type_rsm then gsc.role_rsm()
             end
        into strict email, 
                    ob_role
        from gsc.observer as ob
       where ob.id = id_;
      
      password:= gsc.u_random_string(8);
  --  password:= '12345'; -- for test
      
      INSERT INTO gsc.users
        (parent_id, type_user, user_login, pass, guid, agreement, rolname)   
      VALUES
        (id_, 1, email, 'md5' || md5(password || email), public.uuid_generate_v4(), null, ob_role)
      ON CONFLICT (parent_id, type_user) DO
        UPDATE SET 
          user_login = EXCLUDED.user_login, 
          pass = EXCLUDED.pass, 
          agreement = EXCLUDED.agreement, 
          rolname = EXCLUDED.rolname
      RETURNING guid INTO l_user_guid;
        
      UPDATE gsc.observer o
        SET user_guid = l_user_guid
        WHERE o.id = id_;
        
      IF pg_user = 1 THEN
        execute format('create role %I login password ''%s'' in role %I;', email, password, ob_role);
      END IF;    
      RETURN password;
   ELSE
       RETURN NULL;
 END CASE;  
EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

COMMENT ON FUNCTION gsc.u_create_user(type_ integer, id_ integer, pg_user smallint)
IS 'return password
type_ - 0=expet, 1=manager
type_ - id table expert or observer
pg_user - defaut 0, if 1 then create db user

';

alter function gsc.u_create_user (
  type_ integer,
  id_ integer,
  pg_user smallint 
) owner to gsc;

