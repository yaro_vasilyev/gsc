set search_path = gsc,public;

----------------------------------------------------------------------------------------------------
-- Add column
----------------------------------------------------------------------------------------------------

alter table gsc.violation
  add column project_id int,
  add constraint violation_project_id_fkey foreign key (project_id) references gsc.period (id);

update gsc.violation
   set project_id = (select id
                       from gsc.period
                      where status = 1)
 where project_id is null;


alter table gsc.violation
  alter column project_id set not null;

----------------------------------------------------------------------------------------------------
-- Recreate view
----------------------------------------------------------------------------------------------------

create or replace view gsc.v_violation
as
  select * from gsc.violation;

alter view gsc.v_violation owner to gsc;

----------------------------------------------------------------------------------------------------
-- CRUD functions
----------------------------------------------------------------------------------------------------

create or replace function gsc.violation_insert (
    p_project_id    gsc.period.id%type,
    code            gsc.violation.code%type,
    name            gsc.violation.name%type)
  returns table (
    id              gsc.violation.id%type, 
    ts              gsc.violation.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id      gsc.violation.id%type;
  l_ts      gsc.violation.ts%type;
begin
  insert into gsc.violation (
              project_id,
              code,
              name)
      values (p_project_id, 
              code,
              name)
    returning violation.id,
              violation.ts
         into l_id,
              l_ts;

  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.violation_insert (
    p_project_id  gsc.period.id%type,
    code          gsc.violation.code%type,
    name          gsc.violation.name%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------
create or replace function gsc.violation_update (
    p_id                gsc.violation.id%type,
    p_new_project_id    gsc.period.id%type,
    new_code            gsc.violation.code%type,
    new_name            gsc.violation.name%type,
    old_ts              gsc.violation.ts%type)
  returns table (
    ts                  gsc.violation.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts      gsc.violation.ts%type;
begin
  update gsc.violation
     set project_id   = p_new_project_id,
         code         = new_code,
         name         = new_name
   where id           = p_id
     and violation.ts = old_ts
         returning violation.ts into l_ts;

  if not found then
    select gsc.err_concurrency_control('Нарушение');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.violation_update (
    p_id              gsc.violation.id%type,
    p_new_project_id  gsc.period.id%type,
    new_code          gsc.violation.code%type,
    new_name          gsc.violation.name%type,
    old_ts            gsc.violation.ts%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------
-- Drop old ones
----------------------------------------------------------------------------------------------------

drop function if exists gsc.violation_insert (
    code      gsc.violation.code%type,
    name      gsc.violation.name%type);


drop function gsc.violation_update (
    p_id        gsc.violation.id%type,
    new_code    gsc.violation.code%type,
    new_name    gsc.violation.name%type,
    old_ts      gsc.violation.ts%type);


----------------------------------------------------------------------------------------------------
-- That's all folks!
----------------------------------------------------------------------------------------------------
