set search_path = gsc,public;

drop view if exists gsc.wa_v_mgr_event;

CREATE OR REPLACE VIEW gsc.wa_v_mgr_event (
    id,
    event_ts,
    channel,
    type,
    schedule_id,
    object_type)
AS
SELECT event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type
FROM gsc.event
WHERE event.project_id = gsc.current_project_id();

alter view gsc.wa_v_mgr_event
    owner to gsc;

CREATE OR REPLACE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_mgr_event 
DO INSTEAD (
DELETE FROM gsc.event
  WHERE event.id = old.id; -- flyway
);

CREATE OR REPLACE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_mgr_event 
DO INSTEAD (
INSERT INTO gsc.event (event_ts, channel, type, schedule_id, object_type, project_id)
  VALUES (new.event_ts, new.channel, new.type, new.schedule_id, new.object_type, gsc.current_project_id())
  RETURNING event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type; -- flyway
);

CREATE OR REPLACE RULE "_UPDATE" AS ON UPDATE TO gsc.wa_v_mgr_event 
DO INSTEAD (
UPDATE gsc.event SET event_ts = new.event_ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id, object_type = new.object_type
  WHERE event.id = old.id; -- flyway
);



CREATE OR REPLACE VIEW gsc.wa_v_expert_agreement AS 
  SELECT e.user_guid as guid,
    expa.blob,
    expa.expert_agreement_ts AS ts,
    expa.filename
   FROM gsc.expert_agreement expa
    join gsc.expert e on e.id = expa.expert_id;

alter view gsc.wa_v_expert_agreement
  owner to gsc;

CREATE OR REPLACE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_expert_agreement 
DO INSTEAD (
DELETE FROM gsc.expert_agreement
  WHERE expert_agreement.expert_id = (( SELECT e.id
           FROM gsc.expert e
          WHERE e.user_guid = old.guid)); -- flyway
);

CREATE OR REPLACE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_expert_agreement 
DO INSTEAD (
INSERT INTO gsc.expert_agreement (expert_id, blob, expert_agreement_ts, filename)
  VALUES (( SELECT e.id
           FROM gsc.expert e
          WHERE e.user_guid = new.guid), new.blob, new.ts, new.filename)
  RETURNING NULL::uuid AS guid,
    expert_agreement.blob,
    expert_agreement.expert_agreement_ts AS ts,
    expert_agreement.filename; -- flyway
);


CREATE OR REPLACE VIEW gsc.wa_v_exp_monitor_result (
    id,
    guid,
    schedule_id,
    date,
    violation_id,
    violator_id,
    note,
    object_type,
    schedule_detail_id,
    exam_type,
    event_id)
AS
SELECT mr.id,
    u.guid,
    mr.schedule_id,
    mr.date,
    mr.violation_id,
    mr.violator_id,
    mr.note,
    mr.object_type,
    mr.schedule_detail_id,
    mr.exam_type,
    mr.event_id
FROM gsc.monitor_result mr
     JOIN gsc.schedule sch ON sch.id = mr.schedule_id
     JOIN gsc.expert e ON e.id = sch.expert_id
     LEFT JOIN gsc.users u ON gsc._xxx_roles_all(u.rolname, VARIADIC
         ARRAY[gsc.role_expert()::name]) AND u.guid = e.user_guid;

alter VIEW gsc.wa_v_exp_monitor_result
  owner to gsc;