set search_path = gsc,public;

create domain d_boolean_type as integer
  constraint d_boolean_type_check check (value in (0, 1));
alter domain d_boolean_type
  owner to gsc;

create table station (
  id serial not null primary key,
  region_id integer not null references region(id),
  project_id integer not null references period(id),
  code integer not null,
  name varchar(500) not null,
  address varchar(500) not null,
  tom d_boolean_type not null
);

alter table station
  owner to gsc;

