set search_path = gsc,public;

create or replace function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  returns void
  volatile
  security invoker
  language plpgsql
as $$
declare 
  e gsc.expert;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в исходный проект';
  end if;
  
  for e in 
      select * 
        from gsc.expert 
       where expert.period_id = p_source_project_id
         and expert.next_period_proposal in (1, 2) -- main, reserve
  loop
    perform gsc.expert_insert (
      period_id => p_target_project_id,
      import_code => e.import_code,
      surname => e.surname,
      name => e.name,
      patronymic => e.patronymic,
      workplace => e.workplace,
      post => e.post,
      post_value => e.post_value,
      birth_date => e.birth_date,
      birth_place => e.birth_place,
      base_region_id => e.base_region_id,
      live_address => e.live_address,
      phone => e.phone,
      email => e.email,
      expirience => e.expirience,
      speciality => e.speciality,
      speciality_value => e.speciality_value,
      work_years_total => e.work_years_total,
      work_years_education => e.work_years_education,
      work_years_chief => e.work_years_chief,
      exp_years_education => e.exp_years_education,
      awards => e.awards,
      can_mission => e.can_mission,
      chief_name => e.chief_name,
      chief_post => e.chief_post,
      chief_email => e.chief_email,
      observer_id => null,
      kind => e.kind,
      test_results => e.test_results,
      interview_status => e.interview_status,
      subject_level_value => e.subject_level_value,
      accuracy_level_value => e.accuracy_level_value,
      punctual_level_value => e.punctual_level_value,
      independency_level_value => e.independency_level_value,
      moral_level_value => e.moral_level_value,
      docs_impr_part_level_value => e.docs_impr_part_level_value,
      academic_title_id => e.academic_title_id,
      academic_degree_id => e.academic_degree_id,
      user_guid => e.user_guid);
  end loop;
end;
$$;

alter function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  owner to gsc;