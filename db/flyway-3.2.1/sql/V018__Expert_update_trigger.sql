SET search_path = gsc, public;

--
-- Name: expert_update_ts(); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION expert_update_ts() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.ts := now();
  return new;
end;
$$;


ALTER FUNCTION gsc.expert_update_ts() OWNER TO gsc;

--
-- Name: expert_ts; Type: TRIGGER; Schema: gsc; Owner: gsc
--

CREATE TRIGGER expert_ts BEFORE UPDATE ON expert FOR EACH ROW EXECUTE PROCEDURE expert_update_ts();
