set search_path = gsc,public;


create or replace function gsc.r_gsc060_expert_list (gsc.period.id%type, 
                                                     gsc.expert.kind%type,
                                                     gsc.observer.id%type,
                                                     boolean)
  returns table (
          ExpertId            gsc.expert.id%type,
          ExpertFio           text,
          BaseRegion          gsc.region.name%type,
          ScheduleRegion      text,
          MonitorObjects      text,
          ExpertPostAndWp     text,
          ExpertPhone         gsc.expert.phone%type,
          ExpertEmail         gsc.expert.email%type,
          ExpertAdAt          text,
          Expirience          gsc.expert.expirience%type)
  language sql
  security invoker
  stable
as $$
with cte as (
  select cte_ex.id as expId,
         case
          when exists(select id from gsc.v_schedule_detail as cte_scd where cte_scd.schedule_id = cte_sc.id) then
            case when cte_sc.monitor_rcoi = 1 then /*'ППЭ/РЦОИ'*/3 else /*'ППЭ'*/2 end
          else
            case when cte_sc.monitor_rcoi = 1 then /*'РЦОИ'*/1 else /*''*/0 end
         end as monObj,
         cte_r.name as monReg
    from gsc.v_schedule as cte_sc
    join gsc.v_expert as cte_ex on cte_sc.expert_id = cte_ex.id
    join gsc.v_region as cte_r on cte_r.id = cte_sc.region_id
)
  select e.id as ExpertId,
         concat_ws(' ', e.surname, e.name, e.patronymic) as ExpertFio,
         r.name as BaseRegion,
         array_to_string((select array_agg(distinct cte.monReg) from cte where cte.expId = e.id), ', ') as ScheduleRegion,
         (select case max(monObj) 
                  when 3 then 'ППЭ/РЦОИ' 
                  when 2 then 'ППЭ'
                  when 1 then 'РЦОИ'
                  else '' end 
                 from cte where cte.expId = e.id) as MonitorObjects,
         concat_ws(', ', e.post, e.workplace) as ExpertPostAndWp,
         e.phone as ExpertPhone,
         e.email as ExpertEmail,
         concat_ws(', ', ad.name, at.name) as ExpertAdAt,
         e.expirience as Expirience
    from gsc.v_expert as e
    join gsc.v_region as r on r.id = e.base_region_id
    left join gsc.v_academic_title as at on at.id = e.academic_title_id
    left join gsc.v_academic_degree as ad on ad.id = e.academic_degree_id
  where e.engage = 1
         and
         (e.period_id = $1) and
         ($2 is null or ($2 is not null and e.kind = $2)) and
         (
           ($4 is true and ($3 is null and e.observer_id is null or e.observer_id = $3))
           or
           ($4 is false and (($3 is null or ($3 is not null and e.observer_id = $3))))
         )
$$;

alter function gsc.r_gsc060_expert_list (gsc.period.id%type, 
                                         gsc.expert.kind%type,
                                         gsc.observer.id%type,
                                         boolean)
  owner to gsc;