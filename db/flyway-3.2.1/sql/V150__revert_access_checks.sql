set search_path = gsc,public;
--------------------------------------------------------------------------------
-- Roles/Grants lexical functions
--------------------------------------------------------------------------------
create or replace function gsc.g_rsm (
    out module            text,
    out list              text,
    out new               text,
    out edit              text,
    out delete            text,
    out export_to_excel   text)
  language plpgsql
  immutable
  security invoker
as $$
begin
  module            := 's_rsm_module';
  list              := 's_rsm_list';
  new               := 's_rsm_new';
  edit              := 's_rsm_edit';
  delete            := 's_rsm_delete';
  export_to_excel   := 's_rsm_export';
end;
$$;

alter function gsc.g_rsm ()
  owner to gsc;


--------------------------------------------------------------------------------
create or replace function gsc.g_managers (
    out module            text,
    out list              text,
    out new               text,
    out edit              text,
    out delete            text,
    out export_to_excel   text)
  returns record
  immutable
  security invoker
  language plpgsql
as $$
begin
  module            := 's_manager_module';
  list              := 's_manager_list';
  new               := 's_manager_new';
  edit              := 's_manager_edit';
  delete            := 's_manager_delete';
  export_to_excel   := 's_manager_export';
end;
$$;

alter function gsc.g_managers ()
  owner to gsc;


--------------------------------------------------------------------------------
create or replace function gsc.g_experts (
    out module            text,
    out list              text,
    out edit              text,
    out delete            text,
    out new               text,
    out import_from_csv   text,
    out export_to_excel   text,
    out aggr_add          text,
    out aggr_remove       text,
    out aggr_preview      text)
  returns record
  immutable
  language plpgsql
  security invoker
as $$
begin
  module            := 's_expert_module';
  list              := 's_expert_list';
  edit              := 's_expert_edit';
  delete            := 's_expert_delete';
  new               := 's_expert_new';
  import_from_csv   := 's_expert_import';
  export_to_excel   := 's_expert_export';
  aggr_add          := 's_expert_agr_upload';
  aggr_remove       := 's_expert_agr_remove';
  aggr_preview      := 's_expert_agr_preview';
end;
$$;

alter function gsc.g_experts ()
  owner to gsc;


--------------------------------------------------------------------------------
create or replace function gsc.roles (
    out admin         text,
    out sysadmin      text,
    out an            text,
    out an_lead       text,
    out boss          text,
    out manager       text,
    out rsm           text,
    out expert        text)
  returns     record
  language    plpgsql
  security    invoker
  immutable
as $$
begin
  admin         := 'r_admin';
  sysadmin      := 'r_sysadmin';
  an            := 'r_an';
  an_lead       := 'r_an_lead';
  boss          := 'r_boss';
  manager       := 'r_manager';
  rsm           := 'r_rsm';
  expert        := 'r_expert';
end;
$$;

alter function gsc.roles ()
  owner to gsc;

--------------------------------------------------------------------------------
-- Revert get_experts/get_observers access checks
--------------------------------------------------------------------------------
create or replace function gsc.get_observers (
    p_rolname         name)
  returns setof gsc.observer
  language plpgsql
  stable
  called on null input
  security definer
as $$
declare
  l_user_guid         gsc.users.guid%type;
  l_observer_id       gsc.observer.id%type;
begin

  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users.', p_rolname;
      return;
  end;
  
  if gsc._xxx_roles_all(p_rolname, 
        (gsc.roles ()).boss, 
        (gsc.roles ()).admin, 
        (gsc.roles ()).sysadmin, 
        (gsc.roles ()).an, 
        (gsc.roles ()).an_lead,
        (gsc.roles ()).manager) then
    return query 
      select * 
        from gsc.observer; -- everyone
  elsif _xxx_roles_all(p_rolname, (gsc.roles ()).rsm) then
    return query 
      select o.* 
        from gsc.observer as o 
       where o.observer_type_rsm; -- rsm people only
  else
    -- I'm expert here maybe, so show me my observer
    select observer_id
      into l_observer_id
      from gsc.expert
     where user_guid = l_user_guid;
    
    return query 
      select * 
        from gsc.observer as o 
       where l_observer_id  is not null 
         and o.id           = l_observer_id; -- expert's observer only
  /*else
    return; -- noone*/
  end if;

  return;    
end;
$$;

alter function gsc.get_observers (name)
  owner to gsc;


--------------------------------------------------------------------------------
create or replace function gsc.get_experts (
    p_rolname       name)
  returns     setof gsc.expert
  language    plpgsql
  security    definer
  stable
as
$$
declare
  l_user_guid gsc.users.guid%type;
begin
  
  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users', p_rolname;
      return; -- nope, user is not in the system
  end;
  
  if gsc._xxx_roles_any(p_rolname, 
        gsc.grant_users_list(),
        (gsc.roles ()).admin, 
        (gsc.roles ()).sysadmin, 
        (gsc.roles ()).boss, 
        (gsc.roles ()).an_lead, 
        (gsc.roles ()).an, 
        (gsc.roles ()).manager) then
    return query 
      select * 
        from gsc.expert; -- EVERRRRRYYYYYOOOOONNNNEEEE!!!
  elsif gsc._xxx_roles_all(p_rolname, (gsc.roles ()).rsm) then
    return query 
      select e.* 
        from gsc.expert as e 
       where e.expert_kind_social; --only rsm people
  else --if gsc._xxx_roles_all(p_rolname, gsc.role_expert()) then
  -- if select can find a record with user guid then it's expert himself!
    return query 
      select * 
        from gsc.expert 
       where user_guid = l_user_guid; -- only self
  /*else
    return; -- noone here*/
  end if;
  return;
end;
$$;

alter function gsc.get_experts (name)
  owner to gsc;

--------------------------------------------------------------------------------
-- That's all folks!
--------------------------------------------------------------------------------
