CREATE OR REPLACE FUNCTION gsc.current_project_id (
)
RETURNS integer AS
$body$
DECLARE
  l_id integer;
BEGIN
  SELECT p.id
  INTO l_id
  FROM gsc.period p
  WHERE p.status = 1;
  
  RETURN l_id;
EXCEPTION
  WHEN no_data_found THEN
    return null;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

COMMENT ON FUNCTION gsc.current_project_id()
IS 'Returns current project id. If there is no current project (opened zero projects) returns null.';