set search_path = gsc,public;

alter function gsc.expert_insert(integer, varchar, varchar, varchar, varchar, 
    varchar, varchar, integer, date, varchar, integer, varchar, varchar, varchar, 
    varchar, varchar, integer, integer, integer, integer, integer, varchar, 
    integer, varchar, varchar, varchar, integer, integer, integer, integer, 
    integer, integer, integer, integer, integer, integer, integer, integer, uuid)
  security definer;

alter function gsc.expert_update(integer, varchar, integer, varchar, varchar, 
    varchar, varchar, varchar, integer, date, varchar, integer, varchar, varchar, 
    varchar, varchar, varchar, integer, integer, integer, integer, integer, 
    varchar, integer, varchar, varchar, varchar, integer, integer, integer, 
    integer, integer, integer, integer, integer, integer, integer, integer, 
    integer, uuid, timestamp)
  security definer;
alter function gsc.expert_delete(integer)
  security definer;
alter function gsc.import_row_expert(uuid, varchar, varchar, varchar, 
    varchar, varchar, varchar, integer, date, varchar, varchar, varchar, varchar, 
    varchar, varchar, integer, varchar, integer, varchar, varchar, integer, 
    integer, integer, integer, integer, integer, integer, varchar, integer, 
    integer, integer, varchar, varchar, varchar)
  security definer;
alter function gsc.import_expert(uuid, integer)
  security definer;
alter function gsc.import_row_expert(uuid, varchar, varchar, varchar, 
    varchar, varchar, varchar, integer, date, varchar, varchar, varchar, varchar, 
    varchar, varchar, integer, varchar, integer, varchar, varchar, integer, 
    integer, integer, integer, integer, integer, integer, varchar, integer, 
    integer, integer, varchar, varchar, varchar)
  security definer;
alter function gsc.expert_agreement_insert(integer, varchar, bytea)
  security definer;
alter function gsc.expert_agreement_update(integer, timestamp, 
    varchar, bytea, timestamp)
  security definer;
alter function gsc.expert_agreement_delete(integer)
  security definer;
  
---------------------------------------------------------------------------------------------


alter function gsc.region_insert(integer, varchar, integer)
  security definer;


alter function gsc.region_update(integer, integer, varchar, integer, timestamp)
  security definer;

alter function gsc.region_delete(integer)
  security definer;


alter function gsc.import_region(uuid)
  security definer;

alter function gsc.import_row_region(uuid, integer, varchar)
  security definer;

---------------------------------------------------------------------------------------------

alter function gsc.exec_update(integer, integer, integer, integer, integer, integer, integer, integer, timestamp)
  security definer;

alter function gsc.exec_insert(integer, integer, integer, integer, integer, integer, integer)
  security definer;

alter function gsc.exec_delete(integer)
  security definer;

alter function gsc.import_exec(uuid, integer)
  security definer;

alter function gsc.import_row_exec(uuid, integer, integer, integer, integer, 
    integer, integer, integer, gsc.d_exam_type)
  security definer;

---------------------------------------------------------------------------------------------


alter function gsc.exam_insert(integer, date, integer, integer, integer, integer)
  security definer;

alter function gsc.exam_update(integer, integer, date, integer, integer, integer, integer, timestamp)
  security definer;

alter function gsc.exam_delete(integer)
  security definer;

alter function gsc.import_row_exam(uuid, integer, integer, integer, integer, date)
  security definer;

alter function gsc.import_exam(uuid, integer)
  security definer;

---------------------------------------------------------------------------------------------

alter function gsc.station_insert(integer, integer, integer, varchar, varchar, integer, integer)
  security definer;

alter function gsc.station_update(integer, integer, integer, integer, varchar, 
  varchar, integer, integer, timestamp) security definer;

alter function gsc.station_delete(integer) security definer;

alter function gsc.import_row_station(uuid, integer, integer, 
    varchar, varchar, integer, gsc.d_exam_type) security definer;

alter function gsc.import_station(uuid, integer) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.violation_insert(integer, varchar) security definer;
alter function gsc.violation_update(integer, integer, varchar, timestamp) security definer;
alter function gsc.violation_delete(integer) security definer;
alter function gsc.import_row_violation(uuid, integer, varchar) security definer;
alter function gsc.import_violation(uuid) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.violator_insert(code integer, name varchar) security definer;
alter function gsc.violator_update(p_id integer, new_code integer, new_name varchar, old_ts timestamp) security definer;
alter function gsc.violator_delete(p_id integer) security definer;
alter function gsc.import_violator(p_imp_session_guid uuid) security definer;
alter function gsc.import_row_violator(imp_session_guid uuid, code integer, name varchar) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.observer_insert(integer, varchar, varchar, varchar, varchar, varchar, varchar, uuid) security definer;
alter function gsc.observer_update(integer, integer, varchar, varchar, varchar, varchar, varchar, varchar, uuid, timestamp) security definer;
alter function gsc.observer_delete(integer) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.academic_degree_insert(varchar, varchar, integer) security definer;
alter function gsc.academic_degree_update(integer, varchar, varchar, integer, timestamp) security definer;
alter function gsc.academic_degree_delete(integer) security definer;
alter function gsc.import_row_academic_degree(uuid, varchar, varchar) security definer;
alter function gsc.import_academic_degree(uuid) security definer;


---------------------------------------------------------------------------------------------

alter function gsc.academic_title_insert(varchar, varchar, integer) security definer;
alter function gsc.academic_title_update(integer, varchar, varchar, integer, timestamp) security definer;
alter function gsc.academic_title_delete(integer) security definer;
alter function gsc.import_row_academic_title(uuid, varchar, varchar) security definer;
alter function gsc.import_academic_title(uuid) security definer;

---------------------------------------------------------------------------------------------


alter function gsc.import_row_subject(uuid, integer, varchar) security definer;
alter function gsc.import_subject(uuid) security definer;
alter function gsc.subject_insert(integer, varchar) security definer;
alter function gsc.subject_update(integer, integer, varchar, timestamp) security definer;
alter function gsc.subject_delete(integer) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.schedule_delete(integer) security definer;
alter function gsc.schedule_detail_delete(integer) security definer;
alter function gsc.schedule_detail_insert(integer, integer, date) security definer;
alter function gsc.schedule_detail_update(integer, integer, integer, date, timestamp) security definer;
alter function gsc.schedule_insert(integer, integer, integer, integer, integer, integer) security definer;
alter function gsc.schedule_update(integer, integer, integer, integer, integer, integer, integer, timestamp) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.monitor_result_delete(integer) security definer;
alter function gsc.monitor_result_insert(integer, date, integer, integer, text, integer, integer, integer, integer) security definer;
alter function gsc.monitor_result_update(integer, integer, date, integer, integer, text, integer, integer, integer, integer, timestamp) security definer;
alter function gsc.mr_detail_delete(integer) security definer;
alter function gsc.mr_detail_insert(integer, integer) security definer;
alter function gsc.mr_detail_update(integer, integer, integer, timestamp) security definer;


---------------------------------------------------------------------------------------------


alter function gsc.mr_doc_delete(p_id integer) security definer;
alter function gsc.mr_doc_insert(schedule_id integer, filename varchar, blob bytea) security definer;
alter function gsc.mr_doc_update(p_id integer, new_schedule_id integer, new_blob bytea, new_mr_doc_ts timestamp, new_filename varchar, old_ts timestamp) security definer;
