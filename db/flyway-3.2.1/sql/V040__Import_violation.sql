-- Делаем поле violation.code уникальным
ALTER TABLE gsc.violation
  ADD UNIQUE (code);

-- Import table
CREATE TABLE gsc.imp_violation (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
  code INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  CONSTRAINT imp_violation_pkey PRIMARY KEY(imp_session_guid, code)
) 
WITH (oids = false);

COMMENT ON TABLE gsc.imp_violation
IS 'Violation import table.';

COMMENT ON COLUMN gsc.imp_violation.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_violation.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_violation.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_violation.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_violation.code
IS 'Violation code.';

COMMENT ON COLUMN gsc.imp_violation.name
IS 'Violation name.';

-- Import row function
CREATE FUNCTION gsc.import_row_violation (
  imp_session_guid uuid,
  code integer,
  name varchar
)
RETURNS void AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  INSERT INTO gsc.imp_violation (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-- Bulk import function
CREATE OR REPLACE FUNCTION gsc.import_violation (
  p_imp_session_guid uuid
)
RETURNS TABLE (
  code integer,
  name varchar
) AS
$body$
DECLARE
  l_imported integer [];
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO violation AS ad (code, name)
      SELECT
        iad.code imp_code,
        iad.name
      FROM imp_violation iad
      WHERE iad.imp_session_guid = p_imp_session_guid AND iad.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT violation_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING ad.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_violation iad
  SET imp_status = 'imported'
  WHERE iad.imp_session_guid = p_imp_session_guid;
  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ad.code,
                 ad.name
               FROM gsc.violation ad
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_violation iad
                                WHERE iad.imp_session_guid = p_imp_session_guid AND
                                      ad.code = iad.code);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;