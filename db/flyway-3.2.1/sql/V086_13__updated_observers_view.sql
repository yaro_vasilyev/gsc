set search_path = gsc,public;


create or replace function gsc.get_observers(p_rolname gsc.users.rolname%type)
  returns setof gsc.observer
  stable
  security definer
  language plpgsql
as $$
declare
  l_user_guid gsc.users.guid%type;
  l_observer_id gsc.observer.id%type;
begin

  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users.', p_rolname;
      return;
  end;
  
  if _xxx_roles_any(p_rolname, role_boss(), role_admin(), role_an_lead(), role_an(), role_manager()) then
    return query select * from gsc.observer; -- everyone
  elsif _xxx_roles_all(p_rolname, role_rsm()) then
    return query select o.* from gsc.observer as o where o.observer_type_rsm; -- rsm people only
  elsif gsc._xxx_roles_all(p_rolname, role_expert()) then

    select observer_id
      into l_observer_id
      from gsc.expert
     where user_guid = l_user_guid;
    
    return query select * from gsc.observer as o where l_observer_id is not null and o.id = l_observer_id; -- expert's observer only
  else
    return; -- noone
  end if;

  return;    
end;
$$;

alter function gsc.get_observers(gsc.users.rolname%type)
  owner to gsc;
  

create or replace view gsc.v_observer
as
  select *
    from gsc.get_observers(session_user);
    
alter view gsc.v_observer
  owner to gsc;
