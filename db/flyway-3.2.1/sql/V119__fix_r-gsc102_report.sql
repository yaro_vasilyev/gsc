
set search_path = gsc,public;

drop function if exists gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type);

create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
       where e.project_id = p_project_id
         and exam.period_id = p_project_id
    ),
    cte_count_by_expert_etc as (
      select cte_station_helper.exam_date as date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code as station_code, 
             count(*) as cnt
        from gsc.monitor_result as mr
        join gsc.schedule as sch on sch.id = mr.schedule_id
        left join cte_station_helper on mr.id = cte_station_helper.monitor_result_id
        left join gsc.station as s on s.id = cte_station_helper.station_id
       where mr.exam_type = p_exam_type 
         and sch.period_id = p_project_id
         and (p_exam_date is null or (p_exam_date is not null and cte_station_helper.exam_date = p_exam_date))
         and (p_station_code is null or (p_station_code is not null and s.code = p_station_code))
       group by cte_station_helper.exam_date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code
    ),
    cte_max_by_vnvr_station as (
      select i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code,
             max (i.cnt) as  max_cnt
        from cte_count_by_expert_etc as i
       group by
             i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code
    )
    select vn.code, vn.name, sum(data.max_cnt) as total
      from cte_max_by_vnvr_station as data
      join gsc.violation as vn on vn.id = data.violation_id
     group by vn.code, vn.name, data.violation_id;
end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;

grant execute on function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  to s_repmr_module;
