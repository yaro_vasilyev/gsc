set search_path = gsc,public;

alter table gsc.exec
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger exec_bu before update
  on gsc.exec for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view v_exec
as
  select * from gsc.exec;

alter view gsc.v_exec
  owner to gsc;


create or replace function gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    project_id          gsc.exec.project_id%type,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type)
  returns table (id gsc.exec.id%type, ts gsc.exec.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.exec.id%type;
  l_ts        gsc.exec.ts%type;
begin
  insert into gsc.exec (
              count_part,
              count_room,
              count_room_video,
              kim,
              project_id,
              station_id,
              exam_id)
      values (count_part,
              count_room,
              count_room_video,
              kim,
              project_id,
              station_id,
              exam_id)
    returning exec.id,
              exec.ts
         into l_id,
              l_ts;
  
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    project_id          gsc.exec.project_id%type,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type)
  owner to gsc;



create or replace function gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_project_id          gsc.exec.project_id%type,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type)
  returns table (ts gsc.exec.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.exec.ts%type;
begin
  update gsc.exec
     set count_part = new_count_part,
         count_room = new_count_room,
         count_room_video = new_count_room_video,
         kim = new_kim,
         project_id = new_project_id,
         station_id = new_station_id,
         exam_id = new_exam_id
   where exec.id = p_id
     and exec.ts = old_ts
         returning exec.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Проведение экзамена');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_project_id          gsc.exec.project_id%type,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type)
  owner to gsc;


create or replace function gsc.exec_delete (p_id gsc.exec.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.exec
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Проведение экзамена');
  end if;
end;
$$;

alter function gsc.exec_delete (p_id gsc.exec.id%type)
  owner to gsc;