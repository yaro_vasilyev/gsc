set search_path = gsc,public;
--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  returns table (
          date             date,
          subjects         text,
          station_etc      text,
          violation_code   gsc.violation.code%type,
          violation_name   gsc.violation.name%type,
          violator_code    gsc.violator.code%type,
          violator_name    gsc.violator.name%type,
          serie            bigint)
  stable
  called on null input
  security definer
  language plpgsql
as $$
--
-- Parameters:
--    p_project_id    REQUIRED. Project
--    p_region_id     REQUIRED. Region
--    p_exam_type     OPTIONAL. Type of Exam
begin
  if p_project_id is null then
    return;
  end if;
  if p_region_id is null then
    return;
  end if;
  
  return query
 with cte_subjects as (
      select station.id, 
             exam.exam_date, 
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.exec
        join gsc.station on station.id = exec.station_id
        join gsc.exam on exam.id = exec.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where subject.project_id = p_project_id
         and station.project_id = p_project_id
         and (p_region_id is null or p_region_id is not null and p_region_id = station.region_id)
         and (p_exam_type is null or p_exam_type is not null and p_exam_type = exam.exam_type)
       group by 
             station.id,
             exam.exam_date
),
cte_data as (
  select x1.dt,
         x1.violation_id,
         x1.violator_id,
         x1.object_type,
         x1.exam_type,
         x1.station_id,
         x1.region_id,
         max (x1.cnt) as cnt
    from (select mr.schedule_id, 
                 coalesce (sd.exam_date, mr.date) as dt, 
                 mr.violation_id, 
                 mr.violator_id, 
                 mr.object_type, 
                 mr.exam_type,
                 sd.station_id,
                 s.region_id,
                 count(*) as cnt
            from gsc.monitor_result as mr
            join gsc.schedule as s on s.id = mr.schedule_id
            join gsc.v_expert as e on e.id = s.expert_id
            left join gsc.schedule_detail as sd on sd.id = mr.schedule_detail_id
           where e.period_id = p_project_id
             and s.region_id = p_region_id
             and (p_exam_type is null or p_exam_type is not null and mr.exam_type = p_exam_type)
           group by 
                 mr.schedule_id, 
                 coalesce (sd.exam_date, mr.date), 
                 mr.violation_id, 
                 mr.violator_id, 
                 mr.object_type, 
                 mr.exam_type,
                 sd.station_id,
                 s.region_id) as x1
   group by 
         x1.dt,
         x1.violation_id,
         x1.violator_id,
         x1.object_type,
         x1.exam_type,
         x1.station_id,
         x1.region_id
)
select dt as date,
       cte_subjects.subjects,
       case cte_data.object_type 
            when 1 then 'ППЭ ' || station.code
            when 2 then 'РЦОИ'
            when 3 then 'КК'
            when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violator_name,
           generate_series (1, cte_data.cnt) as serie
  from cte_data
  join gsc.violation on violation.id = cte_data.violation_id
  join gsc.violator on violator.id = cte_data.violator_id
  left join gsc.station on station.id = cte_data.station_id
  left join cte_subjects on cte_subjects.id = cte_data.station_id and cte_subjects.exam_date = cte_data.dt
 order by 1;
end;
$$;

alter function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  owner to gsc;
