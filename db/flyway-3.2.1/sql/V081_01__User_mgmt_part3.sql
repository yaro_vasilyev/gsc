set search_path = gsc,public;


create or replace function gsc.get_user_id(p_rolname name)
  returns gsc.users.id%type
  stable
  language plpgsql
  security definer
as $$
declare l_ret gsc.users.id%type;
begin
  select id
    into strict l_ret /* strict, Carl! */
    from gsc.users
   where users.rolname = p_rolname;
  return l_ret;
exception
  when NO_DATA_FOUND then
    raise exception 'Пользователь не найден для роли %', p_rolname;
  when TOO_MANY_ROWS then
    raise exception 'Для роли % найдено несколько пользователей', p_rolname;
end;
$$;

alter function gsc.get_user_id(name)
  owner to gsc;




create function gsc.get_expert_by_user(p_user_id gsc.users.id%type)
  returns gsc.expert.id%type
  stable
  security definer
  language plpgsql
as $$
declare l_ret gsc.expert.id%type;
begin
  select expert.id
    into /*no strict*/ l_ret
    from gsc.expert
   where expert.user_id = p_user_id
     and expert.period_id = gsc.current_project_id();
  return l_ret;
end;
$$;

alter function gsc.get_expert_by_user(gsc.users.id%type)
  owner to gsc;




create or replace function gsc.get_observer_by_user(p_user_id gsc.users.id%type)
  returns gsc.observer.id%type
  stable
  security definer
  language plpgsql
as $$
declare
  l_ret gsc.observer.id%type;
begin
  select observer.id
    into /*no strict*/ l_ret
    from gsc.observer
   where observer.user_id = p_user_id;
  return l_ret;
end;
$$;

alter function gsc.get_observer_by_user(gsc.users.id%type)
  owner to gsc;




create or replace function gsc.get_observer_type(p_user_id gsc.users.id%type)
  returns observer.type%type
  stable
  security definer
  language plpgsql
as $$
declare
  l_ret gsc.observer.type%type;
begin
  select type
    into l_ret
    from gsc.observer
   where observer.user_id = p_user_id;
  return l_ret;
end;
$$;

alter function gsc.get_observer_type(gsc.users.id%type)
  owner to gsc;




create or replace function gsc.get_experts(p_user_id gsc.users.id%type)
  returns setof gsc.expert
  stable
  security definer
  language plpgsql
as $$
declare
  l_expert_id gsc.expert.id%type = gsc.get_expert_by_user(p_user_id);
  l_observer_id gsc.observer.id%type = gsc.get_observer_by_user(p_user_id);
  l_observer_type gsc.observer.type%type;
begin
  if l_expert_id is not null then
    return query select *
                   from gsc.expert
                  where expert.id = l_expert_id;
  end if;
  
  if l_observer_id is not null then
    select observer.type
      into strict l_observer_type
      from gsc.observer
     where observer.id = l_observer_id;
  
    return query select *
                   from gsc.expert
                  where ((expert.kind = 2 and l_observer_type = 2) or l_observer_type = 1)
                    and expert.period_id = gsc.current_project_id();
  end if;
end;
$$;


alter function gsc.get_experts(gsc.users.id%type)
  owner to gsc;



create or replace view gsc.v_expert
as
  select * from gsc.get_experts(gsc.get_user_id(current_user));
  
alter view gsc.v_expert
  owner to gsc;
  

create or replace view gsc.v_schedule
as
  select s.*
    from gsc.schedule as s
    join v_expert as e on e.id = s.expert_id;

alter view gsc.v_schedule
  owner to gsc;



create or replace function gsc.get_observers(p_user_id gsc.users.id%type)
  returns setof gsc.observer
  stable
  security definer
  language plpgsql
as $$
declare
  l_expert_id gsc.expert.id%type = gsc.get_expert_by_user(p_user_id);
  l_observer_id gsc.observer.id%type = gsc.get_observer_by_user(p_user_id);
begin
  if l_expert_id is not null then
    return query select * 
             from gsc.observer
                  where observer.id = (select expert.observer_id 
                               from gsc.expert 
                      where expert.id = l_expert_id);
  elsif l_observer_id is not null then
    return query select *
                   from gsc.observer
            where ((gsc.get_observer_type(p_user_id) = 2 and observer.type = 2) or gsc.get_observer_type(p_user_id) = 1);
  end if;
end;
$$;

alter function gsc.get_observers(gsc.users.id%type)
  owner to gsc;



create or replace view gsc.v_observer
as
  select *
    from gsc.get_observers(gsc.get_user_id(current_user));
    
alter view gsc.v_observer
  owner to gsc;




create or replace view gsc.v_schedule_detail
as
  select sd.* 
    from gsc.schedule_detail as sd
    join gsc.v_schedule as s on s.id = sd.schedule_id;


alter view gsc.v_schedule_detail
  owner to gsc;

