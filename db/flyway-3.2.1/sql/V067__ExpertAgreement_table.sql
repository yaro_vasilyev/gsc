set search_path = gsc,public;

create table gsc.expert_agreement (
  expert_id integer not null unique references gsc.expert(id),
  blob bytea,
  ts timestamp,
  filename varchar(250)
);

alter table gsc.expert_agreement
  owner to gsc;
