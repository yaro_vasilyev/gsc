set search_path = gsc,public;
  
create or replace function gsc.expert_kind_official(p_expert gsc.expert)
  returns boolean
  stable
  language plpgsql
as $$
begin
  if p_expert.kind = gsc.expert_kind_official() then
    return true;
  end if;
  return false;
end;
$$;

alter function gsc.expert_kind_official(gsc.expert)
  owner to gsc;
  

create or replace function gsc.expert_kind_social(p_expert gsc.expert)
  returns boolean
  stable
  language plpgsql
as $$
begin
  if p_expert.kind = gsc.expert_kind_social() then
    return true;
  end if;
  return false;
end;
$$;

alter function gsc.expert_kind_social(p_expert gsc.expert)
  owner to gsc;



create or replace function gsc.observer_type_manager(p_observer gsc.observer)
  returns boolean
  volatile
  language plpgsql
as $$
begin
  if p_observer.type = gsc.observer_type_manager() then
    return true;
  end if;
  return false;
end;
$$;

alter function gsc.observer_type_manager(gsc.observer)
  owner to gsc;


create or replace function gsc.observer_type_rsm(p_observer gsc.observer)
  returns boolean
  volatile
  language plpgsql
as $$
begin
  if p_observer.type = gsc.observer_type_rsm() then
    return true;
  end if;
  return false;
end;
$$;

alter function gsc.observer_type_rsm(gsc.observer)
  owner to gsc;