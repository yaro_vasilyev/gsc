﻿
/*
dependencies:
gsc.v_contract
gsc.v_event
gsc.v_monitor_result
gsc.v_mr_detail
gsc.v_mr_doc
gsc.v_schedule_detail

schedule_insert
schedule_update
monitor_result_subjects
r_station_violations
r_violations_wo_dup??
r_gsc094_monitor_results
r_gsc095_monitor_results_detailed
r_gsc096_monitor_results_short
r_gsc101_violations_in_region
r_gsc102_violations
*/
drop view gsc.v_schedule cascade;

-----------------------------------------------------------------------------------------------
-- Update dependent on column period_id view
-----------------------------------------------------------------------------------------------
create or replace view gsc.wa_v_exp_schedule (
    guid,
    region_name,
    id,
    period_id,
    monitor_rcoi,
    fixed,
    period_type,
    expert_id)
as
select u.guid,
    r.name as region_name,
    sch.id,
    e.period_id,
    sch.monitor_rcoi,
    sch.fixed,
    sch.period_type,
    sch.expert_id
from gsc.schedule sch
     join gsc.expert e on e.id = sch.expert_id
     join gsc.region r on r.id = sch.region_id
     left join gsc.users u on gsc._xxx_roles_all(u.rolname, variadic
         array[gsc.role_expert()::name]) and u.guid = e.user_guid;

alter view gsc.wa_v_exp_schedule owner to gsc;

-----------------------------------------------------------------------------------------------
-- Drop column
-----------------------------------------------------------------------------------------------

alter table gsc.schedule
  drop column period_id;

-----------------------------------------------------------------------------------------------
-- Add views
-----------------------------------------------------------------------------------------------

create or replace view gsc.v_schedule
as
  select s.*
    from gsc.schedule as s
    join gsc.v_expert as e on e.id = s.expert_id;

alter view gsc.v_schedule
  owner to gsc;


create or replace view gsc.v_schedule_detail
as
  select sd.* 
    from gsc.schedule_detail as sd
    join gsc.v_schedule as s on s.id = sd.schedule_id;


alter view gsc.v_schedule_detail
  owner to gsc;


create or replace view gsc.v_contract
as
  select c.* 
    from gsc.contract as c
    join gsc.v_schedule as s on s.id = c.schedule_id;

alter view gsc.v_contract
  owner to gsc;


create or replace view gsc.v_event
as
select e.id,
    e.event_ts,
    e.channel,
    e.type,
    e.schedule_id,
    e.object_type,
    e.project_id,
    e.user_id,
    e.ts,
    e.user_guid
from gsc.event e
join gsc.v_schedule s on s.id = e.schedule_id;

alter view gsc.v_event
  owner to gsc;

create or replace view gsc.v_monitor_result 
as 
  select mr.* 
    from gsc.monitor_result as mr 
    join gsc.v_schedule as s on s.id = mr.schedule_id;

alter view gsc.v_monitor_result owner to gsc;


create or replace view gsc.v_mr_detail
as
  select d.* 
    from gsc.mr_detail as d
    join gsc.v_monitor_result as mr on mr.id = d.monitor_result_id;

alter view gsc.v_mr_detail
  owner to gsc;


create or replace view gsc.v_mr_doc
as
  select d.*
    from gsc.mr_doc as d
    join gsc.v_schedule as s on d.schedule_id = s.id;
    
alter view gsc.v_mr_doc
  owner to gsc;

-----------------------------------------------------------------------------------------------
-- CRUD functions
-----------------------------------------------------------------------------------------------

create or replace function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   integer,
    region_id     gsc.schedule.region_id%type)
  returns table (id gsc.schedule.id%type, ts gsc.schedule.ts%type)
  security definer
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.schedule.id%type;
  l_ts  gsc.schedule.ts%type;
begin
  insert into gsc.schedule (
              expert_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
      values (expert_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
    returning schedule.id, schedule.ts
         into l_id, l_ts;
   return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   integer,
    region_id     gsc.schedule.region_id%type)
  owner to gsc;



create or replace function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  returns table (ts gsc.schedule.ts%type)
  security definer
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.schedule.ts%type;
begin
  update gsc.schedule
     set expert_id = new_expert_id,
         monitor_rcoi = new_monitor_rcoi,
         fixed = new_fixed,
         period_type = new_period_type,
         region_id = new_region_id
   where id = p_id
     and ts = old_ts
         returning schedule.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('График');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  owner to gsc;


-----------------------------------------------------------------------------------------------
-- Drop the old ones
-----------------------------------------------------------------------------------------------
drop function if exists gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   integer,
    region_id     gsc.schedule.region_id%type);


drop function if exists gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type);

-----------------------------------------------------------------------------------------------
-- Other functions
-----------------------------------------------------------------------------------------------

create or replace function gsc.monitor_result_subjects(
                p_project_id              integer) 
returns table (
                monitor_result_id         integer, 
                subject_names             text, 
                subject_codes             text)
  language plpgsql 
  stable 
  security definer
  strict
as $$
begin
  return query
    select monitor_result.id as monitor_result_id,
           array_to_string (array_agg (subject.name order by subject.name), ', ') as subject_names,
           array_to_string (array_agg (subject.code order by subject.name), ', ') as subject_codes
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.expert on expert.id = schedule.expert_id
      left join gsc.mr_detail on mr_detail.monitor_result_id = monitor_result.id
      left join gsc.exec on exec.id = mr_detail.exec_id and exec.project_id = p_project_id
      left join gsc.exam on exam.id = exec.exam_id
      left join gsc.subject on subject.id = exam.subject_id and subject.project_id = p_project_id
     where expert.period_id = p_project_id
     group by monitor_result.id;
end;
$$;


alter function gsc.monitor_result_subjects(integer) owner to gsc;


create or replace function gsc.r_station_violations (
    p_project_id integer,
    p_exam_type gsc.d_exam_type)
  returns table (
          date date, 
          violation_id integer, 
          violator_id integer, 
          station_id integer, 
          region_id integer, 
          cnt bigint) 
  called on null input
  language plpgsql 
  stable
  security definer
as
$$
-- возвращает статистику по нарушениям без дубликатов. 
-- нарушения на РЦОИ/ППЗ/КК попадают в одну категорию (строки с station_id = null)
begin
  return query
    select group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id,
           max (group_by_expert.cnt) as cnt
      from (select monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id,
                   schedule_detail.station_id,
                   schedule.region_id,
                   count(*) as cnt 
              from gsc.monitor_result
              join gsc.schedule on monitor_result.schedule_id = schedule.id
              join gsc.expert on expert.id = schedule.expert_id
         left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id
             where (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type))
               and expert.period_id = p_project_id
             group by monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id, 
                   schedule_detail.station_id,
                   schedule.region_id) as group_by_expert
     group by group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id;
end;
$$;

alter function gsc.r_station_violations(integer, gsc.d_exam_type)
  owner to gsc;


create or replace function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  returns table (
    Date            gsc.monitor_result.date%type,
    RegionCode      gsc.region.code%type,
    RegionName      gsc.region.name%type,
    ExpertFullName  text,
    ExpertKind      gsc.expert.kind%type,
    StationCode     gsc.station.code%type,
    ViolationPpe    gsc.violation.name%type,
    ViolationOther  gsc.violation.name%type,
    Note            gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
begin
  return query
    select r.date as Date, 
           rg.code as RegionCode, 
           rg.name as RegionName, 
           concat_ws(' ', x.surname, x.name, x.patronymic) as ExpertFullName,
           x.kind as ExpertKind,
           s.code as StationCode,
           case when r.object_type = 1 then vn.name else null end as ViolationPpe,
           case when r.object_type <> 1 then vn.name else null end as ViolationOther,
           r.note
      from gsc.monitor_result as r
      left join (select distinct
                        d.monitor_result_id, e.station_id 
                   from gsc.mr_detail as d
                   join gsc.exec as e on e.id = d.exec_id) as station_helper on station_helper.monitor_result_id = r.id
      left join gsc.station as s on s.id = station_helper.station_id
      join gsc.schedule as sch on sch.id = r.schedule_id
      join gsc.expert as x on x.id = sch.expert_id
      join gsc.violation as vn on vn.id = r.violation_id
      join gsc.region as rg on rg.id = sch.region_id
     where x.period_id = p_project_id and 
           (p_period_type is null or p_period_type is not null and sch.period_type = p_period_type) and
           (p_exam_type is null or p_exam_type is not null and r.exam_type = p_exam_type) and
           (p_date is null or p_date is not null and r.date = p_date)
     order by r.date, rg.name, ExpertFullName, s.code, ViolationPpe;
end;
$$;

alter function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  owner to gsc;

grant execute on function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  to s_repmr_module;


create or replace function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  returns table (
    exam_date           date,
    region_code         gsc.region.code%type,
    region_name         gsc.region.name%type,
    region_zone         gsc.region.zone%type,
    subjects            text,
    expert_fio          text,
    expert_kind         gsc.expert.kind%type,
    ppe                 text,
    violation_code      gsc.violation.code%type,
    violation_name      gsc.violation.name%type,
    violator_code       gsc.violator.code%type,
    violator_name       gsc.violator.name%type,
    note                gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--    Function for GSC-095 report
-- PARAMETERS:
--    p_project_id        REQUIRED. Project ID
--    p_period_type       OPTIONAL. Exam's period type
--    p_exam_type         OPTIONAL. Exam's exam type
--    p_date              OPTIONAL. Filter date
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject as subj on subj.id = exam.subject_id
       where e.project_id = p_project_id
         and subj.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and exam.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and exam.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and exam.exam_date = p_date))
         union all -- другие результаты мониторинга (не только c запольненными ППЭ)
      select mr1.id,
             null,
             mr1.date
        from gsc.monitor_result mr1
        join gsc.schedule as sch1 on sch1.id = mr1.schedule_id -- ограничить по проекту для
        join gsc.expert on expert.id = sch1.expert_id
       where not exists (select * from gsc.mr_detail mrd1 where mrd1.monitor_result_id = mr1.id)
         and expert.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and sch1.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and mr1.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and mr1.date = p_date))
    ),
    cte_subjects as (
      select mr.monitor_result_id, array_to_string (array_agg (j.name order by j.name), ', ') as subjects
        from gsc.mr_detail as mrd
        join cte_station_helper as mr on mr.monitor_result_id = mrd.monitor_result_id
        join gsc.exec as x on x.id = mrd.exec_id
        join gsc.exam as e on e.id = x.exam_id
        join gsc.subject as j on j.id = e.subject_id
       where x.project_id = p_project_id
         and j.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and e.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and e.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and e.exam_date = p_date))
       group by mr.monitor_result_id
    )
    select cte_station_helper.exam_date,
           r.code as region_code,
           r.name as region_name,
           r.zone as region_zone,
           cte_subjects.subjects,
           concat_ws (' ', ex.surname, ex.name, ex.patronymic) as expert_fio,
           ex.kind as expert_kind,
           case mr.object_type
              when 1 then coalesce (st.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
              else '-'
           end as ppe,
           vn.code as violation_code,
           vn.name as violation_name,
           vr.code as violator_code,
           vr.name as violator_name,
           mr.note
      from cte_station_helper
      left join cte_subjects on cte_subjects.monitor_result_id = cte_station_helper.monitor_result_id
      left join gsc.station as st on st.id = cte_station_helper.station_id
      join gsc.monitor_result as mr on mr.id = cte_station_helper.monitor_result_id
      join gsc.schedule as sch on sch.id = mr.schedule_id
      join gsc.region as r on r.id = sch.region_id
      join gsc.expert as ex on ex.id = sch.expert_id
      left join gsc.violation as vn on vn.id = mr.violation_id
      left join gsc.violator as vr on vr.id = mr.violator_id
     where ex.period_id = p_project_id
       and (p_period_type is null or (p_period_type is not null and sch.period_type = p_period_type))
       and (p_exam_type is null or (p_exam_type is not null and mr.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  owner to gsc;
  
grant execute on function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  to s_mrep_module;


create or replace function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  returns table (     region_name        gsc.region.name%type,
                      date               gsc.monitor_result.date%type,
                      subjects           text,
                      station_etc        text,
                      violation_code     gsc.violation.code%type,
                      violator_code      gsc.violator.code%type,
                      note               gsc.monitor_result.note%type)
  stable
  called on null input
  security definer
  language plpgsql
as $$
-- 
-- DESCRIPTION:
--    Function for report GSC-96
-- PARAMETERS
--    p_project_id        REQUIRED. Current project
--    p_exam_type         OPTIONAL. EGE/GVE/OGE
--    p_date              OPTIONAL. Date
--
begin
  return query
    with cte_mr_station as (
      select distinct
             exec.station_id,
             mr_detail.monitor_result_id
        from gsc.mr_detail
        join gsc.exec on mr_detail.exec_id = exec.id
    ),
    cte_mr_subjects as (
      select monitor_result.id as monitor_result_id,
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        join gsc.expert on expert.id = schedule.expert_id
        left join gsc.mr_detail on monitor_result.id = mr_detail.monitor_result_id
        left join gsc.exec on mr_detail.exec_id = exec.id
        left join gsc.exam on exec.exam_id = exam.id
        left join gsc.subject on exam.subject_id = subject.id
       where monitor_result.object_type = 1
         and expert.period_id = p_project_id
         and subject.project_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       group by monitor_result.id
             union all
      select monitor_result.id as monitor_result_id, 
             null as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        join gsc.expert on expert.id = schedule.expert_id
       where monitor_result.object_type <> 1
         and expert.period_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
    )
    select region.name as region_name,
           monitor_result.date,
           cte_mr_subjects.subjects,
           case monitor_result.object_type
              when 1 then coalesce (station.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violator.code as violator_code,
           monitor_result.note
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.expert on expert.id = schedule.expert_id
      join gsc.region on region.id = schedule.region_id
      left join cte_mr_subjects on cte_mr_subjects.monitor_result_id = monitor_result.id
      left join cte_mr_station on cte_mr_station.monitor_result_id = monitor_result.id
      left join gsc.station on station.id = cte_mr_station.station_id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
     where expert.period_id = p_project_id
       and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       and (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  owner to gsc;
  
grant execute on function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  to s_repmr_module;



create or replace function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  returns table (
          date             date,
          subjects         text,
          station_etc      text,
          violation_code   gsc.violation.code%type,
          violation_name   gsc.violation.name%type,
          violator_code    gsc.violator.code%type,
          violator_name    gsc.violator.name%type,
          serie            bigint)
  stable
  called on null input
  security definer
  language plpgsql
as $$
begin
  if p_project_id is null then
    return;
  end if;
  if p_region_id is null then
    return;
  end if;
  
  return query
    select i3.date,
           i3.subject_names as subjects,
           case i3.vot
             when 1 then 'ППЭ ' || station.code
             when 2 then 'РЦОИ'
             when 3 then 'КК'
             when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violation_name,
           generate_series (1, i3.cnt) as serie
      from (  select i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id,
                     max(cnt) as cnt
                from (select i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id,
                             count(*) as cnt
                        from (select monitor_result.*,
                                     subjects.subject_names,
                                     case 
                                      when subjects.subject_names is null then 1
                                      else monitor_result.object_type
                                     end as vot,
                                     schedule_detail.station_id               
                                from gsc.monitor_result
                                join gsc.schedule on schedule.id = monitor_result.schedule_id
                                join gsc.expert on expert.id = schedule.expert_id
                                join gsc.region on region.id = schedule.region_id
                                left join gsc.monitor_result_subjects (p_project_id) 
                                       as subjects (monitor_result_id, subject_names, subject_codes) 
                                       on subjects.monitor_result_id = monitor_result.id
                                left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id 
                                      and schedule_detail.exam_date = monitor_result.date
                               where (p_exam_type is null or p_exam_type is not null and monitor_result.exam_type = p_exam_type)
                                 and region.id = p_region_id
                                 and expert.period_id = p_project_id
                                 ) as i1
                       group by 
                             i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id) as i2
               group by 
                     i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id) as i3
       join gsc.violation on violation.id = i3.violation_id
       join gsc.violator on violator.id = i3.violator_id
       left join gsc.station on station.id = i3.station_id
      order by 
            i3.date,
            station_etc,
            i3.subject_names,
            violation_code,
            violator_code;
end;
$$;

alter function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  owner to gsc;

 grant execute on function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  to s_repmr_module;


create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where e.project_id = p_project_id
         and subject.project_id = p_project_id
    ),
    cte_count_by_expert_etc as (
      select cte_station_helper.exam_date as date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code as station_code, 
             count(*) as cnt
        from gsc.monitor_result as mr
        join gsc.schedule as sch on sch.id = mr.schedule_id
        join gsc.expert on expert.id = sch.expert_id
        left join cte_station_helper on mr.id = cte_station_helper.monitor_result_id
        left join gsc.station as s on s.id = cte_station_helper.station_id
       where mr.exam_type = p_exam_type 
         and expert.period_id = p_project_id
         and (p_exam_date is null or (p_exam_date is not null and cte_station_helper.exam_date = p_exam_date))
         and (p_station_code is null or (p_station_code is not null and s.code = p_station_code))
       group by cte_station_helper.exam_date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code
    ),
    cte_max_by_vnvr_station as (
      select i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code,
             max (i.cnt) as  max_cnt
        from cte_count_by_expert_etc as i
       group by
             i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code
    )
    select vn.code, vn.name, sum(data.max_cnt) as total
      from cte_max_by_vnvr_station as data
      join gsc.violation as vn on vn.id = data.violation_id
     group by vn.code, vn.name, data.violation_id;
end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;

grant execute on function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  to s_repmr_module;


-----------------------------------------------------------------------------------------------
-- End
-----------------------------------------------------------------------------------------------

