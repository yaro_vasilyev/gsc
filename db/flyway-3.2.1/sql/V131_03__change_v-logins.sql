CREATE OR REPLACE VIEW gsc.v_logins (
    id,
    fio,
    user_login,
    password,
    role)
AS
SELECT u.id,
        CASE
            WHEN e.id IS NOT NULL THEN 'Эксперт '::text || gsc.expert_fio(e.*)::text
            WHEN o.id IS NOT NULL THEN 'Менеджер '::text || o.fio::text
            ELSE '[Пользователь не привязан]'::text
        END AS fio,
    u.user_login,
    NULL::character varying AS password,
    u.rolname::text AS role
FROM gsc.v_users u
     LEFT JOIN gsc.v_observer o ON u.guid = o.user_guid
     LEFT JOIN gsc.v_expert e ON u.guid = e.user_guid;