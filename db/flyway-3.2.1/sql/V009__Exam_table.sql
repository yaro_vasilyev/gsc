SET search_path = gsc, public;

--
-- Name: exam; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE exam (
    id integer NOT NULL,
    period_type integer DEFAULT 2 NOT NULL,
    exam_date date NOT NULL,
    subject_id integer NOT NULL,
    exam_type integer DEFAULT 1 NOT NULL,
    period_id integer NOT NULL,
    CONSTRAINT exam_exam_type_check CHECK ((exam_type = ANY (ARRAY[1, 2, 3]))),
    CONSTRAINT exam_period_type_check CHECK ((period_type = ANY (ARRAY[1, 2, 3])))
);

--
-- Name: exam_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_pkey PRIMARY KEY (id);
--
-- Name: exam_period_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_period_id_fkey FOREIGN KEY (period_id) REFERENCES period(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: exam_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY exam
    ADD CONSTRAINT exam_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);



ALTER TABLE exam OWNER TO gsc;

--
-- Name: exam_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE exam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE exam_id_seq OWNER TO gsc;

--
-- Name: exam_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE exam_id_seq OWNED BY exam.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY exam ALTER COLUMN id SET DEFAULT nextval('exam_id_seq'::regclass);

