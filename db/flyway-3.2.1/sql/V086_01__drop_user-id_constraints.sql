set search_path = gsc,public;

alter table gsc.users
  drop column delegate_auth_to_db;


alter table gsc.users
  alter column type_user
    drop not null;


alter table gsc.event
  drop constraint event_user_id_fkey;

alter table gsc.expert
  drop constraint expert_user_id_fk;

alter table gsc.observer
  drop constraint observer_user_id_fk;


alter table gsc.users
  add constraint users_guid_unq unique (guid);

alter table gsc.expert
  drop constraint expert_user_id_unq;

alter table gsc.observer
  drop constraint observer_user_id_unq;