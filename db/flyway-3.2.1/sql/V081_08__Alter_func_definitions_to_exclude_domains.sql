set search_path = gsc,public;

drop function if exists gsc.monitor_result_insert (
    schedule_id            gsc.monitor_result.schedule_id%type,
    date                   gsc.monitor_result.date%type,
    violation_id           gsc.monitor_result.violation_id%type,
    violator_id            gsc.monitor_result.violator_id%type,
    note                   gsc.monitor_result.note%type,
    object_type            gsc.monitor_result.object_type%type,
    schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    exam_type              gsc.monitor_result.exam_type%type,
    event_id               gsc.monitor_result.event_id%type);



create or replace function gsc.monitor_result_insert (
    schedule_id            gsc.monitor_result.schedule_id%type,
    date                   gsc.monitor_result.date%type,
    violation_id           gsc.monitor_result.violation_id%type,
    violator_id            gsc.monitor_result.violator_id%type,
    note                   gsc.monitor_result.note%type,
    object_type            integer,
    schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    exam_type              integer,
    event_id               gsc.monitor_result.event_id%type)
  returns table (id gsc.monitor_result.id%type, ts gsc.monitor_result.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id gsc.monitor_result.id%type;
  l_ts gsc.monitor_result.ts%type;
begin
  insert into gsc.monitor_result (
              -- id,
              schedule_id,
              date,
              violation_id,
              violator_id,
              note,
              object_type,
              schedule_detail_id,
              exam_type,
              event_id)
      values (--id,
              schedule_id,
              date,
              violation_id,
              violator_id,
              note,
              object_type,
              schedule_detail_id,
              exam_type,
              event_id)
    returning monitor_result.id, monitor_result.ts into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.monitor_result_insert (
    schedule_id            gsc.monitor_result.schedule_id%type,
    date                   gsc.monitor_result.date%type,
    violation_id           gsc.monitor_result.violation_id%type,
    violator_id            gsc.monitor_result.violator_id%type,
    note                   gsc.monitor_result.note%type,
    object_type            integer,
    schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    exam_type              integer,
    event_id               gsc.monitor_result.event_id%type)
  owner to gsc;



drop function if exists gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            gsc.monitor_result.object_type%type,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              gsc.monitor_result.exam_type%type,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type);
  

create or replace function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            integer,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              integer,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  returns table (ts gsc.monitor_result.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts gsc.monitor_result.ts%type;
begin
  update gsc.monitor_result
     set schedule_id = new_schedule_id,
         date = new_date,
         violation_id = new_violation_id,
         violator_id = new_violator_id,
         note = new_note,
         object_type = new_object_type,
         schedule_detail_id = new_schedule_detail_id,
         exam_type = new_exam_type,
         event_id = new_event_id
   where id = p_id
     and ts = old_ts
         returning monitor_result.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Результаты мониторинга');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            integer,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              integer,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  owner to gsc;







drop function if exists gsc.observer_insert (
    type          gsc.observer.type%type,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type);




create or replace function gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type)
  returns table (id gsc.observer.id%type, ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.observer.id%type;
  l_ts  gsc.observer.ts%type;
begin
  insert into gsc.observer (
              type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_id)
      values (type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_id)
    returning observer.id, observer.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type)
  owner to gsc;



drop function if exists gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      gsc.observer.type%type,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type);


create or replace function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type)
  returns table (ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.observer.ts%type;
begin
  update gsc.observer
     set type = new_type,
         fio = new_fio,
         wp = new_wp,
         post = new_post,
         phone1 = new_phone1,
         phone2 = new_phone2,
         email = new_email,
         user_id = new_user_id
   where id = p_id
     and ts = old_ts
         returning observer.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('Менеджер/наблюдатель');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type)
  owner to gsc;




drop function if exists gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   gsc.schedule.period_type%type,
    region_id     gsc.schedule.region_id%type);



create or replace function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   integer,
    region_id     gsc.schedule.region_id%type)
  returns table (id gsc.schedule.id%type, ts gsc.schedule.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.schedule.id%type;
  l_ts  gsc.schedule.ts%type;
begin
  insert into gsc.schedule (
              expert_id,
              period_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
      values (expert_id,
              period_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
    returning schedule.id, schedule.ts
         into l_id, l_ts;
   return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   integer,
    region_id     gsc.schedule.region_id%type)
  owner to gsc;


drop function if exists gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   gsc.schedule.period_type%type,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type);


create or replace function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  returns table (ts gsc.schedule.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.schedule.ts%type;
begin
  update gsc.schedule
     set expert_id = new_expert_id,
         period_id = new_period_id,
         monitor_rcoi = new_monitor_rcoi,
         fixed = new_fixed,
         period_type = new_period_type,
         region_id = new_region_id
   where id = p_id
     and ts = old_ts
         returning schedule.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('График');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  owner to gsc;





drop function if exists gsc.contract_insert (
    period_type   gsc.contract.period_type%type,
    schedule_id   gsc.contract.schedule_id%type,
    contract_type gsc.contract.contract_type%type,
    number        gsc.contract.number%type,
    date_from     gsc.contract.date_from%type,
    date_to       gsc.contract.date_to%type,
    summ          gsc.contract.summ%type,
    route         gsc.contract.route%type,
    doc           gsc.contract.doc%type);



create or replace function gsc.contract_insert (
    period_type   integer,
    schedule_id   gsc.contract.schedule_id%type,
    contract_type gsc.contract.contract_type%type,
    number        gsc.contract.number%type,
    date_from     gsc.contract.date_from%type,
    date_to       gsc.contract.date_to%type,
    summ          gsc.contract.summ%type,
    route         gsc.contract.route%type,
    doc           gsc.contract.doc%type)
  returns table (ts gsc.contract.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.contract.ts%type;
begin
  insert into gsc.contract (
              period_type,
              schedule_id,
              contract_type,
              number,
              date_from,
              date_to,
              summ,
              route,
              doc)
      values (period_type,
              schedule_id,
              contract_type,
              number,
              date_from,
              date_to,
              summ,
              route,
              doc)
    returning contract.ts
         into l_ts;
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_insert (
    period_type   integer,
    schedule_id   gsc.contract.schedule_id%type,
    contract_type gsc.contract.contract_type%type,
    number        gsc.contract.number%type,
    date_from     gsc.contract.date_from%type,
    date_to       gsc.contract.date_to%type,
    summ          gsc.contract.summ%type,
    route         gsc.contract.route%type,
    doc           gsc.contract.doc%type)
  owner to gsc;


drop function if exists gsc.contract_update (
    old_number             gsc.contract.number%type,
    new_period_type        gsc.contract.period_type%type,
    new_schedule_id        gsc.contract.schedule_id%type,
    new_contract_type      gsc.contract.contract_type%type,
    new_number             gsc.contract.number%type,
    new_date_from          gsc.contract.date_from%type,
    new_date_to            gsc.contract.date_to%type,
    new_summ               gsc.contract.summ%type,
    new_route              gsc.contract.route%type,
    new_doc                gsc.contract.doc%type,
    old_ts                 gsc.contract.ts%type);


create or replace function gsc.contract_update (
    old_number             gsc.contract.number%type,
    new_period_type        integer,
    new_schedule_id        gsc.contract.schedule_id%type,
    new_contract_type      gsc.contract.contract_type%type,
    new_number             gsc.contract.number%type,
    new_date_from          gsc.contract.date_from%type,
    new_date_to            gsc.contract.date_to%type,
    new_summ               gsc.contract.summ%type,
    new_route              gsc.contract.route%type,
    new_doc                gsc.contract.doc%type,
    old_ts                 gsc.contract.ts%type)
  returns table (ts   gsc.contract.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.contract.ts%type;
begin
  update gsc.contract
     set period_type = new_period_type,
         schedule_id = new_schedule_id,
         contract_type = new_contract_type,
         number = new_number,
         date_from = new_date_from,
         date_to = new_date_to,
         summ = new_summ,
         route = new_route,
         doc = new_doc
   where number = old_number
     and contract.ts = old_ts
         returning contract.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Договор');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.contract_update (
    old_number             gsc.contract.number%type,
    new_period_type        integer,
    new_schedule_id        gsc.contract.schedule_id%type,
    new_contract_type      gsc.contract.contract_type%type,
    new_number             gsc.contract.number%type,
    new_date_from          gsc.contract.date_from%type,
    new_date_to            gsc.contract.date_to%type,
    new_summ               gsc.contract.summ%type,
    new_route              gsc.contract.route%type,
    new_doc                gsc.contract.doc%type,
    old_ts                 gsc.contract.ts%type)
  owner to gsc;



drop function if exists gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       gsc.event.channel%type,
    type          gsc.event.type%type,
    schedule_id   gsc.event.schedule_id%type,
    object_type   gsc.event.object_type%type,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type);



create or replace function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type)
  returns table (id gsc.event.id%type, ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.event.id%type;
  l_ts        gsc.event.ts%type;
begin
  insert into gsc.event (
              event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_id)
      values (event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_id)
    returning event.id, event.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type)
  owner to gsc;


drop function if exists gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       gsc.event.channel%type,
    new_type          gsc.event.type%type,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   gsc.event.object_type%type,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type);


create or replace function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type)
  returns table (ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts        gsc.event.ts%type;
begin
  update gsc.event
     set event_ts = new_event_ts,
         channel = new_channel,
         type = new_type,
         schedule_id = new_schedule_id,
         object_type = new_object_type,
         project_id = new_project_id,
         user_id = new_user_id
   where event.id = p_id
     and event.ts = old_ts
         returning event.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Событие горячей линии');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type)
  owner to gsc;


