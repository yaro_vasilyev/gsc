SET search_path = gsc, public;

--
-- Name: f_get_schedule_header(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION f_get_schedule_header(p_project_id integer) RETURNS TABLE(master_id integer, detail_id integer, exam_date date)
    LANGUAGE plpgsql STRICT
    AS $$
declare
  schedule_row record;
begin
  create temp table if not exists tmp_schedule_rows (
    schedule_id integer not null,
    schedule_detail_id integer,
    exam_date date
  ) on commit drop;

  delete from tmp_schedule_rows;

  for schedule_row in
    select d.id as detail_id,
           d.schedule_id as master_id,
           m.expert_id,
           p.id as ppe_id,
           p.region_id,
           p.exam_date
      from gsc.schedule_detail d
      join gsc.schedule m on d.schedule_id = m.id
      left join gsc.ppe p on p.id = d.ppe_id
     where m.period_id = p_project_id
  loop

    if exists(select *
               from tmp_schedule_rows t
              where t.schedule_id = schedule_row.master_id
                and t.exam_date = schedule_row.exam_date) then
      raise notice 'Found row with %, %. No checks for detail_id. Inserted duplicate.', schedule_row.master_id, schedule_row.exam_date;
      insert into tmp_schedule_rows (schedule_id, schedule_detail_id, exam_date)
           values (schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date);
    elsif exists(select *
                 from tmp_schedule_rows t
                 where t.schedule_id = schedule_row.master_id) then
      raise notice 'Found one detail for master %. Skip it.', schedule_row.master_id;
    else
      insert into tmp_schedule_rows (schedule_id, schedule_detail_id, exam_date)
           values (schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date);
      raise notice 'Inserted new row %, %, %', schedule_row.master_id, schedule_row.detail_id, schedule_row.exam_date;
    end if;

  end loop;

  return query select t.schedule_id, t.schedule_detail_id, t.exam_date from tmp_schedule_rows t;

end;
$$;


ALTER FUNCTION gsc.f_get_schedule_header(p_project_id integer) OWNER TO gsc;



--
-- Name: schedule_calendar_details(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION schedule_calendar_details(integer) RETURNS TABLE(id integer, schedule_id integer, exam_date date, address text)
    LANGUAGE sql IMMUTABLE STRICT
    AS $$
  select schedule_detail.id, schedule_id, ppe.exam_date, ppe.address
    from gsc.schedule_detail
    join gsc.schedule on schedule.id = schedule_id
    left join gsc.ppe on ppe.id = ppe_id;
$$;


ALTER FUNCTION gsc.schedule_calendar_details(integer) OWNER TO gsc;

