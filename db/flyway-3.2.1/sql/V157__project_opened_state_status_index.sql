set search_path = gsc,public;

drop index if exists project_open_status;

create unique index project_open_status 
    on gsc.period (status) 
 where status = 1;