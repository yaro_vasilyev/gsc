set search_path = gsc,public;

-------------------------------------------------------------------------------------------------------
-- Add project_id column
-------------------------------------------------------------------------------------------------------

alter table gsc.rcoi
  add column project_id integer,
  add constraint rcoi_project_fkey foreign key (project_id) references gsc.period (id);

update gsc.rcoi
   set project_id = (select id 
                       from gsc.period
                      where status = 1);

alter table gsc.rcoi
  alter column project_id set not null;


-------------------------------------------------------------------------------------------------------
-- Update view
-------------------------------------------------------------------------------------------------------


create or replace view gsc.v_rcoi
as
  select * from gsc.rcoi;

alter view gsc.v_rcoi
  owner to gsc;



-------------------------------------------------------------------------------------------------------
-- CRUD functions
-------------------------------------------------------------------------------------------------------

drop function gsc.rcoi_insert (
    region_id           gsc.rcoi.region_id%type,
    name                gsc.rcoi.name%type,
    address             gsc.rcoi.address%type,
    chief_post          gsc.rcoi.chief_post%type,
    chief_fio           gsc.rcoi.chief_fio%type,
    chief_phone1        gsc.rcoi.chief_phone1%type,
    chief_phone2        gsc.rcoi.chief_phone2%type,
    email               gsc.rcoi.email%type);

create or replace function gsc.rcoi_insert (
    region_id           gsc.rcoi.region_id%type,
    p_project_id        gsc.period.id%type,
    name                gsc.rcoi.name%type,
    address             gsc.rcoi.address%type,
    chief_post          gsc.rcoi.chief_post%type,
    chief_fio           gsc.rcoi.chief_fio%type,
    chief_phone1        gsc.rcoi.chief_phone1%type,
    chief_phone2        gsc.rcoi.chief_phone2%type,
    email               gsc.rcoi.email%type)
  returns table(id gsc.rcoi.id%type, ts gsc.rcoi.ts%type)
  security definer
  volatile
  language plpgsql
as $$
declare
  l_id    gsc.rcoi.id%type;
  l_ts    gsc.rcoi.ts%type;
begin
  insert into gsc.rcoi (
              region_id,
              project_id,
              name,
              address,
              chief_post,
              chief_fio,
              chief_phone1,
              chief_phone2,
              email)
      values (region_id,
              p_project_id,
              name,
              address,
              chief_post,
              chief_fio,
              chief_phone1,
              chief_phone2,
              email)
    returning rcoi.id,
              rcoi.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.rcoi_insert (
    region_id           gsc.rcoi.region_id%type,
    p_project_id        gsc.period.id%type,
    name                gsc.rcoi.name%type,
    address             gsc.rcoi.address%type,
    chief_post          gsc.rcoi.chief_post%type,
    chief_fio           gsc.rcoi.chief_fio%type,
    chief_phone1        gsc.rcoi.chief_phone1%type,
    chief_phone2        gsc.rcoi.chief_phone2%type,
    email               gsc.rcoi.email%type)
  owner to gsc;

-------------------------------------------------------------------------------------------------------

drop function gsc.rcoi_update (
    p_id                    gsc.rcoi.id%type,
    new_region_id           gsc.rcoi.region_id%type,
    new_name                gsc.rcoi.name%type,
    new_address             gsc.rcoi.address%type,
    new_chief_post          gsc.rcoi.chief_post%type,
    new_chief_fio           gsc.rcoi.chief_fio%type,
    new_chief_phone1        gsc.rcoi.chief_phone1%type,
    new_chief_phone2        gsc.rcoi.chief_phone2%type,
    new_email               gsc.rcoi.email%type,
    old_ts                  gsc.rcoi.ts%type);

create or replace function gsc.rcoi_update (
    p_id                    gsc.rcoi.id%type,
    new_region_id           gsc.rcoi.region_id%type,
    p_new_project_id        gsc.period.id%type,
    new_name                gsc.rcoi.name%type,
    new_address             gsc.rcoi.address%type,
    new_chief_post          gsc.rcoi.chief_post%type,
    new_chief_fio           gsc.rcoi.chief_fio%type,
    new_chief_phone1        gsc.rcoi.chief_phone1%type,
    new_chief_phone2        gsc.rcoi.chief_phone2%type,
    new_email               gsc.rcoi.email%type,
    old_ts                  gsc.rcoi.ts%type)
  returns table (ts gsc.rcoi.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.rcoi.ts%type;
begin
  update gsc.rcoi
     set region_id = new_region_id,
         name = new_name,
         address = new_address,
         chief_post = new_chief_post,
         chief_fio = new_chief_fio,
         chief_phone1 = new_chief_phone1,
         chief_phone2 = new_chief_phone2,
         email = new_email,
         project_id = p_new_project_id
   where id = p_id
     and rcoi.ts = old_ts
         returning rcoi.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('РЦОИ');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.rcoi_update (
    p_id                    gsc.rcoi.id%type,
    new_region_id           gsc.rcoi.region_id%type,
    p_new_project_id          gsc.period.id%type,
    new_name                gsc.rcoi.name%type,
    new_address             gsc.rcoi.address%type,
    new_chief_post          gsc.rcoi.chief_post%type,
    new_chief_fio           gsc.rcoi.chief_fio%type,
    new_chief_phone1        gsc.rcoi.chief_phone1%type,
    new_chief_phone2        gsc.rcoi.chief_phone2%type,
    new_email               gsc.rcoi.email%type,
    old_ts                  gsc.rcoi.ts%type)
  owner to gsc;
  


-------------------------------------------------------------------------------------------------------
-- Add column for project to import tables
-------------------------------------------------------------------------------------------------------

delete from gsc.imp_rcoi;

alter table gsc.imp_rcoi
  add column project_id integer;
alter table gsc.imp_rcoi
  alter column project_id set not null;

-------------------------------------------------------------------------------------------------------
-- Import functions
-------------------------------------------------------------------------------------------------------

-- import row function
create or replace function gsc.import_row_rcoi (
          p_project_id        gsc.period.id%type,
          imp_session_guid    uuid,
          imp_region          integer,
          name                varchar,
          address             varchar,
          chief_post          varchar,
          chief_fio           varchar,
          chief_phone1        varchar,
          chief_email         varchar)
  returns void 
  language plpgsql
  volatile
  called on null input
  security definer
as
$$
declare
  l_region_id integer;
  l_error_detail text;
begin
  -- get region
  select r.id
    into l_region_id
    from gsc.region r
   where r.code = imp_region;

  if l_region_id is null then
    raise exception 'Регион с кодом "%" не найден.', imp_region
      using 
        errcode   = 'foreign_key_violation', 
        hint      = 'Исправьте файл импорта или загрузите регионы и повторите импорт.';
  end if; 

  insert into gsc.imp_rcoi (project_id, imp_session_guid, imp_region, region_id, name, address, chief_post, chief_fio, chief_phone1, chief_email)
  values (p_project_id, imp_session_guid, imp_region, l_region_id, name, address, chief_post, chief_fio, chief_phone1, chief_email);
exception
    when unique_violation then
      get stacked diagnostics l_error_detail = pg_exception_detail;
      raise exception E'Код % дублируется.\nОшибка БД: %', imp_region,  l_error_detail
        using 
          errcode   = 'unique_violation', 
          hint      = 'Исправьте файл и повторите импорт.';
end;
$$;

-------------------------------------------------------------------------------------------------------

-- Bulk import function
create or replace function gsc.import_rcoi (
          p_imp_session_guid      uuid)
  returns table (
          code                    integer,
          name                    varchar) 
  language plpgsql
  volatile
  called on null input
  security definer
as
$$
declare
  l_imported integer [ ];
begin
  --// todo check import right

  -- import rows
  with imported as (
    insert into rcoi as r (region_id, name, address, chief_post, chief_fio, chief_phone1, email, project_id)
    select imp.region_id,
           imp.name,
           imp.address,
           imp.chief_post,
           imp.chief_fio,
           imp.chief_phone1,
           imp.chief_email,
           imp.project_id
      from gsc.imp_rcoi imp
     where imp.imp_session_guid = p_imp_session_guid 
       and imp.imp_status = 'added' 
        on conflict 
        on constraint rcoi_pkey do
    update
       set name = excluded.name 
 returning r.region_id)
    select array_agg(i.region_id)
      from imported i
      into l_imported;

    -- update import status

    update gsc.imp_rcoi imp
       set imp_status             = 'imported'
     where imp.imp_session_guid   = p_imp_session_guid and
           imp.region_id          = any (l_imported);

    -- return records presence in table but missing from import
    return query select 
           rg.code,
           rc.name
      from gsc.rcoi rc
      left join gsc.region rg on rc.region_id = rg.id
     where not exists (
               select null
                 from gsc.imp_rcoi imp
                where imp.imp_session_guid = p_imp_session_guid 
                  and rc.region_id = imp.region_id);
end;
$$;

-------------------------------------------------------------------------------------------------------
-- Drop old import functions
-------------------------------------------------------------------------------------------------------

drop function gsc.import_row_rcoi (
  imp_session_guid uuid,
  imp_region integer,
  name varchar,
  address varchar,
  chief_post varchar,
  chief_fio varchar,
  chief_phone1 varchar,
  chief_email varchar
);

-------------------------------------------------------------------------------------------------------
-- That's all folks!
-------------------------------------------------------------------------------------------------------
