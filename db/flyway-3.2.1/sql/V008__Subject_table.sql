SET search_path = gsc, public;

--
-- Name: subject; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE subject (
    id integer NOT NULL,
    name character varying(500) NOT NULL,
    code integer NOT NULL
);

ALTER TABLE subject OWNER TO postgres;

--
-- Name: subject_code_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--
ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_code_key UNIQUE (code);

--
-- Name: COLUMN subject.code; Type: COMMENT; Schema: gsc; Owner: postgres
--

COMMENT ON COLUMN subject.code IS 'Subject code';
--
-- Name: subject_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


ALTER TABLE subject OWNER TO gsc;

--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subject_id_seq OWNER TO gsc;

--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE subject_id_seq OWNED BY subject.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY subject ALTER COLUMN id SET DEFAULT nextval('subject_id_seq'::regclass);
