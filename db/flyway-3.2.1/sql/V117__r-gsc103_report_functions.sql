
set search_path = gsc,public;


create or replace function gsc.r_gsc103_violators (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  returns table (violator_code   gsc.violator.code%type,
                 violator_name   gsc.violator.name%type,
                 count           bigint)
  stable
  language plpgsql
  called on null input
  security definer
as $$
  -- this function - copy of r_gsc100_violations with minor modifications
begin
  return query 
  with cte as (
    select distinct
           xm.exam_type, 
           xm.exam_date, 
           xc.station_id, 
           mr.violation_id, 
           mr.violator_id
      from gsc.mr_detail as md
      join gsc.monitor_result as mr on mr.id = md.monitor_result_id
      join gsc.exec as xc on xc.id = md.exec_id
      join gsc.exam as xm on xm.id = xc.exam_id
     where (p_exam_type is null or (p_exam_type is not null and xm.exam_type = p_exam_type))
       and xc.project_id = p_project_id
  )
  select vr.code as violation_code,
         vr.name as violation_name, 
         count(*) 
    from cte
    left join gsc.violator as vr on vr.id = cte.violator_id
   group by vr.code, vr.name;
end;
$$;

alter function gsc.r_gsc103_violators (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc103_violators (
    gsc.period.id%type, gsc.exam.exam_type%type)
  to s_repmr_module;
