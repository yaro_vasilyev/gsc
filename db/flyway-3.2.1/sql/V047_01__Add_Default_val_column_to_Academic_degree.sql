ALTER TABLE gsc.academic_degree
  ADD COLUMN default_val INTEGER DEFAULT 0 NOT NULL;
	
ALTER TABLE gsc.academic_degree
  ADD CONSTRAINT chk_default_val CHECK (default_val = ANY (ARRAY[0, 5]));	