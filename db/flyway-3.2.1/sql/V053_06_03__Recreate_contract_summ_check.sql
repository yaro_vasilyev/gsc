ALTER TABLE gsc.contract
  ADD CONSTRAINT contract_summ_chk CHECK (contract_type = 1 AND summ::decimal > 0 OR contract_type = 0 AND summ IS NULL);

COMMENT ON CONSTRAINT contract_summ_chk ON gsc.contract
IS 'Summ should be greater than 0 if contract type is paid and vise versa';