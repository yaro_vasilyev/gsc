set search_path = gsc,public;


create or replace function gsc._xxx_array_uniq(anyarray)
  returns anyarray 
  strict 
  immutable
  language sql 
as $$
  select array(select distinct $1[i] 
                 from generate_series(array_lower($1,1), array_upper($1,1)) g(i)
                order by 1);
$$;

alter function gsc._xxx_array_uniq(anyarray)
  owner to gsc;