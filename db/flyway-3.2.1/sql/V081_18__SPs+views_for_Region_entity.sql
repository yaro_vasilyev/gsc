
set search_path = gsc,public;


alter table gsc.region 
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create trigger region_bu before update
  on gsc.region for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_region
as
  select * from gsc.region;

alter view gsc.v_region
  owner to gsc;



drop function if exists gsc.region_insert(integer, varchar);
drop function if exists gsc.region_update(integer, varchar, integer, varchar);
drop function if exists gsc.region_delete(integer, varchar);

create or replace function gsc.region_insert (
    code        gsc.region.code%type,
    name        gsc.region.name%type,
    zone        integer /* just in case */)
  returns table (id gsc.region.id%type, ts gsc.region.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id      gsc.region.id%type;
  l_ts      gsc.region.ts%type;
begin
  insert into gsc.region (
              code,
              name,
              zone)
      values (code,
              name,
              zone)
    returning region.id,
              region.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.region_insert (
    code        gsc.region.code%type,
    name        gsc.region.name%type,
    zone        integer /* just in case */)
  owner to gsc;


create or replace function gsc.region_update (
    p_id          gsc.region.id%type,
    new_code      gsc.region.code%type,
    new_name      gsc.region.name%type,
    new_zone      integer /* in case of ... */,
    old_ts        gsc.region.ts%type)
  returns table (ts   gsc.region.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.region.ts%type;
begin
  update gsc.region
     set code = new_code,
         name = new_name,
         zone = new_zone
   where id = p_id
     and region.ts = old_ts
         returning region.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Субъект РФ');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.region_update (
    p_id          gsc.region.id%type,
    new_code      gsc.region.code%type,
    new_name      gsc.region.name%type,
    new_zone      integer /* in case of ... */,
    old_ts        gsc.region.ts%type)
  owner to gsc;


create or replace function gsc.region_delete (p_id gsc.region.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.region
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Субъект РФ');
  end if;
end;
$$;


alter function gsc.region_delete (p_id gsc.region.id%type)
  owner to gsc;

