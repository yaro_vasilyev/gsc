ALTER TABLE gsc.imp_station_fault
  ADD COLUMN exam_type gsc.d_exam_type;

COMMENT ON COLUMN gsc.imp_station_fault.exam_type
IS 'Форма экзамена. ЕГЭ=1, ГВЭ=2, ОГЭ=3';