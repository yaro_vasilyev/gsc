set search_path = gsc,public;

create or replace function gsc.copy_project_data (
    p_source_project_id      gsc.period.id%type,
    p_target_project_id      gsc.period.id%type)
  returns void
  language plpgsql
  volatile
  security definer
as $$
begin
  perform gsc.copy_regions (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: regions copied';

  perform gsc.copy_violations (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: violations copied';

  perform gsc.copy_violators (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: violators copied';

  perform gsc.copy_observers (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: observers copied';

  perform gsc.copy_experts (p_source_project_id, p_target_project_id);
  raise notice '%', 'copy_project_data: experts copied';

  update gsc.period
     set parent_id = p_source_project_id
   where id = p_target_project_id;

  raise notice '%', 'copy_project_data: parent updated';
end;
$$;

alter function gsc.copy_project_data (
    p_source_project_id      gsc.period.id%type,
    p_target_project_id      gsc.period.id%type)
  owner to gsc;