set search_path = gsc,public;


drop function gsc.r_gsc61_experts_count_in_regions(gsc.period.id%TYPE);

create or replace function gsc.r_gsc61_experts_count_in_regions(p_project_id gsc.period.id%TYPE, p_period_type gsc.d_period_type)
returns table (RegionCode gsc.region.code%TYPE, RegionName gsc.region.name%TYPE, Official bigint, Social bigint)
stable
as $$
begin
  return query select r.code,
       r.name,
       sum(case when e.kind = 1 then 1 else 0 end) as official, 
       sum(case when e.kind = 2 then 1 else 0 end) as social
  from schedule s
  join expert e on e.id = s.expert_id and e.period_id = p_project_id and s.period_type = p_period_type
 right join region r on r.id = s.region_id
 group by r.id
 order by r.code;
end;
$$ language plpgsql;
alter function gsc.r_gsc61_experts_count_in_regions(p_project_id gsc.period.id%TYPE, p_period_type gsc.d_period_type) owner to gsc;