CREATE VIEW gsc.wa_v_mgr_expert (
    id,
    guid,
    name,
    surname,
    patronymic)
AS
SELECT ex.id,
    u.guid,
    ex.name,
    ex.surname,
    ex.patronymic
FROM gsc.expert ex
     JOIN gsc.users u ON u.parent_id = ex.observer_id AND u.type_user = 1;