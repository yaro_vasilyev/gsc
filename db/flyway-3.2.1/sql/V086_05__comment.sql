set search_path = gsc,public;


comment on column gsc.users.type_user is 'Column type_user is deprecated. Use column rolname to get role of user';
comment on column gsc.event.user_id is 'Column user_id is deprecated. Use column user_guid instead';
