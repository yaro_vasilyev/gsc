set search_path = gsc,public;


create or replace function gsc.project_clear (
    p_project_id        integer)
  returns void
  volatile
  returns null on null input
  language plpgsql
  security invoker
as $$
declare
  l_rc      integer;
begin
  raise notice 'Removing Contract.';
  delete from gsc.contract
    where contract.number in (select contract_info.number
                                from gsc.contract_info
                               where contract_info.project_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Contract-Info.';
  delete from gsc.contract_info
   where contract_info.project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Monitor-Result.';
  delete from gsc.monitor_result
   where monitor_result.id in (
          select mr.id
            from gsc.monitor_result as mr
            join gsc.schedule on schedule.id = mr.schedule_id
            join gsc.expert on expert.id = schedule.expert_id
           where expert.period_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;

  raise notice 'Removing Event.';
  delete from gsc.event
   where event.id in (
          select e.id
            from gsc.event as e
            join gsc.schedule on schedule.id = e.schedule_id
            join gsc.expert on expert.id = schedule.expert_id
           where expert.period_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing MR-Doc.';
  delete from gsc.mr_doc
   where mr_doc.schedule_id in (
          select schedule.id
            from gsc.schedule
            join gsc.expert on expert.id = schedule.expert_id
           where expert.period_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Schedule.';
  delete from gsc.schedule
   where schedule.id in (
          select s.id
            from gsc.schedule as s
            join gsc.expert on expert.id = s.expert_id
           where expert.period_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Expert-Agreement.';
  delete from gsc.expert_agreement
   where expert_id in (
          select expert.id
            from gsc.expert
           where expert.period_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Expert.';
  delete from gsc.expert
   where period_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Observer.';
  delete from gsc.observer
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Oiv-Gek.';
  delete from gsc.oiv_gek
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing RCOI.';
  delete from gsc.rcoi
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Exec.';
  delete from gsc.exec
   where exec.id in (
          select x.id
            from gsc.exec as x
            join gsc.exam on exam.id = x.exam_id
            join gsc.subject on subject.id = exam.subject_id
            join gsc.station on station.id = x.station_id
           where subject.project_id = p_project_id
              or station.project_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Station.';
  delete from gsc.station
   where station.project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Exam.';
  delete from gsc.exam
   where exam.id in (
          select x.id
            from gsc.exam as x
            join gsc.subject on subject.id = x.subject_id
           where subject.project_id = p_project_id);
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Subject.';
  delete from gsc.subject
   where subject.project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Violation.';
  delete from gsc.violation
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Violator.';
  delete from gsc.violator
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed.', l_rc;
  
  raise notice 'Removing Region.';
  delete from gsc.region
   where project_id = p_project_id;
  get diagnostics l_rc    := row_count;
  raise notice '... % rows removed. Done.', l_rc;
  
end;
$$;

alter function gsc.project_clear (integer)
  owner to gsc;


create or replace function gsc.project_delete (p_id gsc.period.id%type)
  returns void
  volatile
  security definer
  language plpgsql
as $$
begin
  perform gsc.project_clear (p_id);

  delete from gsc.period
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Проект');
  end if;
end;
$$;

alter function gsc.project_delete (p_id gsc.period.id%type)
  owner to gsc;