set search_path = gsc,public;

----------------------------------------------------------------------------------------------------
-- Drop view
----------------------------------------------------------------------------------------------------
drop view gsc.v_logins;
drop view gsc.v_observer;

----------------------------------------------------------------------------------------------------
-- Add column
----------------------------------------------------------------------------------------------------
alter table gsc.observer
  drop column user_id,
  add column project_id int,
  add constraint observer_project_id_fkey foreign key (project_id) references gsc.period (id) 
    on update cascade on delete restrict;

----------------------------------------------------------------------------------------------------
-- Update ref to project
----------------------------------------------------------------------------------------------------
update gsc.observer
   set project_id = (select id
                       from gsc.period
                      where status = 1)
 where project_id is null;

alter table gsc.observer
  alter column project_id set not null;

----------------------------------------------------------------------------------------------------
-- Recreate view
----------------------------------------------------------------------------------------------------
create or replace view gsc.v_observer
as
  select * from gsc.observer;

alter view gsc.v_observer
  owner to gsc;

----------------------------------------------------------------------------------------------------
create or replace view gsc.v_logins (
    id,
    observer_id,
    expert_id,
    user_login,
    password)
as
select
  u.id,
  o.id as observer_id,
  e.id as expert_id,
  u.user_login,
  null::character varying as password
from gsc.v_users u
     left join gsc.v_observer o on u.guid = o.user_guid
     left join gsc.v_expert e on u.guid = e.user_guid;

alter table gsc.v_logins
  owner to gsc;

grant select
  on gsc.v_logins to s_users_list;

----------------------------------------------------------------------------------------------------
-- CRUD functions
----------------------------------------------------------------------------------------------------

create or replace function gsc.observer_insert (
    p_project_id  gsc.period.id%type,
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_guid     gsc.observer.user_guid%type)
  returns table (id gsc.observer.id%type, ts gsc.observer.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id  gsc.observer.id%type;
  l_ts  gsc.observer.ts%type;
begin
  insert into gsc.observer (
              type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_guid,
              project_id)
      values (type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_guid,
              p_project_id)
    returning observer.id, observer.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.observer_insert (
    p_project_id  gsc.period.id%type,
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_guid     gsc.observer.user_guid%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------

create or replace function gsc.observer_update (
    p_id          gsc.observer.id%type,
    p_project_id  gsc.period.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_guid gsc.observer.user_guid%type,
    old_ts        gsc.observer.ts%type)
  returns table (ts gsc.observer.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts  gsc.observer.ts%type;
begin
  update gsc.observer
     set type         = new_type,
         fio          = new_fio,
         wp           = new_wp,
         post         = new_post,
         phone1       = new_phone1,
         phone2       = new_phone2,
         email        = new_email,
         user_guid    = new_user_guid,
         project_id   = p_project_id
   where observer.id  = p_id
     and observer.ts  = old_ts
         returning observer.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('Менеджер/наблюдатель');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.observer_update (
    p_id          gsc.observer.id%type,
    p_project_id  gsc.period.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_guid gsc.observer.user_guid%type,
    old_ts        gsc.observer.ts%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------
-- Drop old functions
----------------------------------------------------------------------------------------------------
drop function if exists gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_guid     gsc.observer.user_guid%type);

drop function if exists gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_guid gsc.observer.user_guid%type,
    old_ts        gsc.observer.ts%type);

----------------------------------------------------------------------------------------------------
-- That's all folks!
----------------------------------------------------------------------------------------------------
