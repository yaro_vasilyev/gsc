ALTER TABLE gsc.station
  ADD COLUMN exam_type gsc.d_exam_type;

COMMENT ON COLUMN gsc.station.exam_type
IS 'Форма экзамена. ЕГЭ=1, ГВЭ=2, ОГЭ=3';