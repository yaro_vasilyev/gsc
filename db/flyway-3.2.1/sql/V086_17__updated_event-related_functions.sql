set search_path = gsc,public;


drop function if exists gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type);


create or replace function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.event.project_id%type,
    user_guid     gsc.event.user_guid%type)
  returns table (id gsc.event.id%type, ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.event.id%type;
  l_ts        gsc.event.ts%type;
begin
  insert into gsc.event (
              event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_guid)
      values (event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_guid)
    returning event.id, event.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.event.project_id%type,
    user_guid     gsc.event.user_guid%type)
  owner to gsc;


drop function if exists gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type);


create or replace function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.event.project_id%type,
    new_user_guid     gsc.event.user_guid%type,
    old_ts            gsc.event.ts%type)
  returns table (ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts        gsc.event.ts%type;
begin
  update gsc.event
     set event_ts = new_event_ts,
         channel = new_channel,
         type = new_type,
         schedule_id = new_schedule_id,
         object_type = new_object_type,
         project_id = new_project_id,
         user_guid = new_user_guid
   where event.id = p_id
     and event.ts = old_ts
         returning event.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Событие горячей линии');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.event.project_id%type,
    new_user_guid     gsc.event.user_guid%type,
    old_ts            gsc.event.ts%type)
  owner to gsc;

