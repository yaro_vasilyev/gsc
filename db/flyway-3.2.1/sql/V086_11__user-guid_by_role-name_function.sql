set search_path = gsc,public;


create or replace function gsc.get_user_guid(p_rolname name)
  returns gsc.users.guid%type
  stable
  language plpgsql
  security definer
as $$
declare 
  l_ret gsc.users.guid%type;
begin
  select guid
    into strict l_ret /* strict, Carl! */
    from gsc.users
   where users.rolname = p_rolname;
  return l_ret;
exception
  when NO_DATA_FOUND then
    raise exception 'Пользователь не найден для роли %', p_rolname;
  when TOO_MANY_ROWS then
    raise exception 'Для роли % найдено несколько пользователей', p_rolname;
end;
$$;

alter function gsc.get_user_guid(name)
  owner to gsc;