ALTER TABLE gsc.contract_info
  DROP CONSTRAINT contract_info_number_key CASCADE;

ALTER TABLE gsc.contract_info
  ADD PRIMARY KEY (number);