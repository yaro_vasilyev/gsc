CREATE FUNCTION gsc.login_insert (
  i_login varchar,
  i_password varchar,
  i_observer_id integer
)
RETURNS TABLE (
  id integer
) AS
$body$
DECLARE
  l_guid UUID;
BEGIN
  -- check password
  if (i_password <> '') IS NOT TRUE then -- checks password for null or ''
  	raise exception 'Не допускается пустой пароль';
  end if;
  
  -- create pg user
  EXECUTE gsc.u_create_db_user(i_login, i_password);
  
  -- insert to users
  SELECT gsc.u_register_db_user(i_login)
  INTO STRICT l_guid;
  
  -- map to expert/manager
  IF i_observer_id IS NOT NULL THEN
  	EXECUTE gsc.u_bind_observer(l_guid, i_observer_id);
  END IF;
  
  RETURN QUERY SELECT u.id FROM gsc.users u WHERE u.guid = l_guid;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100 ROWS 1000;

COMMENT ON FUNCTION gsc.login_insert(
  i_login varchar,
  i_password varchar,
  i_observer_id integer)
IS 'Stored procedure mapped on Login entity Insert';

alter function gsc.login_insert (
  i_login varchar,
  i_password varchar,
  i_observer_id integer)
  owner to gsc;