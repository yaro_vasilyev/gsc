CREATE OR REPLACE FUNCTION gsc.import_station (
  p_imp_session_guid uuid,
  p_project_id integer
)
RETURNS TABLE (
  station_code integer,
  name varchar
) AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  --// todo check import right

  -- import rows
  WITH imported AS (
    INSERT INTO gsc.station AS s(project_id, region_id, code, NAME, address, tom, exam_type)
      SELECT p_project_id,
             imp.region_id,
             imp.code,
             imp.name,
             imp.address,
             imp.tom,
             imp.exam_type
      FROM gsc.imp_station imp
      WHERE imp.imp_session_guid = p_imp_session_guid AND
            imp.imp_status = 'added'::gsc.import_status
    ON conflict (project_id, region_id, code, exam_type) DO UPDATE
      SET NAME = excluded.name,
          address = excluded.address,
          tom = excluded.tom
    returning s.code)
  /* update import status */
  UPDATE gsc.imp_station imp
  SET imp_status = 'imported'::gsc.import_status
  WHERE imp.imp_session_guid = p_imp_session_guid AND
        imp.code = ANY (select i.code from imported i);   

    -- return records presence in table but missing from import
    RETURN query
      SELECT s.code,
             s.name
      FROM gsc.station s
      WHERE NOT EXISTS (
                         SELECT NULL
                         FROM gsc.imp_station imp
                         WHERE imp.imp_session_guid = p_imp_session_guid AND
                               s.code = imp.code
            );
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;