set search_path = gsc,public;

create or replace function gsc._xxx_roles_any (p_user_role name, variadic p_roles name [])
  returns boolean
  stable
  language plpgsql
as $$
begin
  if exists(select roles.rolname 
              from gsc.all_roles_of_user(p_user_role) as roles 
             where roles.rolname = any(p_roles)) then
    return true;
  end if;
  return false;
end;
$$;

alter function gsc._xxx_roles_any (p_user_role name, variadic p_roles name [])
  owner to gsc;


create or replace function _xxx_roles_all(p_user_role name, variadic p_roles name[])
  returns boolean
  stable
  language plpgsql
as $$
declare
  l_uniq_roles name[] = gsc._xxx_array_uniq(p_roles);
  l_user_real_roles name[];
begin
  l_user_real_roles := array(select roles.rolname
                 from gsc.all_roles_of_user(p_user_role) as roles
                where roles.rolname = ANY(l_uniq_roles));
  
  if l_uniq_roles = gsc._xxx_array_uniq(l_user_real_roles) then
    return true;
  end if;
  return false;
end;
$$;

alter function _xxx_roles_all(name, variadic name[])
  owner to gsc;