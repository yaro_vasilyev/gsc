set search_path = gsc,public;

create or replace function gsc.r_gsc108_count_calls_regions (
    p_project_id      integer)
  returns table (
    numpp             integer,
    region            text,
    cnt               integer,
    percent           numeric)
  language plpgsql
  stable
  security invoker
  returns null on null input
as $$
begin
  return query
    select (row_number() over())::integer as numpp,
           v_region.name::text as region,
           count(v_event.id)::integer as cnt,
           round((100::FLOAT /
                   (
                     select count(*)
                       from gsc.v_event as e1 
                       join gsc.v_schedule as sch1 on e1.schedule_id = sch1.id
                       join gsc.v_expert as ex1 on sch1.expert_id = ex1.id
                      where ex1.period_id = p_project_id
                   )::FLOAT * count(v_event.id))::NUMERIC, 2) as percent
      from gsc.v_event
      join gsc.v_schedule on v_schedule.id = v_event.schedule_id
      join gsc.v_expert on v_schedule.expert_id = v_expert.id
      join gsc.v_region on v_schedule.region_id = v_region.id
     where v_expert.period_id = p_project_id
       and v_region.project_id = p_project_id
     group by v_region.name
     order by v_region.name;
end;
$$;

alter function gsc.r_gsc108_count_calls_regions (integer)
  owner to gsc;

