
------------------------------------------------------------------------------------
-- Recreate view
------------------------------------------------------------------------------------

drop view gsc.v_exam;

create or replace view gsc.v_exam as 
 select exam.id,
    exam.period_type,
    exam.exam_date,
    exam.subject_id,
    exam.examglobalid,
    exam.exam_type,
    exam.ts
   from gsc.exam;

alter table gsc.v_exam
  owner to gsc;
grant all on table gsc.v_exam to gsc;
grant select on table gsc.v_exam to s_exam_list;

------------------------------------------------------------------------------------
-- Drop column
------------------------------------------------------------------------------------

alter table gsc.exam
  drop constraint exam_period_id_fkey,
  drop column period_id;


------------------------------------------------------------------------------------
-- Report functions
------------------------------------------------------------------------------------

--
-- Name: monitor_result_subjects(integer); Type: FUNCTION; Schema: gsc; Owner: -
--

create or replace function gsc.monitor_result_subjects(
                p_project_id              integer) 
returns table (
                monitor_result_id         integer, 
                subject_names             text, 
                subject_codes             text)
  language plpgsql 
  stable 
  security definer
  strict
as $$
begin
  return query
    select monitor_result.id as monitor_result_id,
           array_to_string (array_agg (subject.name order by subject.name), ', ') as subject_names,
           array_to_string (array_agg (subject.code order by subject.name), ', ') as subject_codes
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id and schedule.period_id = p_project_id
      left join gsc.mr_detail on mr_detail.monitor_result_id = monitor_result.id
      left join gsc.exec on exec.id = mr_detail.exec_id and exec.project_id = p_project_id
      left join gsc.exam on exam.id = exec.exam_id
      left join gsc.subject on subject.id = exam.subject_id and subject.project_id = p_project_id
     group by monitor_result.id;
end;
$$;


alter function gsc.monitor_result_subjects(integer) owner to gsc;



create or replace function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  returns table (
    exam_date           date,
    region_code         gsc.region.code%type,
    region_name         gsc.region.name%type,
    region_zone         gsc.region.zone%type,
    subjects            text,
    expert_fio          text,
    expert_kind         gsc.expert.kind%type,
    ppe                 text,
    violation_code      gsc.violation.code%type,
    violation_name      gsc.violation.name%type,
    violator_code       gsc.violator.code%type,
    violator_name       gsc.violator.name%type,
    note                gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--    Function for GSC-095 report
-- PARAMETERS:
--    p_project_id        REQUIRED. Project ID
--    p_period_type       OPTIONAL. Exam's period type
--    p_exam_type         OPTIONAL. Exam's exam type
--    p_date              OPTIONAL. Filter date
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject as subj on subj.id = exam.subject_id
       where e.project_id = p_project_id
         and subj.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and exam.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and exam.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and exam.exam_date = p_date))
         union all -- другие результаты мониторинга (не только c запольненными ППЭ)
      select mr1.id,
             null,
             mr1.date
        from gsc.monitor_result mr1
        join gsc.schedule as sch1 on sch1.id = mr1.schedule_id -- ограничить по проекту для
       where not exists (select * from gsc.mr_detail mrd1 where mrd1.monitor_result_id = mr1.id)
         and sch1.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and sch1.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and mr1.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and mr1.date = p_date))
    ),
    cte_subjects as (
      select mr.monitor_result_id, array_to_string (array_agg (j.name order by j.name), ', ') as subjects
        from gsc.mr_detail as mrd
        join cte_station_helper as mr on mr.monitor_result_id = mrd.monitor_result_id
        join gsc.exec as x on x.id = mrd.exec_id
        join gsc.exam as e on e.id = x.exam_id
        join gsc.subject as j on j.id = e.subject_id
       where x.project_id = p_project_id
         and j.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and e.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and e.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and e.exam_date = p_date))
       group by mr.monitor_result_id
    )
    select cte_station_helper.exam_date,
           r.code as region_code,
           r.name as region_name,
           r.zone as region_zone,
           cte_subjects.subjects,
           concat_ws (' ', ex.surname, ex.name, ex.patronymic) as expert_fio,
           ex.kind as expert_kind,
           case mr.object_type
              when 1 then coalesce (st.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
              else '-'
           end as ppe,
           vn.code as violation_code,
           vn.name as violation_name,
           vr.code as violator_code,
           vr.name as violator_name,
           mr.note
      from cte_station_helper
      left join cte_subjects on cte_subjects.monitor_result_id = cte_station_helper.monitor_result_id
      left join gsc.station as st on st.id = cte_station_helper.station_id
      join gsc.monitor_result as mr on mr.id = cte_station_helper.monitor_result_id
      join gsc.schedule as sch on sch.id = mr.schedule_id
      join gsc.region as r on r.id = sch.region_id
      join gsc.expert as ex on ex.id = sch.expert_id
      left join gsc.violation as vn on vn.id = mr.violation_id
      left join gsc.violator as vr on vr.id = mr.violator_id
     where sch.period_id = p_project_id
       and ex.period_id = p_project_id
       and (p_period_type is null or (p_period_type is not null and sch.period_type = p_period_type))
       and (p_exam_type is null or (p_exam_type is not null and mr.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  owner to gsc;
  
grant execute on function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  to s_mrep_module;


create or replace function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  returns table (     region_name        gsc.region.name%type,
                      date               gsc.monitor_result.date%type,
                      subjects           text,
                      station_etc        text,
                      violation_code     gsc.violation.code%type,
                      violator_code      gsc.violator.code%type,
                      note               gsc.monitor_result.note%type)
  stable
  called on null input
  security definer
  language plpgsql
as $$
-- 
-- DESCRIPTION:
--    Function for report GSC-96
-- PARAMETERS
--    p_project_id        REQUIRED. Current project
--    p_exam_type         OPTIONAL. EGE/GVE/OGE
--    p_date              OPTIONAL. Date
--
begin
  return query
    with cte_mr_station as (
      select distinct
             exec.station_id,
             mr_detail.monitor_result_id
        from gsc.mr_detail
        join gsc.exec on mr_detail.exec_id = exec.id
    ),
    cte_mr_subjects as (
      select monitor_result.id as monitor_result_id,
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        left join gsc.mr_detail on monitor_result.id = mr_detail.monitor_result_id
        left join gsc.exec on mr_detail.exec_id = exec.id
        left join gsc.exam on exec.exam_id = exam.id
        left join gsc.subject on exam.subject_id = subject.id
       where monitor_result.object_type = 1
         and schedule.period_id = p_project_id
         and subject.project_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       group by monitor_result.id
             union all
      select monitor_result.id as monitor_result_id, 
             null as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
       where monitor_result.object_type <> 1
         and schedule.period_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
    )
    select region.name as region_name,
           monitor_result.date,
           cte_mr_subjects.subjects,
           case monitor_result.object_type
              when 1 then coalesce (station.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violator.code as violator_code,
           monitor_result.note
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.region on region.id = schedule.region_id
      left join cte_mr_subjects on cte_mr_subjects.monitor_result_id = monitor_result.id
      left join cte_mr_station on cte_mr_station.monitor_result_id = monitor_result.id
      left join gsc.station on station.id = cte_mr_station.station_id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
     where schedule.period_id = p_project_id
       and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       and (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  owner to gsc;
  
grant execute on function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  to s_repmr_module;


create or replace function gsc.r_gsc097_count_mr_ppe (
          p_project_id     gsc.period.id%type)
  returns table (
          exam_type        gsc.d_exam_type,
          violations       numeric,
          schedules        numeric)
  stable
  returns null on null input
  security definer
  language plpgsql
as $$
begin
  return query
    with cte_violations as (
      select inner_sql.exam_type,
             inner_sql.cnt
        from (
              select 1::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 1) as v
               where v.station_id is not null
              union
              select 2::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 2) as v
               where v.station_id is not null
              union
              select 3::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 3) as v
               where v.station_id is not null
             ) as inner_sql
    ),
    cte_schedules as (
      select inner_sql.exam_type,
             count (inner_sql.*)::numeric as cnt
        from (
              select distinct
                     exam.exam_type,
                     exam.exam_date,
                     station.id as station_id,
                     schedule_detail.schedule_id
                from gsc.exec
                join gsc.exam on exam.id = exec.exam_id
                join gsc.subject on subject.id = exam.subject_id
                join gsc.station on station.id = exec.station_id
                join gsc.schedule_detail on schedule_detail.station_id = station.id and schedule_detail.exam_date = exam.exam_date
               where exec.project_id = p_project_id
                 and subject.project_id = p_project_id
              ) as inner_sql
        group by
              inner_sql.exam_type
    )
    select et::gsc.d_exam_type as exam_type,
           coalesce (cte_violations.cnt, 0) as violations,
           coalesce (cte_schedules.cnt, 0) as schedules
      from unnest (array [1, 2, 3]) as et
      left join cte_violations on cte_violations.exam_type = et
      left join cte_schedules on cte_schedules.exam_type = et
     order by et;
end;
$$;

alter function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  owner to gsc;

grant execute on function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  to s_repmr_module;



create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where e.project_id = p_project_id
         and subject.project_id = p_project_id
    ),
    cte_count_by_expert_etc as (
      select cte_station_helper.exam_date as date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code as station_code, 
             count(*) as cnt
        from gsc.monitor_result as mr
        join gsc.schedule as sch on sch.id = mr.schedule_id
        left join cte_station_helper on mr.id = cte_station_helper.monitor_result_id
        left join gsc.station as s on s.id = cte_station_helper.station_id
       where mr.exam_type = p_exam_type 
         and sch.period_id = p_project_id
         and (p_exam_date is null or (p_exam_date is not null and cte_station_helper.exam_date = p_exam_date))
         and (p_station_code is null or (p_station_code is not null and s.code = p_station_code))
       group by cte_station_helper.exam_date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code
    ),
    cte_max_by_vnvr_station as (
      select i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code,
             max (i.cnt) as  max_cnt
        from cte_count_by_expert_etc as i
       group by
             i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code
    )
    select vn.code, vn.name, sum(data.max_cnt) as total
      from cte_max_by_vnvr_station as data
      join gsc.violation as vn on vn.id = data.violation_id
     group by vn.code, vn.name, data.violation_id;
end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;

grant execute on function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  to s_repmr_module;


------------------------------------------------------------------------------------
-- CRUD functions
------------------------------------------------------------------------------------

create or replace function gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type)
  returns table (id gsc.exec.id%type, ts gsc.exec.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id        gsc.exec.id%type;
  l_ts        gsc.exec.ts%type;
begin
  insert into gsc.exec (
              count_part,
              count_room,
              count_room_video,
              kim,
              station_id,
              exam_id)
      values (count_part,
              count_room,
              count_room_video,
              kim,
              station_id,
              exam_id)
    returning exec.id,
              exec.ts
         into l_id,
              l_ts;
  
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type)
  owner to gsc;

grant execute on 
  function gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type)
  to s_exec_new;

create or replace function gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type)
  returns table (ts gsc.exec.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts      gsc.exec.ts%type;
begin
  update gsc.exec
     set count_part = new_count_part,
         count_room = new_count_room,
         count_room_video = new_count_room_video,
         kim = new_kim,
         station_id = new_station_id,
         exam_id = new_exam_id
   where exec.id = p_id
     and exec.ts = old_ts
         returning exec.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Проведение экзамена');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type)
  owner to gsc;

grant execute on 
  function gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type)
  to s_exec_edit;

------------------------------------------------------------------------------------
-- Drop the old ones
------------------------------------------------------------------------------------
drop function if exists gsc.exec_insert (
    count_part          gsc.exec.count_part%type,
    count_room          gsc.exec.count_room%type,
    count_room_video    gsc.exec.count_room_video%type,
    kim                 integer,
    project_id          gsc.exec.project_id%type,
    station_id          gsc.exec.station_id%type,
    exam_id             gsc.exec.exam_id%type);


drop function if exists gsc.exec_update (
    p_id                    gsc.exec.id%type,
    new_count_part          gsc.exec.count_part%type,
    new_count_room          gsc.exec.count_room%type,
    new_count_room_video    gsc.exec.count_room_video%type,
    new_kim                 integer,
    new_project_id          gsc.exec.project_id%type,
    new_station_id          gsc.exec.station_id%type,
    new_exam_id             gsc.exec.exam_id%type,
    old_ts                  gsc.exec.ts%type);



------------------------------------------------------------------------------------
-- End
------------------------------------------------------------------------------------
