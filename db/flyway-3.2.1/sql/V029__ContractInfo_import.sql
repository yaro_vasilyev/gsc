SET search_path = gsc, public;
--
-- Name: imp_contract_info; Type: TABLE; Schema: gsc; Owner: postgres
--
CREATE TABLE imp_contract_info (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    number character varying(30) NOT NULL,
    contract_date date NOT NULL
);

ALTER TABLE imp_contract_info OWNER TO postgres;

--
-- Name: imp_contract_info_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--
ALTER TABLE ONLY imp_contract_info
    ADD CONSTRAINT imp_contract_info_key PRIMARY KEY (imp_session_guid, number);

--
-- Name: TABLE imp_contract_info; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON TABLE imp_contract_info IS 'Contract info import table.';

--
-- Name: COLUMN imp_contract_info.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';

--
-- Name: COLUMN imp_contract_info.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.imp_user IS 'User imported row.';

--
-- Name: COLUMN imp_contract_info.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.imp_datetime IS 'Import timestamp.';

--
-- Name: COLUMN imp_contract_info.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.imp_status IS 'Import status';

--
-- Name: COLUMN imp_contract_info.number; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.number IS 'Contract info number.';

--
-- Name: COLUMN imp_contract_info.contract_date; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_contract_info.contract_date IS 'Contract info date.';
--
-- Name: import_contract_info(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_contract_info(p_imp_session_guid uuid) RETURNS TABLE(number character varying, contract_date date)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right (grant)

  -- import rows
  WITH imported AS (
    INSERT INTO contract_info AS ci (number, contract_date)
      SELECT
        ici.number,
        ici.contract_date
      FROM imp_contract_info ici
      WHERE ici.imp_session_guid = p_imp_session_guid AND ici.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT contract_info_number_key
      DO UPDATE SET contract_date = EXCLUDED.contract_date
    RETURNING ci.number)
  SELECT array_agg(i.number)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_contract_info ici
  SET imp_status = 'imported'
  WHERE ici.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ci.number,
                 ci.contract_date
               FROM gsc.contract_info ci
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_contract_info ici
                                WHERE ici.imp_session_guid = p_imp_session_guid AND
                                      ici.number = ci.number);
END;
$$;
ALTER FUNCTION gsc.import_contract_info(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_row_contract_info(uuid, character varying, date); Type: FUNCTION; Schema: gsc; Owner: postgres
--
CREATE FUNCTION import_row_contract_info(imp_session_guid uuid, number character varying, contract_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO gsc.imp_contract_info (imp_session_guid, number, contract_date)
  VALUES (imp_session_guid, number, contract_date);
END;
$$;

ALTER FUNCTION gsc.import_row_contract_info(imp_session_guid uuid, number character varying, contract_date date) OWNER TO postgres;
