CREATE VIEW gsc.wa_v_schedule
AS 
  select
    sch.expert_id,
    sch.id as "schedule_id",
    sch.period_type,
    schd.id as "schedule_detail_id",
    r.id as "region_id",
    r.name as "region_name",
    em.exam_date,
    em.exam_type as "exam_type",
    st.id as "station_id",
    st.name as "station_name",
    st.code as "station_code",
    sub.id as "subject_id",
    sub.name as "subject_name"
  from  
    gsc.schedule as sch
      join gsc.schedule_detail as schd on sch.id = schd.schedule_id
      join gsc.station as st on st.id = schd.station_id
      join gsc.exec as ec on st.id = ec.station_id
      join gsc.exam as em on em.id = ec.exam_id
      join gsc.subject as sub on sub.id = em.subject_id
      join gsc.region as r on r.id = sch.region_id;