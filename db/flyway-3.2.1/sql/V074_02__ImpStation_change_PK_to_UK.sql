 -- object recreation
ALTER TABLE gsc.imp_station
  DROP CONSTRAINT imp_station_pkey RESTRICT;

ALTER TABLE gsc.imp_station
  ADD CONSTRAINT imp_station_uniq 
    UNIQUE (imp_session_guid, code, region_id, exam_type);