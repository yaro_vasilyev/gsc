set search_path = gsc,public;

alter table gsc.expert
  drop constraint expert_next_period_proposal_check;

alter table gsc.expert
  add constraint expert_next_period_proposal_check check (efficiency >= 0 and efficiency <= 3);