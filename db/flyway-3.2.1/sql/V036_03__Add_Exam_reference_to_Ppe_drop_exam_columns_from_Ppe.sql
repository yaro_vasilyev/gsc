alter table ppe
  add column exam_id integer,
  add constraint ppe_exam_fkey foreign key (exam_id) references exam(id)
    on update cascade on delete restrict;


update ppe
   set exam_id = (select id
                     from exam
                    where exam.exam_date = ppe.exam_date and
                          exam.subject_id = ppe.subject_id and
                          exam.period_id = ppe.project_id);

alter table ppe
  drop column exam_date,
  drop column subject_id;