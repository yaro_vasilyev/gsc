set search_path = gsc,public;

------------------------------------------------------------------------------------
-- Add FK for project
------------------------------------------------------------------------------------
alter table gsc.subject
  add column project_id integer,
  add constraint subject_project_id_fkey foreign key (project_id) references gsc.period (id);

update gsc.subject
   set project_id = (select id
                       from gsc.period
                      where status = 1)
 where project_id is null;

alter table gsc.subject
  alter column project_id set not null;


------------------------------------------------------------------------------------
-- Replace view
------------------------------------------------------------------------------------
create or replace view gsc.v_subject as 
 select *
   from gsc.subject;

alter table gsc.v_subject
  owner to gsc;

grant all on table gsc.v_subject to gsc;
grant select on table gsc.v_subject to s_subject_list;
------------------------------------------------------------------------------------
-- Insert proc
------------------------------------------------------------------------------------

create or replace function gsc.subject_insert (
    p_code        gsc.subject.code%type,
    p_name        gsc.subject.name%type,
    p_project_id  gsc.subject.project_id%type)
  returns table ( 
    id            gsc.subject.id%type, 
    ts            gsc.subject.ts%type)
  volatile
  language plpgsql
  security definer
as $$
declare
  l_id      gsc.subject.id%type;
  l_ts      gsc.subject.ts%type;
begin
  insert into gsc.subject (
              code,
              name,
              project_id)
      values (p_code,
              p_name,
              p_project_id)
    returning subject.id,
              subject.ts
         into l_id,
              l_ts;

  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.subject_insert (
    gsc.subject.code%type,
    gsc.subject.name%type,
    gsc.subject.project_id%type)
  owner to gsc;

grant execute on 
  function gsc.subject_insert(integer, varchar, integer) 
  to s_subject_new;


------------------------------------------------------------------------------------
-- Update proc
------------------------------------------------------------------------------------

create or replace function gsc.subject_update (
    p_id              gsc.subject.id%type,
    p_new_code        gsc.subject.code%type,
    p_new_name        gsc.subject.name%type,
    p_new_project_id  gsc.subject.project_id%type,
    p_old_ts          gsc.subject.ts%type)
  returns table (
    ts                gsc.subject.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts      gsc.subject.ts%type;
begin
  update gsc.subject
     set code = p_new_code,
         name = p_new_name,
         project_id = p_new_project_id
   where id = p_id
     and subject.ts = p_old_ts
         returning subject.ts into l_ts;

  if not found then
    select gsc.err_concurrency_control('Предмет');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.subject_update (
    gsc.subject.id%type,
    gsc.subject.code%type,
    gsc.subject.name%type,
    gsc.subject.project_id%type,
    gsc.subject.ts%type)
  owner to gsc;

grant execute on
  function gsc.subject_update(integer, integer, varchar, integer, timestamp)
  to s_subject_edit;

------------------------------------------------------------------------------------
-- Drop the old ones
------------------------------------------------------------------------------------
drop function if exists gsc.subject_insert (
    gsc.subject.code%type,
    gsc.subject.name%type);


drop function if exists gsc.subject_update (
    gsc.subject.id%type,
    gsc.subject.code%type,
    gsc.subject.name%type,
    gsc.subject.ts%type);

------------------------------------------------------------------------------------
-- End
------------------------------------------------------------------------------------
