ALTER TABLE gsc.contract
  DROP CONSTRAINT contract_expert_fk RESTRICT;
	
ALTER TABLE gsc.contract
  RENAME COLUMN expert_id TO schedule_id;

COMMENT ON COLUMN gsc.contract.schedule_id
IS 'Reference to schedule head';

ALTER TABLE gsc.contract
  ADD CONSTRAINT contract_schedule_fk FOREIGN KEY (schedule_id)
    REFERENCES gsc.schedule(id)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

COMMENT ON CONSTRAINT contract_schedule_fk ON gsc.contract
IS 'Reference to schedule head';