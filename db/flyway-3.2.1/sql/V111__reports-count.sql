set search_path = gsc,public;

create or replace function gsc.cc_expert_reports_count(
          p_expert_id integer, 
          p_period_id integer)
  returns integer
  language plpgsql
  stable
as $$
declare
  ret     integer;
begin
  select doc_count
    into ret
    from gsc.v_expert_doc_stat
   where expert_id = p_expert_id;

  return coalesce(ret, 0);
end;
$$;


alter function gsc.cc_expert_reports_count(integer, integer) owner to gsc;
