SET search_path = gsc, public;

--
-- Name: observer; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE observer (
    id integer NOT NULL,
    type integer DEFAULT 1 NOT NULL,
    fio character varying(300) DEFAULT ''::character varying NOT NULL,
    wp character varying(500) DEFAULT ''::character varying NOT NULL,
    post character varying(500) DEFAULT ''::character varying NOT NULL,
    phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    email character varying(100) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT observer_type_check CHECK ((type = ANY (ARRAY[1, 2])))
);

--
-- Name: observer_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY observer
    ADD CONSTRAINT observer_pkey PRIMARY KEY (id);




ALTER TABLE observer OWNER TO gsc;

--
-- Name: observer_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE observer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE observer_id_seq OWNER TO gsc;

--
-- Name: observer_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE observer_id_seq OWNED BY observer.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY observer ALTER COLUMN id SET DEFAULT nextval('observer_id_seq'::regclass);

