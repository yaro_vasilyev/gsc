set search_path = gsc,public;
--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  returns table (
          date             date,
          subjects         text,
          station_etc      text,
          violation_code   gsc.violation.code%type,
          violation_name   gsc.violation.name%type,
          violator_code    gsc.violator.code%type,
          violator_name    gsc.violator.name%type,
          serie            bigint)
  stable
  called on null input
  security definer
  language plpgsql
as $$
--
-- Parameters:
--    p_project_id    REQUIRED. Project
--    p_region_id     REQUIRED. Region
--    p_exam_type     OPTIONAL. Type of Exam
begin
  if p_project_id is null then
    return;
  end if;
  if p_region_id is null then
    return;
  end if;
  
  return query
    with cte_subjects as (
      select station.id, 
             exam.exam_date, 
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.exec
        join gsc.station on station.id = exec.station_id
        join gsc.exam on exam.id = exec.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where subject.project_id = p_project_id
         and station.project_id = p_project_id
         and (p_region_id is null or p_region_id is not null and p_region_id = station.region_id)
         and (p_exam_type is null or p_exam_type is not null and p_exam_type = exam.exam_type)
       group by 
             station.id,
             exam.exam_date
    )
    select distinct
           coalesce (schedule_detail.exam_date, monitor_result.date) as date, 
           cte_subjects.subjects,
           case monitor_result.object_type 
            when 1 then 'ППЭ ' || station.code
            when 2 then 'РЦОИ'
            when 3 then 'КК'
            when 4 then 'ППЗ'
           end as station_etc, 
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violator_name,
           0::bigint as serie
      from gsc.monitor_result
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.v_expert on v_expert.id = schedule.expert_id
      left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id
      left join gsc.station on station.id = schedule_detail.station_id
      left join cte_subjects on cte_subjects.id = station.id and cte_subjects.exam_date = schedule_detail.exam_date
     where v_expert.period_id = p_project_id
       and (p_region_id is null or p_region_id is not null and schedule.region_id = p_region_id)
       and (p_exam_type is null or p_exam_type is not null and monitor_result.exam_type = p_exam_type)
     order by 1;
end;
$$;

alter function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  owner to gsc;
