set search_path = gsc,public;


alter table gsc.monitor_result 
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create or replace view gsc.v_monitor_result 
as 
  select mr.* 
    from gsc.monitor_result as mr 
    join gsc.v_schedule as s on s.id = mr.schedule_id;

alter view gsc.v_monitor_result owner to gsc;

create trigger monitor_result_bu
  before update 
  on gsc.monitor_result for each row 
  execute procedure gsc.update_ts_trigger_fn();


create or replace function gsc.monitor_result_insert (
    schedule_id            gsc.monitor_result.schedule_id%type,
    date                   gsc.monitor_result.date%type,
    violation_id           gsc.monitor_result.violation_id%type,
    violator_id            gsc.monitor_result.violator_id%type,
    note                   gsc.monitor_result.note%type,
    object_type            gsc.monitor_result.object_type%type,
    schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    exam_type              gsc.monitor_result.exam_type%type,
    event_id               gsc.monitor_result.event_id%type)
  returns table (id gsc.monitor_result.id%type, ts gsc.monitor_result.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id gsc.monitor_result.id%type;
  l_ts gsc.monitor_result.ts%type;
begin
  insert into gsc.monitor_result (
              -- id,
              schedule_id,
              date,
              violation_id,
              violator_id,
              note,
              object_type,
              schedule_detail_id,
              exam_type,
              event_id)
      values (--id,
              schedule_id,
              date,
              violation_id,
              violator_id,
              note,
              object_type,
              schedule_detail_id,
              exam_type,
              event_id)
    returning monitor_result.id, monitor_result.ts into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.monitor_result_insert (
    schedule_id            gsc.monitor_result.schedule_id%type,
    date                   gsc.monitor_result.date%type,
    violation_id           gsc.monitor_result.violation_id%type,
    violator_id            gsc.monitor_result.violator_id%type,
    note                   gsc.monitor_result.note%type,
    object_type            gsc.monitor_result.object_type%type,
    schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    exam_type              gsc.monitor_result.exam_type%type,
    event_id               gsc.monitor_result.event_id%type)
  owner to gsc;




create or replace function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            gsc.monitor_result.object_type%type,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              gsc.monitor_result.exam_type%type,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  returns table (ts gsc.monitor_result.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts gsc.monitor_result.ts%type;
begin
  update gsc.monitor_result
     set schedule_id = new_schedule_id,
         date = new_date,
         violation_id = new_violation_id,
         violator_id = new_violator_id,
         note = new_note,
         object_type = new_object_type,
         schedule_detail_id = new_schedule_detail_id,
         exam_type = new_exam_type,
         event_id = new_event_id
   where id = p_id
     and ts = old_ts
         returning monitor_result.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Результаты мониторинга');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            gsc.monitor_result.object_type%type,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              gsc.monitor_result.exam_type%type,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  owner to gsc;



create or replace function gsc.monitor_result_delete (p_id gsc.monitor_result.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.monitor_result
    where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Результаты мониторинга');
  end if;
end;
$$;

alter function gsc.monitor_result_delete (gsc.monitor_result.id%type)
  owner to gsc;



alter table gsc.mr_detail
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create or replace view gsc.v_mr_detail
as
  select d.* from gsc.mr_detail as d
    join v_monitor_result as mr on mr.id = d.monitor_result_id;

alter view gsc.v_mr_detail
 owner to gsc;


create trigger mr_detail_bu
  before update 
  on gsc.mr_detail for each row 
  execute procedure gsc.update_ts_trigger_fn();



create or replace function gsc.mr_detail_insert (
    monitor_result_id      gsc.mr_detail.monitor_result_id%type,
    exec_id                gsc.mr_detail.exec_id%type)
  returns table(id gsc.mr_detail.id%type, ts gsc.mr_detail.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id gsc.mr_detail.id%type;
  l_ts gsc.mr_detail.ts%type;
begin
  insert into gsc.mr_detail (
              monitor_result_id,
              exec_id)
      values (monitor_result_id,
              exec_id)
    returning mr_detail.id, mr_detail.ts 
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.mr_detail_insert (
    monitor_result_id      gsc.mr_detail.monitor_result_id%type,
    exec_id                gsc.mr_detail.exec_id%type)
  owner to gsc;





create or replace function gsc.mr_detail_update (
    p_id                   gsc.mr_detail.id%type,
    new_monitor_result_id  gsc.mr_detail.monitor_result_id%type,
    new_exec_id            gsc.mr_detail.exec_id%type,
    old_ts                 gsc.mr_detail.ts%type)
  returns table (ts gsc.mr_detail.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts gsc.mr_detail.ts%type;
begin
  update gsc.mr_detail
     set monitor_result_id = new_monitor_result_id,
         exec_id = new_exec_id
   where id = p_id
     and ts = old_ts
         returning mr_detail.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Детализация мониторинга');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.mr_detail_update (
    p_id                   gsc.mr_detail.id%type,
    new_monitor_result_id  gsc.mr_detail.monitor_result_id%type,
    new_exec_id            gsc.mr_detail.exec_id%type,
    old_ts                 gsc.mr_detail.ts%type)
  owner to gsc;



create or replace function gsc.mr_detail_delete (p_id gsc.mr_detail.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.mr_detail
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Детализация мониторинга');
  end if;
end;
$$;


alter function gsc.mr_detail_delete (p_id gsc.mr_detail.id%type)
  owner to gsc;



create or replace view gsc.v_mr_detail
as
  select d.* 
    from gsc.mr_detail as d
    join gsc.v_monitor_result as mr on mr.id = d.monitor_result_id;

alter view gsc.v_mr_detail
  owner to gsc;




alter table gsc.observer
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create or replace view gsc.v_observer
as
  select *
    from gsc.get_observers(gsc.get_user_id(current_user));
    
alter view gsc.v_observer
  owner to gsc;

create trigger observer_bu
  before update 
  on gsc.observer for each row 
  execute procedure gsc.update_ts_trigger_fn();




create or replace function gsc.observer_insert (
    type          gsc.observer.type%type,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type)
  returns table (id gsc.observer.id%type, ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.observer.id%type;
  l_ts  gsc.observer.ts%type;
begin
  insert into gsc.observer (
              type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_id)
      values (type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_id)
    returning observer.id, observer.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.observer_insert (
    type          gsc.observer.type%type,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type)
  owner to gsc;






create or replace function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      gsc.observer.type%type,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type)
  returns table (ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.observer.ts%type;
begin
  update gsc.observer
     set type = new_type,
         fio = new_fio,
         wp = new_wp,
         post = new_post,
         phone1 = new_phone1,
         phone2 = new_phone2,
         email = new_email,
         user_id = new_user_id
   where id = p_id
     and ts = old_ts
         returning observer.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('Менеджер/наблюдатель');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      gsc.observer.type%type,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type)
  owner to gsc;



create or replace function gsc.observer_delete (p_id gsc.observer.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.observer
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Менеджер/наблюдатель');
  end if;
end;
$$;

alter function gsc.observer_delete (p_id gsc.observer.id%type)
  owner to gsc;




alter table gsc.schedule
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create or replace view gsc.v_schedule
as
  select s.*
    from gsc.schedule as s
    join v_expert as e on e.id = s.expert_id;

alter view gsc.v_schedule
  owner to gsc;

create trigger schedule_bu
  before update on gsc.schedule
    for each row execute procedure gsc.update_ts_trigger_fn();




create or replace function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   gsc.schedule.period_type%type,
    region_id     gsc.schedule.region_id%type)
  returns table (id gsc.schedule.id%type, ts gsc.schedule.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.schedule.id%type;
  l_ts  gsc.schedule.ts%type;
begin
  insert into gsc.schedule (
              expert_id,
              period_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
      values (expert_id,
              period_id,
              monitor_rcoi,
              fixed,
              period_type,
              region_id)
    returning schedule.id, schedule.ts
         into l_id, l_ts;
   return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.schedule_insert (
    expert_id     gsc.schedule.expert_id%type,
    period_id     gsc.schedule.period_id%type,
    monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    fixed         gsc.schedule.fixed%type,
    period_type   gsc.schedule.period_type%type,
    region_id     gsc.schedule.region_id%type)
  owner to gsc;



create or replace function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   gsc.schedule.period_type%type,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  returns table (ts gsc.schedule.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.schedule.ts%type;
begin
  update gsc.schedule
     set expert_id = new_expert_id,
         period_id = new_period_id,
         monitor_rcoi = new_monitor_rcoi,
         fixed = new_fixed,
         period_type = new_period_type,
         region_id = new_region_id
   where id = p_id
     and ts = old_ts
         returning schedule.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('График');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   gsc.schedule.period_type%type,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  owner to gsc;




create or replace function gsc.schedule_delete (p_id gsc.schedule.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.schedule
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('График');
  end if;
end;
$$;

alter function gsc.schedule_delete (p_id gsc.schedule.id%type)
  owner to gsc;




alter table gsc.schedule_detail
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create or replace view gsc.v_schedule_detail
as
  select sd.* 
    from gsc.schedule_detail as sd
    join gsc.v_schedule as s on s.id = sd.schedule_id;


alter view gsc.v_schedule_detail
  owner to gsc;




create trigger schedule_detail_bu before update
  on gsc.schedule_detail for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace function gsc.schedule_detail_insert (
    schedule_id   gsc.schedule_detail.schedule_id%type,
    station_id    gsc.schedule_detail.station_id%type,
    exam_date     gsc.schedule_detail.exam_date%type)
  returns table (id gsc.schedule_detail.id%type, ts gsc.schedule_detail.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id    gsc.schedule_detail.id%type;
  l_ts    gsc.schedule_detail.ts%type;
begin
  insert into gsc.schedule_detail (
              schedule_id,
              station_id,
              exam_date)
      values (schedule_id,
              station_id,
              exam_date)
   returning schedule_detail.id, schedule_detail.ts
        into l_id, l_ts;
    
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.schedule_detail_insert (
    schedule_id   gsc.schedule_detail.schedule_id%type,
    station_id    gsc.schedule_detail.station_id%type,
    exam_date     gsc.schedule_detail.exam_date%type)
  owner to gsc;




create or replace function gsc.schedule_detail_update (
    p_id              gsc.schedule_detail.id%type,
    new_schedule_id   gsc.schedule_detail.schedule_id%type,
    new_station_id    gsc.schedule_detail.station_id%type,
    new_exam_date     gsc.schedule_detail.exam_date%type,
    old_ts            gsc.schedule_detail.ts%type)
  returns table (ts gsc.schedule_detail.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.schedule_detail.ts%type;
begin
  update gsc.schedule_detail
     set schedule_id = new_schedule_id,
         station_id = new_station_id,
         exam_date = new_exam_date
   where id = p_id
     and ts = old_ts
         returning schedule_detail.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Детализация графика');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.schedule_detail_update (
    p_id              gsc.schedule_detail.id%type,
    new_schedule_id   gsc.schedule_detail.schedule_id%type,
    new_station_id    gsc.schedule_detail.station_id%type,
    new_exam_date     gsc.schedule_detail.exam_date%type,
    old_ts            gsc.schedule_detail.ts%type)
  owner to gsc;




create or replace function gsc.schedule_detail_delete (p_id gsc.schedule_detail.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.schedule_detail
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Детализация графика');
  end if;
end;
$$;

alter function gsc.schedule_detail_delete (p_id gsc.schedule_detail.id%type)
  owner to gsc;
