set search_path = gsc,public;

create or replace view gsc.v_event
as
select e.id,
    e.event_ts,
    e.channel,
    e.type,
    e.schedule_id,
    e.object_type,
    e.project_id,
    e.user_id,
    e.ts,
    e.user_guid
from event e
     join v_schedule s on s.id = e.schedule_id;

alter view gsc.v_event
  owner to gsc;
