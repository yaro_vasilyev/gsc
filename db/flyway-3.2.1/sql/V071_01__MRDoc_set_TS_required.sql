set search_path = gsc,public;

update gsc.mr_doc 
   set ts = current_timestamp 
 where ts is null;

alter table mr_doc 
  alter column ts set not null;
