set search_path = gsc,public;


drop function if exists gsc.gsc_current_user();

create or replace function gsc.gsc_current_user() 
  returns table(Id gsc.users.id%type, 
                UserLogin gsc.users.user_login%type, 
                Role gsc.users.rolname%type, 
                Guid gsc.users.guid%type)
as $$
    select id,
           user_login,
           rolname,
           guid
      from gsc.v_users 
     where rolname = session_user;
$$ language sql;

alter function gsc.gsc_current_user() 
  owner to gsc;