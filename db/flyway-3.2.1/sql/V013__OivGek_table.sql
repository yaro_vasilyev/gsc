SET search_path = gsc, public;

--
-- Name: oiv_gek; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE oiv_gek (
    id integer NOT NULL,
    region_id integer NOT NULL,
    oiv_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    oiv_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    oiv_post character varying(500) DEFAULT ''::character varying NOT NULL,
    oiv_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    oiv_email character varying(100) DEFAULT ''::character varying NOT NULL,
    gek_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    gek_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    gek_post character varying(500) DEFAULT ''::character varying NOT NULL,
    gek_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    gek_email character varying(100) DEFAULT ''::character varying NOT NULL,
    gia_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    gia_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    gia_post character varying(500) DEFAULT ''::character varying NOT NULL,
    gia_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    gia_email character varying(100) DEFAULT ''::character varying NOT NULL,
    nadzor_fio character varying(300) DEFAULT ''::character varying NOT NULL,
    nadzor_wp character varying(500) DEFAULT ''::character varying NOT NULL,
    nadzor_post character varying(500) DEFAULT ''::character varying NOT NULL,
    nadzor_phone1 character varying(50) DEFAULT ''::character varying NOT NULL,
    nadzor_email character varying(100) DEFAULT ''::character varying NOT NULL,
    oiv_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    gek_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    gia_phone2 character varying(50) DEFAULT ''::character varying NOT NULL,
    nadzor_phone2 character varying(50) DEFAULT ''::character varying NOT NULL
);

--
-- Name: oiv_gek_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY oiv_gek
    ADD CONSTRAINT oiv_gek_pkey PRIMARY KEY (id);
--
-- Name: oiv_gek_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY oiv_gek
    ADD CONSTRAINT oiv_gek_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);


ALTER TABLE oiv_gek OWNER TO gsc;

--
-- Name: oiv_gek_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE oiv_gek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oiv_gek_id_seq OWNER TO gsc;

--
-- Name: oiv_gek_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE oiv_gek_id_seq OWNED BY oiv_gek.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY oiv_gek ALTER COLUMN id SET DEFAULT nextval('oiv_gek_id_seq'::regclass);

