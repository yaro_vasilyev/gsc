set search_path = gsc,public;


alter table gsc.schedule 
  add constraint schedule_unq unique (expert_id, period_id, period_type, region_id);