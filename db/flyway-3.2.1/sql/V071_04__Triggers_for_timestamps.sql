set search_path = gsc,public;

create trigger expert_agreement_ts
  before insert
  on gsc.expert_agreement
  for each row
  execute procedure expert_update_ts();


create trigger mr_doc_ts
  before insert
  on gsc.mr_doc
  for each row
  execute procedure expert_update_ts();
