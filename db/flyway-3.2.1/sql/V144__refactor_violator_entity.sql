set search_path = gsc,public;

----------------------------------------------------------------------------------------------------
-- Add column
----------------------------------------------------------------------------------------------------

alter table gsc.violator
  add column project_id int,
  add constraint violator_project_id_fkey foreign key (project_id) references gsc.period (id);


update gsc.violator
   set project_id = (select id
                       from gsc.period
                      where status = 1)
 where project_id is null;
 

alter table gsc.violator
  alter column project_id set not null;

----------------------------------------------------------------------------------------------------
-- Recreate view
----------------------------------------------------------------------------------------------------

create or replace view gsc.v_violator
as
  select * from gsc.violator;

alter view gsc.v_violator owner to gsc;

----------------------------------------------------------------------------------------------------
-- CRUD functions
----------------------------------------------------------------------------------------------------

create or replace function gsc.violator_insert (
    p_project_id    gsc.period.id%type,
    code            gsc.violator.code%type,
    name            gsc.violator.name%type)
  returns table (
    id              gsc.violator.id%type, 
    ts              gsc.violator.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id      gsc.violator.id%type;
  l_ts      gsc.violator.ts%type;
begin
  insert into gsc.violator (
              project_id,
              code,
              name)
      values (p_project_id,
              code,
              name)
    returning violator.id,
              violator.ts
         into l_id,
              l_ts;

  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.violator_insert (
    p_project_id    gsc.period.id%type,
    code            gsc.violator.code%type,
    name            gsc.violator.name%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------

create or replace function gsc.violator_update (
    p_id              gsc.violator.id%type,
    p_new_project_id  gsc.period.id%type,
    new_code          gsc.violator.code%type,
    new_name          gsc.violator.name%type,
    old_ts            gsc.violator.ts%type)
  returns table (
    ts                gsc.violator.ts%type)
  security definer
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.violator.ts%type;
begin
  update gsc.violator
     set project_id   = p_new_project_id,
         code         = new_code,
         name         = new_name
   where id           = p_id
     and violator.ts  = old_ts
         returning violator.ts into l_ts;

  if not found then
    select gsc.err_concurrency_control('Нарушитель');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.violator_update (
    p_id        gsc.violator.id%type,
    p_new_project_id gsc.period.id%type,
    new_code    gsc.violator.code%type,
    new_name    gsc.violator.name%type,
    old_ts      gsc.violator.ts%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------
-- Drop old ones
----------------------------------------------------------------------------------------------------

drop function if exists gsc.violator_insert (
    code      gsc.violator.code%type,
    name      gsc.violator.name%type);


drop function if exists gsc.violator_update (
    p_id        gsc.violator.id%type,
    new_code    gsc.violator.code%type,
    new_name    gsc.violator.name%type,
    old_ts      gsc.violator.ts%type);


----------------------------------------------------------------------------------------------------
-- That's all folks!
----------------------------------------------------------------------------------------------------

