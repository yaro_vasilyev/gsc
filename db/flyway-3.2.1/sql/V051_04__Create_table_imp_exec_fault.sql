CREATE TABLE gsc.imp_exec_fault (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status NOT NULL,
  imp_region INTEGER,
  region_id INTEGER,
  code INTEGER,
  name VARCHAR(500),
  address VARCHAR(500),
  tom gsc.d_boolean_type,
  CONSTRAINT imp_exec_fault_region_id_fkey FOREIGN KEY (region_id)
    REFERENCES gsc.region(id)
    ON DELETE SET NULL
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) 
WITH (oids = false);

ALTER TABLE gsc.imp_exec_fault
  OWNER TO gsc;

COMMENT ON TABLE gsc.imp_exec_fault
IS 'Sttaion import table.';

COMMENT ON COLUMN gsc.imp_exec_fault.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_exec_fault.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_exec_fault.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_exec_fault.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_exec_fault.imp_region
IS 'Region code.';

COMMENT ON COLUMN gsc.imp_exec_fault.region_id
IS 'Region id - FK on regions table.';

COMMENT ON COLUMN gsc.imp_exec_fault.code
IS 'Station code.';

COMMENT ON COLUMN gsc.imp_exec_fault.name
IS 'Station name.';

COMMENT ON COLUMN gsc.imp_exec_fault.address
IS 'Station address.';

COMMENT ON COLUMN gsc.imp_exec_fault.tom
IS 'Station tom.';