SET search_path = gsc, public;

--
-- Name: import_status; Type: TYPE; Schema: gsc; Owner: gsc
--

CREATE TYPE import_status AS ENUM (
    'added',
    'imported',
    'error'
);


ALTER TYPE import_status OWNER TO gsc;

--
-- Name: TYPE import_status; Type: COMMENT; Schema: gsc; Owner: gsc
--

COMMENT ON TYPE import_status IS 'Import status for import (imp_*) tables.';
