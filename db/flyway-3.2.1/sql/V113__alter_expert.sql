ALTER TABLE gsc.expert
  ALTER COLUMN workplace DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.workplace
IS 'Место работы';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN post DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.post
IS 'Должность';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN post_value DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.post_value
IS 'Оценка по критерию «должность»';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN birth_date DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.birth_date
IS 'Дата рождения';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN birth_place DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.birth_place
IS 'Место рождения';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN live_address DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.live_address
IS 'Адрес фактического проживания';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN phone DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.phone
IS 'Номер телефона';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN expirience DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.expirience
IS 'Опыт аналогичной работы';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN speciality DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.speciality
IS 'Образование (специализация)';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN speciality_value DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.speciality_value
IS 'Оценка по критерию «образование»';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN work_years_total DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.work_years_total
IS 'Общий стаж работы';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN work_years_education DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.work_years_education
IS 'Стаж работы в системе образования';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN work_years_education_value DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.work_years_education_value
IS 'Оценка по критерию «стаж работы в системе образования»';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN work_years_chief DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.work_years_chief
IS 'Стаж руководящей работы';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN exp_years_education DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.exp_years_education
IS 'Опыт работы в системе образования';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN exp_years_education_value DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.exp_years_education_value
IS 'Оценка по критерию «опыт работы в системе образования»';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN chief_name DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.chief_name
IS 'ФИО руководителя';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN chief_post DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.chief_post
IS 'Должность руководителя';

--//--

ALTER TABLE gsc.expert
  ALTER COLUMN chief_email DROP NOT NULL;

COMMENT ON COLUMN gsc.expert.chief_email
IS 'Электронный адрес для направления писем работодателю';