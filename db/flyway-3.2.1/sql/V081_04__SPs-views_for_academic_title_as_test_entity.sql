
set search_path = gsc,public;

alter table gsc.academic_title
  add column ts timestamp without time zone default now(),
  alter column ts set not null;



create trigger academic_title_bu before update 
  on gsc.academic_title
    for each row execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_academic_title
as
  select *
    from gsc.academic_title;

alter view gsc.v_academic_title
  owner to gsc;
  


create or replace function gsc.academic_title_insert (
    code      gsc.academic_title.code%type,
    name      gsc.academic_title.name%type,
    default_val   gsc.academic_title.default_val%type)
  returns table(id gsc.academic_title.id%type, ts gsc.academic_title.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.academic_title.id%type;
  l_ts  gsc.academic_title.ts%type;
begin
  insert into gsc.academic_title (
              code,
              name,
              default_val)
      values (code,
              name,
              default_val)
   returning academic_title.id, academic_title.ts
        into l_id, l_ts;
  
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.academic_title_insert (
    code      gsc.academic_title.code%type,
    name      gsc.academic_title.name%type,
    default_val   gsc.academic_title.default_val%type)
  owner to gsc;


create or replace function gsc.academic_title_update (
    p_id      gsc.academic_title.id%type,
    new_code    gsc.academic_title.code%type,
    new_name    gsc.academic_title.name%type,
    new_default_val gsc.academic_title.default_val%type,
    old_ts      gsc.academic_title.ts%type)
  returns table (ts gsc.academic_title.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts   gsc.academic_title.ts%type;
  l_rc   integer;
begin
  update gsc.academic_title
     set code = new_code,
         name = new_name,
         default_val = new_default_val
   where academic_title.id = p_id
     and academic_title.ts = old_ts
         returning academic_title.ts
              into l_ts;

  if not found then
    select gsc.err_concurrency_control('Ученое звание');
  end if;
    
  return query select l_ts as ts;
end;
$$;

alter function gsc.academic_title_update (
    p_id      gsc.academic_title.id%type,
    new_code    gsc.academic_title.code%type,
    new_name    gsc.academic_title.name%type,
    new_default_val gsc.academic_title.default_val%type,
    old_ts      gsc.academic_title.ts%type)
  owner to gsc;
  

create or replace function gsc.academic_title_delete (p_id gsc.academic_title.id%type)
  returns void
  volatile
  language plpgsql
as $$
declare
  l_rc      integer;
begin
  delete from gsc.academic_title
   where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Ученое звание');
  end if;
end;
$$;

alter function gsc.academic_title_delete (p_id gsc.academic_title.id%type)
  owner to gsc;

