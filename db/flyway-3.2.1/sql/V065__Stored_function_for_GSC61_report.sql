set search_path = gsc,public;

create or replace function gsc.r_gsc61_experts_count_in_regions(project_id gsc.period.id%TYPE)
returns table (RegionCode gsc.region.code%TYPE, RegionName gsc.region.name%TYPE, Official bigint, Social bigint)
stable
as $$
begin
  return query select r.code,
       r.name,
       sum(case when e.kind = 1 then 1 else 0 end) as official, 
       sum(case when e.kind = 2 then 1 else 0 end) as social
  from schedule s
  join expert e on e.id = s.expert_id and e.period_id = project_id
 right join region r on r.id = s.region_id
 group by r.id
 order by r.code;
end;
$$ language plpgsql;