set search_path = gsc,public;



alter table gsc.exam
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger exam_bu before update
  on gsc.exam for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_exam
as
  select * from gsc.exam;
  
alter view gsc.v_exam
  owner to gsc;


create or replace function gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    period_id           gsc.exam.period_id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer)
  returns table (id gsc.exam.id%type, ts gsc.exam.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id      gsc.exam.id%type;
  l_ts      gsc.exam.ts%type;
begin
  insert into gsc.exam (
              period_type,
              exam_date,
              subject_id,
              period_id,
              examglobalid,
              exam_type)
      values (period_type,
              exam_date,
              subject_id,
              period_id,
              examglobalid,
              exam_type)
    returning exam.id,
              exam.ts
         into l_id,
              l_ts;
              
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    period_id           gsc.exam.period_id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer)
  owner to gsc;


create or replace function gsc.exam_update (
    p_id                gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_period_id           gsc.exam.period_id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type)
  returns table (ts  gsc.exam.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts   gsc.exam.ts%type;
begin
  update gsc.exam
     set period_type = new_period_type,
         exam_date = new_exam_date,
         subject_id = new_subject_id,
         period_id = new_period_id,
         examglobalid = new_examglobalid,
         exam_type = new_exam_type
   where id = p_id
     and exam.ts = p_ts
         returning exam.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Расписание');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.exam_update (
    p_id                gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_period_id           gsc.exam.period_id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type)
  owner to gsc;


create or replace function gsc.exam_delete (p_id gsc.exam.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.exam
        where id = p_id;
        
  if not found then
    select gsc.err_concurrency_control('Расписание');
  end if;
end;
$$;

alter function gsc.exam_delete (p_id gsc.exam.id%type)
  owner to gsc;