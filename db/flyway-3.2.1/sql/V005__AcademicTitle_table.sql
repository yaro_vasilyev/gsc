SET search_path = gsc, public;

--
-- Name: academic_title; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE academic_title (
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    default_val integer DEFAULT 0 NOT NULL,
    id integer NOT NULL,
    CONSTRAINT chk_default_val CHECK ((default_val = ANY (ARRAY[0, 1, 2, 3, 4, 5])))
);


--
-- Name: academic_title_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY academic_title
    ADD CONSTRAINT academic_title_pkey PRIMARY KEY (id);

--
-- Name: academic_title_code_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY academic_title
    ADD CONSTRAINT academic_title_code_key UNIQUE (code);


ALTER TABLE academic_title OWNER TO gsc;

--
-- Name: academic_title_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE academic_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE academic_title_id_seq OWNER TO gsc;

--
-- Name: academic_title_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE academic_title_id_seq OWNED BY academic_title.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY academic_title ALTER COLUMN id SET DEFAULT nextval('academic_title_id_seq'::regclass);



--
-- Name: f_expert_academic_title(academic_title); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION f_expert_academic_title(at academic_title) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
  ret integer;
begin
  select CASE
            WHEN at.code IS NULL THEN 0
            ELSE at.default_val
        END
    into ret;
  return ret;
end;
$$;


ALTER FUNCTION gsc.f_expert_academic_title(at academic_title) OWNER TO gsc;
