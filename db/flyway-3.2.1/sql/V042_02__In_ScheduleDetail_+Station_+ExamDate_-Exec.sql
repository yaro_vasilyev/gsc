set search_path = gsc,public;

alter table schedule_detail add column station_id integer;

update schedule_detail 
   set station_id = (select exec.station_id 
                       from exec 
                      where exec.id = schedule_detail.ppe_id);


alter table schedule_detail 
  add constraint schedule_detail_station_fkey foreign key (station_id) 
    references station(id) 
    on update cascade 
    on delete restrict;

alter table schedule_detail
  alter column station_id set not null;

alter table schedule_detail 
  add column exam_date date;

update schedule_detail 
   set exam_date = (select exam.exam_date 
                      from exam 
                      join exec on exec.exam_id = exam.id 
                     where exec.id = schedule_detail.ppe_id);

alter table schedule_detail 
  alter column exam_date set not null;

alter table schedule_detail 
  drop column ppe_id;