CREATE OR REPLACE FUNCTION gsc.import_row_exec (
  imp_session_guid uuid,
  imp_region integer,
  imp_station_code integer,
  imp_examglobalid integer,
  kim integer,
  count_part integer = NULL::integer,
  count_room integer = NULL::integer,
  count_room_video integer = NULL::integer,
  exam_type gsc.d_exam_type = NULL::integer::gsc.d_exam_type
)
RETURNS void AS
$body$
DECLARE
  l_station_id integer;
  l_exam_id integer;
	l_error_detail text;
BEGIN
  -- get station
  SELECT s.id
  INTO l_station_id
  FROM gsc.station s
  WHERE s.code = imp_station_code AND
        s.region_id =
        (
          SELECT r.id
          FROM gsc.region r
          WHERE r.code = imp_region
        ) AND
        s.exam_type = import_row_exec.exam_type;
  if l_station_id is null then
    RAISE EXCEPTION 'Станция (ППЭ) с кодом % в регионе с кодом % и типом экзамена % не найдена.', imp_station_code, 
    	imp_region, exam_type
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите регионы и станции (ППЭ) и повторите импорт.';
  end if; 

  -- get exam
	SELECT e.id
  INTO l_exam_id
  FROM gsc.exam e
  WHERE e.examglobalid = imp_examglobalid;
  if l_exam_id is null then
    RAISE EXCEPTION 'Расписание с глобальным кодом (ExamGlobalID) % не найдено.', imp_examglobalid
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите расписание и повторите импорт.';
  end if; 

  BEGIN
    INSERT INTO gsc.imp_exec (imp_session_guid, imp_region, imp_station_code, station_id, imp_examglobalid, exam_id,
      kim, count_part, count_room, count_room_video, exam_type)
    VALUES (imp_session_guid, imp_region, imp_station_code, l_station_id, imp_examglobalid, l_exam_id,
      kim, count_part, count_room, count_room_video, exam_type);
  EXCEPTION
      WHEN unique_violation then
        /*GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
        RAISE EXCEPTION E'Глобальный код экзамена (ExamGlobalID) % на станции (ППЭ) с кодом % в регионе с кодом % дублируется.\nОшибка БД: %',
         imp_examglobalid, imp_station_code, imp_region, l_error_detail
        USING ERRCODE = 'unique_violation', HINT =
          'Исправьте файл и повторите импорт.';*/
    INSERT INTO gsc.imp_exec_fault (imp_session_guid, imp_status, imp_region, imp_station_code, station_id, imp_examglobalid, exam_id,
      kim, count_part, count_room, count_room_video, exam_type)
    VALUES (imp_session_guid, 'doubled'::gsc.import_status, imp_region, imp_station_code, l_station_id, imp_examglobalid, l_exam_id,
      kim, count_part, count_room, count_room_video, exam_type); 
	END;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;