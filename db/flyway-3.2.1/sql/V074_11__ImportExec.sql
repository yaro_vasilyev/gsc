CREATE OR REPLACE FUNCTION gsc.import_exec (
  p_imp_session_guid uuid,
  p_project_id integer
)
RETURNS TABLE (
  station_code integer,
  exam text
) AS
$body$
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO gsc.exec AS e (
    	project_id,
      station_id,
      exam_id,
      kim,
      count_part,
      count_room,
      count_room_video)
      SELECT
        p_project_id,
        ie.station_id,
				ie.exam_id,
				ie.kim,
				ie.count_part,
				ie.count_room,
				ie.count_room_video
      FROM gsc.imp_exec ie
      WHERE ie.imp_session_guid = p_imp_session_guid AND ie.imp_status = 'added'::gsc.import_status
    ON CONFLICT (project_id, exam_id, station_id)
      DO UPDATE SET
        kim = EXCLUDED.kim,
	      count_part = EXCLUDED.count_part,
  	    count_room = EXCLUDED.count_room,
    	  count_room_video = EXCLUDED.count_room_video
		RETURNING e.station_id, e.exam_id)
	/* update import status from returning result */
  UPDATE gsc.imp_exec ie
  	SET imp_status = 'imported'::gsc.import_status
  	WHERE ie.imp_session_guid = p_imp_session_guid AND (ie.station_id, ie.exam_id) in (
      SELECT i.station_id, i.exam_id
      FROM imported i
    );
  
  -- return records presence in table but missing from import
  RETURN QUERY
    SELECT s.code AS station_code,
           sub.name || ' ' || ex.exam_date as exam           
    FROM gsc.exec e
         LEFT JOIN gsc.station s ON e.station_id = s.id
         LEFT JOIN gsc.exam ex ON e.exam_id = ex.id
         LEFT JOIN gsc.subject sub ON ex.subject_id = sub.id
     WHERE e.project_id = p_project_id AND
          NOT exists(SELECT NULL
                      FROM gsc.imp_exec ie
                      WHERE ie.imp_session_guid = p_imp_session_guid AND
                            ie.exam_id = e.exam_id AND
                            ie.station_id = e.station_id);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;