set search_path = gsc,public;

create table gsc.mr_doc (
  id integer primary key,
  schedule_id integer not null references gsc.schedule(id),
  blob bytea,
  ts timestamp,
  filename varchar(250)
);

alter table gsc.mr_doc
  owner to gsc;
