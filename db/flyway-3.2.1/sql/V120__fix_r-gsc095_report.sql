set search_path = gsc,public;


create or replace function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  returns table (
    exam_date           date,
    region_code         gsc.region.code%type,
    region_name         gsc.region.name%type,
    region_zone         gsc.region.zone%type,
    subjects            text,
    expert_fio          text,
    expert_kind         gsc.expert.kind%type,
    ppe                 text,
    violation_code      gsc.violation.code%type,
    violation_name      gsc.violation.name%type,
    violator_code       gsc.violator.code%type,
    violator_name       gsc.violator.name%type,
    note                gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--    Function for GSC-095 report
-- PARAMETERS:
--    p_project_id        REQUIRED. Project ID
--    p_period_type       OPTIONAL. Exam's period type
--    p_exam_type         OPTIONAL. Exam's exam type
--    p_date              OPTIONAL. Filter date
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject as subj on subj.id = exam.subject_id
       where e.project_id = p_project_id
         and exam.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and exam.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and exam.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and exam.exam_date = p_date))
         union all -- другие результаты мониторинга (не только c запольненными ППЭ)
      select mr1.id,
             null,
             mr1.date
        from gsc.monitor_result mr1
        join gsc.schedule as sch1 on sch1.id = mr1.schedule_id -- ограничить по проекту для
       where not exists (select * from gsc.mr_detail mrd1 where mrd1.monitor_result_id = mr1.id)
         and sch1.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and sch1.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and mr1.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and mr1.date = p_date))
    ),
    cte_subjects as (
      select mr.monitor_result_id, array_to_string (array_agg (j.name order by j.name), ', ') as subjects
        from gsc.mr_detail as mrd
        join cte_station_helper as mr on mr.monitor_result_id = mrd.monitor_result_id
        join gsc.exec as x on x.id = mrd.exec_id
        join gsc.exam as e on e.id = x.exam_id
        join gsc.subject as j on j.id = e.subject_id
       where x.project_id = p_project_id
         and e.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and e.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and e.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and e.exam_date = p_date))
       group by mr.monitor_result_id
    )
    select cte_station_helper.exam_date,
           r.code as region_code,
           r.name as region_name,
           r.zone as region_zone,
           cte_subjects.subjects,
           concat_ws (' ', ex.surname, ex.name, ex.patronymic) as expert_fio,
           ex.kind as expert_kind,
           case mr.object_type
              when 1 then coalesce (st.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
              else '-'
           end as ppe,
           vn.code as violation_code,
           vn.name as violation_name,
           vr.code as violator_code,
           vr.name as violator_name,
           mr.note
      from cte_station_helper
      left join cte_subjects on cte_subjects.monitor_result_id = cte_station_helper.monitor_result_id
      left join gsc.station as st on st.id = cte_station_helper.station_id
      join gsc.monitor_result as mr on mr.id = cte_station_helper.monitor_result_id
      join gsc.schedule as sch on sch.id = mr.schedule_id
      join gsc.region as r on r.id = sch.region_id
      join gsc.expert as ex on ex.id = sch.expert_id
      left join gsc.violation as vn on vn.id = mr.violation_id
      left join gsc.violator as vr on vr.id = mr.violator_id
     where sch.period_id = p_project_id
       and ex.period_id = p_project_id
       and (p_period_type is null or (p_period_type is not null and sch.period_type = p_period_type))
       and (p_exam_type is null or (p_exam_type is not null and mr.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  owner to gsc;
  
grant execute on function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  to s_mrep_module;