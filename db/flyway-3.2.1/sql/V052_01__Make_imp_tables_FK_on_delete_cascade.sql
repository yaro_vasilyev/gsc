-- imp_exam
ALTER TABLE gsc.imp_exam
  DROP CONSTRAINT exam_subject_id_fkey RESTRICT;

ALTER TABLE gsc.imp_exam
  ADD CONSTRAINT exam_subject_id_fkey FOREIGN KEY (subject_id)
    REFERENCES gsc.subject(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;
		
-- imp_exec
ALTER TABLE gsc.imp_exec
  DROP CONSTRAINT imp_exec_exam_id_fkey RESTRICT;

ALTER TABLE gsc.imp_exec
  ADD CONSTRAINT imp_exec_exam_id_fkey FOREIGN KEY (exam_id)
    REFERENCES gsc.exam(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;
		
ALTER TABLE gsc.imp_exec
  DROP CONSTRAINT imp_exec_station_id_fkey RESTRICT;

ALTER TABLE gsc.imp_exec
  ADD CONSTRAINT imp_exec_station_id_fkey FOREIGN KEY (station_id)
    REFERENCES gsc.station(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;

-- imp_expert
ALTER TABLE gsc.imp_expert
  DROP CONSTRAINT imp_expert_academic_degree_id_fkey RESTRICT;

ALTER TABLE gsc.imp_expert
  ADD CONSTRAINT imp_expert_academic_degree_id_fkey FOREIGN KEY (academic_degree_id)
    REFERENCES gsc.academic_degree(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;

-- imp_expert
ALTER TABLE gsc.imp_expert
  DROP CONSTRAINT imp_expert_academic_title_id_fkey RESTRICT;

ALTER TABLE gsc.imp_expert
  ADD CONSTRAINT imp_expert_academic_title_id_fkey FOREIGN KEY (academic_title_id)
    REFERENCES gsc.academic_title(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;

-- imp_expert
ALTER TABLE gsc.imp_expert
  DROP CONSTRAINT imp_expert_base_region_id_fkey RESTRICT;

ALTER TABLE gsc.imp_expert
  ADD CONSTRAINT imp_expert_base_region_id_fkey FOREIGN KEY (base_region_id)
    REFERENCES gsc.region(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;

-- imp_rcoi
ALTER TABLE gsc.imp_rcoi
  DROP CONSTRAINT imp_rcoi_region_id_fkey RESTRICT;

ALTER TABLE gsc.imp_rcoi
  ADD CONSTRAINT imp_rcoi_region_id_fkey FOREIGN KEY (region_id)
    REFERENCES gsc.region(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;

-- imp_station
ALTER TABLE gsc.imp_station
  DROP CONSTRAINT imp_station_region_id_fkey RESTRICT;

ALTER TABLE gsc.imp_station
  ADD CONSTRAINT imp_station_region_id_fkey FOREIGN KEY (region_id)
    REFERENCES gsc.region(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    NOT DEFERRABLE;		