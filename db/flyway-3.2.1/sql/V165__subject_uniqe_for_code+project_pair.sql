set search_path = gsc,public;

alter table gsc.subject
  drop constraint subject_code_key,
  add constraint subject_code_key unique (project_id, code);