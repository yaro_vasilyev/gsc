set search_path = gsc,public;

drop view gsc.v_event;

-----------------------------------------------------------------------------------------------------
-- wa_v_mgr_event recreation
-----------------------------------------------------------------------------------------------------

-- object recreation
drop view if exists gsc.wa_v_mgr_event;

create view gsc.wa_v_mgr_event (
    id,
    event_ts,
    channel,
    type,
    schedule_id,
    object_type,
    user_guid)
as
select event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid
from gsc.event
join gsc.schedule on schedule.id = event.schedule_id
join gsc.expert on expert.id = schedule.expert_id
where expert.period_id = gsc.current_project_id();

alter table gsc.wa_v_mgr_event
  owner to gsc;

grant select, insert, update, delete, references, trigger, truncate
  on gsc.wa_v_mgr_event to gsc;

create rule "_delete" as on delete to gsc.wa_v_mgr_event 
do instead (
delete from gsc.event
  where event.id = old.id; --flyway bug: should comment
);

create rule "_insert" as on insert to gsc.wa_v_mgr_event 
do instead (
insert into gsc.event (event_ts, channel, type, schedule_id, object_type, user_guid)
  values (new.event_ts, new.channel, new.type, new.schedule_id, new.object_type, new.user_guid)
  returning event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid;  --flyway bug: should comment
);

create rule "_update" as on update to gsc.wa_v_mgr_event 
do instead (
update gsc.event set event_ts = new.event_ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id,
  object_type = new.object_type, user_guid = new.user_guid
  where event.id = old.id;  --flyway bug: should comment
);

-----------------------------------------------------------------------------------------------------
-- Drop column
-----------------------------------------------------------------------------------------------------

alter table gsc.event
  drop column project_id,
  drop column user_id;

-----------------------------------------------------------------------------------------------------
-- Recreate view
-----------------------------------------------------------------------------------------------------

create or replace view gsc.v_event
as
select e.id,
    e.event_ts,
    e.channel,
    e.type,
    e.schedule_id,
    e.object_type,
    e.ts,
    e.user_guid
from gsc.event e
join gsc.v_schedule s on s.id = e.schedule_id;

alter view gsc.v_event
  owner to gsc;

-----------------------------------------------------------------------------------------------------
-- CRUD functions
-----------------------------------------------------------------------------------------------------

drop function if exists gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    project_id    gsc.period.id%type,
    user_guid     gsc.event.user_guid%type);
  

create or replace function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    user_guid     gsc.event.user_guid%type)
  returns table (id gsc.event.id%type, ts gsc.event.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id        gsc.event.id%type;
  l_ts        gsc.event.ts%type;
begin
  insert into gsc.event (
              event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              user_guid)
      values (event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              user_guid)
    returning event.id, event.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       integer,
    type          integer,
    schedule_id   gsc.event.schedule_id%type,
    object_type   integer,
    user_guid     gsc.event.user_guid%type)
  owner to gsc;

-----------------------------------------------------------------------------------------------------

drop function gsc.event_update (
    p_id              gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_project_id    gsc.period.id%type,
    new_user_guid     gsc.event.user_guid%type,
    old_ts            gsc.event.ts%type);

create or replace function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_user_guid     gsc.event.user_guid%type,
    old_ts            gsc.event.ts%type)
  returns table (ts gsc.event.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts        gsc.event.ts%type;
begin
  update gsc.event
     set event_ts = new_event_ts,
         channel = new_channel,
         type = new_type,
         schedule_id = new_schedule_id,
         object_type = new_object_type,
         user_guid = new_user_guid
   where event.id = p_id
     and event.ts = old_ts
         returning event.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Событие горячей линии');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       integer,
    new_type          integer,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   integer,
    new_user_guid     gsc.event.user_guid%type,
    old_ts            gsc.event.ts%type)
  owner to gsc;

-----------------------------------------------------------------------------------------------------
-- That's all folks
-----------------------------------------------------------------------------------------------------
