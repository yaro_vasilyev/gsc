-- Import table
CREATE TABLE gsc.imp_exam (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
	examglobalid integer not null,
	imp_subject_code integer not null,
	subject_id integer not null,
	exam_type integer not null,
	period_type integer not null,
	exam_date date not null,
	CONSTRAINT imp_exam_pkey PRIMARY KEY(imp_session_guid, examglobalid),
  CONSTRAINT exam_exam_type_check CHECK (exam_type = ANY (ARRAY[1, 2, 3])),
  CONSTRAINT exam_period_type_check CHECK (period_type = ANY (ARRAY[1, 2, 3])),
  CONSTRAINT exam_subject_id_fkey FOREIGN KEY (subject_id)
    REFERENCES gsc.subject(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE
	)
WITH (oids = false);		

ALTER TABLE gsc.imp_exam OWNER TO gsc;

-- Import row function
CREATE OR REPLACE FUNCTION gsc.import_row_exam (
  imp_session_guid uuid,
  examglobalid integer,
  imp_subject_code integer,
  exam_type integer,
  period_type integer,
  exam_date date
)
RETURNS void AS
$body$
DECLARE
  l_subject_id integer;
	l_error_detail text;
BEGIN
  -- get subject
  SELECT s.id
  INTO l_subject_id
  FROM gsc.subject s
  WHERE s.code = imp_subject_code;
  if l_subject_id is null then
    RAISE EXCEPTION 'Предмет с кодом "%" не найден.', imp_subject_code
    USING ERRCODE = 'null_value_not_allowed', HINT =
      'Исправьте файл импорта или загрузите предметы и повторите импорт.';
  end if;
  
  INSERT INTO gsc.imp_exam (
    imp_session_guid, 
    examglobalid ,
		imp_subject_code ,
		subject_id ,
		exam_type ,
		period_type ,
		exam_date) 
	values
	(
		imp_session_guid, 
    examglobalid,
		imp_subject_code,
		l_subject_id,
		exam_type,
		period_type,
		exam_date);
	EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', examglobalid,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-- import bulk data
CREATE OR REPLACE FUNCTION gsc.import_exam (
  p_imp_session_guid uuid,
  p_period_id integer
)
RETURNS TABLE (
  examglobalid integer,
  exam text
) AS
$body$
DECLARE
  l_imported INTEGER [];
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO exam AS e (
    	period_id,
      examglobalid,
			subject_id,
			exam_type,
			period_type,
			exam_date)
      SELECT
        p_period_id,
        ie.examglobalid,
				ie.subject_id,
				ie.exam_type,
				ie.period_type,
				ie.exam_date
      FROM imp_exam ie
      WHERE ie.imp_session_guid = p_imp_session_guid AND ie.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT exam_period_id_examglobalid_key
      DO UPDATE SET
        subject_id  = EXCLUDED.subject_id,
				exam_type = EXCLUDED.exam_type,
				period_type = EXCLUDED.period_type,
				exam_date = EXCLUDED.exam_date
	RETURNING e.examglobalid)
  SELECT array_agg(i.examglobalid)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_exam ie
  SET imp_status = 'imported'
  WHERE ie.imp_session_guid = p_imp_session_guid and ie.examglobalid = ANY (l_imported);

  -- return records presence in table but missing from import
  RETURN QUERY
		SELECT e.examglobalid,
					 s.name || ' ' || e.exam_date AS exam
		FROM gsc.exam e
				 LEFT JOIN gsc.subject s ON e.subject_id = s.id
									 WHERE e.period_id = p_period_id AND
												NOT exists(SELECT NULL
																		FROM gsc.imp_exam ie
																		WHERE ie.imp_session_guid = p_imp_session_guid AND
																					ie.examglobalid = e.examglobalid);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;