CREATE OR REPLACE VIEW gsc.v_r_experts4email(
    expert_name,
    expert_workplace,
    expert_post,
    chief_name,
    chief_post,
    chief_email)
AS
  SELECT concat_ws(' '::TEXT, e.surname, e.name, e.patronymic) AS expert_name,
         e.workplace AS expert_workplace,
         e.post AS expert_post,
         e.chief_name,
         e.chief_post,
         e.chief_email
  FROM gsc.expert e
  WHERE e.period_id = gsc.current_project_id() AND
        e.engage = 1;