set search_path = gsc,public;


create or replace function gsc.expert_kind_official()
  returns integer
  immutable
  language plpgsql
as $$
begin
  return 1;
end;
$$;

alter function gsc.expert_kind_official()
  owner to gsc;

create or replace function gsc.expert_kind_social()
  returns integer
  immutable
  language plpgsql
as $$
begin
  return 2;
end;
$$;

alter function gsc.expert_kind_social()
  owner to gsc;



create or replace function gsc.observer_type_manager()
  returns integer
  immutable
  language plpgsql
as $$
begin
  return 1;
end;
$$;

alter function gsc.observer_type_manager()
  owner to gsc;


create or replace function gsc.observer_type_rsm()
  returns integer
  immutable 
  language plpgsql
as $$
begin
  return 2;
end;
$$;

alter function gsc.observer_type_rsm()
  owner to gsc;

