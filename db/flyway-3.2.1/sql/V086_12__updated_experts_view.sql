
set search_path = gsc,public;


create or replace function gsc.get_experts(p_rolname gsc.users.rolname%type)
  returns setof gsc.expert
  stable
  security definer
  language plpgsql
as $$
declare
  l_user_guid gsc.users.guid%type;
begin
  
  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users', p_rolname;
      return; -- nope, user is not in the system
  end;
  
  if gsc._xxx_roles_any(p_rolname, gsc.role_admin(), gsc.role_boss(), gsc.role_an_lead(), gsc.role_an(), gsc.role_manager()) then
    return query select * from gsc.expert; -- EVERRRRRYYYYYOOOOONNNNEEEE!!!
  elsif gsc._xxx_roles_all(p_rolname, gsc.role_rsm()) then
    return query select e.* from gsc.expert as e where e.expert_kind_social; --only rsm people
  elsif gsc._xxx_roles_all(p_rolname, gsc.role_expert()) then
    return query select * from gsc.expert where user_guid = l_user_guid; -- only self
  else
    return; -- noone here
  end if;
  return;
end;
$$;

alter function gsc.get_experts(gsc.users.rolname%type)
  owner to gsc;


create or replace view gsc.v_expert
as
  select * from gsc.get_experts(session_user);
  
alter view gsc.v_expert
  owner to gsc;
