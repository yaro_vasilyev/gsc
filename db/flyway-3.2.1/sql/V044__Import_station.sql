-- station table unique index
ALTER TABLE gsc.station
  ADD CONSTRAINT station_uniq_idx 
    UNIQUE (project_id, region_id, code);

-- Import table
CREATE TABLE gsc.imp_station (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
  imp_region INTEGER NOT NULL,
  region_id INTEGER NOT NULL,
  code INTEGER NOT NULL,
  name VARCHAR(500) NOT NULL,
  address VARCHAR(500) NOT NULL,
  tom gsc.d_boolean_type NOT NULL,
  CONSTRAINT imp_station_pkey PRIMARY KEY(imp_session_guid, code, region_id),
  CONSTRAINT imp_station_region_id_fkey FOREIGN KEY (region_id)
    REFERENCES gsc.region(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE
) 
WITH (oids = false);

COMMENT ON TABLE gsc.imp_station
IS 'Sttaion import table.';

COMMENT ON COLUMN gsc.imp_station.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_station.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_station.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_station.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_station.imp_region
IS 'Region code.';

COMMENT ON COLUMN gsc.imp_station.region_id
IS 'Region id - FK on regions table.';

COMMENT ON COLUMN gsc.imp_station.code
IS 'Station code.';

COMMENT ON COLUMN gsc.imp_station.name
IS 'Station name.';

COMMENT ON COLUMN gsc.imp_station.address
IS 'Station address.';

COMMENT ON COLUMN gsc.imp_station.tom
IS 'Station tom.';

ALTER TABLE gsc.imp_station OWNER TO gsc;

-- Import row function
CREATE OR REPLACE FUNCTION gsc.import_row_station (
  imp_session_guid uuid,
  imp_region integer,
  code integer,
  name varchar,
  address varchar,
  tom gsc.d_boolean_type
)
RETURNS void AS
$body$
DECLARE
  l_region_id integer;
	l_error_detail text;
BEGIN
  -- get region
  SELECT r.id
  INTO l_region_id
  FROM gsc.region r
  WHERE r.code = imp_region;
  if l_region_id is null then
    RAISE EXCEPTION 'Регион с кодом "%" не найден.', imp_region
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите регионы и повторите импорт.';
  end if; 

  INSERT INTO gsc.imp_station (imp_session_guid, imp_region, region_id, code, name, address, tom)
  VALUES (imp_session_guid, imp_region, l_region_id, code, name, address, tom);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % в регионе % дублируется.\nОшибка БД: %', code, imp_region, l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-- import bulk data
CREATE OR REPLACE FUNCTION gsc.import_station (
  p_imp_session_guid uuid,
  p_project_id integer
)
RETURNS TABLE (
  code integer,
  name varchar
) AS
$body$
DECLARE
  l_imported INTEGER [ ];
BEGIN
  --// todo check import right

  -- import rows
  WITH imported AS (
  INSERT INTO station AS s(project_id, region_id, code, NAME, address, tom)
  SELECT p_project_id,
         imp.region_id,
         imp.code,
         imp.name,
         imp.address,
         imp.tom
  FROM gsc.imp_station imp
  WHERE imp.imp_session_guid = p_imp_session_guid AND
        imp.imp_status = 'added' ON conflict ON CONSTRAINT station_uniq_idx DO
  UPDATE
  SET NAME = excluded.name,
      address = excluded.address,
      tom = excluded.tom returning s.code)
    SELECT array_agg(
        i.code)
    FROM imported i
    INTO l_imported;

    -- update import status

    UPDATE gsc.imp_station imp
    SET imp_status = 'imported'
    WHERE imp.imp_session_guid = p_imp_session_guid AND
          imp.code = ANY (l_imported);

    -- return records presence in table but missing from import
    RETURN query
    SELECT s.code,
           s.name
    FROM gsc.station s
    WHERE NOT EXISTS (
                       SELECT NULL
                       FROM gsc.imp_station imp
                       WHERE imp.imp_session_guid = p_imp_session_guid AND
                             s.code = imp.code
          );
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;