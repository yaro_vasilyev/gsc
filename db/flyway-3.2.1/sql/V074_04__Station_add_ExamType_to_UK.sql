 -- object recreation
ALTER TABLE gsc.station
  DROP CONSTRAINT station_uniq_idx RESTRICT;

ALTER TABLE gsc.station
  ADD CONSTRAINT station_uniq_idx 
    UNIQUE (project_id, region_id, code, exam_type);