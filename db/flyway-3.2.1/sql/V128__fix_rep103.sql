
set search_path = gsc,public;


create or replace function gsc.r_gsc103_violators (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  returns table (violator_code   gsc.violator.code%type,
                 violator_name   gsc.violator.name%type,
                 count           bigint)
  stable
  language plpgsql
  called on null input
  security definer
as $$
begin
  return query 
    select violator.code as violator_code,
           violator.name as violator_name,
           sum (i1.cnt)::bigint as count
      from (select 1 as exam_type, f.* from gsc.r_station_violations (p_project_id, 1) as f
            union 
            select 2 as exam_type, f.* from gsc.r_station_violations (p_project_id, 2) as f
            union
            select 3 as exam_type, f.* from gsc.r_station_violations (p_project_id, 3) as f) as i1
       join gsc.violator on violator.id = i1.violator_id
      where (p_exam_type is null or p_exam_type is not null and i1.exam_type = p_exam_type)
      group by i1.violator_id, violator.code, violator.name;
end;
$$;

alter function gsc.r_gsc103_violators (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc103_violators (
    gsc.period.id%type, gsc.exam.exam_type%type)
  to s_repmr_module;
