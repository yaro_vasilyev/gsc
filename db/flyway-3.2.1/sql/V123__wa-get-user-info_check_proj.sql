--func wa_get_user_info
create or replace function gsc.wa_get_user_info (guid_ uuid)
  returns table (type_user      varchar, 
                 surname        varchar, 
                 name           varchar,
                 patronymic     varchar,
                 fio            varchar,
                 post           varchar,
                 phone          varchar,
                 email          varchar) 
  language plpgsql
  stable
  returns null on null input
as $$
declare  
  l_rolname   gsc.users.rolname%type;
begin
  
  begin  
  select u.rolname
    into strict l_rolname
    from gsc.users as u
   where u.guid = guid_;
  exception
    when no_data_found then
      return;
  end;
  
  if l_rolname is null then 
    return;
  end if;

  if gsc._xxx_roles_any(l_rolname, gsc.role_manager(), gsc.role_rsm()) then
    return query select 'manager'::varchar as type_user,
                        ''::varchar as surname,
                        ''::varchar as name,
                        ''::varchar as patronymic,
                        obs.fio,
                        obs.post,
                        obs.phone1,
                        obs.email
                   from gsc.observer as obs
                  where obs.user_guid = guid_;
  elsif gsc._xxx_roles_all(l_rolname, gsc.role_expert()) then
    return query select 'expert'::varchar as type_user,
                        exp.surname,
                        exp.name,
                        exp.patronymic,
                        ''::varchar as fio,
                        exp.post,
                        exp.phone,
                        exp.email    
                   from gsc.expert as exp
                  where exp.user_guid = guid_
									  and exp.period_id = gsc.current_project_id();
  else
    raise exception 'User % should be in any of the roles: %, %, %.', 
                    guid_, role_expert(), role_manager(), role_rsm();
  end if;
end;
$$;

alter function gsc.wa_get_user_info (uuid)
  owner to gsc;