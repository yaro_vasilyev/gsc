-- station table unique index
ALTER TABLE gsc.exec
  ADD CONSTRAINT exec_uniq_idx 
    UNIQUE (project_id, exam_id, station_id);
		
-- Import table
CREATE TABLE gsc.imp_exec (
  imp_session_guid UUID NOT NULL,
  imp_user TEXT DEFAULT "current_user"() NOT NULL,
  imp_datetime TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  imp_status gsc.import_status DEFAULT 'added'::gsc.import_status NOT NULL,
  imp_region INTEGER NOT NULL,
  imp_station_code INTEGER NOT NULL,
  station_id INTEGER NOT NULL,
  imp_examglobalid INTEGER NOT NULL,
  exam_id INTEGER NOT NULL,
  kim INTEGER NOT NULL,
  count_part INTEGER,
  count_room INTEGER,
  count_room_video INTEGER,
  CONSTRAINT imp_exec_pkey PRIMARY KEY(imp_session_guid, station_id, exam_id),
  CONSTRAINT imp_exec_exam_id_fkey FOREIGN KEY (exam_id)
    REFERENCES gsc.exam(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE,
  CONSTRAINT imp_exec_station_id_fkey FOREIGN KEY (station_id)
    REFERENCES gsc.station(id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    NOT DEFERRABLE
) 
WITH (oids = false);

COMMENT ON TABLE gsc.imp_exec
IS 'Execution import table.';

COMMENT ON COLUMN gsc.imp_exec.imp_session_guid
IS 'Import session GUID for distinguishing data for different import sessions.';

COMMENT ON COLUMN gsc.imp_exec.imp_user
IS 'User imported row.';

COMMENT ON COLUMN gsc.imp_exec.imp_datetime
IS 'Import timestamp.';

COMMENT ON COLUMN gsc.imp_exec.imp_status
IS 'Import status';

COMMENT ON COLUMN gsc.imp_exec.imp_region
IS 'Region code.';

COMMENT ON COLUMN gsc.imp_exec.imp_station_code
IS 'Station code.';

COMMENT ON COLUMN gsc.imp_exec.station_id
IS 'Station id - FK on station table.';

COMMENT ON COLUMN gsc.imp_exec.imp_examglobalid
IS 'ExamGlobalID for linking to exam_id.';

COMMENT ON COLUMN gsc.imp_exec.exam_id
IS 'Exam id - KF on Exam table.';

COMMENT ON COLUMN gsc.imp_exec.kim
IS 'Is KIM printed';

COMMENT ON COLUMN gsc.imp_exec.count_part
IS 'ParticipantPPECount';

COMMENT ON COLUMN gsc.imp_exec.count_room
IS 'AuditoriumsPPECount';

COMMENT ON COLUMN gsc.imp_exec.count_room_video
IS 'AuditoriumsPPEVideoCount';

ALTER TABLE gsc.imp_exec OWNER TO gsc;

-- Import row function
CREATE OR REPLACE FUNCTION gsc.import_row_exec (
  imp_session_guid uuid,
  imp_region integer,
  imp_station_code integer,
  imp_examglobalid integer,
  kim integer,
  count_part integer = NULL::integer,
  count_room integer = NULL::integer,
  count_room_video integer = NULL::integer
)
RETURNS void AS
$body$
DECLARE
  l_station_id integer;
  l_exam_id integer;
	l_error_detail text;
BEGIN
  -- get station
  SELECT s.id
  INTO l_station_id
  FROM gsc.station s
  WHERE s.code = imp_station_code AND
        s.region_id =
        (
          SELECT r.id
          FROM gsc.region r
          WHERE r.code = imp_region
        );
  if l_station_id is null then
    RAISE EXCEPTION 'Станция (ППЭ) с кодом % в регионе с кодом % не найдена.', imp_station_code, imp_region
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите регионы и станции (ППЭ) и повторите импорт.';
  end if; 

  -- get exam
	SELECT e.id
  INTO l_exam_id
  FROM gsc.exam e
  WHERE e.examglobalid = 1;
  if l_exam_id is null then
    RAISE EXCEPTION 'Расписание с глобальным кодом (ExamGlobalID) % не найдено.', imp_examglobalid
    USING ERRCODE = 'foreign_key_violation', HINT =
      'Исправьте файл импорта или загрузите расписание и повторите импорт.';
  end if; 

  INSERT INTO gsc.imp_exec (imp_session_guid, imp_region, imp_station_code, station_id, imp_examglobalid, exam_id,
  	kim, count_part, count_room, count_room_video)
  VALUES (imp_session_guid, imp_region, imp_station_code, l_station_id, imp_examglobalid, l_exam_id,
  	kim, count_part, count_room, count_room_video);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Глобальный код экзамена (ExamGlobalID) % на станции (ППЭ) с кодом % в регионе с кодом % дублируется.\nОшибка БД: %',
       imp_examglobalid, imp_station_code, imp_region, l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-- import bulk data
CREATE OR REPLACE FUNCTION gsc.import_exec (
  p_imp_session_guid uuid,
  p_project_id integer
)
RETURNS TABLE (
  station_code integer,
  exam text
) AS
$body$
DECLARE
  l_imported INTEGER [][];
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO exec AS e (
    	project_id,
      station_id,
      exam_id,
      kim,
      count_part,
      count_room,
      count_room_video)
      SELECT
        p_project_id,
        ie.station_id,
				ie.exam_id,
				ie.kim,
				ie.count_part,
				ie.count_room,
				ie.count_room_video
      FROM imp_exec ie
      WHERE ie.imp_session_guid = p_imp_session_guid AND ie.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT exec_uniq_idx
      DO UPDATE SET
        kim = EXCLUDED.kim,
	      count_part = EXCLUDED.count_part,
  	    count_room = EXCLUDED.count_room,
    	  count_room_video = EXCLUDED.count_room_video
	RETURNING e.station_id, e.exam_id)
	/* update import status from returning result */
  UPDATE gsc.imp_exec ie
  SET imp_status = 'imported'
  WHERE ie.imp_session_guid = p_imp_session_guid and (ie.station_id, ie.exam_id) in (
  	select i.station_id, i.exam_id
    from imported i
  );
  
  -- return records presence in table but missing from import
  RETURN QUERY
    SELECT s.code AS station_code,
           sub.name || ' ' || ex.exam_date as exam           
    FROM gsc.exec e
         LEFT JOIN gsc.station s ON e.station_id = s.id
         LEFT JOIN gsc.exam ex ON e.exam_id = ex.id
         LEFT JOIN gsc.subject sub ON ex.subject_id = sub.id
     WHERE e.project_id = p_project_id AND
          NOT exists(SELECT NULL
                      FROM gsc.imp_exec ie
                      WHERE ie.imp_session_guid = p_imp_session_guid AND
                            ie.exam_id = e.exam_id AND
                            ie.station_id = e.station_id);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;