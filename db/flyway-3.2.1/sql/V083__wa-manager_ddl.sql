set search_path = gsc,public;


DROP VIEW IF EXISTS gsc.wa_v_exp_schedule;
DROP VIEW IF EXISTS gsc.wa_v_mgr_expert;
DROP VIEW IF EXISTS gsc.wa_v_mgr_event cascade;

CREATE OR REPLACE VIEW gsc.wa_v_mgr_expert (
    id,
    manager_guid,
    name,
    surname,
    patronymic)
AS
SELECT ex.id,
    u.guid as manager_guid,
    ex.name,
    ex.surname,
    ex.patronymic
FROM gsc.expert ex
     JOIN gsc.users u ON u.parent_id = ex.observer_id AND u.type_user = 1;
     
     
CREATE OR REPLACE VIEW gsc.wa_v_mgr_event (
    id,
    ts,
    channel,
    type,
    schedule_id,
    object_type)
AS
SELECT event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type
FROM gsc.event
WHERE event.project_id = gsc.current_project_id();

CREATE OR REPLACE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_mgr_event 
DO INSTEAD (
DELETE FROM gsc.event
  WHERE event.id = old.id;  -- flyway! flyway, Carl!
);

CREATE OR REPLACE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_mgr_event 
DO INSTEAD (
INSERT INTO gsc.event (ts, channel, type, schedule_id, object_type, project_id)
  VALUES (new.ts, new.channel, new.type, new.schedule_id, new.object_type, gsc.current_project_id())
  RETURNING event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type;  -- flyway! flyway, Carl!
);

CREATE OR REPLACE RULE "_UPDATE" AS ON UPDATE TO gsc.wa_v_mgr_event 
DO INSTEAD (
UPDATE gsc.event SET ts = new.ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id, object_type = new.object_type
  WHERE event.id = old.id;  -- flyway! flyway, Carl!
);

CREATE OR REPLACE VIEW gsc.wa_v_exp_schedule (
        guid,
        region_name,
        id,
        period_id,
        monitor_rcoi,
        fixed,
        period_type,
        expert_id )
    AS
    SELECT u.guid,
        r.name,
        sch.id,
        sch.period_id,
        sch.monitor_rcoi,
        sch.fixed,
        sch.period_type,
        sch.expert_id
    FROM gsc.schedule sch
         JOIN gsc.region r ON r.id = sch.region_id
         JOIN gsc.users u ON u.parent_id = sch.expert_id AND u.type_user = 0;
