CREATE OR REPLACE FUNCTION gsc.u_bind_observer (
  p_guid uuid,
  p_observer_id integer
)
RETURNS void AS
$body$
-- Привязывает id менеджера/координатора к юзеру, отвязывает от предыдущего
begin
  update gsc.observer o
  	set user_guid = null
  where o.user_guid = p_guid;
  
  update gsc.observer o
     set user_guid = p_guid
   where o.id = p_observer_id;

  if not found then
    raise exception 'Менеджер/координатор % не существует.', p_observer_id;
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

alter FUNCTION gsc.u_bind_observer (
  p_guid uuid,
  p_observer_id integer
) owner to gsc;