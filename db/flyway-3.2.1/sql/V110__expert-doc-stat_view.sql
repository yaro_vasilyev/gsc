set search_path = gsc,public;

create or replace view gsc.v_expert_doc_stat
as 
  select s.expert_id, 
         count(md.blob) as doc_count
    from gsc.mr_doc as md
    join gsc.schedule as s on s.id = md.schedule_id
   group by s.expert_id;

alter view gsc.v_expert_doc_stat
  owner to gsc;

grant select on gsc.v_expert_doc_stat to s_expert_list;

