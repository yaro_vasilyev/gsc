ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone1 DROP DEFAULT;

ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone1 TYPE VARCHAR(255) COLLATE pg_catalog."default";

ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone1 SET DEFAULT ''::character varying;
	
ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone2 DROP DEFAULT;

ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone2 TYPE VARCHAR(255) COLLATE pg_catalog."default";

ALTER TABLE gsc.rcoi
  ALTER COLUMN chief_phone2 SET DEFAULT ''::character varying;