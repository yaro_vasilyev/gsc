set search_path = gsc,public;

create domain gsc.d_visit_object_type as integer 
  default 1 
  constraint d_visit_object_type_chk check (value in (1, 2, 3, 4));

alter domain gsc.d_visit_object_type 
  owner to gsc;


alter table monitor_result 
  drop column location, 
  drop column ppe_id,
  drop column subject_id,
  add column object_type gsc.d_visit_object_type not null;

alter table monitor_result 
  rename column exam_date to date;

alter table monitor_result 
  add column schedule_detail_id integer;

alter table monitor_result 
  drop constraint monitor_result_schedule_id_fkey,
  drop constraint monitor_result_violation_id_fkey,
  drop constraint monitor_result_violator_id_fkey;

alter table monitor_result 
  add constraint monitor_result_schedule_detail_id_fkey 
    foreign key (schedule_detail_id) references schedule_detail(id) 
      on update cascade on delete restrict,
  add constraint monitor_result_schedule_id_fkey 
    foreign key (schedule_id) references schedule(id) 
      on update cascade on delete restrict,
  add constraint monitor_result_violation_id_fkey 
    foreign key (violation_id) references violation(id) 
      on update cascade on delete restrict,
  add constraint monitor_result_violator_id_fkey 
    foreign key (violator_id) references violator(id) 
      on update cascade on delete restrict;


create table mr_detail (
  id serial primary key,
  monitor_result_id integer not null references monitor_result(id) on update cascade on delete restrict,
  exec_id integer not null references exec(id) on update cascade on delete restrict
);
alter table mr_detail owner to gsc;