CREATE VIEW gsc.wa_v_mgr_event (
    id,
    ts,
    channel,
    type,
    schedule_id,
    object_type,
    project_id)
AS
SELECT event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.project_id
FROM gsc.event;

CREATE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_mgr_event 
DO INSTEAD (
DELETE FROM gsc.event
  WHERE event.id = old.id; -- comment to avoid migration fail for rule
);

CREATE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_mgr_event 
DO INSTEAD (
INSERT INTO gsc.event (ts, channel, type, schedule_id, object_type, project_id)
  VALUES (new.ts, new.channel, new.type, new.schedule_id, new.object_type, new.project_id)
  RETURNING event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.project_id; -- comment to avoid migration fail for rule
);

CREATE RULE "_UPDATE" AS ON UPDATE TO gsc.wa_v_mgr_event 
DO INSTEAD (
UPDATE gsc.event SET ts = new.ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id, object_type = new.object_type, project_id = new.project_id
  WHERE event.id = old.id; -- comment to avoid migration fail for rule
);