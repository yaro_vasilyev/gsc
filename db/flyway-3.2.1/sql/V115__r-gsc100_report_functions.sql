
set search_path = gsc,public;


create or replace function gsc.r_gsc100_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  returns table (station_id       gsc.station.id%type,
                 violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            bigint)
  stable
  language plpgsql
  called on null input
  security definer
as $$
begin
  return query 
  with cte as (
    select distinct
           xm.exam_type, 
           xm.exam_date, 
           xc.station_id, 
           mr.violation_id, 
           mr.violator_id
      from gsc.mr_detail as md
      join gsc.monitor_result as mr on mr.id = md.monitor_result_id
      join gsc.exec as xc on xc.id = md.exec_id
      join gsc.exam as xm on xm.id = xc.exam_id
     where (p_exam_type is null or (p_exam_type is not null and xm.exam_type = p_exam_type))
       and xc.project_id = p_project_id
  )
  select cte.station_id, 
         vn.code as violation_code,
         vn.name as violation_name, 
         count(*) 
    from cte
    left join gsc.violation as vn on vn.id = cte.violation_id
   group by cte.station_id, vn.code, vn.name;
end;
$$;

alter function gsc.r_gsc100_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc100_violations (
    gsc.period.id%type, gsc.exam.exam_type%type)
  to s_repmr_module;


create or replace function gsc.r_gsc100_stations (
    p_project_id            gsc.period.id%type,
    p_exam_type             gsc.exam.exam_type%type)
  returns table (region_code      gsc.region.code%type,
                 region_name      gsc.region.name%type,
                 station_id       gsc.station.id%type,
                 station_code     gsc.station.code%type,
                 station_name     gsc.station.name%type)
  called on null input
  stable
  security definer
  language plpgsql
as $$
begin
  return query 
    select r.code as region_code,
           r.name as region_name,
           s.id as station_id,
           s.code as station_code,
           s.name as station_name
      from gsc.station as s
      join gsc.region as r on r.id = s.region_id
     where s.project_id = p_project_id
       and exists (select f.station_id 
                     from gsc.r_gsc100_violations (p_project_id, p_exam_type) as f(station_id, violation_code, violation_name, count) 
                    where f.station_id = s.id)
     order by region_code, 
           station_code;
end;
$$;

alter function gsc.r_gsc100_stations (
    p_project_id            gsc.period.id%type,
    p_exam_type             gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc100_stations (
      gsc.period.id%type, gsc.exam.exam_type%type) 
  to s_repmr_module;
