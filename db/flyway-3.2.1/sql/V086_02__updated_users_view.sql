set search_path = gsc,public;

create or replace view gsc.v_users 
as
select users.id,
    users.user_login,
    users.rolname,
    users.type_user,
    users.guid
from users;

alter view gsc.v_users
  owner to gsc;