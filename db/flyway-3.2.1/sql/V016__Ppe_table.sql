SET search_path = gsc, public;

--
-- Name: ppe; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE ppe (
    id integer NOT NULL,
    region_id integer NOT NULL,
    exam_date date NOT NULL,
    subject_id integer NOT NULL,
    ppe_type integer DEFAULT 1 NOT NULL,
    code integer DEFAULT 0 NOT NULL,
    name character varying(500) DEFAULT ''::character varying NOT NULL,
    address character varying(500) DEFAULT ''::character varying NOT NULL,
    count_part integer,
    count_room integer,
    count_room_video integer,
    kim integer DEFAULT 0 NOT NULL,
    tom integer DEFAULT 0 NOT NULL,
    project_id integer NOT NULL,
    CONSTRAINT ppe_kim_check CHECK ((kim = ANY (ARRAY[0, 1]))),
    CONSTRAINT ppe_ppe_type_check CHECK ((ppe_type = ANY (ARRAY[1, 2, 3]))),
    CONSTRAINT ppe_tom_check CHECK ((tom = ANY (ARRAY[0, 1])))
);

--
-- Name: ppe_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_pkey PRIMARY KEY (id);

--
-- Name: ppe_project_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_project_id_fkey FOREIGN KEY (project_id) REFERENCES period(id);


--
-- Name: ppe_region_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_region_id_fkey FOREIGN KEY (region_id) REFERENCES region(id);


--
-- Name: ppe_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY ppe
    ADD CONSTRAINT ppe_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);




ALTER TABLE ppe OWNER TO gsc;

--
-- Name: ppe_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE ppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ppe_id_seq OWNER TO gsc;

--
-- Name: ppe_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE ppe_id_seq OWNED BY ppe.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY ppe ALTER COLUMN id SET DEFAULT nextval('ppe_id_seq'::regclass);
