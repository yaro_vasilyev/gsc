

set search_path = gsc,public;


alter table gsc.subject
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger subject_bu before update
  on gsc.subject for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_subject
as
  select * from gsc.subject;

alter view v_subject
  owner to gsc;


create or replace function gsc.subject_insert (
    code      gsc.subject.code%type,
    name      gsc.subject.name%type)
  returns table (id gsc.subject.id%type, ts gsc.subject.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id      gsc.subject.id%type;
  l_ts      gsc.subject.ts%type;
begin
  insert into gsc.subject (
              code,
              name)
      values (code,
              name)
    returning subject.id,
              subject.ts
         into l_id,
              l_ts;

  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.subject_insert (
    code      gsc.subject.code%type,
    name      gsc.subject.name%type)
  owner to gsc;


create or replace function gsc.subject_update (
    p_id        gsc.subject.id%type,
    new_code    gsc.subject.code%type,
    new_name    gsc.subject.name%type,
    old_ts      gsc.subject.ts%type)
  returns table (ts gsc.subject.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.subject.ts%type;
begin
  update gsc.subject
     set code = new_code,
         name = new_name
   where id = p_id
     and subject.ts = old_ts
         returning subject.ts into l_ts;

  if not found then
    select gsc.err_concurrency_control('Предмет');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.subject_update (
    p_id        gsc.subject.id%type,
    new_code    gsc.subject.code%type,
    new_name    gsc.subject.name%type,
    old_ts      gsc.subject.ts%type)
  owner to gsc;


create or replace function gsc.subject_delete (p_id gsc.subject.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.subject
        where id = p_id;

  if not found then
    select gsc.err_concurrency_control('Предмет');
  end if;
end;
$$;

alter function gsc.subject_delete (p_id gsc.subject.id%type)
  owner to gsc;

