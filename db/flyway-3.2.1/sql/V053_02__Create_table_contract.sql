CREATE TABLE gsc.contract (
  period_type INTEGER NOT NULL,
  expert_id INTEGER NOT NULL,
  contract_type gsc.contract_type NOT NULL,
  number VARCHAR(30) NOT NULL,
  date_from DATE NOT NULL,
  date_to DATE NOT NULL,
  summ MONEY,
  route VARCHAR(500),
  doc BYTEA,
  CONSTRAINT contract_pkey PRIMARY KEY(number),
  CONSTRAINT contract_summ_chk CHECK (((contract_type = 'paid'::gsc.contract_type) AND ((summ)::numeric > (0)::numeric)) OR ((contract_type = 'unpaid'::gsc.contract_type) AND (summ IS NULL))),
  CONSTRAINT contract_fk FOREIGN KEY (number)
    REFERENCES gsc.contract_info(number)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE
) 
WITH (oids = false);

COMMENT ON TABLE gsc.contract
IS 'Contracts';

COMMENT ON COLUMN gsc.contract.period_type
IS 'Period type';

COMMENT ON COLUMN gsc.contract.expert_id
IS 'Expert';

COMMENT ON COLUMN gsc.contract.contract_type
IS 'Contract type';

COMMENT ON COLUMN gsc.contract.number
IS 'Contract number';

COMMENT ON COLUMN gsc.contract.date_from
IS 'Contract start date.';

COMMENT ON COLUMN gsc.contract.date_to
IS 'Contract end date.';

COMMENT ON COLUMN gsc.contract.summ
IS 'Contruct paid summ';

COMMENT ON COLUMN gsc.contract.route
IS 'Contracter route';

COMMENT ON COLUMN gsc.contract.doc
IS 'Generated contract document (file)';

COMMENT ON CONSTRAINT contract_summ_chk ON gsc.contract
IS 'For paid contracts summ must be greater than 0 and for unpaid summ must be null';