SET search_path = gsc, public;

--
-- Name: imp_academic_degree; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--
CREATE TABLE imp_academic_degree (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL
);

ALTER TABLE imp_academic_degree OWNER TO gsc;

--
-- Name: imp_academic_degree_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY imp_academic_degree
    ADD CONSTRAINT imp_academic_degree_pkey PRIMARY KEY (imp_session_guid, code);

COMMENT ON TABLE imp_academic_degree IS 'Academic degree import table.';

COMMENT ON COLUMN imp_academic_degree.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';
COMMENT ON COLUMN imp_academic_degree.imp_user IS 'User imported row.';
COMMENT ON COLUMN imp_academic_degree.imp_datetime IS 'Import timestamp.';
COMMENT ON COLUMN imp_academic_degree.imp_status IS 'Import status';
COMMENT ON COLUMN imp_academic_degree.code IS 'Acedemic degree code.';
COMMENT ON COLUMN imp_academic_degree.name IS 'Academic degree name.';

--
-- Name: import_academic_degree(uuid); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION import_academic_degree(p_imp_session_guid uuid) RETURNS TABLE(code character varying, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes VARCHAR [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right
  
  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT iad.code
                      FROM gsc.imp_academic_degree iad
                      WHERE iad.imp_session_guid = p_imp_session_guid
                      GROUP BY iad.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/
  
  -- import rows
  WITH imported AS (
    INSERT INTO academic_degree AS ad (code, name)
      SELECT
        iad.code imp_code,
        iad.name
      FROM imp_academic_degree iad
      WHERE iad.imp_session_guid = p_imp_session_guid AND iad.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT academic_degree_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING ad.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_academic_degree iad
  SET imp_status = 'imported'
  WHERE iad.imp_session_guid = p_imp_session_guid;
  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 ad.code,
                 ad.name
               FROM gsc.academic_degree ad
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_academic_degree iad
                                WHERE iad.imp_session_guid = p_imp_session_guid AND
                                      ad.code = iad.code);
END;
$$;

ALTER FUNCTION gsc.import_academic_degree(p_imp_session_guid uuid) OWNER TO gsc;


--
-- Name: import_row_academic_degree(uuid, character varying, character varying); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION import_row_academic_degree(imp_session_guid uuid, code character varying, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO imp_academic_degree (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;

ALTER FUNCTION gsc.import_row_academic_degree(imp_session_guid uuid, code character varying, name character varying) OWNER TO gsc;
