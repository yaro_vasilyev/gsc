SET search_path = gsc, public;

--
-- Name: period; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--
CREATE TABLE period (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    CONSTRAINT period_status_check CHECK ((status = ANY (ARRAY[1, 2])))
);

--
-- Name: period_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY period
    ADD CONSTRAINT period_pkey PRIMARY KEY (id);



ALTER TABLE period OWNER TO gsc;


--
-- Name: period_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE period_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE period_seq OWNER TO gsc;

--
-- Name: period_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE period_seq OWNED BY period.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY period ALTER COLUMN id SET DEFAULT nextval('period_seq'::regclass);



--
-- Name: period_delete(integer, character varying, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION period_delete(pid integer, ptitle character varying, pstatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin
    delete from gsc.period
     where id = pid
       and title = ptitle
       and status = pstatus;
  end;
$$;

ALTER FUNCTION gsc.period_delete(pid integer, ptitle character varying, pstatus integer) OWNER TO gsc;


--
-- Name: period_insert(character varying, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION period_insert(ptitle character varying, pstatus integer, OUT pid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  declare
    ret integer;
  begin
    insert into gsc.period(title, status)
    values (ptitle, pstatus);
    select lastval() into pid;
  end;
$$;

ALTER FUNCTION gsc.period_insert(ptitle character varying, pstatus integer, OUT pid integer) OWNER TO gsc;


--
-- Name: period_update(integer, character varying, integer, integer, character varying, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION period_update(pid integer, ptitle character varying, pstatus integer, pid_original integer, 
                              ptitle_original character varying, pstatus_orginal integer) 
    RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin
    update gsc.period
       set id = pid,
           title = ptitle,
           status = pstatus
     where id = pid_original
       and title = ptitle_original
       and status = pstatus_original;
  end;
$$;

ALTER FUNCTION gsc.period_update(pid integer, ptitle character varying, pstatus integer, pid_original integer, 
                                 ptitle_original character varying, pstatus_orginal integer) OWNER TO gsc;
