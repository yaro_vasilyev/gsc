set search_path = gsc,public;

----------------------------------------------------------------------------------------------------------
-- Add column for project
----------------------------------------------------------------------------------------------------------

alter table gsc.oiv_gek
  add column project_id integer,
  add constraint oiv_gek_period_fkey foreign key (project_id) references gsc.period (id);


update gsc.oiv_gek
   set project_id = (select id
                       from gsc.period
                      where status = 1);

alter table gsc.oiv_gek
  alter column project_id set not null;

drop view gsc.v_oiv_gek;


create or replace view gsc.v_oiv_gek
as
  select * from gsc.oiv_gek;

alter view gsc.v_oiv_gek
  owner to gsc;


----------------------------------------------------------------------------------------------------------
-- CRUD functions
----------------------------------------------------------------------------------------------------------

drop function gsc.oiv_gek_insert (
    region_id             gsc.oiv_gek.region_id%type,
    oiv_fio               gsc.oiv_gek.oiv_fio%type,
    oiv_wp                gsc.oiv_gek.oiv_wp%type,
    oiv_post              gsc.oiv_gek.oiv_post%type,
    oiv_phone1            gsc.oiv_gek.oiv_phone1%type,
    oiv_email             gsc.oiv_gek.oiv_email%type,
    gek_fio               gsc.oiv_gek.gek_fio%type,
    gek_wp                gsc.oiv_gek.gek_wp%type,
    gek_post              gsc.oiv_gek.gek_post%type,
    gek_phone1            gsc.oiv_gek.gek_phone1%type,
    gek_email             gsc.oiv_gek.gek_email%type,
    gia_fio               gsc.oiv_gek.gia_fio%type,
    gia_wp                gsc.oiv_gek.gia_wp%type,
    gia_post              gsc.oiv_gek.gia_post%type,
    gia_phone1            gsc.oiv_gek.gia_phone1%type,
    gia_email             gsc.oiv_gek.gia_email%type,
    nadzor_fio            gsc.oiv_gek.nadzor_fio%type,
    nadzor_wp             gsc.oiv_gek.nadzor_wp%type,
    nadzor_post           gsc.oiv_gek.nadzor_post%type,
    nadzor_phone1         gsc.oiv_gek.nadzor_phone1%type,
    nadzor_email          gsc.oiv_gek.nadzor_email%type,
    oiv_phone2            gsc.oiv_gek.oiv_phone2%type,
    gek_phone2            gsc.oiv_gek.gek_phone2%type,
    gia_phone2            gsc.oiv_gek.gia_phone2%type,
    nadzor_phone2         gsc.oiv_gek.nadzor_phone2%type);


create or replace function gsc.oiv_gek_insert (
    region_id             gsc.oiv_gek.region_id%type,
    p_project_id          gsc.period.id%type,
    oiv_fio               gsc.oiv_gek.oiv_fio%type,
    oiv_wp                gsc.oiv_gek.oiv_wp%type,
    oiv_post              gsc.oiv_gek.oiv_post%type,
    oiv_phone1            gsc.oiv_gek.oiv_phone1%type,
    oiv_email             gsc.oiv_gek.oiv_email%type,
    gek_fio               gsc.oiv_gek.gek_fio%type,
    gek_wp                gsc.oiv_gek.gek_wp%type,
    gek_post              gsc.oiv_gek.gek_post%type,
    gek_phone1            gsc.oiv_gek.gek_phone1%type,
    gek_email             gsc.oiv_gek.gek_email%type,
    gia_fio               gsc.oiv_gek.gia_fio%type,
    gia_wp                gsc.oiv_gek.gia_wp%type,
    gia_post              gsc.oiv_gek.gia_post%type,
    gia_phone1            gsc.oiv_gek.gia_phone1%type,
    gia_email             gsc.oiv_gek.gia_email%type,
    nadzor_fio            gsc.oiv_gek.nadzor_fio%type,
    nadzor_wp             gsc.oiv_gek.nadzor_wp%type,
    nadzor_post           gsc.oiv_gek.nadzor_post%type,
    nadzor_phone1         gsc.oiv_gek.nadzor_phone1%type,
    nadzor_email          gsc.oiv_gek.nadzor_email%type,
    oiv_phone2            gsc.oiv_gek.oiv_phone2%type,
    gek_phone2            gsc.oiv_gek.gek_phone2%type,
    gia_phone2            gsc.oiv_gek.gia_phone2%type,
    nadzor_phone2         gsc.oiv_gek.nadzor_phone2%type)
  returns table (id gsc.oiv_gek.id%type, ts gsc.oiv_gek.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id        gsc.oiv_gek.id%type;
  l_ts        gsc.oiv_gek.ts%type;
begin
  insert into gsc.oiv_gek (
              region_id,
              project_id,
              oiv_fio,
              oiv_wp,
              oiv_post,
              oiv_phone1,
              oiv_email,
              gek_fio,
              gek_wp,
              gek_post,
              gek_phone1,
              gek_email,
              gia_fio,
              gia_wp,
              gia_post,
              gia_phone1,
              gia_email,
              nadzor_fio,
              nadzor_wp,
              nadzor_post,
              nadzor_phone1,
              nadzor_email,
              oiv_phone2,
              gek_phone2,
              gia_phone2,
              nadzor_phone2)
      values (region_id,
              p_project_id,
              oiv_fio,
              oiv_wp,
              oiv_post,
              oiv_phone1,
              oiv_email,
              gek_fio,
              gek_wp,
              gek_post,
              gek_phone1,
              gek_email,
              gia_fio,
              gia_wp,
              gia_post,
              gia_phone1,
              gia_email,
              nadzor_fio,
              nadzor_wp,
              nadzor_post,
              nadzor_phone1,
              nadzor_email,
              oiv_phone2,
              gek_phone2,
              gia_phone2,
              nadzor_phone2)
    returning oiv_gek.id,
              oiv_gek.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.oiv_gek_insert (
    region_id             gsc.oiv_gek.region_id%type,
    p_project_id          gsc.period.id%type,
    oiv_fio               gsc.oiv_gek.oiv_fio%type,
    oiv_wp                gsc.oiv_gek.oiv_wp%type,
    oiv_post              gsc.oiv_gek.oiv_post%type,
    oiv_phone1            gsc.oiv_gek.oiv_phone1%type,
    oiv_email             gsc.oiv_gek.oiv_email%type,
    gek_fio               gsc.oiv_gek.gek_fio%type,
    gek_wp                gsc.oiv_gek.gek_wp%type,
    gek_post              gsc.oiv_gek.gek_post%type,
    gek_phone1            gsc.oiv_gek.gek_phone1%type,
    gek_email             gsc.oiv_gek.gek_email%type,
    gia_fio               gsc.oiv_gek.gia_fio%type,
    gia_wp                gsc.oiv_gek.gia_wp%type,
    gia_post              gsc.oiv_gek.gia_post%type,
    gia_phone1            gsc.oiv_gek.gia_phone1%type,
    gia_email             gsc.oiv_gek.gia_email%type,
    nadzor_fio            gsc.oiv_gek.nadzor_fio%type,
    nadzor_wp             gsc.oiv_gek.nadzor_wp%type,
    nadzor_post           gsc.oiv_gek.nadzor_post%type,
    nadzor_phone1         gsc.oiv_gek.nadzor_phone1%type,
    nadzor_email          gsc.oiv_gek.nadzor_email%type,
    oiv_phone2            gsc.oiv_gek.oiv_phone2%type,
    gek_phone2            gsc.oiv_gek.gek_phone2%type,
    gia_phone2            gsc.oiv_gek.gia_phone2%type,
    nadzor_phone2         gsc.oiv_gek.nadzor_phone2%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------------

drop function gsc.oiv_gek_update (
    p_id                    gsc.oiv_gek.id%type,
    new_region_id           gsc.oiv_gek.region_id%type,
    new_oiv_fio             gsc.oiv_gek.oiv_fio%type,
    new_oiv_wp              gsc.oiv_gek.oiv_wp%type,
    new_oiv_post            gsc.oiv_gek.oiv_post%type,
    new_oiv_phone1          gsc.oiv_gek.oiv_phone1%type,
    new_oiv_email           gsc.oiv_gek.oiv_email%type,
    new_gek_fio             gsc.oiv_gek.gek_fio%type,
    new_gek_wp              gsc.oiv_gek.gek_wp%type,
    new_gek_post            gsc.oiv_gek.gek_post%type,
    new_gek_phone1          gsc.oiv_gek.gek_phone1%type,
    new_gek_email           gsc.oiv_gek.gek_email%type,
    new_gia_fio             gsc.oiv_gek.gia_fio%type,
    new_gia_wp              gsc.oiv_gek.gia_wp%type,
    new_gia_post            gsc.oiv_gek.gia_post%type,
    new_gia_phone1          gsc.oiv_gek.gia_phone1%type,
    new_gia_email           gsc.oiv_gek.gia_email%type,
    new_nadzor_fio          gsc.oiv_gek.nadzor_fio%type,
    new_nadzor_wp           gsc.oiv_gek.nadzor_wp%type,
    new_nadzor_post         gsc.oiv_gek.nadzor_post%type,
    new_nadzor_phone1       gsc.oiv_gek.nadzor_phone1%type,
    new_nadzor_email        gsc.oiv_gek.nadzor_email%type,
    new_oiv_phone2          gsc.oiv_gek.oiv_phone2%type,
    new_gek_phone2          gsc.oiv_gek.gek_phone2%type,
    new_gia_phone2          gsc.oiv_gek.gia_phone2%type,
    new_nadzor_phone2       gsc.oiv_gek.nadzor_phone2%type,
    p_ts                    gsc.oiv_gek.ts%type);

create or replace function gsc.oiv_gek_update (
    p_id                    gsc.oiv_gek.id%type,
    new_region_id           gsc.oiv_gek.region_id%type,
    new_project_id          gsc.period.id%type,
    new_oiv_fio             gsc.oiv_gek.oiv_fio%type,
    new_oiv_wp              gsc.oiv_gek.oiv_wp%type,
    new_oiv_post            gsc.oiv_gek.oiv_post%type,
    new_oiv_phone1          gsc.oiv_gek.oiv_phone1%type,
    new_oiv_email           gsc.oiv_gek.oiv_email%type,
    new_gek_fio             gsc.oiv_gek.gek_fio%type,
    new_gek_wp              gsc.oiv_gek.gek_wp%type,
    new_gek_post            gsc.oiv_gek.gek_post%type,
    new_gek_phone1          gsc.oiv_gek.gek_phone1%type,
    new_gek_email           gsc.oiv_gek.gek_email%type,
    new_gia_fio             gsc.oiv_gek.gia_fio%type,
    new_gia_wp              gsc.oiv_gek.gia_wp%type,
    new_gia_post            gsc.oiv_gek.gia_post%type,
    new_gia_phone1          gsc.oiv_gek.gia_phone1%type,
    new_gia_email           gsc.oiv_gek.gia_email%type,
    new_nadzor_fio          gsc.oiv_gek.nadzor_fio%type,
    new_nadzor_wp           gsc.oiv_gek.nadzor_wp%type,
    new_nadzor_post         gsc.oiv_gek.nadzor_post%type,
    new_nadzor_phone1       gsc.oiv_gek.nadzor_phone1%type,
    new_nadzor_email        gsc.oiv_gek.nadzor_email%type,
    new_oiv_phone2          gsc.oiv_gek.oiv_phone2%type,
    new_gek_phone2          gsc.oiv_gek.gek_phone2%type,
    new_gia_phone2          gsc.oiv_gek.gia_phone2%type,
    new_nadzor_phone2       gsc.oiv_gek.nadzor_phone2%type,
    p_ts                    gsc.oiv_gek.ts%type)
  returns table (ts         gsc.oiv_gek.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts          gsc.oiv_gek.ts%type;
begin
  update gsc.oiv_gek
     set region_id = new_region_id,
         project_id = new_project_id,
         oiv_fio = new_oiv_fio,
         oiv_wp = new_oiv_wp,
         oiv_post = new_oiv_post,
         oiv_phone1 = new_oiv_phone1,
         oiv_email = new_oiv_email,
         gek_fio = new_gek_fio,
         gek_wp = new_gek_wp,
         gek_post = new_gek_post,
         gek_phone1 = new_gek_phone1,
         gek_email = new_gek_email,
         gia_fio = new_gia_fio,
         gia_wp = new_gia_wp,
         gia_post = new_gia_post,
         gia_phone1 = new_gia_phone1,
         gia_email = new_gia_email,
         nadzor_fio = new_nadzor_fio,
         nadzor_wp = new_nadzor_wp,
         nadzor_post = new_nadzor_post,
         nadzor_phone1 = new_nadzor_phone1,
         nadzor_email = new_nadzor_email,
         oiv_phone2 = new_oiv_phone2,
         gek_phone2 = new_gek_phone2,
         gia_phone2 = new_gia_phone2,
         nadzor_phone2 = new_nadzor_phone2
   where id = p_id
     and oiv_gek.ts = p_ts
         returning oiv_gek.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Информация об ОИВ/ГЭК');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.oiv_gek_update (
    p_id                    gsc.oiv_gek.id%type,
    new_region_id           gsc.oiv_gek.region_id%type,
    new_project_id          gsc.period.id%type,
    new_oiv_fio             gsc.oiv_gek.oiv_fio%type,
    new_oiv_wp              gsc.oiv_gek.oiv_wp%type,
    new_oiv_post            gsc.oiv_gek.oiv_post%type,
    new_oiv_phone1          gsc.oiv_gek.oiv_phone1%type,
    new_oiv_email           gsc.oiv_gek.oiv_email%type,
    new_gek_fio             gsc.oiv_gek.gek_fio%type,
    new_gek_wp              gsc.oiv_gek.gek_wp%type,
    new_gek_post            gsc.oiv_gek.gek_post%type,
    new_gek_phone1          gsc.oiv_gek.gek_phone1%type,
    new_gek_email           gsc.oiv_gek.gek_email%type,
    new_gia_fio             gsc.oiv_gek.gia_fio%type,
    new_gia_wp              gsc.oiv_gek.gia_wp%type,
    new_gia_post            gsc.oiv_gek.gia_post%type,
    new_gia_phone1          gsc.oiv_gek.gia_phone1%type,
    new_gia_email           gsc.oiv_gek.gia_email%type,
    new_nadzor_fio          gsc.oiv_gek.nadzor_fio%type,
    new_nadzor_wp           gsc.oiv_gek.nadzor_wp%type,
    new_nadzor_post         gsc.oiv_gek.nadzor_post%type,
    new_nadzor_phone1       gsc.oiv_gek.nadzor_phone1%type,
    new_nadzor_email        gsc.oiv_gek.nadzor_email%type,
    new_oiv_phone2          gsc.oiv_gek.oiv_phone2%type,
    new_gek_phone2          gsc.oiv_gek.gek_phone2%type,
    new_gia_phone2          gsc.oiv_gek.gia_phone2%type,
    new_nadzor_phone2       gsc.oiv_gek.nadzor_phone2%type,
    p_ts                    gsc.oiv_gek.ts%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------------
-- That's all folks!
----------------------------------------------------------------------------------------------------------
