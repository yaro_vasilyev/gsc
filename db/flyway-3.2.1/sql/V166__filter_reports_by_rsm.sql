set search_path = gsc,public;

--------------------------------------------------------------------------------
-- Removed check for s_users_list from expert selection. Due to the presense of this role almost in every other role
create or replace function gsc.get_experts (
    p_rolname       name)
  returns     setof gsc.expert
  language    plpgsql
  security    definer
  stable
as
$$
declare
  l_user_guid gsc.users.guid%type;
begin
  
  begin
    l_user_guid := gsc.get_user_guid(p_rolname);
  exception
    when others then
      raise notice 'User % is not registered in gsc.users', p_rolname;
      return; -- nope, user is not in the system
  end;
  
  if gsc._xxx_roles_any(p_rolname, 
        (gsc.roles ()).admin, 
        (gsc.roles ()).sysadmin, 
        (gsc.roles ()).boss, 
        (gsc.roles ()).an_lead, 
        (gsc.roles ()).an, 
        (gsc.roles ()).manager) then
    return query 
      select * 
        from gsc.expert; -- EVERRRRRYYYYYOOOOONNNNEEEE!!!
  elsif gsc._xxx_roles_all(p_rolname, (gsc.roles ()).rsm) then
    return query 
      select e.* 
        from gsc.expert as e 
       where e.expert_kind_social; --only rsm people
  else --if gsc._xxx_roles_all(p_rolname, gsc.role_expert()) then
  -- if select can find a record with user guid then it's expert himself!
    return query 
      select * 
        from gsc.expert 
       where user_guid = l_user_guid; -- only self
  /*else
    return; -- noone here*/
  end if;
  return;
end;
$$;

alter function gsc.get_experts (name)
  owner to gsc;

--------------------------------------------------------------------------------
create or replace function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  returns table (
    exam_date           date,
    region_code         gsc.region.code%type,
    region_name         gsc.region.name%type,
    region_zone         gsc.region.zone%type,
    subjects            text,
    expert_fio          text,
    expert_kind         gsc.expert.kind%type,
    ppe                 text,
    violation_code      gsc.violation.code%type,
    violation_name      gsc.violation.name%type,
    violator_code       gsc.violator.code%type,
    violator_name       gsc.violator.name%type,
    note                gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--    Function for GSC-095 report
-- PARAMETERS:
--    p_project_id        REQUIRED. Project ID
--    p_period_type       OPTIONAL. Exam's period type
--    p_exam_type         OPTIONAL. Exam's exam type
--    p_date              OPTIONAL. Filter date
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject as subj on subj.id = exam.subject_id
       where subj.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and exam.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and exam.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and exam.exam_date = p_date))
         union all -- другие результаты мониторинга (не только c запольненными ППЭ)
      select mr1.id,
             null,
             mr1.date
        from gsc.monitor_result mr1
        join gsc.schedule as sch1 on sch1.id = mr1.schedule_id -- ограничить по проекту для
        join gsc.v_expert on v_expert.id = sch1.expert_id
       where not exists (select * from gsc.mr_detail mrd1 where mrd1.monitor_result_id = mr1.id)
         and v_expert.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and sch1.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and mr1.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and mr1.date = p_date))
    ),
    cte_subjects as (
      select mr.monitor_result_id, array_to_string (array_agg (j.name order by j.name), ', ') as subjects
        from gsc.mr_detail as mrd
        join cte_station_helper as mr on mr.monitor_result_id = mrd.monitor_result_id
        join gsc.exec as x on x.id = mrd.exec_id
        join gsc.exam as e on e.id = x.exam_id
        join gsc.subject as j on j.id = e.subject_id
       where j.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and e.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and e.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and e.exam_date = p_date))
       group by mr.monitor_result_id
    )
    select cte_station_helper.exam_date,
           r.code as region_code,
           r.name as region_name,
           r.zone as region_zone,
           cte_subjects.subjects,
           concat_ws (' ', ex.surname, ex.name, ex.patronymic) as expert_fio,
           ex.kind as expert_kind,
           case mr.object_type
              when 1 then coalesce (st.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
              else '-'
           end as ppe,
           vn.code as violation_code,
           vn.name as violation_name,
           vr.code as violator_code,
           vr.name as violator_name,
           mr.note
      from cte_station_helper
      left join cte_subjects on cte_subjects.monitor_result_id = cte_station_helper.monitor_result_id
      left join gsc.station as st on st.id = cte_station_helper.station_id
      join gsc.monitor_result as mr on mr.id = cte_station_helper.monitor_result_id
      join gsc.schedule as sch on sch.id = mr.schedule_id
      join gsc.region as r on r.id = sch.region_id
      join gsc.v_expert as ex on ex.id = sch.expert_id
      left join gsc.violation as vn on vn.id = mr.violation_id
      left join gsc.violator as vr on vr.id = mr.violator_id
     where ex.period_id = p_project_id
       and (p_period_type is null or (p_period_type is not null and sch.period_type = p_period_type))
       and (p_exam_type is null or (p_exam_type is not null and mr.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  owner to gsc;


--------------------------------------------------------------------------------
create or replace function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  returns table (
    Date            gsc.monitor_result.date%type,
    RegionCode      gsc.region.code%type,
    RegionName      gsc.region.name%type,
    ExpertFullName  text,
    ExpertKind      gsc.expert.kind%type,
    StationCode     gsc.station.code%type,
    ViolationPpe    gsc.violation.name%type,
    ViolationOther  gsc.violation.name%type,
    Note            gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
begin
  return query
    select r.date as Date, 
           rg.code as RegionCode, 
           rg.name as RegionName, 
           concat_ws(' ', x.surname, x.name, x.patronymic) as ExpertFullName,
           x.kind as ExpertKind,
           s.code as StationCode,
           case when r.object_type = 1 then vn.name else null end as ViolationPpe,
           case when r.object_type <> 1 then vn.name else null end as ViolationOther,
           r.note
      from gsc.monitor_result as r
      left join (select distinct
                        d.monitor_result_id, e.station_id 
                   from gsc.mr_detail as d
                   join gsc.exec as e on e.id = d.exec_id) as station_helper on station_helper.monitor_result_id = r.id
      left join gsc.station as s on s.id = station_helper.station_id
      join gsc.schedule as sch on sch.id = r.schedule_id
      join gsc.v_expert as x on x.id = sch.expert_id
      join gsc.violation as vn on vn.id = r.violation_id
      join gsc.region as rg on rg.id = sch.region_id
     where x.period_id = p_project_id and 
           (p_period_type is null or p_period_type is not null and sch.period_type = p_period_type) and
           (p_exam_type is null or p_exam_type is not null and r.exam_type = p_exam_type) and
           (p_date is null or p_date is not null and r.date = p_date)
     order by r.date, rg.name, ExpertFullName, s.code, ViolationPpe;
end;
$$;

alter function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  owner to gsc;

--------------------------------------------------------------------------------

create or replace function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  returns table (     region_name        gsc.region.name%type,
                      date               gsc.monitor_result.date%type,
                      subjects           text,
                      station_etc        text,
                      violation_code     gsc.violation.code%type,
                      violator_code      gsc.violator.code%type,
                      note               gsc.monitor_result.note%type)
  stable
  called on null input
  security definer
  language plpgsql
as $$
-- 
-- DESCRIPTION:
--    Function for report GSC-96
-- PARAMETERS
--    p_project_id        REQUIRED. Current project
--    p_exam_type         OPTIONAL. EGE/GVE/OGE
--    p_date              OPTIONAL. Date
--
begin
  return query
    with cte_mr_station as (
      select distinct
             exec.station_id,
             mr_detail.monitor_result_id
        from gsc.mr_detail
        join gsc.exec on mr_detail.exec_id = exec.id
    ),
    cte_mr_subjects as (
      select monitor_result.id as monitor_result_id,
             array_to_string (array_agg (subject.name order by subject.name), ', ') as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        join gsc.v_expert on v_expert.id = schedule.expert_id
        left join gsc.mr_detail on monitor_result.id = mr_detail.monitor_result_id
        left join gsc.exec on mr_detail.exec_id = exec.id
        left join gsc.exam on exec.exam_id = exam.id
        left join gsc.subject on exam.subject_id = subject.id
       where monitor_result.object_type = 1
         and v_expert.period_id = p_project_id
         and subject.project_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       group by monitor_result.id
             union all
      select monitor_result.id as monitor_result_id, 
             null as subjects
        from gsc.monitor_result
        join gsc.schedule on schedule.id = monitor_result.schedule_id
        join gsc.v_expert on v_expert.id = schedule.expert_id
       where monitor_result.object_type <> 1
         and v_expert.period_id = p_project_id
         and (p_date is null or (p_date is not null and monitor_result.date = p_date))
    )
    select region.name as region_name,
           monitor_result.date,
           cte_mr_subjects.subjects,
           case monitor_result.object_type
              when 1 then coalesce (station.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violator.code as violator_code,
           monitor_result.note
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.v_expert on v_expert.id = schedule.expert_id
      join gsc.region on region.id = schedule.region_id
      left join cte_mr_subjects on cte_mr_subjects.monitor_result_id = monitor_result.id
      left join cte_mr_station on cte_mr_station.monitor_result_id = monitor_result.id
      left join gsc.station on station.id = cte_mr_station.station_id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.violator on violator.id = monitor_result.violator_id
     where v_expert.period_id = p_project_id
       and (p_date is null or (p_date is not null and monitor_result.date = p_date))
       and (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc096_monitor_results_short (
    p_project_id      gsc.period.id%type,
    p_exam_type       gsc.monitor_result.exam_type%type,
    p_date            gsc.monitor_result.date%type)
  owner to gsc;

--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc097_count_mr_ppe (
          p_project_id     gsc.period.id%type)
  returns table (
          exam_type        gsc.d_exam_type,
          violations       numeric,
          schedules        numeric)
  stable
  returns null on null input
  security definer
  language plpgsql
as $$
begin
  return query
    with cte_violations as (
      select inner_sql.exam_type,
             inner_sql.cnt
        from (
              select 1::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 1) as v
               where v.station_id is not null
              union
              select 2::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 2) as v
               where v.station_id is not null
              union
              select 3::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 3) as v
               where v.station_id is not null
             ) as inner_sql
    ),
    cte_schedules as (
      select inner_sql.exam_type,
             count (inner_sql.*)::numeric as cnt
        from (
              select distinct
                     exam.exam_type,
                     exam.exam_date,
                     station.id as station_id,
                     schedule_detail.schedule_id
                from gsc.exec
                join gsc.exam on exam.id = exec.exam_id
                join gsc.subject on subject.id = exam.subject_id
                join gsc.station on station.id = exec.station_id
                join gsc.schedule_detail on schedule_detail.station_id = station.id and schedule_detail.exam_date = exam.exam_date
                join gsc.schedule on schedule.id = schedule_detail.schedule_id
                join gsc.v_expert on v_expert.id = schedule.expert_id
               where station.project_id = p_project_id
                 and subject.project_id = p_project_id
              ) as inner_sql
        group by
              inner_sql.exam_type
    )
    select et::gsc.d_exam_type as exam_type,
           coalesce (cte_violations.cnt, 0) as violations,
           coalesce (cte_schedules.cnt, 0) as schedules
      from unnest (array [1, 2, 3]) as et
      left join cte_violations on cte_violations.exam_type = et
      left join cte_schedules on cte_schedules.exam_type = et
     order by et;
end;
$$;

alter function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  owner to gsc;
--------------------------------------------------------------------------------------------------------

create or replace function gsc.r_station_violations (
    p_project_id integer,
    p_exam_type gsc.d_exam_type)
  returns table (
          date date, 
          violation_id integer, 
          violator_id integer, 
          station_id integer, 
          region_id integer, 
          cnt bigint) 
  called on null input
  language plpgsql 
  stable
  security definer
as
$$
-- возвращает статистику по нарушениям без дубликатов. 
-- нарушения на РЦОИ/ППЗ/КК попадают в одну категорию (строки с station_id = null)
begin
  return query
    select group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id,
           max (group_by_expert.cnt) as cnt
      from (select monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id,
                   schedule_detail.station_id,
                   schedule.region_id,
                   count(*) as cnt 
              from gsc.monitor_result
              join gsc.schedule on monitor_result.schedule_id = schedule.id
              join gsc.v_expert on v_expert.id = schedule.expert_id
         left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id
             where (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type))
               and v_expert.period_id = p_project_id
             group by monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id, 
                   schedule_detail.station_id,
                   schedule.region_id) as group_by_expert
     group by group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id;
end;
$$;

alter function gsc.r_station_violations(integer, gsc.d_exam_type)
  owner to gsc;

--------------------------------------------------------------------------------------------------------

create or replace function gsc.monitor_result_subjects(
                p_project_id              integer) 
returns table (
                monitor_result_id         integer, 
                subject_names             text, 
                subject_codes             text)
  language plpgsql 
  stable 
  security definer
  strict
as $$
begin
  return query
    select monitor_result.id as monitor_result_id,
           array_to_string (array_agg (subject.name order by subject.name), ', ') as subject_names,
           array_to_string (array_agg (subject.code order by subject.name), ', ') as subject_codes
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.v_expert on v_expert.id = schedule.expert_id
      left join gsc.mr_detail on mr_detail.monitor_result_id = monitor_result.id
      left join gsc.exec on exec.id = mr_detail.exec_id
      left join gsc.exam on exam.id = exec.exam_id
      left join gsc.subject on subject.id = exam.subject_id and subject.project_id = p_project_id
     where v_expert.period_id = p_project_id
     group by monitor_result.id;
end;
$$;


alter function gsc.monitor_result_subjects(integer) owner to gsc;

--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where subject.project_id = p_project_id
    ),
    cte_count_by_expert_etc as (
      select cte_station_helper.exam_date as date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code as station_code, 
             count(*) as cnt
        from gsc.monitor_result as mr
        join gsc.schedule as sch on sch.id = mr.schedule_id
        join gsc.v_expert on v_expert.id = sch.expert_id
        left join cte_station_helper on mr.id = cte_station_helper.monitor_result_id
        left join gsc.station as s on s.id = cte_station_helper.station_id
       where mr.exam_type = p_exam_type 
         and v_expert.period_id = p_project_id
         and (p_exam_date is null or (p_exam_date is not null and cte_station_helper.exam_date = p_exam_date))
         and (p_station_code is null or (p_station_code is not null and s.code = p_station_code))
       group by cte_station_helper.exam_date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code
    ),
    cte_max_by_vnvr_station as (
      select i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code,
             max (i.cnt) as  max_cnt
        from cte_count_by_expert_etc as i
       group by
             i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code
    )
    select vn.code, vn.name, sum(data.max_cnt) as total
      from cte_max_by_vnvr_station as data
      join gsc.violation as vn on vn.id = data.violation_id
     group by vn.code, vn.name, data.violation_id;
end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;

--------------------------------------------------------------------------------------------------------
-- no changes: gsc.r_gsc103_violators (
--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  returns table (
          date             date,
          subjects         text,
          station_etc      text,
          violation_code   gsc.violation.code%type,
          violation_name   gsc.violation.name%type,
          violator_code    gsc.violator.code%type,
          violator_name    gsc.violator.name%type,
          serie            bigint)
  stable
  called on null input
  security definer
  language plpgsql
as $$
begin
  if p_project_id is null then
    return;
  end if;
  if p_region_id is null then
    return;
  end if;
  
  return query
    select i3.date,
           i3.subject_names as subjects,
           case i3.vot
             when 1 then 'ППЭ ' || station.code
             when 2 then 'РЦОИ'
             when 3 then 'КК'
             when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violation_name,
           generate_series (1, i3.cnt) as serie
      from (  select i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id,
                     max(cnt) as cnt
                from (select i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id,
                             count(*) as cnt
                        from (select monitor_result.*,
                                     subjects.subject_names,
                                     case 
                                      when subjects.subject_names is null then 1
                                      else monitor_result.object_type
                                     end as vot,
                                     schedule_detail.station_id               
                                from gsc.monitor_result
                                join gsc.schedule on schedule.id = monitor_result.schedule_id
                                join gsc.v_expert on v_expert.id = schedule.expert_id
                                join gsc.region on region.id = schedule.region_id
                                left join gsc.monitor_result_subjects (p_project_id) 
                                       as subjects (monitor_result_id, subject_names, subject_codes) 
                                       on subjects.monitor_result_id = monitor_result.id
                                left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id 
                                      and schedule_detail.exam_date = monitor_result.date
                               where (p_exam_type is null or p_exam_type is not null and monitor_result.exam_type = p_exam_type)
                                 and region.id = p_region_id
                                 and v_expert.period_id = p_project_id
                                 ) as i1
                       group by 
                             i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id) as i2
               group by 
                     i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id) as i3
       join gsc.violation on violation.id = i3.violation_id
       join gsc.violator on violator.id = i3.violator_id
       left join gsc.station on station.id = i3.station_id
      order by 
            i3.date,
            station_etc,
            i3.subject_names,
            violation_code,
            violator_code;
end;
$$;

alter function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  owner to gsc;

--------------------------------------------------------------------------------------------------------
-- no changes: gsc.r_gsc100_stations (
--------------------------------------------------------------------------------------------------------
-- no changes: gsc.r_gsc100_violations (
