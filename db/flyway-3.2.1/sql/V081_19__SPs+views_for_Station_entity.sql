
set search_path = gsc,public;

alter table gsc.station
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger station_bu before update
  on gsc.station for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_station
as
  select * from gsc.station;
  
alter view gsc.v_station
  owner to gsc;


create or replace function gsc.station_insert (
    region_id       gsc.station.region_id%type,
    project_id      gsc.station.project_id%type,
    code            gsc.station.code%type,
    name            gsc.station.name%type,
    address         gsc.station.address%type,
    tom             integer /* to prevent EF fail */,
    exam_type       integer /* to prevent EF fail */)
  returns table (id gsc.station.id%type, ts gsc.station.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id      gsc.station.id%type;
  l_ts      gsc.station.ts%type;
begin
  insert into gsc.station (
              region_id,
              project_id,
              code,
              name,
              address,
              tom,
              exam_type)
      values (region_id,
              project_id,
              code,
              name,
              address,
              tom,
              exam_type)
    returning station.id,
              station.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.station_insert (
    region_id       gsc.station.region_id%type,
    project_id      gsc.station.project_id%type,
    code            gsc.station.code%type,
    name            gsc.station.name%type,
    address         gsc.station.address%type,
    tom             integer /* to prevent EF fail */,
    exam_type       integer /* to prevent EF fail */)
  owner to gsc;



create or replace function gsc.station_update (
    p_id            gsc.station.id%type,
    new_region_id   gsc.station.region_id%type,
    new_project_id  gsc.station.project_id%type,
    new_code        gsc.station.code%type,
    new_name        gsc.station.name%type,
    new_address     gsc.station.address%type,
    new_tom         integer /* to prevent EF fail */,
    new_exam_type   integer /* to prevent EF fail */,
    old_ts          gsc.station.ts%type)
  returns table (ts gsc.station.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.station.ts%type;
begin
  update gsc.station
     set region_id = new_region_id,
         project_id = new_project_id,
         code = new_code,
         name = new_name,
         address = new_address,
         tom = new_tom,
         exam_type = new_exam_type
   where id = p_id
     and station.ts = old_ts
         returning station.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('ППЭ');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.station_update (
    p_id            gsc.station.id%type,
    new_region_id   gsc.station.region_id%type,
    new_project_id  gsc.station.project_id%type,
    new_code        gsc.station.code%type,
    new_name        gsc.station.name%type,
    new_address     gsc.station.address%type,
    new_tom         integer /* to prevent EF fail */,
    new_exam_type   integer /* to prevent EF fail */,
    old_ts          gsc.station.ts%type)
  owner to gsc;



create or replace function gsc.station_delete (p_id gsc.station.id%type)
  returns void
  language plpgsql
as $$
begin
  delete from gsc.station
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('ППЭ');
  end if;
end;
$$;

alter function gsc.station_delete (p_id gsc.station.id%type)
  owner to gsc;