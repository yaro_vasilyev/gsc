set search_path = gsc,public;

create or replace function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  returns table (
    Date            gsc.monitor_result.date%type,
    RegionCode      gsc.region.code%type,
    RegionName      gsc.region.name%type,
    ExpertFullName  text,
    ExpertKind      gsc.expert.kind%type,
    StationCode     gsc.station.code%type,
    ViolationPpe    gsc.violation.name%type,
    ViolationOther  gsc.violation.name%type,
    Note            gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
begin
  return query
    select r.date as Date, 
           rg.code as RegionCode, 
           rg.name as RegionName, 
           concat_ws(' ', x.surname, x.name, x.patronymic) as ExpertFullName,
           x.kind as ExpertKind,
           s.code as StationCode,
           case when r.object_type = 1 then vn.name else null end as ViolationPpe,
           case when r.object_type <> 1 then vn.name else null end as ViolationOther,
           r.note
      from gsc.monitor_result as r
      left join (select distinct
                        d.monitor_result_id, e.station_id 
                   from gsc.mr_detail as d
                   join gsc.exec as e on e.id = d.exec_id) as station_helper on station_helper.monitor_result_id = r.id
      left join gsc.station as s on s.id = station_helper.station_id
      join gsc.schedule as sch on sch.id = r.schedule_id
      join gsc.expert as x on x.id = sch.expert_id
      join gsc.violation as vn on vn.id = r.violation_id
      join gsc.region as rg on rg.id = sch.region_id
     where sch.period_id = p_project_id and 
           x.period_id = p_project_id and 
           (p_period_type is null or p_period_type is not null and sch.period_type = p_period_type) and
           (p_exam_type is null or p_exam_type is not null and r.exam_type = p_exam_type) and
           (p_date is null or p_date is not null and r.date = p_date)
     order by r.date, rg.name, ExpertFullName, s.code, ViolationPpe;
end;
$$;

alter function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  owner to gsc;

grant execute on function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  to s_repmr_module;