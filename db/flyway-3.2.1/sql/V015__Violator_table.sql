SET search_path = gsc, public;

--
-- Name: violator; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE violator (
    id integer NOT NULL,
    code integer NOT NULL,
    name character varying(500) NOT NULL
);
--
-- Name: violator_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY violator
    ADD CONSTRAINT violator_pkey PRIMARY KEY (id);

ALTER TABLE violator OWNER TO gsc;

--
-- Name: violator_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE violator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE violator_id_seq OWNER TO gsc;

--
-- Name: violator_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE violator_id_seq OWNED BY violator.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY violator ALTER COLUMN id SET DEFAULT nextval('violator_id_seq'::regclass);

