

set search_path = gsc,public;


alter table gsc.violator
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger violator_bu before update
  on gsc.violator for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_violator
as
  select * from gsc.violator;

alter view v_violator
  owner to gsc;


create or replace function gsc.violator_insert (
    code      gsc.violator.code%type,
    name      gsc.violator.name%type)
  returns table (id gsc.violator.id%type, ts gsc.violator.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id      gsc.violator.id%type;
  l_ts      gsc.violator.ts%type;
begin
  insert into gsc.violator (
              code,
              name)
      values (code,
              name)
    returning violator.id,
              violator.ts
         into l_id,
              l_ts;

  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.violator_insert (
    code      gsc.violator.code%type,
    name      gsc.violator.name%type)
  owner to gsc;


create or replace function gsc.violator_update (
    p_id        gsc.violator.id%type,
    new_code    gsc.violator.code%type,
    new_name    gsc.violator.name%type,
    old_ts      gsc.violator.ts%type)
  returns table (ts gsc.violator.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.violator.ts%type;
begin
  update gsc.violator
     set code = new_code,
         name = new_name
   where id = p_id
     and violator.ts = old_ts
         returning violator.ts into l_ts;

  if not found then
    select gsc.err_concurrency_control('Нарушитель');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.violator_update (
    p_id        gsc.violator.id%type,
    new_code    gsc.violator.code%type,
    new_name    gsc.violator.name%type,
    old_ts      gsc.violator.ts%type)
  owner to gsc;


create or replace function gsc.violator_delete (p_id gsc.violator.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.violator
        where id = p_id;

  if not found then
    select gsc.err_concurrency_control('Нарушитель');
  end if;
end;
$$;

alter function gsc.violator_delete (p_id gsc.violator.id%type)
  owner to gsc;

