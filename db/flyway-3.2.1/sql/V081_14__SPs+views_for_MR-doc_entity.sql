
set search_path = gsc,public;

alter table gsc.mr_doc
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


drop trigger if exists mr_doc_ts on gsc.mr_doc;

create trigger mr_doc_bu before update
  on gsc.mr_doc for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_mr_doc
as
  select d.*
    from gsc.mr_doc as d
    join gsc.v_schedule as s on d.schedule_id = s.id;
    
alter view gsc.v_mr_doc
  owner to gsc;




create or replace function gsc.mr_doc_insert (
    schedule_id             gsc.mr_doc.schedule_id%type,
    filename                gsc.mr_doc.filename%type,
    blob                    gsc.mr_doc.blob%type)
  returns table (id         gsc.mr_doc.id%type, 
                 ts         gsc.mr_doc.ts%type, 
                 mr_doc_ts  gsc.mr_doc.mr_doc_ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.mr_doc.id%type;
  l_ts        gsc.mr_doc.ts%type;
  l_mr_doc_ts gsc.mr_doc.mr_doc_ts%type;
begin
  insert into gsc.mr_doc (
              schedule_id,
              filename,
              blob)
      values (schedule_id,
              filename,
              blob)
    returning mr_doc.id,
              mr_doc.ts,
              mr_doc.mr_doc_ts
         into l_id,
              l_ts,
              l_mr_doc_ts;
  return query select l_id as id, l_ts as ts, l_mr_doc_ts as mr_doc_ts;
end;
$$;


alter function gsc.mr_doc_insert (
    schedule_id             gsc.mr_doc.schedule_id%type,
    filename                gsc.mr_doc.filename%type,
    blob                    gsc.mr_doc.blob%type)
  owner to gsc;




create or replace function gsc.mr_doc_update (
    p_id          gsc.mr_doc.id%type,
    new_schedule_id   gsc.mr_doc.schedule_id%type,
    new_blob          gsc.mr_doc.blob%type,
    new_mr_doc_ts     gsc.mr_doc.mr_doc_ts%type,
    new_filename      gsc.mr_doc.filename%type,
    old_ts            gsc.mr_doc.ts%type)
  returns table (ts     gsc.mr_doc.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.mr_doc.ts%type;
begin
  update gsc.mr_doc
     set schedule_id = new_schedule_id,
         blob = new_blob,
         mr_doc_ts = new_mr_doc_ts,
         filename = new_filename
   where id = p_id
     and mr_doc.ts = old_ts
         returning mr_doc.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('Отчет по мониторингу');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.mr_doc_update (
    p_id          gsc.mr_doc.id%type,
    new_schedule_id   gsc.mr_doc.schedule_id%type,
    new_blob          gsc.mr_doc.blob%type,
    new_mr_doc_ts     gsc.mr_doc.mr_doc_ts%type,
    new_filename      gsc.mr_doc.filename%type,
    old_ts            gsc.mr_doc.ts%type)
  owner to gsc;




create or replace function gsc.mr_doc_delete (p_id  gsc.mr_doc.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.mr_doc
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Отчет по мониторингу.');
  end if;
end;
$$;

alter function gsc.mr_doc_delete (p_id  gsc.mr_doc.id%type)
  owner to gsc;


