SET search_path = gsc, public;

--
-- Name: monitor_result; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE monitor_result (
    id integer NOT NULL,
    location d_violation_location DEFAULT 1 NOT NULL,
    schedule_id integer NOT NULL,
    exam_date date NOT NULL,
    ppe_id integer,
    violation_id integer NOT NULL,
    violator_id integer NOT NULL,
    note text,
    subject_id integer
);

--
-- Name: monitor_result_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_pkey PRIMARY KEY (id);

--
-- Name: monitor_result_ppe_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_ppe_id_fkey FOREIGN KEY (ppe_id) REFERENCES ppe(id);


--
-- Name: monitor_result_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id);


--
-- Name: monitor_result_subject_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: monitor_result_violation_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_violation_id_fkey FOREIGN KEY (violation_id) REFERENCES violation(id);


--
-- Name: monitor_result_violator_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result
    ADD CONSTRAINT monitor_result_violator_id_fkey FOREIGN KEY (violator_id) REFERENCES violator(id);



ALTER TABLE monitor_result OWNER TO gsc;

--
-- Name: monitor_result_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE monitor_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE monitor_result_id_seq OWNER TO gsc;

--
-- Name: monitor_result_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE monitor_result_id_seq OWNED BY monitor_result.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY monitor_result ALTER COLUMN id SET DEFAULT nextval('monitor_result_id_seq'::regclass);

