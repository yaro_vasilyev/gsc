set search_path = gsc,public;

create or replace function gsc.expert_insert2 (
    p_project_id                  integer,
    p_import_code                 text,
    p_surname                     text,
    p_name                        text,
    p_patronymic                  text,
    p_workplace                   text,
    p_birth_date                  date,
    p_birth_place                 text,
    p_live_address                text,
    p_phone                       text,
    p_email                       text,
    p_expirience                  text,
    p_work_years_total            integer,
    p_work_years_chief            integer,
    p_chief_name                  text,
    p_chief_post                  text,
    p_chief_email                 text,
    p_kind                        integer,
    p_base_region_id              integer,
    p_observer_id                 integer,
    p_user_guid                   uuid,
    p_work_years_education        integer,
    p_can_mission                 integer,
    p_post                        text,
    p_post_value                  integer,
    p_academic_degree_id          integer,
    p_academic_degree_value       integer,
    p_test_results                integer,
    p_interview_status            integer,
    p_academic_title_id           integer,
    p_academic_title_value        integer,
    p_awards                      text,
    p_awards_value                integer,
    p_exp_years_education         integer,
    p_speciality                  text,
    p_speciality_value            integer,
    p_subject_level_value         integer,
    p_accuracy_value              integer,
    p_punctual_value              integer,
    p_independent_value           integer,
    p_moral_value                 integer,
    p_docs_impr_part_value        integer)
  returns table (
    id                            integer,
    work_years_education_value    integer,
    can_mission_value             integer,
    test_results_value            integer,
    interview_status_value        integer,
    exp_years_education_value     integer,
    stage1_points                 integer,
    engage                        integer,
    report_count                  integer,
    report_count_value            integer,
    stage2_points                 integer,
    efficiency                    integer,
    next_year_proposal            integer,
    ts                            timestamp)
  language plpgsql
  called on null input
  volatile
  security definer
as $$
declare
  l_ts                               timestamp;
  l_id                               integer;
  l_work_years_education_value       integer;
  l_exp_years_education_value        integer;
  l_can_mission_value                integer;
  l_test_results_value               integer;
  l_interview_status_value           integer;
  l_stage1_points                    integer;
  l_stage2_points                    integer;
  l_engage                           integer;
  l_report_count                     integer;
  l_report_count_value               integer;
  l_efficiency                       integer;
  l_next_period_proposal             integer;
begin
  l_work_years_education_value       := gsc.e_work_years_education (p_work_years_education);
  l_exp_years_education_value        := gsc.e_exp_years_education (p_exp_years_education);
  l_can_mission_value                := gsc.e_can_mission (p_can_mission);
  l_test_results_value               := gsc.e_test_results (p_test_results);
  l_interview_status_value           := gsc.e_interview_status (p_interview_status);
  l_stage1_points                    := gsc.e_stage1_points (
                                              p_exp_years_education  := l_exp_years_education_value,
                                              p_speciality           := p_speciality_value,
                                              p_work_years_education := l_work_years_education_value,
                                              p_can_mission          := l_can_mission_value,
                                              p_post                 := p_post_value,
                                              p_academic_degree      := p_academic_degree_value,
                                              p_test_results         := l_test_results_value,
                                              p_interview_status     := l_interview_status_value,
                                              p_academic_title       := p_academic_title_value,
                                              p_awards               := p_awards_value);
  l_engage                           := gsc.e_engage (
                                              stage1_points          := l_stage1_points,
                                              test_results           := l_test_results_value, 
                                              interview_status       := l_interview_status_value);
  l_report_count                     := 0;
  l_report_count_value               := gsc.e_reports_count (l_report_count);
  l_stage2_points                    := gsc.e_stage2_points (
                                              p_stage1_points        := l_stage1_points,
                                              p_reports_count        := l_report_count_value,
                                              p_subject_level        := p_subject_level_value,
                                              p_accuracy             := p_accuracy_value,
                                              p_punctual             := p_punctual_value,
                                              p_independent          := p_independent_value,
                                              p_moral_level          := p_moral_value,
                                              p_docs_impr_part       := p_docs_impr_part_value);
  l_efficiency                       := gsc.e_efficiency (l_stage2_points);
  l_next_period_proposal             := gsc.e_next_period_proposal (l_efficiency);

  insert into gsc.expert (
         period_id,
         import_code,
         surname,
         name,
         patronymic,
         workplace,
         post,
         post_value,
         birth_date,
         birth_place,
         base_region_id,
         live_address,
         phone,
         email,
         academic_degree_value,
         academic_title_value,
         expirience,
         speciality,
         speciality_value,
         work_years_total,
         work_years_education,
         work_years_education_value,
         work_years_chief,
         exp_years_education,
         exp_years_education_value,
         awards,
         awards_value,
         can_mission,
         can_mission_value,
         chief_name,
         chief_post,
         chief_email,
         observer_id,
         kind,
         test_results,
         test_results_value,
         interview_status,
         interview_status_value,
         stage1_points,
         engage,
         subject_level_value,
         accuracy_level_value,
         punctual_level_value,
         independency_level_value,
         moral_level_value,
         docs_impr_part_level_value,
         stage2_points,
         efficiency,
         next_period_proposal,
         academic_title_id,
         academic_degree_id,
         user_guid)
  values (
         p_project_id,
         p_import_code,
         p_surname,
         p_name,
         p_patronymic,
         p_workplace,
         p_post,
         p_post_value,
         p_birth_date,
         p_birth_place,
         p_base_region_id,
         p_live_address,
         p_phone,
         p_email,
         p_academic_degree_value,
         p_academic_title_value,
         p_expirience,
         p_speciality,
         p_speciality_value,
         p_work_years_total,
         p_work_years_education,
         l_work_years_education_value,
         p_work_years_chief,
         p_exp_years_education,
         l_exp_years_education_value,
         p_awards,
         p_awards_value,
         p_can_mission,
         l_can_mission_value,
         p_chief_name,
         p_chief_post,
         p_chief_email,
         p_observer_id,
         p_kind,
         p_test_results,
         l_test_results_value,
         p_interview_status,
         l_interview_status_value,
         l_stage1_points,
         l_engage,
         p_subject_level_value,
         p_accuracy_value,
         p_punctual_value,
         p_independent_value,
         p_moral_value,
         p_docs_impr_part_value,
         l_stage2_points,
         l_efficiency,
         l_next_period_proposal,
         p_academic_title_id,
         p_academic_degree_id,
         p_user_guid)
   returning expert.id, 
             expert.ts
        into l_id, 
             l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Эксперты');
  end if;

  return query
    select l_id                         as id,
           l_work_years_education_value as work_years_education_value,
           l_can_mission_value          as can_mission_value,
           l_test_results_value         as test_results_value,
           l_interview_status_value     as interview_status_value,
           l_exp_years_education_value  as exp_years_education_value,
           l_stage1_points              as stage1_points,
           l_engage                     as engage,
           l_report_count               as report_count,
           l_report_count_value         as report_count_value,
           l_stage2_points              as stage2_points,
           l_efficiency                 as efficiency,
           l_next_period_proposal       as next_year_proposal,
           l_ts                         as ts;
end;
$$;

alter function gsc.expert_insert2 (
    p_project_id                  integer,
    p_import_code                 text,
    p_surname                     text,
    p_name                        text,
    p_patronymic                  text,
    p_workplace                   text,
    p_birth_date                  date,
    p_birth_place                 text,
    p_live_address                text,
    p_phone                       text,
    p_email                       text,
    p_expirience                  text,
    p_work_years_total            integer,
    p_work_years_chief            integer,
    p_chief_name                  text,
    p_chief_post                  text,
    p_chief_email                 text,
    p_kind                        integer,
    p_base_region_id              integer,
    p_observer_id                 integer,
    p_user_guid                   uuid,
    p_work_years_education        integer,
    p_can_mission                 integer,
    p_post                        text,
    p_post_value                  integer,
    p_academic_degree_id          integer,
    p_academic_degree_value       integer,
    p_test_results                integer,
    p_interview_status            integer,
    p_academic_title_id           integer,
    p_academic_title_value        integer,
    p_awards                      text,
    p_awards_value                integer,
    p_exp_years_education         integer,
    p_speciality                  text,
    p_speciality_value            integer,
    p_subject_level_value         integer,
    p_accuracy_value              integer,
    p_punctual_value              integer,
    p_independent_value           integer,
    p_moral_value                 integer,
    p_docs_impr_part_value        integer)
  owner to gsc;

----------------------------------------------------------------------------------------------------------

create or replace function gsc.expert_update2 (
    p_id                          integer,
    p_old_ts                      timestamp,
    p_new_project_id              integer,
    p_new_import_code             text,
    p_new_surname                 text,
    p_new_name                    text,
    p_new_patronymic              text,
    p_new_workplace               text,
    p_new_birth_date              date,
    p_new_birth_place             text,
    p_new_live_address            text,
    p_new_phone                   text,
    p_new_email                   text,
    p_new_expirience              text,
    p_new_work_years_total        integer,
    p_new_work_years_chief        integer,
    p_new_chief_name              text,
    p_new_chief_post              text,
    p_new_chief_email             text,
    p_new_kind                    integer,
    p_new_base_region_id          integer,
    p_new_observer_id             integer,
    p_new_user_guid               uuid,
    p_new_work_years_education    integer,
    p_new_can_mission             integer,
    p_new_post                    text,
    p_new_post_value              integer,
    p_new_academic_degree_id      integer,
    p_new_academic_degree_value   integer,
    p_new_test_results            integer,
    p_new_interview_status        integer,
    p_new_academic_title_id       integer,
    p_new_academic_title_value    integer,
    p_new_awards                  text,
    p_new_awards_value            integer,
    p_new_exp_years_education     integer,
    p_new_speciality              text,
    p_new_speciality_value        integer,
    p_new_subject_level_value     integer,
    p_new_accuracy_value          integer,
    p_new_punctual_value          integer,
    p_new_independent_value       integer,
    p_new_moral_value             integer,
    p_new_docs_impr_part_value    integer)
  returns table (
    work_years_education_value    integer,
    can_mission_value             integer,
    test_results_value            integer,
    interview_status_value        integer,
    exp_years_education_value     integer,
    stage1_points                 integer,
    engage                        integer,
    report_count                  integer,
    report_count_value            integer,
    stage2_points                 integer,
    efficiency                    integer,
    next_year_proposal            integer,
    ts                            timestamp)
  language plpgsql
  called on null input
  volatile
  security definer
as $$
declare
  l_ts                               timestamp;
  l_work_years_education_value       integer;
  l_exp_years_education_value        integer;
  l_can_mission_value                integer;
  l_test_results_value               integer;
  l_interview_status_value           integer;
  l_stage1_points                    integer;
  l_stage2_points                    integer;
  l_engage                           integer;
  l_report_count                     integer;
  l_report_count_value               integer;
  l_efficiency                       integer;
  l_next_period_proposal             integer;
begin
  l_work_years_education_value       := gsc.e_work_years_education (p_new_work_years_education);
  l_exp_years_education_value        := gsc.e_exp_years_education (p_new_exp_years_education);
  l_can_mission_value                := gsc.e_can_mission (p_new_can_mission);
  l_test_results_value               := gsc.e_test_results (p_new_test_results);
  l_interview_status_value           := gsc.e_interview_status (p_new_interview_status);
  l_stage1_points                    := gsc.e_stage1_points (
                                              p_exp_years_education  := l_exp_years_education_value,
                                              p_speciality           := p_new_speciality_value,
                                              p_work_years_education := l_work_years_education_value,
                                              p_can_mission          := l_can_mission_value,
                                              p_post                 := p_new_post_value,
                                              p_academic_degree      := p_new_academic_degree_value,
                                              p_test_results         := l_test_results_value,
                                              p_interview_status     := l_interview_status_value,
                                              p_academic_title       := p_new_academic_title_value,
                                              p_awards               := p_new_awards_value);
  l_engage                           := gsc.e_engage (
                                              stage1_points          := l_stage1_points,
                                              test_results           := l_test_results_value, 
                                              interview_status       := l_interview_status_value);
  l_report_count                     := gsc.cc_expert_reports_count (p_id, p_new_project_id);
  l_report_count_value               := gsc.e_reports_count (l_report_count);
  l_stage2_points                    := gsc.e_stage2_points (
                                              p_stage1_points        := l_stage1_points,
                                              p_reports_count        := l_report_count_value,
                                              p_subject_level        := p_new_subject_level_value,
                                              p_accuracy             := p_new_accuracy_value,
                                              p_punctual             := p_new_punctual_value,
                                              p_independent          := p_new_independent_value,
                                              p_moral_level          := p_new_moral_value,
                                              p_docs_impr_part       := p_new_docs_impr_part_value);
  l_efficiency                       := gsc.e_efficiency (l_stage2_points);
  l_next_period_proposal             := gsc.e_next_period_proposal (l_efficiency);

  update gsc.expert
     set period_id                   = p_new_project_id,
         import_code                 = p_new_import_code,
         surname                     = p_new_surname,
         name                        = p_new_name,
         patronymic                  = p_new_patronymic,
         workplace                   = p_new_workplace,
         post                        = p_new_post,
         post_value                  = p_new_post_value,
         birth_date                  = p_new_birth_date,
         birth_place                 = p_new_birth_place,
         base_region_id              = p_new_base_region_id,
         live_address                = p_new_live_address,
         phone                       = p_new_phone,
         email                       = p_new_email,
         academic_degree_value       = p_new_academic_degree_value,
         academic_title_value        = p_new_academic_title_value,
         expirience                  = p_new_expirience,
         speciality                  = p_new_speciality,
         speciality_value            = p_new_speciality_value,
         work_years_total            = p_new_work_years_total,
         work_years_education        = p_new_work_years_education,
         work_years_education_value  = l_work_years_education_value,
         work_years_chief            = p_new_work_years_chief,
         exp_years_education         = p_new_exp_years_education,
         exp_years_education_value   = l_exp_years_education_value,
         awards                      = p_new_awards,
         awards_value                = p_new_awards_value,
         can_mission                 = p_new_can_mission,
         can_mission_value           = l_can_mission_value,
         chief_name                  = p_new_chief_name,
         chief_post                  = p_new_chief_post,
         chief_email                 = p_new_chief_email,
         observer_id                 = p_new_observer_id,
         kind                        = p_new_kind,
         test_results                = p_new_test_results,
         test_results_value          = l_test_results_value,
         interview_status            = p_new_interview_status,
         interview_status_value      = l_interview_status_value,
         stage1_points               = l_stage1_points,
         engage                      = l_engage,
         subject_level_value         = p_new_subject_level_value,
         accuracy_level_value        = p_new_accuracy_value,
         punctual_level_value        = p_new_punctual_value,
         independency_level_value    = p_new_independent_value,
         moral_level_value           = p_new_moral_value,
         docs_impr_part_level_value  = p_new_docs_impr_part_value,
         stage2_points               = l_stage2_points,
         efficiency                  = l_efficiency,
         next_period_proposal        = l_next_period_proposal,
         academic_title_id           = p_new_academic_title_id,
         academic_degree_id          = p_new_academic_degree_id,
         user_guid                   = p_new_user_guid
   where expert.id                   = p_id
     and expert.ts                   = p_old_ts
         returning expert.ts
              into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Эксперты');
  end if;

  return query
    select l_work_years_education_value as work_years_education_value,
           l_can_mission_value          as can_mission_value,
           l_test_results_value         as test_results_value,
           l_interview_status_value     as interview_status_value,
           l_exp_years_education_value  as exp_years_education_value,
           l_stage1_points              as stage1_points,
           l_engage                     as engage,
           l_report_count               as report_count,
           l_report_count_value         as report_count_value,
           l_stage2_points              as stage2_points,
           l_efficiency                 as efficiency,
           l_next_period_proposal       as next_year_proposal,
           l_ts                         as ts;
end;
$$;

alter function gsc.expert_update2 (
    p_id                          integer,
    p_old_ts                      timestamp,
    p_new_project_id              integer,
    p_new_import_code             text,
    p_new_surname                 text,
    p_new_name                    text,
    p_new_patronymic              text,
    p_new_workplace               text,
    p_new_birth_date              date,
    p_new_birth_place             text,
    p_new_live_address            text,
    p_new_phone                   text,
    p_new_email                   text,
    p_new_expirience              text,
    p_new_work_years_total        integer,
    p_new_work_years_chief        integer,
    p_new_chief_name              text,
    p_new_chief_post              text,
    p_new_chief_email             text,
    p_new_kind                    integer,
    p_new_base_region_id          integer,
    p_new_observer_id             integer,
    p_new_user_guid               uuid,
    p_new_work_years_education    integer,
    p_new_can_mission             integer,
    p_new_post                    text,
    p_new_post_value              integer,
    p_new_academic_degree_id      integer,
    p_new_academic_degree_value   integer,
    p_new_test_results            integer,
    p_new_interview_status        integer,
    p_new_academic_title_id       integer,
    p_new_academic_title_value    integer,
    p_new_awards                  text,
    p_new_awards_value            integer,
    p_new_exp_years_education     integer,
    p_new_speciality              text,
    p_new_speciality_value        integer,
    p_new_subject_level_value     integer,
    p_new_accuracy_value          integer,
    p_new_punctual_value          integer,
    p_new_independent_value       integer,
    p_new_moral_value             integer,
    p_new_docs_impr_part_value    integer)
  owner to gsc;
----------------------------------------------------------------------------------------------------------

create or replace function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  returns void
  volatile
  security invoker
  language plpgsql
as $$
declare 
  e gsc.expert;
begin
  if p_source_project_id = p_target_project_id then
    raise exception '%', 'Копирование в исходный проект';
  end if;
  
  for e in 
      select * 
        from gsc.expert 
       where expert.period_id = p_source_project_id
         and expert.next_period_proposal in (1, 2) -- main, reserve
  loop
    perform gsc.expert_insert2 (
      p_project_id            := p_target_project_id,
      p_import_code           := e.import_code,
      p_surname               := e.surname,
      p_name                  := e.name,
      p_patronymic            := e.patronymic,
      p_workplace             := e.workplace,
      p_birth_date            := e.birth_date,
      p_birth_place           := e.birth_place,
      p_live_address          := e.live_address,
      p_phone                 := e.phone,
      p_email                 := e.email,
      p_expirience            := e.expirience,
      p_work_years_total      := e.work_years_total,
      p_work_years_chief      := e.work_years_chief,
      p_chief_name            := e.chief_name,
      p_chief_post            := e.chief_post,
      p_chief_email           := e.chief_email,
      p_kind                  := e.kind,
      p_base_region_id        := e.base_region_id,
      p_observer_id           := null,
      p_user_guid             := e.user_guid,
      p_work_years_education  := e.work_years_education,
      p_can_mission           := e.can_mission,
      p_post                  := e.post,
      p_post_value            := e.post_value,
      p_academic_degree_id    := e.academic_degree_id,
      p_academic_degree_value := e.academic_degree_value,
      p_test_results          := e.test_results,
      p_interview_status      := e.interview_status,
      p_academic_title_id     := e.academic_title_id,
      p_academic_title_value  := e.academic_title_value,
      p_awards                := e.awards,
      p_awards_value          := e.awards_value,
      p_exp_years_education   := e.exp_years_education,
      p_speciality            := e.speciality,
      p_speciality_value      := e.speciality_value,
      p_subject_level_value   := e.subject_level_value,
      p_accuracy_value        := e.accuracy_level_value,
      p_punctual_value        := e.punctual_level_value,
      p_independent_value     := e.independency_level_value,
      p_moral_value           := e.moral_level_value,
      p_docs_impr_part_value  := e.docs_impr_part_level_value);
  end loop;
end;
$$;

alter function gsc.copy_experts (
    p_source_project_id       gsc.period.id%type,
    p_target_project_id       gsc.period.id%type)
  owner to gsc;

----------------------------------------------------------------------------------------------------------

drop function if exists gsc.expert_insert(integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, date, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying, character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, uuid);
drop function if exists gsc.expert_update(integer, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, date, character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying, character varying, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, uuid, timestamp without time zone);
