set search_path = gsc,public;

--------------------------------------------------------------------------------------------------------
-- Drop column
--------------------------------------------------------------------------------------------------------


drop view gsc.v_exec;

alter table gsc.exec
  drop column project_id;


alter table gsc."exec"
  add constraint exec_uniq_idx unique(exam_id, station_id);


create or replace view gsc.v_exec
as
  select * from gsc.exec;

alter view gsc.v_exec
  owner to gsc;

--------------------------------------------------------------------------------------------------------
-- CRUD functions (for EXAM actually, ore EXEC - in V134, sorry)
--------------------------------------------------------------------------------------------------------

create or replace function gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer)
  returns table (id gsc.exam.id%type, ts gsc.exam.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id      gsc.exam.id%type;
  l_ts      gsc.exam.ts%type;
begin
  insert into gsc.exam (
              period_type,
              exam_date,
              subject_id,
              examglobalid,
              exam_type)
      values (period_type,
              exam_date,
              subject_id,
              examglobalid,
              exam_type)
    returning exam.id,
              exam.ts
         into l_id,
              l_ts;
              
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer)
  owner to gsc;

grant execute on function gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer)
  to s_exam_new;


create or replace function gsc.exam_update (
    p_id                gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type)
  returns table (ts  gsc.exam.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts   gsc.exam.ts%type;
begin
  update gsc.exam
     set period_type = new_period_type,
         exam_date = new_exam_date,
         subject_id = new_subject_id,
         examglobalid = new_examglobalid,
         exam_type = new_exam_type
   where id = p_id
     and exam.ts = p_ts
         returning exam.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Расписание');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.exam_update (
    p_id                gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type)
  owner to gsc;

grant execute on function gsc.exam_update (
    p_id                gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type)
  to s_exam_edit;

--------------------------------------------------------------------------------------------------------
-- Drop old ones
--------------------------------------------------------------------------------------------------------


drop function if exists gsc.exam_insert (
    period_type         integer,
    exam_date           gsc.exam.exam_date%type,
    subject_id          gsc.exam.subject_id%type,
    period_id           gsc.period.id%type,
    examglobalid        gsc.exam.examglobalid%type,
    exam_type           integer);


drop function if exists gsc.exam_update (
    p_id                    gsc.exam.id%type,
    new_period_type         integer,
    new_exam_date           gsc.exam.exam_date%type,
    new_subject_id          gsc.exam.subject_id%type,
    new_period_id           gsc.period.id%type,
    new_examglobalid        gsc.exam.examglobalid%type,
    new_exam_type           integer,
    p_ts                gsc.exam.ts%type);

--------------------------------------------------------------------------------------------------------
-- Other functions
--------------------------------------------------------------------------------------------------------

/*
import_exec
monitor_result_subjects
r_gsc095_monitor_results_detailed
r_gsc097_count_mr_ppe
r_gsc102_violations
r_violations_wo_dup??
*/

create or replace function gsc.import_exec (
    p_imp_session_guid          uuid,
    p_project_id                integer
)
  returns table (
      station_code                integer,
      exam                        text) 
  language 'plpgsql'
  volatile
  called on null input
  security definer
as
$$
begin
  --// todo check import right
  
  -- import rows
  with imported as (
    insert into gsc.exec as e (
      station_id,
      exam_id,
      kim,
      count_part,
      count_room,
      count_room_video)
      select
        ie.station_id,
        ie.exam_id,
        ie.kim,
        ie.count_part,
        ie.count_room,
        ie.count_room_video
      from gsc.imp_exec ie
      where ie.imp_session_guid = p_imp_session_guid and ie.imp_status = 'added'::gsc.import_status
    on conflict (exam_id, station_id)
      do update set
        kim = excluded.kim,
        count_part = excluded.count_part,
        count_room = excluded.count_room,
        count_room_video = excluded.count_room_video
    returning e.station_id, e.exam_id)
  /* update import status from returning result */
  update gsc.imp_exec ie
    set imp_status = 'imported'::gsc.import_status
    where ie.imp_session_guid = p_imp_session_guid and (ie.station_id, ie.exam_id) in (
      select i.station_id, i.exam_id
      from imported i
    );
  
  -- return records presence in table but missing from import
  return query
    select s.code as station_code,
           sub.name || ' ' || ex.exam_date as exam           
    from gsc.exec e
         left join gsc.station s on e.station_id = s.id
         left join gsc.exam ex on e.exam_id = ex.id
         left join gsc.subject sub on ex.subject_id = sub.id
     where s.project_id = p_project_id and
           sub.project_id = p_project_id and
          not exists(select null
                      from gsc.imp_exec ie
                      where ie.imp_session_guid = p_imp_session_guid and
                            ie.exam_id = e.exam_id and
                            ie.station_id = e.station_id);
end;
$$;

alter function gsc.import_exec(uuid, integer) 
  owner to gsc;

--------------------------------------------------------------------------------------------------------

create or replace function gsc.monitor_result_subjects(
                p_project_id              integer) 
returns table (
                monitor_result_id         integer, 
                subject_names             text, 
                subject_codes             text)
  language plpgsql 
  stable 
  security definer
  strict
as $$
begin
  return query
    select monitor_result.id as monitor_result_id,
           array_to_string (array_agg (subject.name order by subject.name), ', ') as subject_names,
           array_to_string (array_agg (subject.code order by subject.name), ', ') as subject_codes
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.expert on expert.id = schedule.expert_id
      left join gsc.mr_detail on mr_detail.monitor_result_id = monitor_result.id
      left join gsc.exec on exec.id = mr_detail.exec_id
      left join gsc.exam on exam.id = exec.exam_id
      left join gsc.subject on subject.id = exam.subject_id and subject.project_id = p_project_id
     where expert.period_id = p_project_id
     group by monitor_result.id;
end;
$$;


alter function gsc.monitor_result_subjects(integer) owner to gsc;

--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  returns table (
    exam_date           date,
    region_code         gsc.region.code%type,
    region_name         gsc.region.name%type,
    region_zone         gsc.region.zone%type,
    subjects            text,
    expert_fio          text,
    expert_kind         gsc.expert.kind%type,
    ppe                 text,
    violation_code      gsc.violation.code%type,
    violation_name      gsc.violation.name%type,
    violator_code       gsc.violator.code%type,
    violator_name       gsc.violator.name%type,
    note                gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--    Function for GSC-095 report
-- PARAMETERS:
--    p_project_id        REQUIRED. Project ID
--    p_period_type       OPTIONAL. Exam's period type
--    p_exam_type         OPTIONAL. Exam's exam type
--    p_date              OPTIONAL. Filter date
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject as subj on subj.id = exam.subject_id
       where subj.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and exam.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and exam.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and exam.exam_date = p_date))
         union all -- другие результаты мониторинга (не только c запольненными ППЭ)
      select mr1.id,
             null,
             mr1.date
        from gsc.monitor_result mr1
        join gsc.schedule as sch1 on sch1.id = mr1.schedule_id -- ограничить по проекту для
        join gsc.expert on expert.id = sch1.expert_id
       where not exists (select * from gsc.mr_detail mrd1 where mrd1.monitor_result_id = mr1.id)
         and expert.period_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and sch1.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and mr1.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and mr1.date = p_date))
    ),
    cte_subjects as (
      select mr.monitor_result_id, array_to_string (array_agg (j.name order by j.name), ', ') as subjects
        from gsc.mr_detail as mrd
        join cte_station_helper as mr on mr.monitor_result_id = mrd.monitor_result_id
        join gsc.exec as x on x.id = mrd.exec_id
        join gsc.exam as e on e.id = x.exam_id
        join gsc.subject as j on j.id = e.subject_id
       where j.project_id = p_project_id
         and (p_period_type is null or (p_period_type is not null and e.period_type = p_period_type))
         and (p_exam_type is null or (p_exam_type is not null and e.exam_type = p_exam_type))
         and (p_date is null or (p_date is not null and e.exam_date = p_date))
       group by mr.monitor_result_id
    )
    select cte_station_helper.exam_date,
           r.code as region_code,
           r.name as region_name,
           r.zone as region_zone,
           cte_subjects.subjects,
           concat_ws (' ', ex.surname, ex.name, ex.patronymic) as expert_fio,
           ex.kind as expert_kind,
           case mr.object_type
              when 1 then coalesce (st.code::text, 'ППЭ')
              when 2 then 'РЦОИ'
              when 3 then 'КК'
              when 4 then 'ППЗ'
              else '-'
           end as ppe,
           vn.code as violation_code,
           vn.name as violation_name,
           vr.code as violator_code,
           vr.name as violator_name,
           mr.note
      from cte_station_helper
      left join cte_subjects on cte_subjects.monitor_result_id = cte_station_helper.monitor_result_id
      left join gsc.station as st on st.id = cte_station_helper.station_id
      join gsc.monitor_result as mr on mr.id = cte_station_helper.monitor_result_id
      join gsc.schedule as sch on sch.id = mr.schedule_id
      join gsc.region as r on r.id = sch.region_id
      join gsc.expert as ex on ex.id = sch.expert_id
      left join gsc.violation as vn on vn.id = mr.violation_id
      left join gsc.violator as vr on vr.id = mr.violator_id
     where ex.period_id = p_project_id
       and (p_period_type is null or (p_period_type is not null and sch.period_type = p_period_type))
       and (p_exam_type is null or (p_exam_type is not null and mr.exam_type = p_exam_type));
end;
$$;

alter function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  owner to gsc;
  
grant execute on function gsc.r_gsc095_monitor_results_detailed (
    p_project_id        gsc.period.id%type,
    p_period_type       gsc.exam.period_type%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_date              date)
  to s_mrep_module;
--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc097_count_mr_ppe (
          p_project_id     gsc.period.id%type)
  returns table (
          exam_type        gsc.d_exam_type,
          violations       numeric,
          schedules        numeric)
  stable
  returns null on null input
  security definer
  language plpgsql
as $$
begin
  return query
    with cte_violations as (
      select inner_sql.exam_type,
             inner_sql.cnt
        from (
              select 1::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 1) as v
               where v.station_id is not null
              union
              select 2::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 2) as v
               where v.station_id is not null
              union
              select 3::gsc.d_exam_type as exam_type, 
                     sum (cnt) as cnt
                from gsc.r_station_violations (p_project_id, 3) as v
               where v.station_id is not null
             ) as inner_sql
    ),
    cte_schedules as (
      select inner_sql.exam_type,
             count (inner_sql.*)::numeric as cnt
        from (
              select distinct
                     exam.exam_type,
                     exam.exam_date,
                     station.id as station_id,
                     schedule_detail.schedule_id
                from gsc.exec
                join gsc.exam on exam.id = exec.exam_id
                join gsc.subject on subject.id = exam.subject_id
                join gsc.station on station.id = exec.station_id
                join gsc.schedule_detail on schedule_detail.station_id = station.id and schedule_detail.exam_date = exam.exam_date
               where station.project_id = p_project_id
                 and subject.project_id = p_project_id
              ) as inner_sql
        group by
              inner_sql.exam_type
    )
    select et::gsc.d_exam_type as exam_type,
           coalesce (cte_violations.cnt, 0) as violations,
           coalesce (cte_schedules.cnt, 0) as schedules
      from unnest (array [1, 2, 3]) as et
      left join cte_violations on cte_violations.exam_type = et
      left join cte_schedules on cte_schedules.exam_type = et
     order by et;
end;
$$;

alter function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  owner to gsc;

grant execute on function gsc.r_gsc097_count_mr_ppe (gsc.period.id%type)
  to s_repmr_module;

--------------------------------------------------------------------------------------------------------
create or replace function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  returns table (violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            numeric)
  stable
  language plpgsql
  called on null input
  security definer
as $$
--
-- DESCRIPTION:
--  Function for GSC-102 report
--
-- PARAMETERS:
--  p_project_id:   REQUIRED. ID of project (period). 
--  p_exam_type:    REQUIRED. Type of Exam.
--  p_station_code: CAN BE NULL. Station code.
--  p_exam_date:    CAN_BE_NULL. Date of exam.
--
begin
  return query
    with cte_station_helper as (
      select distinct
             d.monitor_result_id,
             e.station_id,
             exam.exam_date
        from gsc.mr_detail as d
        join gsc.exec as e on e.id = d.exec_id
        join gsc.exam on exam.id = e.exam_id
        join gsc.subject on subject.id = exam.subject_id
       where subject.project_id = p_project_id
    ),
    cte_count_by_expert_etc as (
      select cte_station_helper.exam_date as date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code as station_code, 
             count(*) as cnt
        from gsc.monitor_result as mr
        join gsc.schedule as sch on sch.id = mr.schedule_id
        join gsc.expert on expert.id = sch.expert_id
        left join cte_station_helper on mr.id = cte_station_helper.monitor_result_id
        left join gsc.station as s on s.id = cte_station_helper.station_id
       where mr.exam_type = p_exam_type 
         and expert.period_id = p_project_id
         and (p_exam_date is null or (p_exam_date is not null and cte_station_helper.exam_date = p_exam_date))
         and (p_station_code is null or (p_station_code is not null and s.code = p_station_code))
       group by cte_station_helper.exam_date, 
             mr.exam_type, 
             mr.schedule_id, 
             mr.violation_id, 
             mr.violator_id, 
             s.code
    ),
    cte_max_by_vnvr_station as (
      select i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code,
             max (i.cnt) as  max_cnt
        from cte_count_by_expert_etc as i
       group by
             i.date,
             i.exam_type,
             i.violation_id,
             i.violator_id,
             i.station_code
    )
    select vn.code, vn.name, sum(data.max_cnt) as total
      from cte_max_by_vnvr_station as data
      join gsc.violation as vn on vn.id = data.violation_id
     group by vn.code, vn.name, data.violation_id;
end;
$$;

alter function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  owner to gsc;

grant execute on function gsc.r_gsc102_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type,
    p_station_code      gsc.station.code%type,
    p_exam_date         gsc.exam.exam_date%type)
  to s_repmr_module;

--------------------------------------------------------------------------------------------------------
-- That's all, folks!
--------------------------------------------------------------------------------------------------------
