CREATE OR REPLACE FUNCTION gsc.u_create_user (
  type_ integer,
  id_ integer,
  pg_user smallint = 0
)
RETURNS varchar AS
$body$
DECLARE
  email VARCHAR;  
  password VARCHAR; 
  l_user_id INT;
BEGIN
 CASE type_
   WHEN 0 THEN -- expert
    SELECT exp.email INTO STRICT email
      FROM gsc.expert exp           
      WHERE exp.id = id_;
      
      password:= gsc.u_random_string(8);
  --  password:= '12345'; -- for test
      
      INSERT INTO gsc.users
      	(parent_id, type_user, user_login, pass, guid, agreement)   
      VALUES
      	(id_, 0, email, 'md5' || md5(password || email), public.uuid_generate_v4(), null)
      RETURNING id into l_user_id;
			
      UPDATE gsc.expert e
      	SET user_id = l_user_id
      	WHERE e.id = id_;
      
      IF pg_user = 1 THEN
        EXECUTE 'CREATE USER '|| email ||' WITH PASSWORD '|| password ||';';
      END IF;
      RETURN password;
   WHEN 1 THEN -- manager (observer)
   	SELECT ob.email INTO STRICT email
      FROM gsc.observer ob           
      WHERE ob.id = id_;
      
      password:= gsc.u_random_string(8);
  --  password:= '12345'; -- for test
      
      INSERT INTO gsc.users
      	(parent_id, type_user, user_login, pass, guid, agreement)   
      VALUES
      	(id_, 1, email, 'md5' || md5(password || email), public.uuid_generate_v4(), null)
      RETURNING id INTO l_user_id;
        
      UPDATE gsc.observer o
      	SET user_id = l_user_id
      	WHERE o.id = id_;
        
      IF pg_user = 1 THEN
        EXECUTE 'CREATE USER '|| email ||' WITH PASSWORD '|| password ||';';
      END IF;    
      RETURN password;
   ELSE
       RETURN NULL;
 END CASE;  
EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;