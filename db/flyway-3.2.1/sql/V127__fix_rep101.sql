set search_path = gsc,public;

drop function if exists gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type);

create or replace function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  returns table (
          date             date,
          subjects         text,
          station_etc      text,
          violation_code   gsc.violation.code%type,
          violation_name   gsc.violation.name%type,
          violator_code    gsc.violator.code%type,
          violator_name    gsc.violator.name%type,
          serie            bigint)
  stable
  called on null input
  security definer
  language plpgsql
as $$
begin
  if p_project_id is null then
    return;
  end if;
  if p_region_id is null then
    return;
  end if;
  
  return query
    select i3.date,
           i3.subject_names as subjects,
           case i3.vot
             when 1 then 'ППЭ ' || station.code
             when 2 then 'РЦОИ'
             when 3 then 'КК'
             when 4 then 'ППЗ'
           end as station_etc,
           violation.code as violation_code,
           violation.name as violation_name,
           violator.code as violator_code,
           violator.name as violation_name,
           generate_series (1, i3.cnt) as serie
      from (  select i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id,
                     max(cnt) as cnt
                from (select i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id,
                             count(*) as cnt
                        from (select monitor_result.*,
                                     subjects.subject_names,
                                     case 
                                      when subjects.subject_names is null then 1
                                      else monitor_result.object_type
                                     end as vot,
                                     schedule_detail.station_id               
                                from gsc.monitor_result
                                join gsc.schedule on schedule.id = monitor_result.schedule_id and schedule.period_id = p_project_id
                                join gsc.region on region.id = schedule.region_id
                                left join gsc.monitor_result_subjects (p_project_id) 
                                       as subjects (monitor_result_id, subject_names, subject_codes) 
                                       on subjects.monitor_result_id = monitor_result.id
                                left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id 
                                      and schedule_detail.exam_date = monitor_result.date
                               where (p_exam_type is null or p_exam_type is not null and monitor_result.exam_type = p_exam_type)
                                 and region.id = p_region_id) as i1
                       group by 
                             i1.schedule_id,
                             i1.date,
                             i1.violation_id,
                             i1.violator_id,
                             i1.subject_names,
                             i1.vot,
                             i1.station_id) as i2
               group by 
                     i2.date,
                     i2.violation_id,
                     i2.violator_id,
                     i2.subject_names,
                     i2.vot,
                     i2.station_id) as i3
       join gsc.violation on violation.id = i3.violation_id
       join gsc.violator on violator.id = i3.violator_id
       left join gsc.station on station.id = i3.station_id
      order by 
            i3.date,
            station_etc,
            i3.subject_names,
            violation_code,
            violator_code;
end;
$$;

alter function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  owner to gsc;

 grant execute on function gsc.r_gsc101_violations_in_region (
          p_project_id     gsc.period.id%type,
          p_region_id      gsc.region.id%type,
          p_exam_type      gsc.exam.exam_type%type)
  to s_repmr_module;