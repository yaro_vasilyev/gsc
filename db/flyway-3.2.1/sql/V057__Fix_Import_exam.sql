 -- object recreation
DROP FUNCTION gsc.import_exam(p_imp_session_guid uuid, p_period_id integer);

CREATE FUNCTION gsc.import_exam (
  p_imp_session_guid uuid,
  p_period_id integer
)
RETURNS TABLE (
  globalid integer,
  exam text
) AS
$body$
BEGIN
  --// todo check import right
  
  -- import rows
  WITH imported AS (
    INSERT INTO exam AS e (
    	period_id,
      examglobalid,
			subject_id,
			exam_type,
			period_type,
			exam_date)
      (SELECT
        p_period_id,
        ie.examglobalid,
				ie.subject_id,
				ie.exam_type,
				ie.period_type,
				ie.exam_date
      FROM imp_exam ie
      WHERE ie.imp_session_guid = p_imp_session_guid AND ie.imp_status = 'added'::gsc.import_status)
    /*ON CONFLICT ON CONSTRAINT exam_period_id_examglobalid_uniq*/
    ON CONFLICT (period_id, examglobalid) WHERE examglobalid IS NOT NULL
      DO UPDATE SET
        subject_id  = EXCLUDED.subject_id,
				exam_type = EXCLUDED.exam_type,
				period_type = EXCLUDED.period_type,
				exam_date = EXCLUDED.exam_date
	RETURNING e.examglobalid)
  -- update import status
  UPDATE gsc.imp_exam ie
  SET imp_status = 'imported'::gsc.import_status
  WHERE ie.imp_session_guid = p_imp_session_guid AND
        ie.examglobalid = ANY (
                                SELECT i.examglobalid
                                FROM imported i
        );

  -- return records presence in table but missing from import
  RETURN QUERY
		SELECT e.examglobalid as examglobid,
					 s.name || ' ' || e.exam_date AS exam
		FROM gsc.exam e
				 LEFT JOIN gsc.subject s ON e.subject_id = s.id
									 WHERE e.period_id = p_period_id AND
												NOT exists(SELECT NULL
																		FROM gsc.imp_exam ie
																		WHERE ie.imp_session_guid = p_imp_session_guid AND
																					ie.examglobalid = e.examglobalid);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000
SET search_path = gsc, public;