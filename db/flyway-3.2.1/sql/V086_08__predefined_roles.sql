set search_path = gsc,public;


create role r_admin;
create role r_boss;
create role r_an_lead;
create role r_an;
create role r_manager;
create role r_rsm;
create role r_expert;


create or replace function gsc.role_expert()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_expert';
end;
$$;

alter function gsc.role_expert()
  owner to gsc;

create or replace function gsc.role_rsm()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_rsm';
end;
$$;

alter function gsc.role_rsm()
  owner to gsc;

create or replace function gsc.role_manager()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_manager';
end;
$$;

alter function gsc.role_manager()
  owner to gsc;


create or replace function gsc.role_an()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_an';
end;
$$;

alter function gsc.role_an()
  owner to gsc;


create or replace function gsc.role_an_lead()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_an_lead';
end;
$$;

alter function gsc.role_an_lead()
  owner to gsc;

create or replace function gsc.role_boss()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_boss';
end;
$$;

alter function gsc.role_boss()
  owner to gsc;

create or replace function gsc.role_admin()
  returns text
  immutable
  language plpgsql
as $$
begin
  return 'r_admin';
end;
$$;

alter function gsc.role_admin()
  owner to gsc;

