SET search_path = gsc, public;
--
-- Name: imp_subject; Type: TABLE; Schema: gsc; Owner: postgres
--
CREATE TABLE imp_subject (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code integer NOT NULL,
    name character varying(100) NOT NULL
);

ALTER TABLE imp_subject OWNER TO postgres;

--
-- Name: imp_subject_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--
ALTER TABLE ONLY imp_subject
    ADD CONSTRAINT imp_subject_pkey PRIMARY KEY (imp_session_guid, code);

--
-- Name: TABLE imp_subject; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON TABLE imp_subject IS 'Subject import table.';

--
-- Name: COLUMN imp_subject.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';

--
-- Name: COLUMN imp_subject.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.imp_user IS 'User imported row.';

--
-- Name: COLUMN imp_subject.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.imp_datetime IS 'Import timestamp.';

--
-- Name: COLUMN imp_subject.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.imp_status IS 'Import status';

--
-- Name: COLUMN imp_subject.code; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.code IS 'Subject code.';

--
-- Name: COLUMN imp_subject.name; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_subject.name IS 'Subject name.';
--
-- Name: import_subject(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--

CREATE FUNCTION import_subject(p_imp_session_guid uuid) RETURNS TABLE(code integer, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes INTEGER [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right (grant)

  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT ims.code
                      FROM gsc.imp_subject ims
                      WHERE ims.imp_session_guid = p_imp_session_guid
                      GROUP BY ims.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/

  -- import rows
  WITH imported AS (
    INSERT INTO subject AS s (code, name)
      SELECT
        ims.code,
        ims.name
      FROM imp_subject ims
      WHERE ims.imp_session_guid = p_imp_session_guid AND ims.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT subject_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING s.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_subject ims
  SET imp_status = 'imported'
  WHERE ims.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 s.code,
                 s.name
               FROM gsc.subject s
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_subject ims
                                WHERE ims.imp_session_guid = p_imp_session_guid AND
                                      ims.code = s.code);
END;
$$;

ALTER FUNCTION gsc.import_subject(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_row_subject(uuid, integer, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--
CREATE FUNCTION import_row_subject(imp_session_guid uuid, code integer, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO gsc.imp_subject (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;

ALTER FUNCTION gsc.import_row_subject(imp_session_guid uuid, code integer, name character varying) OWNER TO postgres;
