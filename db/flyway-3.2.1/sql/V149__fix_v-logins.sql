 -- object recreation
DROP VIEW gsc.v_logins;

CREATE VIEW gsc.v_logins (
    id,
    guid,
    observer_id,
    expert_id,
    user_login,
    password)
AS
SELECT u.id,
		u.guid,
    o.id AS observer_id,
    e.id AS expert_id,
    u.user_login,
    NULL::character varying AS password
FROM gsc.v_users u
     LEFT JOIN gsc.v_observer o ON u.guid = o.user_guid
     LEFT JOIN gsc.v_expert e ON u.guid = e.user_guid;

ALTER TABLE gsc.v_logins
  OWNER TO gsc;
