set search_path = gsc,public;

alter table gsc.event 
  add column user_id integer,
  add constraint event_user_id_fkey foreign key (user_id) references gsc.users (id);