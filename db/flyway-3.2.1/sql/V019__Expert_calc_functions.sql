SET search_path = gsc, public;

--
-- Name: cc_expert_reports_count(integer, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION cc_expert_reports_count(integer, integer) RETURNS integer
    LANGUAGE sql STABLE STRICT
    AS $_$
  -- $1 expert_id
  -- $2 period_id
  select 0;
$_$;

ALTER FUNCTION gsc.cc_expert_reports_count(integer, integer) OWNER TO gsc;


--
-- Name: e_academic_degree(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_academic_degree(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
    select case when $1 is null then 0 else 5 end;
$_$;

ALTER FUNCTION gsc.e_academic_degree(integer) OWNER TO gsc;


--
-- Name: e_academic_title(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_academic_title(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
    select default_val 
      from gsc.academic_title
     where id = $1;
$_$;

ALTER FUNCTION gsc.e_academic_title(integer) OWNER TO gsc;


--
-- Name: e_awards(character varying); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_awards(character varying) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
    select case when coalesce(trim($1), '') = '' then 0 else 5 end;
$_$;

ALTER FUNCTION gsc.e_awards(character varying) OWNER TO gsc;


--
-- Name: e_can_mission(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_can_mission(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
    select case $1
      when 0 then 0
      when 1 then 5
    end;
$_$;

ALTER FUNCTION gsc.e_can_mission(integer) OWNER TO gsc;


--
-- Name: e_efficiency(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_efficiency(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select case when $1 between 12 and 17 then 0 -- отсутствие эффективности
              when $1 between 18 and 39 then 1 -- низкая эффективность
              when $1 between 40 and 59 then 2 -- средняя эффективность
              when $1 between 60 and 85 then 3 -- высокая эффективность
          end;
$_$;

ALTER FUNCTION gsc.e_efficiency(integer) OWNER TO gsc;


--
-- Name: e_engage(integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_engage(stage1_points integer, test_results integer, interview_status integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
  declare
    ret integer default 0;
  begin
    if stage1_points >= 12 and test_results >= 3 and interview_status > 0 then
      ret := 1;
    else
      ret := 0;
    end if;
    return ret;
  end;
$$;

ALTER FUNCTION gsc.e_engage(stage1_points integer, test_results integer, interview_status integer) OWNER TO gsc;


--
-- Name: e_exp_years_education(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_exp_years_education(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select case
    when $1 between  0 and  1 then 1
    when $1 between  2 and  3 then 2
    when $1 between  4 and  5 then 3
    when $1 between  6 and  7 then 4
    when $1 >= 8 then 5
  end;
$_$;

ALTER FUNCTION gsc.e_exp_years_education(integer) OWNER TO gsc;


--
-- Name: e_interview_status(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_interview_status(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
    select case $1
      when 0 then 0
      when 1 then 5
    end;
$_$;

ALTER FUNCTION gsc.e_interview_status(integer) OWNER TO gsc;


--
-- Name: e_next_period_proposal(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_next_period_proposal(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select case $1
           when 2 then 1
           when 3 then 1 -- основной
           when 1 then 2 -- резерв
           when 0 then 3 -- исключить
         end;
$_$;

ALTER FUNCTION gsc.e_next_period_proposal(integer) OWNER TO gsc;


--
-- Name: e_reports_count(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_reports_count(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select case
      when $1 = 0               then 0
      when $1 between  1 and  4 then 1
      when $1 between  5 and  9 then 2
      when $1 between 10 and 14 then 3
      when $1 between 15 and 19 then 4
      when $1 >= 20             then 5
    end;
$_$;

ALTER FUNCTION gsc.e_reports_count(integer) OWNER TO gsc;


--
-- Name: e_stage1_points(integer, integer, integer, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_stage1_points(p_exp_years_education integer, p_speciality integer, p_work_years_education integer, p_can_mission integer, p_post integer, 
                                p_academic_degree integer, p_test_results integer, p_interview_status integer, p_academic_title integer, p_awards integer) 
    RETURNS integer
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
    declare
      ret integer default 0;
    begin
      select p_exp_years_education +
             p_speciality +
             p_work_years_education +
             p_can_mission +
             p_post +
             p_academic_degree +
             p_test_results +
             p_interview_status +
             p_academic_title +
             p_awards
        into ret;
      return ret;
    end;
$$;

ALTER FUNCTION gsc.e_stage1_points(p_exp_years_education integer, p_speciality integer, p_work_years_education integer, p_can_mission integer, 
                                    p_post integer, p_academic_degree integer, p_test_results integer, p_interview_status integer, p_academic_title integer, 
                                    p_awards integer) OWNER TO gsc;


--
-- Name: e_stage2_points(integer, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_stage2_points(p_stage1_points integer, p_reports_count integer, p_subject_level integer, p_accuracy integer, p_punctual integer, 
                                p_independent integer, p_moral_level integer, p_docs_impr_part integer) 
    RETURNS integer
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
  declare
    ret integer default 0;
  begin
    select p_stage1_points +
           p_reports_count +
           p_subject_level +
           p_accuracy +
           p_punctual +
           p_independent +
           p_moral_level +
           p_docs_impr_part
      into ret;
    return ret;
  end;
$$;

ALTER FUNCTION gsc.e_stage2_points(p_stage1_points integer, p_reports_count integer, p_subject_level integer, p_accuracy integer, p_punctual integer, 
                                    p_independent integer, p_moral_level integer, p_docs_impr_part integer) OWNER TO gsc;


--
-- Name: e_test_results(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_test_results(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
    select case
      when $1 between  0 and  4 then 0
      when $1 between  5 and  9 then 1
      when $1 between 10 and 15 then 2
      when $1 between 16 and 18 then 3
      when $1 between 19 and 21 then 4
      when $1 between 22 and 23 then 5
    end;
$_$;

ALTER FUNCTION gsc.e_test_results(integer) OWNER TO gsc;


--
-- Name: e_work_years_education(integer); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION e_work_years_education(integer) RETURNS integer
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select case
    when $1 between  0 and  4 then 1
    when $1 between  5 and  9 then 2
    when $1 between 10 and 14 then 3
    when $1 between 15 and 19 then 4
    when $1 >= 20 then 5
  end;
$_$;
ALTER FUNCTION gsc.e_work_years_education(integer) OWNER TO gsc;
