set search_path = gsc,public;

create domain d_exam_type as integer 
  default 1 
  constraint d_exam_type_chk check (value in (1, 2, 3));
alter domain d_exam_type owner to gsc;

alter table exam rename column exam_type to exam_type_old;
alter table exam add column exam_type d_exam_type;

update exam set exam_type = exam_type_old;

alter table exam 
  alter column exam_type set not null,
  drop column exam_type_old;


alter table monitor_result 
  add column exam_type d_exam_type;