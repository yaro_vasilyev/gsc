SET search_path = gsc, public;

--
-- Name: violation; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE violation (
    id integer NOT NULL,
    code integer NOT NULL,
    name character varying(500) NOT NULL
);
--
-- Name: violation_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY violation
    ADD CONSTRAINT violation_pkey PRIMARY KEY (id);


ALTER TABLE violation OWNER TO gsc;

--
-- Name: violation_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE violation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE violation_id_seq OWNER TO gsc;

--
-- Name: violation_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE violation_id_seq OWNED BY violation.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY violation ALTER COLUMN id SET DEFAULT nextval('violation_id_seq'::regclass);

