set search_path = gsc,public;



alter table gsc.event
  rename column ts to event_ts;

alter table gsc.event
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create or replace view gsc.v_event
as
  select e.* 
    from gsc.event as e
    join gsc.v_schedule as s on s.id = e.schedule_id;

alter view gsc.v_event
  owner to gsc;



create trigger event_bu before update
  on gsc.event for each row
    execute procedure gsc.update_ts_trigger_fn();





create or replace function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       gsc.event.channel%type,
    type          gsc.event.type%type,
    schedule_id   gsc.event.schedule_id%type,
    object_type   gsc.event.object_type%type,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type)
  returns table (id gsc.event.id%type, ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id        gsc.event.id%type;
  l_ts        gsc.event.ts%type;
begin
  insert into gsc.event (
              event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_id)
      values (event_ts,
              channel,
              type,
              schedule_id,
              object_type,
              project_id,
              user_id)
    returning event.id, event.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.event_insert (
    event_ts      gsc.event.event_ts%type,
    channel       gsc.event.channel%type,
    type          gsc.event.type%type,
    schedule_id   gsc.event.schedule_id%type,
    object_type   gsc.event.object_type%type,
    project_id    gsc.event.project_id%type,
    user_id       gsc.event.user_id%type)
  owner to gsc;




create or replace function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       gsc.event.channel%type,
    new_type          gsc.event.type%type,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   gsc.event.object_type%type,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type)
  returns table (ts gsc.event.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts        gsc.event.ts%type;
begin
  update gsc.event
     set event_ts = new_event_ts,
         channel = new_channel,
         type = new_type,
         schedule_id = new_schedule_id,
         object_type = new_object_type,
         project_id = new_project_id,
         user_id = new_user_id
   where event.id = p_id
     and event.ts = old_ts
         returning event.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Событие горячей линии');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.event_update (
    p_id          gsc.event.id%type,
    new_event_ts      gsc.event.event_ts%type,
    new_channel       gsc.event.channel%type,
    new_type          gsc.event.type%type,
    new_schedule_id   gsc.event.schedule_id%type,
    new_object_type   gsc.event.object_type%type,
    new_project_id    gsc.event.project_id%type,
    new_user_id       gsc.event.user_id%type,
    old_ts            gsc.event.ts%type)
  owner to gsc;




create or replace function gsc.event_delete (p_id gsc.event.id%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.event
        where id = p_id;
  
  if not found then
    select gsc.err_concurrency_control('Событие горячей линии');
  end if;
end;
$$;

alter function gsc.event_delete (p_id gsc.event.id%type)
  owner to gsc;

