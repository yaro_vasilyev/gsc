

set search_path = gsc,public;

create or replace function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  returns table (ts gsc.schedule.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts  gsc.schedule.ts%type;
begin
  update gsc.schedule
     set expert_id = new_expert_id,
         period_id = new_period_id,
         monitor_rcoi = new_monitor_rcoi,
         fixed = new_fixed,
         period_type = new_period_type,
         region_id = new_region_id
   where schedule.id = p_id
     and schedule.ts = old_ts
         returning schedule.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('График');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.schedule_update (
    p_id              gsc.schedule.id%type,
    new_expert_id     gsc.schedule.expert_id%type,
    new_period_id     gsc.schedule.period_id%type,
    new_monitor_rcoi  gsc.schedule.monitor_rcoi%type,
    new_fixed         gsc.schedule.fixed%type,
    new_period_type   integer,
    new_region_id     gsc.schedule.region_id%type,
    old_ts            gsc.schedule.ts%type)
  owner to gsc;