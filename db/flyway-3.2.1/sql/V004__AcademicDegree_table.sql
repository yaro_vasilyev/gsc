SET search_path = gsc, public;

--
-- Name: academic_degree; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE academic_degree (
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    id integer NOT NULL
);


--
-- Name: academic_degree_code_key; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY academic_degree
    ADD CONSTRAINT academic_degree_code_key UNIQUE (code);


--
-- Name: academic_degree_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY academic_degree
    ADD CONSTRAINT academic_degree_pkey PRIMARY KEY (id);



ALTER TABLE academic_degree OWNER TO gsc;

--
-- Name: academic_degree_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE academic_degree_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE academic_degree_id_seq OWNER TO gsc;

--
-- Name: academic_degree_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE academic_degree_id_seq OWNED BY academic_degree.id;

--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY academic_degree ALTER COLUMN id SET DEFAULT nextval('academic_degree_id_seq'::regclass);



--
-- Name: f_expert_academic_degree(academic_degree); Type: FUNCTION; Schema: gsc; Owner: gsc
--

CREATE FUNCTION f_expert_academic_degree(ad academic_degree) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
  ret integer;
begin
  select CASE
            WHEN ad.code IS NULL THEN 0
            ELSE ad.default_val
        END
    into ret;
  return ret;
end;
$$;


ALTER FUNCTION gsc.f_expert_academic_degree(ad academic_degree) OWNER TO gsc;
