set search_path = gsc,public;

alter table gsc.expert
  alter column awards set default 'Отсутствуют',
  alter column awards_value set default 0;


create or replace function gsc.e_awards(character varying) 
  returns integer
  language sql 
  immutable
as $$
  select case when coalesce(trim(upper($1), '')) in ('', 'ОТСУТСТВУЮТ') then 0 else 5 end;
$$;

alter function gsc.e_awards(character varying) owner to gsc;