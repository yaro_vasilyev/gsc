set search_path = gsc,public;

alter table mr_detail 
  drop constraint mr_detail_monitor_result_id_fkey;
alter table mr_detail 
  add constraint mr_detail_monitor_result_id_fkey foreign key (monitor_result_id) 
    references monitor_result (id) on update cascade on delete cascade;
