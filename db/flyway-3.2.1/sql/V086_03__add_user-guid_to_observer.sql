set search_path = gsc,public;


alter table gsc.observer
  add column user_guid uuid,
  add constraint observer_user_guid_fkey foreign key (user_guid) references gsc.users (guid) on update cascade on delete set null,
  add constraint observer_user_guid_unq unique (user_guid);


update gsc.observer as o
   set user_guid = (select u.guid from gsc.users as u where u.id = o.user_id);

comment on column gsc.observer.user_id is 'Column user_id is deprecated. Use user_guid column instead.';

update gsc.observer
   set user_id = null;
