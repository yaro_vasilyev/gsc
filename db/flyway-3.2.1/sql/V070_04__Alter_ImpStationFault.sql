ALTER TABLE gsc.imp_station_fault
  ADD COLUMN imp_status_comment TEXT;

COMMENT ON COLUMN gsc.imp_station_fault.imp_status_comment
IS 'Comment for import fault';