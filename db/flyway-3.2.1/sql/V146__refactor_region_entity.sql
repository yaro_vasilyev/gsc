set search_path = gsc,public;

--------------------------------------------------------------------------------------------------
-- Add column
--------------------------------------------------------------------------------------------------

alter table gsc.region
  add column project_id integer,
  add constraint region_project_id_fkey foreign key (project_id) references gsc.period (id);

update gsc.region
   set project_id = (select id
                       from gsc.period
                      where status = 1)
 where project_id is null;

alter table gsc.region
  alter column project_id set not null;

--------------------------------------------------------------------------------------------------
-- Recreate view
--------------------------------------------------------------------------------------------------
create or replace view gsc.v_region
as 
  select *
    from gsc.region;

alter view gsc.v_region
  owner to gsc;

--------------------------------------------------------------------------------------------------
-- CRUD functions
--------------------------------------------------------------------------------------------------
create or replace function gsc.region_insert (
    p_project_id  gsc.period.id%type,
    code          gsc.region.code%type,
    name          gsc.region.name%type,
    zone          integer /* just in case */)
  returns table (id gsc.region.id%type, ts gsc.region.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_id      gsc.region.id%type;
  l_ts      gsc.region.ts%type;
begin
  insert into gsc.region (
              project_id,
              code,
              name,
              zone)
      values (p_project_id,
              code,
              name,
              zone)
    returning region.id,
              region.ts
         into l_id,
              l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.region_insert (
    p_project_id  gsc.period.id%type,
    code          gsc.region.code%type,
    name          gsc.region.name%type,
    zone          integer /* just in case */)
  owner to gsc;
--------------------------------------------------------------------------------------------------
create or replace function gsc.region_update (
    p_id              gsc.region.id%type,
    p_new_project_id  gsc.period.id%type,
    new_code          gsc.region.code%type,
    new_name          gsc.region.name%type,
    new_zone          integer /* in case of ... */,
    old_ts            gsc.region.ts%type)
  returns table (
    ts                gsc.region.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts      gsc.region.ts%type;
begin
  update gsc.region
     set code       = new_code,
         name       = new_name,
         zone       = new_zone,
         project_id = p_new_project_id
   where id         = p_id
     and region.ts  = old_ts
         returning region.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Субъект РФ');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.region_update (
    p_id              gsc.region.id%type,
    p_new_project_id  gsc.period.id%type,
    new_code          gsc.region.code%type,
    new_name          gsc.region.name%type,
    new_zone          integer /* in case of ... */,
    old_ts            gsc.region.ts%type)
  owner to gsc;
--------------------------------------------------------------------------------------------------
-- Drop old functions
--------------------------------------------------------------------------------------------------

drop function if exists gsc.region_insert (
    code        gsc.region.code%type,
    name        gsc.region.name%type,
    zone        integer /* just in case */);

drop function if exists gsc.region_update (
    p_id          gsc.region.id%type,
    new_code      gsc.region.code%type,
    new_name      gsc.region.name%type,
    new_zone      integer /* in case of ... */,
    old_ts        gsc.region.ts%type);


--------------------------------------------------------------------------------------------------
-- Import constraints
--------------------------------------------------------------------------------------------------
alter table gsc.region
  drop constraint region_code_key,
  add constraint region_code_key unique (code, project_id);

--------------------------------------------------------------------------------------------------
-- Import tables
--------------------------------------------------------------------------------------------------

alter table gsc.imp_region
  add column project_id integer;

--------------------------------------------------------------------------------------------------
-- Import functions
--------------------------------------------------------------------------------------------------

create or replace function gsc.import_row_region (
    p_project_id        gsc.period.id%type,
    imp_session_guid    uuid,
    code                integer,
    name                varchar)
  returns void
  language plpgsql
  volatile
  called on null input
  security definer
as
$$
declare
  l_error_detail text;
begin
  insert into gsc.imp_region (imp_session_guid, code, name, project_id)
  values (imp_session_guid, code, name, p_project_id);
exception
  when unique_violation then
    get stacked diagnostics l_error_detail = pg_exception_detail;
    raise exception E'Код % дублируется.\nОшибка БД: %', code, l_error_detail
      using
        errcode   = 'unique_violation', 
        hint      = 'Исправьте файл и повторите импорт.';
end;
$$;

alter function gsc.import_row_region (
    p_project_id          gsc.period.id%type,
    imp_session_guid      uuid,
    code                  integer,
    name                  varchar)
  owner to gsc;

drop function if exists gsc.import_row_region (
    imp_session_guid      uuid,
    code                  integer,
    name                  varchar);

--------------------------------------------------------------------------------------------------
create or replace function gsc.import_region (
      p_imp_session_guid        uuid)
  returns table (
      code                      integer,
      name                      varchar) 
  language plpgsql
  volatile
  called on null input
  security definer
as
$$
declare
  l_dupcodes varchar [];
  l_dups     varchar;
  l_imported varchar [];
begin
  --// todo check import right
  
  -- import rows
  with imported as (
    insert into region as ad (code, name, project_id)
         select iad.code as imp_code,
                iad.name,
                iad.project_id
           from imp_region as iad
          where iad.imp_session_guid = p_imp_session_guid 
            and iad.imp_status = 'added'
             on conflict 
             on constraint region_code_key
             do update 
            set name = excluded.name
      returning ad.code
  )
  select array_agg(i.code)
    from imported i
    into l_imported;

  -- update import status
  update gsc.imp_region iad
     set imp_status           = 'imported'
   where iad.imp_session_guid = p_imp_session_guid;
  -- return records presence in table but missing from import
  return query 
         select ad.code,
               ad.name,
               ad.project_id
          from gsc.region ad
         where not exists(select null
                            from gsc.imp_region iad
                           where iad.imp_session_guid = p_imp_session_guid and
                                 ad.code              = iad.code);
end;
$$;

alter function gsc.import_region (p_imp_session_guid uuid)
  owner to gsc;

--------------------------------------------------------------------------------------------------
-- That's all folks!
--------------------------------------------------------------------------------------------------
