SET search_path = gsc, public;

--
-- Name: contract_info; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE contract_info (
    id integer NOT NULL,
    number character varying(30) NOT NULL,
    contract_date date NOT NULL
);

--
-- Name: contract_info_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY contract_info
    ADD CONSTRAINT contract_info_pkey PRIMARY KEY (id);

--
-- Name: contract_info_number_key; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--

ALTER TABLE ONLY contract_info
    ADD CONSTRAINT contract_info_number_key UNIQUE (number);


ALTER TABLE contract_info OWNER TO gsc;

--
-- Name: contract_info_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE contract_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contract_info_id_seq OWNER TO gsc;

--
-- Name: contract_info_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE contract_info_id_seq OWNED BY contract_info.id;


--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY contract_info ALTER COLUMN id SET DEFAULT nextval('contract_info_id_seq'::regclass);

