set search_path = gsc,public;

drop function if exists gsc.get_expert_by_user(p_user_id integer);
drop function if exists gsc.get_observer_by_user(p_user_id integer);
drop function if exists gsc.get_observers(p_user_id gsc.users.id%type);
drop function if exists gsc.get_experts (user_id gsc.users.id%type);
drop view if exists gsc.v_r_experts4email;