set search_path = gsc,public;

create or replace view gsc.v_observer
as
  select *
    from gsc.get_observers(session_user);
    
alter view gsc.v_observer
  owner to gsc;
