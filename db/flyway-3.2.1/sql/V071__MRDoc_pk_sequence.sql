set search_path = gsc,public;

alter table mr_doc 
  alter column id drop default;

drop sequence if exists mr_doc_id_seq;

alter table mr_doc 
  drop constraint mr_doc_pkey;

alter table mr_doc 
  rename id to id2;
alter table mr_doc 
  add column id serial;

update mr_doc set id = id2;

alter table mr_doc 
  alter column id set not null;
alter table mr_doc 
  add constraint mr_doc_pkey primary key(id);
alter table mr_doc 
  drop column id2;

select setval('mr_doc_id_seq', (select coalesce(max(id), 0) + 1 from mr_doc), true);