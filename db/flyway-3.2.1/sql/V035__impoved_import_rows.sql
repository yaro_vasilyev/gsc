CREATE OR REPLACE FUNCTION gsc.import_row_academic_degree (
  imp_session_guid uuid,
  code varchar,
  name varchar
)
RETURNS void AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  INSERT INTO gsc.imp_academic_degree (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE OR REPLACE FUNCTION gsc.import_row_academic_title (
  imp_session_guid uuid,
  code varchar,
  name varchar
)
RETURNS void AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  INSERT INTO imp_academic_title (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE OR REPLACE FUNCTION gsc.import_row_contract_info (
  imp_session_guid uuid,
  number varchar,
  contract_date date
)
RETURNS void AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  INSERT INTO gsc.imp_contract_info (imp_session_guid, number, contract_date)
  VALUES (imp_session_guid, number, contract_date);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE OR REPLACE FUNCTION gsc.import_row_subject (
  imp_session_guid uuid,
  code integer,
  name varchar
)
RETURNS void AS
$body$
DECLARE
	l_error_detail text;
BEGIN
  INSERT INTO gsc.imp_subject (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
EXCEPTION
	  WHEN unique_violation then
			GET STACKED DIAGNOSTICS l_error_detail = PG_EXCEPTION_DETAIL;
      RAISE EXCEPTION E'Код % дублируется.\nОшибка БД: %', code,  l_error_detail
      USING ERRCODE = 'unique_violation', HINT =
        'Исправьте файл и повторите импорт.';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;