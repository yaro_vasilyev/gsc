set search_path = gsc,public;

create or replace function gsc.r_station_violations (
    p_project_id integer,
    p_exam_type gsc.d_exam_type)
  returns table (
          date date, 
          violation_id integer, 
          violator_id integer, 
          station_id integer, 
          region_id integer, 
          cnt bigint) 
  called on null input
  language plpgsql 
  stable
  security definer
as
$$
-- возвращает статистику по нарушениям без дубликатов. 
-- нарушения на РЦОИ/ППЗ/КК попадают в одну категорию (строки с station_id = null)
begin
  return query
    select group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id,
           max (group_by_expert.cnt) as cnt
      from (select monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id,
                   schedule_detail.station_id,
                   schedule.region_id,
                   count(*) as cnt 
              from gsc.monitor_result
              join gsc.schedule on monitor_result.schedule_id = schedule.id
         left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id
             where (p_exam_type is null or (p_exam_type is not null and monitor_result.exam_type = p_exam_type))
               and schedule.period_id = p_project_id
             group by monitor_result.schedule_id, 
                   monitor_result.date, 
                   monitor_result.violation_id, 
                   monitor_result.violator_id, 
                   schedule_detail.station_id,
                   schedule.region_id) as group_by_expert
     group by group_by_expert.date,
           group_by_expert.violation_id,
           group_by_expert.violator_id,
           group_by_expert.station_id,
           group_by_expert.region_id;
end;
$$;

ALTER FUNCTION gsc.r_station_violations(integer, gsc.d_exam_type)
  OWNER TO gsc;


create or replace function gsc.r_gsc100_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  returns table (station_id       gsc.station.id%type,
                 violation_code   gsc.violation.code%type,
                 violation_name   gsc.violation.name%type,
                 count            bigint)
  stable
  language plpgsql
  called on null input
  security definer
as $$
begin
  return query 
    select inner_sql.station_id,
           violation.code as violation_code,
           violation.name as violation_name,
           inner_sql.cnt
      from (
        select v.region_id,
               v.station_id,
               v.violation_id,
               sum (v.cnt)::bigint as cnt
          from gsc.r_station_violations (p_project_id, p_exam_type)
            as v (date, violation_id, violator_id, station_id, region_id, cnt)
          join gsc.violation on violation.id = v.violation_id
         where v.station_id is not null
         group by
               v.region_id,
               v.station_id,
               v.violation_id
           ) as inner_sql
        join gsc.violation on violation.id = inner_sql.violation_id;
end;
$$;

alter function gsc.r_gsc100_violations (
    p_project_id        gsc.period.id%type,
    p_exam_type         gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc100_violations (
    gsc.period.id%type, gsc.exam.exam_type%type)
  to s_repmr_module;


create or replace function gsc.r_gsc100_stations (
    p_project_id            gsc.period.id%type,
    p_exam_type             gsc.exam.exam_type%type)
  returns table (region_code      gsc.region.code%type,
                 region_name      gsc.region.name%type,
                 station_id       gsc.station.id%type,
                 station_code     gsc.station.code%type,
                 station_name     gsc.station.name%type)
  called on null input
  stable
  security definer
  language plpgsql
as $$
begin
  return query 
    select x.region_code,
           x.region_name,
           x.station_id,
           x.station_code,
           x.station_name
      from (
        select region.code as region_code,
               region.name as region_name,
               inner_sql.station_id,
               station.code as station_code,
               station.name as station_name,
               lpad (station.code::text, 10, '0') as normalized_station_code
          from (
            select distinct
                   v.region_id,
                   v.station_id
              from gsc.r_station_violations (p_project_id, p_exam_type)
                as v (date, violation_id, violator_id, station_id, region_id, cnt)
             where v.station_id is not null
               ) as inner_sql
          join gsc.region on region.id = inner_sql.region_id
          left join gsc.station on station.id = inner_sql.station_id
           ) as x
     order by 
           x.region_name,
           x.normalized_station_code;
end;
$$;

alter function gsc.r_gsc100_stations (
    p_project_id            gsc.period.id%type,
    p_exam_type             gsc.exam.exam_type%type)
  owner to gsc;

grant execute on function gsc.r_gsc100_stations (
      gsc.period.id%type, gsc.exam.exam_type%type) 
  to s_repmr_module;
