set search_path = gsc,public;

create or replace function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  returns table (
    Date            gsc.monitor_result.date%type,
    RegionCode      gsc.region.code%type,
    RegionName      gsc.region.name%type,
    ExpertFullName  text,
    ExpertKind      gsc.expert.kind%type,
    StationCode     gsc.station.code%type,
    ViolationPpe    gsc.violation.name%type,
    ViolationOther  gsc.violation.name%type,
    Note            gsc.monitor_result.note%type)
  language plpgsql
  stable
  called on null input
  security definer
as $$
begin
  return query
    select monitor_result.date as Date, 
           region.code as RegionCode, 
           region.name as RegionName, 
           concat_ws(' ', v_expert.surname, v_expert.name, v_expert.patronymic) as ExpertFullName,
           v_expert.kind as ExpertKind,
           station.code as StationCode,
           case when monitor_result.object_type = 1 then violation.name else null end as ViolationPpe,
           case when monitor_result.object_type <> 1 then violation.name else null end as ViolationOther,
           monitor_result.note
      from gsc.monitor_result
      join gsc.schedule on schedule.id = monitor_result.schedule_id
      join gsc.v_expert on v_expert.id = schedule.expert_id
      join gsc.violation on violation.id = monitor_result.violation_id
      join gsc.region on region.id = schedule.region_id
      left join gsc.schedule_detail on schedule_detail.id = monitor_result.schedule_detail_id
      left join gsc.station on station.id = schedule_detail.station_id
     where v_expert.period_id = p_project_id and 
           /* station.project_id = p_project_id and*/
           (p_period_type is null or p_period_type is not null and schedule.period_type = p_period_type) and
           (p_exam_type is null or p_exam_type is not null and monitor_result.exam_type = p_exam_type) and
           (p_date is null or p_date is not null and monitor_result.date = p_date)
     order by 
           monitor_result.date, 
           region.name, 
           ExpertFullName, 
           station.code, 
           ViolationPpe;
end;
$$;

alter function gsc.r_gsc094_monitor_results (
    p_project_id    gsc.period.id%type,
    p_period_type   gsc.exam.period_type%type,
    p_exam_type     gsc.exam.exam_type%type,
    p_date          date)
  owner to gsc;