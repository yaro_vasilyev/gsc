

set search_path = gsc,public;

alter table event 
  drop column monitor_result_id;

alter table monitor_result
  add column event_id integer,
  add constraint monitor_result_event_id_fkey
    foreign key (event_id) references event(id)
      on update cascade on delete set null;

