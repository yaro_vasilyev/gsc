set search_path = gsc,public;




alter table gsc.contract_info
  add column ts timestamp without time zone default now(),
  alter column ts set not null;


create trigger contract_info_bu before update
  on gsc.contract_info for each row
    execute procedure gsc.update_ts_trigger_fn();


create or replace view gsc.v_contract_info
as
  select * from gsc.contract_info;

alter view gsc.v_contract_info
  owner to gsc;




create or replace function gsc.contract_info_insert (
    number        gsc.contract_info.number%type,
    contract_date gsc.contract_info.contract_date%type)
  returns table (ts gsc.contract_info.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts        gsc.contract_info.ts%type;
begin
  insert into gsc.contract_info (
              number,
              contract_date)
      values (number,
              contract_date)
    returning contract_info.ts
         into l_ts;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_info_insert (
    number        gsc.contract_info.number%type,
    contract_date gsc.contract_info.contract_date%type)
  owner to gsc;




create or replace function gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    old_ts            gsc.contract_info.ts%type)
  returns table (ts gsc.contract_info.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts   gsc.contract_info.ts%type;
begin
  update gsc.contract_info
     set number = new_number,
         contract_date = new_contract_date
   where number = old_number
     and contract_info.ts = old_ts
         returning contract_info.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Номер договора');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_info_update (
    old_number        gsc.contract_info.number%type,
    new_number        gsc.contract_info.number%type,
    new_contract_date gsc.contract_info.contract_date%type,
    old_ts            gsc.contract_info.ts%type)
  owner to gsc;



create or replace function gsc.contract_info_delete (
    p_number        gsc.contract_info.number%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.contract_info
        where number = p_number;
  
  if not found then
    select gsc.err_concurrency_control('Номер договора.');
  end if;
end;
$$;


alter function gsc.contract_info_delete (
    p_number        gsc.contract_info.number%type)
  owner to gsc;





alter table gsc.contract
  add column ts timestamp without time zone default now(),
  alter column ts set not null;

create trigger contract_bu before update
  on gsc.contract for each row
    execute procedure gsc.update_ts_trigger_fn();

create or replace view gsc.v_contract
as
  select c.* 
    from gsc.contract as c
    join gsc.v_schedule as s on s.id = c.schedule_id;

alter view gsc.v_contract
  owner to gsc;




create or replace function gsc.contract_insert (
    period_type   gsc.contract.period_type%type,
    schedule_id   gsc.contract.schedule_id%type,
    contract_type gsc.contract.contract_type%type,
    number        gsc.contract.number%type,
    date_from     gsc.contract.date_from%type,
    date_to       gsc.contract.date_to%type,
    summ          gsc.contract.summ%type,
    route         gsc.contract.route%type,
    doc           gsc.contract.doc%type)
  returns table (ts gsc.contract.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts      gsc.contract.ts%type;
begin
  insert into gsc.contract (
              period_type,
              schedule_id,
              contract_type,
              number,
              date_from,
              date_to,
              summ,
              route,
              doc)
      values (period_type,
              schedule_id,
              contract_type,
              number,
              date_from,
              date_to,
              summ,
              route,
              doc)
    returning contract.ts
         into l_ts;
  return query select l_ts as ts;
end;
$$;

alter function gsc.contract_insert (
    period_type   gsc.contract.period_type%type,
    schedule_id   gsc.contract.schedule_id%type,
    contract_type gsc.contract.contract_type%type,
    number        gsc.contract.number%type,
    date_from     gsc.contract.date_from%type,
    date_to       gsc.contract.date_to%type,
    summ          gsc.contract.summ%type,
    route         gsc.contract.route%type,
    doc           gsc.contract.doc%type)
  owner to gsc;




create or replace function gsc.contract_update (
    old_number             gsc.contract.number%type,
    new_period_type        gsc.contract.period_type%type,
    new_schedule_id        gsc.contract.schedule_id%type,
    new_contract_type      gsc.contract.contract_type%type,
    new_number             gsc.contract.number%type,
    new_date_from          gsc.contract.date_from%type,
    new_date_to            gsc.contract.date_to%type,
    new_summ               gsc.contract.summ%type,
    new_route              gsc.contract.route%type,
    new_doc                gsc.contract.doc%type,
    old_ts                 gsc.contract.ts%type)
  returns table (ts   gsc.contract.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts    gsc.contract.ts%type;
begin
  update gsc.contract
     set period_type = new_period_type,
         schedule_id = new_schedule_id,
         contract_type = new_contract_type,
         number = new_number,
         date_from = new_date_from,
         date_to = new_date_to,
         summ = new_summ,
         route = new_route,
         doc = new_doc
   where number = old_number
     and contract.ts = old_ts
         returning contract.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Договор');
  end if;
  
  return query select l_ts as ts;
end;
$$;


alter function gsc.contract_update (
    old_number             gsc.contract.number%type,
    new_period_type        gsc.contract.period_type%type,
    new_schedule_id        gsc.contract.schedule_id%type,
    new_contract_type      gsc.contract.contract_type%type,
    new_number             gsc.contract.number%type,
    new_date_from          gsc.contract.date_from%type,
    new_date_to            gsc.contract.date_to%type,
    new_summ               gsc.contract.summ%type,
    new_route              gsc.contract.route%type,
    new_doc                gsc.contract.doc%type,
    old_ts                 gsc.contract.ts%type)
  owner to gsc;




create or replace function gsc.contract_delete (
    p_number      gsc.contract.number%type)
  returns void
  volatile
  language plpgsql
as $$
begin
  delete from gsc.contract
        where number = p_number;
  
  if not found then
    select gsc.err_concurrency_control('Договор');
  end if;
end;
$$;

alter function gsc.contract_delete (
    p_number      gsc.contract.number%type)
  owner to gsc;