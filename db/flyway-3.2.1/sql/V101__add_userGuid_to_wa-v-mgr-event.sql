 -- object recreation
DROP VIEW gsc.wa_v_mgr_event;

CREATE VIEW gsc.wa_v_mgr_event (
    id,
    event_ts,
    channel,
    type,
    schedule_id,
    object_type,
    user_guid)
AS
SELECT event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid
FROM gsc.event
WHERE event.project_id = gsc.current_project_id();

ALTER TABLE gsc.wa_v_mgr_event
  OWNER TO gsc;

GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES, TRIGGER, TRUNCATE
  ON gsc.wa_v_mgr_event TO gsc;

CREATE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_mgr_event 
DO INSTEAD (
DELETE FROM gsc.event
  WHERE event.id = old.id; --flyway bug: should comment
);

CREATE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_mgr_event 
DO INSTEAD (
INSERT INTO gsc.event (event_ts, channel, type, schedule_id, object_type, project_id, user_guid)
  VALUES (new.event_ts, new.channel, new.type, new.schedule_id, new.object_type, gsc.current_project_id(), new.user_guid)
  RETURNING event.id,
    event.event_ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type,
    event.user_guid;  --flyway bug: should comment
);

CREATE RULE "_UPDATE" AS ON UPDATE TO gsc.wa_v_mgr_event 
DO INSTEAD (
UPDATE gsc.event SET event_ts = new.event_ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id,
	object_type = new.object_type, user_guid = new.user_guid
  WHERE event.id = old.id;  --flyway bug: should comment
);