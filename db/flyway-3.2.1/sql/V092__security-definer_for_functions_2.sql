set search_path = gsc,public;

alter function gsc.oiv_gek_insert(integer, varchar, varchar, varchar, varchar, varchar, varchar, 
    varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, 
    varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, 
    varchar, varchar) security definer;
alter function gsc.oiv_gek_update(integer, integer, varchar, varchar, varchar, varchar, 
    varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, 
    varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, 
    varchar, varchar, varchar, varchar, timestamp) security definer;
alter function gsc.oiv_gek_delete(integer) security definer;

---------------------------------------------------------------------------------------------

alter function gsc.rcoi_insert(integer, varchar, varchar, varchar, varchar, varchar, varchar, varchar) security definer;
alter function gsc.rcoi_update(integer, integer, varchar, varchar, varchar, varchar, varchar, varchar, varchar, timestamp) security definer;
alter function gsc.rcoi_delete(integer) security definer;
alter function gsc.import_rcoi(uuid) security definer;
alter function gsc.import_row_rcoi(uuid, integer, varchar, varchar, varchar, varchar, varchar, varchar) security definer;

---------------------------------------------------------------------------------------------