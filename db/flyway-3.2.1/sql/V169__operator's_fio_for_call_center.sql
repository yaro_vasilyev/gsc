set search_path = gsc,public;

---------------------------------------------------------------------------------------------------------
create or replace function gsc._xxx_null_if_empty (text)
  returns text
  language sql
  stable
  returns null on null input
  security invoker
as $$
  select case when char_length ($1) = 0 then null else $1 end;
$$;

alter function gsc._xxx_null_if_empty (text)
  owner to gsc;

---------------------------------------------------------------------------------------------------------
create or replace view gsc.v_user_fio
as
  select guid, coalesce (v_observer.fio, 
                  gsc._xxx_null_if_empty (concat_ws (' ', v_expert.surname, v_expert.name, v_expert.patronymic)), 
                  gsc._xxx_null_if_empty (user_login),
                  rolname) as fio
    from gsc.v_users
    left join gsc.v_observer on v_observer.user_guid = v_users.guid
    left join gsc.v_expert on v_expert.user_guid = v_users.guid;


alter view gsc.v_user_fio
  owner to gsc;


