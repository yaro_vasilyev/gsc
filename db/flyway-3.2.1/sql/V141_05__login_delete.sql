CREATE OR REPLACE FUNCTION gsc.login_delete (
  i_id integer
)
RETURNS void AS
$body$
DECLARE
	l_login varchar;
BEGIN
  select u.user_login
  into strict l_login
  from gsc.users u
  where u.id = i_id;
  
	execute format('drop role %I', l_login);

  DELETE FROM gsc.users u WHERE u.id = i_id;  
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

alter function gsc.login_delete (i_id integer)
  owner to gsc;