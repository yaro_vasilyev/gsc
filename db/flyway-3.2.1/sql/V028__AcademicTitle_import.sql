SET search_path = gsc, public;
--
-- Name: imp_academic_title; Type: TABLE; Schema: gsc; Owner: postgres
--
CREATE TABLE imp_academic_title (
    imp_session_guid uuid NOT NULL,
    imp_user text DEFAULT "current_user"() NOT NULL,
    imp_datetime timestamp with time zone DEFAULT now() NOT NULL,
    imp_status import_status DEFAULT 'added'::import_status NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL
);

ALTER TABLE imp_academic_title OWNER TO postgres;

--
-- Name: imp_academic_title_pkey; Type: CONSTRAINT; Schema: gsc; Owner: postgres
--
ALTER TABLE ONLY imp_academic_title
    ADD CONSTRAINT imp_academic_title_pkey PRIMARY KEY (imp_session_guid, code);

--
-- Name: TABLE imp_academic_title; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON TABLE imp_academic_title IS 'Academic title import table.';

--
-- Name: COLUMN imp_academic_title.imp_session_guid; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.imp_session_guid IS 'Import session GUID for distinguishing data for different import sessions.';

--
-- Name: COLUMN imp_academic_title.imp_user; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.imp_user IS 'User imported row.';

--
-- Name: COLUMN imp_academic_title.imp_datetime; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.imp_datetime IS 'Import timestamp.';

--
-- Name: COLUMN imp_academic_title.imp_status; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.imp_status IS 'Import status';

--
-- Name: COLUMN imp_academic_title.code; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.code IS 'Acedemic title code.';

--
-- Name: COLUMN imp_academic_title.name; Type: COMMENT; Schema: gsc; Owner: postgres
--
COMMENT ON COLUMN imp_academic_title.name IS 'Academic title name.';

--
-- Name: import_academic_title(uuid); Type: FUNCTION; Schema: gsc; Owner: postgres
--
CREATE FUNCTION import_academic_title(p_imp_session_guid uuid) RETURNS TABLE(code character varying, name character varying)
    LANGUAGE plpgsql
    SET search_path TO gsc, public
    AS $$
DECLARE
  l_dupcodes VARCHAR [];
  l_dups     VARCHAR;
  l_imported VARCHAR [];
BEGIN
  --// todo check import right
  
  /*replaced by primary key -- check duplicates
  l_dupcodes := array(SELECT iat.code
                      FROM gsc.imp_academic_title iat
                      WHERE iat.imp_session_guid = p_imp_session_guid
                      GROUP BY iat.code
                      HAVING count(*) > 1);
  IF array_length(l_dupcodes, 1) > 0
  THEN
    l_dups := array_to_string(l_dupcodes, ', ');
    RAISE EXCEPTION 'В импортируемых данных дублируются ключи:
%.', l_dups
    USING ERRCODE = 'IMPDU', HINT = 'Исправьте файл и повторите импорт.';
  END IF;*/

  -- import rows
  WITH imported AS (
    INSERT INTO academic_title AS act (code, name)
      SELECT
        act.code imp_code,
        act.name
      FROM imp_academic_title act
      WHERE act.imp_session_guid = p_imp_session_guid AND act.imp_status = 'added'
    ON CONFLICT ON CONSTRAINT academic_title_code_key
      DO UPDATE SET name = EXCLUDED.name
    RETURNING act.code)
  SELECT array_agg(i.code)
  FROM imported i
  INTO l_imported;

  -- update import status
  UPDATE gsc.imp_academic_title iat
  SET imp_status = 'imported'
  WHERE iat.imp_session_guid = p_imp_session_guid;

  -- return records presence in table but missing from import
  RETURN QUERY SELECT
                 act.code,
                 act.name
               FROM gsc.academic_title act
               WHERE NOT exists(SELECT NULL
                                FROM gsc.imp_academic_title iat
                                WHERE iat.imp_session_guid = p_imp_session_guid AND
                                      act.code = iat.code);
END;
$$;
ALTER FUNCTION gsc.import_academic_title(p_imp_session_guid uuid) OWNER TO postgres;

--
-- Name: import_row_academic_title(uuid, character varying, character varying); Type: FUNCTION; Schema: gsc; Owner: postgres
--
CREATE FUNCTION import_row_academic_title(imp_session_guid uuid, code character varying, name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO imp_academic_title (imp_session_guid, code, name)
  VALUES (imp_session_guid, code, name);
END;
$$;

ALTER FUNCTION gsc.import_row_academic_title(imp_session_guid uuid, code character varying, name character varying) OWNER TO postgres;