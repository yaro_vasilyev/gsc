set search_path = gsc,public;

create or replace function gsc.r_gsc108_count_calls_regions (
    p_project_id      integer)
  returns table (
    numpp             integer,
    region            text,
    cnt               integer,
    percent           numeric)
  language plpgsql
  stable
  security invoker
  returns null on null input
as $$
declare
  l_total_rsm   bigint;
begin
  select count(*)
    into l_total_rsm
    from gsc.v_event as e1 
    join gsc.v_schedule as sch1 on e1.schedule_id = sch1.id
    join gsc.v_expert as ex1 on sch1.expert_id = ex1.id
   where ex1.period_id = p_project_id;
   
  return query
    select row_number() over(order by v_region.name)::integer as numpp,
           v_region.name::text as region,
           count(v_event.id)::integer as cnt,
           100::numeric * count(v_event.id) / l_total_rsm as percent
      from gsc.v_event
      join gsc.v_schedule on v_schedule.id = v_event.schedule_id
      join gsc.v_expert on v_schedule.expert_id = v_expert.id
      right join gsc.v_region on v_schedule.region_id = v_region.id and v_expert.period_id = p_project_id
     where v_region.project_id = p_project_id
     group by v_region.name
     order by v_region.name;
end;
$$;

alter function gsc.r_gsc108_count_calls_regions (integer)
  owner to gsc;

