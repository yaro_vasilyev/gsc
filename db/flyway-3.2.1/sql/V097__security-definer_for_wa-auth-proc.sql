set search_path = gsc,public;

alter function gsc.wa_auth_user (varchar, varchar) security definer;
alter function gsc.wa_get_user_info (uuid) security definer;
alter function gsc.wa_get_agreement (uuid) security definer;
alter function gsc.wa_set_agreement (uuid) security definer;