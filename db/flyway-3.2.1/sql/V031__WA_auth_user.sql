﻿ -- object recreation
CREATE FUNCTION gsc.wa_auth_user (
  login varchar,
  password varchar
)
RETURNS integer AS
$body$
DECLARE
  l_oid INT;
BEGIN
  l_oid := (
  SELECT pa.oid
  FROM pg_authid pa
  WHERE pa.rolname = login AND
        pa.rolpassword = 'md5' || md5(PASSWORD || login));
    return l_oid;
exception 
	when no_data_found then
    return null;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER
COST 100;