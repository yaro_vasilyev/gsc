 -- object recreation
DROP FUNCTION gsc.u_create_db_user(p_rolname name, p_pwd_plain text, variadic p_roles name []);

CREATE OR REPLACE FUNCTION gsc.u_create_db_user (
  p_rolname name,
  p_pwd_plain text,
  variadic p_roles name [] = NULL::name[]
)
RETURNS void AS
$body$
-- Создает логин/пароль на базе, входящий в роли (список ролей прилагается)
declare 
  r name;
  l_create_role text := '';
begin
  if gsc.role_admin() = any(p_roles) then
    l_create_role := 'createrole';
  end if;

  execute format('create role %I login %s password ''%s'';', p_rolname, l_create_role, p_pwd_plain);
  if p_roles is not null then  
    foreach r in array p_roles loop
      execute format('grant %I to %I', r, p_rolname);
    end loop;
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION "gsc"."u_create_db_user"(p_rolname name, p_pwd_plain text, variadic p_roles name [])
  OWNER TO gsc;
