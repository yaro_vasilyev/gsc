set search_path = gsc,public;

alter view wa_v_exp_schedule owner to gsc;
alter view wa_v_expert_agreement  owner to gsc;


--func wa_get_user_info
create or replace function gsc.wa_get_user_info (guid_ uuid)
  returns table (type_user      varchar, 
                 surname        varchar, 
                 name           varchar,
                 patronymic     varchar,
                 fio            varchar,
                 post           varchar,
                 phone          varchar,
                 email          varchar) 
  language plpgsql
  stable
  returns null on null input
as $$
declare  
  l_rolname   gsc.users.rolname%type;
begin
  
  begin  
  select u.rolname
    into strict l_rolname
    from gsc.users as u
   where u.guid = guid_;
  exception
    when no_data_found then
      return;
  end;
  
  if l_rolname is null then 
    return;
  end if;

  if gsc._xxx_roles_any(l_rolname, gsc.role_manager(), gsc.role_rsm()) then
    return query select 'manager'::varchar as type_user,
                        ''::varchar as surname,
                        ''::varchar as name,
                        ''::varchar as patronymic,
                        obs.fio,
                        obs.post,
                        obs.phone1,
                        obs.email
                   from gsc.observer as obs
                  where obs.user_guid = guid_;
  elsif gsc._xxx_roles_all(l_rolname, gsc.role_expert()) then
    return query select 'expert'::varchar as type_user,
                        exp.surname,
                        exp.name,
                        exp.patronymic,
                        ''::varchar as fio,
                        exp.post,
                        exp.phone,
                        exp.email    
                   from gsc.expert as exp
                  where exp.user_guid = guid_;
  else
    raise exception 'User % should be in any of the roles: %, %, %.', 
                    guid_, role_expert(), role_manager(), role_rsm();
  end if;
end;
$$;

alter function gsc.wa_get_user_info (uuid)
  owner to gsc;

drop view if exists gsc.wa_v_exp_schedule;

CREATE OR REPLACE VIEW gsc.wa_v_exp_schedule (
    guid,
    region_name,
    id,
    period_id,
    monitor_rcoi,
    fixed,
    period_type,
    expert_id)
AS
SELECT u.guid,
    r.name AS region_name,
    sch.id,
    sch.period_id,
    sch.monitor_rcoi,
    sch.fixed,
    sch.period_type,
    sch.expert_id
FROM gsc.schedule sch
     JOIN gsc.expert e ON e.id = sch.expert_id
     JOIN gsc.region r ON r.id = sch.region_id
     LEFT JOIN gsc.users u ON gsc._xxx_roles_all(u.rolname, VARIADIC
         ARRAY[gsc.role_expert()::name]) AND u.guid = e.user_guid;

alter view gsc.wa_v_exp_schedule owner to gsc;


CREATE OR REPLACE VIEW gsc.wa_v_mgr_event (
    id,
    ts,
    channel,
    type,
    schedule_id,
    object_type)
AS
SELECT event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type
FROM gsc.event
WHERE event.project_id = gsc.current_project_id();

alter view gsc.wa_v_mgr_event owner to gsc;

CREATE OR REPLACE RULE "_DELETE" AS ON DELETE TO gsc.wa_v_mgr_event 
DO INSTEAD (
DELETE FROM gsc.event
  WHERE event.id = old.id; -- fck
);

CREATE OR REPLACE RULE "_INSERT" AS ON INSERT TO gsc.wa_v_mgr_event 
DO INSTEAD (
INSERT INTO gsc.event (ts, channel, type, schedule_id, object_type, project_id)
  VALUES (new.ts, new.channel, new.type, new.schedule_id, new.object_type, gsc.current_project_id())
  RETURNING event.id,
    event.ts,
    event.channel,
    event.type,
    event.schedule_id,
    event.object_type; -- flyway lol
);

CREATE OR REPLACE RULE "_UPDATE" AS ON UPDATE TO gsc.wa_v_mgr_event 
DO INSTEAD (
UPDATE gsc.event SET ts = new.ts, channel = new.channel, type = new.type, schedule_id = new.schedule_id, object_type = new.object_type
  WHERE event.id = old.id; -- flyway
);


CREATE OR REPLACE VIEW gsc.wa_v_mgr_expert (
    id,
    manager_guid,
    name,
    surname,
    patronymic)
AS
SELECT ex.id,
    o.user_guid AS manager_guid,
    ex.name,
    ex.surname,
    ex.patronymic
FROM gsc.expert ex
  join gsc.observer as o on o.id = ex.observer_id
 where ex.period_id = gsc.current_project_id();

alter view gsc.wa_v_mgr_expert owner to gsc;
