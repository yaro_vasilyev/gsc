set search_path = gsc,public;

-- migrate data
insert into station (region_id, project_id, code, name, address, tom)
  select distinct region_id, project_id, code, name, address, tom from ppe;


alter table ppe
  add column station_id integer,
  add constraint ppe_station_id_fkey foreign key (station_id) references station(id)
    on update cascade on delete restrict;

update ppe
   set station_id = (select id
                       from station
                      where ppe.project_id = station.project_id and
                            ppe.region_id = station.region_id and
                            ppe.code = station.code and 
                            ppe.name = station.name and
                            ppe.address = station.address and 
                            ppe.tom = station.tom);

alter table ppe
  alter column station_id set not null;

alter table ppe
  drop column tom,
  drop column address,
  drop column name,
  drop column code;