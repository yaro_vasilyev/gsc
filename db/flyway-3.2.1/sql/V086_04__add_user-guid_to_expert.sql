set search_path = gsc,public;



alter table gsc.expert
  add column user_guid uuid,
  add constraint expert_user_guid_fkey foreign key (user_guid) references gsc.users (guid) on update cascade on delete set null,
  add constraint expert_user_guid_unq unique (user_guid);
  
update gsc.expert as e
   set user_guid = (select u.guid from gsc.users as u where u.id = e.user_id);

comment on column gsc.expert.user_id is 'Column user_id is deprecated. Use column user_guid instead.';

update gsc.expert
   set user_id = null;

