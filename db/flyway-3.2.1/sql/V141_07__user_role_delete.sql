CREATE OR REPLACE FUNCTION gsc.user_role_delete (
  i_user_id integer,
  i_role_name varchar
)
RETURNS void AS
$body$
DECLARE
	l_login varchar;
BEGIN
  if not exists(select 1 from pg_roles where rolname = i_role_name) then
    raise exception 'Роли базы данных % не существует', i_role_name;
  end if;
  
  select u.user_login
  into strict l_login
  from gsc.users u
  where u.id = i_user_id;
  
  EXECUTE format('revoke  %I from %I', i_role_name, l_login);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

alter function gsc.user_role_delete (  i_user_id integer,
  i_role_name varchar
)
  owner to gsc;