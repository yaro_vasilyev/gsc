set search_path = gsc,public;


--------------------------------------------------------------------------------------------------------------

--func wa_get_user_info
create or replace function gsc.wa_get_user_info (guid_ uuid)
  returns table (type_user      varchar, 
                 surname        varchar, 
                 name           varchar,
                 patronymic     varchar,
                 fio            varchar,
                 post           varchar,
                 phone          varchar,
                 email          varchar) 
  language plpgsql
  stable
  returns null on null input
as $$
declare  
  l_rolname   gsc.users.rolname%type;
begin
  
  select u.rolname
    into /*strict*/ l_rolname
    from gsc.users as u
   where u.guid = guid_;
  
  if l_rolname is null then 
    raise exception 'User % not found.', guid_;
  end if;
   
  if gsc._xxx_roles_any(l_rolname, gsc.role_manager(), gsc.role_rsm()) then
    return query select 'manager'::varchar as type_user,
                        ''::varchar as surname,
                        ''::varchar as name,
                        ''::varchar as patronymic,
                        obs.fio,
                        obs.post,
                        obs.phone1,
                        obs.email
                   from gsc.observer as obs
                  where obs.user_guid = guid_;
  elsif gsc._xxx_roles_all(l_rolname, gsc.role_expert()) then
    return query select 'expert'::varchar as type_user,
                        exp.surname,
                        exp.name,
                        exp.patronymic,
                        ''::varchar as fio,
                        exp.post,
                        exp.phone,
                        exp.email    
                   from gsc.expert as exp
                  where exp.user_guid = guid_;
  else
    raise exception 'User % should be in any of the roles: %, %, %.', 
                    guid_, role_expert(), role_manager(), role_rsm();
  end if;
end;
$$;

alter function gsc.wa_get_user_info (uuid)
  owner to gsc;

--------------------------------------------------------------------------------------------------------------

--view wa_v_exp_monitor_result
create or replace view gsc.wa_v_exp_monitor_result (
    id,
    guid,
    schedule_id,
    date,
    violation_id,
    violator_id,
    note,
    object_type,
    schedule_detail_id,
    exam_type,
    event_id)
as
  select mr.id,
         u.guid,
         mr.schedule_id,
         mr.date,
         mr.violation_id,
         mr.violator_id,
         mr.note,
         mr.object_type,
         mr.schedule_detail_id,
         mr.exam_type,
         mr.event_id
    from gsc.monitor_result as mr
    join gsc.schedule as sch on sch.id = mr.schedule_id
    join gsc.expert as e on e.id = sch.expert_id
    join gsc.users as u on gsc._xxx_roles_all(u.rolname, gsc.role_expert()) and u.guid = e.user_guid;

alter view gsc.wa_v_exp_monitor_result
  owner to gsc;

create or replace rule "_DELETE" as on delete to gsc.wa_v_exp_monitor_result 
do instead (
delete from gsc.monitor_result where monitor_result.id = old.id; -- comment
);

create or replace rule "_INSERT" as on insert to gsc.wa_v_exp_monitor_result 
do instead (
  insert into gsc.monitor_result (
              schedule_id, 
              date, 
              violation_id, 
              violator_id, 
              note, 
              object_type, 
              schedule_detail_id, 
              exam_type, 
              event_id)
      values (new.schedule_id, 
              new.date, 
              new.violation_id, 
              new.violator_id, 
              new.note, 
              new.object_type, 
              new.schedule_detail_id, 
              new.exam_type, 
              new.event_id)
    returning monitor_result.id,
              null::uuid as uuid,
              monitor_result.schedule_id,
              monitor_result.date,
              monitor_result.violation_id,
              monitor_result.violator_id,
              monitor_result.note,
              monitor_result.object_type,
              monitor_result.schedule_detail_id,
              monitor_result.exam_type,
              monitor_result.event_id; -- comment
);

create or replace rule "_UPDATE" as on update to gsc.wa_v_exp_monitor_result 
do instead (
  update gsc.monitor_result 
     set schedule_id = new.schedule_id, 
         date = new.date, 
         violation_id = new.violation_id, 
         violator_id = new.violator_id, 
         note = new.note, 
         object_type = new.object_type, 
         schedule_detail_id = new.schedule_detail_id, 
         exam_type = new.exam_type, 
         event_id = new.event_id
   where monitor_result.id = old.id; -- comment
);

--------------------------------------------------------------------------------------------------------------
--view wa_v_exp_schedule   
drop view if exists gsc.wa_v_exp_schedule;

create or replace view gsc.wa_v_exp_schedule
as
select u.guid,
    sch.monitor_rcoi,
    sch.id as schedule_id,
    sch.period_type,
    r.name as region_name,
    schd.id as schedule_detail_id,
    schd.exam_date,
    ec.id as exec_id,
    em.exam_type,
    st.id as station_id,
    st.name as station_name,
    st.code as station_code,
    sub.name as subject_name
from gsc.schedule sch
     join gsc.expert as e on e.id = sch.expert_id
     join gsc.schedule_detail schd on sch.id = schd.schedule_id
     join gsc.station st on st.id = schd.station_id
     join gsc.exec ec on st.id = ec.station_id
     join gsc.exam em on em.id = ec.exam_id and schd.exam_date = em.exam_date
     join gsc.subject sub on sub.id = em.subject_id
     join gsc.region r on r.id = sch.region_id
     join gsc.users as u on gsc._xxx_roles_all(u.rolname, gsc.role_expert()) and u.guid = e.user_guid;

alter view gsc.wa_v_exp_schedule
  owner to gsc;

--------------------------------------------------------------------------------------------------------------


--view wa_v_exp_user_ppe
create or replace view gsc.wa_v_exp_user_ppe
as
select distinct st.id,
    st.name,
    st.code,
    u.guid
from gsc.station st
     join gsc.schedule_detail on schedule_detail.station_id = st.id
     join gsc.schedule on schedule_detail.schedule_id = schedule.id
     join gsc.expert as e on e.id = schedule.expert_id
     join gsc.users as u on gsc._xxx_roles_all(u.rolname, gsc.role_expert()) and u.guid = e.user_guid;

alter view gsc.wa_v_exp_user_ppe
  owner to gsc;

--------------------------------------------------------------------------------------------------------------

--func u_create_uesr
create or replace function gsc.u_create_uesr (
    type_                  integer,
    id_                    integer,
    pg_user                smallint = 0)
  returns varchar
  language plpgsql
  volatile
  called on null input
as $$
declare
  ob_role     text;
  email       varchar;
  password    varchar;  
begin
  case type_
    when 0 then
      select exp.email 
        into strict email
        from gsc.expert as exp
       where exp.id = id_;
    
      password:= gsc.u_random_string(8);
  --  password:= '12345'; -- for test
    
      insert into gsc.users (
                  parent_id, 
                  type_user, 
                  user_login, 
                  pass, 
                  guid, 
                  agreement,
                  rolname)
          values (id_, 
                  0, 
                  email, 
                  'md5' || md5(password || email),
                  uuid_generate_v4(), 
                  null,
                  gsc.role_expert());
    
      if pg_user = 1 then
        execute format('create role %I login password ''%s'' in role %I;', email, password, gsc.role_expert());
      end if;
      return password;
  
    when 1 then
      select ob.email, 
             case 
               when ob.observer_type_manager then gsc.role_manager()
               when ob.observer_type_rsm then gsc.role_rsm()
             end
        into strict email, 
                    ob_role
        from gsc.observer as ob
       where ob.id = id_;
      
      password := gsc.u_random_string(8);
  --  password := '12345'; -- for test
    
      insert into gsc.users (
                  parent_id, 
                  type_user, 
                  user_login, 
                  pass, 
                  guid, 
                  agreement,
                  rolname)
          values (id_, 
                  null,
                  email, 
                  'md5' || md5(password || email),
                  uuid_generate_v4(), 
                  null,
                  ob_role);
                  
      if pg_user = 1 then
        execute format('create role %I login password ''%s'' in role %I;', email, password, ob_role);
      end if;
      return password;
    else
      return null;
  end case;  
exception
  when no_data_found then
    return null;
end;
$$;

/*comment on function gsc.u_create_uesr(type_ integer, id_ integer, pg_user smallint)
is 'return password
type_ - 0=expert, 1=manager
type_ - id table expert or observer
pg_user - defaut 0, if 1 then create db user';
*/
alter function gsc.u_create_uesr (integer, integer, smallint)
  owner to gsc;

--------------------------------------------------------------------------------------------------------------
