set search_path = gsc,public;

create or replace function gsc.gsc_current_user() 
  returns table(Id gsc.users.id%type, 
                UserLogin gsc.users.user_login%type, 
                Role gsc.users.rolname%type, 
                Type gsc.users.type_user%type)
as $$
    select id,
           user_login,
           rolname,
           type_user
      from gsc.v_users 
     where rolname = current_user;
$$ language sql;

