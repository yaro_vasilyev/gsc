SET search_path = gsc, public;


--
-- Name: region_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    
--
-- Name: region; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE region (
    code integer NOT NULL,
    name character varying(100) NOT NULL,
    zone integer DEFAULT 1 NOT NULL,
    id integer DEFAULT nextval('region_id_seq'::regclass) NOT NULL,
    CONSTRAINT region_zone_check CHECK ((zone = ANY (ARRAY[1, 2, 3])))
);

--
-- Name: region_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


ALTER TABLE region OWNER TO gsc;




ALTER TABLE region_id_seq OWNER TO gsc;



--
-- Name: region_delete(integer, character varying); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION region_delete(pcode integer, pname character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin
    delete from gsc.region
     where code = pcode
       and name = pname;
  end;
$$;

ALTER FUNCTION gsc.region_delete(pcode integer, pname character varying) OWNER TO gsc;


--
-- Name: region_insert(integer, character varying); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION region_insert(pcode integer, pname character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin
    insert into gsc.region(code, name) values (pcode, pname);
  end;
$$;

ALTER FUNCTION gsc.region_insert(pcode integer, pname character varying) OWNER TO gsc;


--
-- Name: region_update(integer, character varying, integer, character varying); Type: FUNCTION; Schema: gsc; Owner: gsc
--
CREATE FUNCTION region_update(pcode integer, pname character varying, pcode_original integer, pname_original character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin
    update gsc.region
       set code = pcode,
           name = pname
     where code = pcode_original
       and name = pname_original;
  end;
$$;

ALTER FUNCTION gsc.region_update(pcode integer, pname character varying, pcode_original integer, pname_original character varying) OWNER TO gsc;
