﻿--------------- SQL ---------------

CREATE FUNCTION gsc.wa_get_user_info (
  id INTEGER
)
RETURNS TABLE (
  surname VARCHAR,
  name VARCHAR,
  patronimic VARCHAR,
  pers_agree BOOLEAN,
  visit_rcoi BOOLEAN
) AS
$body$
/*DECLARE  variable_name datatype;*/
BEGIN
  --todo
  return query
  SELECT '' AS surname,
         ''         AS NAME,
         ''         AS patronimic,
         FALSE AS pers_agree,
         FALSE AS visit_rcoi;
  /*EXCEPTION
WHEN exception_name THEN
  statements;*/
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;