set search_path = gsc,public;

create or replace function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            integer,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              integer,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  returns table (ts gsc.monitor_result.ts%type)
  volatile
  security definer
  language plpgsql
as $$
declare
  l_ts gsc.monitor_result.ts%type;
begin
  update gsc.monitor_result
     set schedule_id = new_schedule_id,
         date = new_date,
         violation_id = new_violation_id,
         violator_id = new_violator_id,
         note = new_note,
         object_type = new_object_type,
         schedule_detail_id = new_schedule_detail_id,
         exam_type = new_exam_type,
         event_id = new_event_id
   where monitor_result.id = p_id
     and monitor_result.ts = old_ts
         returning monitor_result.ts into l_ts;
  
  if not found then
    select gsc.err_concurrency_control('Результаты мониторинга');
  end if;

  return query select l_ts as ts;
end;
$$;

alter function gsc.monitor_result_update (
    p_id                       gsc.monitor_result.id%type,
    new_schedule_id            gsc.monitor_result.schedule_id%type,
    new_date                   gsc.monitor_result.date%type,
    new_violation_id           gsc.monitor_result.violation_id%type,
    new_violator_id            gsc.monitor_result.violator_id%type,
    new_note                   gsc.monitor_result.note%type,
    new_object_type            integer,
    new_schedule_detail_id     gsc.monitor_result.schedule_detail_id%type,
    new_exam_type              integer,
    new_event_id               gsc.monitor_result.event_id%type,
    old_ts                     gsc.monitor_result.ts%type)
  owner to gsc;
