DROP VIEW IF EXISTS wa_v_schedule;

CREATE VIEW wa_v_schedule as
  SELECT sch.expert_id,
         sch.monitor_rcoi,
         sch.id AS schedule_id,
         sch.period_type,
         r.name AS region_name,
         schd.id AS schedule_detail_id,
         schd.exam_date,
         ec.id AS exec_id,
         em.exam_type,
         st.id AS station_id,
         st.name AS station_name,
         st.code AS station_code,
         sub.name AS subject_name
  FROM gsc.schedule sch
       JOIN gsc.schedule_detail schd ON sch.id = schd.schedule_id
       JOIN gsc.station st ON st.id = schd.station_id
       JOIN gsc.exec ec ON st.id = ec.station_id
       JOIN gsc.exam em ON em.id = ec.exam_id AND schd.exam_date::DATE = em.exam_date::DATE
       JOIN gsc.subject sub ON sub.id = em.subject_id
       JOIN gsc.region r ON r.id = sch.region_id;