set search_path = gsc,public;


drop function if exists gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_id       gsc.observer.user_id%type);




create or replace function gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_guid     gsc.observer.user_guid%type)
  returns table (id gsc.observer.id%type, ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_id  gsc.observer.id%type;
  l_ts  gsc.observer.ts%type;
begin
  insert into gsc.observer (
              type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_guid)
      values (type,
              fio,
              wp,
              post,
              phone1,
              phone2,
              email,
              user_guid)
    returning observer.id, observer.ts
         into l_id, l_ts;
  return query select l_id as id, l_ts as ts;
end;
$$;

alter function gsc.observer_insert (
    type          integer,
    fio           gsc.observer.fio%type,
    wp            gsc.observer.wp%type,
    post          gsc.observer.post%type,
    phone1        gsc.observer.phone1%type,
    phone2        gsc.observer.phone2%type,
    email         gsc.observer.email%type,
    user_guid     gsc.observer.user_guid%type)
  owner to gsc;



drop function if exists gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_id   gsc.observer.user_id%type,
    old_ts        gsc.observer.ts%type);


create or replace function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_guid gsc.observer.user_guid%type,
    old_ts        gsc.observer.ts%type)
  returns table (ts gsc.observer.ts%type)
  volatile
  language plpgsql
as $$
declare
  l_ts  gsc.observer.ts%type;
begin
  update gsc.observer
     set type = new_type,
         fio = new_fio,
         wp = new_wp,
         post = new_post,
         phone1 = new_phone1,
         phone2 = new_phone2,
         email = new_email,
         user_guid = new_user_guid
   where observer.id = p_id
     and observer.ts = old_ts
         returning observer.ts into l_ts;
         
  if not found then
    select gsc.err_concurrency_control('Менеджер/наблюдатель');
  end if;
  
  return query select l_ts as ts;
end;
$$;

alter function gsc.observer_update (
    p_id          gsc.observer.id%type,
    new_type      integer,
    new_fio       gsc.observer.fio%type,
    new_wp        gsc.observer.wp%type,
    new_post      gsc.observer.post%type,
    new_phone1    gsc.observer.phone1%type,
    new_phone2    gsc.observer.phone2%type,
    new_email     gsc.observer.email%type,
    new_user_guid gsc.observer.user_guid%type,
    old_ts        gsc.observer.ts%type)
  owner to gsc;
