SET search_path = gsc, public;

--
-- Name: event; Type: TABLE; Schema: gsc; Owner: gsc; Tablespace: 
--

CREATE TABLE event (
    id integer NOT NULL,
    ts timestamp without time zone NOT NULL,
    channel d_channel_type NOT NULL,
    location d_violation_location NOT NULL,
    type d_event_type,
    schedule_id integer,
    monitor_result_id integer
);


--
-- Name: event_pkey; Type: CONSTRAINT; Schema: gsc; Owner: gsc; Tablespace: 
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);

--
-- Name: event_monitor_result_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_monitor_result_id_fkey FOREIGN KEY (monitor_result_id) REFERENCES monitor_result(id);


--
-- Name: event_schedule_id_fkey; Type: FK CONSTRAINT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES schedule(id);

    

ALTER TABLE event OWNER TO gsc;

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: gsc; Owner: gsc
--

CREATE SEQUENCE event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_id_seq OWNER TO gsc;

--
-- Name: event_id_seq; Type: SEQUENCE OWNED BY; Schema: gsc; Owner: gsc
--

ALTER SEQUENCE event_id_seq OWNED BY event.id;

--
-- Name: id; Type: DEFAULT; Schema: gsc; Owner: gsc
--

ALTER TABLE ONLY event ALTER COLUMN id SET DEFAULT nextval('event_id_seq'::regclass);

