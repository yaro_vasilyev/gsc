set search_path = gsc,public;

drop view if exists gsc.wa_v_schedule; -- WTF???
drop view if exists gsc.wa_v_exp_schedule;
drop view if exists gsc.wa_v_exam_info;

CREATE VIEW gsc.wa_v_exp_schedule (
        guid,
        region_name,
        id,
        period_id,
        monitor_rcoi,
        fixed,
        period_type)
    AS
    SELECT u.guid,
        r.name,
        sch.id,
        sch.period_id,
        sch.monitor_rcoi,
        sch.fixed,
        sch.period_type
    FROM gsc.schedule sch
         JOIN gsc.region r ON r.id = sch.region_id
         JOIN gsc.users u ON u.parent_id = sch.expert_id AND u.type_user = 0;
alter view gsc.wa_v_exp_schedule
  owner to gsc;
     
CREATE VIEW gsc.wa_v_exam_info (
        exec_id,
        exam_type,
        exam_date,
        station_id,
        station_name,
        station_code,
        subject_name)
    AS
    SELECT ec.id AS exec_id,
        em.exam_type,
        em.exam_date,
        st.id AS station_id,
        st.name AS station_name,
        st.code AS station_code,
        sub.name AS subject_name
    FROM gsc.station st
         JOIN gsc.exec ec ON st.id = ec.station_id
         JOIN gsc.exam em ON em.id = ec.exam_id
         JOIN gsc.subject sub ON sub.id = em.subject_id;
alter view gsc.wa_v_exam_info
  owner to gsc;