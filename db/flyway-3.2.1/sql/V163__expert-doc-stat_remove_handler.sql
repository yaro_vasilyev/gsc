

set search_path = gsc,public;

create or replace function gsc.expert_doc_stat_delete (
    p_expert_id            integer)
  returns void
  volatile
  language plpgsql
  security definer
as $$
begin
  -- do nothing here
end;
$$;

alter function gsc.expert_doc_stat_delete (p_expert_id integer)
  owner to gsc;