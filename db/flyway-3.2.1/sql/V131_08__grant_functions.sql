--------------- SQL ---------------

CREATE FUNCTION gsc.grant_users_list (
)
RETURNS varchar AS
$body$
SELECT 's_users_list'::varchar;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
LEAKPROOF;

ALTER FUNCTION gsc.grant_users_list() OWNER TO GSC;

--------------- SQL ---------------

CREATE OR REPLACE FUNCTION gsc.grant_rsm_list (
)
RETURNS varchar AS
$body$
SELECT 's_rsm_list'::varchar;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
LEAKPROOF
COST 100;

ALTER function gsc.grant_rsm_list() owner to gsc;